package system.main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import system.classes.*;

import com.common.Validators.CommonValidator;

import system.classes.ReservationInfo;
import system.classes.Address;
import system.classes.Flight;
import system.classes.Inbound;
import system.classes.Outbound;
import system.classes.RatesperPassenger;
import system.classes.SearchObject;
import system.classes.Traveler;
import system.classes.UIConfirmationPage;
import system.classes.UICreditCardPayInfo;
import system.classes.UIPaymentPage;
import system.classes.UIPriceInfo;
import system.classes.XMLPriceItinerary;
import system.pages.WebConfirmationPage;

public class CallCenter {
	
	String 							PropfilePath 	= "Properties.properties";
	String 							XLPath 			= "";
	ArrayList<Map<Integer, String>> CCXLtestData	= null;
	HashMap<String, String> 		Propertymap 	= new HashMap<String, String>();
	ArrayList<SearchObject> 		CCSearchList 	= null;
	WebDriver 						driver 			= null;
	String 							tracer 			= "";
	String							direction		= "";
	String							Discount		= "";
	boolean							discountApplied	= false;
	String							discountText	= "";
	
	
	
	
	public HashMap<String, String> getPropertymap() {
		return Propertymap;
	}

	public void setPropertymap(HashMap<String, String> propertymap) {
		Propertymap = propertymap;
	}

	public String getDiscountText() {
		return discountText;
	}

	public void setDiscountText(String discountText) {
		this.discountText = discountText;
	}

	public boolean searchAgain(WebDriver driver, SearchObject searchObject)
	{
		try {
			/*String trip = "";
			if(searchObject.getTriptype().equalsIgnoreCase("One Way Trip"))
			{
				trip = "oneway";
			}
			else if(searchObject.getTriptype().equalsIgnoreCase("Round Trip"))
			{
				trip = "roundtrip";
			}
			else if(searchObject.getTriptype().equalsIgnoreCase("Multi Destination"))
			{
				trip = "multidestination";
			}*/
			
			/*driver.switchTo().defaultContent();
			new Select(driver.findElement(By.className(propertyMap.get("ResultPg_TripType_DropDwn_class")))).selectByValue(trip);
			//driver.findElement(By.id(propertyMap.get("ResultPg_From_TxtBox_id"))).clear();
			driver.findElement(By.id(propertyMap.get("ResultPg_From_TxtBox_id"))).sendKeys(searchObject.getFrom().trim().split("\\|")[0]);
			((JavascriptExecutor)driver).executeScript("document.getElementById('"+propertyMap.get("ResultPg_From_Hidden_id")+"').setAttribute('value','"+searchObject.getFrom().trim()+"');");
			driver.findElement(By.id(propertyMap.get("ResultPg_To_TxtBox_id"))).clear();
			driver.findElement(By.id(propertyMap.get("ResultPg_To_TxtBox_id"))).sendKeys(searchObject.getTo().trim().split("\\|")[0]);
			((JavascriptExecutor)driver).executeScript("document.getElementById('"+propertyMap.get("ResultPg_To_Hidden_id")+"').setAttribute('value','"+searchObject.getTo().trim()+"');");
			driver.findElement(By.className("start-day")).sendKeys("17/03/2015");
			driver.findElement(By.className("end-day")).sendKeys("20/03/2015");*/
			driver.switchTo().frame("bec_container_frame");
			driver.findElement(By.id("search_btns_f")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("live_message_frame");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return true;
	}
	
	public String getDiscount() {
		return Discount;
	}

	public void setDiscount(String discount) {
		Discount = discount;
	}

	public boolean isDiscountApplied() {
		return discountApplied;
	}

	public void setDiscountApplied(boolean discountApplied) {
		this.discountApplied = discountApplied;
	}
	
	public UIPaymentPage getPaymentPage(WebDriver driver, XMLPriceItinerary XMLSelectedFlight, SearchObject searchObject ) throws WebDriverException, IOException
	{
		UIPaymentPage uipaymentpage = new UIPaymentPage();
		
		boolean twoway = false;
		direction = searchObject.getTriptype();
		if(direction.equals("Round Trip"))
		{
			twoway = true;
		}
		
		try
		{
			WebElement alloutbound = driver.findElement(By.id("table_flight_outbound_info"));
			String outbounddetails = alloutbound.getText();
			
			String[] outboundArray = outbounddetails.split("\\n");
			ArrayList<String> sets = new ArrayList<String>();
			ArrayList<Flight> outflightlist = new ArrayList<Flight>();
			
			for(int y=0; y<outboundArray.length; y++)
			{
				if(outboundArray[y].startsWith("Depart") && outboundArray[(y+1)].startsWith("Arrive"))
				{
					String h = outboundArray[y].concat("#").concat(outboundArray[y+1]);
					sets.add(h);
				}
			}
			
			Outbound outbound = new Outbound();
			for(int y=0; y<sets.size(); y++)
			{
				Flight flight = new Flight();
				String Sflight = sets.get(y);
				String depart = Sflight.split("#")[0];
				String arrive = Sflight.split("#")[1];
				
				String[] departArray = depart.split(" ");
				
				String Ddate = "-";
				Ddate = CommonValidator.formatDateToCommon(departArray[2].trim(), "dd-MMM-yyyy");
				flight.setDepartureDate(Ddate);
				String Dtime = "-";
				Dtime = CommonValidator.formatTimeToCommon(departArray[3].trim());
				flight.setDepartureTime(Dtime);
				
				flight.setFlightNo(depart.split("[()]")[3]);
				flight.setDepartureLocationCode(depart.split("[()]")[1]);
				
				String[] arriveArray = arrive.split(" ");
				String Adate = "-";
				Adate = CommonValidator.formatDateToCommon(arriveArray[2].trim(), "dd-MMM-yyyy");
				String Atime = "-";
				Atime = CommonValidator.formatTimeToCommon(arriveArray[3].trim());
				flight.setArrivalDate(Adate);
				flight.setArrivalTime(Atime);
				
				flight.setArrivalLocationCode(arrive.split("[()]")[1]);
				
				outflightlist.add(flight);	
			}
			outbound.setOutBflightlist(outflightlist);
			uipaymentpage.setOutbound(outbound);
			
			WebElement allinbound = null;
			
			if(twoway)
			{
				Inbound inbound = new Inbound();
				allinbound = driver.findElement(By.id("table_flight_inbound_info"));
				String inbounddetails = allinbound.getText();
				
				String[] inboundArray = inbounddetails.split("\\n");
				ArrayList<String> insets = new ArrayList<String>();
				ArrayList<Flight> inflightlist = new ArrayList<Flight>(); 
				
				for(int y=0; y<inboundArray.length; y++)
				{
					if(inboundArray[y].startsWith("Depart") && inboundArray[(y+1)].startsWith("Arrive"))
					{
						String h = inboundArray[y].concat("#").concat(inboundArray[y+1]);
						insets.add(h);
					}
				}
				
				for(int y=0; y<insets.size(); y++)
				{
					Flight flight = new Flight();
					String Sflight = insets.get(y);
					String depart = Sflight.split("#")[0];
					String arrive = Sflight.split("#")[1];
					
					String[] departArray = depart.split(" ");
					
					String Ddate = "-";
					Ddate = CommonValidator.formatDateToCommon(departArray[2].trim(), "dd-MMM-yyyy");
					flight.setDepartureDate(Ddate);
					String Dtime = "-";
					Dtime = CommonValidator.formatTimeToCommon(departArray[3].trim());
					flight.setDepartureTime(Dtime);
					
					flight.setFlightNo(depart.split("[()]")[3]);
					flight.setDepartureLocationCode(depart.split("[()]")[1]);
					
					String[] arriveArray = arrive.split(" ");
					String Adate = "-";
					Adate = CommonValidator.formatDateToCommon(arriveArray[2].trim(), "dd-MMM-yyyy");
					String Atime = "-";
					Atime = CommonValidator.formatTimeToCommon(arriveArray[3].trim());
					flight.setArrivalDate(Adate);
					flight.setArrivalTime(Atime);
					
					flight.setArrivalLocationCode(arrive.split("[()]")[1]);
					
					inflightlist.add(flight);
				}
				
				inbound.setInBflightlist(inflightlist);
				uipaymentpage.setInbound(inbound);
			}
			
			WebElement ratetable = driver.findElement(By.id("booking_rate_details_table"));
			ArrayList<WebElement> rateTRtags = new ArrayList<WebElement> (ratetable.findElements(By.tagName("tr")) );
			
			int passengercount = rateTRtags.size() - 6;
			ArrayList<RatesperPassenger> ratelist = new ArrayList<RatesperPassenger>();
			for(int h=2; h<(2+passengercount); h++)
			{	
				RatesperPassenger rate = new RatesperPassenger();
				ArrayList<WebElement> tdInrateTR = new ArrayList<WebElement> (rateTRtags.get(h).findElements(By.tagName("td")) );
				
				if(tdInrateTR.get(0).getText().equalsIgnoreCase("Adult"))
				{
					rate.setPassengertype("ADT");
					rate.setRateperpsngr(tdInrateTR.get(1).getText());
					rate.setNoofpassengers(tdInrateTR.get(2).getText());
					rate.setTotal(tdInrateTR.get(3).getText());
				}
				if(tdInrateTR.get(0).getText().equalsIgnoreCase("Children"))
				{
					rate.setPassengertype("CHD");
					rate.setRateperpsngr(tdInrateTR.get(1).getText());
					rate.setNoofpassengers(tdInrateTR.get(2).getText());
					rate.setTotal(tdInrateTR.get(3).getText());
				}
				if(tdInrateTR.get(0).getText().equalsIgnoreCase("Infant"))
				{
					rate.setPassengertype("INF");
					rate.setRateperpsngr(tdInrateTR.get(1).getText());
					rate.setNoofpassengers(tdInrateTR.get(2).getText());
					rate.setTotal(tdInrateTR.get(3).getText());
				}
				
				ratelist.add(rate);
			}
			uipaymentpage.setRatelist(ratelist);
			
			
			int forward = 2 + passengercount;
			
			UIPriceInfo priceinfo = new UIPriceInfo();
			
			priceinfo.setTotalbefore(rateTRtags.get(forward).findElement(By.id("td_rate_details_total_rate_before_taxes_1")).getText());
			priceinfo.setTax(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_taxes_1")).getText());
			priceinfo.setBookingfee(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_booking_fee_1")).getText());
			priceinfo.setTotalCost(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_total_cost_1")).getText());
			
			WebElement totfareTable = driver.findElement(By.id("total_transfer_info_table"));
			String[] totfareArray = totfareTable.getText().split("\\n");
			priceinfo.setCurrencycode(totfareArray[0].split("[()]")[1]);
			priceinfo.setSubtotal(totfareArray[1].split(":")[1].trim());
			priceinfo.setTaxplusother(totfareArray[3]);
			priceinfo.setTotal(totfareArray[5]);
			priceinfo.setAmountprocess(totfareArray[7]);
			priceinfo.setAmountprocessAirline(totfareArray[8].split(" ")[5]);
			
			//Discount UI
			try {
				if(searchObject.isApplyDiscount())
				{
					String discount = "0";
					discount = driver.findElement(By.id("td_rate_details_total_rate_before_taxes_1")).getText();
					if(discount.contains("-"))
					{
						discount = discount.trim().replace("-", "");
					}
					priceinfo.setDiscount(discount);
				}
				
			} catch (Exception e) {
				
			}
			
			uipaymentpage.setUisummarypay(priceinfo);
			
			if(searchObject.getPaymentMode().equalsIgnoreCase("Pay Online"))
			{
				UICreditCardPayInfo cardinfo = new UICreditCardPayInfo();
				
				String totredbookingvalue = driver.findElement(By.id("totchgpgamt")).getText();
				String totredCurrencyCode = driver.findElement(By.id("totchgpgcurrency")).getText().split("[()]")[1];
				String gross = driver.findElement(By.id("pkg_depositval")).getText();
				String tax = driver.findElement(By.id("totaltaxpgcurrid")).getText();
				String total = driver.findElement(By.id("totalchargeamtpgcurrid")).getText();
				String amountprocessnw = driver.findElement(By.id("totalpaynowamountpgcurrid")).getText();
				String amountprocessbyAirLine = driver.findElement(By.id("totalamountbyairlinepgcurrid")).getText();
				
				cardinfo.setTotalpackage_bookVal(totredbookingvalue);
				cardinfo.setTotalpackage_bookVal_CurrCode(totredCurrencyCode);
				cardinfo.setSubtotal(gross);
				cardinfo.setTaxandfees(tax);
				cardinfo.setTotal(total);
				cardinfo.setAmountprocess(amountprocessnw);
				cardinfo.setAmountprocessAirline(amountprocessbyAirLine);
				
				uipaymentpage.setCreditcardpay(cardinfo);
			}
			
			WebElement				cancellation		= driver.findElement(By.id("table_cancellation_policies"));
			ArrayList<WebElement>	cancellationTRtags	= new ArrayList<WebElement>(cancellation.findElements(By.tagName("tr")));
			String					flightdeadline		= "";
			try
			{
				flightdeadline = cancellationTRtags.get(1).getText();
				flightdeadline = flightdeadline.split(": ")[1].trim();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				flightdeadline = "";
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/"+System.currentTimeMillis()/1000+".jpg"));
			}
			uipaymentpage.setCancellationdate(flightdeadline);
			
			String packagecancel = "";
			try
			{
				packagecancel =	cancellationTRtags.get((cancellationTRtags.size() - 3)).getText();
				packagecancel =	packagecancel.split(":")[1];
			}
			catch(Exception e)
			{
				e.printStackTrace();
				packagecancel = "";
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/"+System.currentTimeMillis()/1000+".jpg"));
			}
			uipaymentpage.setPackageDeadline(packagecancel);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			TakesScreenshot screen = (TakesScreenshot)driver;
			FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/"+System.currentTimeMillis()/1000+".jpg"));
		}
		
		return uipaymentpage;
	}

	public UIConfirmationPage getConfirmationPage(WebDriver driver, XMLPriceItinerary XMLSelectedFlight) throws WebDriverException, IOException
	{
		UIConfirmationPage uiconfirmationpage = new UIConfirmationPage();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("live_message_frame");
		
		boolean twoway = false;
		if(XMLSelectedFlight.getDirectiontype().equals("Circle"))
		{
			twoway = true;
		}
		
		try
		{
			String ReservationNo = driver.findElement(By.id("p_flight_booking_reservation_no_1")).getText().split(":")[1];
			String suppConfirmationNo = driver.findElement(By.id("p_flight_booking_supplier_confirmation_no_1")).getText().split(":")[1];
			
			uiconfirmationpage.setReservationNo(ReservationNo.trim());
			uiconfirmationpage.setAvailable(true);
			uiconfirmationpage.setSupplierConfirmationNo(suppConfirmationNo.trim());
			
			WebElement alloutbound = driver.findElement(By.id("table_flight_outbound_info"));
			String outbounddetails = alloutbound.getText();
			//System.out.println(alloutbound.getText());
			
			String[] outboundArray = outbounddetails.split("\\n");
			ArrayList<String> sets = new ArrayList<String>();
			ArrayList<Flight> outflightlist = new ArrayList<Flight>(); 
			
			for(int y=0; y<outboundArray.length; y++)
			{
				if(outboundArray[y].startsWith("Depart") && outboundArray[(y+1)].startsWith("Arrive"))
				{
					String h = outboundArray[y].concat("#").concat(outboundArray[y+1]);
					sets.add(h);
				}
			}
			
			Outbound outbound = new Outbound();
			for(int y=0; y<sets.size(); y++)
			{
				Flight flight = new Flight();
				String Sflight = sets.get(y);
				String depart = Sflight.split("#")[0];
				String arrive = Sflight.split("#")[1];
				
				String[] departArray = depart.split(" ");
				
				//System.out.println(departArray[1]);
				String Ddate = "-";
				Ddate = CommonValidator.formatDateToCommon(departArray[2].trim(), "dd-MMM-yyyy");
				flight.setDepartureDate(Ddate);
				String Dtime = "-";
				Dtime = CommonValidator.formatTimeToCommon(departArray[3].trim());
				flight.setDepartureTime(Dtime);
				
				flight.setFlightNo(depart.split("[()]")[3]);
				flight.setDepartureLocationCode(depart.split("[()]")[1]);
				
				String[] arriveArray = arrive.split(" ");
				String Adate = "-";
				Adate = CommonValidator.formatDateToCommon(arriveArray[2].trim(), "dd-MMM-yyyy");
				String Atime = "-";
				Atime = CommonValidator.formatTimeToCommon(arriveArray[3].trim());
				flight.setArrivalDate(Adate);
				flight.setArrivalTime(Atime);
				
				flight.setArrivalLocationCode(arrive.split("[()]")[1]);
				//System.out.println(flight.getArrivalLocationCode());
				
				outflightlist.add(flight);
				
			}
			outbound.setOutBflightlist(outflightlist);
			uiconfirmationpage.setOutbound(outbound);
			
			//Inbound
			WebElement allinbound = null;
			
			if(twoway)
			{
				Inbound inbound = new Inbound();
				allinbound = driver.findElement(By.id("table_flight_inbound_info"));
				//System.out.println();
				String inbounddetails = allinbound.getText();
				//System.out.println(inbounddetails);
				
				String[] inboundArray = inbounddetails.split("\\n");
				ArrayList<String> insets = new ArrayList<String>();
				ArrayList<Flight> inflightlist = new ArrayList<Flight>(); 
				
				for(int y=0; y<inboundArray.length; y++)
				{
					if(inboundArray[y].startsWith("Depart") && inboundArray[(y+1)].startsWith("Arrive"))
					{
						String h = inboundArray[y].concat("#").concat(inboundArray[y+1]);
						insets.add(h);
					}
				}
				
				for(int y=0; y<insets.size(); y++)
				{
					Flight flight = new Flight();
					String Sflight = insets.get(y);
					//System.out.println(Sflight);
					String depart = Sflight.split("#")[0];
					String arrive = Sflight.split("#")[1];
					
					String[] departArray = depart.split(" ");
					
					String Ddate = "-";
					Ddate = CommonValidator.formatDateToCommon(departArray[2].trim(), "dd-MMM-yyyy");
					flight.setDepartureDate(Ddate);
					String Dtime = "-";
					Dtime = CommonValidator.formatTimeToCommon(departArray[3].trim());
					flight.setDepartureTime(Dtime);
					
					flight.setFlightNo(depart.split("[()]")[3]);
					flight.setDepartureLocationCode(depart.split("[()]")[1]);
					
					String[] arriveArray = arrive.split(" ");
					String Adate = "-";
					Adate = CommonValidator.formatDateToCommon(arriveArray[2].trim(), "dd-MMM-yyyy");
					String Atime = "-";
					Atime = CommonValidator.formatTimeToCommon(arriveArray[3].trim());
					flight.setArrivalDate(Adate);
					flight.setArrivalTime(Atime);
					flight.setArrivalLocationCode(arrive.split("[()]")[1]);
					//System.out.println(flight.getArrivalLocationCode());
					
					inflightlist.add(flight);
				}
				
				inbound.setInBflightlist(inflightlist);
				uiconfirmationpage.setInbound(inbound);
			}
			
			WebElement ratetable = driver.findElement(By.id("booking_rate_details_table"));
			ArrayList<WebElement> rateTRtags = new ArrayList<WebElement> (ratetable.findElements(By.tagName("tr")) );
			//System.out.println("No of tr tags : "+rateTRtags.size());
			
			int passengercount = rateTRtags.size() - 6;
			ArrayList<RatesperPassenger> ratelist = new ArrayList<RatesperPassenger>();;
			for(int h=2; h<(2+passengercount); h++)
			{
				
				//System.out.println(i+=1);
				RatesperPassenger rate = new RatesperPassenger();
				ArrayList<WebElement> tdInrateTR = new ArrayList<WebElement> (rateTRtags.get(h).findElements(By.tagName("td")) );
				
				if(tdInrateTR.get(0).getText().equalsIgnoreCase("Adult"))
				{
					rate.setPassengertype("ADT");
					rate.setRateperpsngr(tdInrateTR.get(1).getText());
					rate.setNoofpassengers(tdInrateTR.get(2).getText());
					rate.setTotal(tdInrateTR.get(3).getText());
				}
				if(tdInrateTR.get(0).getText().equalsIgnoreCase("Children"))
				{
					rate.setPassengertype("CHD");
					rate.setRateperpsngr(tdInrateTR.get(1).getText());
					rate.setNoofpassengers(tdInrateTR.get(2).getText());
					rate.setTotal(tdInrateTR.get(3).getText());
				}
				if(tdInrateTR.get(0).getText().equalsIgnoreCase("Infant"))
				{
					rate.setPassengertype("INF");
					rate.setRateperpsngr(tdInrateTR.get(1).getText());
					rate.setNoofpassengers(tdInrateTR.get(2).getText());
					rate.setTotal(tdInrateTR.get(3).getText());
				}
				
				ratelist.add(rate);
			}
			uiconfirmationpage.setRatelist(ratelist);
			
			int forward = 2 + passengercount;
			
			//System.out.println(rateTRtags.get(forward).getText());
			UIPriceInfo priceinfo = new UIPriceInfo();
			
			priceinfo.setTotalbefore(rateTRtags.get(forward).findElement(By.id("td_rate_details_total_rate_before_taxes_1")).getText());
			priceinfo.setTax(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_taxes_1")).getText());
			priceinfo.setBookingfee(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_booking_fee_1")).getText());
			priceinfo.setTotalCost(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_total_cost_1")).getText());
			
			WebElement totfareTable = driver.findElement(By.className("transfer_info_table"));
			//System.out.println("------------=================-----------------");
			//System.out.println(totfareTable.getText());
			String[] totfareArray = totfareTable.getText().split("\\n");
			//System.out.println(totfareArray[0].split("[()]")[0]);
			//priceinfo.setCurrencycode(totfareArray[0].split("[()]")[1]);
			priceinfo.setSubtotal(totfareArray[0].split(":")[1].trim());
			priceinfo.setTaxplusother(totfareArray[1].split(":")[1].trim());
			priceinfo.setTotal(totfareArray[2].split(":")[1].trim());
			priceinfo.setAmountprocess(totfareArray[3].split(" ")[4].trim());
			//System.out.println(totfareArray[9].split(" ")[5]);
			priceinfo.setAmountprocessAirline(totfareArray[4].split(" ")[5]);
			
			uiconfirmationpage.setUisummarypay(priceinfo);
			try
			{
				UICreditCardPayInfo cardinfo = new UICreditCardPayInfo();
			
				cardinfo.setReferenceNo(driver.findElement(By.id("paymentrefno")).getText());
				try {
					cardinfo.setMerchantTrackID(driver.findElement(By.id("paymentid")).getText());
				} catch (Exception e) {
					System.out.println("payment");
				}
				
				cardinfo.setPaymentId(driver.findElement(By.id("paymenttrackid")).getText());
				cardinfo.setAmount(driver.findElement(By.id("onlinecharge")).getText());
				uiconfirmationpage.setCreditcardpay(cardinfo);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Credit Card Info"+System.currentTimeMillis()/1000+".jpg"));
			}
			
			try
			{
				WebElement cancellation = driver.findElement(By.id("table_cancellation_policies"));
				
				ArrayList<WebElement> cancellationTRtags = new ArrayList<WebElement>(cancellation.findElements(By.tagName("tr")));
				
				String flightdeadline = "";
				flightdeadline = cancellationTRtags.get(1).getText().split(": ")[1].trim();

				uiconfirmationpage.setCancellationdate(flightdeadline);
				
				String packagecancel = "";
				packagecancel = cancellationTRtags.get(cancellationTRtags.size()-1).getText();
				packagecancel = packagecancel.split(":")[1].trim();
				uiconfirmationpage.setPackageDeadline(packagecancel);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				String a = "";
				uiconfirmationpage.setCancellationdate(a);
				String b = "";
				uiconfirmationpage.setPackageDeadline(b);
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Confirmation Page CalcellationDate"+System.currentTimeMillis()/1000+".jpg"));
			}
			
			try
			{
				//Main customer
				WebElement mainCus = driver.findElement(By.id("table_customer_details1"));
				ArrayList<WebElement> mainCusTRlist = new ArrayList<WebElement>(mainCus.findElements(By.tagName("tr")));
				
				String fname = mainCusTRlist.get(0).findElement(By.id("cusfirstname")).getText();
				String lname = mainCusTRlist.get(0).findElement(By.id("cuslastname")).getText();
				String phoneNo = mainCusTRlist.get(1).findElement(By.id("custelephone")).getText();
				String emergencyNo = mainCusTRlist.get(1).findElement(By.id("cusaltphone")).getText();
				String email1 = mainCusTRlist.get(2).findElement(By.id("cusemail1")).getText();
				String email2 = mainCusTRlist.get(2).findElement(By.id("cusemail2")).getText();
				String address1 = mainCusTRlist.get(3).findElement(By.id("cusaddress1")).getText();
				String Country = mainCusTRlist.get(4).findElement(By.id("cuscountry")).getText();
				String state = mainCusTRlist.get(4).findElement(By.id("cusstate")).getText();
				String city = mainCusTRlist.get(5).findElement(By.id("cuscity")).getText();
				String postalcode = "";
				try
				{
					ArrayList<WebElement> postcode = new ArrayList<WebElement>(mainCusTRlist.get(5).findElements(By.id("cuspostcode")));
					postalcode = postcode.get(1).getText().trim();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				
				Traveler maincustomer = new Traveler();
				Address address = new Address();
				maincustomer.setGivenName(fname);
				maincustomer.setSurname(lname);
				maincustomer.setPhoneNumber(phoneNo);
				maincustomer.setEmergencyNo(emergencyNo);
				maincustomer.setEmail(email1);
				maincustomer.setAltemail(email2);
				
				address.setAddressStreetNo(address1);
				address.setAddressCountry(Country);
				address.setStateProv(state);
				address.setAddressCity(city);
				address.setPostalCode(postalcode);
				
				maincustomer.setAddress(address);
				
				uiconfirmationpage.setMaincustomer(maincustomer);
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Confirmation page Customer Dertails"+System.currentTimeMillis()/1000+".jpg"));
			}
			
			//Passengers
			ArrayList<Traveler> adults = new ArrayList<Traveler>();
			ArrayList<Traveler> children = new ArrayList<Traveler>();
			ArrayList<Traveler> infants = new ArrayList<Traveler>();
			WebElement passengers = driver.findElement(By.className("table_flight_passenger_details"));
			ArrayList<WebElement> passengersTRlist = new ArrayList<WebElement>(passengers.findElements(By.tagName("tr")));
			
			for(int y=1; y<passengersTRlist.size(); y++)
			{
				//System.out.println(passengersTRlist.size());
				String type = passengersTRlist.get(y).findElement(By.className("Tlightblue")).getText();
				
				if(type.contains("Adult"))
				{
					Traveler adult = new Traveler();
					String title = passengersTRlist.get(y).findElement(By.className("amo_width_5")).getText();
					String gname = passengersTRlist.get(y).findElement(By.className("amo_width_25")).getText();
					String sname = passengersTRlist.get(y).findElement(By.className("amo_width_20")).getText();
					ArrayList<WebElement> list = new ArrayList<WebElement>(passengersTRlist.get(y).findElements(By.className("amo_width_10")));
					String contact = list.get(0).getText();
					String frequentflyer = list.get(1).getText();
					String frequentflyerNo = list.get(2).getText();
					String eticketNo = list.get(3).getText();	
					
					adult.setPassengertypeCode("ADT");
					adult.setNamePrefixTitle(title);
					adult.setGivenName(gname);
					adult.setSurname(sname);
					adult.setPhoneNumber(contact);
					adult.setFrequent_Flyer(frequentflyer);
					adult.setFrequent_Flyer_No(frequentflyerNo);
					adult.setETicket_No(eticketNo);
					
					adults.add(adult);
				}
				else if(type.contains("Child"))
				{
					Traveler child = new Traveler();
					String title = passengersTRlist.get(y).findElement(By.className("amo_width_5")).getText();
					String gname = passengersTRlist.get(y).findElement(By.className("amo_width_25")).getText();
					String sname = passengersTRlist.get(y).findElement(By.className("amo_width_20")).getText();
					ArrayList<WebElement> list = new ArrayList<WebElement>(passengersTRlist.get(y).findElements(By.className("amo_width_10")));
					String contact = list.get(0).getText();
					String frequentflyer = list.get(1).getText();
					String frequentflyerNo = list.get(2).getText();
					String eticketNo = list.get(3).getText();
					
					child.setPassengertypeCode("CHD");
					child.setNamePrefixTitle(title);
					child.setGivenName(gname);
					child.setSurname(sname);
					child.setPhoneNumber(contact);
					child.setFrequent_Flyer(frequentflyer);
					child.setFrequent_Flyer_No(frequentflyerNo);
					child.setETicket_No(eticketNo);
					
					children.add(child);
				}
				else if(type.contains("Infant"))
				{
					Traveler infant = new Traveler();
					String title = passengersTRlist.get(y).findElement(By.className("amo_width_5")).getText();
					String gname = passengersTRlist.get(y).findElement(By.className("amo_width_25")).getText();
					String sname = passengersTRlist.get(y).findElement(By.className("amo_width_20")).getText();
					ArrayList<WebElement> list = new ArrayList<WebElement>(passengersTRlist.get(y).findElements(By.className("amo_width_10")));
					String contact = list.get(0).getText();
					String frequentflyer = list.get(1).getText();
					String frequentflyerNo = list.get(2).getText();
					String eticketNo = list.get(3).getText();	
					
					infant.setPassengertypeCode("INF");
					infant.setNamePrefixTitle(title);
					infant.setGivenName(gname);
					infant.setSurname(sname);
					infant.setPhoneNumber(contact);
					infant.setFrequent_Flyer(frequentflyer);
					infant.setFrequent_Flyer_No(frequentflyerNo);
					infant.setETicket_No(eticketNo);
					
					infants.add(infant);
				}
			}//end of for
			
			uiconfirmationpage.setAdult(adults);
			uiconfirmationpage.setChildren(children);
			uiconfirmationpage.setInfant(infants);

		}
		catch(Exception e)
		{
			e.printStackTrace();
			TakesScreenshot screen = (TakesScreenshot)driver;
			FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Confirmation Page error"+System.currentTimeMillis()/1000+".jpg"));
		}
		
		return uiconfirmationpage;	
	}

	public WebDriver Fill_Reservation_Details(WebDriver driver, ReservationInfo fillObj, SearchObject sobj)
	{
		driver.switchTo().defaultContent();
		driver.switchTo().frame("live_message_frame");
		
		/*try {
			if(sobj.getPaymentMode().equalsIgnoreCase("Pay Offline"))
			{
				driver.findElement(By.id("user_id")).sendKeys(Propertymap.get("portal.username"));
				driver.findElement(By.id("user_password")).sendKeys(Propertymap.get("portal.password"));
				driver.findElement(By.id("activate_mg")).click();
				Thread.sleep(4000);
			}
		} catch (Exception e) {
			System.out.println("Active manager override failed");
		}*/
		
		/*try {
			driver.findElement(By.id("cancellation")).click();
		} catch (Exception e) {
			System.out.println("didnt click the cancellation");
		}
		*/
		
		int adults		= fillObj.getAdult().size();
		int children	= fillObj.getChildren().size();
		int infants		= fillObj.getInfant().size();
		try
		{
			WebDriverWait wait = new WebDriverWait(driver, 10,5);
			Traveler main = fillObj.getMaincustomer();
		
			try {
				driver.findElement(By.id("cusFName")).clear();
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cusFName")));
				driver.findElement(By.id("cusFName")).sendKeys(main.getGivenName());
				driver.findElement(By.id("cusLName")).clear();
				driver.findElement(By.id("cusLName")).sendKeys(main.getSurname());
				driver.findElement(By.id("cusAdd1")).clear();
				driver.findElement(By.id("cusAdd1")).sendKeys(main.getAddress().getAddressStreetNo());
				driver.findElement(By.id("cusAdd2")).clear();
				driver.findElement(By.id("cusAdd2")).sendKeys(main.getAddress().getAddressStreetNo()+"123");
				driver.findElement(By.id("cusCity")).clear();
				driver.findElement(By.id("cusCity")).sendKeys(main.getAddress().getAddressCity());
				new Select(driver.findElement(By.id("cusCountry"))).selectByVisibleText(main.getAddress().getAddressCountry());
				
				
				/*try {
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cusState")));
					try {
						if(driver.findElement(By.id("cusState")).isDisplayed())
						{
							new Select(driver.findElement(By.id("cusState"))).selectByVisibleText(main.getAddress().getStateProv());
						}
						
					} catch (Exception e) {
						System.out.println("State is not entered");
					}
					
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cusZip")));
					try {
						if(driver.findElement(By.id("cusZip")).isDisplayed())
						{
							driver.findElement(By.id("cusZip")).sendKeys(main.getAddress().getCountryZip());
						}
					} catch (Exception e) {
						System.out.println("Zip code is not entered");
					}
				} catch (Exception e) {
					System.out.println("State is not entered");
				}*/
				
				driver.findElement(By.id("cusareacodetext")).clear();
				driver.findElement(By.id("cusareacodetext")).sendKeys("11");
				driver.findElement(By.id("cusPhonetext")).clear();
				driver.findElement(By.id("cusPhonetext")).sendKeys(main.getPhoneNumber());
				driver.findElement(By.id("cusEmail")).clear();
				driver.findElement(By.id("cusEmail")).sendKeys(main.getEmail());
				driver.findElement(By.id("cusPhoneemgtext")).clear();
				driver.findElement(By.id("cusPhoneemgtext")).sendKeys(main.getPhoneNumber());
				
			} catch (Exception e) {
				System.out.println("Main customer information fill fail");
			}

			try {
				for(int a = 0; a<adults; a++)
				{
					try {
						new Select(driver.findElement(By.id("tfpi_adult_title_"+a+""))).selectByVisibleText(fillObj.getAdult().get(a).getNamePrefixTitle());
					} catch (Exception e) {
						try {
							new Select(driver.findElement(By.id("tfpi_adult_title_"+a+""))).selectByVisibleText(fillObj.getAdult().get(a).getNamePrefixTitle());
						} catch (Exception e2) {
							System.out.println("Adult title is not selected");	
						}
					}
						
					driver.findElement(By.id("tfpi_adult_first_name_"+a+"")).clear();
					driver.findElement(By.id("tfpi_adult_first_name_"+a+"")).sendKeys(fillObj.getAdult().get(a).getGivenName());
					driver.findElement(By.id("tfpi_adult_last_name_"+a+"")).clear();
					driver.findElement(By.id("tfpi_adult_last_name_"+a+"")).sendKeys(fillObj.getAdult().get(a).getSurname());
					driver.findElement(By.id("ftpi_adult_contact_information_"+a+"")).clear();
					driver.findElement(By.id("ftpi_adult_contact_information_"+a+"")).sendKeys(fillObj.getAdult().get(a).getPhoneNumber());
					driver.findElement(By.id("ftpi_adult_passport_number_"+a+"")).clear();
					driver.findElement(By.id("ftpi_adult_passport_number_"+a+"")).sendKeys(fillObj.getAdult().get(a).getPassportNo());
					
					((JavascriptExecutor)driver).executeScript("$('#adultPassportExpDate_Air_"+a+"').val('"+fillObj.getAdult().get(a).getPassportExpDate().trim()+"');");
						
					((JavascriptExecutor)driver).executeScript("$('#adultPassportIssueDate_Air_"+a+"').val('"+fillObj.getAdult().get(a).getPassprtIssuDate().trim()+"');");
						
					((JavascriptExecutor)driver).executeScript("$('#adultDateOfBirth_Air_"+a+"').val('"+fillObj.getAdult().get(a).getBirthDay().trim()+"');");
						
					new Select(driver.findElement(By.id("ftpi_adult_sex_"+a+""))).selectByValue(fillObj.getAdult().get(a).getGender());
					new Select(driver.findElement(By.id("ftpi_adult_nationality_"+a+""))).selectByVisibleText(fillObj.getAdult().get(a).getNationality());
					new Select(driver.findElement(By.id("ftpi_adult_pic_country_"+a+""))).selectByVisibleText(fillObj.getAdult().get(a).getPassportIssuCountry());
				}
			} catch (Exception e) {
				System.out.println("Adult information fill fail");
			}
			
			try {
				for(int c=0; c<children; c++)
				{
					new Select(driver.findElement(By.id("tfpi_child_title_"+c+""))).selectByValue(fillObj.getChildren().get(c).getNamePrefixTitle());
					driver.findElement(By.id("tfpi_child_first_name_"+c+"")).clear();
					driver.findElement(By.id("tfpi_child_first_name_"+c+"")).sendKeys(fillObj.getChildren().get(c).getGivenName());
					driver.findElement(By.id("tfpi_child_last_name_"+c+"")).clear();
					driver.findElement(By.id("tfpi_child_last_name_"+c+"")).sendKeys(fillObj.getChildren().get(c).getSurname());
					driver.findElement(By.id("tfpi_child_contact_information_"+c+"")).clear();
					driver.findElement(By.id("tfpi_child_contact_information_"+c+"")).sendKeys(fillObj.getChildren().get(c).getPhoneNumber());
					driver.findElement(By.id("tfpi_child_passport_number_"+c+"")).clear();
					driver.findElement(By.id("tfpi_child_passport_number_"+c+"")).sendKeys(fillObj.getChildren().get(c).getPassportNo());
						
					((JavascriptExecutor)driver).executeScript("$('#childPassportExpDate_Air_"+c+"').val('"+fillObj.getChildren().get(c).getPassportExpDate().trim()+"');");
						
					((JavascriptExecutor)driver).executeScript("$('#childPassportIssueDate_Air_"+c+"').val('"+fillObj.getChildren().get(c).getPassprtIssuDate().trim()+"');");
						
					((JavascriptExecutor)driver).executeScript("$('#childDateOfBirth_Air_"+c+"').val('"+fillObj.getChildren().get(c).getBirthDay().trim()+"');");
						
					new Select(driver.findElement(By.id("tfpi_child_sex_"+c+""))).selectByVisibleText(fillObj.getChildren().get(c).getGender());
					new Select(driver.findElement(By.id("tfpi_child_nationality_"+c+""))).selectByVisibleText(fillObj.getChildren().get(c).getNationality());
					new Select(driver.findElement(By.id("tfpi_child_pic_country_"+c+""))).selectByVisibleText(fillObj.getChildren().get(c).getPassportIssuCountry());
				}
			} catch (Exception e) {
				System.out.println("Children information fill fail");
			}	
			
			try {
				for(int i = 0; i<infants; i++)
				{
					new Select(driver.findElement(By.id("tfpi_infant_title_"+i+""))).selectByVisibleText(fillObj.getInfant().get(i).getNamePrefixTitle());
					driver.findElement(By.id("tfpi_infant_first_name_"+i+"")).clear();
					driver.findElement(By.id("tfpi_infant_first_name_"+i+"")).sendKeys(fillObj.getInfant().get(i).getGivenName());
					driver.findElement(By.id("tfpi_infant_last_name_"+i+"")).clear();
					driver.findElement(By.id("tfpi_infant_last_name_"+i+"")).sendKeys(fillObj.getInfant().get(i).getSurname());
					
					((JavascriptExecutor)driver).executeScript("$('#infantBirthDate_Air_"+i+"').val('"+fillObj.getInfant().get(i).getBirthDay().trim()+"');");
					new Select(driver.findElement(By.id("tfpi_infant_sex_"+i+""))).selectByVisibleText(fillObj.getInfant().get(i).getGender());
					driver.findElement(By.id("tfpi_infant_passport_number_"+i+"")).sendKeys(fillObj.getInfant().get(i).getPassportNo());
				}
			} catch (Exception e) {
				System.out.println("Infant informatin fill fail");
			}	
		}
		catch(Exception e)
		{
			System.out.println("Error in passanger details filling");
		}
		
		try {
			if(sobj.getPaymentMode().equalsIgnoreCase("Pay Offline"))
			{
				driver.findElement(By.id("user_id")).sendKeys(Propertymap.get("portal.username"));
				driver.findElement(By.id("user_password")).sendKeys(Propertymap.get("portal.password"));
				driver.findElement(By.id("activate_mg")).click();
				Thread.sleep(4000);
			}
		} catch (Exception e) {
			System.out.println("Active manager override failed");
		}
		
		try {
			if( !sobj.isApplyDiscount() && !sobj.isQuotation() && sobj.isApplyDiscountAtPayPg() )
			{
				driver.findElement(By.id("discount_coupon_user_id")).sendKeys(Propertymap.get("DiscountCouponNo"));
				driver.findElement(By.id("search_btns")).click();
				
				if(driver.findElement(By.id("dialog-alert-message-WJ_22")).isDisplayed())
				{
					this.setDiscountText(driver.findElement(By.id("dialog-alert-message-WJ_22")).getText().trim());
					try {
						driver.findElement(By.className("ui-button-text")).click();
					} catch (Exception e) {
						System.out.println("Discount coupon message box didnt close");
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Discount application faild");
		}
		
		try {
			WebDriverWait wait2 = new WebDriverWait(driver, 7000);
			if(sobj.getPaymentMode().equalsIgnoreCase("Pay Online"))
			{
				try {
					if(!driver.findElement(By.id("pay_online")).isSelected())
					{
						driver.findElement(By.id("pay_online")).click();
					}
				} catch (Exception e) {
					System.out.println("Pay online is selected is clicked");
				}
			}
			else if(sobj.getPaymentMode().equalsIgnoreCase("Pay Offline"))
			{
				driver.findElement(By.id("pay_offline")).click();
				
				Thread.sleep(5000);
				wait2.until(ExpectedConditions.presenceOfElementLocated(By.id("paymentGuaranteed")));
				driver.findElement(By.id("paymentGuaranteed")).click();
				
				Thread.sleep(3000);
				new Select(driver.findElement(By.id("payment_method_1"))).selectByValue("CS");
				wait2.until(ExpectedConditions.presenceOfElementLocated(By.id("payment_reference")));
				driver.findElement(By.id("payment_reference")).sendKeys("Cash1123");
			}
		} catch (Exception e) {
			System.out.println("Payment mode selection failed");
		}
		
		try {
			driver.findElement(By.id("customer_mail_send_yes")).click();
			driver.findElement(By.id("voucher_mail_send_yes")).click();
			driver.findElement(By.id("supplier_mail_send_yes")).click();
			driver.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div.wrapper div.content_body_wrapper.group div.content_full_col_wrapper div.full_column div.rounded_widget table#table_send_mail.table_send_mail tbody tr td.amo_width_6 input#supplier_mail_send_yes")).click();
		} catch (Exception e) {
			
		try {
			driver.findElement(By.id("cancellation")).click();
		} catch (Exception et) {
			System.out.println("didnt click the cancellation");
		}	
		
		}
		try {
			driver.findElement(By.id("continue_submit")).click();
		} catch (Exception e) {
			System.out.println("Continue button click failed");
		}
		return driver;
	}

	public WebConfirmationPage getConfirmationPageQ(WebDriver driver, XMLPriceItinerary XMLSelectedFlight, HashMap<String, String> PMap ) throws WebDriverException, IOException
	{
		WebConfirmationPage uiconfirmationpage = new WebConfirmationPage(PMap);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("live_message_frame");
		
		boolean twoway = false;
		if(XMLSelectedFlight.getDirectiontype().equals("Circle"))
		{
			twoway = true;
		}
		
		try
		{
			String ReservationNo = driver.findElement(By.id("p_flight_booking_reservation_no_1")).getText().split(":")[1];
			String suppConfirmationNo = driver.findElement(By.id("p_flight_booking_supplier_confirmation_no_1")).getText().split(":")[1];
			
			uiconfirmationpage.setReservationNo(ReservationNo.trim());
			uiconfirmationpage.setAvailable(true);
			uiconfirmationpage.setSupplierConfirmationNo(suppConfirmationNo.trim());
			
			WebElement alloutbound = driver.findElement(By.id("table_flight_outbound_info"));
			String outbounddetails = alloutbound.getText();
			//System.out.println(alloutbound.getText());
			
			String[] outboundArray = outbounddetails.split("\\n");
			ArrayList<String> sets = new ArrayList<String>();
			ArrayList<Flight> outflightlist = new ArrayList<Flight>(); 
			
			for(int y=0; y<outboundArray.length; y++)
			{
				if(outboundArray[y].startsWith("Depart") && outboundArray[(y+1)].startsWith("Arrive"))
				{
					String h = outboundArray[y].concat("#").concat(outboundArray[y+1]);
					sets.add(h);
				}
			}
			
			Outbound outbound = new Outbound();
			for(int y=0; y<sets.size(); y++)
			{
				Flight flight = new Flight();
				String Sflight = sets.get(y);
				String depart = Sflight.split("#")[0];
				String arrive = Sflight.split("#")[1];
				
				String[] departArray = depart.split(" ");
				
				//System.out.println(departArray[1]);
				flight.setDepartureDate(departArray[1]);
				flight.setDepartureTime(departArray[2]);
				
				/*String[] g = depart.split("[()]");
				for (int u=0; u<g.length; u++) {
					System.out.println(g[u]);
				}*/
				//System.out.println(depart);
				//System.out.println(depart.split("[()]")[0]);
				//System.out.println(depart.split("[()]")[1]);
				//System.out.println(depart.split("[()]")[2]);
				flight.setFlightNo(depart.split("[()]")[3]);
				flight.setDepartureLocationCode(depart.split("[()]")[1]);
				
				String[] arriveArray = arrive.split(" ");
				//System.out.println(arriveArray[1]);
				flight.setArrivalDate(arriveArray[2]);
				flight.setArrivalTime(arriveArray[3]);
				
				/*String[] g = arrive.split("[()]");
				for (int u=0; u<g.length; u++) 
				{
					//System.out.println(g[u]);
				}*/
				flight.setArrivalLocationCode(arrive.split("[()]")[1]);
				//System.out.println(flight.getArrivalLocationCode());
				
				outflightlist.add(flight);
				
			}
			outbound.setOutBflightlist(outflightlist);
			uiconfirmationpage.setOutbound(outbound);
			
			//Inbound
			WebElement allinbound = null;
			
			if(twoway)
			{
				Inbound inbound = new Inbound();
				allinbound = driver.findElement(By.id("table_flight_inbound_info"));
				//System.out.println();
				String inbounddetails = allinbound.getText();
				//System.out.println(inbounddetails);
				
				String[] inboundArray = inbounddetails.split("\\n");
				ArrayList<String> insets = new ArrayList<String>();
				ArrayList<Flight> inflightlist = new ArrayList<Flight>(); 
				
				for(int y=0; y<inboundArray.length; y++)
				{
					if(inboundArray[y].startsWith("Depart") && inboundArray[(y+1)].startsWith("Arrive"))
					{
						String h = inboundArray[y].concat("#").concat(inboundArray[y+1]);
						insets.add(h);
					}
				}
				
				for(int y=0; y<insets.size(); y++)
				{
					Flight flight = new Flight();
					String Sflight = insets.get(y);
					//System.out.println(Sflight);
					String depart = Sflight.split("#")[0];
					String arrive = Sflight.split("#")[1];
					
					String[] departArray = depart.split(" ");
					
					flight.setDepartureDate(departArray[1]);
					flight.setDepartureTime(departArray[2]);
					
					String[] g = depart.split("[()]");
					for (int u=0; u<g.length; u++) {
						System.out.println(g[u]);
					}
					//System.out.println("????????????????????????????????"+depart);
					flight.setFlightNo(depart.split("[()]")[3]);
					flight.setDepartureLocationCode(depart.split("[()]")[1]);
					
					String[] arriveArray = arrive.split(" ");
					flight.setArrivalDate(arriveArray[2]);
					flight.setArrivalTime(arriveArray[3]);
					
					/*String[] g = arrive.split("[()]");
					for (int u=0; u<g.length; u++) 
					{
						//System.out.println(g[u]);
					}*/
					flight.setArrivalLocationCode(arrive.split("[()]")[1]);
					//System.out.println(flight.getArrivalLocationCode());
					
					inflightlist.add(flight);
				}
				
				inbound.setInBflightlist(inflightlist);
				uiconfirmationpage.setInbound(inbound);
			}
			
			WebElement ratetable = driver.findElement(By.id("booking_rate_details_table"));
			ArrayList<WebElement> rateTRtags = new ArrayList<WebElement> (ratetable.findElements(By.tagName("tr")) );
			//System.out.println("No of tr tags : "+rateTRtags.size());
			
			int passengercount = rateTRtags.size() - 6;
			ArrayList<RatesperPassenger> ratelist = new ArrayList<RatesperPassenger>();;
			for(int h=2; h<(2+passengercount); h++)
			{
				
				//System.out.println(i+=1);
				RatesperPassenger rate = new RatesperPassenger();
				ArrayList<WebElement> tdInrateTR = new ArrayList<WebElement> (rateTRtags.get(h).findElements(By.tagName("td")) );
				
				if(tdInrateTR.get(0).getText().equalsIgnoreCase("Adult"))
				{
					rate.setPassengertype("ADT");
					rate.setRateperpsngr(tdInrateTR.get(1).getText());
					rate.setNoofpassengers(tdInrateTR.get(2).getText());
					rate.setTotal(tdInrateTR.get(3).getText());
				}
				if(tdInrateTR.get(0).getText().equalsIgnoreCase("Children"))
				{
					rate.setPassengertype("CHD");
					rate.setRateperpsngr(tdInrateTR.get(1).getText());
					rate.setNoofpassengers(tdInrateTR.get(2).getText());
					rate.setTotal(tdInrateTR.get(3).getText());
				}
				if(tdInrateTR.get(0).getText().equalsIgnoreCase("Infant"))
				{
					rate.setPassengertype("INF");
					rate.setRateperpsngr(tdInrateTR.get(1).getText());
					rate.setNoofpassengers(tdInrateTR.get(2).getText());
					rate.setTotal(tdInrateTR.get(3).getText());
				}
				
				ratelist.add(rate);
			}
			uiconfirmationpage.setRatelist(ratelist);
			
			int forward = 2 + passengercount;
			
			//System.out.println(rateTRtags.get(forward).getText());
			UIPriceInfo priceinfo = new UIPriceInfo();
			
			priceinfo.setTotalbefore(rateTRtags.get(forward).findElement(By.id("td_rate_details_total_rate_before_taxes_1")).getText());
			priceinfo.setTax(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_taxes_1")).getText());
			priceinfo.setBookingfee(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_booking_fee_1")).getText());
			priceinfo.setTotalCost(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_total_cost_1")).getText());
			
			WebElement totfareTable = driver.findElement(By.className("transfer_info_table"));
			//System.out.println("------------=================-----------------");
			//System.out.println(totfareTable.getText());
			String[] totfareArray = totfareTable.getText().split("\\n");
			//System.out.println(totfareArray[0].split("[()]")[0]);
			//priceinfo.setCurrencycode(totfareArray[0].split("[()]")[1]);
			priceinfo.setSubtotal(totfareArray[0].split(":")[1].trim());
			priceinfo.setTaxplusother(totfareArray[1].split(":")[1].trim());
			priceinfo.setTotal(totfareArray[2].split(":")[1].trim());
			priceinfo.setAmountprocess(totfareArray[3].split(" ")[4].trim());
			//System.out.println(totfareArray[9].split(" ")[5]);
			priceinfo.setAmountprocessAirline(totfareArray[4].split(" ")[5]);
			
			uiconfirmationpage.setUisummarypay(priceinfo);
			try
			{
				UICreditCardPayInfo cardinfo = new UICreditCardPayInfo();
			
				cardinfo.setReferenceNo(driver.findElement(By.id("paymentrefno")).getText());
				try {
					cardinfo.setMerchantTrackID(driver.findElement(By.id("paymentid")).getText());
				} catch (Exception e) {
					System.out.println("payment");
				}
				
				cardinfo.setPaymentId(driver.findElement(By.id("paymenttrackid")).getText());
				cardinfo.setAmount(driver.findElement(By.id("onlinecharge")).getText());
				uiconfirmationpage.setCreditcardpay(cardinfo);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Credit Card Info"+System.currentTimeMillis()/1000+".jpg"));
			}
			
			try
			{
				WebElement cancellation = driver.findElement(By.id("table_cancellation_policies"));
				
				ArrayList<WebElement> cancellationTRtags = new ArrayList<WebElement>(cancellation.findElements(By.tagName("tr")));
				
				String flightdeadline = "";
				flightdeadline = cancellationTRtags.get(1).getText().split(": ")[1].trim();

				uiconfirmationpage.setCancellationdate(flightdeadline);
				
				String packagecancel = "";
				packagecancel = cancellationTRtags.get(cancellationTRtags.size()-1).getText();
				packagecancel = packagecancel.split(":")[1].trim();
				uiconfirmationpage.setPackageDeadline(packagecancel);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				String a = "";
				uiconfirmationpage.setCancellationdate(a);
				String b = "";
				uiconfirmationpage.setPackageDeadline(b);
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Confirmation Page CalcellationDate"+System.currentTimeMillis()/1000+".jpg"));
			}
			
			try
			{
				//Main customer
				WebElement mainCus = driver.findElement(By.id("table_customer_details1"));
				ArrayList<WebElement> mainCusTRlist = new ArrayList<WebElement>(mainCus.findElements(By.tagName("tr")));
				
				String fname = mainCusTRlist.get(0).findElement(By.id("cusfirstname")).getText();
				String lname = mainCusTRlist.get(0).findElement(By.id("cuslastname")).getText();
				String phoneNo = mainCusTRlist.get(1).findElement(By.id("custelephone")).getText();
				String emergencyNo = mainCusTRlist.get(1).findElement(By.id("cusaltphone")).getText();
				String email1 = mainCusTRlist.get(2).findElement(By.id("cusemail1")).getText();
				String email2 = mainCusTRlist.get(2).findElement(By.id("cusemail2")).getText();
				String address1 = mainCusTRlist.get(3).findElement(By.id("cusaddress1")).getText();
				String Country = mainCusTRlist.get(4).findElement(By.id("cuscountry")).getText();
				String state = mainCusTRlist.get(4).findElement(By.id("cusstate")).getText();
				String city = mainCusTRlist.get(5).findElement(By.id("cuscity")).getText();
				String postalcode = "";
				try
				{
					ArrayList<WebElement> postcode = new ArrayList<WebElement>(mainCusTRlist.get(5).findElements(By.id("cuspostcode")));
					postalcode = postcode.get(1).getText().trim();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				
				Traveler maincustomer = new Traveler();
				Address address = new Address();
				maincustomer.setGivenName(fname);
				maincustomer.setSurname(lname);
				maincustomer.setPhoneNumber(phoneNo);
				maincustomer.setEmergencyNo(emergencyNo);
				maincustomer.setEmail(email1);
				maincustomer.setAltemail(email2);
				
				address.setAddressStreetNo(address1);
				address.setAddressCountry(Country);
				address.setStateProv(state);
				address.setAddressCity(city);
				address.setPostalCode(postalcode);
				
				maincustomer.setAddress(address);
				
				uiconfirmationpage.setMaincustomer(maincustomer);
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Confirmation page Customer Dertails"+System.currentTimeMillis()/1000+".jpg"));
			}
			
			//Passengers
			ArrayList<Traveler> adults = new ArrayList<Traveler>();
			ArrayList<Traveler> children = new ArrayList<Traveler>();
			ArrayList<Traveler> infants = new ArrayList<Traveler>();
			WebElement passengers = driver.findElement(By.className("table_flight_passenger_details"));
			ArrayList<WebElement> passengersTRlist = new ArrayList<WebElement>(passengers.findElements(By.tagName("tr")));
			
			for(int y=1; y<passengersTRlist.size(); y++)
			{
				//System.out.println(passengersTRlist.size());
				String type = passengersTRlist.get(y).findElement(By.className("Tlightblue")).getText();
				
				if(type.contains("Adult"))
				{
					Traveler adult = new Traveler();
					String title = passengersTRlist.get(y).findElement(By.className("amo_width_5")).getText();
					String gname = passengersTRlist.get(y).findElement(By.className("amo_width_25")).getText();
					String sname = passengersTRlist.get(y).findElement(By.className("amo_width_20")).getText();
					ArrayList<WebElement> list = new ArrayList<WebElement>(passengersTRlist.get(y).findElements(By.className("amo_width_10")));
					String contact = list.get(0).getText();
					String frequentflyer = list.get(1).getText();
					String frequentflyerNo = list.get(2).getText();
					String eticketNo = list.get(3).getText();	
					
					adult.setPassengertypeCode("ADT");
					adult.setNamePrefixTitle(title);
					adult.setGivenName(gname);
					adult.setSurname(sname);
					adult.setPhoneNumber(contact);
					adult.setFrequent_Flyer(frequentflyer);
					adult.setFrequent_Flyer_No(frequentflyerNo);
					adult.setETicket_No(eticketNo);
					
					adults.add(adult);
				}
				else if(type.contains("Child"))
				{
					Traveler child = new Traveler();
					String title = passengersTRlist.get(y).findElement(By.className("amo_width_5")).getText();
					String gname = passengersTRlist.get(y).findElement(By.className("amo_width_25")).getText();
					String sname = passengersTRlist.get(y).findElement(By.className("amo_width_20")).getText();
					ArrayList<WebElement> list = new ArrayList<WebElement>(passengersTRlist.get(y).findElements(By.className("amo_width_10")));
					String contact = list.get(0).getText();
					String frequentflyer = list.get(1).getText();
					String frequentflyerNo = list.get(2).getText();
					String eticketNo = list.get(3).getText();
					
					child.setPassengertypeCode("CHD");
					child.setNamePrefixTitle(title);
					child.setGivenName(gname);
					child.setSurname(sname);
					child.setPhoneNumber(contact);
					child.setFrequent_Flyer(frequentflyer);
					child.setFrequent_Flyer_No(frequentflyerNo);
					child.setETicket_No(eticketNo);
					
					children.add(child);
				}
				else if(type.contains("Infant"))
				{
					Traveler infant = new Traveler();
					String title = passengersTRlist.get(y).findElement(By.className("amo_width_5")).getText();
					String gname = passengersTRlist.get(y).findElement(By.className("amo_width_25")).getText();
					String sname = passengersTRlist.get(y).findElement(By.className("amo_width_20")).getText();
					ArrayList<WebElement> list = new ArrayList<WebElement>(passengersTRlist.get(y).findElements(By.className("amo_width_10")));
					String contact = list.get(0).getText();
					String frequentflyer = list.get(1).getText();
					String frequentflyerNo = list.get(2).getText();
					String eticketNo = list.get(3).getText();	
					
					infant.setPassengertypeCode("INF");
					infant.setNamePrefixTitle(title);
					infant.setGivenName(gname);
					infant.setSurname(sname);
					infant.setPhoneNumber(contact);
					infant.setFrequent_Flyer(frequentflyer);
					infant.setFrequent_Flyer_No(frequentflyerNo);
					infant.setETicket_No(eticketNo);
					
					infants.add(infant);
				}
			}//end of for
			
			uiconfirmationpage.setAdult(adults);
			uiconfirmationpage.setChildren(children);
			uiconfirmationpage.setInfant(infants);

		}
		catch(Exception e)
		{
			e.printStackTrace();
			TakesScreenshot screen = (TakesScreenshot)driver;
			FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Confirmation Page error"+System.currentTimeMillis()/1000+".jpg"));
		}
		
		return uiconfirmationpage;	
	}

	public WebDriver CCRUN(WebDriver driver, SearchObject searchObject) throws WebDriverException, IOException
	{
		try
		{
			driver.get(Propertymap.get("Portal.Url").concat("/operations/reservation/CallCenterWarSearchCriteria.do?module=operations"));
			
			//Handle alert
			try {
				Alert alert = driver.switchTo().alert();
				alert.accept();
			} catch (Exception e) {
				//e.printStackTrace();
			}
			
			//Frame switch
			driver.switchTo().defaultContent();
			driver.switchTo().frame("live_message_frame");
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			
			if(searchObject.getTOBooking().equalsIgnoreCase("Yes"))
			{
				driver.findElement(By.id("tour_operator_radio_WJ_9")).click();
			}
			
			
			
			String currency = searchObject.getSellingCurrency().trim();
			try {
				new Select(driver.findElement(By.id("select_currency"))).selectByValue(currency); 		
			} catch (Exception e) {
				
			}

			//Click flight button
			driver.switchTo().frame("bec_container_frame");
			driver.findElement(By.id("flights")).click();
			try {
				new Select(driver.findElement(By.id("F_Country"))).selectByVisibleText(searchObject.getCountry().trim());
			} catch (Exception e) {
				
			}
			try {
				new Select(driver.findElement(By.id("F_Country"))).selectByVisibleText(searchObject.getCountry().trim());
			} catch (Exception e) {
				
			}
		
			//Selecting trip type
			if(searchObject.getTriptype().equals("Round Trip"))
			{
				driver.findElement(By.id("Air_TripType2")).click();
				((JavascriptExecutor)driver).executeScript("$('#air_departure').val('"+searchObject.getDepartureDate()+"');");
				((JavascriptExecutor)driver).executeScript("$('#air_arrival').val('"+searchObject.getReturnDate()+"');");
				new Select(driver.findElement(By.id("Air_DepTime_a"))).selectByVisibleText(searchObject.getDepartureTime().trim());
				new Select(driver.findElement(By.id("Air_RetTime_a"))).selectByVisibleText(searchObject.getReturnTime().trim());
			}
			if(searchObject.getTriptype().equals("One Way Trip"))
			{
				driver.findElement(By.id("Air_TripType1")).click();
				((JavascriptExecutor)driver).executeScript("$('#air_departure').val('"+searchObject.getDepartureDate()+"');");
				new Select(driver.findElement(By.id("Air_DepTime_a"))).selectByVisibleText(searchObject.getDepartureTime().trim());
			}
			if(searchObject.getTriptype().equals("Multi Destination"))
			{
				driver.findElement(By.id("Air_TripType3")).click();
			}
			
			//Select From To values
			try {
				String from = searchObject.getFrom().trim().split("\\|")[0];
				driver.findElement(By.id("air_Loc_a")).sendKeys(from);//Type to avoid the location value missing error
				((JavascriptExecutor)driver).executeScript("document.getElementById('hid_air_Loc_a').setAttribute('value','"+searchObject.getFrom()+"');");//Setting the hidden value
				String to = searchObject.getTo().trim().split("\\|")[0];
				driver.findElement(By.id("air_Loc1_a")).sendKeys(to);//Type to avoid the location value missing error
				((JavascriptExecutor)driver).executeScript("document.getElementById('hid_air_Loc1_a').setAttribute('value','"+searchObject.getTo()+"');");//Setting the hidden value
				
			} catch (Exception e) {
				System.out.println("From To selection fail anna..!");
			}
			
			//Select flexible flights
			try {
				if(searchObject.isFlexible())
				{
					driver.findElement(By.id("isFlexSearch_F")).click();
				}
			} catch (Exception e) {
				System.out.println("Selecting Flexible search fail anna.!");
			}
			
			
			//Select Adult / Child
			try {
				new Select(driver.findElement(By.id("R1occAdults_F"))).selectByValue(searchObject.getAdult());//select number of adults
				
				if(!searchObject.getChildren().equals("0"))//Count no of children from excel
				{
					new Select(driver.findElement(By.id("R1occChildren_F"))).selectByValue(searchObject.getChildren().trim());
					
					for(int i = 1; i<=searchObject.getChildrenAge().size(); i++)
					{
						new Select(driver.findElement(By.xpath(".//*[@id='F_R1childage_"+i+"']"))).selectByValue(searchObject.getChildrenAge().get((i-1)).trim());//Child Loop
					}
				}
		
				new Select(driver.findElement(By.id("R1occInfant_F"))).selectByValue(searchObject.getInfant().trim());
			} catch (Exception e) {
				System.out.println("filling passengers fail anna..!");
			}
			
			//Selecting cabin class
			try {
				new Select(driver.findElement(By.id("Aira_FlightClass"))).selectByValue(searchObject.getCabinClass().trim());
				
			} catch (Exception e) {
				System.out.println("Selecting cabin class fail anna..!");
			}
			
			//Selecting a currency
			try {
				if(!searchObject.getPreferredCurrency().trim().equals("Select Currency"))
				{
					new Select(driver.findElement(By.id("F_consumerCurrencyCode"))).selectByValue(searchObject.getPreferredCurrency().trim());
				}
			} catch (Exception e) {
				System.out.println("Selecting currency fail anna..!");
			}
			
			//Select preferred airline
			try {
				if(!searchObject.getPreferredAirline().trim().equals("NONE"))
				{
					driver.findElement(By.id("Air_line")).sendKeys(searchObject.getPreferredAirline().trim());
				}
			} catch (Exception e) {
				System.out.println("Select preffer airline fail anna..!");
			}
			
			//Selecting non stop
			try {
				if(searchObject.isNonStop())
				{
					driver.findElement(By.xpath(".//*[@id='pref_nonstop_F']/div/input")).click();
				}
			} catch (Exception e) {
				System.out.println("Selecting non-stop faild anna..!");
			}
			
			//Apply discount
			try {
				driver.findElement(By.id("discountCoupon_No_H")).sendKeys(Propertymap.get("DiscountCouponNo"));
			} catch (Exception e) {
				System.out.println("Select discount coupon fail anna..!");
			}
			
			try {
				driver.findElement(By.className("required error")).click();
			} catch (Exception e) {
				System.out.println("Select cancellation check box..!");
			}
			
			//Click search button
			try {
				driver.findElement(By.id("search_btns_f")).click();
			} catch (Exception e) {
				System.out.println("Clicking Search Button in BE fail");
			}	
			
			
		}	
		catch(Exception e)
		{
			e.printStackTrace();
			TakesScreenshot screen = (TakesScreenshot)driver;
			FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/"+System.currentTimeMillis()/1000+".jpg"));
		}

		return driver;
		
	}
	
	public ArrayList<UIFlightItineraryCC> getResults(WebDriver driver, SearchObject searchObject) throws WebDriverException, IOException
	{
		boolean twoway = false;
		ArrayList<UIFlightItineraryCC> Resultlist = new ArrayList<UIFlightItineraryCC>(); 
		int nflights;
		driver.switchTo().defaultContent();
		driver.switchTo().frame("live_message_frame");
		String numofflights = driver.findElement(By.xpath("/html/body/div[7]/div[3]/div[1]/div/div[3]/div/div/div/div[1]/div/div[1]")).getText();
		int navcount1 = 0 ;
		int navcount2 = 0;
		try
		{
			String[] hotels = numofflights.split(" ");
		
			nflights = Integer.parseInt(hotels[6]);//Get no of flights
			navcount1 = nflights/10;//Per page 10 find how many pages to navigate
			navcount2 = nflights%10;//Remaining flights
		}
		catch(Exception e)
		{
			e.printStackTrace();
			TakesScreenshot screen = (TakesScreenshot)driver;
			FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/"+System.currentTimeMillis()/1000+".jpg"));
		}
		
		int navigate = navcount1;
		
		if((navcount2>0)&&(navcount2<10))
		{
			navigate+=1;	
		}
		
		int clickpattern = 2; 
		int count = 0;
		
		
		ArrayList<WebElement> flights = null;
		
		if(Integer.parseInt(Propertymap.get("Flight_PageIterations")) <= navigate)
			navigate  = Integer.parseInt(Propertymap.get("Flight_PageIterations"));
		
		int counter = 0;
		while(count<navigate)
		{
			try
			{	
				flights = new ArrayList<WebElement>(driver.findElements(By.xpath("/html/body/div[7]/div[3]/div[1]/div/div[3]/div/div/div/div[3]/div[2]/table")));
				Iterator<WebElement> flightsIter = flights.iterator();
				int nt = 1;
				
				whileloop:while(flightsIter.hasNext())
				{
					
					if(nt > Integer.parseInt(Propertymap.get("Flight_Iterations")))
						break whileloop;
					
					
					WebElement flight = flightsIter.next();
					UIFlightItineraryCC flightItinearary = new UIFlightItineraryCC();
					
					
					ArrayList<WebElement> Topinner1 = (ArrayList<WebElement>) flight.findElements(By.tagName("tr"));
					ArrayList<WebElement> Topinner1ele = (ArrayList<WebElement>) Topinner1.get(0).findElements(By.tagName("th"));
					
					String currCode 	= "";
					currCode			= Topinner1ele.get(0).findElement(By.id("rate_"+counter+"")).getText().trim().split(" ")[0];
					flightItinearary.setCurrencyCode(currCode);
					
					String cost			= "";
					cost				= Topinner1ele.get(0).findElement(By.id("rate_"+counter+"")).getText().trim().split(" ")[1];
					flightItinearary.setCost(cost);
					
					String departing	= "";
					departing			= Topinner1ele.get(1).getText().trim().replace("Departing", "").trim();
					flightItinearary.setDeparting(departing);
					
					ArrayList<WebElement> trlist		= (ArrayList<WebElement>)Topinner1.get(1).findElements(By.tagName("tr"));
					ArrayList<WebElement> trlistrowbold	= new ArrayList<WebElement>();
					ArrayList<WebElement> trlistrowalt	= new ArrayList<WebElement>();
					ArrayList<WebElement> moreLinkTR	= new ArrayList<WebElement>();
					
					if(trlist.size() == 7)
					{
						trlistrowbold.add(trlist.get(0));
						trlistrowbold.add(trlist.get(3));
						
						trlistrowalt.add(trlist.get(1));
						trlistrowalt.add(trlist.get(2));
						trlistrowalt.add(trlist.get(4));
						trlistrowalt.add(trlist.get(5));
						
						moreLinkTR.add(trlist.get(6));
					}
					else if(trlist.size() == 4)
					{
						trlistrowbold.add(trlist.get(0));
						
						trlistrowalt.add(trlist.get(1));
						trlistrowalt.add(trlist.get(2));
						
						moreLinkTR.add(trlist.get(3));
					}
					
					ArrayList<WebElement> trlistrowboldLeavetop = (ArrayList<WebElement>) trlistrowbold.get(0).findElements(By.tagName("td"));
					
					String leaving		= "";
					leaving				= trlistrowboldLeavetop.get(0).getText().trim();
					flightItinearary.setLeaving(leaving);	
					
					String leaveFlight	= "";
					leaveFlight			= trlistrowboldLeavetop.get(1).getText().trim();
					flightItinearary.setLeaveFlight(leaveFlight);
					
					String leavDur	= "";
					leavDur			= trlistrowboldLeavetop.get(2).getText().trim();
					flightItinearary.setLeaveduration(leavDur);
					
					ArrayList<WebElement> trlistrowboldReturntop = null;
					ArrayList<WebElement> trlistrowaltLeave = (ArrayList<WebElement>) trlistrowalt.get(0).findElements(By.tagName("td"));
					
					String LDTime	= "";
					LDTime			= trlistrowaltLeave.get(1).getText().trim().split("\\n")[0];
					flightItinearary.setLeaveDepartTime(LDTime);
					
					String LATime	= "";
					LATime			= trlistrowaltLeave.get(1).getText().trim().split("\\n")[1];
					flightItinearary.setLeaveArriveTime(LATime);
					
					String LDFrom	= "";
					LDFrom			= trlistrowaltLeave.get(2).getText().trim().split("\\n")[0];
					flightItinearary.setLeaveDepartFrom(LDFrom);
					
					String LATo		= "";
					LATo			= trlistrowaltLeave.get(2).getText().trim().split("\\n")[1];
					flightItinearary.setLeaveArriveTo(LATo);
					
					String DFLeeave	= "";
					DFLeeave		= trlistrowaltLeave.get(3).getText().trim();
					flightItinearary.setDirectFlightLeave(DFLeeave);
					
					String cabin	= "";
					cabin			= trlistrowalt.get(1).findElements(By.tagName("td")).get(0).getText();
					flightItinearary.setCabintype(cabin);
					
					ArrayList<WebElement> trlistrowaltReturn = null;
					if(searchObject.getTriptype().equals("Round Trip"))
					{
						twoway = true;
						trlistrowboldReturntop = (ArrayList<WebElement>) trlistrowbold.get(1).findElements(By.tagName("td"));
						
						String returning	= "";
						returning			= trlistrowboldReturntop.get(0).getText().trim().replace("Return", "").trim();
						flightItinearary.setReturning(returning);	
						
						String returnFlight = "";
						returnFlight		= trlistrowboldReturntop.get(1).getText().trim();
						flightItinearary.setReturnFlight(returnFlight);
						
						String returDur		= "";
						returDur			= trlistrowboldReturntop.get(2).getText().trim();
						flightItinearary.setReturnduration(returDur);
						
						trlistrowaltReturn	= (ArrayList<WebElement>) trlistrowalt.get(2).findElements(By.tagName("td"));
						
						String returnDTime	= "";
						returnDTime			= trlistrowaltReturn.get(1).getText().trim().split("\\n")[0];
						flightItinearary.setReturnDepartTime(returnDTime);
						
						String returnATime	= "";
						returnATime			= trlistrowaltReturn.get(1).getText().trim().split("\\n")[1];
						flightItinearary.setReturnArrivalTime(returnATime);
						
						String returnDFrom	= "";
						returnDFrom			= trlistrowaltReturn.get(2).getText().trim().split("\\n")[0];
						flightItinearary.setReturnDepartFrom(returnDFrom);
						
						String returnATo	= "";
						returnATo			= trlistrowaltReturn.get(2).getText().trim().split("\\n")[1];
						flightItinearary.setReturnArrivalTo(returnATo);
						
						String returnRFlight= "";
						returnRFlight		= trlistrowaltReturn.get(3).getText().trim();
						flightItinearary.setReturnFlight(returnRFlight);
						
						String retCabin		= "";
						returnRFlight		= trlistrowalt.get(3).findElements(By.tagName("td")).get(0).getText();
						flightItinearary.setRetCabin(retCabin);
						
					}
					
					
					moreLinkTR.get(0).findElement(By.id("info_"+counter+"")).click();
					
					WebDriverWait wait = new WebDriverWait(driver, 60);
					try
					{
						wait.until(ExpectedConditions.presenceOfElementLocated(By.id("more_info_flights_WJ_20")));
						Thread.sleep(2000);
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
					
					if(driver.findElement(By.id("more_info_flights_WJ_20")).isDisplayed())
					{
						driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	
						WebElement OutFlights = driver.findElement(By.className("out_bound"));
						ArrayList<WebElement> outboundflight = new ArrayList<WebElement>(OutFlights.findElements(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.out_bound div.lineheight25.margin10")));
						
						WebElement InFlights = null;
						ArrayList<WebElement> inboundflight = null;
						
						if(twoway)
						{
							InFlights = driver.findElement(By.className("in_bound"));
							inboundflight = new ArrayList<WebElement>(InFlights.findElements(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.in_bound div.lineheight25.margin10")));
						}
						
						/*OUTBOUND FLIGHT LOOP*/
						Outbound Outobj = new Outbound();
						ArrayList<Flight> OBflightlist = new ArrayList<Flight>();
						for(int i = 0; i<outboundflight.size() - 1; i++)
						{
							try
							{
								driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
								
								Flight obj = new Flight();
								
								String depDate	= "";
								depDate			= outboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.out_bound div.lineheight25.margin10 div.flight_info_cl_01.fleft")).getText();
								obj.setDepartureDate(depDate);
								
								String arrDate	= "";
								arrDate			= outboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.out_bound div.lineheight25.margin10 div.flight_info_cl_01.fleft")).getText();
								obj.setArrivalDate(arrDate);
								
								String portid = "";
								portid = outboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.out_bound div.lineheight25.margin10 div.flight_info_cl_02.fleft")).getText();
								String depart = portid.split("\\n")[0];
								obj.setDeparture_port(depart.split(" ")[0]);
								String arrive = portid.split("\\n")[1];
								obj.setArrival_port(arrive.split(" ")[0]);
								
								String Departportid = "";
								Departportid		= portid.split("\\n")[0].split("[()]")[1];
								obj.setDepartureLocationCode(Departportid);
								
								String Arriveportid = "";
								Arriveportid = portid.split("\\n")[1].split("[()]")[1];
								obj.setArrivalLocationCode(Arriveportid);
								
								String depTime		= "";
								depTime				= outboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.out_bound div.lineheight25.margin10 div.flight_info_cl_03.fleft")).getText().split("\\n")[0];
								obj.setDepartureTime(depTime);
								
								String arrTime		= "";
								arrTime				= outboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.out_bound div.lineheight25.margin10 div.flight_info_cl_03.fleft")).getText().split("\\n")[1];
								obj.setArrivalTime(arrTime);							
								
								String marketAirLine = "";
								marketAirLine		 = outboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.out_bound div.lineheight25.margin10 div.flight_info_cl_05.fleft")).getText();
								obj.setMarketingAirline(marketAirLine);
								
								String flightNo		= "";
								flightNo			= outboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.out_bound div.lineheight25.margin10 div.flight_info_cl_05.fleft")).getText();
								obj.setFlightNo(flightNo);
								
								String Cabin		= "";
								Cabin				= outboundflight.get(outboundflight.size()-1).getText();
								obj.setCabintype(Cabin);
								
								OBflightlist.add(obj);

							}
							
							catch(Exception e)
							{
								e.printStackTrace();
								TakesScreenshot screen = (TakesScreenshot)driver;
								FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/"+System.currentTimeMillis()/1000+".jpg"));
							}
						}
						
						Outobj.setOutBflightlist(OBflightlist);
						flightItinearary.setOutbound(Outobj);
						
						
						if(twoway)
						{
							/*INBOUND FLIGHT LOOP*/
							Inbound Inobj = new Inbound();
							ArrayList<Flight> IBflightlist = new ArrayList<Flight>();
							for(int i = 0; i<inboundflight.size() - 1; i++)
							{
								try
								{
									driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
									
									Flight obj = new Flight();
									
									String depDate	= "";
									depDate			= inboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.in_bound div.lineheight25.margin10 div.flight_info_cl_01.fleft")).getText();
									obj.setDepartureDate(depDate);
									
									String arrDate	= "";
									arrDate			= inboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.in_bound div.lineheight25.margin10 div.flight_info_cl_01.fleft")).getText(); 
									obj.setArrivalDate(arrDate);
									
									String portid = "";
									portid = inboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.in_bound div.lineheight25.margin10 div.flight_info_cl_02.fleft")).getText();
									String depart = portid.split("\\n")[0];
									obj.setDeparture_port(depart.split(" ")[0]);
									String arrive = portid.split("\\n")[1];
									obj.setArrival_port(arrive.split(" ")[0]);
									String Departportid = portid.split("\\n")[0].split("[()]")[1];
									obj.setDepartureLocationCode(Departportid);
									String Arriveportid = portid.split("\\n")[1].split("[()]")[1];
									obj.setArrivalLocationCode(Arriveportid);
									
									String depTime	= "";
									depTime			= inboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.in_bound div.lineheight25.margin10 div.flight_info_cl_03.fleft")).getText().split("\\n")[0];
									obj.setDepartureTime(depTime);
									
									String arrTime	= "";
									arrTime			= inboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.in_bound div.lineheight25.margin10 div.flight_info_cl_03.fleft")).getText().split("\\n")[1];
									obj.setArrivalTime(arrTime);							
									
									String marketALine	= "";
									marketALine			=  inboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.in_bound div.lineheight25.margin10 div.flight_info_cl_05.fleft")).getText();
									obj.setMarketingAirline(marketALine);
									
									String flightNo		= "";
									flightNo			= inboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.in_bound div.lineheight25.margin10 div.flight_info_cl_05.fleft")).getText();
									obj.setFlightNo(flightNo);
									
									String Cabin		= "";
									Cabin				= inboundflight.get(inboundflight.size()-1).getText();
									obj.setCabintype(Cabin);
									
									IBflightlist.add(obj);
								}
								catch(Exception e)
								{
									e.printStackTrace();
									TakesScreenshot screen = (TakesScreenshot)driver;
									FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/"+System.currentTimeMillis()/1000+".jpg"));
								}
							}
							
							Inobj.setInBflightlist(IBflightlist);
							flightItinearary.setInbound(Inobj);
						}
						
					}
					
					driver.findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.close_modal_window")).click();
					Resultlist.add(flightItinearary);
					nt++;
					counter++;
				}
				
			}

			catch(Exception e)
			{
				e.printStackTrace();
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/"+System.currentTimeMillis()/1000+".jpg"));
			}
				
			((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
			clickpattern++;		
			count++; 
		}

		clickpattern = 1;
		((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
			
		return Resultlist;
	}
	
	public CartFlightCC getCCCart(WebDriver driver, String way) throws WebDriverException, IOException
	{
		driver.switchTo().defaultContent();
		driver.switchTo().frame("live_message_frame");
		boolean twoway = false;
		CartFlightCC cart = new CartFlightCC();
		direction = way;
		if(direction.equals("Round Trip"))
		{
			twoway = true;
		}
		
		try
		{
			try {
				driver.findElement(By.id("full_details")).click();
				driver.switchTo().frame("productdetailsframe");
				
				WebElement alloutbound = driver.findElement(By.id("table_flight_outbound_info"));
				String outbounddetails = alloutbound.getText();
				
				String[] outboundArray = outbounddetails.split("\\n");
				ArrayList<String> sets = new ArrayList<String>();
				ArrayList<Flight> outflightlist = new ArrayList<Flight>(); 
				
				for(int y=0; y<outboundArray.length; y++)
				{
					if(outboundArray[y].startsWith("Depart") && outboundArray[(y+1)].startsWith("Arrive"))
					{
						String h = outboundArray[y].concat("#").concat(outboundArray[y+1]);
						sets.add(h);
					}
				}
				
				Outbound outbound = new Outbound();
				for(int y=0; y<sets.size(); y++)
				{
					Flight flight = new Flight();
					String Sflight = sets.get(y);
					String depart = Sflight.split("#")[0];
					String arrive = Sflight.split("#")[1];
					
					String[] departArray = depart.split(" ");
					
					flight.setDepartureDate(departArray[1]);
					flight.setDepartureTime(departArray[2]);
					
					flight.setFlightNo(depart.split("[()]")[3]);
					flight.setDepartureLocationCode(depart.split("[()]")[1]);
					
					String[] arriveArray = arrive.split(" ");
					flight.setArrivalDate(arriveArray[2]);
					flight.setArrivalTime(arriveArray[3]);
					
					flight.setArrivalLocationCode(arrive.split("[()]")[1]);
					
					outflightlist.add(flight);
					
				}
				outbound.setOutBflightlist(outflightlist);
				cart.setOutbound(outbound);
				
			} catch (Exception e) {
				System.out.println("Cart outbound detail extraction fails anna..!");
				e.printStackTrace();
			}
			
			
			try {
				
				WebElement allinbound = null;
				if(twoway)
				{
					Inbound inbound = new Inbound();
					allinbound = driver.findElement(By.id("table_flight_inbound_info"));
					String inbounddetails = allinbound.getText();
					
					String[] inboundArray = inbounddetails.split("\\n");
					ArrayList<String> insets = new ArrayList<String>();
					ArrayList<Flight> inflightlist = new ArrayList<Flight>(); 
					
					for(int y=0; y<inboundArray.length; y++)
					{
						if(inboundArray[y].startsWith("Depart") && inboundArray[(y+1)].startsWith("Arrive"))
						{
							String h = inboundArray[y].concat("#").concat(inboundArray[y+1]);
							insets.add(h);
						}
					}
					
					for(int y=0; y<insets.size(); y++)
					{
						Flight flight = new Flight();
						String Sflight = insets.get(y);
						String depart = Sflight.split("#")[0];
						String arrive = Sflight.split("#")[1];
						
						String[] departArray = depart.split(" ");
						
						flight.setDepartureDate(departArray[1]);
						flight.setDepartureTime(departArray[2]);
						
						flight.setFlightNo(depart.split("[()]")[3]);
						flight.setDepartureLocationCode(depart.split("[()]")[1]);
						
						String[] arriveArray = arrive.split(" ");
						flight.setArrivalDate(arriveArray[2]);
						flight.setArrivalTime(arriveArray[3]);
						
						flight.setArrivalLocationCode(arrive.split("[()]")[1]);
						
						inflightlist.add(flight);
					}
					
					inbound.setInBflightlist(inflightlist);
					cart.setInbound(inbound);
				}
			} catch (Exception e) {
				System.out.println("Inbound Cart details extraction fails");
				e.printStackTrace();
			}
			
			try {
				WebElement ratetable = driver.findElement(By.id("booking_rate_details_table"));
				ArrayList<WebElement> rateTRtags = new ArrayList<WebElement> (ratetable.findElements(By.tagName("tr")) );
				
				int passengercount = rateTRtags.size() - 6;
				ArrayList<RatesperPassenger> ratelist = new ArrayList<RatesperPassenger>();
				for(int h=2; h<(2+passengercount); h++)
				{
					RatesperPassenger rate = new RatesperPassenger();
					ArrayList<WebElement> tdInrateTR = new ArrayList<WebElement> (rateTRtags.get(h).findElements(By.tagName("td")) );
					
					if(tdInrateTR.get(0).getText().equalsIgnoreCase("Adult"))
					{
						rate.setPassengertype("ADT");
						rate.setRateperpsngr(tdInrateTR.get(1).getText());
						rate.setNoofpassengers(tdInrateTR.get(2).getText());
						rate.setTotal(tdInrateTR.get(3).getText());
					}
					if(tdInrateTR.get(0).getText().equalsIgnoreCase("Children"))
					{
						rate.setPassengertype("CHD");
						rate.setRateperpsngr(tdInrateTR.get(1).getText());
						rate.setNoofpassengers(tdInrateTR.get(2).getText());
						rate.setTotal(tdInrateTR.get(3).getText());
					}
					if(tdInrateTR.get(0).getText().equalsIgnoreCase("Infant"))
					{
						rate.setPassengertype("INF");
						rate.setRateperpsngr(tdInrateTR.get(1).getText());
						rate.setNoofpassengers(tdInrateTR.get(2).getText());
						rate.setTotal(tdInrateTR.get(3).getText());
					}
					
					ratelist.add(rate);
				}
				cart.setRatelist(ratelist);
				
				int forward = 2 + passengercount;
				
				cart.setTotalBeforeTax(rateTRtags.get(forward).findElement(By.id("td_rate_details_total_rate_before_taxes_1")).getText());
				cart.setTaxes(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_taxes_1")).getText());
				cart.setBookingfee(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_booking_fee_1")).getText());
				cart.setTotalInclusiveTax(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_total_cost_1")).getText());
				
			} catch (Exception e) {
				System.out.println("Passenger and rate details in cart extraction faild anna..!");
				e.printStackTrace();
			}
			
			
			try
			{
				WebElement totfareTable = driver.findElement(By.id("total_transfer_info_table"));
				String[] totfareArray = totfareTable.getText().split("\\n");
				cart.setCurrencyCode(totfareArray[0].split("[()]")[1]);
				cart.setTotalGrossBookingValue(totfareArray[1].split(":")[1].trim());
				cart.setTaxesandOther(totfareArray[3]);
				cart.setFinalvalue(totfareArray[5]);
				cart.setAmountprocessnowFinal(totfareArray[7]);
				cart.setAmountbyairlineFinal(totfareArray[8].split(" ")[5]);
				
				try {
					WebElement cancellation = driver.findElement(By.id("table_cancellation_policies"));
					ArrayList<WebElement> cancellationTRtags = new ArrayList<WebElement>(cancellation.findElements(By.tagName("tr")));
					String flightdeadline = cancellationTRtags.get(1).getText().split(": ")[1].trim();
					cart.setDeadline(flightdeadline);
					String packagecancel = cancellationTRtags.get((cancellationTRtags.size() - 1)).getText().split(":")[1];
					cart.setPackageDeadline(packagecancel);
				} catch (Exception e) {
					System.out.println("Cancellation details extraction in cart fails anna..!");
					e.printStackTrace();
				}
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/"+System.currentTimeMillis()/1000+".jpg"));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			TakesScreenshot screen = (TakesScreenshot)driver;
			FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/"+System.currentTimeMillis()/1000+".jpg"));
		}
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame("live_message_frame");
		try
		{
			ArrayList<WebElement> a = new ArrayList<WebElement>(driver.findElements(By.className("ui-dialog-titlebar-close")));
			a.get(0).findElement(By.className("ui-icon")).click();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			ArrayList<WebElement> a = new ArrayList<WebElement>();
			try {
				a = new ArrayList<WebElement>(driver.findElements(By.className("ui-dialog-titlebar-close")));
			} catch (Exception e2) {
				e.printStackTrace();
			}
			
			try {
				driver.findElement(By.className("ui-icon")).click();
			} catch (Exception e2) {
				for(int y=0; y<a.size(); y++)
				{
					try {
						if(a.get(y).findElement(By.className("ui-icon")).getText().contains("close"))
						{
							a.get(y).findElement(By.className("ui-icon")).click();
						}
					} catch (Exception e3) {
						e.printStackTrace();
					}
				}
				
				try {
					driver.findElement(By.xpath("html/body/div[10]/div[1]/a/span")).click();
				} catch (Exception e3) {
					e.printStackTrace();
				}				
				
			}
			
		}
		
		return cart;
	}
	
	public WebDriver Fill_Reservation_DetailsQ(WebDriver driver, ReservationInfo fillObj, SearchObject sobj)
	{
		WebDriverWait wait = new WebDriverWait(driver, 10,5);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("live_message_frame");
		try {
			if(sobj.getPaymentMode().equalsIgnoreCase("Pay Offline"))
			{
				driver.findElement(By.id("user_id")).sendKeys(Propertymap.get("portal.username"));
				driver.findElement(By.id("user_password")).sendKeys(Propertymap.get("portal.password"));
				driver.findElement(By.id("activate_mg")).click();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cusFName")));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			wait.until(ExpectedConditions.elementToBeClickable(By.id("cancellation")));
			driver.findElement(By.id("cancellation")).click();
		} catch (Exception e) {
			System.out.println("cancellation check box click fail anna..!");
			e.printStackTrace();
		}
		
		//driver.switchTo().frame("live_message_frame");
		int adults		= fillObj.getAdult().size();
		int children	= fillObj.getChildren().size();
		int infants		= fillObj.getInfant().size();
		try
		{
			try {
				Traveler main = fillObj.getMaincustomer();
				driver.findElement(By.id("cusFName")).clear();
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cusFName")));
				driver.findElement(By.id("cusFName")).sendKeys(main.getGivenName());
				driver.findElement(By.id("cusLName")).clear();
				driver.findElement(By.id("cusLName")).sendKeys(main.getSurname());
				driver.findElement(By.id("cusAdd1")).clear();
				driver.findElement(By.id("cusAdd1")).sendKeys(main.getAddress().getAddressStreetNo());
				driver.findElement(By.id("cusAdd2")).clear();
				driver.findElement(By.id("cusAdd2")).sendKeys(main.getAddress().getAddressStreetNo()+"123");
				driver.findElement(By.id("cusCity")).clear();
				driver.findElement(By.id("cusCity")).sendKeys(main.getAddress().getAddressCity());
				new Select(driver.findElement(By.id("cusCountry"))).selectByVisibleText(main.getAddress().getAddressCountry());
				driver.findElement(By.id("cusareacodetext")).clear();
				driver.findElement(By.id("cusareacodetext")).sendKeys("11");
				driver.findElement(By.id("cusPhonetext")).clear();
				driver.findElement(By.id("cusPhonetext")).sendKeys(main.getPhoneNumber());
				//driver.findElement(By.id("cusmobnos")).clear();
				//driver.findElement(By.id("cusmobnos")).sendKeys("88");
				//driver.findElement(By.id("cusmobnoe")).clear();
				//driver.findElement(By.id("cusmobnoe")).sendKeys(main.getPhoneNumber());
				driver.findElement(By.id("cusEmail")).clear();
				driver.findElement(By.id("cusEmail")).sendKeys(main.getEmail());
			} catch (Exception e) {
				System.out.println("Filling main traveller faild anna..!");
				e.printStackTrace();
			}
			
			if(true)
			{
				try {
					for(int a = 0; a<adults; a++)
					{
						try {
							new Select(driver.findElement(By.id("tfpi_adult_title_"+a+""))).selectByVisibleText(fillObj.getAdult().get(a).getNamePrefixTitle());
						} catch (Exception e) {
							try {
								new Select(driver.findElement(By.id("tfpi_adult_title_"+a+""))).selectByVisibleText(fillObj.getAdult().get(a).getNamePrefixTitle());
							} catch (Exception e2) {
								e.printStackTrace();
							}
						}
						
						driver.findElement(By.id("tfpi_adult_first_name_"+a+"")).clear();
						driver.findElement(By.id("tfpi_adult_first_name_"+a+"")).sendKeys(fillObj.getAdult().get(a).getGivenName());
						driver.findElement(By.id("tfpi_adult_last_name_"+a+"")).clear();
						driver.findElement(By.id("tfpi_adult_last_name_"+a+"")).sendKeys(fillObj.getAdult().get(a).getSurname());
						driver.findElement(By.id("ftpi_adult_contact_information_"+a+"")).clear();
						driver.findElement(By.id("ftpi_adult_contact_information_"+a+"")).sendKeys(fillObj.getAdult().get(a).getPhoneNumber());
						driver.findElement(By.id("ftpi_adult_passport_number_"+a+"")).clear();
						driver.findElement(By.id("ftpi_adult_passport_number_"+a+"")).sendKeys(fillObj.getAdult().get(a).getPassportNo());
		
						((JavascriptExecutor)driver).executeScript("$('#adultPassportExpDate_Air_"+a+"').val('"+fillObj.getAdult().get(a).getPassportExpDate().trim()+"');");
						
						((JavascriptExecutor)driver).executeScript("$('#adultPassportIssueDate_Air_"+a+"').val('"+fillObj.getAdult().get(a).getPassprtIssuDate().trim()+"');");
						
						((JavascriptExecutor)driver).executeScript("$('#adultDateOfBirth_Air_"+a+"').val('"+fillObj.getAdult().get(a).getBirthDay().trim()+"');");
						
						new Select(driver.findElement(By.id("ftpi_adult_sex_"+a+""))).selectByValue(fillObj.getAdult().get(a).getGender());
						new Select(driver.findElement(By.id("ftpi_adult_nationality_"+a+""))).selectByVisibleText(fillObj.getAdult().get(a).getNationality());
						new Select(driver.findElement(By.id("ftpi_adult_pic_country_"+a+""))).selectByVisibleText(fillObj.getAdult().get(a).getPassportIssuCountry());
					}
				} catch (Exception e) {
					System.out.println("Filling adult details faild anna..!");
					e.printStackTrace();
				}
				
				
				try {
					for(int c=0; c<children; c++)
					{
						new Select(driver.findElement(By.id("tfpi_child_title_"+c+""))).selectByValue(fillObj.getChildren().get(c).getNamePrefixTitle());
						driver.findElement(By.id("tfpi_child_first_name_"+c+"")).clear();
						driver.findElement(By.id("tfpi_child_first_name_"+c+"")).sendKeys(fillObj.getChildren().get(c).getGivenName());
						driver.findElement(By.id("tfpi_child_last_name_"+c+"")).clear();
						driver.findElement(By.id("tfpi_child_last_name_"+c+"")).sendKeys(fillObj.getChildren().get(c).getSurname());
						driver.findElement(By.id("tfpi_child_contact_information_"+c+"")).clear();
						driver.findElement(By.id("tfpi_child_contact_information_"+c+"")).sendKeys(fillObj.getChildren().get(c).getPhoneNumber());
						driver.findElement(By.id("tfpi_child_passport_number_"+c+"")).clear();
						driver.findElement(By.id("tfpi_child_passport_number_"+c+"")).sendKeys(fillObj.getChildren().get(c).getPassportNo());
						
						((JavascriptExecutor)driver).executeScript("$('#childPassportExpDate_Air_"+c+"').val('"+fillObj.getChildren().get(c).getPassportExpDate().trim()+"');");
						
						((JavascriptExecutor)driver).executeScript("$('#childPassportIssueDate_Air_"+c+"').val('"+fillObj.getChildren().get(c).getPassprtIssuDate().trim()+"');");
						
						((JavascriptExecutor)driver).executeScript("$('#childDateOfBirth_Air_"+c+"').val('"+fillObj.getChildren().get(c).getBirthDay().trim()+"');");
						
						new Select(driver.findElement(By.id("tfpi_child_sex_"+c+""))).selectByVisibleText(fillObj.getChildren().get(c).getGender());
						new Select(driver.findElement(By.id("tfpi_child_nationality_"+c+""))).selectByVisibleText(fillObj.getChildren().get(c).getNationality());
						new Select(driver.findElement(By.id("tfpi_child_pic_country_"+c+""))).selectByVisibleText(fillObj.getChildren().get(c).getPassportIssuCountry());
					}
				} catch (Exception e) {
					System.out.println("Filling children details faild anna..!");
					e.printStackTrace();
				}
				
				
				try {
					for(int i = 0; i<infants; i++)
					{
						new Select(driver.findElement(By.id("tfpi_infant_title_"+i+""))).selectByVisibleText(fillObj.getInfant().get(i).getNamePrefixTitle());
						driver.findElement(By.id("tfpi_infant_first_name_"+i+"")).clear();
						driver.findElement(By.id("tfpi_infant_first_name_"+i+"")).sendKeys(fillObj.getInfant().get(i).getGivenName());
						driver.findElement(By.id("tfpi_infant_last_name_"+i+"")).clear();
						driver.findElement(By.id("tfpi_infant_last_name_"+i+"")).sendKeys(fillObj.getInfant().get(i).getSurname());
						
						((JavascriptExecutor)driver).executeScript("$('#infantBirthDate_Air_"+i+"').val('"+fillObj.getInfant().get(i).getBirthDay().trim()+"');");
					
						new Select(driver.findElement(By.id("tfpi_infant_sex_"+i+""))).selectByVisibleText(fillObj.getInfant().get(i).getGender());
						driver.findElement(By.id("tfpi_infant_passport_number_"+i+"")).sendKeys(fillObj.getInfant().get(i).getPassportNo());
						//new Select(driver.findElement(By.id("tfpi_infant_passport_number_"+i+""))).selectByVisibleText(fillObj.getInfant().get(i).getNationality());
					}
				} catch (Exception e2) {
					System.out.println("Filling child details fail anna..!");
					e2.printStackTrace();
				}
			}
			
			/*try {
				if(sobj.isQuotation())
				{
					driver.findElement(By.id("submit_quote")).click();
				}
			} catch (Exception e) {
				System.out.println("Fail clicking submit quote anna ..!");
				e.printStackTrace();
			}*/
			
			try {
				if(/*!sobj.isQuotation()*/true)
				{
					if(sobj.getPaymentMode().equalsIgnoreCase("Pay Online"))
					{
						try {
							driver.findElement(By.id("pay_online")).click();
						} catch (Exception e) {
							System.out.println("click pay online fail anna..!");
							e.printStackTrace();
						}
						
					}
					else if(sobj.getPaymentMode().equalsIgnoreCase("Pay Offline"))
					{
						try {
							driver.findElement(By.id("pay_offline")).click();
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						try {
							wait.until(ExpectedConditions.presenceOfElementLocated(By.id("paymentGuaranteed")));
							driver.findElement(By.id("paymentGuaranteed")).click();
							new Select(driver.findElement(By.id("payment_method_1"))).selectByValue("CS");
							wait.until(ExpectedConditions.presenceOfElementLocated(By.id("payment_reference")));
							driver.findElement(By.id("payment_reference")).sendKeys("Cash1123");
						
						} catch (Exception e) {
							System.out.println("payment gurantee click faild anna..!");
							e.printStackTrace();
						}
					}
					
					
					
					/*try {
						driver.findElement(By.className("required")).click();
					} catch (Exception e) {
						System.out.println("cancellation check box click fail anna..!");
						e.printStackTrace();
					}*/
					
					try {
						driver.findElement(By.id("customer_mail_send_yes")).click();
						driver.findElement(By.id("voucher_mail_send_yes")).click();
						driver.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div.wrapper div.content_body_wrapper.group div.content_full_col_wrapper div.full_column div.rounded_widget table#table_send_mail.table_send_mail tbody tr td.amo_width_6 input#supplier_mail_send_yes")).click();
					} catch (Exception e) {
						System.out.println("mail selection faild anna..!!");
						e.printStackTrace();
					}
					
					try {
						driver.findElement(By.id("continue_submit")).click();
					} catch (Exception e) {
						//System.out.println("Clicking continue botton faild");
						e.printStackTrace();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}	
		
		
		return driver;
	}
	
	
}
