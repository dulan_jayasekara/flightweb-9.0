package system.main;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import Flight.Cancellation.CancellationReport;
import Flight.Cancellation.CancellationScreen;
import system.classes.PopupMessage;
import system.classes.ReservationInfo;
import system.classes.TOperator;
import system.classes.UIPaymentPage;
import Flight.Fare.FareRequest;
import Flight.Fare.FareRequestReader;
import Flight.Fare.FareResponse;
import Flight.Fare.FareResponseReader;
import Flight.Price.PriceResponse;
import Flight.Price.PriceResponseReader;
import Flight.Reports.QuotationMail;
import Flight.Reservation.ReservationRequest;
import Flight.Reservation.ReservationResponse;
import Flight.Reservation.ResvRequestReader;
import Flight.Reservation.ResvResponseReader;
import Results.Validate.Validate;
import Results.Validate.ValidateFilters;
import Results.Validate.ValidateReports;
import system.enumtypes.*;
import com.utilities.ExchangeRateUpdater;
import com.utilities.FillReservationDetails;
import com.utilities.Repository;
import com.utilities.Screenshot;
import com.utilities.ScreenshotPathSingleton;

import system.supportParents.*;
import system.classes.*;
import system.pages.QuotationBooking;
import system.pages.QuotationPage;
import system.pages.WebConfirmationPage;
import system.pages.WebPaymentPage;
import system.pages.WebReservationPage;
import system.pages.WebResultsPage;

public class Web {

	ArrayList<UIFlightItinerary> WebResultlist = new ArrayList<UIFlightItinerary>();
	CartFlight cart = null;
	HashMap<String, String> PropertyMap = new HashMap<String, String>();

	public void go(WebDriver driver, SearchObject searchObject, AirConfig configuration, Validate validate, HashMap<String, String> propertyMap, StringBuffer ReportPrinter, int Reportcount) {

		PropertyMap = propertyMap;
		WebReservationPage webReservationPage = new WebReservationPage(propertyMap);
		WebResultsPage webResultsPage = new WebResultsPage(propertyMap);
		WebPaymentPage webPaymentPage = new WebPaymentPage(propertyMap);
		WebConfirmationPage webConfirmationPage = new WebConfirmationPage(propertyMap);

		boolean done = false;
		boolean results = false;
		String intermediateStatus = "";
		String intermediateMessage = "";
		String payAmount = "";
		String payAmountCurrency = "";
		String PageSource = "";
		ArrayList<IntermediateInfo> intermediateList = new ArrayList<IntermediateInfo>();
		@SuppressWarnings("unused")
		int m = 1;
		@SuppressWarnings("unused")
		boolean eticketIssued = false;
		TOperator instanceTO1 = new TOperator();
		TOperator instanceTO2 = new TOperator();
		Map<String, String> CurrencyMap = new HashMap<String, String>();
		@SuppressWarnings("unused")
		String scenarioCommonPath = "";
		String scenarioFailedPath = "";
		scenarioCommonPath = ScreenshotPathSingleton.getInstance().getScenarioCommonPath();
		scenarioFailedPath = ScreenshotPathSingleton.getInstance().getScenarioFailedPath();

		try {
			// SET XML URLS TO NULL
			Repository.setAll();
			IntermediateInfo intermediateinfo = new IntermediateInfo();
			// READ EXCHANGE RATE TABLE TO HASH MAP
			ExchangeRateUpdater rates = new ExchangeRateUpdater();
			try {
				CurrencyMap = rates.getExchangeRates(PropertyMap, driver);
			} catch (IOException e) {
				e.printStackTrace();
			}
			// INITIALIZE VALIDATING OBJECT
			validate = new Validate(propertyMap, driver, CurrencyMap);

			// STARTING EXECUTION OF SEARCH OBJECT LIST
			try {
				if (searchObject.getTOBooking().equalsIgnoreCase("YES")) {
					try {
						// CHECK THE LOGING TYPE
						System.out.println("=======================================");
						System.out.println("INFO -> TO BOOKING");
						boolean log = false;
						SupportMethods support = new SupportMethods(propertyMap);
						String userName = "";
						String password = "";
						try {
							if (searchObject.getToType() == system.enumtypes.TO.NetCash) {
								userName = propertyMap.get("NetCash");
								password = propertyMap.get("NetCashPass");
							} else if (searchObject.getToType() == system.enumtypes.TO.NetCreditLpoN) {
								userName = propertyMap.get("NetCreditLpoN");
								password = propertyMap.get("NetCreditLpoNPass");
							} else if (searchObject.getToType() == system.enumtypes.TO.NetCreditLpoY) {
								userName = propertyMap.get("NetCreditLpoY");
								password = propertyMap.get("NetCreditLpoYPass");
							} else if (searchObject.getToType() == system.enumtypes.TO.CommCash) {
								userName = propertyMap.get("CommCash");
								password = propertyMap.get("CommCashPass");
							} else if (searchObject.getToType() == system.enumtypes.TO.CommCreditLpoN) {
								userName = propertyMap.get("CommCreditLpoN");
								password = propertyMap.get("CommCreditLpoNPass");
							} else if (searchObject.getToType() == system.enumtypes.TO.CommCreditLpoY) {
								userName = propertyMap.get("CommCreditLpoY");
								password = propertyMap.get("CommCreditLpoYPass");
							}

							log = support.login(driver, userName, password);

						} catch (Exception e) {
							Screenshot.takeScreenshot(scenarioFailedPath + "/TO Login Failed", driver);
						}

						// LOGIN SUCCESS VALIDATION
						//System.out.println("INFO -> TO LOGIN REPORT");
						ReportPrinter.append("<span><center><p class='Hedding0'>Check TO Login</p></center></span>");
						ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
						ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>TO Login</td>" + "<td>If Login - true, If fail - false</td>");
						if (log) {
							ReportPrinter.append("<td>" + log + "</td>" + "<td class='Passed'>PASS</td></tr>");
							Reportcount++;
						} else {
							ReportPrinter.append("<td>" + log + "</td>" + "<td class='Failed'>Fail</td></tr>");
							Reportcount++;
						}
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");

						driver.switchTo().defaultContent();

						// FOR TO LOGINS READING INITIAL TO DETAILS (CREDIT BALANCE)
						if (log) {
							System.out.println("INFO -> TO DETAILS READ");
							boolean pass = false;
							try {
								pass = instanceTO1.setTO(driver);
							} catch (Exception e) {
								Screenshot.takeScreenshot(scenarioFailedPath + "/TO Home page read fail", driver);
							}

							// CLICK RESERVAION LINK
							if (pass) {
								driver.findElement(By.partialLinkText("Make Reservations")).click();
								//System.out.println("INFO -> SEARCH");
								done = webReservationPage.search(driver, searchObject);
							} else {
								System.out.println("To details extraction " + pass + "");
							}
						}
					} catch (Exception e) {
						System.out.println("Smthing wrong with the TO LOGIN dude...!!!!");
					}
					
					System.out.println("INFO -> TO BOOKING LOG END");
					System.out.println("=======================================");
					
				} else {
					// DIRECT USER ~ NOT TO
					System.out.println("=======================================");
					System.out.println("INFO -> DC BOOKING");
					driver.get(PropertyMap.get("PortalWEB.Url"));
					
					done = webReservationPage.search(driver, searchObject);
					System.out.println("INFO -> DC BOOKING LOG END");
					System.out.println("=======================================");
				}

				ReportPrinter.append("<span><center><p class='Hedding0'>Booking Engine/p></center></span>");
				ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
				System.out.println("INFO -> VALIDATING BOOKING PAGE");
				validate.validateBookingPage(webReservationPage, ReportPrinter);
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			} catch (Exception e) {
				Screenshot.takeScreenshot(scenarioFailedPath + "/Search failed", driver);
			}

			// CHECK FOR NO RESULTS MESSAGES
			SupportMethods su = new SupportMethods(propertyMap);
			PopupMessage popup1 = new PopupMessage();
			System.out.println("INFO -> POP UP MESSAGE AFTER SEARCH");
			popup1 = su.popupHandler(driver);

			if (popup1.getTitle().equals("Notification")) {
				System.out.println("CAN NOT CONTINUE");
			}

			driver.switchTo().defaultContent();
			WebDriverWait wait = new WebDriverWait(driver, 25);

			// GET THE TRACER FROM THE RESULTS PAGE
			String tracer = "null";
			try {
				System.out.println("INFO -> GET TRACER");
				results = getTracer(driver, searchObject, tracer);

				// VALIDATION OF RESULTS AVAILABILITY AND THE NOTIFICATION POPUP FOR RESULTS UNAVAILABILITY
				ReportPrinter.append("<span><center><p class='Hedding0'>Check No Results Notification Functionality</p></center></span>");
				ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
				ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>Booking Engine Search</td>" + "<td>If Results Available In Results Page - True and Notification - False</td>");
				if (!popup1.getTitle().equals("") && results) {
					ReportPrinter.append("<td>Results: " + results + " Notification: " + popup1.getTitle() + "</td>" + "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
				} else if (popup1.getTitle().equals("") && results) {
					ReportPrinter.append("<td>Results: " + results + " Notification: " + popup1.getTitle() + "</td>" + "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
				} else if (!popup1.getTitle().equals("") && !results) {
					ReportPrinter.append("<td>Results: " + results + " Notification: " + popup1.getTitle() + "</td>" + "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
				} else if (popup1.getTitle().equals("") && !results) {
					ReportPrinter.append("<td>Results: " + results + " Notification: " + popup1.getTitle() + "</td>" + "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
				}
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");

				// RESULTS AVAILABILITY TRUE OR FALSE
				ReportPrinter.append("<span><center><p class='Hedding0'>Check Results Availability</p></center></span>");
				ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
				ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>Booking Engine Search</td>" + "<td>If Results Available In Results Page - True</td>");
				if (results) {
					ReportPrinter.append("<td>" + results + "</td>" + "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
				} else {
					ReportPrinter.append("<td>" + results + "</td>" + "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
				}

				// SEARCH AGAIN VALIDATION
				if (results && searchObject.isSearchAgain()) {
					System.out.println("INFO -> SEARCH AGAIN VALIDATION");
					done = webResultsPage.searchAgain(driver, searchObject);

					// GET THE TRACER FROM THE RESULTS PAGE
					if (done) {
						System.out.println("INFO -> GET TRACER AFTER SEARCH AGAIN");
						results = getTracer(driver, searchObject, tracer);

						ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>Search Again Search</td>" + "<td>If Results Not Available In Results Page - False</td>");
						if (results) {
							ReportPrinter.append("<td>" + results + "</td>" + "<td class='Passed'>PASS</td></tr>");
							Reportcount++;
						} else {
							ReportPrinter.append("<td>" + results + "</td>" + "<td class='Failed'>Fail</td></tr>");
							Reportcount++;
						}
					}
				}
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			} catch (Exception e) {
				Screenshot.takeScreenshot(scenarioFailedPath + "/Unexpected.jpg", driver);
			}

			// LOWFARE REQUEST RESPOSE READERS
			FareRequestReader farereqreader = new FareRequestReader(propertyMap);
			FareResponseReader fareresreader = new FareResponseReader(propertyMap);

			XMLFileType ReqType = null;
			XMLFileType ResType = null;

			// REQ/RES XML LOG PREFIX
			if (!searchObject.getPreferredAirline().trim().equals("NONE")) {
				ReqType = XMLFileType.AIR1_PrefAirlineSearchRequest_;
				ResType = XMLFileType.AIR1_PrefAirlineSearchResponse_;
			} else {
				ReqType = XMLFileType.AIR1_LowFareSearchRequest_;
				ResType = XMLFileType.AIR1_LowFareSearchResponse_;
			}

			System.out.println("INFO -> REQ TYPE : "+ReqType.toString());
			System.out.println("INFO -> RES TYPE : "+ResType.toString());
			
			// VALIDATION OF NO RESULTS NOTIFICATION
			if (!results) {
				ReportPrinter.append("<span><center><p class='Hedding0'>No Results Notification Validation</p></center></span>");
				ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
				ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>Check proper implementation of No Results Notification</td>" + "<td>If Results Not Available In Results Page, Notification should be displayed</td>");

				PopupMessage popupErrorFareRes = new PopupMessage();
				try {
					System.out.println("INFO -> POP UP MESSAGE AFTER SEARCH AGAIN");
					popupErrorFareRes = su.popupHandler(driver);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (!popupErrorFareRes.getMessage().equals("")) {
					ReportPrinter.append("<td>" + popupErrorFareRes.getMessage() + "</td>" + "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
				} else {
					ReportPrinter.append("<td>" + popupErrorFareRes.getMessage() + "</td>" + "<td>Fail</td></tr>");
					Reportcount++;
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
				}
			}

			if (results) {
				// =========================================LOWFARE REQUEST XML VALIDATION==============================================
				// READ FARE REQUEST XML
				FareRequest fareRequest = new FareRequest();
				try {
					fareRequest = farereqreader.RequestReader(XMLLocateType.TRACER, searchObject.getTracer(), ReqType);
				} catch (Exception e) {
					e.printStackTrace();
				}

				// VALIDATING LOWFAREREQUEST XML
				ReportPrinter.append("<span><center><p class='Hedding0'>LowFareRequest XML Validation</p></center></span>");
				ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");

				try {
					validate.validateFareRequest(searchObject, fareRequest, ReportPrinter);
				} catch (Exception e) {

				}

				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");

				// ==============================================LOWFARE RESPONSE VALIDATION=============================================
				// READ FARE RESPONSE AND CHECK FOR SUPPLIER SENDING ERRORS
				FareResponse response = new FareResponse();
				try {
					ReportPrinter.append("<span><center><p class='Hedding0'>Validation For LowFareResponse XML Errors</p></center></span>");
					ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");

					try {
						response = fareresreader.ResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), ResType);
					} catch (Exception e) {

					}

					ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>Check for errors in Low Fare Response XML</td>" + "<td>If error occured - Error Message should be displayed, Else - Available</td>");

					if (response.getError().getErrorMessage().equals("")) {
						ReportPrinter.append("<td>Available</td>" + "<td class='Passed'>PASS</td></tr>");
						Reportcount++;
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");
					} else {
						ReportPrinter.append("<td>" + response.getError().getErrorMessage() + "</td>" + "<td class='Failed'>Fail</td></tr>");
						Reportcount++;
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");
					}
				} catch (Exception e) {
					e.printStackTrace();
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
				}

				// ==================================RESULTS WITH LOWFARE RESPONSE XML VALIDATION=================================================
				try {
					System.out.println("================================");
					System.out.println("RESULTS PAGE READING STARTED");
					done = webResultsPage.setResults(driver, searchObject);
					WebResultlist = webResultsPage.getResultlist();
					System.out.println("RESULTS PAGE READING DONE");
					System.out.println("================================");
				} catch (Exception e) {
					e.printStackTrace();
				}

				// VALIDATE FILTERS
				try {
					if (searchObject.isValidateFilters()) {
						ValidateFilters validateFilters = new ValidateFilters(PropertyMap);
						done = validateFilters.validateAllFilters(driver, ReportPrinter, Reportcount, searchObject.getTriptype()/* , WebResultlist */);
					}
				} catch (Exception e) {

				}

				// VALIDATE RESULTS PAGE WITH LOWFARE RESPONSE XML
				ReportPrinter.append("<span><center><p class='Hedding0'>Results Validation with LowFareResponse XML</p></center></span>");
				ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
				try {
					validate.validateResultsPage(webResultsPage, response, searchObject, configuration, ReportPrinter);
				} catch (Exception e) {
					e.printStackTrace();
				}

				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");

				// ============================================CART VALIDATION====================================================================

				String selection = searchObject.getSelectingFlight();
				int selectFlight = Integer.parseInt(selection);
				PopupMessage popupaddtocart = new PopupMessage();

				try {
					Thread.sleep(3000);

					// VALIDATING REMOVE CART FUNCTIONALITY
					if (searchObject.isRemovecartStatus()) {
						System.out.println("INFO -> REMOVE CART FUNCTION");
						popupaddtocart = su.addToCart(driver, WebResultlist.size(), selectFlight);
						Thread.sleep(3000);
						boolean status = su.removeFromCart(driver);
						ReportPrinter.append("<span><center><p class='Hedding0'>Remove from cart functionality test</p></center></span>");
						ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
						ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>Remove from cart functionality test</td>" + "<td>True</td>");
						if (status) {
							ReportPrinter.append("<td>" + status + "</td>" + "<td class = 'Passed'>PASS</td></tr>");
							Reportcount++;
							Thread.sleep(3000);
						} else {
							ReportPrinter.append("<td>" + status + "</td>" + "<td class = 'Failed'>Fail</td></tr>");
							Reportcount++;
						}
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");

						// ADD ITEM TO CART
						try {
							System.out.println("INFO -> ADD TO CART AFTER REMOVAL");
							popupaddtocart = su.addToCart(driver, WebResultlist.size(), selectFlight);
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
						// ADD ITEM TO CART
						System.out.println("INFO -> ADD TO CART");
						popupaddtocart = su.addToCart(driver, WebResultlist.size(), selectFlight);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				// GET SELECTING ITEM FROM RESPONSE XML
				XMLPriceItinerary XMLSelectFlight = new XMLPriceItinerary();
				try {
					System.out.println("INFO -> GET THE SELECTED FLIGHT");
					XMLSelectFlight = response.getList().get(selectFlight - 1);
				} catch (Exception e) {

				}

				// READ PRICE RESPONSE XML
				PriceResponseReader priceresreader = new PriceResponseReader(propertyMap);
				PriceResponse priceresponse1 = new PriceResponse();
				try {
					priceresponse1 = priceresreader.AmadeusPriceResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_PriceResponse_);
				} catch (Exception e) {
					e.printStackTrace();
				}

				// VALIDATE PRICE RESPONSE XML
				ReportPrinter.append("<span><center><p class='Hedding0'>Error Validation For PriceResponse XML Errors</p></center></span>");
				ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");

				ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>If error occured in Price Response XML</td>" + "<td>If error occured - Error Message should be displayed, Else - empty</td>");

				if (!priceresponse1.getError().getErrorMessage().equals("")) {
					ReportPrinter.append("<td>" + priceresponse1.getError().getErrorMessage() + "</td>" + "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
				} else {
					ReportPrinter.append("<td>" + priceresponse1.getError().getErrorMessage() + "</td>" + "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");

					// CHECK FOR PRICE CHANGE
					boolean ischange = false;
					if (priceresponse1.isAvailable()) {
						try {
							ischange = validate.Ispricechanged(priceresponse1.getPriceinfo(), XMLSelectFlight.getPricinginfo());
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					// 05-11-2014 ADD TO CART PRICE CHANGE - READ NEW PRICE RESPONSE AFTER ADD TO CART
					System.out.println("INFO -> NEW PRICE");
					XMLPriceInfo newPrice = priceresponse1.getPriceinfo();
					XMLSelectFlight.setPricinginfo(newPrice);
					XMLSelectFlight.getPricinginfo().setIschanged(ischange);

					// PRICE VALIDATION WHILE ADD TO CART
					ReportPrinter.append("<span><center><p class='Hedding0'>Price Validation While Add To Cart</p></center></span>");
					ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
					ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>If Prices have changed actual must give an alert message</td>" + "<td>Error/Notification</td>");

					if (!popupaddtocart.getTitle().equals("") && ischange) {
						ReportPrinter.append("<td>Message : " + popupaddtocart.getMessage() + " Price Changed : " + ischange + "</td>" + "<td class='Passed'>PASS</td></tr>");
						Reportcount++;
					} else if (popupaddtocart.getTitle().equals("Error") && !ischange) {
						ReportPrinter.append("<td>Message : " + popupaddtocart.getMessage() + " Price Changed : " + ischange + "</td>" + "<td class='Failed'>Fail</td></tr>");
						Reportcount++;
					} else if (ischange) {
						if (popupaddtocart.getTitle().equals("")) {
							ReportPrinter.append("<td>No popup but prices have changed!</td>" + "<td class='Failed'>Fail</td></tr>");
							Reportcount++;
						}
					} else if (!ischange == false) {
						if (!popupaddtocart.getTitle().equals("")) {
							ReportPrinter.append("<td>Popup displayed but prices are same!</td>" + "<td class='Failed'>Fail</td></tr>");
							Reportcount++;
						}
					} else {
						ReportPrinter.append("<td>Prices have not changed</td>" + "<td class = 'Passed'>PASS</td></tr>");
						Reportcount++;
					}

					

					// SET AND GET CART
					try {
						System.out.println("INFO -> READ CART");
						done = webResultsPage.setWebCart(driver);
						this.cart = webResultsPage.getCart();
						XMLSelectFlight = validate.getNewPrice(propertyMap.get("Portal_Currency_Code"), priceresponse1.getPriceinfo(), XMLSelectFlight, searchObject, configuration);
					} catch (Exception e) {
						e.printStackTrace();
					}

					// VALIDATE CART DETAILS
					ReportPrinter.append("<span><center><p class='Hedding0'>Cart Item Validation</p></center></span>");
					ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");

					try {
						ReportPrinter = validate.validateCart(cart, priceresponse1.getPriceinfo(), XMLSelectFlight, searchObject, configuration, ReportPrinter);
					} catch (Exception e) {

					}
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br>");

					// ======================================================CHECKOUT=============================================================
					try {
						((JavascriptExecutor) driver).executeScript("JavaScript:loadPaymentPage();");
					} catch (Exception e) {
						e.printStackTrace();
					}

					// ERROR VALIDATION WHEN CHECKOUT FROM CART
					ReportPrinter.append("<span><center><p class='Hedding0'>Unexpected Error Validation While Checkout Cart Item</p></center></span>");
					ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
					ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>Check blocker errors that prevents checkout</td>" + "<td>If error occurred - Error Message should be displayed</td>");

					PopupMessage popup2 = new PopupMessage();
					
					// HANDLE DISCOUNT APPLICATION MESSAGE ALERT
					try {
						if (!driver.findElement(By.id("status_F_AIR1")).isDisplayed() || !driver.findElement(By.id("customername")).isDisplayed() || !driver.findElement(By.id("dialog-alert-message-WJ_19")).isDisplayed()) {
							if (driver.findElement(By.id("dialog-alert-message-WJ_19")).isDisplayed()) {
								System.out.println("INFO -> DISCOUNT APPLIED MESSAGE - PAYMENT PAGE");
								String alert = driver.findElement(By.id("dialog-alert-message-WJ_19")).getText();
								if (!alert.contains("The discount has been applied according to the conditions")) {
									System.out.println("INFO -> POP UP AFTER CHECK OUT");
									popup2 = su.popupHandler(driver);
								}
							}
						}
					} catch (Exception e) {

					}

					if (!popup2.getMessage().equals("")) {
						ReportPrinter.append("<td>Popup error message : " + popup2.getMessage() + "</td>" + "<td class='Failed'>Fail</td></tr>");
						Reportcount++;
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");
					} else {
						ReportPrinter.append("<td>No Errors</td>" + "<td class='Passed'>PASS</td></tr>");
						Reportcount++;
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");

						// ================================================FILL PAYMENT PAGE UI DETAILS===========================================
						// DISCOUNT APPLICATION FOR THE SELECTED PRICE INFO OBJECT FROM PROPERTY FILE
						try {
							if (searchObject.isApplyDiscount()) {
								System.out.println("INFO -> APPLY DISCOUNT");
								if (driver.findElement(By.id("dialog-alert-message-WJ_19")).isDisplayed()) {
									String alert = "";
									alert = driver.findElement(By.id("dialog-alert-message-WJ_19")).getText();
									if (alert.equalsIgnoreCase(" The discount has been applied according to the conditions. The booking values are updated accordingly. ")
											|| alert.contains("The discount has been applied according to the conditions. The booking values are updated accordingly.")) {
										// XMLSelectFlight.getPricinginfo().setDiscount(PropertyMap.get("DiscountType"), PropertyMap.get("DiscountValue"));

										XMLSelectFlight.getPricinginfo().setDiscount(PropertyMap.get("DiscountType"), PropertyMap.get("DiscountValue"), PropertyMap.get("Portal_Currency_Code"), CurrencyMap);

										driver.findElement(By.className("ui-button-text")).click();
									}
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}

						// FILLING CUSTOMER DETAILS FOR THE FILLING OBJECT
						FillReservationDetails fill = new FillReservationDetails();
						ReservationInfo fillingObject = new ReservationInfo();
						try {
							fillingObject = fill.Fill_Reservation_details(searchObject);
							done = webPaymentPage.setPaymentPage(driver, fillingObject, searchObject, XMLSelectFlight, PropertyMap, CurrencyMap);
						} catch (Exception e) {
							e.printStackTrace();
							System.out.println(e.toString());
						}

						// ==============================================PAYMENT PAGE VALIDATION==================================================

						ReportPrinter.append("<span><center><p class='Hedding0'>Payment Page Details Validation (Web)</p></center></span>");
						ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
						try {
							ReportPrinter = validate.validatePaymentPage(webPaymentPage, XMLSelectFlight, searchObject, configuration, ReportPrinter);
						} catch (Exception e) {

						}
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");

						// =====================================CLICK PROCEED BUTTON IN PAYMENT PAGE==============================================
						 
						if (!searchObject.isQuotation()) {
							try {
								wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("status_F_AIR1")));
								intermediateStatus = driver.findElement(By.id("status_F_AIR1")).getText();
								intermediateinfo.setIntermediateStatus(intermediateStatus);
								intermediateMessage = driver.findElement(By.id("message_F_AIR1")).getText();
								intermediateinfo.setIntermediateMessage(intermediateMessage);

								driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
								driver.switchTo().defaultContent();

								try {
									WebDriverWait wait1 = new WebDriverWait(driver, 120);
									wait1.until(ExpectedConditions.presenceOfElementLocated(By.className("complete-btn")));
									wait1.until(ExpectedConditions.elementToBeClickable(By.className("complete-btn")));
									driver.findElement(By.id("divConfirm")).findElement(By.className("complete-btn")).click();
									System.out.println("INFO -> CONFIRM PROCEED TO PAYMENT GATEWAY");
								} catch (Exception e) {
									System.out.println("ERROR EXCEPTION : Class - "+this.getClass().getName() + " Line - "+e.getStackTrace()[0].getLineNumber());
									System.out.println("INFO -> CONFIRM PROCEED TO PAYMENT GATEWAY FAILED....!!!!");
								}

								PopupMessage popupatCreditPay = new PopupMessage();
								if (!popupatCreditPay.getMessage().equals("")) {
									
								} else {

									ResvRequestReader resvRequestReader11 = new ResvRequestReader(propertyMap);
									ReservationRequest reservationReq11 = new ReservationRequest();
									try {
										reservationReq11 = resvRequestReader11.RequestReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ReservationRequest_);
									} catch (Exception e) {

									}
									
									ReportPrinter.append("<span><center><p class='Hedding0'>ReservationRequest XML Validation</p></center></span>");
									ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
									ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>Check Availability of Reservation Request XML</td>" + "<td>Available</td>");

									if (reservationReq11.isAvailable()) {
										ReportPrinter.append("<td>Available</td>" + "<td class='Passed'>PASS</td></tr>");
										Reportcount++;

										try {
											ReportPrinter = validate.validateResvRequest(reservationReq11, fillingObject, XMLSelectFlight, ReportPrinter);
										} catch (Exception e) {

										}

										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");

										ResvResponseReader resvResponseReader = new ResvResponseReader(propertyMap);

										driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

										ReservationResponse reservationResp = new ReservationResponse();
										try {
											reservationResp = resvResponseReader.ResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ReservationResponse_);
										} catch (Exception e) {

										}

										ReportPrinter.append("<span><center><p class='Hedding0'>Unexpected Error Validation For ReservationResponse XML Errors</p></center></span>");
										ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");

										ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>If error occured in Reservation Response XML</td>" + "<td>If error occured - Error Message should be displayed</td>");

										PopupMessage PopupResvRespError = new PopupMessage();
										/*try {
											try {
												// !driver.findElement(By.id(propertyMap.get("ConfPg_BookingReference_Lbl_id").trim())).isDisplayed()
												String ele = propertyMap.get("ConfPg_BookingReference_Lbl_id").trim();
												boolean c = false;
												c = CommonValidator.elementVisible(driver, ele);
												if (!c) {
													PopupResvRespError = su.popupHandler(driver);
												}
											} catch (Exception e) {

											}
										} catch (Exception e) {

										}*/

										if (!reservationResp.getError().getErrorMessage().equals("")) {
											ReportPrinter.append("<td>XML error message : " + reservationResp.getError().getErrorMessage() + "</td>" + "<td class='Failed'>Fail</td></tr>");
											Reportcount++;

											ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>A popup message should be displayed in the system if an error occured in Reservation Response XML</td>"
													+ "<td>If error occurred - Pop up error message content should be displayed</td>");
											if (!PopupResvRespError.getMessage().equals("")) {
												ReportPrinter.append("<td>" + PopupResvRespError.getMessage() + "</td>" + "<td class='Failed'>Fail</td></tr>");
												Reportcount++;
											} else {
												ReportPrinter.append("<td>" + PopupResvRespError.getMessage() + "</td>" + "<td class='Failed'>Fail</td></tr>");
												Reportcount++;
											}
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
										} else {
											ReportPrinter.append("<td>No errors detected in Reservation Response XML</td>" + "<td class='Passed'>PASS</td></tr>");
											Reportcount++;
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");

											// ===================================VALIDATE RESERVATION RESPONSE XML=====================================

											ReportPrinter.append("<span><center><p class='Hedding0'>ReservationResponse XML Validation</p></center></span>");
											ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
											try {
												ReportPrinter = validate.validateResvResponse(reservationResp, fillingObject, XMLSelectFlight, ReportPrinter);
											} catch (Exception e) {

											}

											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");

											// OMAN PAYMENT
											if (PropertyMap.get("PayGatewayChange").trim().equalsIgnoreCase("true")) {
												try {
													driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
													if (searchObject.getPaymentMode().equalsIgnoreCase("Pay Online")) {
														
														PaymentGateway pay = new PaymentGateway();
														//pay.rezPay  (driver, PropertyMap.get("CardNo1"), PropertyMap.get("CardNo2"), PropertyMap.get("CardNo3"), PropertyMap.get("CardNo4"), PropertyMap.get("CardMonth"), PropertyMap.get("CardYear"), PropertyMap.get("CardHolder"), PropertyMap.get("CardHolderSSN"));
														pay.wireCard(driver, PropertyMap.get("CardNo1").concat(PropertyMap.get("CardNo2")).concat(PropertyMap.get("CardNo3")).concat(PropertyMap.get("CardNo4")), PropertyMap.get("CardHolderSSN"), PropertyMap.get("CardHolderFname"), PropertyMap.get("CardHolderLname"), PropertyMap.get("CardType"), PropertyMap.get("CardMonth"), PropertyMap.get("CardYear"));
														//pay.wireCard(driver, PropertyMap.get("CardNo1").concat(PropertyMap.get("CardNo2")).concat(PropertyMap.get("CardNo3")).concat(PropertyMap.get("CardNo4")), PropertyMap.get("CardHolderSSN"), PropertyMap.get("CardHolder"), PropertyMap.get("CardHolder"), PropertyMap.get("CardMonth"), PropertyMap.get("CardYear"));
													}

												} catch (Exception e) {
													e.printStackTrace();
												}
											}

											// ====================================VALIDATING PNR AND ETICKET XMLS=======================================

											/*
											 * //VALIDATE E-TICKET REQUEST XML WITH RESERVATION RESPONSE XML EticketRequestReader eTicketReqReader = new EticketRequestReader(Propertymap);
											 * EticketRequest eTicketRequest = eTicketReqReader.RequestReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ETicketRequest_);
											 * ReportPrinter.append("<span><center><p class='Hedding0'>E-Ticket Request Validation</p></center></span>");
											 * ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" +
											 * "<th>Actual Result</th>" + "<th>Test Status</th></tr>"); ReportPrinter = validate.validateEticketRequest(eTicketRequest, confirmationpage,
											 * reservationResp, ReportPrinter); ReportPrinter.append("</table>"); ReportPrinter.append("<br><br><br>");
											 * 
											 * //VALIDATE E-TICKET RESPONSE XML WITH E-TICKET REQUEST XML EticketResponseReader eTicketResReader = new EticketResponseReader(Propertymap);
											 * EticketResponse eTicketResponse = eTicketResReader.ResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ETicketResponse_);
											 * 
											 * ReportPrinter.append("<span><center><p class='Hedding0'>Testing E-Ticker Response XML errors</p></center></span>");
											 * ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" +
											 * "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
											 * 
											 * ReportPrinter.append("<tr><td>"+Reportcount+"</td>" + "<td>Check for E-ticket response errors</td>" +
											 * "<td>If error occured - Error Message should be displayed</td>"); if( !eTicketResponse.getError().getErrorMessage().equals("") ) {
											 * ReportPrinter.append("<td>Popup error message : "+eTicketResponse.getError().getErrorMessage()+"</td>" +
											 * "<td class='Passed'>Error occurred and Message displayed - PASS</td></tr>"); Reportcount++; ReportPrinter.append("</table>");
											 * ReportPrinter.append("<br><br><br>"); } else { ReportPrinter.append("<td>E-ticket response contains no errors</td>" +
											 * "<td>TEST DID NOT OCCUR</td></tr>"); Reportcount++; ReportPrinter.append("</table>"); ReportPrinter.append("<br><br><br>");
											 * 
											 * ReportPrinter.append("<span><center><p class='Hedding0'>E-Ticket Response Validation</p></center></span>");
											 * ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" +
											 * "<th>Actual Result</th>" + "<th>Test Status</th></tr>"); ReportPrinter = validate.validateEticketResponse(eTicketResponse, eTicketRequest,
											 * ReportPrinter); ReportPrinter.append("</table>"); ReportPrinter.append("<br><br><br>"); }//End E-ticket response error handler
											 */

											// =========================================VALIDATE CONFIRMATION PAGE=======================================
											
											try {
												try {
													done = webConfirmationPage.setConfirmationPage(driver, searchObject);
												} catch (Exception e) {

												}

												ReportPrinter.append("<span><center><p class='Hedding0'>Confirmation Page Validation(Web)</p></center></span>");
												ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");

												try {
													ReportPrinter = validate.validateConfirmationPage(webConfirmationPage, ReportPrinter, fillingObject, XMLSelectFlight/* , reservationResp */);
												} catch (Exception e) {

												}

												ReportPrinter.append("</table>");
												ReportPrinter.append("<br><br><br>");
											} catch (Exception e) {
												e.printStackTrace();
											}

											// =========================================START VALIDATING REPORTS==========================================

											if (webConfirmationPage.isDone()) {
												try {
													su.login(driver, propertyMap.get("portal.username"), propertyMap.get("portal.password"));
												} catch (Exception e) {
													System.out.println("TO login failed");
												}

												ValidateReports validateReports = new ValidateReports(propertyMap, validate);

												// ===================================RESERVATION REPORT VALIDATION=======================================

												try {
													ReportPrinter = validateReports.validateReservationReport(driver, webConfirmationPage.getReservationNo(), ReportPrinter, searchObject, configuration, XMLSelectFlight, Reportcount);
												} catch (Exception e) {

												}

												// ===================================SUPPLIER PAYABLE VALIDATION=======================================

												try {
													ReportPrinter = validateReports.validateSuppPlayableReport(driver, webConfirmationPage.getReservationNo(), webConfirmationPage.getSupplierConfirmationNo(),  ReportPrinter, searchObject, fillingObject, XMLSelectFlight, configuration, Reportcount)/*																																													 */;
												} catch (Exception e) {

												}

												// =================================CUSTOMER VOUCHER E-MAIL VALIDATION====================================

												try {
													ReportPrinter = validateReports.validateCustomerVoucherEmail(driver, webConfirmationPage.getReservationNo(), ReportPrinter, searchObject, XMLSelectFlight, fillingObject, configuration, Reportcount);
												} catch (Exception e) {

												}

												// ===================================BOOKING CONFIRMATION VALIDATION======================================

												/*
												 * try { ReportPrinter = validateReports.validateBookingConfReport(driver, webConfirmationPage, searchObject, fillingObject, XMLSelectFlight,
												 * configuration, ReportPrinter, Reportcount); } catch (Exception e) {
												 * 
												 * }
												 */

												// ==================================BOOKING LIST REPORT VALIDATION=========================================

												try {
													ReportPrinter = validateReports.validateBookingListReport(driver, webConfirmationPage.getReservationNo(), searchObject, fillingObject, XMLSelectFlight, configuration, ReportPrinter, Reportcount);
												} catch (Exception e) {

												}

												// =========================================PROFIT AND LOSS REPORT==========================================

												try {
													ReportPrinter = validateReports.validateProfitLossReport(driver, webConfirmationPage.getReservationNo(), searchObject, fillingObject, XMLSelectFlight, configuration, ReportPrinter, Reportcount);
												} catch (Exception e) {

												}

												// ======================================TOUR OPERATOR PROCESS STARTS=========================================
												if (searchObject.getTOBooking().equalsIgnoreCase("YES")) {
													try {
														su.login(driver, propertyMap.get("portal.TOusername"), propertyMap.get("portal.TOpassword"));

														TOBooking TObooking = new TOBooking();

														// Read Booking List Report after reservation and Issue Voucher
														TObooking.bookingListReportTOBfinal(driver, webConfirmationPage.getReservationNo().trim(), instanceTO1, XMLSelectFlight, XMLSelectFlight.getCancellationDate(), searchObject.getTOCashCredit(),
																PropertyMap.get("Portal_Currency_Code"), searchObject.getPaymentMode(), CurrencyMap, ReportPrinter);

														// Login as the TO
														su.login(driver, propertyMap.get("portal.TOusername"), propertyMap.get("portal.TOpassword"));

														// Read TO after issue voucher & validate
														instanceTO2.setTO(driver);
														validate.validateTOafterVoucher(instanceTO1, instanceTO2, XMLSelectFlight, searchObject, configuration, ReportPrinter);

														// Login as the Internal
														su.login(driver, propertyMap.get("portal.username"), propertyMap.get("portal.password"));

														// Pay for the reservation
														String url = "http://dev3.rezg.net/rezbase_v3/reports/operational/mainReport.do?reportId=8&reportName=Payment%20Report";
														TObooking.pay(driver, webConfirmationPage, url);

														// Login as TO
														su.login(driver, propertyMap.get("portal.TOusername"), propertyMap.get("portal.TOpassword"));
														TObooking.bookingListReportToFinal(driver, webConfirmationPage, instanceTO1, XMLSelectFlight, XMLSelectFlight.getCancellationDate(), searchObject.getTOCashCredit(),
																PropertyMap.get("Portal_Currency_Code"), searchObject.getPaymentMode(), CurrencyMap, ReportPrinter);

														// Login as the TO
														su.login(driver, propertyMap.get("portal.TOusername"), propertyMap.get("portal.TOpassword"));

														// Read TO after issue payment & validate
														TOperator intanceTOPaid = new TOperator();
														intanceTOPaid.setTO(driver);
														validate.validateTOafterPay(instanceTO1, intanceTOPaid, XMLSelectFlight, searchObject, configuration, ReportPrinter);

													} catch (Exception e) {
														System.out.println("TO login failed");
													}

												}// TO "YES"

												// ===========================================CANCELLATION PROCESS==========================================
												if (searchObject.getCancellationStatus().equalsIgnoreCase("Yes")) {
													CancellationScreen cancellationScreen = new CancellationScreen(propertyMap);
													CancellationReport cancellationReport = new CancellationReport(propertyMap);
													ReportPrinter = validateReports.doCancellation(driver, cancellationScreen, cancellationReport, webConfirmationPage.getReservationNo(), webConfirmationPage.getSupplierConfirmationNo().trim(), searchObject, fillingObject, XMLSelectFlight, ReportPrinter, Reportcount);

													// =====================================RESERVATION REPORT VALIDATION=====================================
													try {
														ReportPrinter = validateReports.validateReservationReportAfterCancel(driver, webConfirmationPage.getReservationNo(), searchObject, XMLSelectFlight, configuration, ReportPrinter, Reportcount);
													} catch (Exception e) {

													}

													// =========================================PROFIT AND LOSS REPORT========================================
													try {
														ReportPrinter = validateReports.validateProfitLossReportAfterCancel(driver, cancellationScreen, cancellationReport, webConfirmationPage.getReservationNo(), searchObject, fillingObject, XMLSelectFlight,
																configuration, ReportPrinter, Reportcount);
													} catch (Exception e) {

													}

													// ======================================SUPPLIER PAYABLE VALIDATION======================================
													try {
														ReportPrinter = validateReports.validateThirdPartySuppPayableAfterCancel(driver, webConfirmationPage.getReservationNo(), webConfirmationPage.getSupplierConfirmationNo(), searchObject, XMLSelectFlight, configuration, ReportPrinter, Reportcount);
													} catch (Exception e) {

													}

													// =====================================BOOKING LIST REPORT VALIDATION====================================
													try {
														ReportPrinter = validateReports.validateBookingListReportAfterCancel(driver, webConfirmationPage.getReservationNo(), searchObject, fillingObject, XMLSelectFlight, configuration, ReportPrinter, Reportcount);
													} catch (Exception e) {

													}

												}// Cancellation process

											}// Confirmation page

										}// End Reservation Response error handler

									} else {
										ReportPrinter.append("<td>Not Available</td>" + "<td class='Failed'>Fail</td></tr>");
										Reportcount++;
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
									}

								}// End Payment Gateway error handler
							} catch (Exception e) {
								e.printStackTrace();
								ReportPrinter.append("</table>");
								ReportPrinter.append("<br><br><br>");
							}

						}// Starts Quotation
						else {
							CallCenter CCRun = new CallCenter();
							CCRun.setPropertymap(PropertyMap);
							QuotationPage quotation = new QuotationPage(PropertyMap);
							try {
								done = quotation.setQuotation(driver, searchObject);
							} catch (Exception e) {
								e.printStackTrace();
							}

							if (done) {
								ReportPrinter.append("<span><center><p class='Hedding0'>Quotation Page Validation</p></center></span>");
								ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
								try {
									ReportPrinter = validate.validateQuotationPage(quotation, ReportPrinter, fillingObject, XMLSelectFlight);
								} catch (Exception e) {
									e.printStackTrace();
								}
								ReportPrinter.append("</table>");
								ReportPrinter.append("<br><br><br>");

								// Check quotation mail
								Calendar cal = Calendar.getInstance();
								String Date = new SimpleDateFormat("dd-MM-yyyy").format(cal.getTime());
								String mailUrl = "";
								mailUrl = propertyMap.get("QuotationMailLink");
								mailUrl = mailUrl.concat("%2F" + Date);
								try {
									SupportMethods SUP = new SupportMethods(PropertyMap);
									WebDriver driver2 = SUP.initalizeDriver();
									driver2.get(mailUrl);
									try {
										driver2.findElement(By.partialLinkText(quotation.getReservationNo().trim())).click();
										driver2.findElement(By.partialLinkText("air_quotation")).click();
										ArrayList<String> windows = new ArrayList<String>(driver2.getWindowHandles());
										driver2.switchTo().window(windows.get(1));
										QuotationMail qmail = new QuotationMail();
										qmail.setQuotation(driver2);
										driver2.close();
										driver2.switchTo().window(windows.get(0));
										driver2.close();

										validate.validateQuotationMail(qmail, quotation, searchObject, XMLSelectFlight, ReportPrinter);

									} catch (Exception e) {
										driver2.close();
									}

								} catch (IOException e) {
									// Screenshot.takeScreenshot(scenarioFailedPath + "/"+TracerID+" XML read fail.jpg", driver);
								}

								try {
									QuotationBooking bookQuotation = new QuotationBooking(PropertyMap);
									boolean retreival = false;
									retreival = bookQuotation.loadQuotation(driver, searchObject, quotation);
									try {
										boolean x = false;
										try {
											x = driver.findElement(By.id("loadpayment")).isDisplayed();
										} catch (Exception e) {
											System.out.println("loadpayment element displayed");
										}

										if (x) {
											try {
												wait.until(ExpectedConditions.presenceOfElementLocated(By.id("loadpayment")));
												((JavascriptExecutor) driver).executeScript("JavaScript:loadPaymentPage();");
											} catch (Exception e) {
												e.printStackTrace();
											}
										}
									} catch (Exception e) {
										System.out.println("quotation load fails");
									}
									ReportPrinter.append("<span><center><p class='Hedding0'>Quotation Retrieval</p></center></span>");
									ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
									ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>Check quotation retrieval</td>" + "<td>If retrieval pass - true , if fails - false</td>");
									if (retreival) {
										ReportPrinter.append("<td>" + retreival + "</td>" + "<td class='Passed'>PASS</td></tr>");
										Reportcount++;
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
									} else {
										ReportPrinter.append("<td>" + retreival + "</td>" + "<td class='Failed'>Fail</td></tr>");
										Reportcount++;
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
									}

									// driver.findElement(By.id("loadpayment")).click();
									ReportPrinter.append("<span><center><p class='Hedding0'>Unexpected Error Validation While Checkout Cart Item</p></center></span>");
									ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
									ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>Check blocker errors that prevents checkout</td>" + "<td>If error occurred - Error Message should be displayed</td>");

									PopupMessage popupcc = new PopupMessage();
									try {
										if (/* !driver.findElement(By.id("status_F_AIR1")).isDisplayed() || */!driver.findElement(By.id("user_id")).isDisplayed() || !driver.findElement(By.id("dialog-alert-message-WJ_22")).isDisplayed()) {
											if (driver.findElement(By.id("dialog-alert-message-WJ_22")).isDisplayed()) {
												String alert = driver.findElement(By.id("dialog-alert-message-WJ_22")).getText();
												if (!alert.contains("The discount has been applied according to the conditions")) {
													popupcc = su.popupHandler(driver);
												}
											}

										}
									} catch (Exception e) {

									}
									if (!popupcc.getMessage().equals("")) {
										ReportPrinter.append("<td>Popup error message : " + popupcc.getMessage() + "</td>" + "<td class='Failed'>Fail</td></tr>");
										Reportcount++;
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
									} else {
										ReportPrinter.append("<td>No Errors</td>" + "<td class='Passed'>PASS</td></tr>");
										Reportcount++;
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
									}

									// wait.until(ExpectedConditions.presenceOfElementLocated(By.id("user_id")));
									try {
										if (searchObject.isApplyDiscount()) {
											if (driver.findElement(By.id("dialog-alert-message-WJ_22")).isDisplayed()) {
												String alert = "";
												alert = driver.findElement(By.id("dialog-alert-message-WJ_22")).getText();
												if (alert.equalsIgnoreCase(" The discount has been applied according to the conditions. The booking values are updated accordingly. ")
														|| alert.contains("The discount has been applied according to the conditions. The booking values are updated accordingly.")) {
													// XMLSelectFlight.getPricinginfo().setDiscount(propertyMap.get("DiscountType"), propertyMap.get("DiscountValue"));
													XMLSelectFlight.getPricinginfo().setDiscount(PropertyMap.get("DiscountType"), PropertyMap.get("DiscountValue"), PropertyMap.get("Portal_Currency_Code"), CurrencyMap);

													driver.findElement(By.className("ui-button-text")).click();
												}
											}
										}
									} catch (Exception e) {
										e.printStackTrace();
									}

									try {
										FillReservationDetails fillcc = new FillReservationDetails();
										@SuppressWarnings("unused")
										ReservationInfo fillingObjectcc = new ReservationInfo();
										fillingObjectcc = fillcc.Fill_Reservation_details(searchObject);
										UIPaymentPage paymentpage = new UIPaymentPage();

										driver = CCRun.Fill_Reservation_Details(driver, fillingObject, searchObject);

										try {
											paymentpage = CCRun.getPaymentPage(driver, XMLSelectFlight, searchObject);
										} catch (Exception e) {

										}

										ReportPrinter.append("<span><center><p class='Hedding0'>Payment Page Details Validation</p></center></span>");
										ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
										try {
											ReportPrinter = validate.validateCCPaymentPage(paymentpage, XMLSelectFlight, searchObject, configuration, ReportPrinter);
										} catch (Exception e) {

										}
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");

									} catch (Exception e) {
										e.printStackTrace();
									}

									ReportPrinter.append("<span><center><p class='Hedding0'>Unexpected Error Validation While Checkout Cart Item</p></center></span>");
									ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
									ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>Check blocker errors that prevents checkout</td>" + "<td>If error occurred - Error Message should be displayed</td>");

									PopupMessage popup3 = new PopupMessage();
									try {
										if (!driver.findElement(By.id("pre_res_dlgWJ_13")).isDisplayed()) {
											popup2 = su.popupHandler(driver);
										}

									} catch (Exception e) {

									}

									if (!popup3.getMessage().equals("")) {
										ReportPrinter.append("<td>Popup error message : " + popup3.getMessage() + "</td>" + "<td class='Failed'>Fail</td></tr>");
										Reportcount++;
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
									} else {
										ReportPrinter.append("<td>No Errors</td>" + "<td class='Passed'>PASS</td></tr>");
										Reportcount++;
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");

										try {
											wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("status_F_AIR1")));
											intermediateStatus = driver.findElement(By.id("status_F_AIR1")).getText();
											intermediateinfo.setIntermediateStatus(intermediateStatus);
											intermediateMessage = driver.findElement(By.id("message_F_AIR1")).getText();
											intermediateinfo.setIntermediateMessage(intermediateMessage);

											driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
											WebDriverWait wait2 = new WebDriverWait(driver, 20);

											try {
												driver.findElement(By.id("confirmbooking")).click();
											} catch (Exception e) {
												e.printStackTrace();
											}

											if (searchObject.getPaymentMode().equalsIgnoreCase("Pay Online")) {
												if (PropertyMap.get("PayGatewayChange").equalsIgnoreCase("false")) {
													try {
														driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
														driver.switchTo().frame("paygatewayFrame");
														wait2.until(ExpectedConditions.presenceOfElementLocated(By.id("cardnumberpart1")));
														((JavascriptExecutor) driver).executeScript("$('#cardnumberpart1').val('" + PropertyMap.get("CardNo1") + "');");
														((JavascriptExecutor) driver).executeScript("$('#cardnumberpart2').val('" + PropertyMap.get("CardNo2") + "');");
														((JavascriptExecutor) driver).executeScript("$('#cardnumberpart3').val('" + PropertyMap.get("CardNo3") + "');");
														((JavascriptExecutor) driver).executeScript("$('#cardnumberpart4').val('" + PropertyMap.get("CardNo4") + "');");

														new Select(driver.findElement(By.id("cardexpmonth"))).selectByVisibleText(PropertyMap.get("CardMonth"));
														new Select(driver.findElement(By.id("cardexpyear"))).selectByVisibleText(PropertyMap.get("CardYear"));
														driver.findElement(By.id("cardholdername")).sendKeys(PropertyMap.get("CardHolder"));
														((JavascriptExecutor) driver).executeScript("$('#cv2').val(" + PropertyMap.get("CardHolderSSN") + ");");

														payAmountCurrency = driver.findElement(By.xpath("/html/body/div/div/form/div[1]/div[6]/div[1]/h3")).getText();
														intermediateinfo.setPayAmountCurrency(payAmountCurrency);
														payAmount = driver.findElement(By.xpath("/html/body/div/div/form/div[1]/div[6]/div[2]")).getText();
														intermediateinfo.setPayAmount(payAmount);

														try {
															driver.findElement(By.className("proceed_btn")).click();
														} catch (Exception e) {
															e.printStackTrace();
															try {
																((JavascriptExecutor) driver).executeScript("javascript:submit_page()");
															} catch (Exception e2) {
																e.printStackTrace();
															}
														}

														try {
															driver.findElement(By.id("divConfirm")).findElement(By.className("complete-btn")).click();
														} catch (Exception e) {

														}

														driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
														PageSource = driver.getPageSource();
														intermediateinfo.setCreditCPaySumeryPage(PageSource);
														intermediateList.add(intermediateinfo);

														try {
															wait.until(ExpectedConditions.visibilityOf(driver.findElement(By
																	.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[2]/td[2]/b/input[1]"))));
															if (driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[2]/td[2]/b/input[1]"))
																	.isDisplayed()) {
																driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[2]/td[2]/b/input[1]"))
																		.sendKeys("1234");

																driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[4]/td[2]/b/input")).click();
															}
														} catch (Exception e) {
															e.printStackTrace();
														}

													} catch (Exception e) {

													}
												}
											}

											PopupMessage popupatCreditPay = new PopupMessage();
											popupatCreditPay = su.popupHandler(driver);

											ReportPrinter.append("<span><center><p class='Hedding0'>Error Validation For Unexpected Blocker Issues while Payment</p></center></span>");
											ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
											ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>Test for payment gateway issues</td>" + "<td>If error occured - Error Message should be displayed</td>");

											if (!popupatCreditPay.getMessage().equals("") && !popupatCreditPay.getMessage().contains("Unable to create the E-Ticket")) {
												ReportPrinter.append("<td>Popup error message : " + popupatCreditPay.getMessage() + "</td>" + "<td class='Failed'>Fail</td></tr>");
												Reportcount++;
												ReportPrinter.append("</table>");
												ReportPrinter.append("<br><br><br>");

												ResvRequestReader resvRequestReader1 = new ResvRequestReader(propertyMap);
												ReservationRequest reservationReq1 = new ReservationRequest();
												try {
													reservationReq1 = resvRequestReader1.RequestReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ReservationRequest_);
												} catch (Exception e) {

												}

												ReportPrinter.append("<span><center><p class='Hedding0'>Reservation Request XML Validation</p></center></span>");
												ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
												ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>Check Availability of Reservation Request XML</td>" + "<td>Available</td>");

												if (reservationReq1.isAvailable()) {
													ReportPrinter.append("<td>Available</td>" + "<td class='Passed'>PASS</td></tr>");
													Reportcount++;

													try {
														ReportPrinter = validate.validateResvRequest(reservationReq1, fillingObject, XMLSelectFlight, ReportPrinter);
													} catch (Exception e) {

													}

													ReportPrinter.append("</table>");
													ReportPrinter.append("<br><br><br>");
												} else {
													ReportPrinter.append("<td>Not Available</td>" + "<td class='Failed'>Fail</td></tr>");
													Reportcount++;
													ReportPrinter.append("</table>");
													ReportPrinter.append("<br><br><br>");
												}

												ResvResponseReader resvResponseReader1 = new ResvResponseReader(propertyMap);
												ReservationResponse reservationResp1 = new ReservationResponse();
												try {
													reservationResp1 = resvResponseReader1.ResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ReservationResponse_);
												} catch (Exception e) {

												}

												ReportPrinter.append("<span><center><p class='Hedding0'>Check for Reservation Response XML Error Caused for Blocker</p></center></span>");
												ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");

												ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>Error occured in Reservation Response XML</td>" + "<td>Error Message should be displayed</td>");

												if (!reservationResp1.isAvailable()) {

													if (!reservationResp1.getError().getErrorMessage().equals("")) {
														ReportPrinter.append("<td>XML error message : " + reservationResp1.getError().getErrorMessage() + "</td>" + "<td class='Passed'>PASS</td></tr>");
														Reportcount++;

														ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>XA Popup message should be displayed in the system if an error occured in Reservation Response XML</td>"
																+ "<td>Displayed - TRUE</td>");
														if (!popupatCreditPay.getMessage().equals("")) {
															ReportPrinter.append("<td>TRUE</td>" + "<td class='Passed'>PASS</td></tr>");
															Reportcount++;
														} else {
															ReportPrinter.append("<td>FALSE</td>" + "<td class='Failed'>Fail</td></tr>");
															Reportcount++;
														}

														ReportPrinter.append("</table>");
														ReportPrinter.append("<br><br><br>");
													} else {
														ReportPrinter.append("</table>");
														ReportPrinter.append("<br><br><br>");
													}

												} else {
													ReportPrinter.append("<td>XML error message : NO ERROR IN RESPONSE XML</td>" + "<td class='Passed'>PASS</td></tr>");
													Reportcount++;

													ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>If Popup message displayed in the system without any error occurred in Reservation Response XML</td>" + "<td>Displayed - TRUE</td>");
													if (!popupatCreditPay.getMessage().equals("")) {
														ReportPrinter.append("<td>TRUE</td>" + "<td class='Passed'>PASS</td></tr>");
														Reportcount++;
													} else {
														ReportPrinter.append("<td>FALSE</td>" + "<td class='Failed'>Fail</td></tr>");
														Reportcount++;
													}
													ReportPrinter.append("</table>");
													ReportPrinter.append("<br><br><br>");
												}
											} else {

												if (popupatCreditPay.getMessage().contains("Unable to create the E-Ticket")) {
													eticketIssued = false;
												} else {
													eticketIssued = true;
												}

												ReportPrinter.append("<td>No Payment gateway errors</td>" + "<td class='Passed'>PASS</td></tr>");
												Reportcount++;
												ReportPrinter.append("</table>");
												ReportPrinter.append("<br><br><br>");

												ResvRequestReader resvRequestReader = new ResvRequestReader(propertyMap);
												try {
													Thread.sleep(6000);
												} catch (InterruptedException e) {
													e.printStackTrace();
												}

												ReservationRequest reservationReq = new ReservationRequest();
												try {
													reservationReq = resvRequestReader.RequestReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ReservationRequest_);
												} catch (Exception e) {

												}

												ReportPrinter.append("<span><center><p class='Hedding0'>ReservationRequest XML Validation</p></center></span>");
												ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
												ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>Check Availability of Reservation Request XML</td>" + "<td>Available</td>");

												if (reservationReq.isAvailable()) {
													ReportPrinter.append("<td>Available</td>" + "<td class='Passed'>PASS</td></tr>");
													Reportcount++;

													try {
														ReportPrinter = validate.validateResvRequest(reservationReq, fillingObject, XMLSelectFlight, ReportPrinter);
													} catch (Exception e) {

													}
													ReportPrinter.append("</table>");
													ReportPrinter.append("<br><br><br>");

													ResvResponseReader resvResponseReader = new ResvResponseReader(propertyMap);
													try {
														Thread.sleep(4000);
													} catch (InterruptedException e) {
														e.printStackTrace();
													}

													driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

													ReservationResponse reservationResp = new ReservationResponse();
													try {
														reservationResp = resvResponseReader.ResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ReservationResponse_);
													} catch (Exception e) {

													}

													ReportPrinter.append("<span><center><p class='Hedding0'>Unexpected Error Validation for ReservationResponse XML Errors</p></center></span>");
													ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");

													ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>If error occured in Reservation Response XML</td>" + "<td>If error occured - Error Message should be displayed</td>");

													PopupMessage PopupResvRespError = su.popupHandler(driver);

													if (!reservationResp.getError().getErrorMessage().equals("")) {
														ReportPrinter.append("<td>XML error message : " + reservationResp.getError().getErrorMessage() + "</td>" + "<td class='Failed'>Fail</td></tr>");
														Reportcount++;

														ReportPrinter.append("<tr><td>" + Reportcount + "</td>" + "<td>A popup message should be displayed in the system if an error occured in Reservation Response XML</td>"
																+ "<td>If error occurred - Pop up error message content should be displayed</td>");
														if (!PopupResvRespError.getMessage().equals("")) {
															ReportPrinter.append("<td>" + PopupResvRespError.getMessage() + "</td>" + "<td class='Failed'>Fail</td></tr>");
															Reportcount++;
														} else {
															ReportPrinter.append("<td>" + PopupResvRespError.getMessage() + "</td>" + "<td class='Failed'>Fail</td></tr>");
															Reportcount++;
														}

														ReportPrinter.append("</table>");
														ReportPrinter.append("<br><br><br>");
													} else {
														ReportPrinter.append("<td>No errors detected in Reservation Response XML</td>" + "<td class='Passed'>Passed</td></tr>");
														Reportcount++;
														ReportPrinter.append("</table>");
														ReportPrinter.append("<br><br><br>");

														// ===================================VALIDATE RESERVATION RESPONSE XML=====================================

														ReportPrinter.append("<span><center><p class='Hedding0'>ReservationResponse XML Validation</p></center></span>");
														ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
														try {
															ReportPrinter = validate.validateResvResponse(reservationResp, fillingObject, XMLSelectFlight, ReportPrinter);
														} catch (Exception e) {

														}
														ReportPrinter.append("</table>");
														ReportPrinter.append("<br><br><br>");

														if (searchObject.getPaymentMode().equalsIgnoreCase("Pay Online")) {
															if (propertyMap.get("PayGatewayChange").equalsIgnoreCase("false")) {
																try {
																	driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
																	driver.switchTo().frame("paygatewayFrame");
																	wait2.until(ExpectedConditions.presenceOfElementLocated(By.id("cardnumberpart1")));
																	((JavascriptExecutor) driver).executeScript("$('#cardnumberpart1').val('" + PropertyMap.get("CardNo1") + "');");
																	((JavascriptExecutor) driver).executeScript("$('#cardnumberpart2').val('" + PropertyMap.get("CardNo2") + "');");
																	((JavascriptExecutor) driver).executeScript("$('#cardnumberpart3').val('" + PropertyMap.get("CardNo3") + "');");
																	((JavascriptExecutor) driver).executeScript("$('#cardnumberpart4').val('" + PropertyMap.get("CardNo4") + "');");

																	new Select(driver.findElement(By.id("cardexpmonth"))).selectByVisibleText(PropertyMap.get("CardMonth"));
																	new Select(driver.findElement(By.id("cardexpyear"))).selectByVisibleText(PropertyMap.get("CardYear"));
																	driver.findElement(By.id("cardholdername")).sendKeys(PropertyMap.get("CardHolder"));
																	((JavascriptExecutor) driver).executeScript("$('#cv2').val(" + PropertyMap.get("CardHolderSSN") + ");");

																	payAmountCurrency = driver.findElement(By.xpath("/html/body/div/div/form/div[1]/div[6]/div[1]/h3")).getText();
																	intermediateinfo.setPayAmountCurrency(payAmountCurrency);
																	payAmount = driver.findElement(By.xpath("/html/body/div/div/form/div[1]/div[6]/div[2]")).getText();
																	intermediateinfo.setPayAmount(payAmount);

																	try {
																		driver.findElement(By.className("proceed_btn")).click();
																	} catch (Exception e) {
																		e.printStackTrace();
																		try {
																			((JavascriptExecutor) driver).executeScript("javascript:submit_page()");
																		} catch (Exception e2) {
																			e.printStackTrace();
																		}
																	}

																	try {
																		driver.findElement(By.id("divConfirm")).findElement(By.className("complete-btn")).click();
																	} catch (Exception e) {

																	}

																	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
																	PageSource = driver.getPageSource();
																	intermediateinfo.setCreditCPaySumeryPage(PageSource);
																	intermediateList.add(intermediateinfo);

																	try {
																		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By
																				.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[2]/td[2]/b/input[1]"))));
																		if (driver.findElement(
																				By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[2]/td[2]/b/input[1]"))
																				.isDisplayed()) {
																			driver.findElement(
																					By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[2]/td[2]/b/input[1]"))
																					.sendKeys("1234");

																			driver.findElement(
																					By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[4]/td[2]/b/input")).click();
																		}
																	} catch (Exception e) {
																		e.printStackTrace();
																	}

																} catch (Exception e) {

																}
															}
														}

														// ====================================VALIDATING PNR AND ETICKET XMLS=======================================

														/*
														 * //VALIDATE E-TICKET REQUEST XML WITH RESERVATION RESPONSE XML EticketRequestReader eTicketReqReader = new EticketRequestReader(Propertymap);
														 * EticketRequest eTicketRequest = eTicketReqReader.RequestReader(XMLLocateType.TRACER, searchObject.getTracer(),
														 * XMLFileType.AIR1_ETicketRequest_); ReportPrinter.append("<span><center><p class='Hedding0'>E-Ticket Request Validation</p></center></span>");
														 * ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" +
														 * "<th>Actual Result</th>" + "<th>Test Status</th></tr>"); ReportPrinter = validate.validateEticketRequest(eTicketRequest, confirmationpage,
														 * reservationResp, ReportPrinter); ReportPrinter.append("</table>"); ReportPrinter.append("<br><br><br>");
														 * 
														 * //VALIDATE E-TICKET RESPONSE XML WITH E-TICKET REQUEST XML EticketResponseReader eTicketResReader = new EticketResponseReader(Propertymap);
														 * EticketResponse eTicketResponse = eTicketResReader.ResponseReader(XMLLocateType.TRACER, searchObject.getTracer(),
														 * XMLFileType.AIR1_ETicketResponse_);
														 * 
														 * ReportPrinter.append("<span><center><p class='Hedding0'>Testing E-Ticker Response XML errors</p></center></span>");
														 * ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" +
														 * "<th>Actual Result</th>" + "<th>Test Status</th></tr>");
														 * 
														 * ReportPrinter.append("<tr><td>"+Reportcount+"</td>" + "<td>Check for E-ticket response errors</td>" +
														 * "<td>If error occured - Error Message should be displayed</td>"); if( !eTicketResponse.getError().getErrorMessage().equals("") ) {
														 * ReportPrinter.append("<td>Popup error message : "+eTicketResponse.getError().getErrorMessage()+"</td>" +
														 * "<td class='Passed'>Error occurred and Message displayed - PASS</td></tr>"); Reportcount++; ReportPrinter.append("</table>");
														 * ReportPrinter.append("<br><br><br>"); } else { ReportPrinter.append("<td>E-ticket response contains no errors</td>" +
														 * "<td>TEST DID NOT OCCUR</td></tr>"); Reportcount++; ReportPrinter.append("</table>"); ReportPrinter.append("<br><br><br>");
														 * 
														 * ReportPrinter.append("<span><center><p class='Hedding0'>E-Ticket Response Validation</p></center></span>");
														 * ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" +
														 * "<th>Actual Result</th>" + "<th>Test Status</th></tr>"); ReportPrinter = validate.validateEticketResponse(eTicketResponse, eTicketRequest,
														 * ReportPrinter); ReportPrinter.append("</table>"); ReportPrinter.append("<br><br><br>"); }//End E-ticket response error handler
														 */

														// =========================================VALIDATE CONFIRMATION PAGE=======================================

														WebConfirmationPage confirmationpage = new WebConfirmationPage(PropertyMap);
														try {
															try {
																confirmationpage = CCRun.getConfirmationPageQ(driver, XMLSelectFlight, PropertyMap);
																// c = CCRun.getConfirmationPage(driver, XMLSelectFlight);
															} catch (Exception e) {

															}

															ReportPrinter.append("<span><center><p class='Hedding0'>Confirmation Page Validation</p></center></span>");
															ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>"
																	+ "<th>Test Status</th></tr>");
															try {
																ReportPrinter = validate.validateConfirmationPageCCQ(searchObject, confirmationpage, ReportPrinter, fillingObject, XMLSelectFlight);
															} catch (Exception e) {

															}

															ReportPrinter.append("</table>");
															ReportPrinter.append("<br><br><br>");

														} catch (Exception e) {
															e.printStackTrace();
														}

														// =========================================START VALIDATING REPORTS==========================================

														if (confirmationpage.isAvailable()) {

															ValidateReports validateReports = new ValidateReports(propertyMap, validate);

															// ===================================RESERVATION REPORT VALIDATION=======================================

															try {
																ReportPrinter = validateReports.validateReservationReport(driver, confirmationpage.getReservationNo(), ReportPrinter, searchObject, configuration, XMLSelectFlight, Reportcount);
															} catch (Exception e) {

															}

															// ===================================SUPPLIER PAYABLE VALIDATION=======================================

															try {
																ReportPrinter = validateReports.validateSuppPlayableReport(driver, confirmationpage.getReservationNo(), confirmationpage.getSupplierConfirmationNo(), ReportPrinter, searchObject, fillingObject, XMLSelectFlight, configuration, Reportcount)/*																																											 */;
															} catch (Exception e) {

															}

															// =================================CUSTOMER VOUCHER E-MAIL VALIDATION====================================

															try {
																ReportPrinter = validateReports.validateCustomerVoucherEmail(driver, confirmationpage.getReservationNo(), ReportPrinter, searchObject, XMLSelectFlight, fillingObject, configuration, Reportcount);
															} catch (Exception e) {

															}

															// ===================================BOOKING CONFIRMATION VALIDATION======================================

															/*
															 * try { ReportPrinter = validateReports.validateBookingConfReport(driver, confirmationpage, searchObject, fillingObject, XMLSelectFlight,
															 * configuration, ReportPrinter, Reportcount); } catch (Exception e) {
															 * 
															 * }
															 */

															// ==================================BOOKING LIST REPORT VALIDATION=========================================

															try {
																ReportPrinter = validateReports.validateBookingListReport(driver, confirmationpage.getReservationNo(), searchObject, fillingObject, XMLSelectFlight, configuration, ReportPrinter, Reportcount);
															} catch (Exception e) {

															}

															// =========================================PROFIT AND LOSS REPORT==========================================

															try {
																ReportPrinter = validateReports.validateProfitLossReport(driver, confirmationpage.getReservationNo(), searchObject, fillingObject, XMLSelectFlight, configuration, ReportPrinter, Reportcount);
															} catch (Exception e) {

															}

															// ===========================================CANCELLATION PROCESS==========================================
															if (searchObject.getCancellationStatus().equalsIgnoreCase("Yes")) {
																CancellationScreen cancellationScreen = new CancellationScreen(propertyMap);
																CancellationReport cancellationReport = new CancellationReport(propertyMap);
																ReportPrinter = validateReports
																		.doCancellation(driver, cancellationScreen, cancellationReport, confirmationpage.getReservationNo(), confirmationpage.getSupplierConfirmationNo().trim(), searchObject, fillingObject, XMLSelectFlight, ReportPrinter, Reportcount);

																// =====================================RESERVATION REPORT VALIDATION=====================================
																try {
																	ReportPrinter = validateReports.validateReservationReportAfterCancel(driver, confirmationpage.getReservationNo(), searchObject, XMLSelectFlight, configuration, ReportPrinter, Reportcount);
																} catch (Exception e) {

																}

																// =========================================PROFIT AND LOSS REPORT========================================
																try {
																	ReportPrinter = validateReports.validateProfitLossReportAfterCancel(driver, cancellationScreen, cancellationReport, confirmationpage.getReservationNo(), searchObject, fillingObject, XMLSelectFlight,
																			configuration, ReportPrinter, Reportcount);
																} catch (Exception e) {

																}

																// ======================================SUPPLIER PAYABLE VALIDATION======================================
																try {
																	ReportPrinter = validateReports.validateThirdPartySuppPayableAfterCancel(driver, confirmationpage.getReservationNo(), confirmationpage.getSupplierConfirmationNo(), searchObject, XMLSelectFlight, configuration, ReportPrinter, Reportcount);
																} catch (Exception e) {

																}

																// =====================================BOOKING LIST REPORT VALIDATION====================================
																try {
																	ReportPrinter = validateReports.validateBookingListReportAfterCancel(driver, confirmationpage.getReservationNo(), searchObject, fillingObject, XMLSelectFlight, configuration, ReportPrinter,
																			Reportcount);
																} catch (Exception e) {

																}

															}
														}
													}
												} else {
													ReportPrinter.append("<td>Not Available</td>" + "<td class='Failed'>Fail</td></tr>");
													Reportcount++;
													ReportPrinter.append("</table>");
													ReportPrinter.append("<br><br><br>");
												}

											}

										} catch (Exception e) {
											e.printStackTrace();
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
										}
									}

								} catch (Exception e) {

								}

							}
						}

					}// End confirmation pass

				}// Price response error handler end

			}// If results not available in results page

			// }//No Notification

			if (results) {
				ReportPrinter.append("<span><center><p class='Hedding0'>XML Links</p></center></span>");
				ReportPrinter.append("<table style=width:100%>" + "<tr><th>XML Type</th>" + "<th>Link</th></tr>");
				if (Repository.LowFareSearchRequest.contains(searchObject.getTracer())) {
					ReportPrinter.append("<tr><td>Low Fare Search Request</td>" + "<td><a href=" + Repository.LowFareSearchRequest + " target = '_blank' >View XML File</a></td></tr>");
				}
				if (Repository.LowFareSearchResponse.contains(searchObject.getTracer())) {
					ReportPrinter.append("<tr><td>Low Fare Search Response</td>" + "<td><a href=" + Repository.LowFareSearchResponse + " target = '_blank' >View XML File</a></td></tr>");
				}
				if (Repository.ReservationRequest.contains(searchObject.getTracer())) {
					ReportPrinter.append("<tr><td>Reservation Request</td>" + "<td><a href=" + Repository.ReservationRequest + " target = '_blank' >View XML File</a></td></tr>");
				}
				if (Repository.ReservationResponse.contains(searchObject.getTracer())) {
					ReportPrinter.append("<tr><td>Reservation Response</td>" + "<td><a href=" + Repository.ReservationResponse + " target = '_blank' >View XML File</a></td></tr>");
				}
				if (Repository.PrefAirlineSearchRequest.contains(searchObject.getTracer())) {
					ReportPrinter.append("<tr><td>Pref. Airline Search Request</td>" + "<td><a href=" + Repository.PrefAirlineSearchRequest + " target = '_blank' >View XML File</a></td></tr>");
				}
				if (Repository.PrefAirlineSearchResponse.contains(searchObject.getTracer())) {
					ReportPrinter.append("<tr><td>Pref. Airline Search Response</td>" + "<td><a href=" + Repository.PrefAirlineSearchResponse + " target = '_blank' >View XML File</a></td></tr>");
				}
				if (Repository.PriceRequest.contains(searchObject.getTracer())) {
					ReportPrinter.append("<tr><td>Price Request</td>" + "<td><a href=" + Repository.PriceRequest + " target = '_blank' >View XML File</a></td></tr>");
				}
				if (Repository.PriceResponse.contains(searchObject.getTracer())) {
					ReportPrinter.append("<tr><td>Price Response</td>" + "<td><a href=" + Repository.PriceResponse + " target = '_blank' >View XML File</a></td></tr>");
				}
				if (Repository.ETicketRequest.contains(searchObject.getTracer())) {
					ReportPrinter.append("<tr><td>E-Ticket Request</td>" + "<td><a href=" + Repository.ETicketRequest + " target = '_blank' >View XML File</a></td></tr>");
				}
				if (Repository.ETicketResponse.contains(searchObject.getTracer())) {
					ReportPrinter.append("<tr><td>E-Ticket Response</td>" + "<td><a href=" + Repository.ETicketResponse + " target = '_blank' >View XML File</a></td></tr>");
				}
				if (Repository.PNRUpdateRequest.contains(searchObject.getTracer())) {
					ReportPrinter.append("<tr><td>PNR Update Request</td>" + "<td><a href=" + Repository.PNRUpdateRequest + " target = '_blank' >View XML File</a></td></tr>");
				}
				if (Repository.PNRUpdateResponse.contains(searchObject.getTracer())) {
					ReportPrinter.append("<tr><td>PNR Update Response</td>" + "<td><a href=" + Repository.PNRUpdateResponse + " target = '_blank' >View XML File</a></td></tr>");
				}
				if (Repository.Cancellationrequest.contains(webConfirmationPage.getSupplierConfirmationNo())) {
					ReportPrinter.append("<tr><td>Cancellation Request</td>" + "<td><a href=" + Repository.Cancellationrequest + " target = '_blank' >View XML File</a></td></tr>");
				}
				if (Repository.CancellationResponse.contains(webConfirmationPage.getSupplierConfirmationNo())) {
					ReportPrinter.append("<tr><td>Cancellation Response</td>" + "<td><a href=" + Repository.CancellationResponse + " target = '_blank' >View XML File</a></td></tr>");
				}

				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}

			ReportPrinter.append("<span><center><p class='Hedding0'>URLs and Path</p></center></span>");
			ReportPrinter.append("<table style=width:100%>" + "<tr><th>XML Type</th>" + "<th>Link</th></tr>");

			ReportPrinter.append("<tr><td>Portal Name </td>" + "<td>" + propertyMap.get("Portal.Name") + "</td></tr>");
			ReportPrinter.append("<tr><td>Portal Web URL </td>" + "<td>" + propertyMap.get("PortalWEB.Url") + "</td></tr>");

			if (searchObject.getTOBooking().equalsIgnoreCase("YES")) {
				if (searchObject.getToType() == system.enumtypes.TO.NetCash) {
					ReportPrinter.append("<tr><td>User Name TO</td>" + "<td>" + propertyMap.get("NetCash") + "</td></tr>");
				} else if (searchObject.getToType() == system.enumtypes.TO.NetCreditLpoN) {
					ReportPrinter.append("<tr><td>User Name TO</td>" + "<td>" + propertyMap.get("NetCreditLpoN") + "</td></tr>");
				} else if (searchObject.getToType() == system.enumtypes.TO.NetCreditLpoY) {
					ReportPrinter.append("<tr><td>User Name TO</td>" + "<td>" + propertyMap.get("NetCreditLpoY") + "</td></tr>");
				} else if (searchObject.getToType() == system.enumtypes.TO.CommCash) {
					ReportPrinter.append("<tr><td>User Name TO</td>" + "<td>" + propertyMap.get("CommCash") + "</td></tr>");
				} else if (searchObject.getToType() == system.enumtypes.TO.CommCreditLpoN) {
					ReportPrinter.append("<tr><td>User Name TO</td>" + "<td>" + propertyMap.get("CommCreditLpoN") + "</td></tr>");
				} else if (searchObject.getToType() == system.enumtypes.TO.CommCreditLpoY) {
					ReportPrinter.append("<tr><td>User Name TO</td>" + "<td>" + propertyMap.get("CommCreditLpoY") + "</td></tr>");
				}

			} else {
				ReportPrinter.append("<tr><td>User Name DC</td>" + "<td>" + propertyMap.get("portal.username") + "</td></tr>");
			}

			ReportPrinter.append("<tr><td>Flight Supplier </td>" + "<td>" + propertyMap.get("Flight_Supplier") + "</td></tr>");
			ReportPrinter.append("<tr><td>Payment Gateway Currency </td>" + "<td>" + propertyMap.get("Payment_Gateway_Currency_Code") + "</td></tr>");
			ReportPrinter.append("<tr><td>Portal Currency </td>" + "<td>" + propertyMap.get("Portal_Currency_Code") + "</td></tr>");
			ReportPrinter.append("<tr><td>Credit Card Cost Type </td>" + "<td>" + propertyMap.get("CreditCard_Cost_Type") + "</td></tr>");
			ReportPrinter.append("<tr><td>Credit Card Value </td>" + "<td>" + propertyMap.get("CreditCard_Cost") + "</td></tr>");
			ReportPrinter.append("<tr><td>Profit Markup Type </td>" + "<td>" + searchObject.getProfitType() + "</td></tr>");
			ReportPrinter.append("<tr><td>Profit Markup </td>" + "<td>" + searchObject.getProfit() + "</td></tr>");
			ReportPrinter.append("<tr><td>Booking Fee Type </td>" + "<td>" + searchObject.getBookingFeeType() + "</td></tr>");
			ReportPrinter.append("<tr><td>Booking Fee </td>" + "<td>" + searchObject.getBookingFee() + "</td></tr>");

			if (searchObject.isApplyDiscount()) {
				ReportPrinter.append("<tr><td>Discount Booking</td>" + "<td>Yes</td></tr>");
				ReportPrinter.append("<tr><td>Discount Coupon No</td>" + "<td>" + propertyMap.get("DiscountCouponNo") + "</td></tr>");
				ReportPrinter.append("<tr><td>Discount Type</td>" + "<td>" + propertyMap.get("DiscountType") + "</td></tr>");
				ReportPrinter.append("<tr><td>Discount Value</td>" + "<td>" + propertyMap.get("DiscountValue") + "</td></tr>");
			}

			/*
			 * ReportPrinter.append("<tr><td>Cancellation Response</td>"+"<td>"+propertyMap.get("Portal.Name")+"</td></tr>");
			 * ReportPrinter.append("<tr><td>Cancellation Response</td>"+"<td>"+propertyMap.get("Portal.Name")+"</td></tr>");
			 * ReportPrinter.append("<tr><td>Cancellation Response</td>"+"<td>"+propertyMap.get("Portal.Name")+"</td></tr>");
			 */

			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean getTracer(WebDriver driver, SearchObject searchObject, String tracer) {
		boolean results = false;
		WebDriverWait wait = new WebDriverWait(driver, 25);
		try {
			if (searchObject.isSearchAgain()) {
				try {
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id(PropertyMap.get("ResultPg_Tracer_id_1").trim())));
					tracer = driver.findElement(By.id(PropertyMap.get("ResultPg_Tracer_id_1").trim())).getAttribute("value");
					results = true;
				} catch (Exception e) {
					// e.printStackTrace();
				}

			} else {
				try {
					try {
						wait.until(ExpectedConditions.presenceOfElementLocated(By.id(PropertyMap.get("ResultPg_Tracer_id_1").trim())));
						tracer = driver.findElement(By.id(PropertyMap.get("ResultPg_Tracer_id_1").trim())).getAttribute("value");
						results = true;
					} catch (Exception e) {
						// e.printStackTrace();
					}
					if (tracer == null) {
						wait.until(ExpectedConditions.presenceOfElementLocated(By.id(PropertyMap.get("ResultPg_Tracer_id_2").trim())));
						tracer = driver.findElement(By.id(PropertyMap.get("ResultPg_Tracer_id_2").trim())).getAttribute("value");
						results = true;
					}

				} catch (Exception e) {
					// e.printStackTrace();
				}

			}

			searchObject.setTracer(tracer);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return results;
	}

}
