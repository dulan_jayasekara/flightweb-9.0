package system.main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.utilities.ExchangeRateUpdater;
import com.utilities.FillReservationDetails;
import com.utilities.Repository;

import system.enumtypes.*;
import system.classes.*;
import system.supportParents.*;
import system.classes.AirConfig;
import system.classes.IntermediateInfo;
import system.classes.SearchObject;
import system.pages.*;
import Flight.Cancellation.CancellationReport;
import Flight.Cancellation.CancellationScreen;
import Flight.Fare.FareRequest;
import Flight.Fare.FareRequestReader;
import Flight.Fare.FareResponse;
import Flight.Fare.FareResponseReader;
import Flight.Price.PriceResponse;
import Flight.Price.PriceResponseReader;
import Flight.Reservation.ReservationRequest;
import Flight.Reservation.ReservationResponse;
import Flight.Reservation.ResvRequestReader;
import Flight.Reservation.ResvResponseReader;
import Results.Validate.Validate;
import Results.Validate.ValidateCC;
import Results.Validate.ValidateReports;

public class CC {

	HashMap<String, String>			Propertymap			= new HashMap<String, String>();
	//String 							PropfilePath		= "../Flight_Reservation_Details/Properties_Dev3.properties";
	//String 							PropfilePath		= "../Flight_Reservation_Details/Properties_CCBeverly.properties";
	//String 							PropfilePath		= "../Flight_Reservation_Details/Properties_Dev3CMB.properties";
	String							PropfilePath	= "../Flight_Reservation_Details/Properties_ProdCC.properties";
	WebDriver 						driver				= null;
	boolean 						login				= false;
	ArrayList<Map<Integer, String>> XLtestData			= new ArrayList<Map<Integer,String>>();
	ArrayList<Map<Integer, String>> XLReservationData	= new ArrayList<Map<Integer,String>>();
	String 							tracer				= "";
	SupportMethods 					SUP					= null;
	StringBuffer 					ReportPrinter		= null;
	String							intermediateStatus	= "";
	String 							intermediateMessage	= "";
	int 							Reportcount			= 1;
	String 							payAmount			= "";
	String					 		payAmountCurrency	= "";
	String 							PageSource			= "";
	ArrayList<IntermediateInfo>		intermediateList	= new ArrayList<IntermediateInfo>();
	int 							m 					= 1;
	boolean							eticketIssued		= false;
	boolean							results				= false;
	Validate						validate			= null;
	TOperator						instanceTO1			= new TOperator();
	TOperator						instanceTO2			= new TOperator();
	boolean							done				= false;
	IntermediateInfo				intermediateinfo	= new IntermediateInfo();
	Map<String, String> CurrencyMap = new HashMap<String, String>();
	
	public void go(WebDriver driver, SearchObject searchObject, AirConfig configuration, ValidateCC validate, HashMap<String, String> propertyMap, StringBuffer ReportPrinter, int Reportcount) {
		
		try {
			Propertymap = propertyMap;
			Repository.setAll();
			CallCenter CCRun = new CallCenter();
			CCRun.setPropertymap(Propertymap);
			driver							= CCRun.CCRUN(driver, searchObject);	
			SupportMethods	succ			= new SupportMethods();
			PopupMessage	popup1			= new PopupMessage();
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			// SET XML URLS TO NULL
			Repository.setAll();
			IntermediateInfo intermediateinfo = new IntermediateInfo();
			// READ EXCHANGE RATE TABLE TO HASH MAP
			ExchangeRateUpdater rates = new ExchangeRateUpdater();
			try {
				CurrencyMap = rates.getExchangeRates(Propertymap, driver);
			} catch (IOException e) {
				e.printStackTrace();
			}
			// INITIALIZE VALIDATING OBJECT
			validate = new ValidateCC(Propertymap, driver);
			
			if(popup1.getTitle().equals("Notification"))
			{
				
			}
			else
			{
				WebDriverWait wait = new WebDriverWait(driver, 20);
				
				driver.switchTo().defaultContent();
				driver.switchTo().frame("live_message_frame");
				
				String tracer = "null";
				try
				{
					driver.switchTo().defaultContent();
					driver.switchTo().frame("live_message_frame");
					results = getTracer(driver, searchObject, tracer);
					
					ReportPrinter.append("<span><center><p class='Hedding0'>Check Results Availability</p></center></span>");
					ReportPrinter.append("<table style=width:100%>"
					+ "<tr><th>Test Case</th>"
					+ "<th>Test Description</th>"
					+ "<th>Expected Result</th>"
					+ "<th>Actual Result</th>"
					+ "<th>Test Status</th></tr>");
					ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
					+ "<td>Booking Engine Search</td>"
					+ "<td>If Results Available In Results Page - True</td>");
					if(results)
					{
						ReportPrinter.append("<td>"+results+"</td>"
						+ "<td class='Passed'>PASS</td></tr>");
						Reportcount++;
					}
					else
					{
						ReportPrinter.append("<td>"+results+"</td>"
						+ "<td class='Failed'>Fail</td></tr>");
						Reportcount++;
					}
					
					if(results && searchObject.isSearchAgain())
					{
						try {
							done = CCRun.searchAgain(driver, searchObject);
						} catch (Exception e1) {
							//e1.printStackTrace();
						}
						
						if(done)
						{
							try {
								results = getTracer(driver, searchObject, tracer);
							} catch (Exception e) {
								System.out.println("getTracer failed");
							}
							
							ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
							+ "<td>Search Again Search</td>"
							+ "<td>If Results Not Available In Results Page - False</td>");
							if(results)
							{
								ReportPrinter.append("<td>"+results+"</td>"
								+ "<td class='Passed'>PASS</td></tr>");
								Reportcount++;
							}
							else
							{
								ReportPrinter.append("<td>"+results+"</td>"
								+ "<td class='Failed'>Fail</td></tr>");
								Reportcount++;
							}
						}
					}
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
				}
				catch(Exception e)
				{
					e.printStackTrace();
					TakesScreenshot screen = (TakesScreenshot)driver;
					FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Test Case "+(m-1)+"- Unexpected.jpg"));
				}
				
				FareRequestReader farereqreader		= new FareRequestReader(Propertymap);
				FareResponseReader fareresreader	= new FareResponseReader(Propertymap);
				
				XMLFileType ReqType = null;
				XMLFileType ResType = null;
				
				if(!searchObject.getPreferredAirline().trim().equals("NONE"))
				{
					ReqType = XMLFileType.AIR1_PrefAirlineSearchRequest_;
					ResType = XMLFileType.AIR1_PrefAirlineSearchResponse_;
				}
				else
				{
					ReqType = XMLFileType.AIR1_LowFareSearchRequest_;
					ResType = XMLFileType.AIR1_LowFareSearchResponse_;
				}
				
				if(!results)
				{
					ReportPrinter.append("<span><center><p class='Hedding0'>No Results Notification Validation</p></center></span>");
					ReportPrinter.append("<table style=width:100%>"
					+ "<tr><th>Test Case</th>"
					+ "<th>Test Description</th>"
					+ "<th>Expected Result</th>"
					+ "<th>Actual Result</th>"
					+ "<th>Test Status</th></tr>");
					
					ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
					+ "<td>Check proper implementation of No Results Notification</td>"
					+ "<td>No Results Available</td>");
					
					PopupMessage popupErrorFareRes = new PopupMessage();
					try {
						popupErrorFareRes = succ.popupHandler(driver);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					if( (!popupErrorFareRes.getMessage().equals("")) )
					{
						ReportPrinter.append("<td>"+popupErrorFareRes.getMessage()+"</td>"
						+ "<td class='Passed'>PASS</td></tr>");
						Reportcount++;
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");
					}
					else
					{
						ReportPrinter.append("<td>"+popupErrorFareRes.getMessage()+"</td>"
						+ "<td class='Passed'>PASS</td></tr>");
						Reportcount++;
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");
					}
				}
				
				if(results)
				{
					//=========================================LOWFARE REQUEST XML VALIDATION==============================================
					
					//ValidateFilters filters = new ValidateFilters(Propertymap);
					//filters.validateAllFilters(driver, ReportPrinter, Reportcount, searchObject.getTriptype());
					
					FareRequest fareRequest = new FareRequest();
					try {
						fareRequest = farereqreader.RequestReader(XMLLocateType.TRACER, searchObject.getTracer(), ReqType);	
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					ReportPrinter.append("<span><center><p class='Hedding0'>LowFareRequest XML Validation</p></center></span>");
						ReportPrinter.append("<table style=width:100%>"
						+ "<tr><th>Test Case</th>"
						+ "<th>Test Description</th>"
						+ "<th>Expected Result</th>"
						+ "<th>Actual Result</th>"
						+ "<th>Test Status</th></tr>");
					
						try {
							ReportPrinter = validate.validateFareRequest(searchObject, fareRequest, ReportPrinter );
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");
					
					
					//==============================================LOWFARE RESPONSE VALIDATION===============================================
					
					FareResponse response = new FareResponse();
					try
					{
						ReportPrinter.append("<span><center><p class='Hedding0'>Validation For LowFareResponse XML Errors</p></center></span>");
						ReportPrinter.append("<table style=width:100%>"
						+ "<tr><th>Test Case</th>"
						+ "<th>Test Description</th>"
						+ "<th>Expected Result</th>"
						+ "<th>Actual Result</th>"
						+ "<th>Test Status</th></tr>");
						
						try {
							response = fareresreader.ResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), ResType);
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
						+ "<td>Check for errors in Low Fare Response XML</td>"
						+ "<td>If error occured - Error Message should be displayed, Else - Available</td>");
						
						if(response.getError().getErrorMessage().equals(""))
						{
							ReportPrinter.append("<td>Available</td>"
							+ "<td class='Passed'>PASS</td></tr>");
							Reportcount++;
							ReportPrinter.append("</table>");
							ReportPrinter.append("<br><br><br>");
						}
						else
						{
							ReportPrinter.append("<td>"+response.getError().getErrorMessage()+"</td>"
							+ "<td class='Failed'>Fail</td></tr>");
							Reportcount++;
							ReportPrinter.append("</table>");
							ReportPrinter.append("<br><br><br>");
						}
					}
					catch(Exception e)
					{
						e.printStackTrace();
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");
					}
					
					
					//==================================RESULTS WITH LOWFARE RESPONSE XML VALIDATION=================================================
					
					ArrayList<UIFlightItineraryCC> CCResultlist	= new ArrayList<UIFlightItineraryCC>();
					CCResultlist								= CCRun.getResults(driver, searchObject);
					
					/*if(searchObject.isValidate())
					{*/
						ReportPrinter.append("<span><center><p class='Hedding0'>Results Validation with LowFareResponse XML</p></center></span>");	
						ReportPrinter.append("<table style=width:100%>"
						+ "<tr><th>Test Case</th>"
						+ "<th>Test Description</th>"
						+ "<th>Expected Result</th>"
						+ "<th>Actual Result</th>"
						+ "<th>Test Status</th></tr>");
						try {
							ReportPrinter = validate.validateFareResponse(CCResultlist, response, searchObject, configuration, ReportPrinter );
						} catch (Exception e) {
							
						}
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");
					/*}*/
					
					//============================================CART VALIDATION===============================================================
					
					String				selection		= searchObject.getSelectingFlight();
					int					selectFlight	= Integer.parseInt(selection);
						
					PopupMessage		popupaddtocart	= new PopupMessage();
					try
					{
						Thread.sleep(3000);
 						
 						if(searchObject.isRemovecartStatus())
 						{
 							popupaddtocart	= succ.addToCartCC(driver, CCResultlist.size(), selectFlight);
 							boolean status	= succ.removeFromCartCC(driver);
 							ReportPrinter.append("<span><center><p class='Hedding0'>Remove from cart functionality test</p></center></span>");	
 							ReportPrinter.append("<table style=width:100%>"
 							+ "<tr><th>Test Case</th>"
 							+ "<th>Test Description</th>"
 							+ "<th>Expected Result</th>"
 							+ "<th>Actual Result</th>"
 							+ "<th>Test Status</th></tr>");
 							ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
 							+ "<td>Remove from cart functionality test</td>"
 							+ "<td>True</td>");
 							if(status)
 							{
 								ReportPrinter.append("<td>"+status+"</td>"
 								+ "<td class = 'Passed'>PASS</td></tr>");
 								Reportcount++;
 								popupaddtocart	= succ.addToCartCC(driver, CCResultlist.size(), selectFlight);
 							}
 							else
 							{
 								ReportPrinter.append("<td>"+status+"</td>"
 								+ "<td class = 'Failed'>Fail</td></tr>");
 								Reportcount++;
 							}
 							ReportPrinter.append("</table>");
 							ReportPrinter.append("<br><br><br>");
 							
 						}
 						else
 						{
 							popupaddtocart	= succ.addToCartCC(driver, CCResultlist.size(), selectFlight);
 						}
 						
					} catch (Exception e) {
						e.printStackTrace();
					}	
					
					XMLPriceItinerary	XMLSelectFlight	= response.getList().get(selectFlight-1);
					PriceResponseReader priceresreader	= new PriceResponseReader(Propertymap);
					PriceResponse		priceresponse1	= priceresreader.AmadeusPriceResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_PriceResponse_);
						
					ReportPrinter.append("<span><center><p class='Hedding0'>Error Validation for PriceResponse XML Errors</p></center></span>");
					ReportPrinter.append("<table style=width:100%>"
					+ "<tr><th>Test Case</th>"
					+ "<th>Test Description</th>"
					+ "<th>Expected Result</th>"
					+ "<th>Actual Result</th>"
					+ "<th>Test Status</th></tr>");
						
					ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
					+ "<td>If error occured in Price Response XML</td>"
					+ "<td>If error occured - Error Message should be displayed, Else - empty</td>");
						
					if(!priceresponse1.getError().getErrorMessage().equals(""))
					{
						ReportPrinter.append("<td>"+priceresponse1.getError().getErrorMessage()+"</td>"
						+ "<td class='Failed'>Fail</td></tr>");
						Reportcount++;
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");
					}
					else
					{		
						ReportPrinter.append("<td>"+priceresponse1.getError().getErrorMessage()+"</td>"
						+ "<td class='Passed'>Pass</td></tr>");
						Reportcount++;
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");
						
						
						CartFlightCC cart	= new CartFlightCC();
						boolean ischange	= false;
						/*if(searchObject.isValidate())
						{*/
							try {
								ischange	= validate.Ispricechanged(priceresponse1.getPriceinfo(), XMLSelectFlight.getPricinginfo());
							} catch (Exception e) {
								
							}
						/*}*/
						//05-11-2014 ADD TO CART PRICE CHANGE
						//if(ischange && popupaddtocart.getMessage().contains("Alert! Rates have being changed from"))
						//{
						XMLSelectFlight.setPricinginfo(priceresponse1.getPriceinfo());
						XMLSelectFlight.getPricinginfo().setIschanged(ischange);	
						//}
						//XMLSelectFlight.getPricinginfo().setIschanged(ischange);
							
						ReportPrinter.append("<span><center><p class='Hedding0'>Price Validation While Add To Cart</p></center></span>");
						ReportPrinter.append("<table style=width:100%>"
						+ "<tr><th>Test Case</th>"
						+ "<th>Test Description</th>"
						+ "<th>Expected Result</th>"
						+ "<th>Actual Result</th>"
						+ "<th>Test Status</th></tr>");
								
						ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
						+ "<td>If Prices have changed actual must give an alert message</td>"
						+ "<td>Error/Notification</td>");
								
						if(!popupaddtocart.getTitle().equals("") && ischange)
						{
							ReportPrinter.append("<td>Message : "+popupaddtocart.getMessage()+" Price Changed : "+ischange+"</td>"
							+ "<td class='Passed'>PASS</td></tr>");
							Reportcount++;
						}
						else if(!popupaddtocart.getTitle().equals("") && !ischange)
						{
							ReportPrinter.append("<td>Message : "+popupaddtocart.getMessage()+" Price Changed : "+ischange+"</td>"
							+ "<td class='Failed'>Fail</td></tr>");
							Reportcount++;
						}
						else if(ischange)
						{
							if(popupaddtocart.getTitle().equals(""))
							{
								ReportPrinter.append("<td>No popup but prices have changed!</td>"
								+ "<td class='Failed'>Fail</td></tr>");
								Reportcount++;
							}
						}
						else if(!ischange==false)
						{
							if(!popupaddtocart.getTitle().equals(""))
							{
								ReportPrinter.append("<td>Popup displayed but prices are same!</td>"
								+ "<td class='Failed'>Fail</td></tr>");
								Reportcount++;
							}
						}
						else
						{
							ReportPrinter.append("<td>Prices have not changed</td>"
							+ "<td class = 'Passed'>PASS</td></tr>");
							Reportcount++;
						}
							
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");
						
						try {
							cart		= CCRun.getCCCart(driver, searchObject.getTriptype());
						} catch (Exception e) {
							
						}
								
						/*if(searchObject.isValidate())
						{*/
							XMLSelectFlight	= validate.getNewPrice(cart.getCurrencyCode(), priceresponse1.getPriceinfo(), XMLSelectFlight, searchObject, configuration);
							
							ReportPrinter.append("<span><center><p class='Hedding0'>Cart Item Details Validation</p></center></span>");
							ReportPrinter.append("<table style=width:100%>"
							+ "<tr><th>Test Case</th>"
							+ "<th>Test Description</th>"
							+ "<th>Expected Result</th>"
							+ "<th>Actual Result</th>"
							+ "<th>Test Status</th></tr>");
							try {
								ReportPrinter = validate.validateCartCC(cart, priceresponse1.getPriceinfo(), XMLSelectFlight, searchObject, configuration, ReportPrinter);
							} catch (Exception e) {
								
							}
							ReportPrinter.append("</table>");
							ReportPrinter.append("<br><br>");
						/*}*/
						
						//======================================================CHECKOUT==============================================================
						
						driver.findElement(By.id("loadpayment")).click();
						ReportPrinter.append("<span><center><p class='Hedding0'>Unexpected Error Validation While Checkout Cart Item</p></center></span>");
						ReportPrinter.append("<table style=width:100%>"
						+ "<tr><th>Test Case</th>"
						+ "<th>Test Description</th>"
						+ "<th>Expected Result</th>"
						+ "<th>Actual Result</th>"
						+ "<th>Test Status</th></tr>");
						ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
						+ "<td>Check blocker errors that prevents checkout</td>"
						+ "<td>If error occurred - Error Message should be displayed</td>");
						
						PopupMessage popup2 = new PopupMessage();
						try {
							if(/*!driver.findElement(By.id("status_F_AIR1")).isDisplayed() || */!driver.findElement(By.id("user_id")).isDisplayed() || !driver.findElement(By.id("dialog-alert-message-WJ_22")).isDisplayed())
							{		
								if(driver.findElement(By.id("dialog-alert-message-WJ_22")).isDisplayed())
								{
									String alert = driver.findElement(By.id("dialog-alert-message-WJ_22")).getText();
									if(!alert.contains("The discount has been applied according to the conditions"))
									{
										popup2				= SUP.popupHandler(driver);
									}
								}
								
							}
						} catch (Exception e) {
							
						}
						if(!popup2.getMessage().equals(""))
						{
							ReportPrinter.append("<td>Popup error message : "+popup2.getMessage()+"</td>"
							+ "<td class='Failed'>Fail</td></tr>");
							Reportcount++;
							ReportPrinter.append("</table>");
							ReportPrinter.append("<br><br><br>");
						}
						else
						{
							ReportPrinter.append("<td>No Errors</td>"
							+ "<td class='Passed'>PASS</td></tr>");
							Reportcount++;
							ReportPrinter.append("</table>");
							ReportPrinter.append("<br><br><br>");
						}
						
						//================================================FILL PAYMENT PAGE UI DETAILS=================================================
						
						wait.until(ExpectedConditions.presenceOfElementLocated(By.id("user_id")));
						try
						{
							if(searchObject.isApplyDiscount())
							{
								if(driver.findElement(By.id("dialog-alert-message-WJ_22")).isDisplayed())
								{
									String alert = "";
									alert = driver.findElement(By.id("dialog-alert-message-WJ_22")).getText();
									if( alert.equalsIgnoreCase(" The discount has been applied according to the conditions. The booking values are updated accordingly. ") ||
										alert.contains("The discount has been applied according to the conditions. The booking values are updated accordingly.") )
									{
										XMLSelectFlight.getPricinginfo().setDiscount(Propertymap.get("DiscountType"), Propertymap.get("DiscountValue"), Propertymap.get("Portal_Currency_Code"), CurrencyMap);
										driver.findElement(By.className("ui-button-text")).click();
									}
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						FillReservationDetails	fill			= new FillReservationDetails();
						ReservationInfo			fillingObject	= new ReservationInfo();
						fillingObject							= fill.Fill_Reservation_details(searchObject);
						UIPaymentPage			paymentpage		= new UIPaymentPage();
						try {
							paymentpage	= CCRun.getPaymentPage(driver, XMLSelectFlight, searchObject);
							
							ReportPrinter.append("<span><center><p class='Hedding0'>Payment Page Details Validation</p></center></span>");
							ReportPrinter.append("<table style=width:100%>"
							+ "<tr><th>Test Case</th>"
							+ "<th>Test Description</th>"
							+ "<th>Expected Result</th>"
							+ "<th>Actual Result</th>"
							+ "<th>Test Status</th></tr>");
							try {
								ReportPrinter = validate.validateCCPaymentPage(paymentpage, XMLSelectFlight, searchObject, configuration, ReportPrinter);
							} catch (Exception e) {
								
							}
							ReportPrinter.append("</table>");
							ReportPrinter.append("<br><br><br>");
								
							driver		= CCRun.Fill_Reservation_Details(driver, fillingObject, searchObject);
							
						} catch (Exception e) {
							e.printStackTrace();
						}
								
						ReportPrinter.append("<span><center><p class='Hedding0'>Unexpected Error Validation While Checkout Cart Item</p></center></span>");
						ReportPrinter.append("<table style=width:100%>"
						+ "<tr><th>Test Case</th>"
						+ "<th>Test Description</th>"
						+ "<th>Expected Result</th>"
						+ "<th>Actual Result</th>"
						+ "<th>Test Status</th></tr>");
						ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
						+ "<td>Check blocker errors that prevents checkout</td>"
						+ "<td>If error occurred - Error Message should be displayed</td>");
									
						PopupMessage popup3	= new PopupMessage();
						try {
							if(!driver.findElement(By.id("status_F_AIR1")).getText().trim().equalsIgnoreCase("Available"))
							{
								popup2				= succ.popupHandler(driver);////////////////////////////#$%#@
							}
						} catch (Exception e) {
							System.out.println("");
						}
						
								
						if(!popup3.getMessage().equals(""))
						{
							ReportPrinter.append("<td>Popup error message : "+popup3.getMessage()+"</td>"
							+ "<td class='Failed'>Fail</td></tr>");
							Reportcount++;
							ReportPrinter.append("</table>");
							ReportPrinter.append("<br><br><br>");
						}
						else
						{
							ReportPrinter.append("<td>No Errors</td>"
							+ "<td class='Passed'>PASS</td></tr>");
							Reportcount++;
							ReportPrinter.append("</table>");
							ReportPrinter.append("<br><br><br>");
							
							//==============================================PAYMENT PAGE VALIDATION===============================================
							
							if(!searchObject.isQuotation())
							{	
								//=====================================CLICK PROCEED BUTTON IN PAYMENT PAGE==========================================
										
								try
								{		
									wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("status_F_AIR1")));
									intermediateStatus = driver.findElement(By.id("status_F_AIR1")).getText();
									intermediateinfo.setIntermediateStatus(intermediateStatus);
									intermediateMessage = driver.findElement(By.id("message_F_AIR1")).getText();
									intermediateinfo.setIntermediateMessage(intermediateMessage);

									driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
									WebDriverWait wait2 = new WebDriverWait(driver, 30);
									
									try
									{
										driver.findElement(By.id("confirmbooking")).click();
									}
									catch(Exception e)
									{
										e.printStackTrace();
										try {
											driver.findElement(By.className("proceed_btn")).click();
										} catch (Exception e2) {
											
										}
									}
									
									if(searchObject.getPaymentMode().equalsIgnoreCase("Pay Online"))
									{
										try
										{
											driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
											driver.switchTo().frame("paygatewayFrame");
											wait2.until(ExpectedConditions.presenceOfElementLocated(By.id("cardnumberpart1")));
											((JavascriptExecutor)driver).executeScript("$('#cardnumberpart1').val('"+Propertymap.get("CardNo1")+"');");
											((JavascriptExecutor)driver).executeScript("$('#cardnumberpart2').val('"+Propertymap.get("CardNo2")+"');");
											((JavascriptExecutor)driver).executeScript("$('#cardnumberpart3').val('"+Propertymap.get("CardNo3")+"');");
											((JavascriptExecutor)driver).executeScript("$('#cardnumberpart4').val('"+Propertymap.get("CardNo4")+"');");
		
											new Select(driver.findElement(By.id("cardexpmonth"))).selectByVisibleText(Propertymap.get("CardMonth"));
											new Select(driver.findElement(By.id("cardexpyear"))).selectByVisibleText(Propertymap.get("CardYear"));
											driver.findElement(By.id("cardholdername")).sendKeys(Propertymap.get("CardHolder"));
											((JavascriptExecutor)driver).executeScript("$('#cv2').val("+Propertymap.get("CardHolderSSN")+");");
										
											payAmountCurrency = driver.findElement(By.xpath("/html/body/div/div/form/div[1]/div[6]/div[1]/h3")).getText();
											intermediateinfo.setPayAmountCurrency(payAmountCurrency);
											payAmount = driver.findElement(By.xpath("/html/body/div/div/form/div[1]/div[6]/div[2]")).getText();
											intermediateinfo.setPayAmount(payAmount);
										
											try
											{
												driver.findElement(By.className("proceed_btn")).click();
											}
											catch (Exception e)
											{
												e.printStackTrace();
												try
												{
													((JavascriptExecutor)driver).executeScript("javascript:submit_page()");
												}
												catch (Exception e2)
												{
													e.printStackTrace();
												}
											}
											
											driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
											PageSource = driver.getPageSource();
											intermediateinfo.setCreditCPaySumeryPage(PageSource);
											intermediateList.add(intermediateinfo);
										
											try {
												wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[2]/td[2]/b/input[1]"))));
												if(driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[2]/td[2]/b/input[1]")).isDisplayed())
												{
													driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[2]/td[2]/b/input[1]")).sendKeys("1234");
													//((JavascriptExecutor) driver).executeScript("submitForm()")
													driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[4]/td[2]/b/input")).click();
												}
											} catch (Exception e) {
												e.printStackTrace();
											}
											
										}
										catch(Exception e)
										{
											e.printStackTrace();
										}
									}
									
									
									PopupMessage popupatCreditPay	= new PopupMessage();
									try {
										if(!driver.findElement(By.id("p_flight_booking_reservation_no_1")).isDisplayed())
										{
											popupatCreditPay				= succ.popupHandler(driver);
										}
									} catch (Exception e) {
										
									}
									
									ReportPrinter.append("<span><center><p class='Hedding0'>Error Validation For Unexpected Blocker Issues while Payment</p></center></span>");
									ReportPrinter.append("<table style=width:100%>"
									+ "<tr><th>Test Case</th>"
									+ "<th>Test Description</th>"
									+ "<th>Expected Result</th>"
									+ "<th>Actual Result</th>"
									+ "<th>Test Status</th></tr>");
									ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
									+ "<td>Test for payment gateway issues</td>"
									+ "<td>If error occured - Error Message should be displayed</td>");
									
									if( !popupatCreditPay.getMessage().equals("") && !popupatCreditPay.getMessage().contains("Unable to create the E-Ticket") )
									{
										ReportPrinter.append("<td>Popup error message : "+popupatCreditPay.getMessage()+"</td>"
										+ "<td class='Failed'>Fail</td></tr>");
										Reportcount++;
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
									
										ResvRequestReader	resvRequestReader1	= new ResvRequestReader(Propertymap);
										ReservationRequest	reservationReq1		= new ReservationRequest();
										try {
											reservationReq1		= resvRequestReader1.RequestReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ReservationRequest_);
										} catch (Exception e) {
											
										}
										ReportPrinter.append("<span><center><p class='Hedding0'>Reservation Request XML Validation</p></center></span>");
										ReportPrinter.append("<table style=width:100%>"
										+ "<tr><th>Test Case</th>"
										+ "<th>Test Description</th>"
										+ "<th>Expected Result</th>"
										+ "<th>Actual Result</th>"
										+ "<th>Test Status</th></tr>");
										ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
										+ "<td>Check Availability of Reservation Request XML</td>"
										+ "<td>Available</td>");
										
										if(reservationReq1.isAvailable())
										{
											ReportPrinter.append("<td>Available</td>"
											+ "<td class='Passed'>PASS</td></tr>");
											Reportcount++;
											/*if(searchObject.isValidate())
											{*/
												try {
													ReportPrinter = validate.validateResvRequest(reservationReq1, fillingObject, XMLSelectFlight, ReportPrinter);
												} catch (Exception e) {
													
												}
											/*}*/
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
										}
										else
										{
											ReportPrinter.append("<td>Not Available</td>"
											+ "<td class='Failed'>Fail</td></tr>");
											Reportcount++;
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
										}
										
										ResvResponseReader resvResponseReader1	= new ResvResponseReader(Propertymap);
										ReservationResponse reservationResp1	= new ReservationResponse();
										try {
											reservationResp1	= resvResponseReader1.ResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ReservationResponse_);
										} catch (Exception e) {
											
										}
										
										
										ReportPrinter.append("<span><center><p class='Hedding0'>Check for Reservation Response XML Error Caused for Blocker</p></center></span>");
										ReportPrinter.append("<table style=width:100%>"
										+ "<tr><th>Test Case</th>"
										+ "<th>Test Description</th>"
										+ "<th>Expected Result</th>"
										+ "<th>Actual Result</th>"
										+ "<th>Test Status</th></tr>");
										
										ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
										+ "<td>Error occured in Reservation Response XML</td>"
										+ "<td>Error Message should be displayed</td>");
										
										if( !reservationResp1.isAvailable() )
										{
											if( !reservationResp1.getError().getErrorMessage().equals("") )
											{
												ReportPrinter.append("<td>XML error message : "+reservationResp1.getError().getErrorMessage()+"</td>"
												+ "<td class='Passed'>PASS</td></tr>");
												Reportcount++;
												
												ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
												+ "<td>XA Popup message should be displayed in the system if an error occured in Reservation Response XML</td>"
												+ "<td>Displayed - TRUE</td>");
												if( !popupatCreditPay.getMessage().equals("") )
												{
													ReportPrinter.append("<td>TRUE</td>"
													+ "<td class='Passed'>PASS</td></tr>");
													Reportcount++;
												}
												else
												{
													ReportPrinter.append("<td>FALSE</td>"
													+ "<td class='Failed'>Fail</td></tr>");
													Reportcount++;
												}	
													
												ReportPrinter.append("</table>");
												ReportPrinter.append("<br><br><br>");
											}
											else
											{
												ReportPrinter.append("</table>");
												ReportPrinter.append("<br><br><br>");
											}
										}
										else
										{
											ReportPrinter.append("<td>XML error message : NO ERROR IN RESPONSE XML</td>"
											+ "<td class='Passed'>PASS</td></tr>");
											Reportcount++;
													
											ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
											+ "<td>If Popup message displayed in the system without any error occurred in Reservation Response XML</td>"
											+ "<td>Displayed - TRUE</td>");
											if( !popupatCreditPay.getMessage().equals("") )
											{
												ReportPrinter.append("<td>TRUE</td>"
												+ "<td class='Passed'>PASS</td></tr>");
												Reportcount++;
											}
											else
											{
												ReportPrinter.append("<td>FALSE</td>"
												+ "<td class='Failed'>Fail</td></tr>");
												Reportcount++;
											}				
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
										}
									}
								else
								{
									if(popupatCreditPay.getMessage().contains("Unable to create the E-Ticket"))
									{
										eticketIssued = false;
									}
									else
									{
										eticketIssued = true;
									}
										
									ReportPrinter.append("<td>No Payment gateway errors</td>"
									+ "<td class='Passed'>PASS</td></tr>");
									Reportcount++;
									ReportPrinter.append("</table>");
									ReportPrinter.append("<br><br><br>");
									
									ResvRequestReader resvRequestReader = new ResvRequestReader(Propertymap);
									try 
									{
										Thread.sleep(6000);
									} catch (InterruptedException e) 
									{
										e.printStackTrace();
									}
									
									ReservationRequest reservationReq = new ReservationRequest();
									try {
										reservationReq = resvRequestReader.RequestReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ReservationRequest_);
									} catch (Exception e) {
										
									}
									
									ReportPrinter.append("<span><center><p class='Hedding0'>ReservationRequest XML Validation</p></center></span>");
									ReportPrinter.append("<table style=width:100%>"
									+ "<tr><th>Test Case</th>"
									+ "<th>Test Description</th>"
									+ "<th>Expected Result</th>"
									+ "<th>Actual Result</th>"
									+ "<th>Test Status</th></tr>");
									ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
									+ "<td>Check Availability of Reservation Request XML</td>"
									+ "<td>Available</td>");
									
									if(reservationReq.isAvailable())
									{
										ReportPrinter.append("<td>Available</td>"
										+ "<td class='Passed'>PASS</td></tr>");
										Reportcount++;
										/*if(searchObject.isValidate())
										{*/
											try {
												ReportPrinter = validate.validateResvRequest(reservationReq, fillingObject, XMLSelectFlight, ReportPrinter);
											} catch (Exception e) {
												
											}
										/*}*/
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
										
										ResvResponseReader resvResponseReader = new ResvResponseReader(Propertymap);
										try 
										{
											Thread.sleep(4000);
										} 
										catch (InterruptedException e) 
										{
											e.printStackTrace();
										}
										
										driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
										
										ReservationResponse reservationResp = new ReservationResponse();
										try {
											reservationResp = resvResponseReader.ResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ReservationResponse_);
										} catch (Exception e) {
											
										}
										
										ReportPrinter.append("<span><center><p class='Hedding0'>Unexpected Error Validation for ReservationResponse XML Errors</p></center></span>");
										ReportPrinter.append("<table style=width:100%>"
										+ "<tr><th>Test Case</th>"
										+ "<th>Test Description</th>"
										+ "<th>Expected Result</th>"
										+ "<th>Actual Result</th>"
										+ "<th>Test Status</th></tr>");
										
										ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
										+ "<td>If error occured in Reservation Response XML</td>"
										+ "<td>If error occured - Error Message should be displayed</td>");
										///////////////////////////////////////////////////////
										PopupMessage PopupResvRespError = succ.popupHandler(driver);
										
										if( !reservationResp.getError().getErrorMessage().equals("") )
										{
											ReportPrinter.append("<td>XML error message : "+reservationResp.getError().getErrorMessage()+"</td>"
											+ "<td class='Failed'>Fail</td></tr>");
											Reportcount++;
												
											ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
											+ "<td>A popup message should be displayed in the system if an error occured in Reservation Response XML</td>"
											+ "<td>If error occurred - Pop up error message content should be displayed</td>");
											if( !PopupResvRespError.getMessage().equals("") )
											{
												ReportPrinter.append("<td>"+PopupResvRespError.getMessage()+"</td>"
												+ "<td class='Failed'>Fail</td></tr>");
												Reportcount++;
											}
											else
											{
												ReportPrinter.append("<td>"+PopupResvRespError.getMessage()+"</td>"
												+ "<td class='Failed'>Fail</td></tr>");
												Reportcount++;
											}	
												
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
										}
										else
										{
											ReportPrinter.append("<td>No errors detected in Reservation Response XML</td>"
											+ "<td class='Passed'>Passed</td></tr>");
											Reportcount++;
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
											
											
											//===================================VALIDATE RESERVATION RESPONSE XML=====================================
											/*if(searchObject.isValidate())
											{*/
												ReportPrinter.append("<span><center><p class='Hedding0'>ReservationResponse XML Validation</p></center></span>");
												ReportPrinter.append("<table style=width:100%>"
												+ "<tr><th>Test Case</th>"
												+ "<th>Test Description</th>"
												+ "<th>Expected Result</th>"
												+ "<th>Actual Result</th>"
												+ "<th>Test Status</th></tr>");
												try {
													ReportPrinter = validate.validateResvResponse(reservationResp, fillingObject, XMLSelectFlight, ReportPrinter);
												} catch (Exception e) {
													
												}
												ReportPrinter.append("</table>");
												ReportPrinter.append("<br><br><br>");
											/*}*/
											
											//====================================VALIDATING PNR AND ETICKET XMLS=======================================
											
											/*//VALIDATE E-TICKET REQUEST XML WITH RESERVATION RESPONSE XML
											EticketRequestReader eTicketReqReader = new EticketRequestReader(Propertymap);
											EticketRequest eTicketRequest = eTicketReqReader.RequestReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ETicketRequest_);
											ReportPrinter.append("<span><center><p class='Hedding0'>E-Ticket Request Validation</p></center></span>");
											ReportPrinter.append("<table style=width:100%>"
											+ "<tr><th>Test Case</th>"
											+ "<th>Test Description</th>"
											+ "<th>Expected Result</th>"
											+ "<th>Actual Result</th>"
											+ "<th>Test Status</th></tr>");
											ReportPrinter = validate.validateEticketRequest(eTicketRequest, confirmationpage, reservationResp, ReportPrinter);
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
											
											//VALIDATE E-TICKET RESPONSE XML WITH E-TICKET REQUEST XML
											EticketResponseReader eTicketResReader = new EticketResponseReader(Propertymap);
											EticketResponse eTicketResponse = eTicketResReader.ResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ETicketResponse_);
											
											ReportPrinter.append("<span><center><p class='Hedding0'>Testing E-Ticker Response XML errors</p></center></span>");
											ReportPrinter.append("<table style=width:100%>"
											+ "<tr><th>Test Case</th>"
											+ "<th>Test Description</th>"
											+ "<th>Expected Result</th>"
											+ "<th>Actual Result</th>"
											+ "<th>Test Status</th></tr>");
											
											ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
											+ "<td>Check for E-ticket response errors</td>"
											+ "<td>If error occured - Error Message should be displayed</td>");
											if( !eTicketResponse.getError().getErrorMessage().equals("") )
											{
												ReportPrinter.append("<td>Popup error message : "+eTicketResponse.getError().getErrorMessage()+"</td>"
												+ "<td class='Passed'>Error occurred and Message displayed - PASS</td></tr>");
												Reportcount++;
												ReportPrinter.append("</table>");
												ReportPrinter.append("<br><br><br>");
											}
											else
											{
												ReportPrinter.append("<td>E-ticket response contains no errors</td>"
												+ "<td>TEST DID NOT OCCUR</td></tr>");
												Reportcount++;
												ReportPrinter.append("</table>");
												ReportPrinter.append("<br><br><br>");
												
												ReportPrinter.append("<span><center><p class='Hedding0'>E-Ticket Response Validation</p></center></span>");
												ReportPrinter.append("<table style=width:100%>"
												+ "<tr><th>Test Case</th>"
												+ "<th>Test Description</th>"
												+ "<th>Expected Result</th>"
												+ "<th>Actual Result</th>"
												+ "<th>Test Status</th></tr>");
												ReportPrinter = validate.validateEticketResponse(eTicketResponse, eTicketRequest, ReportPrinter);
												ReportPrinter.append("</table>");
												ReportPrinter.append("<br><br><br>");	
											}//End E-ticket response error handler*/
											
											//=========================================VALIDATE CONFIRMATION PAGE=======================================
											
											UIConfirmationPage confirmationpage = new UIConfirmationPage();
											try
											{
												try {
													confirmationpage = CCRun.getConfirmationPage(driver, XMLSelectFlight);
												} catch (Exception e) {
													
												}
												
												/*if(searchObject.isValidate())
												{*/
													ReportPrinter.append("<span><center><p class='Hedding0'>Confirmation Page Validation</p></center></span>");
													ReportPrinter.append("<table style=width:100%>"
													+ "<tr><th>Test Case</th>"
													+ "<th>Test Description</th>"
													+ "<th>Expected Result</th>"
													+ "<th>Actual Result</th>"
													+ "<th>Test Status</th></tr>");
													try {
														ReportPrinter = validate.validateConfirmationPageCC(searchObject, confirmationpage, ReportPrinter, fillingObject, XMLSelectFlight/*, reservationResp*/);
													} catch (Exception e) {
														
													}
													
													ReportPrinter.append("</table>");
													ReportPrinter.append("<br><br><br>");
												/*}*/
											}
											catch(Exception e)
											{
												e.printStackTrace();
											}
											
											//=========================================START VALIDATING REPORTS==========================================
											
											if(confirmationpage.isAvailable())
											{
												try {
													// System.out.println(propertyMap.get("portal.username"));
													// System.out.println(propertyMap.get("portal.username"));
													/* boolean log = */SUP.login(driver, propertyMap.get("portal.username"), propertyMap.get("portal.password"));
												} catch (Exception e) {
													System.out.println("TO login failed");
												}

												ValidateReports validateReports = new ValidateReports(propertyMap, validate);

												// ===================================RESERVATION REPORT VALIDATION=======================================

												try {
													ReportPrinter = validateReports.validateReservationReport(driver, confirmationpage.getReservationNo(), ReportPrinter, searchObject, configuration, XMLSelectFlight, Reportcount);
												} catch (Exception e) {

												}

												// ===================================SUPPLIER PAYABLE VALIDATION=======================================

												try {
													ReportPrinter = validateReports.validateSuppPlayableReport(driver, confirmationpage.getReservationNo().trim(), confirmationpage.getSupplierConfirmationNo().trim(), ReportPrinter, searchObject, fillingObject, XMLSelectFlight, configuration, Reportcount)/*																																													 */;
												} catch (Exception e) {

												}

												// =================================CUSTOMER VOUCHER E-MAIL VALIDATION====================================

												try {
													ReportPrinter = validateReports.validateCustomerVoucherEmail(driver, confirmationpage.getReservationNo(), ReportPrinter, searchObject, XMLSelectFlight, fillingObject, configuration, Reportcount);
												} catch (Exception e) {

												}

												// ===================================BOOKING CONFIRMATION VALIDATION======================================

												/*
												 * try { ReportPrinter = validateReports.validateBookingConfReport(driver, webConfirmationPage, searchObject, fillingObject, XMLSelectFlight,
												 * configuration, ReportPrinter, Reportcount); } catch (Exception e) {
												 * 
												 * }
												 */

												// ==================================BOOKING LIST REPORT VALIDATION=========================================

												try {
													ReportPrinter = validateReports.validateBookingListReport(driver, confirmationpage.getReservationNo(), searchObject, fillingObject, XMLSelectFlight, configuration, ReportPrinter, Reportcount);
												} catch (Exception e) {

												}

												// =========================================PROFIT AND LOSS REPORT==========================================

												try {
													ReportPrinter = validateReports.validateProfitLossReport(driver, confirmationpage.getReservationNo(), searchObject, fillingObject, XMLSelectFlight, configuration, ReportPrinter, Reportcount);
												} catch (Exception e) {

												}
													
													//===========================================CANCELLATION PROCESS==========================================
													if(searchObject.getCancellationStatus().equalsIgnoreCase("Yes"))
													{
														CancellationScreen cancellationScreen = new CancellationScreen(Propertymap);
														CancellationReport cancellationReport = new CancellationReport(Propertymap);
														ReportPrinter = validateReports.doCancellation(driver, cancellationScreen, cancellationReport, confirmationpage.getReservationNo(), confirmationpage.getSupplierConfirmationNo(), searchObject, fillingObject, XMLSelectFlight, ReportPrinter, Reportcount);
														
														//=====================================RESERVATION REPORT VALIDATION=====================================
														try {
															ReportPrinter = validateReports.validateReservationReportAfterCancel(driver, confirmationpage.getReservationNo(), searchObject, XMLSelectFlight, configuration, ReportPrinter, Reportcount);
														} catch (Exception e) {
															
														}
														
														//=========================================PROFIT AND LOSS REPORT========================================
														try {	
															ReportPrinter = validateReports.validateProfitLossReportAfterCancel(driver, cancellationScreen, cancellationReport, confirmationpage.getReservationNo(), searchObject, fillingObject, XMLSelectFlight, configuration, ReportPrinter, Reportcount);
														} catch (Exception e) {
															
														}
														
														//======================================SUPPLIER PAYABLE VALIDATION======================================
														try {
															ReportPrinter = validateReports.validateThirdPartySuppPayableAfterCancel(driver, confirmationpage.getReservationNo(), confirmationpage.getSupplierConfirmationNo(), searchObject, XMLSelectFlight, configuration, ReportPrinter, Reportcount);
														} catch (Exception e) {
															
														}
														
														//=====================================BOOKING LIST REPORT VALIDATION====================================
														try {	
															ReportPrinter = validateReports.validateBookingListReportAfterCancel(driver, confirmationpage.getReservationNo(), searchObject, fillingObject, XMLSelectFlight, configuration, ReportPrinter, Reportcount);
														} catch (Exception e) {
															
														}
													}
												}
											}
										}
										else
										{
											ReportPrinter.append("<td>Not Available</td>"
											+ "<td class='Failed'>Fail</td></tr>");
											Reportcount++;
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
										}
									}
								}
								catch(Exception e)
								{
									e.printStackTrace();
									ReportPrinter.append("</table>");
									ReportPrinter.append("<br><br><br>");
								}	
							}
							else
							{
								QuotationPage quotation = new QuotationPage(Propertymap);
								try {
									done = quotation.setQuotationCC(driver, searchObject);
								} catch (Exception e) {
									e.printStackTrace();
								}
								
								if(done)
								{
									ReportPrinter.append("<span><center><p class='Hedding0'>Quotation Page Validation</p></center></span>");
									ReportPrinter.append("<table style=width:100%>"
									+ "<tr><th>Test Case</th>"
									+ "<th>Test Description</th>"
									+ "<th>Expected Result</th>"
									+ "<th>Actual Result</th>"
									+ "<th>Test Status</th></tr>");
									try {
										ReportPrinter = validate.validateQuotationPage(quotation, ReportPrinter, fillingObject, XMLSelectFlight);
									} catch (Exception e) {
										e.printStackTrace();
									}
									ReportPrinter.append("</table>");
									ReportPrinter.append("<br><br><br>");
									
									try {
										QuotationBooking bookQuotation = new QuotationBooking(Propertymap);
										boolean retreival = false;
										retreival = bookQuotation.loadQuotation(driver, searchObject, quotation);
										try {
											boolean x = false;
											try {
												x = driver.findElement(By.id("loadpayment")).isDisplayed();
											} catch (Exception e) {
												System.out.println("loadpayment element displayed");
											}
											
											if(x)
											{
												try {
													wait.until(ExpectedConditions.presenceOfElementLocated(By.id("loadpayment")));
													((JavascriptExecutor)driver).executeScript("JavaScript:loadPaymentPage();");
												} catch (Exception e) {
													e.printStackTrace();
												}
											}
										} catch (Exception e) {
											System.out.println("quotation load fails");
										}
										ReportPrinter.append("<span><center><p class='Hedding0'>Quotation Retrieval</p></center></span>");
										ReportPrinter.append("<table style=width:100%>"
										+ "<tr><th>Test Case</th>"
										+ "<th>Test Description</th>"
										+ "<th>Expected Result</th>"
										+ "<th>Actual Result</th>"
										+ "<th>Test Status</th></tr>");
										ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
										+ "<td>Check quotation retrieval</td>"
										+ "<td>If retrieval pass - true , if fails - false</td>");
										if(retreival)
										{
											ReportPrinter.append("<td>"+retreival+"</td>"
											+ "<td class='Passed'>PASS</td></tr>");
											Reportcount++;
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
										}
										else
										{
											ReportPrinter.append("<td>"+retreival+"</td>"
											+ "<td class='Failed'>Fail</td></tr>");
											Reportcount++;
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
										}
										
										//driver.findElement(By.id("loadpayment")).click();
										ReportPrinter.append("<span><center><p class='Hedding0'>Unexpected Error Validation While Checkout Cart Item</p></center></span>");
										ReportPrinter.append("<table style=width:100%>"
										+ "<tr><th>Test Case</th>"
										+ "<th>Test Description</th>"
										+ "<th>Expected Result</th>"
										+ "<th>Actual Result</th>"
										+ "<th>Test Status</th></tr>");
										ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
										+ "<td>Check blocker errors that prevents checkout</td>"
										+ "<td>If error occurred - Error Message should be displayed</td>");
										
										PopupMessage popupcc = new PopupMessage();
										try {
											if(/*!driver.findElement(By.id("status_F_AIR1")).isDisplayed() || */!driver.findElement(By.id("user_id")).isDisplayed() || !driver.findElement(By.id("dialog-alert-message-WJ_22")).isDisplayed())
											{		
												if(driver.findElement(By.id("dialog-alert-message-WJ_22")).isDisplayed())
												{
													String alert = driver.findElement(By.id("dialog-alert-message-WJ_22")).getText();
													if(!alert.contains("The discount has been applied according to the conditions"))
													{
														popupcc				= SUP.popupHandler(driver);
													}
												}
												
											}
										} catch (Exception e) {
											
										}
										if(!popupcc.getMessage().equals(""))
										{
											ReportPrinter.append("<td>Popup error message : "+popupcc.getMessage()+"</td>"
											+ "<td class='Failed'>Fail</td></tr>");
											Reportcount++;
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
										}
										else
										{
											ReportPrinter.append("<td>No Errors</td>"
											+ "<td class='Passed'>PASS</td></tr>");
											Reportcount++;
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
										}
										
										//wait.until(ExpectedConditions.presenceOfElementLocated(By.id("user_id")));
										try
										{
											if(searchObject.isApplyDiscount())
											{
												if(driver.findElement(By.id("dialog-alert-message-WJ_22")).isDisplayed())
												{
													String alert = "";
													alert = driver.findElement(By.id("dialog-alert-message-WJ_22")).getText();
													if( alert.equalsIgnoreCase(" The discount has been applied according to the conditions. The booking values are updated accordingly. ") ||
														alert.contains("The discount has been applied according to the conditions. The booking values are updated accordingly.") )
													{
														XMLSelectFlight.getPricinginfo().setDiscount(Propertymap.get("DiscountType"), Propertymap.get("DiscountValue"), Propertymap.get("Portal_Currency_Code"), CurrencyMap);
														driver.findElement(By.className("ui-button-text")).click();
													}
												}
											}
										} catch (Exception e) {
											e.printStackTrace();
										}
										
										
										try {
											FillReservationDetails	fillcc			= new FillReservationDetails();
											//@SuppressWarnings("unused")
											ReservationInfo			fillingObjectcc	= new ReservationInfo();
											fillingObjectcc							= fillcc.Fill_Reservation_details(searchObject);
											UIPaymentPage			paymentpageQ	= new UIPaymentPage();

											driver		= CCRun.Fill_Reservation_DetailsQ(driver, fillingObject, searchObject);
											
											try {
												paymentpage	= CCRun.getPaymentPage(driver, XMLSelectFlight, searchObject);
											} catch (Exception e) {
												
											}
											
											ReportPrinter.append("<span><center><p class='Hedding0'>Payment Page Details Validation</p></center></span>");
											ReportPrinter.append("<table style=width:100%>"
											+ "<tr><th>Test Case</th>"
											+ "<th>Test Description</th>"
											+ "<th>Expected Result</th>"
											+ "<th>Actual Result</th>"
											+ "<th>Test Status</th></tr>");
											try {
												ReportPrinter = validate.validateCCPaymentPage(paymentpage, XMLSelectFlight, searchObject, configuration, ReportPrinter);
											} catch (Exception e) {
													
											}
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
										
										} catch (Exception e) {
											e.printStackTrace();
										}
										
										
										ReportPrinter.append("<span><center><p class='Hedding0'>Unexpected Error Validation While Checkout Cart Item</p></center></span>");
										ReportPrinter.append("<table style=width:100%>"
										+ "<tr><th>Test Case</th>"
										+ "<th>Test Description</th>"
										+ "<th>Expected Result</th>"
										+ "<th>Actual Result</th>"
										+ "<th>Test Status</th></tr>");
										ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
										+ "<td>Check blocker errors that prevents checkout</td>"
										+ "<td>If error occurred - Error Message should be displayed</td>");
													
										PopupMessage popupq	= new PopupMessage();
										try {
											if(!driver.findElement(By.id("pre_res_dlgWJ_13")).isDisplayed())
											{
												popup2				= SUP.popupHandler(driver);
											}
										} catch (Exception e) {
											
										}
										
										if(!popup3.getMessage().equals(""))
										{
											ReportPrinter.append("<td>Popup error message : "+popup3.getMessage()+"</td>"
											+ "<td class='Failed'>Fail</td></tr>");
											Reportcount++;
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
										}
										else
										{
											ReportPrinter.append("<td>No Errors</td>"
											+ "<td class='Passed'>PASS</td></tr>");
											Reportcount++;
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
											
											try {
												wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("status_F_AIR1")));
												intermediateStatus = driver.findElement(By.id("status_F_AIR1")).getText();
												intermediateinfo.setIntermediateStatus(intermediateStatus);
												intermediateMessage = driver.findElement(By.id("message_F_AIR1")).getText();
												intermediateinfo.setIntermediateMessage(intermediateMessage);
		
												driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
												WebDriverWait wait2 = new WebDriverWait(driver, 20);
												
												try
												{
													driver.findElement(By.id("confirmbooking")).click();
												}
												catch(Exception e)
												{
													e.printStackTrace();
												}
												
												if(searchObject.getPaymentMode().equalsIgnoreCase("Pay Online"))
												{
													try
													{
														driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
														driver.switchTo().frame("paygatewayFrame");
														wait2.until(ExpectedConditions.presenceOfElementLocated(By.id("cardnumberpart1")));
														/*((JavascriptExecutor)driver).executeScript("$('#cardnumberpart1').val('4111');");
														((JavascriptExecutor)driver).executeScript("$('#cardnumberpart2').val('1111');");
														((JavascriptExecutor)driver).executeScript("$('#cardnumberpart3').val('1111');");
														((JavascriptExecutor)driver).executeScript("$('#cardnumberpart4').val('1111');");
					
														new Select(driver.findElement(By.id("cardexpmonth"))).selectByVisibleText("01");
														new Select(driver.findElement(By.id("cardexpyear"))).selectByVisibleText("2017");
														driver.findElement(By.id("cardholdername")).sendKeys("Dan");
														((JavascriptExecutor)driver).executeScript("$('#cv2').val(123);");*/
														
														((JavascriptExecutor)driver).executeScript("$('#cardnumberpart1').val('"+Propertymap.get("CardNo1")+"');");
														((JavascriptExecutor)driver).executeScript("$('#cardnumberpart2').val('"+Propertymap.get("CardNo2")+"');");
														((JavascriptExecutor)driver).executeScript("$('#cardnumberpart3').val('"+Propertymap.get("CardNo3")+"');");
														((JavascriptExecutor)driver).executeScript("$('#cardnumberpart4').val('"+Propertymap.get("CardNo4")+"');");
					
														new Select(driver.findElement(By.id("cardexpmonth"))).selectByVisibleText(Propertymap.get("CardMonth"));
														new Select(driver.findElement(By.id("cardexpyear"))).selectByVisibleText(Propertymap.get("CardYear"));
														driver.findElement(By.id("cardholdername")).sendKeys(Propertymap.get("CardHolder"));
														((JavascriptExecutor)driver).executeScript("$('#cv2').val("+Propertymap.get("CardHolderSSN")+");");
													
														payAmountCurrency = driver.findElement(By.xpath("/html/body/div/div/form/div[1]/div[6]/div[1]/h3")).getText();
														intermediateinfo.setPayAmountCurrency(payAmountCurrency);
														payAmount = driver.findElement(By.xpath("/html/body/div/div/form/div[1]/div[6]/div[2]")).getText();
														intermediateinfo.setPayAmount(payAmount);
														
														try
														{
															driver.findElement(By.className("proceed_btn")).click();
														}
														catch (Exception e)
														{
															e.printStackTrace();
															try
															{
																((JavascriptExecutor)driver).executeScript("javascript:submit_page()");
															}
															catch (Exception e2)
															{
																e.printStackTrace();
															}
														}
														
														driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
														PageSource = driver.getPageSource();
														intermediateinfo.setCreditCPaySumeryPage(PageSource);
														intermediateList.add(intermediateinfo);
													
														try {
															wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[2]/td[2]/b/input[1]"))));
															if(driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[2]/td[2]/b/input[1]")).isDisplayed())
															{
																driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[2]/td[2]/b/input[1]")).sendKeys("1234");
																//((JavascriptExecutor) driver).executeScript("submitForm()")
																driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[4]/td[2]/b/input")).click();
															}
														} catch (Exception e) {
															e.printStackTrace();
														}
													
													} catch (Exception e) {
														
													}
												}
												
												PopupMessage popupatCreditPay	= new PopupMessage();//123456789123456789
												popupatCreditPay				= SUP.popupHandler(driver);
												
												ReportPrinter.append("<span><center><p class='Hedding0'>Error Validation For Unexpected Blocker Issues while Payment</p></center></span>");
												ReportPrinter.append("<table style=width:100%>"
												+ "<tr><th>Test Case</th>"
												+ "<th>Test Description</th>"
												+ "<th>Expected Result</th>"
												+ "<th>Actual Result</th>"
												+ "<th>Test Status</th></tr>");
												ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
												+ "<td>Test for payment gateway issues</td>"
												+ "<td>If error occured - Error Message should be displayed</td>");
												
												if( !popupatCreditPay.getMessage().equals("") && !popupatCreditPay.getMessage().contains("Unable to create the E-Ticket") )
												{
													ReportPrinter.append("<td>Popup error message : "+popupatCreditPay.getMessage()+"</td>"
													+ "<td class='Failed'>Fail</td></tr>");
													Reportcount++;
													ReportPrinter.append("</table>");
													ReportPrinter.append("<br><br><br>");
														
													ResvRequestReader	resvRequestReader1	= new ResvRequestReader(Propertymap);
													ReservationRequest	reservationReq1		= new ReservationRequest();
													try {
														reservationReq1		= resvRequestReader1.RequestReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ReservationRequest_);
													} catch (Exception e) {
																
													}
													
													ReportPrinter.append("<span><center><p class='Hedding0'>Reservation Request XML Validation</p></center></span>");
													ReportPrinter.append("<table style=width:100%>"
													+ "<tr><th>Test Case</th>"
													+ "<th>Test Description</th>"
													+ "<th>Expected Result</th>"
													+ "<th>Actual Result</th>"
													+ "<th>Test Status</th></tr>");
													ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
													+ "<td>Check Availability of Reservation Request XML</td>"
													+ "<td>Available</td>");
													
													if(reservationReq1.isAvailable())
													{
														ReportPrinter.append("<td>Available</td>"
														+ "<td class='Passed'>PASS</td></tr>");
														Reportcount++;
												
														try {
															ReportPrinter = validate.validateResvRequest(reservationReq1, fillingObject, XMLSelectFlight, ReportPrinter);
														} catch (Exception e) {
																
														}
														
														ReportPrinter.append("</table>");
														ReportPrinter.append("<br><br><br>");
													}
													else
													{
														ReportPrinter.append("<td>Not Available</td>"
														+ "<td class='Failed'>Fail</td></tr>");
														Reportcount++;
														ReportPrinter.append("</table>");
														ReportPrinter.append("<br><br><br>");
													}
													
													ResvResponseReader resvResponseReader1	= new ResvResponseReader(Propertymap);
													ReservationResponse reservationResp1	= new ReservationResponse();
													try {
														reservationResp1	= resvResponseReader1.ResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ReservationResponse_);
													} catch (Exception e) {
														
													}
													
													ReportPrinter.append("<span><center><p class='Hedding0'>Check for Reservation Response XML Error Caused for Blocker</p></center></span>");
													ReportPrinter.append("<table style=width:100%>"
													+ "<tr><th>Test Case</th>"
													+ "<th>Test Description</th>"
													+ "<th>Expected Result</th>"
													+ "<th>Actual Result</th>"
													+ "<th>Test Status</th></tr>");
													
													ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
													+ "<td>Error occured in Reservation Response XML</td>"
													+ "<td>Error Message should be displayed</td>");
													
													if( !reservationResp1.isAvailable() )
													{

														if( !reservationResp1.getError().getErrorMessage().equals("") )
														{
															ReportPrinter.append("<td>XML error message : "+reservationResp1.getError().getErrorMessage()+"</td>"
															+ "<td class='Passed'>PASS</td></tr>");
															Reportcount++;
															
															ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
															+ "<td>XA Popup message should be displayed in the system if an error occured in Reservation Response XML</td>"
															+ "<td>Displayed - TRUE</td>");
															if( !popupatCreditPay.getMessage().equals("") )
															{
																ReportPrinter.append("<td>TRUE</td>"
																+ "<td class='Passed'>PASS</td></tr>");
																Reportcount++;
															}
															else
															{
																ReportPrinter.append("<td>FALSE</td>"
																+ "<td class='Failed'>Fail</td></tr>");
																Reportcount++;
															}	
																
															ReportPrinter.append("</table>");
															ReportPrinter.append("<br><br><br>");
														}
														else
														{
															ReportPrinter.append("</table>");
															ReportPrinter.append("<br><br><br>");
														}
													
													}
													else
													{
														ReportPrinter.append("<td>XML error message : NO ERROR IN RESPONSE XML</td>"
														+ "<td class='Passed'>PASS</td></tr>");
														Reportcount++;
																
														ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
														+ "<td>If Popup message displayed in the system without any error occurred in Reservation Response XML</td>"
														+ "<td>Displayed - TRUE</td>");
														if( !popupatCreditPay.getMessage().equals("") )
														{
															ReportPrinter.append("<td>TRUE</td>"
															+ "<td class='Passed'>PASS</td></tr>");
															Reportcount++;
														}
														else
														{
															ReportPrinter.append("<td>FALSE</td>"
															+ "<td class='Failed'>Fail</td></tr>");
															Reportcount++;
														}				
														ReportPrinter.append("</table>");
														ReportPrinter.append("<br><br><br>");
													}
												}
												else
												{

													if(popupatCreditPay.getMessage().contains("Unable to create the E-Ticket"))
													{
														eticketIssued = false;
													}
													else
													{
														eticketIssued = true;
													}
														
													ReportPrinter.append("<td>No Payment gateway errors</td>"
													+ "<td class='Passed'>PASS</td></tr>");
													Reportcount++;
													ReportPrinter.append("</table>");
													ReportPrinter.append("<br><br><br>");
													
													ResvRequestReader resvRequestReader = new ResvRequestReader(Propertymap);
													try 
													{
														Thread.sleep(6000);
													} catch (InterruptedException e) 
													{
														e.printStackTrace();
													}
													
													ReservationRequest reservationReq = new ReservationRequest();
													try {
														reservationReq = resvRequestReader.RequestReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ReservationRequest_);
													} catch (Exception e) {
														
													}
													
													ReportPrinter.append("<span><center><p class='Hedding0'>ReservationRequest XML Validation</p></center></span>");
													ReportPrinter.append("<table style=width:100%>"
													+ "<tr><th>Test Case</th>"
													+ "<th>Test Description</th>"
													+ "<th>Expected Result</th>"
													+ "<th>Actual Result</th>"
													+ "<th>Test Status</th></tr>");
													ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
													+ "<td>Check Availability of Reservation Request XML</td>"
													+ "<td>Available</td>");
													
													if(reservationReq.isAvailable())
													{
														ReportPrinter.append("<td>Available</td>"
														+ "<td class='Passed'>PASS</td></tr>");
														Reportcount++;
														
														try {
																ReportPrinter = validate.validateResvRequest(reservationReq, fillingObject, XMLSelectFlight, ReportPrinter);
														} catch (Exception e) {
																
														}
														ReportPrinter.append("</table>");
														ReportPrinter.append("<br><br><br>");
														
														ResvResponseReader resvResponseReader = new ResvResponseReader(Propertymap);
														try 
														{
															Thread.sleep(4000);
														} 
														catch (InterruptedException e) 
														{
															e.printStackTrace();
														}
														
														driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
														
														ReservationResponse reservationResp = new ReservationResponse();
														try {
															reservationResp = resvResponseReader.ResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ReservationResponse_);
														} catch (Exception e) {
															
														}
														
														ReportPrinter.append("<span><center><p class='Hedding0'>Unexpected Error Validation for ReservationResponse XML Errors</p></center></span>");
														ReportPrinter.append("<table style=width:100%>"
														+ "<tr><th>Test Case</th>"
														+ "<th>Test Description</th>"
														+ "<th>Expected Result</th>"
														+ "<th>Actual Result</th>"
														+ "<th>Test Status</th></tr>");
														
														ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
														+ "<td>If error occured in Reservation Response XML</td>"
														+ "<td>If error occured - Error Message should be displayed</td>");
														
														PopupMessage PopupResvRespError = SUP.popupHandler(driver);
														
														if( !reservationResp.getError().getErrorMessage().equals("") )
														{
															ReportPrinter.append("<td>XML error message : "+reservationResp.getError().getErrorMessage()+"</td>"
															+ "<td class='Failed'>Fail</td></tr>");
															Reportcount++;
																
															ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
															+ "<td>A popup message should be displayed in the system if an error occured in Reservation Response XML</td>"
															+ "<td>If error occurred - Pop up error message content should be displayed</td>");
															if( !PopupResvRespError.getMessage().equals("") )
															{
																ReportPrinter.append("<td>"+PopupResvRespError.getMessage()+"</td>"
																+ "<td class='Failed'>Fail</td></tr>");
																Reportcount++;
															}
															else
															{
																ReportPrinter.append("<td>"+PopupResvRespError.getMessage()+"</td>"
																+ "<td class='Failed'>Fail</td></tr>");
																Reportcount++;
															}	
																
															ReportPrinter.append("</table>");
															ReportPrinter.append("<br><br><br>");
														}
														else
														{
															ReportPrinter.append("<td>No errors detected in Reservation Response XML</td>"
															+ "<td class='Passed'>Passed</td></tr>");
															Reportcount++;
															ReportPrinter.append("</table>");
															ReportPrinter.append("<br><br><br>");
															
															//===================================VALIDATE RESERVATION RESPONSE XML=====================================
													
															ReportPrinter.append("<span><center><p class='Hedding0'>ReservationResponse XML Validation</p></center></span>");
															ReportPrinter.append("<table style=width:100%>"
															+ "<tr><th>Test Case</th>"
															+ "<th>Test Description</th>"
															+ "<th>Expected Result</th>"
															+ "<th>Actual Result</th>"
															+ "<th>Test Status</th></tr>");
															try {
																ReportPrinter = validate.validateResvResponse(reservationResp, fillingObject, XMLSelectFlight, ReportPrinter);
															} catch (Exception e) {
																	
															}
															ReportPrinter.append("</table>");
															ReportPrinter.append("<br><br><br>");
														
															//====================================VALIDATING PNR AND ETICKET XMLS=======================================
															
															/*//VALIDATE E-TICKET REQUEST XML WITH RESERVATION RESPONSE XML
															EticketRequestReader eTicketReqReader = new EticketRequestReader(Propertymap);
															EticketRequest eTicketRequest = eTicketReqReader.RequestReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ETicketRequest_);
															ReportPrinter.append("<span><center><p class='Hedding0'>E-Ticket Request Validation</p></center></span>");
															ReportPrinter.append("<table style=width:100%>"
															+ "<tr><th>Test Case</th>"
															+ "<th>Test Description</th>"
															+ "<th>Expected Result</th>"
															+ "<th>Actual Result</th>"
															+ "<th>Test Status</th></tr>");
															ReportPrinter = validate.validateEticketRequest(eTicketRequest, confirmationpage, reservationResp, ReportPrinter);
															ReportPrinter.append("</table>");
															ReportPrinter.append("<br><br><br>");
															
															//VALIDATE E-TICKET RESPONSE XML WITH E-TICKET REQUEST XML
															EticketResponseReader eTicketResReader = new EticketResponseReader(Propertymap);
															EticketResponse eTicketResponse = eTicketResReader.ResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ETicketResponse_);
															
															ReportPrinter.append("<span><center><p class='Hedding0'>Testing E-Ticker Response XML errors</p></center></span>");
															ReportPrinter.append("<table style=width:100%>"
															+ "<tr><th>Test Case</th>"
															+ "<th>Test Description</th>"
															+ "<th>Expected Result</th>"
															+ "<th>Actual Result</th>"
															+ "<th>Test Status</th></tr>");
															
															ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
															+ "<td>Check for E-ticket response errors</td>"
															+ "<td>If error occured - Error Message should be displayed</td>");
															if( !eTicketResponse.getError().getErrorMessage().equals("") )
															{
																ReportPrinter.append("<td>Popup error message : "+eTicketResponse.getError().getErrorMessage()+"</td>"
																+ "<td class='Passed'>Error occurred and Message displayed - PASS</td></tr>");
																Reportcount++;
																ReportPrinter.append("</table>");
																ReportPrinter.append("<br><br><br>");
															}
															else
															{
																ReportPrinter.append("<td>E-ticket response contains no errors</td>"
																+ "<td>TEST DID NOT OCCUR</td></tr>");
																Reportcount++;
																ReportPrinter.append("</table>");
																ReportPrinter.append("<br><br><br>");
																
																ReportPrinter.append("<span><center><p class='Hedding0'>E-Ticket Response Validation</p></center></span>");
																ReportPrinter.append("<table style=width:100%>"
																+ "<tr><th>Test Case</th>"
																+ "<th>Test Description</th>"
																+ "<th>Expected Result</th>"
																+ "<th>Actual Result</th>"
																+ "<th>Test Status</th></tr>");
																ReportPrinter = validate.validateEticketResponse(eTicketResponse, eTicketRequest, ReportPrinter);
																ReportPrinter.append("</table>");
																ReportPrinter.append("<br><br><br>");	
															}//End E-ticket response error handler*/
															
															
															//=========================================VALIDATE CONFIRMATION PAGE=======================================
															
															UIConfirmationPage confirmationpage = new UIConfirmationPage();
															try
															{
																try {
																	confirmationpage = CCRun.getConfirmationPage(driver, XMLSelectFlight);
																} catch (Exception e) {
																	
																}
																
																ReportPrinter.append("<span><center><p class='Hedding0'>Confirmation Page Validation</p></center></span>");
																ReportPrinter.append("<table style=width:100%>"
																+ "<tr><th>Test Case</th>"
																+ "<th>Test Description</th>"
																+ "<th>Expected Result</th>"
																+ "<th>Actual Result</th>"
																+ "<th>Test Status</th></tr>");
																try {
																	ReportPrinter = validate.validateConfirmationPageCC(searchObject, confirmationpage, ReportPrinter, fillingObject, XMLSelectFlight);
																} catch (Exception e) {
																		
																}
																	
																ReportPrinter.append("</table>");
																ReportPrinter.append("<br><br><br>");
																
															}
															catch(Exception e)
															{
																e.printStackTrace();
															}
															
															//=========================================START VALIDATING REPORTS==========================================
															
															if(confirmationpage.isAvailable())
															{
																try {
																	System.out.println(Propertymap.get("portal.username"));
																	System.out.println(Propertymap.get("portal.username"));
																	/*boolean log = */SUP.login(driver, Propertymap.get("portal.username"), Propertymap.get("portal.password") );
																} catch (Exception e) {
																	System.out.println("TO login failed");
																}

																//Validate validate = new Validate(Propertymap, driver);
																ValidateReports validateReports = new ValidateReports(Propertymap, validate);
																
																//===================================RESERVATION REPORT VALIDATION=======================================
																
																try {
																	ReportPrinter = validateReports.validateReservationReport(driver, confirmationpage.getReservationNo(), ReportPrinter, searchObject, configuration, XMLSelectFlight, Reportcount);
																} catch (Exception e) {
																	
																}
																
																//===================================SUPPLIER PAYABLE VALIDATION=======================================
																
																try {
																	ReportPrinter = validateReports.validateSuppPlayableReport(driver, confirmationpage.getReservationNo(), confirmationpage.getSupplierConfirmationNo(), ReportPrinter, searchObject, fillingObject, XMLSelectFlight, configuration, Reportcount)/*(confirmationpage, ReportPrinter, searchObject, XMLSelectFlight)*/;
																} catch (Exception e) {
																	
																}
																
																//=================================CUSTOMER VOUCHER E-MAIL VALIDATION====================================
																
																try {	
																	ReportPrinter = validateReports.validateCustomerVoucherEmail(driver, confirmationpage.getReservationNo(), ReportPrinter, searchObject, XMLSelectFlight, fillingObject, configuration, Reportcount);
																} catch (Exception e) {
																	
																}
																
																//===================================BOOKING CONFIRMATION VALIDATION======================================
																
																/*try {	
																	ReportPrinter = validateReports.validateBookingConfReport(driver, confirmationpage, searchObject, fillingObject, XMLSelectFlight, configuration, ReportPrinter, Reportcount);
																} catch (Exception e) {
																	
																}*/
																
																//==================================BOOKING LIST REPORT VALIDATION=========================================
																
																try {	
																	ReportPrinter = validateReports.validateBookingListReport(driver, confirmationpage.getReservationNo(), searchObject, fillingObject, XMLSelectFlight, configuration, ReportPrinter, Reportcount);
																} catch (Exception e) {
																	
																}
																
																//=========================================PROFIT AND LOSS REPORT==========================================
																
																try {	
																	ReportPrinter = validateReports.validateProfitLossReport(driver, confirmationpage.getReservationNo(), searchObject, fillingObject, XMLSelectFlight, configuration, ReportPrinter, Reportcount);
																} catch (Exception e) {
																	
																}
																
																//======================================TOUR OPERATOR PROCESS STARTS=========================================
																if(searchObject.getTOBooking().equalsIgnoreCase("YES"))
																{/*
																	try {
																		if(searchObject.getPaymentMode().equalsIgnoreCase("Pay Offline"))
																		{
																			SUP.login(driver, Propertymap.get("portal.TOusername"), Propertymap.get("portal.TOpassword") );
																			
																			TOBooking TObooking = new TOBooking();
																			
																			//Read Booking List Report after reservation and Issue Voucher
																			TObooking.bookingListReportTOBfinal(driver, confirmationpage.getReservationNo().trim(), ReportPrinter);
																			
																			//Login as the TO 
																			SUP.login(driver, Propertymap.get("portal.TOusername"), Propertymap.get("portal.TOpassword") );
																			
																			//Read TO after issue voucher & validate
																			instanceTO2.setTO(driver);
																			validate.validateTOafterVoucher(instanceTO1, instanceTO2, XMLSelectFlight, searchObject, configuration, ReportPrinter);
																			
																			//Login as the Internal 
																			SUP.login(driver, Propertymap.get("portal.username"), Propertymap.get("portal.password") );
																			
																			//Pay for the reservation
																			String url = "http://dev3.rezg.net/rezbase_v3/reports/operational/mainReport.do?reportId=8&reportName=Payment%20Report";
																			TObooking.pay(driver, confirmationpage, url);
																			
																			//Login as TO
																			SUP.login(driver, Propertymap.get("portal.TOusername"), Propertymap.get("portal.TOpassword") );
																			TObooking.bookingListReportToFinal(driver,confirmationpage, ReportPrinter, instanceTO1, instanceTO2);
																		}
																		
																		//Login as the TO 
																		SUP.login(driver, Propertymap.get("portal.TOusername"), Propertymap.get("portal.TOpassword") );
																		
																		//Read TO after issue payment & validate
																		TOperator intanceTOPaid = new TOperator();
																		intanceTOPaid.setTO(driver);
																		validate.validateTOafterPay(instanceTO1, intanceTOPaid, XMLSelectFlight, searchObject, configuration, ReportPrinter);
																		
																	} catch (Exception e) {
																		System.out.println("TO login failed");
																	}
																	
																*/}//TO "YES"
																
																
																
																//===========================================CANCELLATION PROCESS==========================================
																if(searchObject.getCancellationStatus().equalsIgnoreCase("Yes"))
																{
																	CancellationScreen cancellationScreen = new CancellationScreen(Propertymap);
																	CancellationReport cancellationReport = new CancellationReport(Propertymap);
																	ReportPrinter = validateReports.doCancellation(driver, cancellationScreen, cancellationReport, confirmationpage.getReservationNo(), confirmationpage.getSupplierConfirmationNo(),  searchObject, fillingObject, XMLSelectFlight, ReportPrinter, Reportcount);
																	
																	//=====================================RESERVATION REPORT VALIDATION=====================================
																	try {
																		ReportPrinter = validateReports.validateReservationReportAfterCancel(driver, confirmationpage.getReservationNo(), searchObject, XMLSelectFlight, configuration, ReportPrinter, Reportcount);
																	} catch (Exception e) {
																		
																	}
																	
																	//=========================================PROFIT AND LOSS REPORT========================================
																	try {	
																		ReportPrinter = validateReports.validateProfitLossReportAfterCancel(driver, cancellationScreen, cancellationReport, confirmationpage.getReservationNo(), searchObject, fillingObject, XMLSelectFlight, configuration, ReportPrinter, Reportcount);
																	} catch (Exception e) {
																		
																	}
																	
																	//======================================SUPPLIER PAYABLE VALIDATION======================================
																	try {
																		ReportPrinter = validateReports.validateThirdPartySuppPayableAfterCancel(driver, confirmationpage.getReservationNo(), confirmationpage.getSupplierConfirmationNo(), searchObject, XMLSelectFlight, configuration, ReportPrinter, Reportcount);
																	} catch (Exception e) {
																		
																	}
																	
																	//=====================================BOOKING LIST REPORT VALIDATION====================================
																	try {	
																		ReportPrinter = validateReports.validateBookingListReportAfterCancel(driver, confirmationpage.getReservationNo(), searchObject, fillingObject, XMLSelectFlight, configuration, ReportPrinter, Reportcount);
																	} catch (Exception e) {
																		
																	}
																	
																}
																
															}
														}
													}
													else
													{
														ReportPrinter.append("<td>Not Available</td>"
														+ "<td class='Failed'>Fail</td></tr>");
														Reportcount++;
														ReportPrinter.append("</table>");
														ReportPrinter.append("<br><br><br>");
													}
												
												}
												
												
											} catch (Exception e) {
												e.printStackTrace();
												ReportPrinter.append("</table>");
												ReportPrinter.append("<br><br><br>");
											}
										}
							
									} catch (Exception e) {
										
									}
									
								}
							}
							
						}//End Else blocker errors when checkout
					}
				}

				if(results)
				{
					ReportPrinter.append("<span><center><p class='Hedding0'>XML Links</p></center></span>");
					ReportPrinter.append("<table style=width:100%>"
					+ "<tr><th>XML Type</th>"
					+ "<th>Link</th></tr>");
					
					if(Repository.LowFareSearchRequest.contains(searchObject.getTracer()))
					{
						ReportPrinter.append("<tr><td>Low Fare Search Request</td>"+"<td><a href="+Repository.LowFareSearchRequest+" target = '_blank' >View XML File</a></td></tr>"); 
					}
					if(Repository.LowFareSearchResponse.contains(searchObject.getTracer()))
					{
						ReportPrinter.append("<tr><td>Low Fare Search Response</td>"+"<td><a href="+Repository.LowFareSearchResponse+" target = '_blank' >View XML File</a></td></tr>");
					}
					if(Repository.ReservationRequest.contains(searchObject.getTracer()))
					{
						ReportPrinter.append("<tr><td>Reservation Request</td>"+"<td><a href="+Repository.ReservationRequest+" target = '_blank' >View XML File</a></td></tr>");
					}
					if(Repository.ReservationResponse.contains(searchObject.getTracer()))
					{
						ReportPrinter.append("<tr><td>Reservation Response</td>"+"<td><a href="+Repository.ReservationResponse+" target = '_blank' >View XML File</a></td></tr>");
					}
					if(Repository.PrefAirlineSearchRequest.contains(searchObject.getTracer()))
					{
						ReportPrinter.append("<tr><td>Pref. Airline Search Request</td>"+"<td><a href="+Repository.PrefAirlineSearchRequest+" target = '_blank' >View XML File</a></td></tr>");
					}
					if(Repository.PrefAirlineSearchResponse.contains(searchObject.getTracer()))
					{
						ReportPrinter.append("<tr><td>Pref. Airline Search Response</td>"+"<td><a href="+Repository.PrefAirlineSearchResponse+" target = '_blank' >View XML File</a></td></tr>");
					}
					if(Repository.PriceRequest.contains(searchObject.getTracer()))
					{
						ReportPrinter.append("<tr><td>Price Request</td>"+"<td><a href="+Repository.PriceRequest+" target = '_blank' >View XML File</a></td></tr>");
					}
					if(Repository.PriceResponse.contains(searchObject.getTracer()))
					{
						ReportPrinter.append("<tr><td>Price Response</td>"+"<td><a href="+Repository.PriceResponse+" target = '_blank' >View XML File</a></td></tr>");
					}
					if(Repository.ETicketRequest.contains(searchObject.getTracer()))
					{
						ReportPrinter.append("<tr><td>E-Ticket Request</td>"+"<td><a href="+Repository.ETicketRequest+" target = '_blank' >View XML File</a></td></tr>");
					}
					if(Repository.ETicketResponse.contains(searchObject.getTracer()))
					{
						ReportPrinter.append("<tr><td>E-Ticket Response</td>"+"<td><a href="+Repository.ETicketResponse+" target = '_blank' >View XML File</a></td></tr>");
					}
					if(Repository.PNRUpdateRequest.contains(searchObject.getTracer()))
					{
						ReportPrinter.append("<tr><td>PNR Update Request</td>"+"<td><a href="+Repository.PNRUpdateRequest+" target = '_blank' >View XML File</a></td></tr>");
					}
					if(Repository.PNRUpdateResponse.contains(searchObject.getTracer()))
					{
						ReportPrinter.append("<tr><td>PNR Update Response</td>"+"<td><a href="+Repository.PNRUpdateResponse+" target = '_blank' >View XML File</a></td></tr>");
					}
					
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
				}
				
			}
				
		
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
	}

	public boolean getTracer(WebDriver driver, SearchObject searchObject, String tracer)
	{
		boolean results = false;
		WebDriverWait	wait	= new WebDriverWait(driver, 25);
		try
		{
			Thread.sleep(5000);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("airridetracer_0")));
			
			tracer	= driver.findElement(By.id("airridetracer_0")).getAttribute("value");
			results = true;
			
			searchObject.setTracer(tracer);
		}
		catch(Exception e)
		{
			System.out.println("Inside getTracer error fire");
			//e.printStackTrace();
		}
		
		return results;
	}
	
}
