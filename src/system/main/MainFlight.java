package system.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
//import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.io.output.TeeOutputStream;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import system.enumtypes.SearchType;
import system.enumtypes.TO;
import Results.Validate.Validate;
import Results.Validate.ValidateCC;

import com.utilities.Repository;
import com.utilities.Screenshot;
import com.utilities.ScreenshotPathSingleton;

import system.classes.AirConfiguration;
import system.classes.AirConfig;
import system.classes.SearchObject;
import system.supportParents.ExcelReader;
import system.supportParents.ReadProperties;
import system.supportParents.SupportMethods;


public class MainFlight

 {
	
	HashMap<String, String> propertyMap = new HashMap<String, String>();
	// String propertyPath = "Flight_Reservation_Details/PropertiesWEBCMB.properties";
	// String propertyPath = "Flight_Reservation_Details/PropertiesWEB.properties";
	// String propertyPath = "Flight_Reservation_Details/PropertiesWEBDemo.properties";
	// String propertyPath = "Flight_Reservation_Details/PropertiesTest.properties";
	// String propertyPath = "Flight_Reservation_Details/PropertiesWEBOMAN.properties";
	// String propertyPath = "Flight_Reservation_Details/PropertiesWEBBeverly.properties";
	String propertyPath = "Flight_Reservation_Details/PropertiesWEB_Prod.properties";
	//String propertyPath = "Flight_Reservation_Details/PropertiesWEBET.properties";

	StringBuffer ReportPrinter = null;
	SupportMethods support = null;
	WebDriver driver = null;
	boolean login = false;
	ArrayList<Map<Integer, String>> XLtestData = new ArrayList<Map<Integer, String>>();
	int m = 1;
	Validate validate = null;
	ValidateCC validatecc = null;
	int Reportcount = 1;
	int count = 1;

	@Before
	public void born() {
		try {
			FileOutputStream fos = new FileOutputStream("CONSOLE_OUTPUT.txt");
			//we will want to print in standard "System.out" and in "file"
			TeeOutputStream myOut=new TeeOutputStream(System.out, fos);
			PrintStream ps = new PrintStream(myOut);
			System.setOut(ps);
			
		} catch (Exception e) {
			System.out.println("ERROR EXCEPTION : Class - "+this.getClass().getName() + " Line - "+e.getStackTrace()[0].getLineNumber());
			e.printStackTrace();
		}
		ReportPrinter = new StringBuffer();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

		ReportPrinter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
		ReportPrinter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Flight Reservation - Web (" + sdf.format(Calendar.getInstance().getTime()) + ")</p></div>");
		ReportPrinter.append("<body>");

		try {
			propertyMap = ReadProperties.readpropeties(propertyPath);
			support = new SupportMethods(propertyMap);
		} catch (Exception e) {
			System.out.println("ERROR EXCEPTION : Class - "+this.getClass().getName() + " Line - "+e.getStackTrace()[0].getLineNumber());
			e.printStackTrace();
		}

		System.out.println("INFO -> EXCEL READER");
		ExcelReader Reader = new ExcelReader();
		XLtestData = Reader.init(propertyMap.get("XLPath"));
	}

	@Test
	public void live() {
		String scenarioCommonPath = "";
		String scenarioFailedPath = "";
		scenarioCommonPath = ScreenshotPathSingleton.getInstance().getScenarioCommonPath();
		scenarioFailedPath = ScreenshotPathSingleton.getInstance().getScenarioFailedPath();
		Web web = new Web();
		CC cc = new CC();

		ArrayList<SearchObject> list = getSearchObjList();

		AirConfiguration airconfig = new AirConfiguration(propertyMap);
		System.out.println("INFO -> GET AIR CONFIGURATION LIST");
		ArrayList<AirConfig> configlist = airconfig.getConfigurationList();

		Iterator<SearchObject> listiter = list.iterator();

		while (listiter.hasNext()) {
			SearchObject searchObject = listiter.next();
			if (searchObject.getExcecuteStatus().equalsIgnoreCase("YES")) {
				ScreenshotPathSingleton.getInstance().setScenarioPaths(searchObject.getScenario());
				try {
					driver = support.initalizeDriver();
					System.out.println("INFO -> LOGIN");
					login = support.login(driver, propertyMap.get("portal.username"), propertyMap.get("portal.password"));
				} catch (Exception e) {
					System.out.println("ERROR EXCEPTION : Class - "+this.getClass().getName() + " Line - "+e.getStackTrace()[0].getLineNumber());
					e.printStackTrace();
				}

				if (login) {
					ReportPrinter.append("<span><center><p class='Hedding0'>TEST CASE " + m + " STARTED</p></center></span>");
					ReportPrinter.append("<br><br>");
					
					try {
						System.out.println("INFO -> PRINT SEARCH DETAILS TO REPORT");
						ReportPrinter.append("<span><center><p class='Hedding0'>Search Details</p></center></span>");
						ReportPrinter.append("<table style=width:100%>" + "<tr><th>XML Type</th>" + "<th>Link</th></tr>");

						ReportPrinter.append("<tr><td>Portal Name </td>" + "<td>" + propertyMap.get("Portal.Name") + "</td></tr>");
						ReportPrinter.append("<tr><td>Portal Web URL </td>" + "<td>" + propertyMap.get("PortalWEB.Url") + "</td></tr>");

						if (searchObject.getTOBooking().equalsIgnoreCase("YES")) {
							if (searchObject.getToType() == system.enumtypes.TO.NetCash) {
								ReportPrinter.append("<tr><td>User Name TO</td>" + "<td>" + propertyMap.get("NetCash") + "</td></tr>");
							} else if (searchObject.getToType() == system.enumtypes.TO.NetCreditLpoN) {
								ReportPrinter.append("<tr><td>User Name TO</td>" + "<td>" + propertyMap.get("NetCreditLpoN") + "</td></tr>");
							} else if (searchObject.getToType() == system.enumtypes.TO.NetCreditLpoY) {
								ReportPrinter.append("<tr><td>User Name TO</td>" + "<td>" + propertyMap.get("NetCreditLpoY") + "</td></tr>");
							} else if (searchObject.getToType() == system.enumtypes.TO.CommCash) {
								ReportPrinter.append("<tr><td>User Name TO</td>" + "<td>" + propertyMap.get("CommCash") + "</td></tr>");
							} else if (searchObject.getToType() == system.enumtypes.TO.CommCreditLpoN) {
								ReportPrinter.append("<tr><td>User Name TO</td>" + "<td>" + propertyMap.get("CommCreditLpoN") + "</td></tr>");
							} else if (searchObject.getToType() == system.enumtypes.TO.CommCreditLpoY) {
								ReportPrinter.append("<tr><td>User Name TO</td>" + "<td>" + propertyMap.get("CommCreditLpoY") + "</td></tr>");
							}
						} else {
							ReportPrinter.append("<tr><td>User Name DC</td>" + "<td>" + propertyMap.get("portal.username") + "</td></tr>");
						}

						ReportPrinter.append("<tr><td>Flight Supplier </td>" + "<td>" + propertyMap.get("Flight_Supplier") + "</td></tr>");
						ReportPrinter.append("<tr><td>Payment Gateway Currency </td>" + "<td>" + propertyMap.get("Payment_Gateway_Currency_Code") + "</td></tr>");
						ReportPrinter.append("<tr><td>Portal Currency </td>" + "<td>" + propertyMap.get("Portal_Currency_Code") + "</td></tr>");
						ReportPrinter.append("<tr><td>Credit Card Cost Type </td>" + "<td>" + propertyMap.get("CreditCard_Cost_Type") + "</td></tr>");
						ReportPrinter.append("<tr><td>Credit Card Value </td>" + "<td>" + propertyMap.get("CreditCard_Cost") + "</td></tr>");
						ReportPrinter.append("<tr><td>Profit Markup Type </td>" + "<td>" + searchObject.getProfitType() + "</td></tr>");
						ReportPrinter.append("<tr><td>Profit Markup </td>" + "<td>" + searchObject.getProfit() + "</td></tr>");
						ReportPrinter.append("<tr><td>Booking Fee Type </td>" + "<td>" + searchObject.getBookingFeeType() + "</td></tr>");
						ReportPrinter.append("<tr><td>Booking Fee </td>" + "<td>" + searchObject.getBookingFee() + "</td></tr>");

						if (searchObject.isApplyDiscount()) {
							ReportPrinter.append("<tr><td>Discount Booking</td>" + "<td>Yes</td></tr>");
							ReportPrinter.append("<tr><td>Discount Coupon No</td>" + "<td>" + propertyMap.get("DiscountCouponNo") + "</td></tr>");
							ReportPrinter.append("<tr><td>Discount Type</td>" + "<td>" + propertyMap.get("DiscountType") + "</td></tr>");
							ReportPrinter.append("<tr><td>Discount Value</td>" + "<td>" + propertyMap.get("DiscountValue") + "</td></tr>");
						}

						/*
						 * ReportPrinter.append("<tr><td>Cancellation Response</td>"+"<td>"+propertyMap.get("Portal.Name")+"</td></tr>");
						 * ReportPrinter.append("<tr><td>Cancellation Response</td>"+"<td>"+propertyMap.get("Portal.Name")+"</td></tr>");
						 * ReportPrinter.append("<tr><td>Cancellation Response</td>"+"<td>"+propertyMap.get("Portal.Name")+"</td></tr>");
						 */

						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");
					} catch (Exception e) {
						System.out.println("ERROR EXCEPTION : Class - "+this.getClass().getName() + " Line - "+e.getStackTrace()[0].getLineNumber());
						e.printStackTrace();
					}
					Repository.setAll();
					Screenshot.takeScreenshot(scenarioCommonPath + "/Login Passed.jpg", driver);

					try {

						AirConfig configuration = new AirConfig();
						try {
							System.out.println("INFO -> SET AIR CONFIGURATIONS");
							AirConfig configuration1 = configlist.get(m - 1);
							driver = airconfig.setConfiguration(driver, configuration1);
							configuration = configuration1;
						} catch (Exception e) {
							System.out.println("ERROR EXCEPTION : Class - "+this.getClass().getName() + " Line - "+e.getStackTrace()[0].getLineNumber());
							e.printStackTrace();
						}

						String type = searchObject.getSearchType().trim();

						if (type.equals(SearchType.Call_Center.toString())) {

							System.out.println("INFO -> CALL CENTER");
							cc.go(driver, searchObject, configuration, validatecc, propertyMap, ReportPrinter, Reportcount);

						} else if (type.equals(SearchType.Web.toString())) {

							System.out.println("==================================================================");
							System.out.println("INFO -> WEB BEGIN");
							web.go(driver, searchObject, configuration, validate, propertyMap, ReportPrinter, Reportcount);
							System.out.println("INFO -> WEB END");
							System.out.println("==================================================================");
						}

					} catch (Exception e) {
						System.out.println("ERROR EXCEPTION : Class - "+this.getClass().getName() + " Line - "+e.getStackTrace()[0].getLineNumber());
						e.printStackTrace();
					}
				} else {
					Screenshot.takeScreenshot(scenarioFailedPath + "/Login Failed.jpg", driver);
				}
				
			}
			m += 1;
			try {
				driver.quit();
			} catch (Exception e) {
				System.out.println("ERROR EXCEPTION : Class - "+this.getClass().getName() + " Line - "+e.getStackTrace()[0].getLineNumber());
			}
			count += 1;
		}
	}

	@After
	public void die() {
		ReportPrinter.append("</body></html>");
		BufferedWriter bwr;
		try {
			System.out.println("INFO -> PRINTING FINAL REPORT");
			bwr = new BufferedWriter(new FileWriter(new File("Reports/Flight Web Reservation.html")));
			bwr.write(ReportPrinter.toString());
			bwr.flush();
			bwr.close();
		} catch (IOException e) {
			System.out.println("ERROR EXCEPTION : Class - "+this.getClass().getName() + " Line - "+e.getStackTrace()[0].getLineNumber());
			e.printStackTrace();
		}
		
		System.out.println("INFO -> EXCECUTION COMPLETE");
		driver.quit();
		
		try {
			System.out.println("INFO -> OPEN REPORT");
			Runtime.getRuntime().exec(new String[] {"cmd.exe", "/C", "C:\\Users\\Sanoj\\git\\flightweb-9.0\\Reports\\Flight Web Reservation.html"});
		} catch (Exception e) {
			System.out.println("ERROR EXCEPTION : Class - "+this.getClass().getName() + " Line - "+e.getStackTrace()[0].getLineNumber());
			e.printStackTrace();
		}
		
	}

	public ArrayList<SearchObject> getSearchObjList() {
		
		ArrayList<SearchObject> SearchList = new ArrayList<SearchObject>();

		for (int y = 0; y < 1; y++) {
			Map<Integer, String> Sheet = XLtestData.get(y);

			for (int x = 0; x < Sheet.size(); x++) {
				String[] testData = Sheet.get(x).split(",");
				SearchObject SearchObj = new SearchObject();
				SearchObj.setSearchType(testData[0].trim());
				SearchObj.setSellingCurrency(testData[1].trim());
				SearchObj.setCountry(testData[2].trim());
				SearchObj.setTriptype(testData[3].trim());
				SearchObj.setFrom(testData[4].trim());
				SearchObj.setTo(testData[5].trim());
				SearchObj.setDepartureDate(testData[6].trim());
				SearchObj.setDepartureTime(testData[7].trim());
				SearchObj.setReturnDate(testData[8].trim());
				SearchObj.setReturnTime(testData[9].trim());
				SearchObj.setFlexible(Boolean.parseBoolean(testData[10].toLowerCase().trim()));
				SearchObj.setAdult(testData[11].trim());
				SearchObj.setChildren(testData[12].trim());

				ArrayList<String> childrenage = new ArrayList<String>();
				String[] age = testData[13].trim().split("/");

				for (int u = 0; u < age.length; u++) {
					childrenage.add(age[u].trim());
				}

				SearchObj.setChildrenAge(childrenage);
				SearchObj.setInfant(testData[14].trim());
				SearchObj.setCabinClass(testData[15].trim());
				SearchObj.setPreferredCurrency(testData[16].trim());
				SearchObj.setPreferredAirline(testData[17].trim());
				SearchObj.setNonStop(Boolean.parseBoolean(testData[18].toLowerCase().trim()));

				SearchObj.setProfitType(testData[19].toLowerCase().trim());
				SearchObj.setProfit(Double.parseDouble(testData[20].trim()));

				SearchObj.setSelectingFlight(testData[21].trim());

				SearchObj.setBookingFeeType(testData[22].trim());
				SearchObj.setBookingFee(Double.parseDouble(testData[23].trim()));

				SearchObj.setExcecuteStatus(testData[24].trim());
				SearchObj.setPaymentMode(testData[25].trim());
				SearchObj.setCancellationStatus(testData[26].trim());
				SearchObj.setSupplierPayablePayStatus(testData[27].trim());
				SearchObj.setRemovecartStatus(Boolean.parseBoolean(testData[29].trim()));
				SearchObj.setQuotation(Boolean.parseBoolean(testData[30].trim()));
				SearchObj.setSearchAgain(Boolean.parseBoolean(testData[31].trim()));
				SearchObj.setValidateFilters(Boolean.parseBoolean(testData[32].trim()));
				SearchObj.setTOBooking(testData[33].trim());
				SearchObj.setApplyDiscount(Boolean.parseBoolean(testData[34].trim()));
				SearchObj.setApplyDiscountAtPayPg(Boolean.parseBoolean(testData[35].trim()));

				if (testData[36].trim().equalsIgnoreCase("NetCash")) {
					SearchObj.setToType(TO.NetCash);
				} else if (testData[36].trim().equalsIgnoreCase("NetCreditLpoY")) {
					SearchObj.setToType(TO.NetCreditLpoY);
				} else if (testData[36].trim().equalsIgnoreCase("NetCreditLpoN")) {
					SearchObj.setToType(TO.NetCreditLpoN);
				} else if (testData[36].trim().equalsIgnoreCase("CommCash")) {
					SearchObj.setToType(TO.CommCash);
				} else if (testData[36].trim().equalsIgnoreCase("CommCreditLpoY")) {
					SearchObj.setToType(TO.CommCreditLpoY);
				} else if (testData[36].trim().equalsIgnoreCase("CommCreditLpoN")) {
					SearchObj.setToType(TO.CommCreditLpoN);
				}
				SearchObj.setScenario(testData[37].trim());

				SearchList.add(SearchObj);
			}
		}

		return SearchList;
	}

}
