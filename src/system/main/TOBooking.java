package system.main;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
//import system.classes.TO;









import com.common.Validators.CommonValidator;

import system.classes.TOperator;
import system.classes.XMLPriceItinerary;
import system.pages.WebConfirmationPage;

public class TOBooking
{
	public void bookingListReportTOBfinal(
			WebDriver	driver,					String resNo, 
			TOperator	instanceTO1, 			XMLPriceItinerary	XMLSelectFlight, 
			String		cancellationDeadline,	String 				TOCashCredit,
			String		portalCurrency,			String				payMode,				
			Map<String, String> CurrencyMap,	StringBuffer		ReportPrinter) throws ParseException
	{
		WebDriverWait wait = new WebDriverWait(driver, 5);
		
		try {
			
			driver.findElement(By.partialLinkText("Booking List Report")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			driver.findElement(By.id("reservationno")).clear();
			driver.findElement(By.id("reservationno")).sendKeys(resNo);
			driver.findElement(By.id("reservationno_lkup")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("lookup");
			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By.className("rowstyle0")));
			} catch (Exception e) {
				System.out.println("Control bar missing");
			}
			driver.findElement(By.className("rowstyle0")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
		} catch (Exception e) {
			
		}
		
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")));
		} catch (Exception e) {
			System.out.println("Control bar missing");
		}
		driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")).click();
		String invoice = "";
		String payment = "";
		String voucher = "";
		String eticket = "";
		
		// get current date
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String temp2 = dateFormat.format(date);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date today = sdf.parse(temp2);
		Date canday = sdf.parse(cancellationDeadline);
		
		
		if(driver.findElements(By.id("imgiorange")).size() > 0)
		{
			invoice = "not issued";
		}
		else
		{
			invoice = "issued";
		}
		
		if(driver.findElements(By.id("imgporange")).size() > 0)
		{
			payment = "not issued";
		}
		else
		{
			payment = "issued";
		}
		
		if(driver.findElements(By.id("imgvorange")).size() > 0)
		{
			voucher = "not issued";
		}
		else
		{
			voucher = "issued";
		}
		if(driver.findElements(By.id("imgtorange")).size() > 0)
		{
			eticket = "not issued";
		}
		else
		{
			eticket = "issued";
		}
		
		ReportPrinter.append("<span><center><p class='Hedding0'>Notifications before issueing the voucher</p></center></span>");
		ReportPrinter.append("<table style=width:100%>"
		+ "<tr><th>Items Issued</th>"
		+ "<th>Test Status</th>");

		if(TOCashCredit.equals("Cash"))
		{
			if(payMode.contains("offline"))
			{
				if(eticket.equalsIgnoreCase("not issued"))
				{
					ReportPrinter.append("<tr>");
					ReportPrinter.append("<td>Eticket</td>");
					ReportPrinter.append("<td class = 'Passed'>"+ eticket +"</td>");
					ReportPrinter.append("</tr>");
				}
				else
				{
					ReportPrinter.append("<tr>");
					ReportPrinter.append("<td>Eticket</td>");
					ReportPrinter.append("<td class = 'Failed'>"+ eticket +"</td>");
					ReportPrinter.append("</tr>");
				}
			}
			else if(payMode.contains("online"))
			{
				if(eticket.equalsIgnoreCase("issued"))
				{
					ReportPrinter.append("<tr>");
					ReportPrinter.append("<td>Eticket</td>");
					ReportPrinter.append("<td class = 'Passed'>"+ eticket +"</td>");
					ReportPrinter.append("</tr>");
				}
				else
				{
					ReportPrinter.append("<tr>");
					ReportPrinter.append("<td>Eticket</td>");
					ReportPrinter.append("<td class = 'Failed'>"+ eticket +"</td>");
					ReportPrinter.append("</tr>");
				}
			}
			
			if(canday.after(today))
			{
				if(invoice.equals("issued") && payment.equals("not issued") && voucher.equals("not issued"))
				{
					ReportPrinter.append("<tr>");
					ReportPrinter.append("<td>Invoice</td>");
					ReportPrinter.append("<td class = 'Passed'>"+ invoice +"</td>");
					ReportPrinter.append("</tr>");
					
					ReportPrinter.append("<tr>");
					ReportPrinter.append("<td>Payment</td>");
					ReportPrinter.append("<td class = 'Passed'>"+ payment +"</td>");
					ReportPrinter.append("</tr>");
					
					ReportPrinter.append("<tr>");
					ReportPrinter.append("<td>Voucher</td>");
					ReportPrinter.append("<td class = 'Passed'>"+ voucher +"</td>");
					ReportPrinter.append("</tr>");
				}
				else if(invoice.equals("not issued"))
				{
					ReportPrinter.append("<tr>");
					ReportPrinter.append("<td>Invoice</td>");
					ReportPrinter.append("<td class = 'Failed'>"+ invoice +"</td>");
					ReportPrinter.append("</tr>");
				}
				else if(payment.equals("issued"))
				{
					ReportPrinter.append("<tr>");
					ReportPrinter.append("<td>Payment</td>");
					ReportPrinter.append("<td class = 'Failed'>"+ payment +"</td>");
					ReportPrinter.append("</tr>");
				}
				else if(voucher.equals("issued"))
				{
					ReportPrinter.append("<tr>");
					ReportPrinter.append("<td>Voucher</td>");
					ReportPrinter.append("<td class = 'Failed'>"+ voucher +"</td>");
					ReportPrinter.append("</tr>");
				}
			}
			else
			{
				if(invoice.equals("issued") && payment.equals("issued") && voucher.equals("issued"))
				{
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'Issued'>Invoice - "+ invoice +"</span>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'Issued'>Payment - "+ payment +"</span>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'Issued'>Voucher - "+ voucher +"</span>");
					ReportPrinter.append("<br>");
				}
				else if(invoice.equals("not issued"))
				{
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'Failed'>Invoice - "+ invoice +"</span>");
					ReportPrinter.append("<br>");
				}
				else if(payment.equals("not issued"))
				{
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'Failed'>Payment - "+ payment +"</span>");
					ReportPrinter.append("<br>");
				}
				else if(voucher.equals("not issued"))
				{
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'Failed'>Voucher - "+ voucher +"</span>");
					ReportPrinter.append("<br>");
				}
			}
		}
		else
		{
			if(today.after(canday))
			{
				if(payMode.contains("offline"))
				{
					if(eticket.equalsIgnoreCase("not issued"))
					{
						ReportPrinter.append("<tr>");
						ReportPrinter.append("<td>Eticket</td>");
						ReportPrinter.append("<td class = 'Passed'>"+ eticket +"</td>");
						ReportPrinter.append("</tr>");
					}
					else
					{
						ReportPrinter.append("<tr>");
						ReportPrinter.append("<td>Eticket</td>");
						ReportPrinter.append("<td class = 'Failed'>"+ eticket +"</td>");
						ReportPrinter.append("</tr>");
					}
					
					if(invoice.equals("issued") && payment.equals("not issued") && voucher.equals("issued"))
					{
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'Passed'>Invoice - "+ invoice +"</span>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'Passed'>Payment - "+ payment +"</span>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'Passed'>Voucher - "+ voucher +"</span>");
						ReportPrinter.append("<br>");
					}
					else if(invoice.equals("not issued"))
					{
						ReportPrinter.append("<br>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'Failed'>Invoice - "+ invoice +"</span>");
						ReportPrinter.append("<br>");
					}
					else if(payment.equals("issued"))
					{
						ReportPrinter.append("<br>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'Failed'>Payment - "+ payment +"</span>");
						ReportPrinter.append("<br>");
					}
					else if(voucher.equals("not issued"))
					{
						ReportPrinter.append("<br>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'Failed'>Voucher - "+ voucher +"</span>");
						ReportPrinter.append("<br>");
					}
				}
				else
				{
					if(eticket.equalsIgnoreCase("issued"))
					{
						ReportPrinter.append("<tr>");
						ReportPrinter.append("<td>Eticket</td>");
						ReportPrinter.append("<td class = 'Passed'>"+ eticket +"</td>");
						ReportPrinter.append("</tr>");
					}
					else
					{
						ReportPrinter.append("<tr>");
						ReportPrinter.append("<td>Eticket</td>");
						ReportPrinter.append("<td class = 'Failed'>"+ eticket +"</td>");
						ReportPrinter.append("</tr>");
					}
					
					if(invoice.equals("issued") && payment.equals("issued") && voucher.equals("issued"))
					{
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'Passed'>Invoice - "+ invoice +"</span>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'Passed'>Payment - "+ payment +"</span>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'Passed'>Voucher - "+ voucher +"</span>");
						ReportPrinter.append("<br>");
					}
					else if(invoice.equals("not issued"))
					{
						ReportPrinter.append("<br>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'Failed'>Invoice - "+ invoice +"</span>");
						ReportPrinter.append("<br>");
					}
					else if(payment.equals("not issued"))
					{
						ReportPrinter.append("<br>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'Failed'>Payment - "+ payment +"</span>");
						ReportPrinter.append("<br>");
					}
					else if(voucher.equals("not issued"))
					{
						ReportPrinter.append("<br>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'Failed'>Voucher - "+ voucher +"</span>");
						ReportPrinter.append("<br>");
					}
				}
			}
			else
			{
				if(payMode.contains("offline"))
				{
					if(eticket.equalsIgnoreCase("not issued"))
					{
						ReportPrinter.append("<tr>");
						ReportPrinter.append("<td>Eticket</td>");
						ReportPrinter.append("<td class = 'Passed'>"+ eticket +"</td>");
						ReportPrinter.append("</tr>");
					}
					else
					{
						ReportPrinter.append("<tr>");
						ReportPrinter.append("<td>Eticket</td>");
						ReportPrinter.append("<td class = 'Failed'>"+ eticket +"</td>");
						ReportPrinter.append("</tr>");
					}
					
					if(invoice.equals("not issued") && payment.equals("not issued") && voucher.equals("not issued"))
					{
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'NotIssued'>Invoice - "+ invoice +"</span>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'NotIssued'>Payment - "+ payment +"</span>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'NotIssued'>Voucher - "+ voucher +"</span>");
						ReportPrinter.append("<br>");
					}
					else if(invoice.equals("issued"))
					{
						ReportPrinter.append("<br>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'Failed'>Invoice - "+ invoice +"</span>");
						ReportPrinter.append("<br>");
					}
					else if(payment.equals("issued"))
					{
						ReportPrinter.append("<br>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'Failed'>Payment - "+ payment +"</span>");
						ReportPrinter.append("<br>");
					}
					else if(voucher.equals("issued"))
					{
						ReportPrinter.append("<br>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'Failed'>Voucher - "+ voucher +"</span>");
						ReportPrinter.append("<br>");
					}
				}
				else
				{
					if(eticket.equalsIgnoreCase("issued"))
					{
						ReportPrinter.append("<tr>");
						ReportPrinter.append("<td>Eticket</td>");
						ReportPrinter.append("<td class = 'Passed'>"+ eticket +"</td>");
						ReportPrinter.append("</tr>");
					}
					else
					{
						ReportPrinter.append("<tr>");
						ReportPrinter.append("<td>Eticket</td>");
						ReportPrinter.append("<td class = 'Failed'>"+ eticket +"</td>");
						ReportPrinter.append("</tr>");
					}
					
					if(invoice.equals("issued") && payment.equals("issued") && voucher.equals("issued"))
					{
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'Issued'>Invoice - "+ invoice +"</span>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'Issued'>Payment - "+ payment +"</span>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'Issued'>Voucher - "+ voucher +"</span>");
						ReportPrinter.append("<br>");
					}
					else if(!invoice.equals("not issued"))
					{
						ReportPrinter.append("<br>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'Failed'>Invoice - "+ invoice +"</span>");
						ReportPrinter.append("<br>");
					}
					else if(!payment.equals("not issued"))
					{
						ReportPrinter.append("<br>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'Failed'>Payment - "+ payment +"</span>");
						ReportPrinter.append("<br>");
					}
					else if(!voucher.equals("not issued"))
					{
						ReportPrinter.append("<br>");
						ReportPrinter.append("<br>");
						ReportPrinter.append("<span class = 'Failed'>Voucher - "+ voucher +"</span>");
						ReportPrinter.append("<br>");
					}
				}
			}
		
		}
		
		ReportPrinter.append("</table>");
		ReportPrinter.append("<br><br><br>");
		try {
			driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[21]/a/img")).click();
		} catch (Exception e) {
			try {
				driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[19]/a/img")).click();
			} catch (Exception e2) {
				try {
					driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[20]/a/img")).click();
				} catch (Exception e3) {
					
				}
			}
		}                             
		driver.switchTo().frame("dialogwindow");
		
		
		try {
			double	credit		= instanceTO1.getCreditBlanace();
			double	sellcost	= XMLSelectFlight.getPricinginfo().getSellingCost();
			sellcost			= CommonValidator.convert(XMLSelectFlight.getPricinginfo().getNewBasefareCurrencyCode(), instanceTO1.getCurrency(), portalCurrency, sellcost, CurrencyMap);
			
			//check logic
			ReportPrinter.append("<span><center><p class='Hedding0'>Notifications before issueing the voucher</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Items Issued</th>"
			+ "<th>Test Status</th>"
			/*+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>"*/);
			if(credit < sellcost)
			{
				ReportPrinter.append("<tr><td> Credit : "+ credit +"</td>"
				+ 	"<td>"+ sellcost +"</td>"
				+	"<td>to credit balance > booking value</td>"
				+ 	"<td>to credit balance > booking value</td>"
				+	"<td class='Failed'>Failed</td></tr>");
				
				if(driver.findElements(By.xpath(".//*[@id='issuevoucher']/a/img")).size() > 0)
				{
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class='Failed'>System allows issuing the voucher</span>");
					ReportPrinter.append("<br>");
				}
				else
				{
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class='Pass'>System does not allow issuing the voucher</span>");
					ReportPrinter.append("<br>");
				}
			}
			else
			{
				ReportPrinter.append("<tr><td>"+ credit +"</td>"
				+ 	"<td>"+ sellcost +"</td>"
				+	"<td>to credit balance > booking value</td>"
				+ 	"<td>to credit balance > booking value</td>"
				+	"<td class='passed'>Passeed</td></tr>");
				
				if(driver.findElements(By.xpath(".//*[@id='issuevoucher']/a/img")).size() > 0)
				{
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class='passed'>System allows issuing the voucher</span>");
					ReportPrinter.append("<br>");
				}
				else
				{
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class='Fail'>System does not allow issuing the voucher</span>");
					ReportPrinter.append("<br>");
				}
				
			}
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		} catch (Exception e) {
			
		}
		ReportPrinter.append("</table>");
		ReportPrinter.append("<br><br><br>");
		//issue voucher
		try {
			driver.findElement(By.xpath(".//*[@id='issuevoucher']/a/img")).click();
		} catch (Exception e) {
			System.out.println("Voucher issue failed");
		}
		
		ReportPrinter.append("<br>");
		ReportPrinter.append("<span><center><p class='Hedding0'>Check whether to credit balance is higher than the booking value</p></center></span>");
		ReportPrinter.append("<table border=1 style=width:100%>"
		+ "<tr><th>TO balance</th>"
		+ "<th>Booking value</th>"
		+ "<th>Expected result</th>"
		+ "<th>Actual result</th>"
		+ "<th>Test Status</th></tr>");
		
		//ReportPrinter.append("</table>");
		
		if(TOCashCredit.equals("cash"))
		{
			if(driver.findElements(By.xpath(".//*[@id='issuevoucher']/a/img")).size() > 0)
			{
				ReportPrinter.append("<br>");
				ReportPrinter.append("<br>");
				ReportPrinter.append("<span class = 'Failed'>Issue voucher button available</span>");
				ReportPrinter.append("<br>");
			}
			else
			{
				ReportPrinter.append("<br>");
				ReportPrinter.append("<br>");
				ReportPrinter.append("<span class = 'passed'>Issue voucher button not available</span>");
				ReportPrinter.append("<br>");
			}
			try {
				driver.findElement(By.xpath(".//*[@id='dialogwindowdragdrop']/td[3]/a/img")).click();
			} catch (Exception e2) {
				
			}
		}
		else
		{
			if(driver.findElements(By.xpath(".//*[@id='issuevoucher']/a/img")).size() > 0)
			{
				ReportPrinter.append("<br>");
				ReportPrinter.append("<br>");
				ReportPrinter.append("<span class = 'Failed'>Issue voucher button not available</span>");
				ReportPrinter.append("<br>");
			}
			else
			{
				ReportPrinter.append("<br>");
				ReportPrinter.append("<br>");
				ReportPrinter.append("<span class = 'passed'>Issue voucher button available</span>");
				ReportPrinter.append("<br>");
			}
			// issue voucher
			try {
				driver.findElement(By.xpath(".//*[@id='issuevoucher']/a/img")).click();
			} catch (Exception e) {
				
			}
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("imgvgreen")));
			} catch (Exception e) {
				
			}
		}
		
		if(TOCashCredit.equalsIgnoreCase("cash"))
		{
			
		}
		else
		{
			//validate after issueing the voucher
			ReportPrinter.append("<br>");
			ReportPrinter.append("<span>After issuing the voucher</span>");
			if(driver.findElements(By.id("imgigreen")).size() > 0)
			{
				invoice = "issued";
			}
			else
			{
				invoice = "not issued";
			}
			
			if(driver.findElements(By.id("imgporange")).size() > 0)
			{
				payment = "not issued";
			}
			else
			{
				payment = "issued";
			}
			
			if(driver.findElements(By.id("imgvgreen")).size() > 0)
			{
				voucher = "issued";
			}
			else
			{
				voucher = "not issued";
			}
			if(driver.findElements(By.id("imgtgreen")).size() > 0)
			{
				eticket = "issued";
			}
			else
			{
				eticket = "not issued";
			}
			if(payMode.equals("contains"))
			{
				if(invoice.equals("issued") && payment.equals("not issued") && voucher.equals("issued"))
				{
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'Issued'>Invoice - "+ invoice +"</span>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'NotIssued'>Payment - "+ payment +"</span>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'Issued'>Voucher - "+ voucher +"</span>");
					ReportPrinter.append("<br>");
				}
				else if(invoice.equals("not issued"))
				{
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'Failed'>Invoice - "+ invoice +"</span>");
					ReportPrinter.append("<br>");
				}
				else if(payment.equals("issued"))
				{
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'Failed'>Payment - "+ payment +"</span>");
					ReportPrinter.append("<br>");
				}
				else if(voucher.equals("not issued"))
				{
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'Failed'>Voucher - "+ voucher +"</span>");
					ReportPrinter.append("<br>");
				}
			}
		}
		
		try {
			driver.switchTo().defaultContent();
			driver.findElement(By.xpath(".//*[@id='topMenuItemsRight']/table/tbody/tr/td[1]")).click();
		} catch (Exception e) {
			
		}
		
		ReportPrinter.append("</table>");
	}

	public void pay(WebDriver driver, WebConfirmationPage webConfirmationPage, String url)
	{
		try {
			driver.get(url);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			driver.findElement(By.id("reportCtegory_detail")).click();
			driver.findElement(By.id("searchby_reservationno")).click();
			driver.findElement(By.id("reservationno")).clear();
			driver.findElement(By.id("reservationno")).sendKeys(webConfirmationPage.getReservationNo());
			driver.findElement(By.id("reservationno_lkup")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("lookup");
			driver.findElement(By.xpath(".//*[@id='row-1']/td")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[3]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[7]/a/img")).click();
			driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[5]/a/img")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			driver.switchTo().frame("dialogwindow");
			driver.findElement(By.id("paidAmountArray[0]")).sendKeys(webConfirmationPage.getPriceinfo().getAmountprocess());
			driver.findElement(By.id("saveButId")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			try {
				driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
			} catch (Exception e) {
				
			}
		} catch (Exception e) {
			
		}
	}
	
	public void bookingListReportToFinal(
			WebDriver	driver,					WebConfirmationPage webConfirmationPage, 
			TOperator			instanceTO1, 			XMLPriceItinerary	XMLSelectFlight, 
			String		cancellationDeadline,	String 				TOCashCredit,
			String		portalCurrency,			String				payMode,				
			Map<String, String> CurrencyMap,	StringBuffer		ReportPrinter) throws ParseException
	{
		WebDriverWait wait	= new WebDriverWait(driver, 5);
		
		try {
			driver.findElement(By.partialLinkText("Booking List Report")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			driver.findElement(By.id("reservationno")).clear();
			driver.findElement(By.id("reservationno")).sendKeys(webConfirmationPage.getReservationNo());
			System.out.println(webConfirmationPage.getReservationNo());
			driver.findElement(By.id("reservationno_lkup")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("lookup");
			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By.className("rowstyle0")));
			} catch (Exception e) {
				System.out.println("Control bar missing");
			}
			driver.findElement(By.className("rowstyle0")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
		} catch (Exception e) {
			
		}
		
		
		
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")));
		} catch (Exception e) {
			
		}
		driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")).click();
		String invoice = "";
		String payment = "";
		String voucher = "";
		String eticket = "";
		
		if(driver.findElements(By.id("imgigreen")).size() > 0)
		{
			invoice = "issued";
		}
		else
		{
			invoice = "not issued";
		}
		
		if(driver.findElements(By.id("imgpgreen")).size() > 0)
		{
			payment = "issued";
		}
		else
		{
			payment = "not issued";
		}
		
		if(driver.findElements(By.id("imgvgreen")).size() > 0)
		{
			eticket = "issued";
		}
		else
		{
			eticket = "not issued";
		}
		
		
		// get current date
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String temp2 = dateFormat.format(date);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date today = sdf.parse(temp2);
		Date canday = sdf.parse(cancellationDeadline);
		
		ReportPrinter.append("<span><center><p class='Hedding0'>Notifications after payment voucher</p></center></span>");
		ReportPrinter.append("<table style=width:100%>"
		+ "<tr><th>Notification</th>"
		+ "<th>Status</th></tr>");
		if(TOCashCredit.equals("Cash"))
		{
			if(canday.after(today))
			{
				if(payMode.contains("offline"))
				{
					if(eticket.equalsIgnoreCase("not issued"))
					{
						ReportPrinter.append("<tr>");
						ReportPrinter.append("<td>Eticket</td>");
						ReportPrinter.append("<td class = 'Passed'>"+ eticket +"</td>");
						ReportPrinter.append("</tr>");
					}
					else
					{
						ReportPrinter.append("<tr>");
						ReportPrinter.append("<td>Eticket</td>");
						ReportPrinter.append("<td class = 'Failed'>"+ eticket +"</td>");
						ReportPrinter.append("</tr>");
					}
				}
				else if(payMode.contains("online"))
				{
					if(eticket.equalsIgnoreCase("issued"))
					{
						ReportPrinter.append("<tr>");
						ReportPrinter.append("<td>Eticket</td>");
						ReportPrinter.append("<td class = 'Passed'>"+ eticket +"</td>");
						ReportPrinter.append("</tr>");
					}
					else
					{
						ReportPrinter.append("<tr>");
						ReportPrinter.append("<td>Eticket</td>");
						ReportPrinter.append("<td class = 'Failed'>"+ eticket +"</td>");
						ReportPrinter.append("</tr>");
					}
				}
				if(invoice.equals("issued") && payment.equals("issued") && voucher.equals("not issued"))
				{
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'Issued'>Invoice - "+ invoice +"</span>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'Issued'>Payment - "+ payment +"</span>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'NotIssued'>Voucher - "+ voucher +"</span>");
					ReportPrinter.append("<br>");
				}
				else if(invoice.equals("not issued"))
				{
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'Failed'>Invoice - "+ invoice +"</span>");
					ReportPrinter.append("<br>");
				}
				else if(payment.equals("not issued"))
				{
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'Failed'>Payment - "+ payment +"</span>");
					ReportPrinter.append("<br>");
				}
				else if(voucher.equals("issued"))
				{
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'Failed'>Voucher - "+ voucher +"</span>");
					ReportPrinter.append("<br>");
				}
			}
			else
			{
				if(payMode.contains("offline"))
				{
					if(eticket.equalsIgnoreCase("not issued"))
					{
						ReportPrinter.append("<tr>");
						ReportPrinter.append("<td>Eticket</td>");
						ReportPrinter.append("<td class = 'Passed'>"+ eticket +"</td>");
						ReportPrinter.append("</tr>");
					}
					else
					{
						ReportPrinter.append("<tr>");
						ReportPrinter.append("<td>Eticket</td>");
						ReportPrinter.append("<td class = 'Failed'>"+ eticket +"</td>");
						ReportPrinter.append("</tr>");
					}
				}
				else if(payMode.contains("online"))
				{
					if(eticket.equalsIgnoreCase("issued"))
					{
						ReportPrinter.append("<tr>");
						ReportPrinter.append("<td>Eticket</td>");
						ReportPrinter.append("<td class = 'Passed'>"+ eticket +"</td>");
						ReportPrinter.append("</tr>");
					}
					else
					{
						ReportPrinter.append("<tr>");
						ReportPrinter.append("<td>Eticket</td>");
						ReportPrinter.append("<td class = 'Failed'>"+ eticket +"</td>");
						ReportPrinter.append("</tr>");
					}
				}
				if(invoice.equals("issued") && payment.equals("issued") && voucher.equals("issued"))
				{
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'Issued'>Invoice - "+ invoice +"</span>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'Issued'>Payment - "+ payment +"</span>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'Issued'>Voucher - "+ voucher +"</span>");
					ReportPrinter.append("<br>");
				}
				else if(invoice.equals("not issued"))
				{
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'Failed'>Invoice - "+ invoice +"</span>");
					ReportPrinter.append("<br>");
				}
				else if(payment.equals("not issued"))
				{
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'Failed'>Payment - "+ payment +"</span>");
					ReportPrinter.append("<br>");
				}
				else if(voucher.equals("not issued"))
				{
					ReportPrinter.append("<br>");
					ReportPrinter.append("<br>");
					ReportPrinter.append("<span class = 'Failed'>Voucher - "+ voucher +"</span>");
					ReportPrinter.append("<br>");
				}
			}
			
		}
		else
		{
			if(payMode.contains("offline"))
			{
				if(eticket.equalsIgnoreCase("not issued"))
				{
					ReportPrinter.append("<tr>");
					ReportPrinter.append("<td>Eticket</td>");
					ReportPrinter.append("<td class = 'Passed'>"+ eticket +"</td>");
					ReportPrinter.append("</tr>");
				}
				else
				{
					ReportPrinter.append("<tr>");
					ReportPrinter.append("<td>Eticket</td>");
					ReportPrinter.append("<td class = 'Failed'>"+ eticket +"</td>");
					ReportPrinter.append("</tr>");
				}
			}
			else if(payMode.equalsIgnoreCase("online"))
			{
				if(eticket.equalsIgnoreCase("issued"))
				{
					ReportPrinter.append("<tr>");
					ReportPrinter.append("<td>Eticket</td>");
					ReportPrinter.append("<td class = 'Passed'>"+ eticket +"</td>");
					ReportPrinter.append("</tr>");
				}
				else
				{
					ReportPrinter.append("<tr>");
					ReportPrinter.append("<td>Eticket</td>");
					ReportPrinter.append("<td class = 'Failed'>"+ eticket +"</td>");
					ReportPrinter.append("</tr>");
				}
			}
			if(invoice.equals("issued") && payment.equals("issued") && voucher.equals("issued"))
			{
				ReportPrinter.append("<br>");
				ReportPrinter.append("<span class = 'Issued'>Invoice - "+ invoice +"</span>");
				ReportPrinter.append("<br>");
				ReportPrinter.append("<br>");
				ReportPrinter.append("<span class = 'Issued'>Payment - "+ payment +"</span>");
				ReportPrinter.append("<br>");
				ReportPrinter.append("<br>");
				ReportPrinter.append("<span class = 'Issued'>Voucher - "+ voucher +"</span>");
				ReportPrinter.append("<br>");
			}
			else if(!invoice.equals("issued"))
			{
				ReportPrinter.append("<br>");
				ReportPrinter.append("<br>");
				ReportPrinter.append("<span class = 'Failed'>Invoice - "+ invoice +"</span>");
				ReportPrinter.append("<br>");
			}
			else if(!payment.equals("issued"))
			{
				ReportPrinter.append("<br>");
				ReportPrinter.append("<br>");
				ReportPrinter.append("<span class = 'Failed'>Payment - "+ payment +"</span>");
				ReportPrinter.append("<br>");
			}
			else if(!voucher.equals("issued"))
			{
				ReportPrinter.append("<br>");
				ReportPrinter.append("<br>");
				ReportPrinter.append("<span class = 'Failed'>Voucher - "+ voucher +"</span>");
				ReportPrinter.append("<br>");
			}
		}
		
		/*if(invoice.equals("issued") && payment.equals("issued") && voucher.equals("issued"))
		{
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Invoice</td>");
			ReportPrinter.append("<td class = 'Passed'>"+ invoice +"</td>");
			ReportPrinter.append("</tr>");
			
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Payment</td>");
			ReportPrinter.append("<td class = 'Passed'>"+ payment +"</td>");
			ReportPrinter.append("</tr>");
			
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Voucher</td>");
			ReportPrinter.append("<td class = 'Passed'>"+ voucher +"</td>");
			ReportPrinter.append("</tr>");
			
			ReportPrinter.append("<br>");
			ReportPrinter.append("<span class = 'Issued'>Invoice - "+ invoice +"</span>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<span class = 'Issued'>Payment - "+ payment +"</span>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<span class = 'Issued'>Voucher - "+ voucher +"</span>");
			ReportPrinter.append("<br>");
		}
		else if(!invoice.equals("issued"))
		{
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Invoice</td>");
			ReportPrinter.append("<td class = 'Failed'>"+ invoice +"</td>");
			ReportPrinter.append("</tr>");
			
			ReportPrinter.append("<br>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<span class = 'Failed'>Invoice - "+ invoice +"</span>");
			ReportPrinter.append("<br>");
		}
		else if(!payment.equals("issued"))
		{
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Payment</td>");
			ReportPrinter.append("<td class = 'Failed'>"+ payment +"</td>");
			ReportPrinter.append("</tr>");
			
			ReportPrinter.append("<br>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<span class = 'Failed'>Payment - "+ payment +"</span>");
			ReportPrinter.append("<br>");
		}
		else if(!voucher.equals("issued"))
		{
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Voucher</td>");
			ReportPrinter.append("<td class = 'Failed'>"+ voucher +"</td>");
			ReportPrinter.append("</tr>");
			
			ReportPrinter.append("<br>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<span class = 'Failed'>Voucher - "+ voucher +"</span>");
			ReportPrinter.append("<br>");
		}*/
		
		ReportPrinter.append("</table>");
		ReportPrinter.append("<br><br><br>");
		
		try {
			driver.switchTo().defaultContent();
			driver.findElement(By.xpath(".//*[@id='topMenuItemsRight']/table/tbody/tr/td[1]")).click();
		} catch (Exception e) {
			
		}
		
		System.out.println("to done");
	}

}
