package system.classes;

import system.enumtypes.CreditCard;

//import Flight.TypeEnum.CreditCard;

public class UICreditCardPayInfo 
{
	CreditCard cardType = null;
	String totalpackage_bookVal_CurrCode = "";
	String totalpackage_bookVal = "0";
	String subtotal = "0";
	String discount = "0";
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	String taxandfees = "0";
	String Total = "0";
	String amountprocess = "0";
	String amountprocessAirline = "0";
	
	String referenceNo = "";
	String MerchantTrackID = "";
	String paymentId = "";
	String amount = "0";
	String amountCurrency = "";
	

	public String getReferenceNo() {
		return referenceNo;
	}
	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}
	public String getMerchantTrackID() {
		return MerchantTrackID;
	}
	public void setMerchantTrackID(String merchantTrackID) {
		MerchantTrackID = merchantTrackID;
	}
	public String getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getAmountCurrency() {
		return amountCurrency;
	}
	public void setAmountCurrency(String amountCurrency) {
		this.amountCurrency = amountCurrency;
	}
	
	public CreditCard getCardType() {
		return cardType;
	}
	public void setCardType(CreditCard cardType) {
		this.cardType = cardType;
	}
	public String getTotalpackage_bookVal_CurrCode() {
		return totalpackage_bookVal_CurrCode;
	}
	public void setTotalpackage_bookVal_CurrCode(
			String totalpackage_bookVal_CurrCode) {
		this.totalpackage_bookVal_CurrCode = totalpackage_bookVal_CurrCode;
	}
	public String getTotalpackage_bookVal() {
		return totalpackage_bookVal;
	}
	public void setTotalpackage_bookVal(String totalpackage_bookVal) {
		this.totalpackage_bookVal = totalpackage_bookVal;
	}
	public String getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(String subtotal) {
		this.subtotal = subtotal;
	}
	public String getTaxandfees() {
		return taxandfees;
	}
	public void setTaxandfees(String taxandfees) {
		this.taxandfees = taxandfees;
	}
	public String getTotal() {
		return Total;
	}
	public void setTotal(String total) {
		Total = total;
	}
	public String getAmountprocess() {
		return amountprocess;
	}
	public void setAmountprocess(String amountprocess) {
		this.amountprocess = amountprocess;
	}
	public String getAmountprocessAirline() {
		return amountprocessAirline;
	}
	public void setAmountprocessAirline(String amountprocessAirline) {
		this.amountprocessAirline = amountprocessAirline;
	}

}
