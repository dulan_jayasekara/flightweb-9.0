/*Sanoj*/
package system.classes;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;

/*SANOJ*/

public class UIFlightItinerary 
{
	private Outbound	outbound			= null;
	private Inbound		inbound				= null;
	
	private ArrayList<WebElement> Note      = null;
	
	private String tripType					= "";
	private String fromBox					= "";
	private String toBox					= "";
	private String fromDate					= "";
	private String toDate					= "";
	private String paxCount					= "";
	private String fromLabel				= "";
	private String toLabel					= "";
	
	private String OBDepFlightCode			= "";
	private String OBDepLocationCode		= "";
	private String OBDepDate				= "";
	private String OBDepTime				= "";
	private String OBArrFlightCode			= "";
	private String OBArrLocationCode		= "";
	private String OBArrDate				= "";
	private String OBArrTime				= "";
	private String OBDuration				= "";
	private String OBStops					= "";
	private String OBLayover				= "";
	
	private String IBDepFlightCode			= "";
	private String IBDepLocationCode		= "";
	private String IBDepDate				= "";
	private String IBDepTime				= "";
	private String IBArrFlightCode			= "";
	private String IBArrLocationCode		= "";
	private String IBArrDate				= "";
	private String IBArrTime				= "";
	private String IBDuration				= "";
	private String IBStops					= "";
	private String IBLayover				= "";
	
	private String currencyCode				= "";
	private String totalCost				= "";
	
	
	public String getTripType() {
		return tripType;
	}
	public void setTripType(String tripType) {
		this.tripType = tripType;
	}
	public String getFromBox() {
		return fromBox;
	}
	public void setFromBox(String fromBox) {
		this.fromBox = fromBox;
	}
	public String getToBox() {
		return toBox;
	}
	public void setToBox(String toBox) {
		this.toBox = toBox;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getPaxCount() {
		return paxCount;
	}
	public void setPaxCount(String paxCount) {
		this.paxCount = paxCount;
	}
	public String getFromLabel() {
		return fromLabel;
	}
	public void setFromLabel(String fromLabel) {
		this.fromLabel = fromLabel;
	}
	public String getToLabel() {
		return toLabel;
	}
	public void setToLabel(String toLabel) {
		this.toLabel = toLabel;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}
	public Outbound getOutbound() {
		return outbound;
	}
	public void setOutbound(Outbound outbound) {
		this.outbound = outbound;
	}
	public Inbound getInbound() {
		return inbound;
	}
	public void setInbound(Inbound inbound) {
		this.inbound = inbound;
	}
	public ArrayList<WebElement> getNote() {
		return Note;
	}
	public void setNote(ArrayList<WebElement> note) {
		Note = note;
	}
	public String getOBDepFlightCode() {
		return OBDepFlightCode;
	}
	public void setOBDepFlightCode(String oBDepFlightCode) {
		OBDepFlightCode = oBDepFlightCode;
	}
	public String getOBDepLocationCode() {
		return OBDepLocationCode;
	}
	public void setOBDepLocationCode(String oBDepLocationCode) {
		OBDepLocationCode = oBDepLocationCode;
	}
	public String getOBDepDate() {
		return OBDepDate;
	}
	public void setOBDepDate(String oBDepDate) {
		OBDepDate = oBDepDate;
	}
	public String getOBDepTime() {
		return OBDepTime;
	}
	public void setOBDepTime(String oBDepTime) {
		OBDepTime = oBDepTime;
	}
	public String getOBArrFlightCode() {
		return OBArrFlightCode;
	}
	public void setOBArrFlightCode(String oBArrFlightCode) {
		OBArrFlightCode = oBArrFlightCode;
	}
	public String getOBArrLocationCode() {
		return OBArrLocationCode;
	}
	public void setOBArrLocationCode(String oBArrLocationCode) {
		OBArrLocationCode = oBArrLocationCode;
	}
	public String getOBArrDate() {
		return OBArrDate;
	}
	public void setOBArrDate(String oBArrDate) {
		OBArrDate = oBArrDate;
	}
	public String getOBArrTime() {
		return OBArrTime;
	}
	public void setOBArrTime(String oBArrTime) {
		OBArrTime = oBArrTime;
	}
	public String getOBDuration() {
		return OBDuration;
	}
	public void setOBDuration(String oBDuration) {
		OBDuration = oBDuration;
	}
	public String getOBStops() {
		return OBStops;
	}
	public void setOBStops(String oBStops) {
		OBStops = oBStops;
	}
	public String getOBLayover() {
		return OBLayover;
	}
	public void setOBLayover(String oBLayover) {
		OBLayover = oBLayover;
	}
	public String getIBDepFlightCode() {
		return IBDepFlightCode;
	}
	public void setIBDepFlightCode(String iBDepFlightCode) {
		IBDepFlightCode = iBDepFlightCode;
	}
	public String getIBDepLocationCode() {
		return IBDepLocationCode;
	}
	public void setIBDepLocationCode(String iBDepLocationCode) {
		IBDepLocationCode = iBDepLocationCode;
	}
	public String getIBDepDate() {
		return IBDepDate;
	}
	public void setIBDepDate(String iBDepDate) {
		IBDepDate = iBDepDate;
	}
	public String getIBDepTime() {
		return IBDepTime;
	}
	public void setIBDepTime(String iBDepTime) {
		IBDepTime = iBDepTime;
	}
	public String getIBArrFlightCode() {
		return IBArrFlightCode;
	}
	public void setIBArrFlightCode(String iBArrFlightCode) {
		IBArrFlightCode = iBArrFlightCode;
	}
	public String getIBArrLocationCode() {
		return IBArrLocationCode;
	}
	public void setIBArrLocationCode(String iBArrLocationCode) {
		IBArrLocationCode = iBArrLocationCode;
	}
	public String getIBArrDate() {
		return IBArrDate;
	}
	public void setIBArrDate(String iBArrDate) {
		IBArrDate = iBArrDate;
	}
	public String getIBArrTime() {
		return IBArrTime;
	}
	public void setIBArrTime(String iBArrTime) {
		IBArrTime = iBArrTime;
	}
	public String getIBDuration() {
		return IBDuration;
	}
	public void setIBDuration(String iBDuration) {
		IBDuration = iBDuration;
	}
	public String getIBStops() {
		return IBStops;
	}
	public void setIBStops(String iBStops) {
		IBStops = iBStops;
	}
	public String getIBLayover() {
		return IBLayover;
	}
	public void setIBLayover(String iBLayover) {
		IBLayover = iBLayover;
	}
	
}
