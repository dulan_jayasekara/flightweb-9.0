package system.classes;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class PaymentGateway {
	
	public void rezPay(WebDriver driver,
			String numberPart1,
			String numberPart2,
			String numberPart3,
			String numberPart4,
			String expMonth,
			String expYear,
			String cardHolderName,
			String ccvNumber)
	{
		WebDriverWait wait = new WebDriverWait(driver, 25);
		
		try {
			
			driver.switchTo().frame("paygatewayFrame");
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cardnumberpart1")));
			
			((JavascriptExecutor)driver).executeScript("$('#cardnumberpart1').val('"+numberPart1+"');");
			((JavascriptExecutor)driver).executeScript("$('#cardnumberpart2').val('"+numberPart2+"');");
			((JavascriptExecutor)driver).executeScript("$('#cardnumberpart3').val('"+numberPart3+"');");
			((JavascriptExecutor)driver).executeScript("$('#cardnumberpart4').val('"+numberPart4+"');");
			
			new Select(driver.findElement(By.id("cardexpmonth"))).selectByVisibleText(expMonth);
			new Select(driver.findElement(By.id("cardexpyear"))).selectByVisibleText(expYear);
			driver.findElement(By.id("cardholdername")).sendKeys(cardHolderName);
			((JavascriptExecutor)driver).executeScript("$('#cv2').val('"+ccvNumber+"');");
			
			Thread.sleep(3000);
			driver.findElement(By.className("proceed_btn")).click();
			
		} catch (InterruptedException e) {
			System.out.println("Class :" +e.getStackTrace().getClass());
			System.out.println("Line  :" +e.getStackTrace()[2].getLineNumber());
		}
	}
	
	public void wireCard(WebDriver driver,
			String cardNumber,
			String ccvNumber,
			String FName,
			String LName,
			String cardType,
			String expireMonth, String expireYear
			)
	{
		driver.switchTo().frame("paygatewayFrame");
		driver.findElement(By.id("first_name")).sendKeys(FName);
		driver.findElement(By.id("last_name")).sendKeys(LName);
		driver.findElement(By.id("hpp-creditcard-form-cardtypeBtn")).click();
		if(cardType.equalsIgnoreCase("Visa"))
		{
			driver.findElement(By.id("visa")).click();
		}
		if(cardType.equalsIgnoreCase("Master"))
		{
			driver.findElement(By.id("master")).click();
		}	
		
		driver.findElement(By.id("account_number")).sendKeys(cardNumber);
		driver.findElement(By.id("card_security_code")).sendKeys(ccvNumber);
		new Select(driver.findElement(By.id("expiration_month_list"))).selectByVisibleText(expireMonth);
		new Select(driver.findElement(By.id("expiration_year_list"))).selectByVisibleText(expireYear);
		
		driver.findElement(By.id("hpp-form-submit")).click();
		
	}
	
	public void payPal(WebDriver driver, 
			String cardType, 
			String country, 
			String cardNumber, 
			String expireMonth, String expireYear,
			String ccvNumber,
			String fName, String middleName, String lastName,
			String address1, String city, String state, String zip,
			String hPhoneNumber,
			String email
			)
	{
		WebDriverWait wait = new WebDriverWait(driver, 25);
		
		try {
			
			driver.switchTo().defaultContent();
			driver.switchTo().frame("_yuiResizeMonitor");
			wait.until(ExpectedConditions.elementToBeSelected(By.id("country_code")));
			
			new Select(driver.findElement(By.id("country_code"))).selectByVisibleText(country);
			driver.findElement(By.id("cc_number")).sendKeys(cardNumber);
			if(cardType.equalsIgnoreCase("Visa"))
			{
				driver.findElement(By.id("pm-visa")).click();
			}
			if(cardType.equalsIgnoreCase("Master"))
			{
				driver.findElement(By.id("pm-mastercard")).click();
			}
			if(cardType.equalsIgnoreCase("Discover"))
			{
				driver.findElement(By.id("pm-discover")).click();
			}
			if(cardType.equalsIgnoreCase("Amex"))
			{
				driver.findElement(By.id("pm-amex")).click();
			}
			driver.findElement(By.id("expdate_month")).sendKeys(expireMonth);
			driver.findElement(By.id("expdate_year")).sendKeys(expireYear);
			driver.findElement(By.id("cvv2_number")).sendKeys(ccvNumber);
			
			driver.findElement(By.id("first_name")).sendKeys(fName);
			driver.findElement(By.id("middle_name")).sendKeys(middleName);
			driver.findElement(By.id("last_name")).sendKeys(lastName);
			driver.findElement(By.id("address1")).sendKeys(address1);
			driver.findElement(By.id("city")).sendKeys(city);
			driver.findElement(By.id("state")).sendKeys(state);
			driver.findElement(By.id("zip")).sendKeys(zip);
			driver.findElement(By.id("same_as_billing")).click();
			driver.findElement(By.id("H_PhoneNumber")).sendKeys(hPhoneNumber);
			driver.findElement(By.id("email-address")).sendKeys(email);
			
			driver.findElement(By.id("submitBilling")).click();
			
		} catch (Exception e) {
			System.out.println("Class :" +e.getStackTrace().getClass());
			System.out.println("Line  :" +e.getStackTrace()[2].getLineNumber());
		}
	}
	
}
