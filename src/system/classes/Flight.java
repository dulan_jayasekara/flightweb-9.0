/*Sanoj*/
package system.classes;

/*SANOJ*/

public class Flight 
{	
	private String 		departureDate 				= "";
	private String		departure_port				= "";
	private String		departureLocationCode		= "";
	private String		departureTime				= "";

	private String 		arrivalDate 				= "";
	private String		arrival_port				= "";
	private String		arrivalLocationCode			= "";
	private String 		arrivalTime 				= "";
	
	private String		operatingAirline			= "";
	private String		operatingAirlineCode		= "";
	private String		marketingAirline			= "";
	private String		cabintype					= "";
	private String 		flightNo 					= "";
	private String 		marketingAirline_Loc_Code 	= "";
	private String		fareclass					= "";
	
	private String		segmentStatus				= "";
	
	
	
	public String getOperatingAirlineCode() {
		return operatingAirlineCode;
	}
	public void setOperatingAirlineCode(String operatingAirlineCode) {
		this.operatingAirlineCode = operatingAirlineCode;
	}
	public String getSegmentStatus() {
		return segmentStatus;
	}
	public void setSegmentStatus(String segmentStatus) {
		this.segmentStatus = segmentStatus;
	}
	public String getSegmentInfo() {
		return segmentStatus;
	}
	public void setSegmentInfo(String segmentStatus) {
		this.segmentStatus = segmentStatus;
	}
	public String getFareclass() {
		return fareclass;
	}
	public void setFareclass(String fareclass) {
		this.fareclass = fareclass;
	}
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}
	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getDepartureDate() {
		return departureDate;
	}	
	public String getDepartureTime() {
		return departureTime;
	}
	public String getArrivalTime() {
		return arrivalTime;
	}
	public String getDeparture_port() {
		return departure_port;
	}
	public void setDeparture_port(String departure_port) {
		this.departure_port = departure_port;
	}
	public String getDepartureLocationCode() {
		return departureLocationCode;
	}
	public void setDepartureLocationCode(String departureLocationCode) {
		this.departureLocationCode = departureLocationCode;
	}
	public String getArrival_port() {
		return arrival_port;
	}
	public void setArrival_port(String arrival_port) {
		this.arrival_port = arrival_port;
	}
	public String getArrivalLocationCode() {
		return arrivalLocationCode;
	}
	public void setArrivalLocationCode(String arrivalLocationCode) {
		this.arrivalLocationCode = arrivalLocationCode;
	}
	public String getOperatingAirline() {
		return operatingAirline;
	}
	public void setOperatingAirline(String operatingAirline) {
		this.operatingAirline = operatingAirline;
	}

	public String getMarketingAirline() {
		return marketingAirline;
	}
	public void setMarketingAirline(String marketingAirline) {
		this.marketingAirline = marketingAirline;
	}
	public String getCabintype() {
		return cabintype;
	}
	public void setCabintype(String cabintype) {
		this.cabintype = cabintype;
	}

	public void setxmlDepartureDate(String departureDateTime) {
		String Date = departureDateTime.split("T")[0];
		String Time = departureDateTime.split("T")[1];
		this.departureDate = Date;
		this.departureTime = Time;
		setDepartureTime(Time);
	}
	public String getArrivalDate() {
		return arrivalDate;
	}
	public void setxmlArrivalDate(String arrivalDateTime) {
		String Date = arrivalDateTime.split("T")[0];
		String Time = arrivalDateTime.split("T")[1];
		this.arrivalDate = Date;
		this.arrivalTime = Time;
		setArrivalTime(Time);
	}
	public String getFlightNo() {
		return flightNo;
	}
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}
	
	public String getMarketingAirline_Loc_Code() {
		return marketingAirline_Loc_Code;
	}
	public void setMarketingAirline_Loc_Code(String marketingAirline_Loc_Code) {
		this.marketingAirline_Loc_Code = marketingAirline_Loc_Code;
	}


	public void getAll()
	{
		System.out.println();
		System.out.println("-------Flight Object is called-------");
		System.out.println();
		System.out.println("Departure Airport : "+getDeparture_port());
		System.out.println("Departure Location Code : "+getDepartureLocationCode());
		System.out.println("Arrival airport : "+getArrival_port());
		System.out.println("Arrival Location Code : "+getArrivalLocationCode());
		System.out.println("Opearating Air Line : "+getOperatingAirline());
		System.out.println("Marketing Airline : "+getMarketingAirline());
		System.out.println("Cabin type : "+getCabintype());
		System.out.println();
	}
}
