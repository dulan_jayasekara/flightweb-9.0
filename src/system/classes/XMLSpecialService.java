/*Sanoj*/
package system.classes;

public class XMLSpecialService {
	
	String SSRCode = "";
	String FlightRefNumber = "";
	String TravelerRefNumber = "";
	String Text = "";
	String[] TextArray = null;
	
	
	
	public String[] getTextArray() {
		return TextArray;
	}
	public void setTextArray(String[] textArray) {
		TextArray = textArray;
	}
	public String getSSRCode() {
		return SSRCode;
	}
	public void setSSRCode(String sSRCode) {
		SSRCode = sSRCode;
	}
	public String getFlightRefNumber() {
		return FlightRefNumber;
	}
	public void setFlightRefNumber(String flightRefNumber) {
		FlightRefNumber = flightRefNumber;
	}
	public String getTravelerRefNumber() {
		return TravelerRefNumber;
	}
	public void setTravelerRefNumber(String travelerRefNumber) {
		TravelerRefNumber = travelerRefNumber;
	}
	public String getText() {
		return Text;
	}
	public void setText(String text) {
		Text = text;
		//setTextArray(text.split("-"));
	}
	
}
