/*Sanoj*/
package system.classes;

import java.util.ArrayList;

import system.enumtypes.TO;

public class SearchObject {
	
	String				SearchType					= "";
	String				SellingCurrency 			= "";
	String				Country						= "";
	String				Triptype					= "";
	String				From						= "";
	String				To							= "";
	String				DepartureDate				= "";
	String				DepartureTime				= "";
	String				ReturnDate					= "";
	String				ReturnTime					= "";
	boolean				Flexible					= false;
	String				Adult						= "";
	String				Children					= "";
	ArrayList<String>	ChildrenAge					= null;
	String				infant						= "";
	String				CabinClass					= "";
	String				PreferredCurrency			= "";
	String				PreferredAirline			= "";
	boolean				NonStop						= false;
	String				tracer						= "";	
	String				ProfitType					= "";
	double				Profit						= 0;
	String				selectingFlight				= "";
	String				BookingFeeType				= "";
	double				BookingFee					= 0;
	String				ExcecuteStatus				= "";
	String				paymentMode					= "";
	String				CancellationStatus			= "";
	String				SupplierPayablePayStatus	= "";
	String				airConfigurationStatus		= "";
	String				promotionCode				= "";
	boolean				removecartStatus			= false;
	boolean				Quotation					= false;
	boolean				searchAgain					= false;
	boolean				validateFilters				= false;
	String				TOBooking					= "";
	TO					ToType						= null ;				
	boolean				applyDiscount				= false;
	boolean				applyDiscountAtPayPg		= false;
	String				scenario					= "null";
	String				TOCashCredit				= "";
	
	
	
	
	public String getTOCashCredit() {
		return TOCashCredit;
	}
	public void setTOCashCredit(String tOCashCredit) {
		TOCashCredit = tOCashCredit;
	}
	public String getScenario() {
		return scenario;
	}
	public void setScenario(String scenario) {
		this.scenario = scenario;
	}
	public TO getToType() {
		return ToType;
	}
	public void setToType(TO toType) {
		ToType = toType;
	}
	public boolean isApplyDiscountAtPayPg() {
		return applyDiscountAtPayPg;
	}
	public void setApplyDiscountAtPayPg(boolean applyDiscountAtPayPg) {
		this.applyDiscountAtPayPg = applyDiscountAtPayPg;
	}
	public boolean isApplyDiscount() {
		return applyDiscount;
	}
	public void setApplyDiscount(boolean applyDiscount) {
		this.applyDiscount = applyDiscount;
	}
	public String getTOBooking() {
		return TOBooking;
	}
	public void setTOBooking(String tOBooking) {
		TOBooking = tOBooking;
	}
	public boolean isValidateFilters() {
		return validateFilters;
	}
	public void setValidateFilters(boolean validateFilters) {
		this.validateFilters = validateFilters;
	}
	public boolean isSearchAgain() {
		return searchAgain;
	}
	public void setSearchAgain(boolean searchAgain) {
		this.searchAgain = searchAgain;
	}
	public boolean isQuotation() {
		return Quotation;
	}
	public void setQuotation(boolean quotation) {
		Quotation = quotation;
	}
	public boolean isRemovecartStatus() {
		return removecartStatus;
	}
	public void setRemovecartStatus(boolean Status) {
		removecartStatus = Status;
	}
	public String getPromotionCode() {
		return promotionCode;
	}
	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}
	public String getAirConfigurationStatus() {
		return airConfigurationStatus;
	}
	public void setAirConfigurationStatus(String airConfigurationStatus) {
		this.airConfigurationStatus = airConfigurationStatus;
	}
	public String getSupplierPayablePayStatus() {
		return SupplierPayablePayStatus;
	}
	public void setSupplierPayablePayStatus(String supplierPayablePayStatus) {
		SupplierPayablePayStatus = supplierPayablePayStatus;
	}
	public String getCancellationStatus() {
		return CancellationStatus;
	}
	public void setCancellationStatus(String cancellationStatus) {
		CancellationStatus = cancellationStatus;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public String getExcecuteStatus() {
		return ExcecuteStatus;
	}
	public void setExcecuteStatus(String excecuteStatus) {
		ExcecuteStatus = excecuteStatus;
	}
	public String getBookingFeeType() {
		return BookingFeeType;
	}
	public void setBookingFeeType(String bookingFeeType) {
		BookingFeeType = bookingFeeType;
	}
	public double getBookingFee() {
		return BookingFee;
	}
	public void setBookingFee(double bookingFee) {
		BookingFee = bookingFee;
	}

	public String getSelectingFlight() {
		return selectingFlight;
	}
	public void setSelectingFlight(String selectingFlight) {
		this.selectingFlight = selectingFlight;
	}
	public String getProfitType() {
		return ProfitType;
	}
	public void setProfitType(String profitType) {
		ProfitType = profitType;
	}
	public double getProfit() {
		return Profit;
	}
	public void setProfit(double profit) {
		Profit = profit;
	}

	public String getTracer() {
		return tracer;
	}
	public void setTracer(String tracer) {
		this.tracer = tracer;
	}
	public String getSearchType() {
		return SearchType;
	}
	public void setSearchType(String searchType) {
		SearchType = searchType;
	}
	public String getSellingCurrency() {
		return SellingCurrency;
	}
	public void setSellingCurrency(String sellingCurrency) {
		SellingCurrency = sellingCurrency;
	}
	public String getCountry() {
		return Country;
	}
	public void setCountry(String country) {
		Country = country;
	}
	public String getTriptype() {
		return Triptype;
	}
	public void setTriptype(String triptype) {
		Triptype = triptype;
	}
	public String getFrom() {
		return From;
	}
	public void setFrom(String from) {
		From = from;
	}
	public String getTo() {
		return To;
	}
	public void setTo(String to) {
		To = to;
	}
	public String getDepartureDate() {
		return DepartureDate;
	}
	public void setDepartureDate(String departureDate) {
		DepartureDate = departureDate;
	}
	public String getDepartureTime() {
		return DepartureTime;
	}
	public void setDepartureTime(String departureTime) {
		DepartureTime = departureTime;
	}
	public String getReturnDate() {
		return ReturnDate;
	}
	public void setReturnDate(String returnDate) {
		ReturnDate = returnDate;
	}
	public String getReturnTime() {
		return ReturnTime;
	}
	public void setReturnTime(String returnTime) {
		ReturnTime = returnTime;
	}
	public boolean isFlexible() {
		return Flexible;
	}
	public void setFlexible(boolean flexible) {
		Flexible = flexible;
	}
	public String getAdult() {
		return Adult;
	}
	public void setAdult(String adult) {
		Adult = adult;
	}
	public String getChildren() {
		return Children;
	}
	public void setChildren(String children) {
		Children = children;
	}
	public ArrayList<String> getChildrenAge() {
		return ChildrenAge;
	}
	public void setChildrenAge(ArrayList<String> childrenAge) {
		ChildrenAge = childrenAge;
	}
	public String getInfant() {
		return infant;
	}
	public void setInfant(String infant) {
		this.infant = infant;
	}
	public String getCabinClass() {
		return CabinClass;
	}
	public void setCabinClass(String cabinClass) {
		CabinClass = cabinClass;
	}
	public String getPreferredCurrency() {
		return PreferredCurrency;
	}
	public void setPreferredCurrency(String preferredCurrency) {
		PreferredCurrency = preferredCurrency;
	}
	public String getPreferredAirline() {
		return PreferredAirline;
	}
	public void setPreferredAirline(String preferredAirline) {
		PreferredAirline = preferredAirline;
	}
	public boolean isNonStop() {
		return NonStop;
	}
	public void setNonStop(boolean nonStop) {
		NonStop = nonStop;
	}
}
