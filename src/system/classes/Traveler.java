/*Sanoj*/
package system.classes;

public class Traveler {
	
	String  passenger			= "";
	String  age					= "";
	
	String  passengertypeCode 	= "";
	String  NamePrefixTitle 	= "Mr";
	String  givenName 			= "";
	String  Surname 			= "";
	String  phoneNumber 		= "";
	String  EmergencyNo 		= "";
	
	String  passportNo 			= "";
	String  passprtIssuDate 	= "";
	String  passportExpDate 	= "";
	String  birthDay 			= "";
	String  gender 				= "";
	String  nationality 		= "";
	String  passportIssuCountry = "";
	
	String  TravelerRefNo		= "";
	String  CusLoyalProgID		= "";
	String  MembershipID		= "";
	Address address				= null;
	String  email				= "";
	String  altemail			= "";
	
	String  phoneType			= "";
	
	String  Frequent_Flyer		= "";
	String  Frequent_Flyer_No	= "";
	String  ETicket_No			= "";
	
	RatesperPassenger rate		= null;
	

	public RatesperPassenger getRate() {
		return rate;
	}
	public void setRate(RatesperPassenger rate) {
		this.rate = rate;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	
	public String getPassenger() {
		return passenger;
	}
	public void setPassenger(String passenger) {
		this.passenger = passenger;
	}

	public String getAltemail() {
		return altemail;
	}
	public void setAltemail(String altemail) {
		this.altemail = altemail;
	}
	public String getEmergencyNo() {
		return EmergencyNo;
	}
	public void setEmergencyNo(String emergencyNo) {
		EmergencyNo = emergencyNo;
	}
	public String getFrequent_Flyer() {
		return Frequent_Flyer;
	}
	public void setFrequent_Flyer(String frequent_Flyer) {
		Frequent_Flyer = frequent_Flyer;
	}
	public String getFrequent_Flyer_No() {
		return Frequent_Flyer_No;
	}
	public void setFrequent_Flyer_No(String frequent_Flyer_No) {
		Frequent_Flyer_No = frequent_Flyer_No;
	}
	public String getETicket_No() {
		return ETicket_No;
	}
	public void setETicket_No(String eTicket_No) {
		ETicket_No = eTicket_No;
	}
	
	public String getPassengertypeCode() {
		return passengertypeCode;
	}
	public void setPassengertypeCode(String passengertypeCode) {
		this.passengertypeCode = passengertypeCode;
	}
	public String getNamePrefixTitle() {
		return NamePrefixTitle;
	}
	public void setNamePrefixTitle(String namePrefixTitle) {
		NamePrefixTitle = namePrefixTitle;
	}
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public String getSurname() {
		return Surname;
	}
	public void setSurname(String surname) {
		Surname = surname;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPassportNo() {
		return passportNo;
	}
	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}
	public String getPassprtIssuDate() {
		return passprtIssuDate;
	}
	public void setPassprtIssuDate(String passprtIssuDate) {
		this.passprtIssuDate = passprtIssuDate;
	}
	public String getPassportExpDate() {
		return passportExpDate;
	}
	public void setPassportExpDate(String passportExpDate) {
		this.passportExpDate = passportExpDate;
	}
	public String getBirthDay() {
		return birthDay;
	}
	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getPassportIssuCountry() {
		return passportIssuCountry;
	}
	public void setPassportIssuCountry(String passportIssuCountry) {
		this.passportIssuCountry = passportIssuCountry;
	}
	public String getTravelerRefNo() {
		return TravelerRefNo;
	}
	public void setTravelerRefNo(String travelerRefNo) {
		TravelerRefNo = travelerRefNo;
	}
	public String getCusLoyalProgID() {
		return CusLoyalProgID;
	}
	public void setCusLoyalProgID(String cusLoyalProgID) {
		CusLoyalProgID = cusLoyalProgID;
	}
	public String getMembershipID() {
		return MembershipID;
	}
	public void setMembershipID(String membershipID) {
		MembershipID = membershipID;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneType() {
		return phoneType;
	}
	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}
	
}
