package system.classes;

import java.util.ArrayList;

public class UIConfirmationPage 
 {
	boolean Available = false;
	String departure = "";
	String destination = "";
	String fromDate = "";
	String toDate = "";

	String bookingReference = "";
	String reservationNo = "";
	String supplierConfirmationNo = "";

	Outbound outbound = null;
	Inbound inbound = null;

	ArrayList<RatesperPassenger> ratelist = null;

	UIPriceInfo uisummarypay = null;
	UICreditCardPayInfo creditcardpay = null;

	String cancellationdate = "";
	String PackageDeadline = "";

	Traveler maincustomer = null;
	ArrayList<Traveler> adult = null;
	ArrayList<Traveler> children = null;
	ArrayList<Traveler> infant = null;

	public boolean isAvailable() {
		return Available;
	}

	public void setAvailable(boolean available) {
		Available = available;
	}

	public String getPackageDeadline() {
		return PackageDeadline;
	}

	public void setPackageDeadline(String packageDeadline) {
		PackageDeadline = packageDeadline;
	}

	public String getDeparture() {
		return departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getBookingReference() {
		return bookingReference;
	}

	public void setBookingReference(String bookingReference) {
		this.bookingReference = bookingReference;
	}

	public String getReservationNo() {
		return reservationNo;
	}

	public void setReservationNo(String reservationNo) {
		this.reservationNo = reservationNo;
	}

	public String getSupplierConfirmationNo() {
		return supplierConfirmationNo;
	}

	public void setSupplierConfirmationNo(String supplierConfirmationNo) {
		this.supplierConfirmationNo = supplierConfirmationNo;
	}

	public Outbound getOutbound() {
		return outbound;
	}

	public void setOutbound(Outbound outbound) {
		this.outbound = outbound;
	}

	public Inbound getInbound() {
		return inbound;
	}

	public void setInbound(Inbound inbound) {
		this.inbound = inbound;
	}

	public ArrayList<RatesperPassenger> getRatelist() {
		return ratelist;
	}

	public void setRatelist(ArrayList<RatesperPassenger> ratelist) {
		this.ratelist = ratelist;
	}

	public UIPriceInfo getUisummarypay() {
		return uisummarypay;
	}

	public void setUisummarypay(UIPriceInfo uisummarypay) {
		this.uisummarypay = uisummarypay;
	}

	public UICreditCardPayInfo getCreditcardpay() {
		return creditcardpay;
	}

	public void setCreditcardpay(UICreditCardPayInfo creditcardpay) {
		this.creditcardpay = creditcardpay;
	}

	public String getCancellationdate() {
		return cancellationdate;
	}

	public void setCancellationdate(String cancellationdate) {
		this.cancellationdate = cancellationdate;
	}

	public Traveler getMaincustomer() {
		return maincustomer;
	}

	public void setMaincustomer(Traveler maincustomer) {
		this.maincustomer = maincustomer;
	}

	public ArrayList<Traveler> getAdult() {
		return adult;
	}

	public void setAdult(ArrayList<Traveler> adult) {
		this.adult = adult;
	}

	public ArrayList<Traveler> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<Traveler> children) {
		this.children = children;
	}

	public ArrayList<Traveler> getInfant() {
		return infant;
	}

	public void setInfant(ArrayList<Traveler> infant) {
		this.infant = infant;
	}

}
