/*Sanoj*/
package system.classes;

import java.util.ArrayList;

public class UIPaymentPage 
{
	String departure						= "";
	String destination						= "";
	String fromDate							= "";
	String toDate							= "";
	ArrayList<RatesperPassenger> ratelist	= null;
	
	Outbound outbound						= null;
	Inbound inbound							= null;
	
	UIPriceInfo uisummarypay				= null;
	UICreditCardPayInfo creditcardpay		= null;
		
	String amendmentemail					= "";
	String amendmentphone					= "";
	String cancellationdate					= "";
	String PackageDeadline					= "";
	
	
	
	public String getPackageDeadline() {
		return PackageDeadline;
	}
	public void setPackageDeadline(String packageDeadline) {
		PackageDeadline = packageDeadline;
	}
	public String getCancellationdate() {
		return cancellationdate;
	}
	public void setCancellationdate(String cancellationdate) {
		this.cancellationdate = cancellationdate;
	}
	public String getDeparture() {
		return departure;
	}
	public void setDeparture(String departure) {
		this.departure = departure;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public ArrayList<RatesperPassenger> getRatelist() {
		return ratelist;
	}
	public void setRatelist(ArrayList<RatesperPassenger> ratelist) {
		this.ratelist = ratelist;
	}
	public Outbound getOutbound() {
		return outbound;
	}
	public void setOutbound(Outbound outbound) {
		this.outbound = outbound;
	}
	public Inbound getInbound() {
		return inbound;
	}
	public void setInbound(Inbound inbound) {
		this.inbound = inbound;
	}
	public UICreditCardPayInfo getCreditcardpay() {
		return creditcardpay;
	}
	public void setCreditcardpay(UICreditCardPayInfo creditcardpay) {
		this.creditcardpay = creditcardpay;
	}
	public UIPriceInfo getUisummarypay() {
		return uisummarypay;
	}
	public void setUisummarypay(UIPriceInfo uisummarypay) {
		this.uisummarypay = uisummarypay;
	}
	public String getAmendmentemail() {
		return amendmentemail;
	}
	public void setAmendmentemail(String amendmentemail) {
		this.amendmentemail = amendmentemail;
	}
	public String getAmendmentphone() {
		return amendmentphone;
	}
	public void setAmendmentphone(String amendmentphone) {
		this.amendmentphone = amendmentphone;
	}
	
}
