package system.classes;

import system.enumtypes.ConfigFareType;

public class AirConfig 
{
	int testNo						= 1;
	public int getTestNo() {
		return testNo;
	}
	public void setTestNo(int testNo) {
		this.testNo = testNo;
	}
	String AirLine					= "";
	
	ConfigFareType  PubFareType		= null;
	String PubBookingFee			= "";
	String PubProfitMarkup			= "";
	String PubBoth					= "";
	String PubNone					= "";
	
	ConfigFareType  PvtFareType		= null;
	String PvtBookingFee			= "";
	String PvtProfitMarkup			= "";
	String PvtBoth					= "";
	
	String FTypeConfgShopCart		= "";//1 value
	String FTypeConfgFplusH			= "";//2 value
	String FTypeConfgFixPack		= "";//2 value
	
	String FlightPayOptCartBooking	= "";//3 value
	String FlightPayOptPayfullFplusH= "";//1 value
	
	
	public String getAirLine() {
		return AirLine;
	}
	public void setAirLine(String airLine) {
		AirLine = airLine;
	}
	public ConfigFareType getPubFareType() {
		return PubFareType;
	}
	public void setPubFareType(ConfigFareType pubFareType) {
		PubFareType = pubFareType;
	}
	public ConfigFareType getPvtFareType() {
		return PvtFareType;
	}
	public void setPvtFareType(ConfigFareType pvtFareType) {
		PvtFareType = pvtFareType;
	}
	public String getFTypeConfgShopCart() {
		return FTypeConfgShopCart;
	}
	public void setFTypeConfgShopCart(String fTypeConfgShopCart) {
		FTypeConfgShopCart = fTypeConfgShopCart;
	}
	public String getFTypeConfgFplusH() {
		return FTypeConfgFplusH;
	}
	public void setFTypeConfgFplusH(String fTypeConfgFplusH) {
		FTypeConfgFplusH = fTypeConfgFplusH;
	}
	public String getFTypeConfgFixPack() {
		return FTypeConfgFixPack;
	}
	public void setFTypeConfgFixPack(String fTypeConfgFixPack) {
		FTypeConfgFixPack = fTypeConfgFixPack;
	}
	public String getFlightPayOptCartBooking() {
		return FlightPayOptCartBooking;
	}
	public void setFlightPayOptCartBooking(String flightPayOptCartBooking) {
		FlightPayOptCartBooking = flightPayOptCartBooking;
	}
	public String getFlightPayOptPayfullFplusH() {
		return FlightPayOptPayfullFplusH;
	}
	public void setFlightPayOptPayfullFplusH(String payfullFplusH) {
		FlightPayOptPayfullFplusH = payfullFplusH;
	}
	
}
