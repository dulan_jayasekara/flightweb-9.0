package system.classes;

public class ReservationReport
{
	boolean available				= false;
	
	String reservationNo			= "";
	String documentNo				= "";
	String bookingDate				= "";
	String confirmationDue			= "";
	
	String customerType				= "";
	String customerName				= "";
	
	String supplierName				= "";
	String supplierConfirmationNo	= "";

	String productType				= "";
	String bookingStatus			= "";
	String bookingChannel			= "";
	String consultantName			= "";
	
	String guestName				= "";
	String guestContactNo			= "";
	String approvalCode				= "";
	String transactionID			= "";
	
	String voucherIssued			= "";
	String invoiceIssued			= "";
	String invoiceIssuedDate		= "";
	
	String LPOrequired				= "";
	String LPOprovided				= "";
	String LPOno					= "";
	
	//SELLING
	String sellingCurrency1			= "";  //Match with SearchObject Selling Currency
	String totalRate				= "0"; //Match with XML base + profit + tax costs
	String creditcardFee			= "0";
	String bookingFeeAndCharges		= "0";
	String amountPaid				= "0";
	
	String sellingCurrency2			= "";
	String grossOrderValue			= "0";
	String agentCommission			= "0";
	String netOrderValue			= "0";
	String totalCost				= "0";
	
	String baseCurrency				= "";
	String baseNetOrderValue		= "0";
	String basetotalCost			= "0";
	
	String pageTotal				= "0";
	String grandTotal				= "0";
	
	
	
	public boolean isAvailable() {
		return available;
	}
	public void setAvailable(boolean available) {
		this.available = available;
	}
	public String getReservationNo() {
		return reservationNo;
	}
	public void setReservationNo(String reservationNo) {
		this.reservationNo = reservationNo;
	}
	public String getDocumentNo() {
		return documentNo;
	}
	public void setDocumentNo(String documentNo) {
		this.documentNo = documentNo;
	}
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getConfirmationDue() {
		return confirmationDue;
	}
	public void setConfirmationDue(String confirmationDue) {
		this.confirmationDue = confirmationDue;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getSupplierConfirmationNo() {
		return supplierConfirmationNo;
	}
	public void setSupplierConfirmationNo(String supplierConfirmationNo) {
		this.supplierConfirmationNo = supplierConfirmationNo;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getBookingStatus() {
		return bookingStatus;
	}
	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}
	public String getBookingChannel() {
		return bookingChannel;
	}
	public void setBookingChannel(String bookingChannel) {
		this.bookingChannel = bookingChannel;
	}
	public String getConsultantName() {
		return consultantName;
	}
	public void setConsultantName(String consultantName) {
		this.consultantName = consultantName;
	}
	public String getGuestName() {
		return guestName;
	}
	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}
	public String getGuestContactNo() {
		return guestContactNo;
	}
	public void setGuestContactNo(String guestContactNo) {
		this.guestContactNo = guestContactNo;
	}
	public String getApprovalCode() {
		return approvalCode;
	}
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}
	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	public String getVoucherIssued() {
		return voucherIssued;
	}
	public void setVoucherIssued(String voucherIssued) {
		this.voucherIssued = voucherIssued;
	}
	public String getInvoiceIssued() {
		return invoiceIssued;
	}
	public void setInvoiceIssued(String invoiceIssued) {
		this.invoiceIssued = invoiceIssued;
	}
	public String getInvoiceIssuedDate() {
		return invoiceIssuedDate;
	}
	public void setInvoiceIssuedDate(String invoiceIssuedDate) {
		this.invoiceIssuedDate = invoiceIssuedDate;
	}
	public String getLPOrequired() {
		return LPOrequired;
	}
	public void setLPOrequired(String lPOrequired) {
		LPOrequired = lPOrequired;
	}
	public String getLPOprovided() {
		return LPOprovided;
	}
	public void setLPOprovided(String lPOprovided) {
		LPOprovided = lPOprovided;
	}
	public String getLPOno() {
		return LPOno;
	}
	public void setLPOno(String lPOno) {
		LPOno = lPOno;
	}
	public String getSellingCurrency1() {
		return sellingCurrency1;
	}
	public void setSellingCurrency1(String sellingCurrency1) {
		this.sellingCurrency1 = sellingCurrency1;
	}
	public String getTotalRate() {
		return totalRate;
	}
	public void setTotalRate(String totalRate) {
		this.totalRate = totalRate;
	}
	public String getCreditcardFee() {
		return creditcardFee;
	}
	public void setCreditcardFee(String creditcardFee) {
		this.creditcardFee = creditcardFee;
	}
	public String getBookingFeeAndCharges() {
		return bookingFeeAndCharges;
	}
	public void setBookingFeeAndCharges(String bookingFeeAndCharges) {
		this.bookingFeeAndCharges = bookingFeeAndCharges;
	}
	public String getAmountPaid() {
		return amountPaid;
	}
	public void setAmountPaid(String amountPaid) {
		this.amountPaid = amountPaid;
	}
	public String getSellingCurrency2() {
		return sellingCurrency2;
	}
	public void setSellingCurrency2(String sellingCurrency2) {
		this.sellingCurrency2 = sellingCurrency2;
	}
	public String getGrossOrderValue() {
		return grossOrderValue;
	}
	public void setGrossOrderValue(String grossOrderValue) {
		this.grossOrderValue = grossOrderValue;
	}
	public String getAgentCommission() {
		return agentCommission;
	}
	public void setAgentCommission(String agentCommission) {
		this.agentCommission = agentCommission;
	}
	public String getNetOrderValue() {
		return netOrderValue;
	}
	public void setNetOrderValue(String netOrderValue) {
		this.netOrderValue = netOrderValue;
	}
	public String getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}
	public String getBaseCurrency() {
		return baseCurrency;
	}
	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}
	public String getBaseNetOrderValue() {
		return baseNetOrderValue;
	}
	public void setBaseNetOrderValue(String baseNetOrderValue) {
		this.baseNetOrderValue = baseNetOrderValue;
	}
	public String getBasetotalCost() {
		return basetotalCost;
	}
	public void setBasetotalCost(String basetotalCost) {
		this.basetotalCost = basetotalCost;
	}
	public String getPageTotal() {
		return pageTotal;
	}
	public void setPageTotal(String pageTotal) {
		this.pageTotal = pageTotal;
	}
	public String getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(String grandTotal) {
		this.grandTotal = grandTotal;
	}
	
	
}
