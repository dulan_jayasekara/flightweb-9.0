package system.classes;

public class UIPriceInfo 
{
	String currencycode			= "";
	String totalbefore			= "0";//newbase + profit
	String tax					= "0";//newtax
	String bookingfee			= "0";//booking fee
	String totalCost			= "0";
	
	String subtotal				= "0";//newbase + profit
	String bookingFee			= "0";
	public String getBookingFee() {
		return bookingFee;
	}
	public void setBookingFee(String bookingFee) {
		this.bookingFee = bookingFee;
	}
	String taxplusother			= "0";//newtax + booking fee + credit card cost
	String Total				= "0";// subtotal + taxplusother
	String amountprocess		= "0";// ALL
	String amountprocessAirline = "0";
	
	String discount				= "0";
	

	
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public String getCurrencycode() {
		return currencycode;
	}
	public void setCurrencycode(String currencycode) {
		this.currencycode = currencycode;
	}
	public String getTotalbefore() {
		return totalbefore;
	}
	public void setTotalbefore(String totalbefore) {
		this.totalbefore = totalbefore;
	}
	public String getTax() {
		return tax;
	}
	public void setTax(String tax) {
		this.tax = tax;
	}
	public String getBookingfee() {
		return bookingfee;
	}
	public void setBookingfee(String bookingfee) {
		this.bookingfee = bookingfee;
	}
	public String getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}
	public String getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(String subtotal) {
		this.subtotal = subtotal;
	}
	public String getTaxplusother() {
		return taxplusother;
	}
	public void setTaxplusother(String taxplusother) {
		this.taxplusother = taxplusother;
	}
	public String getTotal() {
		return Total;
	}
	public void setTotal(String total) {
		Total = total;
	}
	public String getAmountprocess() {
		return amountprocess;
	}
	public void setAmountprocess(String amountprocess) {
		this.amountprocess = amountprocess;
	}
	public String getAmountprocessAirline() {
		return amountprocessAirline;
	}
	public void setAmountprocessAirline(String amountprocessAirline) {
		this.amountprocessAirline = amountprocessAirline;
	}
	
}
