package system.classes;

public class IntermediateInfo 
{
	String intermediateStatus = "";
	String intermediateMessage = "";
	
	String payAmount = "";
	String payAmountCurrency = "";
	
	String CreditCPaySumeryPage = "";

	
	public String getCreditCPaySumeryPage() {
		return CreditCPaySumeryPage;
	}
	public void setCreditCPaySumeryPage(String creditCPaySumeryPage) {
		CreditCPaySumeryPage = creditCPaySumeryPage;
	}
	public String getIntermediateStatus() {
		return intermediateStatus;
	}
	public void setIntermediateStatus(String intermediateStatus) {
		this.intermediateStatus = intermediateStatus;
	}
	public String getIntermediateMessage() {
		return intermediateMessage;
	}
	public void setIntermediateMessage(String intermediateMessage) {
		this.intermediateMessage = intermediateMessage;
	}
	public String getPayAmount() {
		return payAmount;
	}
	public void setPayAmount(String payAmount) {
		this.payAmount = payAmount;
	}
	public String getPayAmountCurrency() {
		return payAmountCurrency;
	}
	public void setPayAmountCurrency(String payAmountCurrency) {
		this.payAmountCurrency = payAmountCurrency;
	}
	
}
