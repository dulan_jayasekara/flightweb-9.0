package system.classes;

import java.util.ArrayList;

public class ReservationInfo 
{	
	Traveler maincustomer = null;
	ArrayList<Traveler> adult = null;
	ArrayList<Traveler> children = null;
	ArrayList<Traveler> infant = null; 
	
	public Traveler getMaincustomer() {
		return maincustomer;
	}
	public void setMaincustomer(Traveler maincustomer) {
		this.maincustomer = maincustomer;
	}
	public ArrayList<Traveler> getAdult() {
		return adult;
	}
	public void setAdult(ArrayList<Traveler> adult) {
		this.adult = adult;
	}
	public ArrayList<Traveler> getChildren() {
		return children;
	}
	public void setChildren(ArrayList<Traveler> children) {
		this.children = children;
	}
	public ArrayList<Traveler> getInfant() {
		return infant;
	}
	public void setInfant(ArrayList<Traveler> infant) {
		this.infant = infant;
	}


}
