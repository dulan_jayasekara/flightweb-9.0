/*Sanoj*/
package system.classes;

import java.util.Map;

import com.common.Validators.CommonValidator;

public class XMLPriceInfo
{
	private 	String			BasefareAmount					= "0";
	private 	String			BasefareCurrencyCode			= "";
	private 	String			TaxCode							= "";
	private 	String			TaxAmount						= "0";
	private 	String			TaxCurrencyCode					= "";
	private		String			TotalFare						= "0";
	private		String			TotalFareCurrencyCode			= "";
	private		int				TotalFareDecimal				= 2;
	private		int				TaxAmountDecimal				= 2;
	private		int				BasefareAmountDecimal			= 2;
	
	private		String			EquivFare						= "";
	private		String			EquivCurrencyCode				= "";
	private		int				EquivDecimal					= 0;
	
	private		String			PricingSource					= "Published";
	
	private 	String			newBasefareCurrencyCode			= "";
	private		double			newbase							= 0;
	private		double			newtax							= 0;
	private		double			SellingCost						= 0;
	private		double			profit							= 0;
	private		double			bookingfee						= 0;
	private		double			creditcardfeeinSellCurr			= 0;
	private		double			processnow						= 0;
	
	private		String			portalCurrencyCode				= "";
	private		double			baseFareInPortalCurrency		= 0;
	private		double			taxFareInPortalCurrency			= 0;
	private		double			profitInPortalCurrency			= 0;
	private		double			bookingFeeInPortalCurrency		= 0;
	private		double			creditcardFeeInPortalCurrency	= 0;
	
	private		boolean			ischanged						= false;
	private		double			sellCostDiscount				= 0;
	private		double			discount						= 0;
	private		double			discountInPortalCurr			= 0;
	private		double			discountInSellingCurr			= 0;
	
	
	public double getDiscountInPortalCurr() {
		return discountInPortalCurr;
	}

	public void setDiscountInPortalCurr(double discountInPortal) {
		this.discountInPortalCurr = discountInPortal;
	}

	public double getDiscountInSellingCurr() {
		return discountInSellingCurr;
	}

	public void setDiscountInSellingCurr(double discountInSelling) {
		this.discountInSellingCurr = discountInSelling;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public void setDiscount(String discountType, String value, String currency, Map<String, String> CurrencyMap)
	{
		double val = Double.parseDouble(value);
		
		if(newBasefareCurrencyCode.equalsIgnoreCase(currency))
		{
			if(discountType.equalsIgnoreCase("Percentage"))
			{
				discount			  = (newbase + profit) * (val/100); 
				discount			  = Math.ceil(discount);
				sellCostDiscount	  = SellingCost - discount;
				SellingCost			  = Math.ceil(sellCostDiscount);
			}
			else if(discountType.equalsIgnoreCase("Value"))
			{
				//discount			  = (newbase + profit) - val;
				discount			  = Math.ceil(discount);
				discountInSellingCurr = Math.ceil( CommonValidator.convert(currency, newBasefareCurrencyCode, currency, val, CurrencyMap) );
				discountInPortalCurr  = val;
				sellCostDiscount	  = SellingCost - val;
				SellingCost			  = Math.ceil(sellCostDiscount);
			}
		}
		else
		{
			if(discountType.equalsIgnoreCase("Percentage"))
			{
				discount			  = (newbase + profit) * (val/100); 
				discount			  = Math.ceil(discount);
				sellCostDiscount	  = SellingCost - discount;
				SellingCost			  = Math.ceil(sellCostDiscount);
			}
			else if(discountType.equalsIgnoreCase("Value"))
			{
				discountInSellingCurr = CommonValidator.convert(currency, newBasefareCurrencyCode, currency, val, CurrencyMap);
				discountInSellingCurr = Math.ceil(discountInSellingCurr);
				discountInPortalCurr  = val;
				sellCostDiscount	  = SellingCost - discountInSellingCurr;
				SellingCost			  = sellCostDiscount;
			}
		}	
	}
	
	public double getSellCostDiscount() {
		return sellCostDiscount;
	}
	
	public void setSellCostDiscount(double sellCostDiscount) {
		this.sellCostDiscount = sellCostDiscount;
	}
	
	public double getCreditcardFeeInPortalCurrency() {
		return creditcardFeeInPortalCurrency;
	}
	public void setCreditcardFeeInPortalCurrency(
			double creditcardFeeInPortalCurrency) {
		this.creditcardFeeInPortalCurrency = creditcardFeeInPortalCurrency;
	}
	public String getPortalCurrencyCode() {
		return portalCurrencyCode;
	}
	public void setPortalCurrencyCode(String portalCurrencyCode) {
		this.portalCurrencyCode = portalCurrencyCode;
	}
	public double getBaseFareInPortalCurrency() {
		return baseFareInPortalCurrency;
	}
	public void setBaseFareInPortalCurrency(double baseFareInPortalCurrency) {
		this.baseFareInPortalCurrency = baseFareInPortalCurrency;
	}
	public double getTaxFareInPortalCurrency() {
		return taxFareInPortalCurrency;
	}
	public void setTaxFareInPortalCurrency(double taxFareInPortalCurrency) {
		this.taxFareInPortalCurrency = taxFareInPortalCurrency;
	}
	public double getProfitInPortalCurrency() {
		return profitInPortalCurrency;
	}
	public void setProfitInPortalCurrency(double profitInPortalCurrency) {
		this.profitInPortalCurrency = profitInPortalCurrency;
	}
	public double getBookingFeeInPortalCurrency() {
		return bookingFeeInPortalCurrency;
	}
	public void setBookingFeeInPortalCurrency(double bookingFeeInPortalCurrency) {
		this.bookingFeeInPortalCurrency = bookingFeeInPortalCurrency;
	}
	public double getProcessnow() {
		return processnow;
	}
	public void setProcessnow(double processnow) {
		this.processnow = processnow;
	}
	public double getCreditcardfeeinSellCurr() {
		return creditcardfeeinSellCurr;
	}
	public void setCreditcardfeeinSellCurr(double creditcardfeeinSellCurr) {
		this.creditcardfeeinSellCurr = creditcardfeeinSellCurr;
	}

	public String getNewBasefareCurrencyCode() {
		return newBasefareCurrencyCode;
	}
	public void setNewBasefareCurrencyCode(String newBasefareCurrencyCode) {
		this.newBasefareCurrencyCode = newBasefareCurrencyCode;
	}
	public double getNewbase() {
		return newbase;
	}
	public void setNewbase(double newbase) {
		this.newbase = newbase;
	}
	public double getNewtax() {
		return newtax;
	}
	public void setNewtax(double newtax) {
		this.newtax = newtax;
	}
	public boolean isIschanged() {
		return ischanged;
	}
	public void setIschanged(boolean ischanged) {
		this.ischanged = ischanged;
	}
	public double getBookingfee() {
		return bookingfee;
	}
	public void setBookingfee(double bookingfee) {
		this.bookingfee = bookingfee;
	}
	public double getProfit() {
		return profit;
	}
	public void setProfit(double profit) {
		this.profit = profit;
	}
	public double getSellingCost() {
		return SellingCost;
	}
	public void setSellingCost(double sellingCost) {
		SellingCost = sellingCost;
	}
	public String getEquivFare() {
		return EquivFare;
	}
	public void setEquivFare(String equivFare) {
		EquivFare = equivFare;
	}
	public String getEquivCurrencyCode() {
		return EquivCurrencyCode;
	}
	public void setEquivCurrencyCode(String equivCurrencyCode) {
		EquivCurrencyCode = equivCurrencyCode;
	}
	public int getEquivDecimal() {
		return EquivDecimal;
	}
	public void setEquivDecimal(String equivDecimal) {
		EquivDecimal = Integer.parseInt(equivDecimal);
	}

	public String getTotalFareCurrencyCode() {
		return TotalFareCurrencyCode;
	}
	public void setTotalFareCurrencyCode(String totalFareCurrencyCode) {
		TotalFareCurrencyCode = totalFareCurrencyCode;
	}

	public int getTotalFareDecimal() {
		return TotalFareDecimal;
	}
	public void setTotalFareDecimal(String totalFareDecimal) {
		try
		{
			TotalFareDecimal = Integer.parseInt(totalFareDecimal);
		}
		catch(Exception e)
		{
			
		}
		
	}
	public int getTaxAmountDecimal() {
		return TaxAmountDecimal;
	}
	public void setTaxAmountDecimal(String taxAmountDecimal) {
		try
		{
			TaxAmountDecimal = Integer.parseInt(taxAmountDecimal);
		}
		catch(Exception e){
			
		}
		
	}
	public int getBasefareAmountDecimal() {
		return BasefareAmountDecimal;
	}
	public void setBasefareAmountDecimal(String basefareAmountDecimal) {
		try
		{
			BasefareAmountDecimal = Integer.parseInt(basefareAmountDecimal);
		}
		catch(Exception e)
		{
			
		}
		
	}

	public String getTotalFare() {
		
		return TotalFare;
	}
	public void setTotalFare(String totalFare) {
		
		TotalFare = totalFare;
	}

	public String getPricingSource() {
		return PricingSource;
	}
	public void setPricingSource(String pricingSource) {
		PricingSource = pricingSource;
	}
	public String getBasefareAmount() {
		return BasefareAmount;
	}
	public void setBasefareAmount(String string) {
		BasefareAmount = string;
	}
	public String getBasefareCurrencyCode() {
		return BasefareCurrencyCode;
	}
	public void setBasefareCurrencyCode(String basefareCurrencyCode) {
		BasefareCurrencyCode = basefareCurrencyCode;
	}
	public String getTaxCode() {
		return TaxCode;
	}
	public void setTaxCode(String taxCode) {
		TaxCode = taxCode;
	}
	public String getTaxAmount() {
		return TaxAmount;
	}
	public void setTaxAmount(String taxAmount) {
		TaxAmount = taxAmount;
	}
	public String getTaxCurrencyCode() {
		return TaxCurrencyCode;
	}
	public void setTaxCurrencyCode(String taxCurrencyCode) {
		TaxCurrencyCode = taxCurrencyCode;
	}

	public void getAll()
	{
		System.out.println();
		System.out.println("-----Price info is called----");
		System.out.println();
		System.out.println("Pricing Source : "+getPricingSource());
		System.out.println("Base fare amount : "+getBasefareAmount());
		System.out.println("Base fare Decimal places : "+getBasefareAmountDecimal());
		System.out.println("Basa fare currency code : "+getBasefareCurrencyCode());
		System.out.println("Tax code : "+getTaxCode());
		System.out.println("Tax amount : "+getTaxAmount());
		System.out.println("Tax amount decimal : "+getTaxAmountDecimal());
		System.out.println("Tax currency code : "+getTaxCurrencyCode());
		System.out.println("Total Fare : "+getTotalFare());
		System.out.println("Total fare decimal : "+getTotalFareDecimal());
		System.out.println();
	}
	
}
