/*Sanoj*/
package system.classes;

import java.util.ArrayList;


public class CartFlight 
{
	
	String LeaveAirport			= "";
	String ReturnAirport		= "";
	//String LeaveDate			= "";
	//String ReturnDate			= "";
	String LeaveTime			= "";
	String ReturnTime			= "";
	String LeavingFlightNO		= "";
	String ReturningFlightNO	= "";
	String Total				= "0";
	
	Outbound	outbound		= null;	
	Inbound		inbound			= null;
	
	String		CurrencyCode	= "";
	
	String		Subtotal1		= "0";
	String		taxesandOther1	= "0";
	String		totalforflight1 = "0";
	
	String		SubtotalFinal			= "0";
	String		taxesandOtherFinal		= "0";
	String		totalforflightFinal 	= "0";
	String		amountprocessnowFinal 	= "0";
	String 		amountbyairlineFinal	= "0";
	String 		finalvalue				= "0";
	
	ArrayList<String> flights			= null;
	
	String totalBeforeTax	= "0";
	String totalTax		= "0";
	String bookingFee		= "0";
	String totalCost		= "0";
	
	
	
	public String getTotalBeforeTax() {
		return totalBeforeTax;
	}
	public void setTotalBeforeTax(String totalBeforeTax) {
		this.totalBeforeTax = totalBeforeTax;
	}
	public String getTotalTax() {
		return totalTax;
	}
	public void setTotalTax(String totalTax) {
		this.totalTax = totalTax;
	}
	public String getBookingFee() {
		return bookingFee;
	}
	public void setBookingFee(String bookingFee) {
		this.bookingFee = bookingFee;
	}
	public String getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}
	public String getAmountbyairlineFinal() {
		return amountbyairlineFinal;
	}
	public Outbound getOutbound() {
		return outbound;
	}
	public void setOutbound(Outbound outbound) {
		this.outbound = outbound;
	}
	public Inbound getInbound() {
		return inbound;
	}
	public void setInbound(Inbound inbound) {
		this.inbound = inbound;
	}

	public ArrayList<String> getFlights() {
		return flights;
	}
	public void setFlights(ArrayList<String> flights) {
		this.flights = flights;
	}
	public String getAmountprocessnowFinal() {
		return amountprocessnowFinal;
	}
	public void setAmountprocessnowFinal(String amountprocessnowFinal) {
		this.amountprocessnowFinal = amountprocessnowFinal;
	}
	
	public String getSubtotal1() {
		return Subtotal1;
	}
	public void setSubtotal1(String subtotal1) {
		Subtotal1 = subtotal1;
	}
	public String getTaxesandOther1() {
		return taxesandOther1;
	}
	public void setTaxesandOther1(String taxesandOther1) {
		this.taxesandOther1 = taxesandOther1;
	}
	public String getTotalforflight1() {
		return totalforflight1;
	}
	public void setTotalforflight1(String totalforflight1) {
		this.totalforflight1 = totalforflight1;
	}
	public String getSubtotalFinal() {
		return SubtotalFinal;
	}
	public void setSubtotalFinal(String subtotalFinal) {
		SubtotalFinal = subtotalFinal;
	}
	public String getTaxesandOtherFinal() {
		return taxesandOtherFinal;
	}
	public void setTaxesandOtherFinal(String taxesandOtherFinal) {
		this.taxesandOtherFinal = taxesandOtherFinal;
	}
	public String getTotalforflightFinal() {
		return totalforflightFinal;
	}
	public void setTotalforflightFinal(String totalforflightFinal) {
		this.totalforflightFinal = totalforflightFinal;
	}

	public String getAmountbyairline() {
		return amountbyairlineFinal;
	}
	public void setAmountbyairlineFinal(String amountbyairlineFinal) {
		this.amountbyairlineFinal = amountbyairlineFinal;
	}
	public String getFinalvalue() {
		return finalvalue;
	}
	public void setFinalvalue(String finalvalue) {
		this.finalvalue = finalvalue;
	}

	public String getCurrencyCode() {
		return CurrencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		CurrencyCode = currencyCode;
	}
	public String getLeaveAirport() {
		return LeaveAirport;
	}
	public void setLeaveAirport(String leaveAirport) {
		LeaveAirport = leaveAirport;
	}
	public String getReturnAirport() {
		return ReturnAirport;
	}
	public void setReturnAirport(String returnAirport) {
		ReturnAirport = returnAirport;
	}
	/*public String getLeaveDate() {
		return LeaveDate;
	}
	public void setLeaveDate(String leaveDate) {
		LeaveDate = leaveDate;
	}
	public String getReturnDate() {
		return ReturnDate;
	}
	public void setReturnDate(String returnDate) {
		ReturnDate = returnDate;
	}*/
	public String getLeaveTime() {
		return LeaveTime;
	}
	public void setLeaveTime(String leaveTime) {
		LeaveTime = leaveTime;
	}
	public String getReturnTime() {
		return ReturnTime;
	}
	public void setReturnTime(String returnTime) {
		ReturnTime = returnTime;
	}
	public String getLeavingFlightNO() {
		return LeavingFlightNO;
	}
	public void setLeavingFlightNO(String leavingFlightNO) {
		LeavingFlightNO = leavingFlightNO;
	}
	public String getReturningFlightNO() {
		return ReturningFlightNO;
	}
	public void setReturningFlightNO(String returningFlightNO) {
		ReturningFlightNO = returningFlightNO;
	}
	public String getTotal() {
		return Total;
	}
	public void setTotal(String total) {
		Total = total;
	}

}
