package system.classes;

import java.util.ArrayList;

public class CartFlightCC
{

	Outbound outbound				= null;
	Inbound inbound					= null;
	
	String CurrencyCode				= "";
	
	ArrayList
	<RatesperPassenger> ratelist	= null;
	
	String totalBeforeTax			= "";
	String taxes					= "";
	String bookingfee				= "";
	String TotalInclusiveTax		= "";
	
	String totalGrossBookingValue	= "";
	String taxesandOther			= "";
	String finalvalue				= "";
	String amountprocessnowFinal	= "";
	String amountbyairlineFinal		= "";
	
	String deadline					= "";
	String packageDeadline			= "";
	
	
	public String getPackageDeadline() {
		return packageDeadline;
	}

	public void setPackageDeadline(String packageDeadline) {
		this.packageDeadline = packageDeadline;
	}

	public Outbound getOutbound() {
		return outbound;
	}

	public void setOutbound(Outbound outbound) {
		this.outbound = outbound;
	}

	public Inbound getInbound() {
		return inbound;
	}

	public void setInbound(Inbound inbound) {
		this.inbound = inbound;
	}

	public String getCurrencyCode() {
		return CurrencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		CurrencyCode = currencyCode;
	}

	public ArrayList<RatesperPassenger> getRatelist() {
		return ratelist;
	}

	public void setRatelist(ArrayList<RatesperPassenger> ratelist) {
		this.ratelist = ratelist;
	}

	public String getTotalBeforeTax() {
		return totalBeforeTax;
	}

	public void setTotalBeforeTax(String totalBeforeTax) {
		this.totalBeforeTax = totalBeforeTax;
	}

	public String getTaxes() {
		return taxes;
	}

	public void setTaxes(String taxes) {
		this.taxes = taxes;
	}

	public String getBookingfee() {
		return bookingfee;
	}

	public void setBookingfee(String bookingfee) {
		this.bookingfee = bookingfee;
	}

	public String getTotalInclusiveTax() {
		return TotalInclusiveTax;
	}

	public void setTotalInclusiveTax(String totalInclusiveTax) {
		TotalInclusiveTax = totalInclusiveTax;
	}

	public String getTotalGrossBookingValue() {
		return totalGrossBookingValue;
	}

	public void setTotalGrossBookingValue(String totalGrossBookingValue) {
		this.totalGrossBookingValue = totalGrossBookingValue;
	}

	public String getTaxesandOther() {
		return taxesandOther;
	}

	public void setTaxesandOther(String taxesandOther) {
		this.taxesandOther = taxesandOther;
	}

	public String getFinalvalue() {
		return finalvalue;
	}

	public void setFinalvalue(String finalvalue) {
		this.finalvalue = finalvalue;
	}

	public String getAmountprocessnowFinal() {
		return amountprocessnowFinal;
	}

	public void setAmountprocessnowFinal(String amountprocessnowFinal) {
		this.amountprocessnowFinal = amountprocessnowFinal;
	}

	public String getAmountbyairlineFinal() {
		return amountbyairlineFinal;
	}

	public void setAmountbyairlineFinal(String amountbyairlineFinal) {
		this.amountbyairlineFinal = amountbyairlineFinal;
	}

	public String getDeadline() {
		return deadline;
	}

	public void setDeadline(String deadline) {
		this.deadline = deadline;
	}

	

}
