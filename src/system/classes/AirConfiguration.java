package system.classes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import com.utilities.Screenshot;
import com.utilities.ScreenshotPathSingleton;
import system.supportParents.ExcelReader;
import system.classes.AirConfig;
import system.enumtypes.ConfigFareType;


public class AirConfiguration 
{
	static HashMap<String, String> propertymap	= new HashMap<String, String>();
	ArrayList<Map<Integer,String>> returnlist	= null;
	String airline								= "";
	ExcelReader xl								= new ExcelReader();
	boolean found								= false;
	
	
	public AirConfiguration(HashMap<String, String> propmap)
	{
		propertymap = propmap;
		returnlist = xl.init(propertymap.get("AirConfigXL_Path"));
	}
	
	public WebDriver setConfiguration(WebDriver driver, AirConfig configObj ) throws WebDriverException, IOException
	{
		String scenarioCommonPath = "";
		String scenarioFailedPath = "";
		scenarioCommonPath = ScreenshotPathSingleton.getInstance().getScenarioCommonPath();
		scenarioFailedPath = ScreenshotPathSingleton.getInstance().getScenarioFailedPath();
		
		driver.get(propertymap.get("Portal.Url").concat("/air/setup/AirConfigurationSetupPage.do?module=contract"));
		Screenshot.takeScreenshot(scenarioCommonPath + "/Air Configure page.jpg", driver);
		
		try {
			Alert alert = driver.switchTo().alert();
			Screenshot.takeScreenshot(scenarioCommonPath + "/Alert.jpg", driver);
			alert.accept();
		} catch (Exception e) {
			
		}
		
		try
		{
			driver.findElement(By.id("airLineName")).sendKeys(configObj.getAirLine().trim());
			driver.findElement(By.id("airLineName_lkup")).click();
			driver.switchTo().frame("lookup");
			airline = driver.findElement(By.className("rezgLook0")).getText().trim();
			if(configObj.getAirLine().trim().equalsIgnoreCase(airline))
			{
				found = true;
				Screenshot.takeScreenshot(scenarioCommonPath + "/Select AirLine.jpg", driver);
				driver.findElement(By.id("lookupDataArea")).findElement(By.className("rezgLook0")).click();
			}
		
			driver.switchTo().defaultContent();
		
		} catch (Exception e) {
			Screenshot.takeScreenshot(scenarioFailedPath + "/Before Air Configure.jpg", driver);
		}
		
		if(found)
		{
			boolean set = false;
			
			while(!set)
			{
				if( !configObj.getPubFareType().equals("") )
				{
					ArrayList<WebElement> publishedRadio = new ArrayList<WebElement>(driver.findElements(By.id("publishedFareOption")));
					if( configObj.getPubFareType().equals(ConfigFareType.Booking_Fee) )
					{
						if( !publishedRadio.get(0).isSelected() )
						{	
							publishedRadio.get(0).click();
							Screenshot.takeScreenshot(scenarioCommonPath + "/Publish fare booking Fee.jpg", driver);
						}
					}
					
					else if( configObj.getPubFareType().equals(ConfigFareType.Profit_Markup) )
					{
						if( !publishedRadio.get(1).isSelected() )
						{
							publishedRadio.get(1).click();
							Screenshot.takeScreenshot(scenarioCommonPath + "/Publish fare profit markup.jpg", driver);
						}
					}
					
					else if( configObj.getPubFareType().equals(ConfigFareType.Both) )
					{
						if( !publishedRadio.get(2).isSelected() )
						{
							publishedRadio.get(2).click();
							Screenshot.takeScreenshot(scenarioCommonPath + "/Publish fare both.jpg", driver);
						}
					}
					
					else if( configObj.getPubFareType().equals(ConfigFareType.None) )
					{
						if( !publishedRadio.get(3).isSelected() )
						{
							publishedRadio.get(3).click();
							Screenshot.takeScreenshot(scenarioCommonPath + "/Publish fare none.jpg", driver);
						}
					}
					
					driver.findElement(By.id("addAirConfigurationMapping")).click();
					
					if(driver.findElement(By.id("dialogMsgText")).isDisplayed())
					{
						Screenshot.takeScreenshot(scenarioCommonPath + "/Configuration already exist.jpg", driver);
						((JavascriptExecutor)driver).executeScript("javascript:closeDialogMsg(dialogMsgBox);");
						set = false;
						ArrayList<WebElement> table = new ArrayList<WebElement>(driver.findElements(By.xpath(".//*[@id='air_configuration_mapping_data']/table/tbody")));
						ArrayList<WebElement> tabletr = new ArrayList<WebElement>(table.get(0).findElements(By.className("reportDataRows1")));
						
						for(int y=0; y<tabletr.size(); y++)
						{
							String text = tabletr.get(y).findElements(By.className("tablegridcell")).get(0).getText();
							
							if(configObj.getAirLine().trim().equalsIgnoreCase(text))
							{
								((JavascriptExecutor)driver).executeScript("removeAirConfigurationMapping("+(y+1)+")");
								Screenshot.takeScreenshot(scenarioCommonPath + "/Removed configuration already exist.jpg", driver);
								break;
							}
						}
					}
					else
					{
						Screenshot.takeScreenshot(scenarioCommonPath + "/Configuration mapping done.jpg", driver);
						set = true;
					}
				}
			
			}
			
			
			if( !configObj.getPvtFareType().equals("") )
			{
				ArrayList<WebElement> privateRadio = new ArrayList<WebElement>(driver.findElements(By.id("privateFareOption")) );
				
				if(configObj.getPvtFareType().equals(ConfigFareType.Booking_Fee))
				{
					if( !privateRadio.get(0).isSelected())
					{
						privateRadio.get(0).click();
						Screenshot.takeScreenshot(scenarioCommonPath + "/Private fare booking Fee.jpg", driver);
					}
				}
				
				else if( configObj.getPvtFareType().equals(ConfigFareType.Profit_Markup) )
				{
					System.out.println();
					if( !privateRadio.get(1).isSelected() )
					{
						privateRadio.get(1).click();
						Screenshot.takeScreenshot(scenarioCommonPath + "/Private fare profit markup.jpg", driver);
					}
				}
				
				else if( configObj.getPvtFareType().equals(ConfigFareType.Both) )
				{
					if( !privateRadio.get(2).isSelected())
					{
						privateRadio.get(2).click();
						Screenshot.takeScreenshot(scenarioCommonPath + "/Private fare both.jpg", driver);
					}
				}
			}
			
			if(!configObj.getFTypeConfgShopCart().equals(""))
			{
				if(configObj.getFTypeConfgShopCart().equals("Published_Fares_Only"))
				{
					driver.findElement(By.id("fareTypeShoppingCart_PRP")).click();
					Screenshot.takeScreenshot(scenarioCommonPath + "/Publish fare shopping cart.jpg", driver);
				}
			}
			
			if( !configObj.getFlightPayOptCartBooking().equals("") )
			{
				if(configObj.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Full_Amount_at_Booking"))
				{
					driver.findElement(By.id("payementOptionShoppingCart_PF")).click();
					Screenshot.takeScreenshot(scenarioCommonPath + "/Pay Full at booking.jpg", driver);
				}
				else if(configObj.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Booking_Fee_as_Deposit"))
				{
					driver.findElement(By.id("payementOptionShoppingCart_PB")).click();
					Screenshot.takeScreenshot(scenarioCommonPath + "/Pay booking fee at booking.jpg", driver);
				}
				else if(configObj.getFlightPayOptCartBooking().equalsIgnoreCase("Pass_to_Air_Line_to_Charge"))
				{
					driver.findElement(By.id("payementOptionShoppingCart_AC")).click();
					Screenshot.takeScreenshot(scenarioCommonPath + "/Pass to airline.jpg", driver);
				}
			}
		}//End if(found)
		
		driver.findElement(By.id("saveButId")).click();
		
		if(driver.findElement(By.id("dialogMsgBox")).isDisplayed())
		{
			Screenshot.takeScreenshot(scenarioCommonPath + "/Save Configuration(Before message).jpg", driver);
			driver.findElement(By.id("dialogMsgActionButtons")).click();
			Screenshot.takeScreenshot(scenarioCommonPath + "/Save Configuration(After message).jpg", driver);
		}
		
		return driver;
	}
	
	public ArrayList<AirConfig> getConfigurationList()
	{
		ArrayList<AirConfig> configObjList = new ArrayList<AirConfig>();
		
		for(int i=0; i<returnlist.size(); i++)
		{
			Map<Integer, String> sheet = returnlist.get(i);
			
			for(int j=0; j<sheet.size(); j++)
			{
				String[] data = sheet.get(j).split(",");
				
				AirConfig obj = new AirConfig();
				
				obj.setTestNo(Integer.parseInt(data[0]));
				obj.setAirLine(data[1]);
				
				if(data[2].equals(ConfigFareType.Booking_Fee.toString()))
				{
					obj.setPubFareType(ConfigFareType.Booking_Fee);
				}
				else if(data[2].equals(ConfigFareType.Profit_Markup.toString()))
				{
					obj.setPubFareType(ConfigFareType.Profit_Markup);
				}
				else if(data[2].equals(ConfigFareType.Both.toString()))
				{
					obj.setPubFareType(ConfigFareType.Both);
				}
				else if(data[2].equals(ConfigFareType.None.toString()))
				{
					obj.setPubFareType(ConfigFareType.None);
				}
				
				if(data[3].equals(ConfigFareType.Booking_Fee.toString()))
				{
					obj.setPvtFareType(ConfigFareType.Booking_Fee);
				}
				else if(data[3].equals(ConfigFareType.Profit_Markup.toString()))
				{
					obj.setPvtFareType(ConfigFareType.Profit_Markup);
				}
				else if(data[3].equals(ConfigFareType.Both.toString()))
				{
					obj.setPvtFareType(ConfigFareType.Both);
				}

				obj.setFTypeConfgShopCart(data[4]);
				obj.setFTypeConfgFplusH(data[5]);
				obj.setFTypeConfgFixPack(data[6]);
				obj.setFlightPayOptCartBooking(data[7]);
				obj.setFlightPayOptPayfullFplusH(data[8]);
			
				configObjList.add(obj);
			}
		}
		
		return configObjList;
	}

}
