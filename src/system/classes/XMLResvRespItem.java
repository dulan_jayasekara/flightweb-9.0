/*Sanoj*/
package system.classes;


public class XMLResvRespItem {
	String Status = "";
	String ItinerarySeq = "";
	Flight flight = null;
	String RPH = "";
	String DepartureDay = "";
	String E_TicketEligibility = "";
	String TPAExt_ConfirmationNumber = "";

	
	public String getTPAExt_ConfirmationNumber() {
		return TPAExt_ConfirmationNumber;
	}
	public void setTPAExt_ConfirmationNumber(String tPAExt_ConfirmationNumber) {
		TPAExt_ConfirmationNumber = tPAExt_ConfirmationNumber;
	}
	public String getE_TicketEligibility() {
		return E_TicketEligibility;
	}
	public void setE_TicketEligibility(String e_TicketEligibility) {
		E_TicketEligibility = e_TicketEligibility;
	}
	public String getDepartureDay() {
		return DepartureDay;
	}
	public void setDepartureDay(String departureDay) {
		DepartureDay = departureDay;
	}
	public String getRPH() {
		return RPH;
	}
	public void setRPH(String rPH) {
		RPH = rPH;
	}
	public Flight getFlight() {
		return flight;
	}
	public void setFlight(Flight flight) {
		this.flight = flight;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getItinerarySeq() {
		return ItinerarySeq;
	}
	public void setItinerarySeq(String itinerarySeq) {
		ItinerarySeq = itinerarySeq;
	}


}
