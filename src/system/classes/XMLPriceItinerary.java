/*Sanoj*/
package system.classes;
import java.util.ArrayList;

import com.common.Validators.CommonValidator;


public class XMLPriceItinerary implements Cloneable
{
	private		String						JournyDuration			= "";
	private		ArrayList<XMLOriginOptions>	originoptions			= null;
	private		XMLPriceInfo				pricinginfo				= null;
	private		String						directiontype			= "";
	private		int							NoOforigins				= 0;
	private 	String						ticketTimeLimit			= "";
	private		String						cancellationDate		= "";
	
	
	
	
	public String getCancellationDate() {
		return cancellationDate;
	}
	public void setCancellationDate(String cancellationDate) {
		this.cancellationDate = cancellationDate;
	}
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	public int getNoOforigins() {
		return NoOforigins;
	}
	public void setNoOforigins(int noOforigins) {
		NoOforigins = noOforigins;
	}
	public String getTicketTimeLimit() {
		return ticketTimeLimit;
	}
	public void setTicketTimeLimit(String ticketTimeLimit) {
		this.ticketTimeLimit	= CommonValidator.formatDateToCommon(ticketTimeLimit, "yyyy-MM-dd");
		this.cancellationDate	= CommonValidator.getCancellationDate(ticketTimeLimit, "yyyy-MM-dd");
	}

	public String getDirectiontype() {
		return directiontype;
	}
	public void setDirectiontype(String directiontype) {
		this.directiontype = directiontype;
	}
	public ArrayList<XMLOriginOptions> getOriginoptions() {
		return originoptions;
	}
	public void setOriginoptions(ArrayList<XMLOriginOptions> originoptions) {
		this.originoptions = originoptions;
	}
	public XMLPriceInfo getPricinginfo() {
		return pricinginfo;
	}
	public void setPricinginfo(XMLPriceInfo pricinginfo) {
		this.pricinginfo = pricinginfo;
	}
	public String getJournyDuration() {
		return JournyDuration;
	}
	public void setJournyDuration(String journyDuration) {
		JournyDuration = journyDuration;
	}
	
	
	public ArrayList<XMLOriginOptions> getOrigins() {
		return originoptions;
	}
	public void setOrigins(ArrayList<XMLOriginOptions> origins) {
		this.originoptions = origins;
	}
	
	public void getAll()
	{
		System.out.println();
		//System.out.println("Whole journey duration : "+getJournyDuration());
		System.out.println("Direction Type : "+getDirectiontype());
		System.out.println("No of origin options : "+getNoOforigins());
		System.out.println("Ticket time limit : "+getTicketTimeLimit());
		pricinginfo.getAll();
		for(int u = 0; u<originoptions.size(); u++)
		{
			System.out.println("Origin Option : "+(u+1));
			originoptions.get(u).getAll();
		}
		System.out.println();
	}
}
