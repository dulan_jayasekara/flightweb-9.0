package system.classes;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import com.utilities.Screenshot;
import com.utilities.ScreenshotPathSingleton;

public class TOperator {

	private String currency				= "";
	private double creditLimit			= 0;
	private double creditBlanace		= 0;
	private double creditUtilization	= 0;
	
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public double getCreditLimit() {
		return creditLimit;
	}
	public void setCreditLimit(double creditLimit) {
		this.creditLimit = creditLimit;
	}
	public double getCreditBlanace() {
		return creditBlanace;
	}
	public void setCreditBlanace(double creditBlanace) {
		this.creditBlanace = creditBlanace;
	}
	public double getCreditUtilization() {
		return creditUtilization;
	}
	public void setCreditUtilization(double creditUtilization) {
		this.creditUtilization = creditUtilization;
	}
	
	public boolean setTO(WebDriver driver) throws WebDriverException, IOException
	{
		String							scenarioCommonPath	= "";
		//String							scenarioFailedPath	= "";
		scenarioCommonPath									= ScreenshotPathSingleton.getInstance().getScenarioCommonPath();
		//scenarioFailedPath									= ScreenshotPathSingleton.getInstance().getScenarioFailedPath();
		
		TakesScreenshot screen = (TakesScreenshot)driver;
		FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FlowScreenshots/TOPage.jpg"));
		Screenshot.takeScreenshot(scenarioCommonPath + "/TOPage.jpg", driver);
		String curr = "";
		try {
			curr = driver.findElement(By.xpath("/html/body/table/tbody/tr[2]/td/div/table/tbody/tr/td[1]/table/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table[2]/tbody/tr[3]/td[3]")).getText().trim();
			this.setCurrency(curr);
		} catch (Exception e) {
			System.out.println("Fail currency reading");
		}
		
		String creditLim = "0";
		try {
			creditLim = driver.findElement(By.xpath(".//*[@id='partnermenuitems']/table[2]/tbody/tr[4]/td[3]")).getText().trim();
			//this.setCreditLimit(Double.parseDouble(creditLim));
			this.creditLimit = Double.parseDouble(creditLim);
		} catch (Exception e) {
			System.out.println("Fail credit limit reading");
		}
		
		String creditBal = "0";
		try {
			creditBal = driver.findElement(By.xpath(".//*[@id='partnermenuitems']/table[2]/tbody/tr[5]/td[3]")).getText().trim();
			//this.setCreditLimit(Double.parseDouble(creditBal));
			this.creditBlanace = Double.parseDouble(creditBal);
		} catch (Exception e) {
			System.out.println("Fail credit limit reading");
		}
		
		String creditUtil = "0";
		try {
			creditUtil = driver.findElement(By.xpath(".//*[@id='partnermenuitems']/table[2]/tbody/tr[6]/td[3]")).getText().trim();
			//this.setCreditLimit(Double.parseDouble(creditUtil));
			this.creditUtilization = Double.parseDouble(creditUtil);
		} catch (Exception e) {
			System.out.println("Fail credit utilization reading");
		}
		
		return true;
	}
}
