/*Sanoj*/
package system.classes;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;

/*SANOJ*/

public class UIFlightItineraryCC 
{	
	private String		currencyCode				= "";
	private String		cost						= "";
	private String 		departing					= "";
	private String 		leaving						= "";
	private String 		returning					= "";
	private String		leaveFlight					= "";
	private String		leaveduration				= "";
	private String 		returnduration				= "";
	
	private String 		leaveDepartTime				= "";
	private String		leaveArriveTime				= "";
	private String		leaveDepartFrom				= "";
	private String		leaveArriveTo				= "";
	
	private String		returnDepartTime			= "";
	private String		returnArrivalTime			= "";
	private String		returnDepartFrom			= "";
	private String		returnArrivalTo				= "";
	
	private String		returnFlight				= "";
	private String		directFlightLeave			= "";
	private	String		returnDirectFlight			= "";
	
	private Outbound	outbound					= null;
	private Inbound		inbound						= null;
	private ArrayList<WebElement> Note              = null; 
	
	private String		cabintype					= "";
	private String		retCabin					= "";
	
	public String getRetCabin() {
		return retCabin;
	}
	public void setRetCabin(String retCabin) {
		this.retCabin = retCabin;
	}
	public ArrayList<WebElement> getNote() {
		return Note;
	}
	public void setNote(ArrayList<WebElement> note) {
		Note = note;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getCabintype() {
		return cabintype;
	}
	public void setCabintype(String cabintype) {
		this.cabintype = cabintype;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getDeparting() {
		return departing;
	}
	public void setDeparting(String departing) {
		this.departing = departing;
	}
	public String getLeaving() {
		return leaving;
	}
	public void setLeaving(String leaving) {
		this.leaving = leaving;
	}
	public String getReturning() {
		return returning;
	}
	public void setReturning(String returning) {
		this.returning = returning;
	}
	public String getLeaveFlight() {
		return leaveFlight;
	}
	public void setLeaveFlight(String leaveFlight) {
		this.leaveFlight = leaveFlight;
	}
	public String getLeaveduration() {
		return leaveduration;
	}
	public void setLeaveduration(String leaveduration) {
		this.leaveduration = leaveduration;
	}
	public String getReturnduration() {
		return returnduration;
	}
	public void setReturnduration(String returnduration) {
		this.returnduration = returnduration;
	}
	public String getLeaveDepartTime() {
		return leaveDepartTime;
	}
	public void setLeaveDepartTime(String leaveDepartTime) {
		this.leaveDepartTime = leaveDepartTime;
	}
	public String getLeaveArriveTime() {
		return leaveArriveTime;
	}
	public void setLeaveArriveTime(String leaveArriveTime) {
		this.leaveArriveTime = leaveArriveTime;
	}
	public String getLeaveDepartFrom() {
		return leaveDepartFrom;
	}
	public void setLeaveDepartFrom(String leaveDepartFrom) {
		this.leaveDepartFrom = leaveDepartFrom;
	}
	public String getLeaveArriveTo() {
		return leaveArriveTo;
	}
	public void setLeaveArriveTo(String leaveArriveTo) {
		this.leaveArriveTo = leaveArriveTo;
	}
	public String getReturnDepartTime() {
		return returnDepartTime;
	}
	public void setReturnDepartTime(String returnDepartTime) {
		this.returnDepartTime = returnDepartTime;
	}
	public String getReturnArrivalTime() {
		return returnArrivalTime;
	}
	public void setReturnArrivalTime(String returnArrivalTime) {
		this.returnArrivalTime = returnArrivalTime;
	}
	public String getReturnDepartFrom() {
		return returnDepartFrom;
	}
	public void setReturnDepartFrom(String returnDepartFrom) {
		this.returnDepartFrom = returnDepartFrom;
	}
	public String getReturnArrivalTo() {
		return returnArrivalTo;
	}
	public void setReturnArrivalTo(String returnArrivalTo) {
		this.returnArrivalTo = returnArrivalTo;
	}
	public String getReturnFlight() {
		return returnFlight;
	}
	public void setReturnFlight(String returnFlight) {
		this.returnFlight = returnFlight;
	}
	public String getDirectFlightLeave() {
		return directFlightLeave;
	}
	public void setDirectFlightLeave(String directFlightLeave) {
		this.directFlightLeave = directFlightLeave;
	}
	public String getReturnDirectFlight() {
		return returnDirectFlight;
	}
	public void setReturnDirectFlight(String returnDirectFlight) {
		this.returnDirectFlight = returnDirectFlight;
	}
	public Outbound getOutbound() {
		return outbound;
	}
	public void setOutbound(Outbound outbound) {
		this.outbound = outbound;
	}
	public Inbound getInbound() {
		return inbound;
	}
	public void setInbound(Inbound inbound) {
		this.inbound = inbound;
	}
	
	
	/*private String 		LeaveDate	 				= null;
	private String		LeaveDeparture_port			= null;
	private String		LeaveDepartureLocationCode	= null;
	private String 		LeaveDepartureTime 			= null;
	private String		LeaveArrival_port			= null;
	private String		LeaveArrivalLocationCode	= null;
	private String 		LeaveArrivalTime 			= null;
	private String		LeaveOperatingAirline		= null;
	private String		LeaveMarketingAirline		= null;
	private String 		LeaveflightNo 				= null;
	private String 		LeaveMarketingAirlineCode 	= null;	

	
	private String		ReturnDate						= null;
	private String		ReturnDeparture_port			= null;
	private String		ReturnDepartureLocationCode		= null;
	private String 		ReturnDepartureTime 			= null;
	private String		ReturnArrival_port				= null;
	private String		ReturnArrivalLocationCode		= null;
	private String		ReturnArrivalTime				= null;
	private String		ReturnOperatingAirline			= null;
	private String		ReturnMarketingAirline			= null;
	private String 		ReturnflightNo 					= null;
	private String 		ReturnMarketingAirlineCode 		= null;	*/

	
	

}
