package system.classes;

import system.enumtypes.CancellationType;

//import Flight.TypeEnum.CancellationType;

public class CancellationBreakdown {

	CancellationType cancellationScenario	= null;
	String	currencyCode	= "";
	double	baseFare		= 0;
	double	tax				= 0;
	double	profit			= 0;
	double	creditCardFee	= 0;
	double	bookingFee		= 0;
	double	cancellationFee	= 0;
	double	supplierGet		= 0;
	double	portalGet		= 0;
	double	customerGet		= 0;
	
	
	
	public double getSupplierGet() {
		return supplierGet;
	}
	public void setSupplierGet(double supplierGet) {
		this.supplierGet = supplierGet;
	}
	public double getPortalGet() {
		return portalGet;
	}
	public void setPortalGet(double portalGet) {
		this.portalGet = portalGet;
	}
	public double getCustomerGet() {
		return customerGet;
	}
	public void setCustomerGet(double customerGet) {
		this.customerGet = customerGet;
	}
	public CancellationType getCancellationScenario() {
		return cancellationScenario;
	}
	public void setCancellationScenario(CancellationType cancellationScenario) {
		this.cancellationScenario = cancellationScenario;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public double getBaseFare() {
		return baseFare;
	}
	public void setBaseFare(double baseFare) {
		this.baseFare = baseFare;
	}
	public double getTax() {
		return tax;
	}
	public void setTax(double tax) {
		this.tax = tax;
	}
	public double getProfit() {
		return profit;
	}
	public void setProfit(double profit) {
		this.profit = profit;
	}
	public double getCreditCardFee() {
		return creditCardFee;
	}
	public void setCreditCardFee(double creditCardFee) {
		this.creditCardFee = creditCardFee;
	}
	public double getBookingFee() {
		return bookingFee;
	}
	public void setBookingFee(double bookingFee) {
		this.bookingFee = bookingFee;
	}
	public double getCancellationFee() {
		return cancellationFee;
	}
	public void setCancellationFee(double cancellationFee) {
		this.cancellationFee = cancellationFee;
	}
	
}
