/*Sanoj*/
package system.supportParents;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


public class ReadProperties {
	
	static FileInputStream fs = null;
	static Properties prop = new Properties();
	
	
	
	public static String  readpropety(String property,String File) throws IOException {
	
		try 
		{
			fs= new FileInputStream(new File(File));
			prop.load(fs);
			
			return  prop.getProperty(property);
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return prop.getProperty(property);
	
	}
	
	
	public static String  readpropety(String property) throws IOException {
		
		try 
		{
			fs = new FileInputStream(new File("Config.properties"));
			prop.load(fs);
			
			return  prop.getProperty(property);
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return prop.getProperty(property);
			
	}
	
		
	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	public static HashMap<String, String> readpropeties(String path) throws IOException {
		
		HashMap<String, String>    mymap = null ;
		
		try {
			fs= new FileInputStream(new File(path));
			prop.load(fs);
			mymap= new HashMap<String, String>((Map)prop);
			return mymap;
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return mymap;

	}


}
