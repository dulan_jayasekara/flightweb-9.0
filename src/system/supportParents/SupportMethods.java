/*Sanoj*/
package system.supportParents;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
//import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
/*import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;*/
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.utilities.Screenshot;
import com.utilities.ScreenshotPathSingleton;

import system.classes.PopupMessage;

//Added Sample Comment

public class SupportMethods {
	
	private WebDriver driver = null;
	private HashMap<String, String>  PropertySet;
	String xmlfilepath = "";
	

	public SupportMethods()
	{
		
	}

	public SupportMethods(HashMap<String, String> map)
	{
	   PropertySet = map;	
	}
	
	public WebDriver initalizeDriver() throws IOException 
	{
		
		String browser = PropertySet.get("Browser");
		
		if(browser.equalsIgnoreCase("chrome"))
		{
			System.setProperty("webdriver.chrome.driver", "E:\\chromedriver.exe");
			WebDriver driver = new ChromeDriver(); 
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			return driver;
		}
		
		String Type = PropertySet.get("BinaryType").trim();	
		if(Type.equalsIgnoreCase("FirefoxBin") && browser.equalsIgnoreCase("Firefox"))
		{
			FirefoxProfile prof = new FirefoxProfile();
			FirefoxBinary bin = new FirefoxBinary();
			driver = new FirefoxDriver(bin,prof);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			try {
				WebDriverWait wait = new WebDriverWait(driver, 4);
		    	wait.until(ExpectedConditions.alertIsPresent());
		    	Alert alert = driver.switchTo().alert();
				System.out.println(alert.getText());
				alert.accept();
			} catch (Exception e) {
				System.out.println("No Alert Present");
			}
			return driver;
		}
		/*if(Type.equalsIgnoreCase("Phantom"))
		{
			String ExecutablePath = PropertySet.get("phantomjs.executable.path");
			DesiredCapabilities cap = new DesiredCapabilities();
			cap.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, ExecutablePath);
			cap.setCapability("TakeScreenshot", true);
		    driver = new PhantomJSDriver(cap);
		    driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		    return driver;
			
		}*/
		if(Type.equalsIgnoreCase("Firefoxpro") && browser.equalsIgnoreCase("Firefox"))
		{
			File profile = new File(PropertySet.get("firefox.profile.path").trim());
			FirefoxProfile prof = new FirefoxProfile(profile);
			prof.setPreference("network.proxy.type", 0);
			driver = new FirefoxDriver(prof);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			try {
				WebDriverWait wait = new WebDriverWait(driver, 4);
		    	wait.until(ExpectedConditions.alertIsPresent());
		    	Alert alert = driver.switchTo().alert();
				System.out.println(alert.getText());
				alert.accept();
			} catch (Exception e) {
				System.out.println("No Alert Present");
			}
		    return driver;
		}
		else
		{
			FirefoxProfile prof = new FirefoxProfile();
			prof.setPreference("network.proxy.type", 0);
			driver = new FirefoxDriver(prof);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			try {
				WebDriverWait wait = new WebDriverWait(driver, 4);
		    	wait.until(ExpectedConditions.alertIsPresent());
		    	Alert alert = driver.switchTo().alert();
				System.out.println(alert.getText());
				alert.accept();
			} catch (Exception e) {
				System.out.println("No Alert Present");
			}
			return driver;
		}
		
	}
	
	public boolean login(WebDriver driver, String userName, String passWord) throws IOException
	{
		String scenarioCommonPath = "";
		String scenarioFailedPath = "";
		scenarioCommonPath = ScreenshotPathSingleton.getInstance().getScenarioCommonPath();
		scenarioFailedPath = ScreenshotPathSingleton.getInstance().getScenarioFailedPath();
		
		try {
			driver.get(PropertySet.get("Portal.Url").concat("/admin/common/LoginPage.do"));
			Thread.sleep(5000);
			Screenshot.takeScreenshot(scenarioCommonPath + "/Login page.jpg", driver);
		} catch (InterruptedException e1) {
			Screenshot.takeScreenshot(scenarioFailedPath + "/Login page.jpg", driver);
			System.out.println("Login url fails");
		}
		
		try
		{
			driver.findElement(By.id("user_id")).clear();
			driver.findElement(By.id("user_id")).sendKeys(userName);
			driver.findElement(By.id("password")).clear();
			driver.findElement(By.id("password")).sendKeys(passWord);
			Screenshot.takeScreenshot(scenarioCommonPath + "/Before Login.jpg", driver);
			driver.findElement(By.id("loginbutton")).click();
			driver.findElement(By.id("mainmenurezglogo"));
			return true;
		}
		
		catch (Exception e) {
			Screenshot.takeScreenshot(scenarioFailedPath + "/Login Error.jpg", driver);
			System.out.println("user id , password - error");
			return false;
		}
	}
	
	public void getReservationPage(WebDriver driver) throws IOException
	{
			driver.get(PropertySet.get("Portal.Url").concat("/operations/common/Main.do?module=operations"));
		    WebDriverWait wait = new WebDriverWait(driver, 3);	
			
		    try {
		    	wait.until(ExpectedConditions.alertIsPresent());
		    	Alert alert = driver.switchTo().alert();
				System.out.println(alert.getText());
				alert.accept();
			} catch (Exception e) {
				System.out.println("No Alert Present");
			}
		    
			driver.get(PropertySet.get("Portal.Url").concat("/operations/reservation/CallCenterWarSearchCriteria.do?module=operations"));
			
			try {
				WebDriverWait wait1 = new WebDriverWait(driver, 180, 5);
				wait1.until(ExpectedConditions.presenceOfElementLocated(By.id("live_message_frame")));
			} catch (Exception e) {
				//System.out.println("Problem with Navigating to the reservation page..");
			}

	}

	public boolean navigateToPaymentsPage(WebDriver driver)
	{
		driver.switchTo().defaultContent();
		((JavascriptExecutor)driver).executeScript("submitSelectedData()");
		
		try {
			driver.findElement(By.className("legendsubbg"));
			return true;
		} catch (Exception e) {
			return false;
		}
		
	}
	
	public boolean isElementPresent(By by,WebDriver driver)
	{  
		String scenarioFailedPath = "";
		scenarioFailedPath = ScreenshotPathSingleton.getInstance().getScenarioFailedPath();
		
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		try
		{  
			driver.findElement(by);
			return true;
		}
		catch(Exception ex)
		{
			System.out.println("isElementPresent");
			Screenshot.takeScreenshot(scenarioFailedPath + "Login Error.jpg", driver);
			return false;
		}
		finally{
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}
	}
	
	public PopupMessage addToCartCC(WebDriver driver, int n, int selector)
	{
		PopupMessage popup = new PopupMessage();
		//boolean done = false;
		int navcount1 = n/10;//Per page 10 find how many pages to navigate
		//System.out.println("pages :"+navcount1);
		int navcount2 = n%10;//Remaining flights
		//System.out.println("remainder :"+navcount2);
		int navigate = navcount1;
		
		if((navcount2>0)&&(navcount2<10))
		{
			navigate+=1;
		}
		
		int clickpattern = 2; 
		//System.out.println("NAVIGATE  :  "+navigate);
		int count = 0;
		ArrayList<WebElement> button = null;
		while(count<navigate)
		{
			try
			{	
				button = new ArrayList<WebElement>(driver.findElements(By.className("select_button")));
				if(button.size()>selector)
				{
					button.get(selector-1).findElement(By.id("add_book_"+(selector-1)+"")).click();
					Thread.sleep(5000);
					popup = popupHandler(driver);
					//done =  true;
					break;
				}	
			}	
			catch(Exception e)
			{
				System.out.println("addToCartCC");
				//e.printStackTrace();
				//done = false;
			}

			((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
			clickpattern++;		
			count++; 

		}
		
		clickpattern = 1;
		((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
		return popup;
	
	}

	public PopupMessage addToCart(WebDriver driver, int n, int selector)
	{
		WebDriverWait wait = new WebDriverWait(driver, 10);
		PopupMessage popup = new PopupMessage();
		//boolean done = false;
		int navcount1 = n/10;//Per page 10 find how many pages to navigate
		//System.out.println("pages :"+navcount1);
		int navcount2 = n%10;//Remaining flights
		//System.out.println("remainder :"+navcount2);
		int navigate = navcount1;
		if((navcount2>0)&&(navcount2<10))
		{
			navigate+=1;
		}
		int clickpattern = 2; 
		//System.out.println("NAVIGATE  :  "+navigate);
		int count = 0;
		ArrayList<WebElement> button = null;
		while(count<navigate)
		{
			try
			{	
				button = new ArrayList<WebElement>(driver.findElements(By.className("continue-shopping-btn")));
				try {
					if(button.size()>selector)
					{
						try {
							button.get(selector-1).click();
						} catch (Exception e) {
							//e.printStackTrace();
							System.out.println("addToCart - button.get(selector-1).click();");
						}
						
						Thread.sleep(3000);
						
						try {
							wait.until(ExpectedConditions.presenceOfElementLocated(By.id("upselling_dig_WJ_4")));
							try {
								Thread.sleep(3000);
								wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("upselling_dig_WJ_4"))));
							} catch (Exception e) {
								//e.printStackTrace();
								System.out.println("addToCart - driver.findElement(By.id(upselling_dig_WJ_4))");
							}
							
							if(driver.findElement(By.id("upselling_dig_WJ_4")).isDisplayed())
							{
								try {
									ArrayList<WebElement> ele = new ArrayList<WebElement> (driver.findElements(By.className("close-popup")));
									ele.get(0).findElement(By.className("fa-times")).click();
								} catch (Exception e) {
									try {
										ArrayList<WebElement> ele = new ArrayList<WebElement> (driver.findElements(By.className("close-popup")));
										ele.get(0).findElement(By.className("fa-times")).click();
									} catch (Exception e2) {
										
									}
								}
								
							}
							popup = popupHandler(driver);
						} catch (Exception e) {
							System.out.println("addToCart");
						}
						
						break;
					}
				} catch (Exception e) {
					//e.printStackTrace();
					System.out.println("addToCart - if(button.size()>selector)");
				}
				
			}	
			catch(Exception e)
			{
				//e.printStackTrace();
				System.out.println("button = new ArrayList<WebElement>(driver.findElements(By.className(continue-shopping-btn)))");
			}

			((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
			clickpattern++;		
			count++; 

		}
		
		clickpattern = 1;
		((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
		
		return popup;
		
	}

	public PopupMessage popupHandler(WebDriver driver)
	{
		boolean notCaught = true;
		PopupMessage popup = new PopupMessage();
		popup.setTitle("");
		popup.setMessage("");
		popup.setMessagetype("");
		String scenarioCommonPath	= "";
		//String scenarioFailedPath	= "";
		scenarioCommonPath			= ScreenshotPathSingleton.getInstance().getScenarioCommonPath();
		//scenarioFailedPath			= ScreenshotPathSingleton.getInstance().getScenarioFailedPath();

		try
		{
			driver.switchTo().defaultContent();
			driver.switchTo().frame("live_message_frame");
		}
		catch(Exception e)
		{
			System.out.println("ERROR EXCEPTION : Class - "+this.getClass().getName() + " Line - "+e.getStackTrace()[0].getLineNumber());
			//e.printStackTrace();
		}
		
		if(notCaught)
		{
			try
			{
				if(driver.findElement(By.id("dialog-error-message-WJ_22")).isDisplayed() )
				{
					popup.setMessagetype("Error");
					popup.setTitle(driver.findElement(By.id("ui-dialog-title-dialog-error-message-WJ_22")).getText());
					popup.setMessage(driver.findElement(By.id("dialog-error-message-WJ_22")).getText());
					
					Screenshot.takeScreenshot(scenarioCommonPath + "/dialog-error-message-WJ_22", driver);
					
					driver.findElement(By.className("ui-button-text")).click();
					notCaught = false;
				}
			}	
			catch(Exception e)
			{
				
			}
		}
		
		if(notCaught)
		{
			try {
				if(driver.findElement(By.id("dialog-error-message-WJ_20")).isDisplayed())
				{
					popup.setMessagetype("Error");
					popup.setTitle(driver.findElement(By.id("ui-dialog-title-dialog-error-message-WJ_20")).getText());
					popup.setMessage(driver.findElement(By.id("dialog-error-message-WJ_20")).getText());
					
					Screenshot.takeScreenshot(scenarioCommonPath + "/dialog-error-message-WJ_22", driver);
					
					driver.findElement(By.className("ui-button-text")).click();
					notCaught = false;
				}
			} catch (Exception e) {
				
			}
		}
		
		if(notCaught)
		{
			try
			{
				if(driver.findElement(By.id("dialog-alert-message-WJ_19")).isDisplayed() )
				{
					popup.setMessagetype("Error");
					popup.setTitle(driver.findElement(By.id("ui-dialog-title-dialog-alert-message-WJ_19")).getText());
					popup.setMessage(driver.findElement(By.id("dialog-alert-message-WJ_19")).getText());
					
					Screenshot.takeScreenshot(scenarioCommonPath + "/dialog-alert-message-WJ_19", driver);
					
					driver.findElement(By.className("ui-button-text")).click();
					notCaught = false;
				}
			}	
			catch(Exception e)
			{
				
			}
		}
		
		if(notCaught)
		{
			try
			{
				if(driver.findElement(By.id("dialog-warning-message-WJ_22")).isDisplayed() )
				{
					popup.setMessagetype("Warning");
					popup.setTitle(driver.findElement(By.id("ui-dialog-title-dialog-warning-message-WJ_22")).getText());
					popup.setMessage(driver.findElement(By.id("dialog-warning-message-WJ_22")).getText());
					
					Screenshot.takeScreenshot(scenarioCommonPath + "/dialog-warning-message-WJ_22", driver);
					
					driver.findElements(By.className("ui-dialog-titlebar-close")).get(0).findElement(By.className("ui-icon")).click();
					notCaught = false;
				}
			}	
			catch(Exception e)
			{
				
			}
		}
		
		if(notCaught)
		{
			try
			{	
				if(driver.findElement(By.id("dialog-alert-message-WJ_18")).isDisplayed() )
				{
					popup.setMessagetype("Warning");
					popup.setTitle(driver.findElement(By.id("ui-dialog-title")).getText());
					popup.setMessage(driver.findElement(By.id("dialog-alert-message-WJ_18")).getText());
					
					Screenshot.takeScreenshot(scenarioCommonPath + "/dialog-alert-message-WJ_18", driver);
					
					driver.findElement(By.xpath("/html/body/div[6]/div[3]/div/button")).click();
					notCaught = false;
				}
			}	
			catch(Exception e)
			{
				
			}
		}
		
		if(notCaught)
		{
			try
			{
				if(driver.findElement(By.id("dialog-warning-message-WJ_23")).isDisplayed())
				{
					popup.setMessagetype("Warning");
					popup.setTitle(driver.findElement(By.id("ui-dialog-title")).getText());
					popup.setMessage(driver.findElement(By.id("dialog-warning-message-WJ_22")).getText());
					
					Screenshot.takeScreenshot(scenarioCommonPath + "/dialog-warning-message-WJ_23", driver);
					
					driver.findElement(By.linkText("Ok")).click();
					//driver.findElement(By.xpath("/html/body/div[13]/div[3]/div/button[1]")).click();
					notCaught = false;
				}
			}
			catch(Exception e)
			{
				
			}
		}
		
		if(notCaught)
		{
			try
			{
				if(driver.findElement(By.id("dialog-error-message-WJ_19")).isDisplayed() )
				{
					popup.setMessagetype("Error");
					popup.setTitle(driver.findElement(By.id("ui-dialog-title-dialog-error-message-WJ_19")).getText());
					popup.setMessage(driver.findElement(By.id("dialog-error-message-WJ_19")).getText());
					
					Screenshot.takeScreenshot(scenarioCommonPath + "/dialog-error-message-WJ_19", driver);
					
					driver.findElement(By.xpath("/html/body/div[4]/div[11]/div/button")).click();
					notCaught = false;
				}
			}	
			catch(Exception e)
			{
				
			}
		}
		
		//Error: Booking cannot be processed due to the following error in processing your credit card. Please try again:Test Case Not Found
		if(notCaught)
		{
			try
			{
				if(driver.findElement(By.id("ui-dialog-title-dialog-alert-message-WJ_19")).isDisplayed() )
				{
					popup.setMessagetype("Error");
					popup.setTitle(driver.findElement(By.id("ui-dialog-title-dialog-alert-message-WJ_19")).getText());
					popup.setMessage(driver.findElement(By.id("dialog-alert-message-WJ_19")).getText());
					
					Screenshot.takeScreenshot(scenarioCommonPath + "/ui-dialog-title-dialog-alert-message-WJ_19", driver);
					
					driver.findElement(By.xpath("/html/body/div[8]/div[1]/a/span")).click();
					notCaught = false;
				}
			}	
			catch(Exception e)
			{
				
			}
		}
		
		if(notCaught)
		{
			try
			{
				if(driver.findElement(By.id("dialog-error-message-WJ_21")).isDisplayed() )
				{
					popup.setMessagetype("Error");
					popup.setTitle(driver.findElement(By.id("ui-dialog-title-dialog-error-message-WJ_21")).getText());
					popup.setMessage(driver.findElement(By.id("dialog-error-message-WJ_21")).getText());
					
					Screenshot.takeScreenshot(scenarioCommonPath + "/dialog-error-message-WJ_21", driver);
					
					driver.findElement(By.xpath("/html/body/div[7]/div[11]/div/button")).click();
					notCaught = false;
				}
			}	
			catch(Exception e)
			{
				
			}
		}
		
		if(notCaught)
		{
			try
			{
				if(driver.findElement(By.id("dialog-error-message-WJ_23")).isDisplayed() )
				{
					popup.setMessagetype("Error");
					popup.setTitle(driver.findElement(By.id("ui-dialog-title-dialog-error-message-WJ_23")).getText());
					popup.setMessage(driver.findElement(By.id("dialog-error-message-WJ_23")).getText());
					
					Screenshot.takeScreenshot(scenarioCommonPath + "/dialog-error-message-WJ_23", driver);
					
					driver.findElement(By.xpath("/html/body/div[2]/div[11]/div/button")).click();
					notCaught = false;
				}
				else
				{
					
				}
			}	
			catch(Exception e)
			{
				
			}
		}
		
		if(notCaught)
		{
			try
			{
				if(driver.findElement(By.id("dialog-error-message-WJ_0")).isDisplayed() )
				{
					popup.setMessagetype("Error");
					popup.setTitle(driver.findElement(By.id("ui-dialog-title-dialog-error-message-WJ_0")).getText());
					popup.setMessage(driver.findElement(By.id("dialog-error-message-WJ_0")).getText());
					
					Screenshot.takeScreenshot(scenarioCommonPath + "/dialog-error-message-WJ_0", driver);
					
					driver.findElement(By.xpath("html/body/div[10]/div[11]/div/button")).click();
					notCaught = false;
				}
			}	
			catch(Exception e)
			{
				
			}
		}
		
		return popup;
	
	}
	
	public void writeToFile(String key, String Value, String pathkey)
	{
		xmlfilepath = PropertySet.get(pathkey);
		Properties prop = new Properties();
		OutputStream output = null;
		File file = new File(xmlfilepath);
		
		if(!file.exists())
		{
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		try {
			InputStream input = null;
			input = new FileInputStream(file.getAbsoluteFile());
			prop.load(input);
			//System.out.println(prop.get(key));
			//System.out.println();
			output = new FileOutputStream(file);
			prop.setProperty(key, Value);
			prop.store(output, "XML url's");
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(output!=null){
				try {
					output.close();
				} catch (IOException e2) {
					e2.printStackTrace();
				}
			}
		}
	
	}
	
	public StringBuffer writeUrlToReport(StringBuffer ReportPrinter)
	{
		Properties prop = new Properties();
		InputStream input = null;
		File file = new File(PropertySet.get("XMLURLs"));

		try 
        {	
    	   input = new FileInputStream(file);
           prop.load(input);
        } 
        catch (FileNotFoundException e) 
        {
           e.printStackTrace();
        } 
        catch (IOException e) 
        {
           e.printStackTrace();
        }
		
        Set<Object> keys =  prop.keySet();
        

		ReportPrinter.append("<span><center><p class='Hedding0'>XML Links</p></center></span>");
		ReportPrinter.append("<table style=width:100%>"
		+ "<tr><th>XML Type</th>"
		+ "<th>Link</th></tr>");
		
        for(Object k:keys)
        {
           String key = (String)k;
           System.out.println(key+": "+prop.getProperty(key));
           
           String xmltype = key.split("_")[1].split("_")[0];
           String xmllink = prop.getProperty(key);
           
           System.out.println(xmltype);
           System.out.println(xmllink);
           
           ReportPrinter.append("<tr><td>"+xmltype+"</td>"+"<td><a href="+xmllink+" target = '_blank' >View XML File</a></td></tr>");  
        }
       
        ReportPrinter.append("</table>");
        ReportPrinter.append("<br><br><br>");
        
        return ReportPrinter;
	 }

	public boolean removeFromCart(WebDriver driver)
	{
		boolean status = false;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			ArrayList<WebElement> elem = new ArrayList<WebElement> (driver.findElements(By.className("remove-basket-itm")));
			WebElement e = elem.get(0).findElement(By.className("fa-times"));
			e.click();
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("confirmMsgDiv"))));
			
			if(driver.findElement(By.id("confirmMsgDiv")).isDisplayed())
			{
				wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.className("ui-dialog-buttonset"))));
				ArrayList<WebElement> buttons = new ArrayList<WebElement> (driver.findElements(By.className("ui-dialog-buttonset")));
				ArrayList<WebElement> ele = new ArrayList<WebElement> (buttons.get(0).findElements(By.className("ui-button-text")));
				//System.out.println(buttons.get(0).findElements(By.className("ui-button-text")).get(1).getText());
				ele.get(0).click();
				status = true;
			}
		} catch (Exception e) {
			//e.printStackTrace();
			System.out.println("Remove from cart test failed bro.....");
		}
		
		return status;
	}
	
	public boolean removeFromCartCC(WebDriver driver)
	{
		boolean status = false;
		try {
			driver.findElement(By.id("remove_f")).click();
			status = true;
			/*ArrayList<WebElement> elem = new ArrayList<WebElement> (driver.findElements(By.className("remove_f")));
			WebElement e = elem.get(0).findElement(By.className("fa-times"));
			e.click();
			if(driver.findElement(By.id("confirmMsgDiv")).isDisplayed())
			{
				ArrayList<WebElement> buttons = new ArrayList<WebElement> (driver.findElements(By.className("ui-dialog-buttonset")));
				ArrayList<WebElement> ele = new ArrayList<WebElement> (buttons.get(0).findElements(By.className("ui-button-text")));
				//System.out.println(buttons.get(0).findElements(By.className("ui-button-text")).get(1).getText());
				ele.get(0).click();
				
			}*/
		} catch (Exception e) {
			//e.printStackTrace();
			System.out.println("Remove from cart CC test failed bro.....");
		}
		
		return status;
	}
	
}
