package system.pages;

import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.common.Validators.CommonValidator;

import system.classes.Address;
import system.classes.Flight;
import system.classes.Inbound;
import system.classes.Outbound;
import system.classes.RatesperPassenger;
import system.classes.SearchObject;
import system.classes.Traveler;
import system.classes.UICreditCardPayInfo;
import system.classes.UIPriceInfo;

public class WebConfirmationPage {

	private HashMap<String, String>		 propertyMap			= new HashMap<String, String>();
	
	private boolean						 done					= false;
	private String						 departure				= "";
	private String						 destination			= "";
	private String						 fromDate				= "";
	private String						 toDate					= "";
	
	private String						 bookingReference		= "";
	private String						 reservationNo			= "";
	private String						 supplierConfirmationNo	= "null";
	
	private Outbound					 outbound				= new Outbound();
	private Inbound						 inbound				= new Inbound();
	
	private ArrayList<RatesperPassenger> ratelist				= new ArrayList<RatesperPassenger>();
	
	private UIPriceInfo					 uisummarypay			= null;
	private UICreditCardPayInfo 		 creditcardpay			= null;
	
	private String						 cancellationdate		= "";
	private String						 PackageDeadline		= "";

	private Traveler					 maincustomer			= null;
	private ArrayList<Traveler>			 adult					= null;
	private ArrayList<Traveler> 		 children				= null;
	private ArrayList<Traveler> 		 infant					= null;
	private boolean						 twoway					= false;
	private UIPriceInfo					 priceinfo				= null;
	private boolean available	= false;
	
	
	
	public boolean isAvailable() {
		return available;
	}

	public boolean isTwoway() {
		return twoway;
	}

	public void setTwoway(boolean twoway) {
		this.twoway = twoway;
	}

	public UIPriceInfo getPriceinfo() {
		return priceinfo;
	}

	public void setPriceinfo(UIPriceInfo priceinfo) {
		this.priceinfo = priceinfo;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public HashMap<String, String> getPropertyMap() {
		return propertyMap;
	}

	public void setPropertyMap(HashMap<String, String> propertyMap) {
		this.propertyMap = propertyMap;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

	public String getDeparture() {
		return departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getBookingReference() {
		return bookingReference;
	}

	public void setBookingReference(String bookingReference) {
		this.bookingReference = bookingReference;
	}

	public String getReservationNo() {
		return reservationNo;
	}

	public void setReservationNo(String reservationNo) {
		this.reservationNo = reservationNo;
	}

	public String getSupplierConfirmationNo() {
		return supplierConfirmationNo;
	}

	public void setSupplierConfirmationNo(String supplierConfirmationNo) {
		this.supplierConfirmationNo = supplierConfirmationNo;
	}

	public Outbound getOutbound() {
		return outbound;
	}

	public void setOutbound(Outbound outbound) {
		this.outbound = outbound;
	}

	public Inbound getInbound() {
		return inbound;
	}

	public void setInbound(Inbound inbound) {
		this.inbound = inbound;
	}

	public ArrayList<RatesperPassenger> getRatelist() {
		return ratelist;
	}

	public void setRatelist(ArrayList<RatesperPassenger> ratelist) {
		this.ratelist = ratelist;
	}

	public UIPriceInfo getUisummarypay() {
		return uisummarypay;
	}

	public void setUisummarypay(UIPriceInfo uisummarypay) {
		this.uisummarypay = uisummarypay;
	}

	public UICreditCardPayInfo getCreditcardpay() {
		return creditcardpay;
	}

	public void setCreditcardpay(UICreditCardPayInfo creditcardpay) {
		this.creditcardpay = creditcardpay;
	}

	public String getCancellationdate() {
		return cancellationdate;
	}

	public void setCancellationdate(String cancellationdate) {
		this.cancellationdate = cancellationdate;
	}

	public String getPackageDeadline() {
		return PackageDeadline;
	}

	public void setPackageDeadline(String packageDeadline) {
		PackageDeadline = packageDeadline;
	}

	public Traveler getMaincustomer() {
		return maincustomer;
	}

	public void setMaincustomer(Traveler maincustomer) {
		this.maincustomer = maincustomer;
	}

	public ArrayList<Traveler> getAdult() {
		return adult;
	}

	public void setAdult(ArrayList<Traveler> adult) {
		this.adult = adult;
	}

	public ArrayList<Traveler> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<Traveler> children) {
		this.children = children;
	}

	public ArrayList<Traveler> getInfant() {
		return infant;
	}

	public void setInfant(ArrayList<Traveler> infant) {
		this.infant = infant;
	}

	
	
	
	public WebConfirmationPage(HashMap<String, String> Map)
	{
		propertyMap = Map;
	}
	
	
	public boolean setConfirmationPage(WebDriver driver, SearchObject searchObject)
	{
		
		if(searchObject.getTriptype().equals("Round Trip"))
		{
			twoway = true;
		}
		
		try {
			WebDriverWait wait = new WebDriverWait(driver, 120);
			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id(propertyMap.get("ConfPg_BookingReference_Lbl_id").trim())));
			} catch (Exception e1) {
				System.out.println("Waited - ConfPg_BookingReference_Lbl_id");
			}
			Thread.sleep(6000);
			this.bookingReference		= driver.findElement(By.id(propertyMap.get("ConfPg_BookingReference_Lbl_id").trim())).getText().trim();
			this.reservationNo			= driver.findElement(By.id(propertyMap.get("ConfPg_ReservationNo_Lbl_id").trim())).getText().trim();
			if(reservationNo.contains(":"))
			{
				reservationNo = reservationNo.replace(":", "").trim();
			}
			this.supplierConfirmationNo	= driver.findElement(By.id(propertyMap.get("ConfPg_SupplierConfNo_Lbl_id").trim())).getText().trim();
			if(supplierConfirmationNo.contains(":"))
			{
				supplierConfirmationNo = supplierConfirmationNo.replace(":", "").trim();
			}
			this.departure				= driver.findElement(By.id(propertyMap.get("ConfPg_Origin_Lbl_id").trim())).getText().trim();
			this.destination			= driver.findElement(By.id(propertyMap.get("ConfPg_Destination_Lbl_id").trim())).getText().trim();
			this.fromDate				= driver.findElement(By.id(propertyMap.get("ConfPg_DepartureDate_Lbl_id").trim())).getText().trim();
			this.toDate					= driver.findElement(By.id(propertyMap.get("ConfPg_ReturnDate_Lbl_id").trim())).getText().trim();
			done = true;
			ArrayList<WebElement> passengerDetails = new ArrayList<WebElement>(driver.findElements(By.className(propertyMap.get("ConfPg_PassengerDetails_div_class").trim())));
			for(int p = 0; p<passengerDetails.size(); p++)
			{
				RatesperPassenger rate		= new RatesperPassenger();
				
				String id					= "";
				id							= passengerDetails.get(p).findElement(By.tagName("ul")).getAttribute("id").trim();
				ArrayList<WebElement> list	= new ArrayList<WebElement>(passengerDetails.get(p).findElement(By.tagName("ul")).findElements(By.tagName("li")));
				try {
					String type = "";
					if(id.equalsIgnoreCase(propertyMap.get("ConfPg_ADTPassengerDetails_ul_id").trim())) 
					{
						type = list.get(0).getText().trim();
					}
					else if(id.equalsIgnoreCase(propertyMap.get("ConfPg_CHDPassengerDetails_ul_id").trim()))
					{
						type = list.get(0).getText().trim();
					}
					else if(id.equalsIgnoreCase(propertyMap.get("ConfPg_INFPassengerDetails_ul_id").trim()))
					{
						type = list.get(0).getText().trim();
					}
						
					rate.setPassengertype(type);
					String rateperpsngr		= "";
					rateperpsngr			= list.get(2).getText().trim();
					rate.setRateperpsngr(rateperpsngr);
					String noofpassengers	= "";
					noofpassengers			= list.get(3).getText().trim();
					rate.setNoofpassengers(noofpassengers);
					
					String [] total		= new String[4];
					total				= passengerDetails.get(p).getText().split("\\n");
					String totval		= total[3];
					rate.setTotal(totval);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				ratelist.add(rate);
			}
			
			
			ArrayList<WebElement> journeyDetails	= new ArrayList<WebElement>(driver.findElements(By.className(propertyMap.get("ConfPg_JourneyDetails_div_class").trim())));
			ArrayList<WebElement> journeyCellsOut	= new ArrayList<WebElement>(journeyDetails.get(0).findElements(By.className(propertyMap.get("ConfPg_JourneyOutCells_div_class").trim())));
			ArrayList<WebElement> journeyCellsIn	= new ArrayList<WebElement>(journeyDetails.get(0).findElements(By.className(propertyMap.get("ConfPg_JourneyInCells_div_class").trim())));
			ArrayList<Flight> outFlights			= new ArrayList<Flight>();
			ArrayList<Flight> inFlights				= new ArrayList<Flight>();
			for(int out = 0; out<journeyCellsOut.size(); out++)
			{
				Flight flight = new Flight();
				WebElement ele = journeyCellsOut.get(out);
				String depDate = "";
				String depTime = "";
				String arrDate = "";
				String arrTime = "";
				String depLoc  = "";
				String arrLoc  = "";
				String airline = "";
				try {
					depDate	= ele.findElement(By.id(propertyMap.get("ConfPg_JourneyOutDepDateTime_div_id").trim())).getText();
					depDate = depDate.replace("Depart ", "").split(" ")[0];
					depDate = CommonValidator.formatDateToCommon(depDate, "dd-MMM-yyyy");
					flight.setDepartureDate(depDate);
					
					depTime = ele.findElement(By.id(propertyMap.get("ConfPg_JourneyOutDepDateTime_div_id").trim())).getText();
					depTime = depTime.replace("Depart ", "").split(" ")[1];
					depTime = CommonValidator.formatTimeToCommon(depTime);
					flight.setDepartureTime(depTime);
					
					arrDate = ele.findElement(By.id(propertyMap.get("ConfPg_JourneyOutArrDateTime_div_id").trim())).getText();
					arrDate = arrDate.replace("Arrive ", "").split(" ")[0];
					arrDate = CommonValidator.formatDateToCommon(arrDate, "dd-MMM-yyyy");
					flight.setArrivalDate(arrDate);
					
					arrTime = ele.findElement(By.id(propertyMap.get("ConfPg_JourneyOutArrDateTime_div_id").trim())).getText();
					arrTime = arrTime.replace("Arrive ", "").split(" ")[1];
					arrTime = CommonValidator.formatTimeToCommon(arrTime);
					flight.setArrivalTime(arrTime);
					
					depLoc	= ele.findElement(By.id(propertyMap.get("ConfPg_JourneyOutDepLocation_div_id").trim())).getText();
					flight.setDeparture_port(depLoc);
					flight.setDepartureLocationCode(depLoc);
					
					arrLoc	= ele.findElement(By.id(propertyMap.get("ConfPg_JourneyOutArrLocation_div_id").trim())).getText();
					flight.setArrival_port(arrLoc);
					flight.setArrivalLocationCode(arrLoc);
					
					airline	= ele.findElement(By.id(propertyMap.get("ConfPg_JourneyOutAirLine_div_id").trim())).getText();
					flight.setMarketingAirline(airline);
					flight.setMarketingAirline_Loc_Code(airline);
					flight.setFlightNo(airline);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				outFlights.add(flight);
			}
			this.outbound.setOutBflightlist(outFlights);
			
			if(twoway)
			{
				for(int in = 0; in<journeyCellsIn.size(); in++)
				{
					Flight flight = new Flight();
					WebElement ele = journeyCellsIn.get(in);
					String depDate = "";
					String depTime = "";
					String arrDate = "";
					String arrTime = "";
					String depLoc  = "";
					String arrLoc  = "";
					String airline = "";
					try {
						depDate	= ele.findElement(By.id(propertyMap.get("ConfPg_JourneyInDepDateTime_div_id").trim())).getText();
						depDate = depDate.replace("Depart ", "").split(" ")[0];
						depDate = CommonValidator.formatDateToCommon(depDate, "dd-MMM-yyyy");
						flight.setDepartureDate(depDate);
						
						depTime = ele.findElement(By.id(propertyMap.get("ConfPg_JourneyInDepDateTime_div_id").trim())).getText();
						depTime = depTime.replace("Depart ", "").split(" ")[1];
						depTime = CommonValidator.formatTimeToCommon(depTime);
						flight.setDepartureTime(depTime);
						
						arrDate = ele.findElement(By.id(propertyMap.get("ConfPg_JourneyInArrDateTime_div_id").trim())).getText();
						arrDate = arrDate.replace("Arrive ", "").split(" ")[0];
						arrDate = CommonValidator.formatDateToCommon(arrDate, "dd-MMM-yyyy");
						flight.setArrivalDate(arrDate);
						
						arrTime = ele.findElement(By.id(propertyMap.get("ConfPg_JourneyInArrDateTime_div_id").trim())).getText();
						arrTime = arrTime.replace("Arrive ", "").split(" ")[1];
						arrTime = CommonValidator.formatTimeToCommon(arrTime);
						flight.setArrivalTime(arrTime);
						
						depLoc	= ele.findElement(By.id(propertyMap.get("ConfPg_JourneyInDepLocation_div_id").trim())).getText();
						flight.setDeparture_port(depLoc);
						flight.setDepartureLocationCode(depLoc);
						arrLoc	= ele.findElement(By.id(propertyMap.get("ConfPg_JourneyInArrLocation_div_id").trim())).getText();
						flight.setArrival_port(arrLoc);
						flight.setArrivalLocationCode(arrLoc);
						
						airline	= ele.findElement(By.id(propertyMap.get("ConfPg_JourneyInAirLine_div_id").trim())).getText();
						flight.setMarketingAirline(airline);
						flight.setMarketingAirline_Loc_Code(airline);
						flight.setFlightNo(airline);
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					inFlights.add(flight);
				}
				this.inbound.setInBflightlist(inFlights);
			}
			
			try {
				
				this.priceinfo = new UIPriceInfo();
				String currencyCode = "";
				String Totalbefore	= "0";
				String discount1	= "0";
				String Tax			= "0";
				String Bookingfee	= "0";
				String TotalCost	= "0";
				String Subtotal		= "0";
				String BookingFee2	= "0";
				String Taxplusother	= "0";
				String Total		= "0";
				String Amountprocess= "0";
				
				currencyCode	= driver.findElement(By.id(propertyMap.get("ConfPg_CurrencyCodeTop_Lbl_id").trim())).getText().trim();
				this.priceinfo.setCurrencycode(currencyCode);
				
				Totalbefore		= driver.findElement(By.id(propertyMap.get("ConfPg_TotalBeforTax_Lbl_id").trim())).getText().trim();
				this.priceinfo.setTotalbefore(Totalbefore);
				
				if(searchObject.isApplyDiscount())
				{
					discount1		= driver.findElement(By.id("discountcoupanvalue")).getText().trim();
					this.priceinfo.setDiscount(discount1);
				}
				
				Tax				= driver.findElement(By.id(propertyMap.get("ConfPg_Tax_Lbl_id").trim())).getText().trim();
				this.priceinfo.setTax(Tax);
				
				Bookingfee		= driver.findElement(By.id(propertyMap.get("ConfPg_BookingFee_Lbl_id").trim())).getText().trim();
				this.priceinfo.setBookingfee(Bookingfee);
				
				//TotalCost		= driver.findElement(By.id(propertyMap.get("ConfPg_TotalCost_Lbl_id").trim())).getText().trim();
				this.priceinfo.setTotalCost(TotalCost);
				
				Subtotal		= driver.findElement(By.id(propertyMap.get("ConfPg_Subtotal_Lbl_id").trim())).getText().trim();
				this.priceinfo.setSubtotal(Subtotal);
				
				BookingFee2		= driver.findElement(By.id(propertyMap.get("ConfPg_BookingFee2_Lbl_id").trim())).getText().trim();
				this.priceinfo.setBookingFee(BookingFee2);
				
				Taxplusother	= driver.findElement(By.id(propertyMap.get("ConfPg_TaxANDOther_Lbl_id").trim())).getText().trim(); 
				this.priceinfo.setTaxplusother(Taxplusother);
				
				Total			= driver.findElement(By.id(propertyMap.get("ConfPg_TotalAmount_Lbl_id").trim())).getText().trim();
				this.priceinfo.setTotal(Total);
				
				Amountprocess	= driver.findElement(By.id(propertyMap.get("ConfPg_ProcessNow_Lbl_id").trim())).getText().trim(); 
				this.priceinfo.setAmountprocess(Amountprocess);
				
				Amountprocess	= driver.findElement(By.id(propertyMap.get("ConfPg_ProcessNowByAirline_Lbl_id").trim())).getText().trim(); 
				this.priceinfo.setAmountprocessAirline(Amountprocess);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			try {
				
				this.maincustomer = new Traveler();
				this.maincustomer.setGivenName( driver.findElement(By.id(propertyMap.get("ConfPg_UserInfoCusFName_Lbl_id").trim())).getText().trim() );
				this.maincustomer.setSurname( driver.findElement(By.id(propertyMap.get("ConfPg_UserInfoCusLName_Lbl_id").trim())).getText().trim() );
				
				Address maincusaddress = new Address();
				maincusaddress.setAddressStreetNo( driver.findElement(By.id(propertyMap.get("ConfPg_UserInfoCusAddress1_Lbl_id").trim())).getText().trim() );
				maincusaddress.setAddressCity( driver.findElement(By.id(propertyMap.get("ConfPg_UserInfoCusCity_Lbl_id").trim())).getText().trim() );
				maincusaddress.setAddressCountry( driver.findElement(By.id(propertyMap.get("ConfPg_UserInfoCusCountry_Lbl_id").trim())).getText().trim() );
				maincusaddress.setStateProv( driver.findElement(By.id(propertyMap.get("ConfPg_UserInfoCusState_Lbl_id").trim())).getText().trim() );
				maincusaddress.setPostalCode( driver.findElement(By.id(propertyMap.get("ConfPg_UserInfoCusPostCode_Lbl_id").trim())).getText().trim() );
				this.maincustomer.setAddress(maincusaddress);
				
				this.maincustomer.setPhoneNumber( driver.findElement(By.id(propertyMap.get("ConfPg_UserInfoCusPhone_Lbl_id").trim())).getText().trim() );
				this.maincustomer.setEmergencyNo(driver.findElement(By.id(propertyMap.get("ConfPg_UserInfoCusEmergencyTel_Lbl_id").trim())).getText().trim() );
				this.maincustomer.setEmail( driver.findElement(By.id(propertyMap.get("ConfPg_UserInfoCusEmail_Lbl_id").trim())).getText().trim() );
			
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			/*try {
				ArrayList<WebElement> psngrDetails = new ArrayList<WebElement>( driver.findElements(By.className(propertyMap.get("ConfPg_FlightPsngrDetails_div_class").trim())) );
				
				//ArrayList<WebElement> header = new ArrayList<WebElement>( psngrDetails.get(index))
				
				for(int c = 0; c < psngrDetails.size(); c++)
				{
					ArrayList<WebElement> header = new ArrayList<WebElement>( psngrDetails.get(c).findElements(By.className(propertyMap.get("ConfPg_FlightPsngrDetails_divHeader_class").trim())) );
					String text = header.get(0).getText();
					
					if(text.contains("Adult") || text.contains("adult"))
					{
						ArrayList<WebElement> detailsADT = new ArrayList<WebElement>( psngrDetails.get(c).findElements(By.className(propertyMap.get("ConfPg_FlightPsngrDetails_divBody_class").trim())) );
						Traveler adult = new Traveler();
						adult.setNamePrefixTitle();
						adult.setGivenName();
						adult.setSurname();
						adult.setPhoneNumber();
						
					}
					else if(text.contains("Child") || text.contains("Child"))
					{
						ArrayList<WebElement> detailsCHD = new ArrayList<WebElement>( psngrDetails.get(c).findElements(By.className(propertyMap.get("ConfPg_FlightPsngrDetails_divBody_class").trim())) );
						
					}
					else if(text.contains("Infant") || text.contains("Infant"))
					{
						ArrayList<WebElement> detailsINF = new ArrayList<WebElement>( psngrDetails.get(c).findElements(By.className(propertyMap.get("ConfPg_FlightPsngrDetails_divBody_class").trim())) );
							
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}*/
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Done Bro...");
		setDone(done);
		
		return done;
	}
	
	
}
