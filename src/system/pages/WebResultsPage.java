package system.pages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;



import org.openqa.selenium.support.ui.Select;

import com.utilities.Screenshot;
import com.utilities.ScreenshotPathSingleton;

import system.classes.Flight;
import system.classes.Inbound;
import system.classes.Outbound;
import system.classes.CartFlight;
import system.classes.SearchObject;
import system.classes.UIFlightItinerary;

public class WebResultsPage {

	private ArrayList<UIFlightItinerary>	Resultlist		= new ArrayList<UIFlightItinerary>();
	private HashMap<String, String>			propertyMap		= new HashMap<String, String>();
	private boolean							done			= false;
	private CartFlight						cart			= new CartFlight();
	private boolean							twoway			= false;
	private String							tripType		= "";
	private String							fromBox			= "";
	private String							toBox			= "";
	private String							fromDate		= "";
	private String							toDate			= "";
	private String							paxCount		= "";
	private String							common			= "";
	private String							fromLabel		= "";
	private String							toLabel			= "";
	private ArrayList<WebElement>			outboundRows	= new ArrayList<WebElement>();
	private ArrayList<WebElement>			inboundRows		= new ArrayList<WebElement>();
	private ArrayList<WebElement>			flights			= new ArrayList<WebElement>();
	
	
	
	public HashMap<String, String> getPropertyMap() {
		return propertyMap;
	}

	public void setPropertyMap(HashMap<String, String> propertyMap) {
		this.propertyMap = propertyMap;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

	public boolean isTwoway() {
		return twoway;
	}

	public void setTwoway(boolean twoway) {
		this.twoway = twoway;
	}

	public String getTripType() {
		return tripType;
	}

	public void setTripType(String tripType) {
		this.tripType = tripType;
	}

	public String getFromBox() {
		return fromBox;
	}

	public void setFromBox(String fromBox) {
		this.fromBox = fromBox;
	}

	public String getToBox() {
		return toBox;
	}

	public void setToBox(String toBox) {
		this.toBox = toBox;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getPaxCount() {
		return paxCount;
	}

	public void setPaxCount(String paxCount) {
		this.paxCount = paxCount;
	}

	public String getCommon() {
		return common;
	}

	public void setCommon(String common) {
		this.common = common;
	}

	public String getFromLabel() {
		return fromLabel;
	}

	public void setFromLabel(String fromLabel) {
		this.fromLabel = fromLabel;
	}

	public String getToLabel() {
		return toLabel;
	}

	public void setToLabel(String toLabel) {
		this.toLabel = toLabel;
	}

	public ArrayList<WebElement> getOutboundRows() {
		return outboundRows;
	}

	public void setOutboundRows(ArrayList<WebElement> outboundRows) {
		this.outboundRows = outboundRows;
	}

	public ArrayList<WebElement> getInboundRows() {
		return inboundRows;
	}

	public void setInboundRows(ArrayList<WebElement> inboundRows) {
		this.inboundRows = inboundRows;
	}

	public ArrayList<WebElement> getFlights() {
		return flights;
	}

	public void setFlights(ArrayList<WebElement> flights) {
		this.flights = flights;
	}

	public void setResultlist(ArrayList<UIFlightItinerary> resultlist) {
		Resultlist = resultlist;
	}

	//Constructor
	public WebResultsPage(HashMap<String, String> Map) {
		propertyMap = Map;
	}	
	
	public CartFlight getCart() {
		return cart;
	}

	public void setCart(CartFlight cart) {
		this.cart = cart;
	}
	
	public boolean setResults(WebDriver driver, SearchObject searchObject)
	{
		String scenarioCommonPath = "";
		@SuppressWarnings("unused")
		String scenarioFailedPath = "";
		scenarioCommonPath = ScreenshotPathSingleton.getInstance().getScenarioCommonPath();
		scenarioFailedPath = ScreenshotPathSingleton.getInstance().getScenarioFailedPath();
		
		if(searchObject.getTriptype().equalsIgnoreCase("Round Trip")) {
			twoway = true;
		}
		
		try {
			try {
				Screenshot.takeScreenshot(scenarioCommonPath + "/Results page.jpg", driver);
			} catch (Exception e) {
				
			}
			
			
			int nflights = 0;
			driver.switchTo().defaultContent();
			String numofflights = "0"; 
			numofflights = driver.findElement(By.className(propertyMap.get("ResultPg_FlightCount_Lbl_class"))).getText();
			
			nflights = Integer.parseInt(numofflights.trim());//Get no of flights
			int navcount1 = nflights/10;//Per page 10 find how many pages to navigate
			int navcount2 = nflights%10;//Remaining flights
			
			int navigate = navcount1;
			
			if((navcount2>0)&&(navcount2<10))
			{
				navigate+=1;
			}
			int clickpattern = 2; 
			int count = 0;
			
			if(Integer.parseInt(propertyMap.get("Flight_PageIterations")) <= navigate)
				navigate	= Integer.parseInt(propertyMap.get("Flight_PageIterations"));
			
			Select select = new Select(driver.findElement(By.className(propertyMap.get("ResultPg_TripType_DropDwn_class").trim())));
			WebElement tmp = select.getFirstSelectedOption();
			this.tripType	= tmp.getText().trim();
			this.fromBox	= driver.findElement(By.id(propertyMap.get("ResultPg_From_TxtBox_id").trim())).getAttribute("value");
			this.toBox		= driver.findElement(By.id(propertyMap.get("ResultPg_To_TxtBox_id").trim())).getAttribute("value");
			
			try {
				this.fromDate	= driver.findElement(By.id(propertyMap.get("ResultPg_DateRange_TxtBox_id").trim())).getAttribute("value").trim().split(" to ")[0].trim();
				this.toDate		= driver.findElement(By.id(propertyMap.get("ResultPg_DateRange_TxtBox_id").trim())).getAttribute("value").trim().split(" to ")[1].trim();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				this.paxCount	= driver.findElement(By.id(propertyMap.get("ResultPg_Pax_TxtBox_class").trim())).getAttribute("value").trim();
			} catch (Exception e) {
				
			}
			
			this.fromLabel	= driver.findElement(By.id(propertyMap.get("ResultPg_FromTo_Lbl_id").trim())).getText().trim();
			this.toLabel	= driver.findElement(By.id(propertyMap.get("ResultPg_FromTo_Lbl_id").trim())).getText().trim();
			
			
			while(count<navigate)
			{
				try {
					Screenshot.takeScreenshot(scenarioCommonPath + "/Results page "+count+".jpg", driver);
				} catch (Exception e) {
					
				}
				
				
				try
				{	
					flights = new ArrayList<WebElement>(driver.findElements(By.className(propertyMap.get("ResultPg_FlightItinerary_article_class").trim()/*"result-container-flights"*/)));
					
					Iterator<WebElement> flightsIter = flights.iterator();
					int nt = 1;
					int index = 0;
					whileloop:while(flightsIter.hasNext())
					{
						if(nt > Integer.parseInt(propertyMap.get("Flight_Iterations")))
						break whileloop;
						
						UIFlightItinerary flightItinerary = new UIFlightItinerary();
						
						String OBDepFlightCode		= "";
						//OBDepFlightCode				= propertyMap.get("ResultPg_OutDepFlightCode_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1));
						OBDepFlightCode				= flights.get(index).findElement(By.id(propertyMap.get("ResultPg_OutDepFlightCode_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
						flightItinerary.setOBDepFlightCode(OBDepFlightCode);
						
						String OBDepLocationCode	= "";
						OBDepLocationCode			= flights.get(index).findElement(By.id(propertyMap.get("ResultPg_OutDepLocCode_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim().split("[()]")[1].trim();
						flightItinerary.setOBDepLocationCode(OBDepLocationCode);
						
						String OBDepDate			= "";
						OBDepDate					= flights.get(index).findElement(By.id(propertyMap.get("ResultPg_OutDepDate_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
						flightItinerary.setOBDepDate(OBDepDate);
						
						String OBDepTime			= "";
						OBDepTime					= flights.get(index).findElement(By.id(propertyMap.get("ResultPg_OutDepTime_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
						flightItinerary.setOBDepTime(OBDepTime);
						
						String OBArrFlightCode		= "";
						OBArrFlightCode				= flights.get(index).findElement(By.id(propertyMap.get("ResultPg_OutArrFlightCode_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
						flightItinerary.setOBArrFlightCode(OBArrFlightCode);
						
						String OBArrLocationCode	= "";
						OBArrLocationCode			= flights.get(index).findElement(By.id(propertyMap.get("ResultPg_OutArrLocCode_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim().split("[()]")[1].trim();
						flightItinerary.setOBArrLocationCode(OBArrLocationCode);
						
						String OBArrDate			= "";
						OBArrDate					= flights.get(index).findElement(By.id(propertyMap.get("ResultPg_OutArrDate_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
						flightItinerary.setOBArrDate(OBArrDate);
						
						String OBArrTime			= "";
						OBArrTime					= flights.get(index).findElement(By.id(propertyMap.get("ResultPg_OutArrTime_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
						flightItinerary.setOBArrTime(OBArrTime);
						
						String OBDuration			= "";
						OBDuration					= flights.get(index).findElement(By.id(propertyMap.get("ResultPg_OutDuration_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
						flightItinerary.setOBDuration(OBDuration);
						
						String OBStops				= "";
						OBStops						= flights.get(index).findElement(By.id(propertyMap.get("ResultPg_OutStops_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
						flightItinerary.setOBStops(OBStops);
						
						String OBLayover			= "";
						OBDuration					= flights.get(index).findElement(By.id(propertyMap.get("ResultPg_OutLayover_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
						flightItinerary.setOBLayover(OBLayover);
						
						
						if(twoway)
						{
							String IBDepFlightCode		= "";
							IBDepFlightCode				= flights.get(index).findElement(By.id(propertyMap.get("ResultPg_InDepFlightCode_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
							flightItinerary.setIBDepFlightCode(IBDepFlightCode);
							
							String IBDepLocationCode	= "";
							IBDepLocationCode			= flights.get(index).findElement(By.id(propertyMap.get("ResultPg_InDepLocCode_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim().split("[()]")[1].trim();
							flightItinerary.setIBDepLocationCode(IBDepLocationCode);
							
							String IBDepDate			= "";
							IBDepDate					= flights.get(index).findElement(By.id(propertyMap.get("ResultPg_InDepDate_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
							flightItinerary.setIBDepDate(IBDepDate);
							
							String IBDepTime			= "";
							IBDepTime					= flights.get(index).findElement(By.id(propertyMap.get("ResultPg_InDepTime_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
							flightItinerary.setIBDepTime(IBDepTime);
							
							String IBArrFlightCode		= "";
							IBArrFlightCode				= flights.get(index).findElement(By.id(propertyMap.get("ResultPg_InArrFlightCode_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
							flightItinerary.setIBArrFlightCode(IBArrFlightCode);
							
							String IBArrLocationCode	= "";
							IBArrLocationCode			= flights.get(index).findElement(By.id(propertyMap.get("ResultPg_InArrLocCode_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim().split("[()]")[1].trim();
							flightItinerary.setIBArrLocationCode(IBArrLocationCode);
							
							String IBArrDate			= "";
							IBArrDate					= flights.get(index).findElement(By.id(propertyMap.get("ResultPg_InArrDate_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
							flightItinerary.setIBArrDate(IBArrDate);
							
							String IBArrTime			= "";
							IBArrTime					= flights.get(index).findElement(By.id(propertyMap.get("ResultPg_InArrTime_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
							flightItinerary.setIBArrTime(IBArrTime);
							
							String IBDuration			= "";
							IBDuration					= flights.get(index).findElement(By.id(propertyMap.get("ResultPg_InDuration_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
							flightItinerary.setIBDuration(IBDuration);
							
							String IBStops				= "";
							IBStops						= flights.get(index).findElement(By.id(propertyMap.get("ResultPg_InStops_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
							flightItinerary.setIBStops(IBStops);
							
							String IBLayover			= "";
							IBLayover					= flights.get(index).findElement(By.id(propertyMap.get("ResultPg_InLayover_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
							flightItinerary.setIBLayover(IBLayover);	
						}
						
						
						String currencyCode = "";
						String cost			= "0";
						try {
							currencyCode		= flights.get(index).findElement(By.className(propertyMap.get("ResultPg_ItineraryCurrencyCode_Lbl_class").trim())).getText().trim();
							cost				= flights.get(index).findElement(By.className(propertyMap.get("ResultPg_ItineraryPrice_Lbl_class").trim())).getText().trim();
						} catch (Exception e) {
							e.printStackTrace();
						}
						flightItinerary.setCurrencyCode(currencyCode);
						flightItinerary.setTotalCost(cost);
						
						
						flights.get(index).findElement(By.partialLinkText(propertyMap.get("ResultPg_MoreFlightDetails_PartialLinkText").trim())).click();
						try {
							Screenshot.takeScreenshot(scenarioCommonPath + "/More details "+nt+".jpg", driver);
						} catch (Exception e) {
							
						}
						
						try {
							
							outboundRows = 	new ArrayList<WebElement>(flights.get(index).findElements(By.className(propertyMap.get("ResultPg_MoreOutDetails_Row_class").trim())));
							outboundRows = new ArrayList<WebElement>(outboundRows.get(0).findElements(By.className(propertyMap.get("ResultPg_MoreOutDetails_RowRow_class").trim()))); 
							Outbound outbound = new Outbound();
							ArrayList<Flight> outboundFlights = new ArrayList<Flight>(); 
							for(int out = 0; out<outboundRows.size(); out++) 
							{
								Flight flight = new Flight();
								
								String airLine = "";
								airLine = outboundRows.get(out).findElement(By.id(propertyMap.get("ResultPg_MoreOutAirline_Lbl_id").trim().replace("REPLACE", String.valueOf(out)))).getText();
								flight.setMarketingAirline(airLine);
								
								String depDate = "";
								depDate = outboundRows.get(out).findElement(By.id(propertyMap.get("ResultPg_MoreOutDepDatetime_Lbl_id").trim().replace("REPLACE", String.valueOf(out)))).getText().split(" : ")[0].trim();
								flight.setDepartureDate(depDate);
								String depTime = "";
								depTime = outboundRows.get(out).findElement(By.id(propertyMap.get("ResultPg_MoreOutDepDatetime_Lbl_id").trim().replace("REPLACE", String.valueOf(out)))).getText().split(" : ")[1].replace("hrs", "").trim();
								flight.setDepartureTime(depTime);
								
								String depLocation = "";
								depLocation = outboundRows.get(out).findElement(By.id(propertyMap.get("ResultPg_MoreOutDepLoc_Lbl_id").trim().replace("REPLACE", String.valueOf(out)))).getText();
								flight.setDeparture_port(depLocation);
								
								String depLocationCode = "";
								depLocationCode = outboundRows.get(out).findElement(By.id(propertyMap.get("ResultPg_MoreOutDepLoc_Lbl_id").trim().replace("REPLACE", String.valueOf(out)))).getText();
								flight.setDepartureLocationCode(depLocationCode);
								
								String arrDate = "";
								arrDate = outboundRows.get(out).findElement(By.id(propertyMap.get("ResultPg_MoreOutArrDateTime_Lbl_id").trim().replace("REPLACE", String.valueOf(out)))).getText().split(" : ")[0].trim();
								flight.setArrivalDate(arrDate);
								
								String arrTime = "";
								arrTime = outboundRows.get(out).findElement(By.id(propertyMap.get("ResultPg_MoreOutArrDateTime_Lbl_id").trim().replace("REPLACE", String.valueOf(out)))).getText().split(" : ")[1].replace("hrs", "").trim();
								flight.setArrivalTime(arrTime);
								
								String arrLocation = "";
								arrLocation = outboundRows.get(out).findElement(By.id(propertyMap.get("ResultPg_MoreOutArrLoc_Lbl_id").trim().replace("REPLACE", String.valueOf(out)))).getText();
								flight.setArrival_port(arrLocation);
								
								String arrLocationCode = "";
								arrLocationCode = outboundRows.get(out).findElement(By.id(propertyMap.get("ResultPg_MoreOutArrLoc_Lbl_id").trim().replace("REPLACE", String.valueOf(out)))).getText();
								flight.setArrivalLocationCode(arrLocationCode);
								
								String flightNo = "";
								flightNo = outboundRows.get(out).findElement(By.id(propertyMap.get("ResultPg_MoreOutAirline_Lbl_id").trim().replace("REPLACE", String.valueOf(out)))).getText();
								flight.setFlightNo(flightNo);
								
								outboundFlights.add(flight);
							}
							outbound.setOutBflightlist(outboundFlights);
							flightItinerary.setOutbound(outbound);
						} catch (Exception e) {
							e.printStackTrace();
							Screenshot.takeScreenshot(scenarioCommonPath + "/More details "+nt+" read fail.jpg", driver);
						}
						
						if(twoway)
						{
							try {
								//inboundRows =  new ArrayList<WebElement>(flights.get(index).findElements(By.id(propertyMap.get("ResultPg_MoreInDetails_Row_id"))));
								inboundRows =  new ArrayList<WebElement>(flights.get(index).findElements(By.className(propertyMap.get("ResultPg_MoreInDetails_Row_class"))));
								inboundRows =  new ArrayList<WebElement>(inboundRows.get(0).findElements(By.className(propertyMap.get("ResultPg_MoreInDetails_RowRow_class"))));
								Inbound inbound = new Inbound();
								ArrayList<Flight> inboundFlights = new ArrayList<Flight>(); 
								for(int in = 0; in<inboundRows.size(); in++) 
								{
									Flight flight = new Flight();
									
									String airLine = "";
									System.out.println(inboundRows.get(in).findElement(By.id(propertyMap.get("ResultPg_MoreInAirline_Lbl_id").trim().replace("REPLACE", String.valueOf(in)))));
									airLine = inboundRows.get(in).findElement(By.id(propertyMap.get("ResultPg_MoreInAirline_Lbl_id").trim().replace("REPLACE", String.valueOf(in)))).getText();
									flight.setMarketingAirline(airLine);
									
									String depDate = "";
									depDate = inboundRows.get(in).findElement(By.id(propertyMap.get("ResultPg_MoreInDepDatetime_Lbl_id").trim().replace("REPLACE", String.valueOf(in)))).getText().split(" : ")[0].trim();
									flight.setDepartureDate(depDate);
									String depTime = "";
									depTime = inboundRows.get(in).findElement(By.id(propertyMap.get("ResultPg_MoreInDepDatetime_Lbl_id").trim().replace("REPLACE", String.valueOf(in)))).getText().split(" : ")[1].replace("hrs", "").trim();
									flight.setDepartureTime(depTime);
									
									String depLocation = "";
									depLocation = inboundRows.get(in).findElement(By.id(propertyMap.get("ResultPg_MoreInDepLoc_Lbl_id").trim().replace("REPLACE", String.valueOf(in)))).getText();
									flight.setDeparture_port(depLocation);
									String depLocationCode = "";
									depLocationCode = inboundRows.get(in).findElement(By.id(propertyMap.get("ResultPg_MoreInDepLoc_Lbl_id").trim().replace("REPLACE", String.valueOf(in)))).getText();
									flight.setDepartureLocationCode(depLocationCode);
									
									String arrDate = "";
									arrDate = inboundRows.get(in).findElement(By.id(propertyMap.get("ResultPg_MoreInArrDateTime_Lbl_id").trim().replace("REPLACE", String.valueOf(in)))).getText().split(" : ")[0].trim();
									flight.setArrivalDate(arrDate);
									
									String arrTime = "";
									arrTime = inboundRows.get(in).findElement(By.id(propertyMap.get("ResultPg_MoreInArrDateTime_Lbl_id").trim().replace("REPLACE", String.valueOf(in)))).getText().split(" : ")[1].replace("hrs", "").trim();
									flight.setArrivalTime(arrTime);
									
									String arrLocation = "";
									arrLocation = inboundRows.get(in).findElement(By.id(propertyMap.get("ResultPg_MoreInArrLoc_Lbl_id").trim().replace("REPLACE", String.valueOf(in)))).getText();
									flight.setArrival_port(arrLocation);
									
									String arrLocationCode = "";
									arrLocationCode = inboundRows.get(in).findElement(By.id(propertyMap.get("ResultPg_MoreInArrLoc_Lbl_id").trim().replace("REPLACE", String.valueOf(in)))).getText();
									flight.setArrivalLocationCode(arrLocationCode);
									
									String flightNo = "";
									flightNo = inboundRows.get(in).findElement(By.id(propertyMap.get("ResultPg_MoreInAirline_Lbl_id").trim().replace("REPLACE", String.valueOf(in)))).getText();
									flight.setFlightNo(flightNo);
									
									inboundFlights.add(flight);
								}
								inbound.setInBflightlist(inboundFlights);
								flightItinerary.setInbound(inbound);
								
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						
						Resultlist.add(flightItinerary);
						nt++;
						index++;
					}	
				}	
				catch(Exception e)
				{
					e.printStackTrace();
					Screenshot.takeScreenshot(scenarioCommonPath + "/Result page read fail.jpg", driver);
				}

				((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
				clickpattern++;		
				count++; 

			}
			
			clickpattern = 1;
			((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return true;
	}

	public boolean searchAgain(WebDriver driver, SearchObject searchObject)
	{
		String scenarioCommonPath = "";
		@SuppressWarnings("unused")
		String scenarioFailedPath = "";
		scenarioCommonPath = ScreenshotPathSingleton.getInstance().getScenarioCommonPath();
		scenarioFailedPath = ScreenshotPathSingleton.getInstance().getScenarioFailedPath();
		try {
			/*String trip = "";
			if(searchObject.getTriptype().equalsIgnoreCase("One Way Trip"))
			{
				trip = "oneway";
			}
			else if(searchObject.getTriptype().equalsIgnoreCase("Round Trip"))
			{
				trip = "roundtrip";
			}
			else if(searchObject.getTriptype().equalsIgnoreCase("Multi Destination"))
			{
				trip = "multidestination";
			}*/
			
			/*driver.switchTo().defaultContent();
			new Select(driver.findElement(By.className(propertyMap.get("ResultPg_TripType_DropDwn_class")))).selectByValue(trip);
			//driver.findElement(By.id(propertyMap.get("ResultPg_From_TxtBox_id"))).clear();
			driver.findElement(By.id(propertyMap.get("ResultPg_From_TxtBox_id"))).sendKeys(searchObject.getFrom().trim().split("\\|")[0]);
			((JavascriptExecutor)driver).executeScript("document.getElementById('"+propertyMap.get("ResultPg_From_Hidden_id")+"').setAttribute('value','"+searchObject.getFrom().trim()+"');");
			driver.findElement(By.id(propertyMap.get("ResultPg_To_TxtBox_id"))).clear();
			driver.findElement(By.id(propertyMap.get("ResultPg_To_TxtBox_id"))).sendKeys(searchObject.getTo().trim().split("\\|")[0]);
			((JavascriptExecutor)driver).executeScript("document.getElementById('"+propertyMap.get("ResultPg_To_Hidden_id")+"').setAttribute('value','"+searchObject.getTo().trim()+"');");
			driver.findElement(By.className("start-day")).sendKeys("17/03/2015");
			driver.findElement(By.className("end-day")).sendKeys("20/03/2015");*/
			Screenshot.takeScreenshot(scenarioCommonPath + "/Before search again.jpg", driver);
			driver.findElement(By.className(propertyMap.get("ResultPg_SearchAgain_class"))).click();
			Screenshot.takeScreenshot(scenarioCommonPath + "/After search again.jpg", driver);
			
		} catch (Exception e) {
			e.printStackTrace();
			Screenshot.takeScreenshot(scenarioCommonPath + "/Exception in search again", driver);
		}
		
		return true;
	}
	
	public boolean setWebCart(WebDriver driver)
	{
		String scenarioCommonPath	= "";
		String scenarioFailedPath	= "";
		scenarioCommonPath			= ScreenshotPathSingleton.getInstance().getScenarioCommonPath();
		scenarioFailedPath			= ScreenshotPathSingleton.getInstance().getScenarioFailedPath();
		CartFlight	basket			= new CartFlight(); 
		
		try {
			((JavascriptExecutor)driver).executeScript("$('.my-basket-item-header').trigger('mouseover');");
			
			if(driver.findElement(By.className("my-basket-item-details-wrapper-inner")).isDisplayed())
			{
				Screenshot.takeScreenshot(scenarioCommonPath + "/Basket.jpg", driver);
				WebElement				cart			= driver.findElement(By.className("mybasket-air-common-info-wrapper"));
				ArrayList<WebElement>	fromto			= new ArrayList<WebElement>(cart.findElements(By.className("my-basket-header")));
				ArrayList<WebElement>	outInFlights	= new ArrayList<WebElement>(cart.findElements(By.className("my-basket-air-common-info")));
				ArrayList<WebElement>	cost			= new ArrayList<WebElement>(cart.findElements(By.className("my-basket-detals-wrapper")));
				String[] out = outInFlights.get(0).getText().split("\\n");
				
				try {
					
					basket.setLeaveAirport(fromto.get(0).getText());
					
					Outbound outbound = new Outbound();
					ArrayList<Flight> outflights = new ArrayList<Flight>();
					for(int i=0; i< out.length; i++ )
					{
						Flight flight = new Flight();
						flight.setFlightNo(out[0]);
						flight.setMarketingAirline(out[0]);
						flight.setMarketingAirline_Loc_Code(out[0]);
						outflights.add(flight);
					}
					
					outbound.setOutBflightlist(outflights);
					basket.setOutbound(outbound);
					
					try {
						if(twoway)
						{
							basket.setReturnAirport(fromto.get(0).getText());
							String[] in = outInFlights.get(1).getText().split("\\n");
							Inbound inbound = new Inbound();
							ArrayList<Flight> inflights = new ArrayList<Flight>();
							for(int i=0; i< in.length; i++ )
							{
								Flight flight = new Flight();
								flight.setFlightNo(out[0]);
								flight.setMarketingAirline(out[0]);
								flight.setMarketingAirline_Loc_Code(out[0]);
								inflights.add(flight);
							}
							
							inbound.setInBflightlist(inflights);
							basket.setInbound(inbound);
						}
						String currencyCode		= propertyMap.get("");
						currencyCode			= cost.get(0).findElement(By.className("my-basket-details-currency")).getText().trim();
						basket.setCurrencyCode(currencyCode);
					} catch (Exception e) {
						e.printStackTrace();
					}	
					
				} catch (Exception e) {
					e.printStackTrace();
					Screenshot.takeScreenshot(scenarioFailedPath + "/Basket read fail.jpg", driver);
				}	
				
				try {
					ArrayList<WebElement> priceList = new ArrayList<WebElement>(cost.get(0).findElements(By.className("my-basket-item-price-detail")));
					String totalBeforeTax	= "0";
					totalBeforeTax			= priceList.get(0).getText().split("\\n")[1].trim();
					basket.setTotalBeforeTax(totalBeforeTax);
					String totalTax			= "0";
					totalTax				= priceList.get(1).getText().split("\\n")[1].trim();
					basket.setTotalTax(totalTax);
					String bookingFee		= "0";
					bookingFee				= priceList.get(2).getText().split("\\n")[1].trim();
					basket.setBookingFee(bookingFee);
					String totalCost		= "0";
					totalCost				= priceList.get(3).findElement(By.className("my-basket-item-price-total")).getText().trim();
					basket.setTotalCost(totalCost);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
			}//If displayed
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		((JavascriptExecutor)driver).executeScript("$('.my-basket-item-header').trigger('mouseover');");
		Screenshot.takeScreenshot(scenarioCommonPath + "/Basket after closed.jpg", driver);
		
		this.cart = basket;
		return true;
	}
	
	public ArrayList<UIFlightItinerary> getResultlist() {
		return Resultlist;
	}
	
	
}
