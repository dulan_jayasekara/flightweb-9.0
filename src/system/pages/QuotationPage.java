package system.pages;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import system.classes.Address;
import system.classes.Flight;
import system.classes.Inbound;
import system.classes.Outbound;
import system.classes.RatesperPassenger;
import system.classes.SearchObject;
import system.classes.Traveler;
import system.classes.UICreditCardPayInfo;
import system.classes.UIPriceInfo;

import com.common.Validators.CommonValidator;

public class QuotationPage {

	boolean								 available				= false;
	private HashMap<String, String>		 propertyMap			= new HashMap<String, String>();
	
	private String						 departure				= "";
	private String						 destination			= "";
	private String						 fromDate				= "";
	private String						 toDate					= "";
	
	private String						 bookingReference		= "";
	private String						 reservationNo			= "";
	private String						 supplierConfirmationNo	= "null";
	
	private Outbound					 outbound				= new Outbound();
	private Inbound						 inbound				= new Inbound();
	
	private ArrayList<RatesperPassenger> ratelist				= new ArrayList<RatesperPassenger>();
	
	private UIPriceInfo					 uisummarypay			= null;
	private UICreditCardPayInfo 		 creditcardpay			= null;
	
	private String						 cancellationdate		= "";
	private String						 PackageDeadline		= "";
	private Traveler					 maincustomer			= null;
	private ArrayList<Traveler>			 adult					= null;
	private ArrayList<Traveler> 		 children				= null;
	private ArrayList<Traveler> 		 infant					= null;
	private boolean						 twoway					= false;
	private UIPriceInfo					 priceinfo				= null;
	
	
	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public HashMap<String, String> getPropertyMap() {
		return propertyMap;
	}

	public void setPropertyMap(HashMap<String, String> propertyMap) {
		this.propertyMap = propertyMap;
	}

	public String getDeparture() {
		return departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getBookingReference() {
		return bookingReference;
	}

	public void setBookingReference(String bookingReference) {
		this.bookingReference = bookingReference;
	}

	public String getReservationNo() {
		return reservationNo;
	}

	public void setReservationNo(String reservationNo) {
		this.reservationNo = reservationNo;
	}

	public String getSupplierConfirmationNo() {
		return supplierConfirmationNo;
	}

	public void setSupplierConfirmationNo(String supplierConfirmationNo) {
		this.supplierConfirmationNo = supplierConfirmationNo;
	}

	public Outbound getOutbound() {
		return outbound;
	}

	public void setOutbound(Outbound outbound) {
		this.outbound = outbound;
	}

	public Inbound getInbound() {
		return inbound;
	}

	public void setInbound(Inbound inbound) {
		this.inbound = inbound;
	}

	public ArrayList<RatesperPassenger> getRatelist() {
		return ratelist;
	}

	public void setRatelist(ArrayList<RatesperPassenger> ratelist) {
		this.ratelist = ratelist;
	}

	public UIPriceInfo getUisummarypay() {
		return uisummarypay;
	}

	public void setUisummarypay(UIPriceInfo uisummarypay) {
		this.uisummarypay = uisummarypay;
	}

	public UICreditCardPayInfo getCreditcardpay() {
		return creditcardpay;
	}

	public void setCreditcardpay(UICreditCardPayInfo creditcardpay) {
		this.creditcardpay = creditcardpay;
	}

	public String getCancellationdate() {
		return cancellationdate;
	}

	public void setCancellationdate(String cancellationdate) {
		this.cancellationdate = cancellationdate;
	}

	public String getPackageDeadline() {
		return PackageDeadline;
	}

	public void setPackageDeadline(String packageDeadline) {
		PackageDeadline = packageDeadline;
	}

	public Traveler getMaincustomer() {
		return maincustomer;
	}

	public void setMaincustomer(Traveler maincustomer) {
		this.maincustomer = maincustomer;
	}

	public ArrayList<Traveler> getAdult() {
		return adult;
	}

	public void setAdult(ArrayList<Traveler> adult) {
		this.adult = adult;
	}

	public ArrayList<Traveler> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<Traveler> children) {
		this.children = children;
	}

	public ArrayList<Traveler> getInfant() {
		return infant;
	}

	public void setInfant(ArrayList<Traveler> infant) {
		this.infant = infant;
	}

	public boolean isTwoway() {
		return twoway;
	}

	public void setTwoway(boolean twoway) {
		this.twoway = twoway;
	}

	public UIPriceInfo getPriceinfo() {
		return priceinfo;
	}

	public void setPriceinfo(UIPriceInfo priceinfo) {
		this.priceinfo = priceinfo;
	}
	
	public QuotationPage(HashMap<String, String> Map)
	{
		propertyMap = Map;
	}
	
	public boolean setQuotation(WebDriver driver, SearchObject searchObject)
	{
		if(searchObject.getTriptype().equals("Round Trip"))
		{
			twoway = true;
		}
		
		try {
			try {
				
				this.bookingReference		= driver.findElement(By.id(propertyMap.get("ConfPg_BookingReference_Lbl_id").trim())).getText();
				available					= true;
				this.reservationNo			= driver.findElement(By.xpath("/html/body/section/div/div/section/div/article[1]/ul/li[3]/p[2]/b")).getText().trim();
				this.departure				= driver.findElement(By.id(propertyMap.get("ConfPg_Origin_Lbl_id").trim())).getText();
				this.destination			= driver.findElement(By.id(propertyMap.get("ConfPg_Destination_Lbl_id").trim())).getText();
				this.fromDate				= driver.findElement(By.id(propertyMap.get("ConfPg_DepartureDate_Lbl_id").trim())).getText().replace("to", "").trim();
				this.toDate					= driver.findElement(By.id(propertyMap.get("ConfPg_ReturnDate_Lbl_id").trim())).getText();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				
				ArrayList<WebElement> passengerDetails = new ArrayList<WebElement>(driver.findElements(By.className(propertyMap.get("ConfPg_PassengerDetails_div_class").trim())));
				for(int p = 0; p<passengerDetails.size(); p++)
				{
					RatesperPassenger rate		= new RatesperPassenger();
					
					String id					= "";
					id							= passengerDetails.get(p).findElement(By.tagName("ul")).getAttribute("id").trim();
					ArrayList<WebElement> list	= new ArrayList<WebElement>(passengerDetails.get(p).findElement(By.tagName("ul")).findElements(By.tagName("li")));
					try {
						String type = "";
						if(id.equalsIgnoreCase(propertyMap.get("ConfPg_ADTPassengerDetails_ul_id").trim())) 
						{
							type = list.get(0).getText();
						}
						else if(id.equalsIgnoreCase(propertyMap.get("ConfPg_CHDPassengerDetails_ul_id").trim()))
						{
							type = list.get(0).getText();
						}
						else if(id.equalsIgnoreCase(propertyMap.get("ConfPg_INFPassengerDetails_ul_id").trim()))
						{
							type = list.get(0).getText();
						}
							
						rate.setPassengertype(type);
						String rateperpsngr		= "";
						rateperpsngr			= list.get(2).getText();
						if(rateperpsngr.contains("Rate per Passenger"))
						{
							rateperpsngr = rateperpsngr.replace("Rate per Passenger", "").trim();
						}
						rate.setRateperpsngr(rateperpsngr);
						
						String noofpassengers	= "";
						noofpassengers			= list.get(3).getText();
						if(noofpassengers.contains("Number of Passengers"))
						{
							noofpassengers = noofpassengers.replace("Number of Passengers", "").trim();
						}
						rate.setNoofpassengers(noofpassengers);
						
						String [] total		= new String[4];
						total				= passengerDetails.get(p).getText().split("\\n");
						String totval		= total[3];
						if(totval.contains("Total"))
						{
							totval = totval.replace("Total", "").trim();
							totval = totval.split("[)]")[1];
						}
						rate.setTotal(totval);
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					ratelist.add(rate);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			ArrayList<WebElement>	journeyDetails	= new ArrayList<WebElement>(driver.findElements(By.className(propertyMap.get("ConfPg_JourneyDetails_div_class").trim())));
			ArrayList<WebElement>	journeyCellsOut	= new ArrayList<WebElement>(journeyDetails.get(0).findElements(By.className(propertyMap.get("ConfPg_JourneyOutCells_div_class").trim())));
			ArrayList<WebElement>	journeyCellsIn	= new ArrayList<WebElement>(journeyDetails.get(0).findElements(By.className(propertyMap.get("ConfPg_JourneyInCells_div_class").trim())));
			ArrayList<Flight>		outFlights		= new ArrayList<Flight>();
			ArrayList<Flight>		inFlights		= new ArrayList<Flight>();
			
			try {
				for(int out = 0; out<journeyCellsOut.size(); out++)
				{
					Flight		flight	= new Flight();
					WebElement	ele		= journeyCellsOut.get(out);
					String		depDate = "";
					String		depTime = "";
					String		arrDate = "";
					String		arrTime = "";
					String		depLoc  = "";
					String		arrLoc  = "";
					String		airline = "";
					try {
						depDate	= ele.findElement(By.id(propertyMap.get("ConfPg_JourneyOutDepDateTime_div_id").trim())).getText();
						depDate = depDate.replace("Depart ", "").split(" ")[0];
						depDate = CommonValidator.formatDateToCommon(depDate, "dd-MMM-yyyy");
						flight.setDepartureDate(depDate);
						
						depTime = ele.findElement(By.id(propertyMap.get("ConfPg_JourneyOutDepDateTime_div_id").trim())).getText();
						depTime = depTime.replace("Depart ", "").split(" ")[1];
						depTime = CommonValidator.formatTimeToCommon(depTime);
						flight.setDepartureTime(depTime);
						
						arrDate = ele.findElement(By.id(propertyMap.get("ConfPg_JourneyOutArrDateTime_div_id").trim())).getText();
						arrDate = arrDate.replace("Arrive ", "").split(" ")[0];
						arrDate = CommonValidator.formatDateToCommon(arrDate, "dd-MMM-yyyy");
						flight.setArrivalDate(arrDate);
						
						arrTime = ele.findElement(By.id(propertyMap.get("ConfPg_JourneyOutArrDateTime_div_id").trim())).getText();
						arrTime = arrTime.replace("Arrive ", "").split(" ")[1];
						arrTime = CommonValidator.formatTimeToCommon(arrTime);
						flight.setArrivalTime(arrTime);
						
						depLoc	= ele.findElement(By.id(propertyMap.get("ConfPg_JourneyOutDepLocation_div_id").trim())).getText();
						flight.setDeparture_port(depLoc);
						flight.setDepartureLocationCode(depLoc);
						
						arrLoc	= ele.findElement(By.id(propertyMap.get("ConfPg_JourneyOutArrLocation_div_id").trim())).getText();
						flight.setArrival_port(arrLoc);
						flight.setArrivalLocationCode(arrLoc);
						
						airline	= ele.findElement(By.id(propertyMap.get("ConfPg_JourneyOutAirLine_div_id").trim())).getText();
						flight.setMarketingAirline(airline);
						flight.setMarketingAirline_Loc_Code(airline);
						flight.setFlightNo(airline);
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					outFlights.add(flight);
				}
				this.outbound.setOutBflightlist(outFlights);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			try {
				if(twoway)
				{
					for(int in = 0; in<journeyCellsIn.size(); in++)
					{
						Flight		flight	= new Flight();
						WebElement	ele		= journeyCellsIn.get(in);
						String		depDate = "";
						String		depTime = "";
						String		arrDate = "";
						String		arrTime = "";
						String		depLoc  = "";
						String		arrLoc  = "";
						String		airline = "";
						try {
							depDate	= ele.findElement(By.id(propertyMap.get("ConfPg_JourneyInDepDateTime_div_id").trim())).getText();
							depDate = depDate.replace("Depart ", "").split(" ")[0];
							depDate = CommonValidator.formatDateToCommon(depDate, "dd-MMM-yyyy");
							flight.setDepartureDate(depDate);
							
							depTime = ele.findElement(By.id(propertyMap.get("ConfPg_JourneyInDepDateTime_div_id").trim())).getText();
							depTime = depTime.replace("Depart ", "").split(" ")[1];
							depTime = CommonValidator.formatTimeToCommon(depTime);
							flight.setDepartureTime(depTime);
							
							arrDate = ele.findElement(By.id(propertyMap.get("ConfPg_JourneyInArrDateTime_div_id").trim())).getText();
							arrDate = arrDate.replace("Arrive ", "").split(" ")[0];
							arrDate = CommonValidator.formatDateToCommon(arrDate, "dd-MMM-yyyy");
							flight.setArrivalDate(arrDate);
							
							arrTime = ele.findElement(By.id(propertyMap.get("ConfPg_JourneyInArrDateTime_div_id").trim())).getText();
							arrTime = arrTime.replace("Arrive ", "").split(" ")[1];
							arrTime = CommonValidator.formatTimeToCommon(arrTime);
							flight.setArrivalTime(arrTime);
							
							depLoc	= ele.findElement(By.id(propertyMap.get("ConfPg_JourneyInDepLocation_div_id").trim())).getText();
							flight.setDeparture_port(depLoc);
							flight.setDepartureLocationCode(depLoc);
							arrLoc	= ele.findElement(By.id(propertyMap.get("ConfPg_JourneyInArrLocation_div_id").trim())).getText();
							flight.setArrival_port(arrLoc);
							flight.setArrivalLocationCode(arrLoc);
							
							airline	= ele.findElement(By.id(propertyMap.get("ConfPg_JourneyInAirLine_div_id").trim())).getText();
							flight.setMarketingAirline(airline);
							flight.setMarketingAirline_Loc_Code(airline);
							flight.setFlightNo(airline);
							
						} catch (Exception e) {
							e.printStackTrace();
						}
						inFlights.add(flight);
					}
					this.inbound.setInBflightlist(inFlights);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			try {
				this.priceinfo		= new UIPriceInfo();
				String currencyCode = "";
				String Totalbefore	= "0";
				String Tax			= "0";
				String Bookingfee	= "0";
				//String TotalCost	= "0";
				String Subtotal		= "0";
				String BookingFee2	= "0";
				String Taxplusother	= "0";
				String Total		= "0";
				//String Amountprocess= "0";
				
				currencyCode	= driver.findElement(By.id(propertyMap.get("ConfPg_CurrencyCodeTop_Lbl_id").trim())).getText().trim();
				this.priceinfo.setCurrencycode(currencyCode);
				
				Totalbefore		= driver.findElement(By.id(propertyMap.get("ConfPg_TotalBeforTax_Lbl_id").trim())).getText().trim();
				this.priceinfo.setTotalbefore(Totalbefore);
				
				Tax				= driver.findElement(By.id(propertyMap.get("ConfPg_Tax_Lbl_id").trim())).getText().trim();
				this.priceinfo.setTax(Tax);
				
				Bookingfee		= driver.findElement(By.id(propertyMap.get("ConfPg_BookingFee_Lbl_id").trim())).getText().trim();
				this.priceinfo.setBookingfee(Bookingfee);
				
				//TotalCost		= driver.findElement(By.id(propertyMap.get("ConfPg_TotalCost_Lbl_id").trim())).getText().trim();
				double total	= 0;
				total 			= Double.parseDouble(Totalbefore) + Double.parseDouble(Tax) + Double.parseDouble(Bookingfee);
				this.priceinfo.setTotalCost(String.valueOf(total).replace(".0", ""));
				
				Subtotal		= driver.findElement(By.id(propertyMap.get("ConfPg_Subtotal_Lbl_id").trim())).getText().trim();
				this.priceinfo.setSubtotal(Subtotal);
				
				BookingFee2		= driver.findElement(By.id(propertyMap.get("ConfPg_BookingFee2_Lbl_id").trim())).getText().trim();
				this.priceinfo.setBookingFee(BookingFee2);
				
				Taxplusother	= driver.findElement(By.id(propertyMap.get("ConfPg_TaxANDOther_Lbl_id").trim())).getText().trim(); 
				this.priceinfo.setTaxplusother(Taxplusother);
				
				Total			= driver.findElement(By.id(propertyMap.get("ConfPg_TotalAmount_Lbl_id").trim())).getText().trim();
				this.priceinfo.setTotal(Total);
				
				//Amountprocess	= driver.findElement(By.id(propertyMap.get("ConfPg_ProcessNow_Lbl_id").trim())).getText().trim(); 
				//this.priceinfo.setAmountprocess(Amountprocess);
				
				//Amountprocess	= driver.findElement(By.id(propertyMap.get("ConfPg_ProcessNowByAirline_Lbl_id").trim())).getText().trim(); 
				//this.priceinfo.setAmountprocessAirline(Amountprocess);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			try {
				this.maincustomer = new Traveler();
				this.maincustomer.setGivenName( driver.findElement(By.id(propertyMap.get("ConfPg_UserInfoCusFName_Lbl_id").trim())).getText().trim() );
				this.maincustomer.setSurname( driver.findElement(By.id(propertyMap.get("ConfPg_UserInfoCusLName_Lbl_id").trim())).getText().trim() );
				
				Address maincusaddress = new Address();
				maincusaddress.setAddressStreetNo( driver.findElement(By.id(propertyMap.get("ConfPg_UserInfoCusAddress1_Lbl_id").trim())).getText().trim() );
				maincusaddress.setAddressCity( driver.findElement(By.id(propertyMap.get("ConfPg_UserInfoCusCity_Lbl_id").trim())).getText().trim() );
				maincusaddress.setAddressCountry( driver.findElement(By.id(propertyMap.get("ConfPg_UserInfoCusCountry_Lbl_id").trim())).getText().trim() );
				maincusaddress.setStateProv( driver.findElement(By.id(propertyMap.get("ConfPg_UserInfoCusState_Lbl_id").trim())).getText().trim() );
				maincusaddress.setPostalCode( driver.findElement(By.id(propertyMap.get("ConfPg_UserInfoCusPostCode_Lbl_id").trim())).getText().trim() );
				this.maincustomer.setAddress(maincusaddress);
				
				this.maincustomer.setPhoneNumber( driver.findElement(By.id(propertyMap.get("ConfPg_UserInfoCusPhone_Lbl_id").trim())).getText().trim() );
				this.maincustomer.setEmergencyNo(driver.findElement(By.id(propertyMap.get("ConfPg_UserInfoCusEmergencyTel_Lbl_id").trim())).getText().trim() );
				this.maincustomer.setEmail( driver.findElement(By.id(propertyMap.get("ConfPg_UserInfoCusEmail_Lbl_id").trim())).getText().trim() );
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			/*try {
				ArrayList<WebElement> psngrDetails = new ArrayList<WebElement>( driver.findElements(By.className(propertyMap.get("ConfPg_FlightPsngrDetails_div_class").trim())) );
				
				//ArrayList<WebElement> header = new ArrayList<WebElement>( psngrDetails.get(index))
				
				for(int c = 0; c < psngrDetails.size(); c++)
				{
					ArrayList<WebElement> header = new ArrayList<WebElement>( psngrDetails.get(c).findElements(By.className(propertyMap.get("ConfPg_FlightPsngrDetails_divHeader_class").trim())) );
					String text = header.get(0).getText();
					
					if(text.contains("Adult") || text.contains("adult"))
					{
						ArrayList<WebElement> detailsADT = new ArrayList<WebElement>( psngrDetails.get(c).findElements(By.className(propertyMap.get("ConfPg_FlightPsngrDetails_divBody_class").trim())) );
						Traveler adult = new Traveler();
						adult.setNamePrefixTitle();
						adult.setGivenName();
						adult.setSurname();
						adult.setPhoneNumber();
						
					}
					else if(text.contains("Child") || text.contains("Child"))
					{
						ArrayList<WebElement> detailsCHD = new ArrayList<WebElement>( psngrDetails.get(c).findElements(By.className(propertyMap.get("ConfPg_FlightPsngrDetails_divBody_class").trim())) );
						
						
					}
					else if(text.contains("Infant") || text.contains("Infant"))
					{
						ArrayList<WebElement> detailsINF = new ArrayList<WebElement>( psngrDetails.get(c).findElements(By.className(propertyMap.get("ConfPg_FlightPsngrDetails_divBody_class").trim())) );
						
						
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}*/
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//setAvailable(available);
		
	
		return available;
	}

	public boolean setQuotationCC(WebDriver driver, SearchObject searchObject)
	{
		try
		{
			driver.switchTo().defaultContent();
			driver.switchTo().frame("live_message_frame");
			
			boolean twoway = false;
			if(searchObject.getTriptype().equals("Round Trip"))
			{
				twoway = true;
			}
			
			try
			{
				try {
					String ReservationNo		= driver.findElement(By.id("p_flight_booking_reservation_no_1")).getText().split(":")[1].trim();
					this.reservationNo = ReservationNo;
					this.available = true;
				} catch (Exception e) {
					
				}
				
				
				try {
					WebElement	alloutbound		= driver.findElement(By.id("table_flight_outbound_info"));
					String		outbounddetails = alloutbound.getText();
					
					String[]			outboundArray	= outbounddetails.split("\\n");
					ArrayList<String>	sets			= new ArrayList<String>();
					ArrayList<Flight>	outflightlist	= new ArrayList<Flight>(); 
					
					for(int y=0; y<outboundArray.length; y++)
					{
						if(outboundArray[y].startsWith("Depart") && outboundArray[(y+1)].startsWith("Arrive"))
						{
							String h = outboundArray[y].concat("#").concat(outboundArray[y+1]);
							sets.add(h);
						}
					}
					
					for(int y=0; y<sets.size(); y++)
					{
						Flight	flight	= new Flight();
						String	Sflight	= sets.get(y);
						String	depart	= Sflight.split("#")[0];
						String	arrive	= Sflight.split("#")[1];
						
						String[] departArray = depart.split(" ");
						String ddate	= "";
						ddate			= departArray[2];
						try {
							ddate = CommonValidator.formatDateToCommon(ddate, "dd-MMM-yyyy");
						} catch (Exception e) {
							
						}
						flight.setDepartureDate(ddate);
						flight.setDepartureTime(departArray[2]);
						flight.setFlightNo(depart.split("[()]")[3]);
						flight.setDepartureLocationCode(depart.split("[()]")[1]);
						
						String[] arriveArray = arrive.split(" ");
						String adate	= "";
						adate			= arriveArray[2];
						try {
							adate = CommonValidator.formatDateToCommon(adate, "dd-MMM-yyyy");
						} catch (Exception e) {
							
						}
						flight.setArrivalDate(adate);
						flight.setArrivalTime(arriveArray[3]);
						
						flight.setArrivalLocationCode(arrive.split("[()]")[1]);
						
						outflightlist.add(flight);
						
					}
					
					this.outbound.setOutBflightlist(outflightlist);
					
				} catch (Exception e) {
					
				}
				
				
				try {
					WebElement allinbound = null;
					
					if(twoway)
					{
						allinbound = driver.findElement(By.id("table_flight_inbound_info"));
						String inbounddetails = allinbound.getText();
						
						String[] inboundArray = inbounddetails.split("\\n");
						ArrayList<String> insets = new ArrayList<String>();
						ArrayList<Flight> inflightlist = new ArrayList<Flight>(); 
						
						for(int y=0; y<inboundArray.length; y++)
						{
							if(inboundArray[y].startsWith("Depart") && inboundArray[(y+1)].startsWith("Arrive"))
							{
								String h = inboundArray[y].concat("#").concat(inboundArray[y+1]);
								insets.add(h);
							}
						}
						
						for(int y=0; y<insets.size(); y++)
						{
							Flight flight = new Flight();
							String Sflight = insets.get(y);
							String depart = Sflight.split("#")[0];
							String arrive = Sflight.split("#")[1];
							
							String[] departArray = depart.split(" ");
							
							String ddate	= "";
							ddate			= departArray[2];
							try {
								ddate = CommonValidator.formatDateToCommon(ddate, "dd-MMM-yyyy");
							} catch (Exception e) {
								
							}
							flight.setDepartureDate(ddate);
							
							flight.setDepartureTime(departArray[2]);
							flight.setFlightNo(depart.split("[()]")[3]);
							flight.setDepartureLocationCode(depart.split("[()]")[1]);
							
							String[] arriveArray = arrive.split(" ");
							
							String adate	= "";
							adate			= arriveArray[2];
							try {
								adate = CommonValidator.formatDateToCommon(adate, "dd-MMM-yyyy");
							} catch (Exception e) {
								
							}
							flight.setArrivalDate(adate);
							
							flight.setArrivalTime(arriveArray[3]);
							flight.setArrivalLocationCode(arrive.split("[()]")[1]);
							
							inflightlist.add(flight);
						}
						
						this.inbound.setInBflightlist(inflightlist);
					}
				} catch (Exception e) {
					
				}
				
				WebElement				ratetable	= driver.findElement(By.id("booking_rate_details_table"));
				ArrayList<WebElement>	rateTRtags	= new ArrayList<WebElement> (ratetable.findElements(By.tagName("tr")) );
				
				
				int passengercount = rateTRtags.size() - 6;
				try {
					for(int h=2; h<(2+passengercount); h++)
					{
						RatesperPassenger rate = new RatesperPassenger();
						ArrayList<WebElement> tdInrateTR = new ArrayList<WebElement> (rateTRtags.get(h).findElements(By.tagName("td")) );
						
						if(tdInrateTR.get(0).getText().equalsIgnoreCase("Adult"))
						{
							rate.setPassengertype("ADT");
							rate.setRateperpsngr(tdInrateTR.get(1).getText());
							rate.setNoofpassengers(tdInrateTR.get(2).getText());
							rate.setTotal(tdInrateTR.get(3).getText());
						}
						if(tdInrateTR.get(0).getText().equalsIgnoreCase("Children"))
						{
							rate.setPassengertype("CHD");
							rate.setRateperpsngr(tdInrateTR.get(1).getText());
							rate.setNoofpassengers(tdInrateTR.get(2).getText());
							rate.setTotal(tdInrateTR.get(3).getText());
						}
						if(tdInrateTR.get(0).getText().equalsIgnoreCase("Infant"))
						{
							rate.setPassengertype("INF");
							rate.setRateperpsngr(tdInrateTR.get(1).getText());
							rate.setNoofpassengers(tdInrateTR.get(2).getText());
							rate.setTotal(tdInrateTR.get(3).getText());
						}
						
						ratelist.add(rate);
					}
					
					
					int forward = 2 + passengercount;
					
					this.priceinfo.setTotalbefore(rateTRtags.get(forward).findElement(By.id("td_rate_details_total_rate_before_taxes_1")).getText());
					this.priceinfo.setTax(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_taxes_1")).getText());
					this.priceinfo.setBookingfee(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_booking_fee_1")).getText());
					this.priceinfo.setTotalCost(rateTRtags.get(forward+=1).findElement(By.id("td_rate_details_total_rate_total_cost_1")).getText());
					
					WebElement totfareTable = driver.findElement(By.className("transfer_info_table"));
					String[] totfareArray = totfareTable.getText().split("\\n");
					this.priceinfo.setSubtotal(totfareArray[0].split(":")[1].trim());
					this.priceinfo.setTaxplusother(totfareArray[1].split(":")[1].trim());
					this.priceinfo.setTotal(totfareArray[2].split(":")[1].trim());

				} catch (Exception e) {
					
				}
				
				try
				{
					Address address = new Address();
					//Main customer
					WebElement mainCus = driver.findElement(By.id("table_customer_details1"));
					ArrayList<WebElement> mainCusTRlist = new ArrayList<WebElement>(mainCus.findElements(By.tagName("tr")));
					
					String fname = mainCusTRlist.get(0).findElement(By.id("cusfirstname")).getText();
					String lname = mainCusTRlist.get(0).findElement(By.id("cuslastname")).getText();
					String phoneNo = mainCusTRlist.get(1).findElement(By.id("custelephone")).getText();
					String emergencyNo = mainCusTRlist.get(1).findElement(By.id("cusaltphone")).getText();
					String email1 = mainCusTRlist.get(2).findElement(By.id("cusemail1")).getText();
					String email2 = mainCusTRlist.get(2).findElement(By.id("cusemail2")).getText();
					String address1 = mainCusTRlist.get(3).findElement(By.id("cusaddress1")).getText();
					String Country = mainCusTRlist.get(4).findElement(By.id("cuscountry")).getText();
					try {
						String state = mainCusTRlist.get(4).findElement(By.id("cusstate")).getText();
						address.setStateProv(state);
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					String city = mainCusTRlist.get(5).findElement(By.id("cuscity")).getText();
					String postalcode = "";
					try
					{
						ArrayList<WebElement> postcode = new ArrayList<WebElement>(mainCusTRlist.get(5).findElements(By.id("cuspostcode")));
						postalcode = postcode.get(1).getText().trim();
					}
					catch(Exception e)
					{
						//e.printStackTrace();
					}
					
					//Traveler maincustomer = new Traveler();
					
					this.maincustomer.setGivenName(fname);
					this.maincustomer.setSurname(lname);
					this.maincustomer.setPhoneNumber(phoneNo);
					this.maincustomer.setEmergencyNo(emergencyNo);
					this.maincustomer.setEmail(email1);
					this.maincustomer.setAltemail(email2);
					
					address.setAddressStreetNo(address1);
					address.setAddressCountry(Country);
					
					address.setAddressCity(city);
					address.setPostalCode(postalcode);
					
					this.maincustomer.setAddress(address);
					
					//uiconfirmationpage.setMaincustomer(maincustomer);
				}
				catch(Exception e)
				{
					//e.printStackTrace();
					TakesScreenshot screen = (TakesScreenshot)driver;
					FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Confirmation page Customer Dertails"+System.currentTimeMillis()/1000+".jpg"));
				}
				
				//Passengers
				/*try {

					ArrayList<Traveler> adults = new ArrayList<Traveler>();
					ArrayList<Traveler> children = new ArrayList<Traveler>();
					ArrayList<Traveler> infants = new ArrayList<Traveler>();
					WebElement passengers = driver.findElement(By.className("table_flight_passenger_details"));
					ArrayList<WebElement> passengersTRlist = new ArrayList<WebElement>(passengers.findElements(By.tagName("tr")));
					
					for(int y=1; y<passengersTRlist.size(); y++)
					{
						//System.out.println(passengersTRlist.size());
						String type = passengersTRlist.get(y).findElement(By.className("Tlightblue")).getText();
						
						if(type.contains("Adult"))
						{
							Traveler adult = new Traveler();
							String title = passengersTRlist.get(y).findElement(By.className("amo_width_5")).getText();
							String gname = passengersTRlist.get(y).findElement(By.className("amo_width_25")).getText();
							String sname = passengersTRlist.get(y).findElement(By.className("amo_width_20")).getText();
							ArrayList<WebElement> list = new ArrayList<WebElement>(passengersTRlist.get(y).findElements(By.className("amo_width_10")));
							String contact = list.get(0).getText();
							String frequentflyer = list.get(1).getText();
							String frequentflyerNo = list.get(2).getText();
							String eticketNo = list.get(3).getText();	
							
							adult.setPassengertypeCode("ADT");
							adult.setNamePrefixTitle(title);
							adult.setGivenName(gname);
							adult.setSurname(sname);
							adult.setPhoneNumber(contact);
							adult.setFrequent_Flyer(frequentflyer);
							adult.setFrequent_Flyer_No(frequentflyerNo);
							adult.setETicket_No(eticketNo);
							
							adults.add(adult);
						}
						else if(type.contains("Child"))
						{
							Traveler child = new Traveler();
							String title = passengersTRlist.get(y).findElement(By.className("amo_width_5")).getText();
							String gname = passengersTRlist.get(y).findElement(By.className("amo_width_25")).getText();
							String sname = passengersTRlist.get(y).findElement(By.className("amo_width_20")).getText();
							ArrayList<WebElement> list = new ArrayList<WebElement>(passengersTRlist.get(y).findElements(By.className("amo_width_10")));
							String contact = list.get(0).getText();
							String frequentflyer = list.get(1).getText();
							String frequentflyerNo = list.get(2).getText();
							String eticketNo = list.get(3).getText();
							
							child.setPassengertypeCode("CHD");
							child.setNamePrefixTitle(title);
							child.setGivenName(gname);
							child.setSurname(sname);
							child.setPhoneNumber(contact);
							child.setFrequent_Flyer(frequentflyer);
							child.setFrequent_Flyer_No(frequentflyerNo);
							child.setETicket_No(eticketNo);
							
							children.add(child);
						}
						else if(type.contains("Infant"))
						{
							Traveler infant = new Traveler();
							String title = passengersTRlist.get(y).findElement(By.className("amo_width_5")).getText();
							String gname = passengersTRlist.get(y).findElement(By.className("amo_width_25")).getText();
							String sname = passengersTRlist.get(y).findElement(By.className("amo_width_20")).getText();
							ArrayList<WebElement> list = new ArrayList<WebElement>(passengersTRlist.get(y).findElements(By.className("amo_width_10")));
							String contact = list.get(0).getText();
							String frequentflyer = list.get(1).getText();
							String frequentflyerNo = list.get(2).getText();
							String eticketNo = list.get(3).getText();	
							
							infant.setPassengertypeCode("INF");
							infant.setNamePrefixTitle(title);
							infant.setGivenName(gname);
							infant.setSurname(sname);
							infant.setPhoneNumber(contact);
							infant.setFrequent_Flyer(frequentflyer);
							infant.setFrequent_Flyer_No(frequentflyerNo);
							infant.setETicket_No(eticketNo);
							
							infants.add(infant);
						}
					}//end of for
					
					uiconfirmationpage.setAdult(adults);
					uiconfirmationpage.setChildren(children);
					uiconfirmationpage.setInfant(infants);
				} catch (Exception e) {
					
				}
				*/
			}
			catch(Exception e)
			{
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Confirmation Page error"+System.currentTimeMillis()/1000+".jpg"));
			}	
		
		}
		catch(Exception e)
		{
			
		}
		
		return available;
	}
	

}
