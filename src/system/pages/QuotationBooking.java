package system.pages;

import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import system.classes.SearchObject;
import system.supportParents.*; 

public class QuotationBooking {
	
	boolean					load		= false;
	HashMap<String, String> Propertymap	= new HashMap<String, String>();
	SupportMethods 			SUP 		= null;
	
	public QuotationBooking(HashMap<String, String> Propymap)
	{
		Propertymap	= Propymap;
		SUP			= new SupportMethods(Propertymap);
	}
	
	public boolean loadQuotation(WebDriver driver, SearchObject searchObject, QuotationPage quotation)
	{
		try
		{
			boolean login = false;
			try {
				login			= SUP.login(driver, Propertymap.get("portal.username"), Propertymap.get("portal.password") );
			} catch (Exception e) {
				//e.printStackTrace();
			}
			if(login)
			{
				driver.get(Propertymap.get("Portal.Url").concat("/operations/reservation/CallCenterWarSearchCriteria.do?module=operations"));
				driver.switchTo().defaultContent();
				driver.switchTo().frame("live_message_frame");
				
				String resNo	= "";
				resNo			= quotation.getReservationNo();
				
				driver.findElement(By.id("quot_ref_radio_WJ_6")).click();
				driver.findElement(By.id("quotationNo_WJ_6")).sendKeys(resNo);
				driver.findElement(By.id("Quotation Ref. Number_lkup")).click();
				
				WebDriverWait			wait = new WebDriverWait(driver, 15);
				//wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[7]/div[2]/div[1]/ul/li[1]")));
				ArrayList<WebElement>	list = new ArrayList<WebElement>(driver.findElements(By.className("top_li")));
				try {
					list.get(0).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(By.className("top_li")));
					driver.findElement(By.className("top_li")).click();
				} catch (Exception e) {
					System.out.println("top_li didnt click");
				}
				
				try {
					driver.findElement(By.className("/html/body/div[7]/div[2]/div[1]/ul/li[1]")).click();
				} catch (Exception e) {
					System.out.println("xpath didnt click");
				}
				
				try {
					ArrayList<WebElement> ele = new ArrayList<WebElement>(driver.findElements(By.className("lookup_data")));
					ele.get(0).findElement(By.className("top_li")).click();
				} catch (Exception e) {
					System.out.println("look up data didnt clicked");
				}
				
				try {
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("loadpayment")));
					((JavascriptExecutor)driver).executeScript("JavaScript:loadPaymentPage();");
					load = true;
				} catch (Exception e) {
					e.printStackTrace();
				}
				//driver.findElement(By.id("search_btns_f")).click();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return load;
	}

}
