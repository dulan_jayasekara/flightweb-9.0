package system.pages;

import java.io.File;
//import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.common.Validators.CommonValidator;

import system.classes.SearchObject;
import system.classes.Traveler;
import system.classes.ReservationInfo;
//import system.classes.UIPaymentPage;
import system.classes.UICreditCardPayInfo;
import system.classes.UIPriceInfo;
import system.classes.XMLPriceItinerary;

public class WebPaymentPage {

	
	//UIPaymentPage 	paymentpage				= new UIPaymentPage();
	
	//private String	basketTopPrice			= "0";
	//private String	basketFromTo			= "";
	//private String	basketPrice				= "0";
	//private String	billingInfoSellingCurr	= "";
	//private String	billingInfoSubtotal		= "0";
	//private String	billingInfoTaxAndOther	= "0";
	//private String	billingInfoTotalValue	= "0";
	//private String	billingInforProcessNow	= "0";
	
	private HashMap<String, String>	propertyMap	= new HashMap<String, String>();
	private String  Currency					= "";
	private String	Subtotal					= "0";
	private String  discount					= "0";
	private String	TotalTax					= "0";
	private String	TotalValue					= "0";
	private String 	discountText				= "";
	
	private String	ProcessNow					= "0";
	private String	ProcessByAirline			= "0";

	private UIPriceInfo			uisummarypay	= null;
	private UICreditCardPayInfo creditcardpay	= null;
	
	private boolean	airRulesDisplayed			= false;
	private boolean discountApplied				= false;
	private boolean offlineElementExist			= false;
	
	private String cancellationDate				= "";
	
	
	public String getCancellationDate() {
		return cancellationDate;
	}

	public void setCancellationDate(String cancellationDate) {
		this.cancellationDate = cancellationDate;
	}

	public String getCurrency() {
		return Currency;
	}

	public void setCurrency(String currency) {
		Currency = currency;
	}

	public String getSubtotal() {
		return Subtotal;
	}

	public void setSubtotal(String subtotal) {
		Subtotal = subtotal;
	}

	public String getTotalTax() {
		return TotalTax;
	}

	public void setTotalTax(String totalTax) {
		TotalTax = totalTax;
	}

	public String getTotalValue() {
		return TotalValue;
	}

	public void setTotalValue(String totalValue) {
		TotalValue = totalValue;
	}

	public String getProcessNow() {
		return ProcessNow;
	}

	public void setProcessNow(String processNow) {
		ProcessNow = processNow;
	}

	public String getProcessByAirline() {
		return ProcessByAirline;
	}

	public void setProcessByAirline(String processByAirline) {
		ProcessByAirline = processByAirline;
	}

	public boolean isOfflineElementExist() {
		return offlineElementExist;
	}

	public void setOfflineElementExist(boolean offlineElementExist) {
		this.offlineElementExist = offlineElementExist;
	}

	public boolean isDiscountApplied() {
		return discountApplied;
	}

	public void setDiscountApplied(boolean discountApplied) {
		this.discountApplied = discountApplied;
	}

	public String getDiscountText() {
		return discountText;
	}

	public void setDiscountText(String discountText) {
		this.discountText = discountText;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}
	
	public boolean isAirRulesDisplayed() {
		return airRulesDisplayed;
	}

	public void setAirRulesDisplayed(boolean airRulesDisplayed) {
		this.airRulesDisplayed = airRulesDisplayed;
	}

	public UICreditCardPayInfo getCreditcardpay() {
		return creditcardpay;
	}
	
	public void setCreditcardpay(UICreditCardPayInfo creditcardpay) {
		this.creditcardpay = creditcardpay;
	}
	
	public UIPriceInfo getUisummarypay() {
		return uisummarypay;
	}
	
	public void setUisummarypay(UIPriceInfo uisummarypay) {
		this.uisummarypay = uisummarypay;
	}

	public WebPaymentPage(HashMap<String, String> Map)
	{
		propertyMap = Map;
	}
	
	public boolean setPaymentPage(WebDriver driver, ReservationInfo fillObj, SearchObject searchObject, XMLPriceItinerary XMLSelectFlight, HashMap<String, String> PropertyMap, Map<String, String> CurrencyMap)
	{
		System.out.println("===================================");
		System.out.println("SET PAYMENT PAGE");
		try {
			Fill_Reservation_Details(driver, fillObj, searchObject, XMLSelectFlight, PropertyMap, CurrencyMap);
		} catch (Exception e) {
			
		}
		
		System.out.println("SET PAYMENT PAGE END");
		System.out.println("===================================");
		return true;
	}
	
	private boolean Fill_Reservation_Details(WebDriver driver, ReservationInfo fillObj, SearchObject searchObject, XMLPriceItinerary XMLSelectFlight, HashMap<String, String> PropertyMap, Map<String, String> CurrencyMap )
	{
		int adults		= fillObj.getAdult().size();
		int children	= fillObj.getChildren().size();
		int infants		= fillObj.getInfant().size();
		try
		{
			try {
				System.out.println("INFO -> Main traveller");
				Traveler main = fillObj.getMaincustomer();
				driver.findElement(By.id(propertyMap.get("PayPg_BookingCusFname_TxtBox_id").trim())).clear();
				driver.findElement(By.id(propertyMap.get("PayPg_BookingCusFname_TxtBox_id").trim())).sendKeys(main.getGivenName());
				driver.findElement(By.id(propertyMap.get("PayPg_BookingCusLname_TxtBox_id").trim())).clear();
				driver.findElement(By.id(propertyMap.get("PayPg_BookingCusLname_TxtBox_id").trim())).sendKeys(main.getSurname());
				driver.findElement(By.id(propertyMap.get("PayPg_BookingCusAddress_TxtBox_id").trim())).clear();
				driver.findElement(By.id(propertyMap.get("PayPg_BookingCusAddress_TxtBox_id").trim())).sendKeys(main.getAddress().getAddressStreetNo());
				driver.findElement(By.id(propertyMap.get("PayPg_BookingCusCity_TxtBox_id").trim())).clear();
				driver.findElement(By.id(propertyMap.get("PayPg_BookingCusCity_TxtBox_id").trim())).sendKeys(main.getAddress().getAddressCity());
				new Select(driver.findElement(By.id(propertyMap.get("PayPg_BookingCusCountry_DropDwn_id").trim()))).selectByVisibleText(main.getAddress().getAddressCountry());
				try {
					WebDriverWait wait = new WebDriverWait(driver, 5000);
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cusState")));
					try {
						if(driver.findElement(By.id("cusState")).isDisplayed())
						{
							new Select(driver.findElement(By.id("cusState"))).selectByVisibleText(main.getAddress().getStateProv());
						}
						
					} catch (Exception e) {
						System.out.println("State is not entered");
					}
					
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cusZip")));
					try {
						if(driver.findElement(By.id("cusZip")).isDisplayed())
						{
							driver.findElement(By.id("cusZip")).sendKeys(main.getAddress().getCountryZip());
						}
					} catch (Exception e) {
						System.out.println("Zip code is not entered");
					}
				} catch (Exception e) {
					System.out.println("State is not entered");
				}
				try {
					driver.findElement(By.id(propertyMap.get("PayPg_BookingCusTelAreaCode_TxtBox_id").trim())).clear();
					((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_BookingCusTelAreaCode_TxtBox_id").trim()+"').val('"+"011"+"');");
					//driver.findElement(By.id(propertyMap.get("PayPg_BookingCusTelAreaCode_TxtBox_id").trim())).sendKeys("011");
					driver.findElement(By.id(propertyMap.get("PayPg_BookingCusTel_TxtBox_id").trim())).clear();
					((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_BookingCusTel_TxtBox_id").trim()+"').val('"+main.getPhoneNumber()+"');");
					//driver.findElement(By.id(propertyMap.get("PayPg_BookingCusTel_TxtBox_id").trim())).sendKeys(main.getPhoneNumber());
					
					driver.findElement(By.id(propertyMap.get("PayPg_BookingCusEmail_TxtBox_id").trim())).clear();
					driver.findElement(By.id(propertyMap.get("PayPg_BookingCusEmail_TxtBox_id").trim())).sendKeys(main.getEmail());
					driver.findElement(By.id(propertyMap.get("PayPg_BookingCusVerifyEmail_TxtBox_id").trim())).clear();
					driver.findElement(By.id(propertyMap.get("PayPg_BookingCusVerifyEmail_TxtBox_id").trim())).sendKeys(main.getEmail());
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				if(!searchObject.isQuotation())
				{
					driver.findElement(By.cssSelector(propertyMap.get("PayPg_ProceedToOccupancy_Btn_css").trim())).click();
				}
				
				System.out.println("INFO -> Main traveller end");
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e.toString());
			}
			
			if(!searchObject.isQuotation())
			{
				try {
					for(int a = 0; a<adults; a++)
					{
						System.out.println("INFO -> adult : "+a+1);
						//new Select(driver.findElement(By.id(propertyMap.get("PayPg_ProceedToOccupancy_Btn_xpath").trim().replace("REPLACE", String.valueOf(a))))).selectByVisibleText(fillObj.getAdult().get(a).getNamePrefixTitle());
						System.out.println(propertyMap.get("PayPg_OccupancyADTFname_TxtBox_id").trim().replace("REPLACE", String.valueOf(a)));
						driver.findElement(By.id(propertyMap.get("PayPg_OccupancyADTFname_TxtBox_id").trim().replace("REPLACE", String.valueOf(a)))).clear();
						driver.findElement(By.id(propertyMap.get("PayPg_OccupancyADTFname_TxtBox_id").trim().replace("REPLACE", String.valueOf(a)))).sendKeys(fillObj.getAdult().get(a).getGivenName());
						driver.findElement(By.id(propertyMap.get("PayPg_OccupancyADTLname_TxtBox_id").trim().replace("REPLACE", String.valueOf(a)))).clear();
						driver.findElement(By.id(propertyMap.get("PayPg_OccupancyADTLname_TxtBox_id").trim().replace("REPLACE", String.valueOf(a)))).sendKeys(fillObj.getAdult().get(a).getSurname());
						driver.findElement(By.id(propertyMap.get("PayPg_OccupancyADTContact_TxtBox_id").trim().replace("REPLACE", String.valueOf(a)))).clear();
						driver.findElement(By.id(propertyMap.get("PayPg_OccupancyADTContact_TxtBox_id").trim().replace("REPLACE", String.valueOf(a)))).sendKeys(fillObj.getAdult().get(a).getPhoneNumber());
						driver.findElement(By.id(propertyMap.get("PayPg_OccupancyADTPassportNo_TxtBox_id").trim().replace("REPLACE", String.valueOf(a)))).clear();
						driver.findElement(By.id(propertyMap.get("PayPg_OccupancyADTPassportNo_TxtBox_id").trim().replace("REPLACE", String.valueOf(a)))).sendKeys(fillObj.getAdult().get(a).getPassportNo());
	
						((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_OccupancyADTPassportExp_TxtBox_id").trim().replace("REPLACE", String.valueOf(a))+"').val('"+fillObj.getAdult().get(a).getPassportExpDate().trim()+"');");
						
						((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_OccupancyADTPassportIssue_TxtBox_id").trim().replace("REPLACE", String.valueOf(a))+"').val('"+fillObj.getAdult().get(a).getPassprtIssuDate().trim()+"');");
						
						((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_OccupancyADTBdate_TxtBox_id").trim().replace("REPLACE", String.valueOf(a))+"').val('"+fillObj.getAdult().get(a).getBirthDay().trim()+"');");
						
						new Select(driver.findElement(By.className(propertyMap.get("PayPg_OccupancyADTGender_DropDwn_class").trim()))).selectByVisibleText(fillObj.getAdult().get(a).getGender());
						new Select(driver.findElement(By.id(propertyMap.get("PayPg_OccupancyADTNationality_DropDwn_id").trim().replace("REPLACE", String.valueOf(a))))).selectByVisibleText(fillObj.getAdult().get(a).getNationality());
						new Select(driver.findElement(By.id(propertyMap.get("PayPg_OccupancyADTPassIssuCounrty_DropDwn_id").trim().replace("REPLACE", String.valueOf(a))))).selectByVisibleText(fillObj.getAdult().get(a).getPassportIssuCountry());
					}
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println(e.toString());
				}
			
			
				try {
					for(int c=0; c<children; c++)
					{
						System.out.println("INFO -> child : "+c+1);
						//new Select(driver.findElement(By.name("childTitle_Air["+c+"]"))).selectByVisibleText(fillObj.getChildren().get(c).getNamePrefixTitle());
						driver.findElement(By.id(propertyMap.get("PayPg_OccupancyCHDFname_TxtBox_id").trim().replace("REPLACE", String.valueOf(c)))).clear();
						driver.findElement(By.id(propertyMap.get("PayPg_OccupancyCHDFname_TxtBox_id").trim().replace("REPLACE", String.valueOf(c)))).sendKeys(fillObj.getChildren().get(c).getGivenName());
						driver.findElement(By.id(propertyMap.get("PayPg_OccupancyCHDLname_TxtBox_id").trim().replace("REPLACE", String.valueOf(c)))).clear();
						driver.findElement(By.id(propertyMap.get("PayPg_OccupancyCHDLname_TxtBox_id").trim().replace("REPLACE", String.valueOf(c)))).sendKeys(fillObj.getChildren().get(c).getSurname());
						driver.findElement(By.id(propertyMap.get("PayPg_OccupancyCHDContact_TxtBox_id").trim().replace("REPLACE", String.valueOf(c)))).clear();
						driver.findElement(By.id(propertyMap.get("PayPg_OccupancyCHDContact_TxtBox_id").trim().replace("REPLACE", String.valueOf(c)))).sendKeys(fillObj.getChildren().get(c).getPhoneNumber());
						driver.findElement(By.id(propertyMap.get("PayPg_OccupancyCHDPassportNo_TxtBox_id").trim().replace("REPLACE", String.valueOf(c)))).clear();
						driver.findElement(By.id(propertyMap.get("PayPg_OccupancyCHDPassportNo_TxtBox_id").trim().replace("REPLACE", String.valueOf(c)))).sendKeys(fillObj.getChildren().get(c).getPassportNo());
						
						((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_OccupancyCHDPassportExp_TxtBox_id").trim().replace("REPLACE", String.valueOf(c))+"').val('"+fillObj.getChildren().get(c).getPassportExpDate().trim()+"');");
						new Select(driver.findElement(By.id(propertyMap.get("PayPg_OccupancyCHDPassIssuCounrty_DropDwn_id").trim().replace("REPLACE", String.valueOf(c))))).selectByVisibleText(fillObj.getChildren().get(c).getPassportIssuCountry());
						((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_OccupancyCHDPassportIssue_TxtBox_id").trim().replace("REPLACE", String.valueOf(c))+"').val('"+fillObj.getChildren().get(c).getPassprtIssuDate().trim()+"');");
						((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_OccupancyCHDBdate_TxtBox_id").trim().replace("REPLACE", String.valueOf(c))+"').val('"+fillObj.getChildren().get(c).getBirthDay().trim()+"');");
						new Select(driver.findElement(By.className(propertyMap.get("PayPg_OccupancyCHDGender_DropDwn_class").trim().replace("REPLACE", String.valueOf(c))))).selectByVisibleText(fillObj.getChildren().get(c).getGender());
						new Select(driver.findElement(By.id(propertyMap.get("PayPg_OccupancyCHDNationality_DropDwn_id").trim().replace("REPLACE", String.valueOf(c))))).selectByVisibleText(fillObj.getChildren().get(c).getNationality());
						//new Select(driver.findElement(By.name(propertyMap.get("PayPg_OccupancyADTPassIssuCounrty_DropDwn_id").trim()))).selectByVisibleText(fillObj.getChildren().get(c).getPassportIssuCountry());
						
						//((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_OccupancyCHDBdate_TxtBox_id").trim().replace("REPLACE", String.valueOf(c))+"').val('"+fillObj.getChildren().get(c).getBirthDay().trim()+"');");
						
					}
					
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println(e.toString());
				}
			
				try {
					for(int i = 0; i<infants; i++)
					{
						System.out.println("INFO -> infant : "+i+1);
						//new Select(driver.findElement(By.name("infantTitle_Air["+i+"]"))).selectByVisibleText(fillObj.getInfant().get(i).getNamePrefixTitle());
						driver.findElement(By.id(propertyMap.get("PayPg_OccupancyINFFname_TxtBox_id").trim().replace("REPLACE", String.valueOf(i)))).clear();
						driver.findElement(By.id(propertyMap.get("PayPg_OccupancyINFFname_TxtBox_id").trim().replace("REPLACE", String.valueOf(i)))).sendKeys(fillObj.getInfant().get(i).getGivenName());
						driver.findElement(By.id(propertyMap.get("PayPg_OccupancyINFLname_TxtBox_id").trim().replace("REPLACE", String.valueOf(i)))).clear();
						driver.findElement(By.id(propertyMap.get("PayPg_OccupancyINFLname_TxtBox_id").trim().replace("REPLACE", String.valueOf(i)))).sendKeys(fillObj.getInfant().get(i).getSurname());
						//driver.findElement(By.id(propertyMap.get("PayPg_OccupancyINFLname_TxtBox_id").trim().replace("REPLACE", String.valueOf(i)))).sendKeys(fillObj.getInfant().get(i).getPassportNo());                                                
	
						((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_OccupancyINFBdate_TxtBox_id").trim().replace("REPLACE", String.valueOf(i))+"').val('"+fillObj.getInfant().get(i).getBirthDay().trim()+"');");
					
						new Select(driver.findElement(By.className(propertyMap.get("PayPg_OccupancyINFGender_DropDwn_class").trim()))).selectByVisibleText(fillObj.getInfant().get(i).getGender());
						new Select(driver.findElement(By.id(propertyMap.get("PayPg_OccupancyINFNationality_DropDwn_id").trim().replace("REPLACE", String.valueOf(i))))).selectByVisibleText(fillObj.getInfant().get(i).getNationality());
						((JavascriptExecutor)driver).executeScript("$('#"+propertyMap.get("PayPg_OccupancyINFBdate_TxtBox_id").trim().replace("REPLACE", String.valueOf(i))+"').val('"+fillObj.getInfant().get(i).getBirthDay().trim()+"');");
						
					}
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println(e.toString());
				}
				
				try {
					Thread.sleep(3000);
					System.out.println("INFO -> Proceed to billing");
					driver.findElement(By.className(propertyMap.get("PayPg_ProceedToBilling_Btn_class").trim())).click();
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println(e.toString());
				}
				
				try {
					if(searchObject.getTOBooking().equalsIgnoreCase("YES") && searchObject.getPaymentMode().equalsIgnoreCase("Pay Offline"))
					{
						System.out.println("INFO -> OFFLINE PAY");
						driver.findElement(By.id("pay_offline")).click();
					}
				} catch (Exception e) {
					
				}
				
				try {
					if(searchObject.getTOBooking().equalsIgnoreCase("YES"))
					{
						System.out.println("INFO -> TO OFFLINE PAY");
						offlineElementExist = CommonValidator.elementVisible(driver, "pay_offline");
					}
				} catch (Exception e) {
					
				}
				
				try {
					if(!searchObject.isApplyDiscount() && !searchObject.isQuotation() && searchObject.isApplyDiscountAtPayPg()
					   && !searchObject.getTOBooking().equalsIgnoreCase("YES") && !searchObject.getPaymentMode().equalsIgnoreCase("Pay Offline")	)
					{
						System.out.println("INFO -> ENTER DISCOUNT COUPON NO");
						driver.findElement(By.id("couponno")).sendKeys(propertyMap.get("DiscountCouponNo"));
						((JavascriptExecutor)driver).executeScript("javascript:validateCoupon();");
						//wait ??????
						if(driver.findElement(By.id(propertyMap.get("PayPg_Discount_PassMessageBox_id"))).isDisplayed())
						{
							System.out.println("INFO -> DISCOUNT APPLIED");
							this.setDiscountText(driver.findElement(By.id(propertyMap.get("PayPg_Discount_PassMessageBox_id"))).getText().trim());
							try {
								try {
									driver.findElement(By.className("ui-button-text")).click();
								} catch (Exception e) {
									
								}
								
								try {
									if( this.getDiscountText().trim().contains(propertyMap.get("PayPg_Discount_PassMessageText").trim()) || propertyMap.get("PayPg_Discount_PassMessageText").trim().contains(this.getDiscountText().trim()))
									{
										System.out.println("INFO -> DISCOUNT APPLIED");
										this.setDiscountApplied(true);
										XMLSelectFlight.getPricinginfo().setDiscount(PropertyMap.get("DiscountType"), PropertyMap.get("DiscountValue"), PropertyMap.get("Portal_Currency_Code"), CurrencyMap);
										
										
										try {
											System.out.println("INFO -> PROCEED TO OCCUPANCY");
											driver.findElement(By.cssSelector(propertyMap.get("PayPg_ProceedToOccupancy_Btn_css").trim())).click();
										} catch (Exception e) {
											e.printStackTrace();
										}
										try {
											System.out.println("INFO -> PROCEED TO BILLING");
											driver.findElement(By.className(propertyMap.get("PayPg_ProceedToBilling_Btn_class").trim())).click();
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
								} catch (Exception e) {
									System.out.println(e.toString());
									System.out.println("INFO -> DISCOUNT APPLICATION FAILED");
								}
								
							} catch (Exception e) {
								System.out.println(e.toString());
								System.out.println("INFO -> DISCOUNT COUPON MESSAGE BOX NOT CLOSED");
							}
						}
					}
				} catch (Exception e) {
					System.out.println(e.toString());
					System.out.println("INFO -> DISCOUNT APPLICATION FAILED");
				}
				
				
				//CREDIT CARD DETAILS
				creditcardpay	 = new UICreditCardPayInfo();
				ArrayList<WebElement> creditNote = new ArrayList<WebElement>(driver.findElements(By.className(propertyMap.get("PayPg_CreditCardAmountNote_Lbl_class").trim())));
				try {
					if(/*searchObject.getTOBooking().equalsIgnoreCase("YES") && !*/searchObject.getPaymentMode().equalsIgnoreCase("Pay Online"))
					{
						try {
							System.out.println("CC DETAILS READ");
							String totalPackBookVal = "0";
							totalPackBookVal = creditNote.get(0).findElement(By.id(propertyMap.get("PayPg_CreditCardMountNoteTotal_Lbl_id").trim())).getText().trim();
							creditcardpay.setTotalpackage_bookVal(totalPackBookVal);
							
							String totalPackBookCurrCode = "";
							totalPackBookCurrCode = creditNote.get(0).getText().trim();
							creditcardpay.setTotalpackage_bookVal_CurrCode(totalPackBookCurrCode);
							
							String Subtotal			= "0";
							Subtotal				= driver.findElement(By.id(propertyMap.get("PayPg_CreditCardSubtotal_Lbl_id").trim())).getText().trim();
							creditcardpay.setSubtotal(Subtotal);
							
							String discount			= "0";
							if(searchObject.isApplyDiscount() || searchObject.isApplyDiscountAtPayPg())
							{
								discount				= driver.findElement(By.id("paymentdetails_discount")).getText().trim().replace("-", "").trim();
							}
							creditcardpay.setDiscount(discount);
							
							String TotalTax			= "0";
							TotalTax				= driver.findElement(By.id(propertyMap.get("PayPg_CreditCardTotalTax_Lbl_id").trim())).getText().trim();
							creditcardpay.setTaxandfees(TotalTax);
							
							String TotalValue		= "0";
							TotalValue				= driver.findElement(By.id(propertyMap.get("PayPg_CreditCardTotalValue_Lbl_id").trim())).getText().trim();
							creditcardpay.setTotal(TotalValue);
							
							String ProcessNow		= "0";
							ProcessNow				= driver.findElement(By.id(propertyMap.get("PayPg_CreditCardProcessNow_Lbl_id").trim())).getText().trim();
							creditcardpay.setAmountprocess(ProcessNow);
							
							String ProcessByAirline = "0";
							ProcessByAirline		= driver.findElement(By.id(propertyMap.get("PayPg_CreditCardProcessByAirline_Lbl_id").trim())).getText().trim();
							creditcardpay.setAmountprocessAirline(ProcessByAirline);
							
							System.out.println("CC DETAILS READ END");
							
						} catch (Exception e) {
							System.out.println(e.toString());
						}
					}
				} catch (Exception e) {
					System.out.println(e.toString());
				}
				
				
				try {
					Thread.sleep(5000);
					WebDriverWait wait = new WebDriverWait(driver, 7000);
					wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(propertyMap.get("PayPg_ProceedToMakeSpecReq_Btn_css").trim()))); 
					wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(propertyMap.get("PayPg_ProceedToMakeSpecReq_Btn_css").trim())));
					driver.findElement(By.cssSelector(propertyMap.get("PayPg_ProceedToMakeSpecReq_Btn_css").trim())).click();
					System.out.println("INFO -> proceed to special request");
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				try {
					driver.findElement(By.cssSelector(propertyMap.get("PayPg_ProceedToTermsANDCond_Btn_css").trim())).click();
					this.cancellationDate = driver.findElement(By.className("last-cancellation-date")).getText().trim();
					this.cancellationDate = this.cancellationDate.split(":")[1].trim();
					this.cancellationDate = CommonValidator.formatDateToCommon(cancellationDate, "dd-MMM-yyyy");
					System.out.println("INFO -> Cancellation date read");
				} catch (Exception e) {
					System.out.println(e.toString());
					e.printStackTrace();
				}
				
				try {
					Thread.sleep(2000);
					driver.findElement(By.id(propertyMap.get("PayPg_AgreeCancelPolicy_ChkBox_id").trim())).click();	
					System.out.println("INFO -> Agree cancellation policy");
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				try {
					Thread.sleep(2000);
					driver.findElement(By.id(propertyMap.get("PayPg_AgreeTermsANDCon_ChkBox_id").trim())).click();
					System.out.println("INFO -> Agree terms and conditions");
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
			this.uisummarypay = new UIPriceInfo();
			try {
				System.out.println("SUMMERY PAY DETAILS READ");
				Currency					= driver.findElement(By.id(propertyMap.get("PayPg_BillingInfoSellingCurr_Lbl_id").trim())).getText().trim();
				uisummarypay.setCurrencycode(Currency);
				Subtotal					= driver.findElement(By.id(propertyMap.get("PayPg_BillingInfoSubtotal_Lbl_id").trim())).getText().trim();
				uisummarypay.setSubtotal(Subtotal);
				TotalTax					= driver.findElement(By.id(propertyMap.get("PayPg_BillingInfoTaxANDOther_Lbl_id").trim())).getText().trim();
				uisummarypay.setTaxplusother(TotalTax);
				TotalValue					= driver.findElement(By.id(propertyMap.get("PayPg_BillingInfoTotalVal_Lbl_id").trim())).getText().trim();
				uisummarypay.setTotalCost(TotalValue);
				ProcessNow					= driver.findElement(By.id(propertyMap.get("PayPg_BillingInfoProcessNw_Lbl_id").trim())).getText().trim();
				uisummarypay.setAmountprocess(ProcessNow);
				ProcessByAirline			= driver.findElement(By.id(propertyMap.get("PayPg_BillingInfoProcessNw_Lbl_id").trim())).getText().trim();
				uisummarypay.setAmountprocessAirline(ProcessByAirline);
				System.out.println("SUMMERY PAY DETAILS READ END");
				
				try {
					
					if(searchObject.isApplyDiscount() || searchObject.isApplyDiscountAtPayPg())
					{
						WebElement summeryPay = driver.findElement(By.className("price-list"));
						ArrayList<WebElement> liList = new ArrayList<WebElement>(summeryPay.findElements(By.tagName("li")));
						ArrayList<WebElement> divList = new ArrayList<WebElement>(liList.get(1).findElements(By.tagName("div")));
						String discountval = "0";
						discountval = divList.get(3).getText();
						System.out.println(discountval);
						if(discountval.contains("-"))
						{
							discountval = discountval.trim().replace("-", "").trim();
						}
						discount = discountval.trim();
						uisummarypay.setDiscount(discount);
						System.out.println("INFO -> DISCOUNT SET TO OBJECT");
					}
					
				} catch (Exception e) {
					System.out.println(e.toString());
					e.printStackTrace();
				}
				
			} catch (Exception e) {
				System.out.println(e.toString());
			}
			
			if(searchObject.isQuotation())
			{
				((JavascriptExecutor)driver).executeScript(propertyMap.get("PayPg_Quotation_Btn_javascript"));
				System.out.println("INFO -> QUOTATION SAVE BUTTON CLICKED");
			}
			
			try {
				driver.findElement(By.id(propertyMap.get("PayPg_ConfirmReservation_Btn_id").trim())).click();
				System.out.println("INFO -> CONFIRMATION RESERVATION BUTTON CLICKED");
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e.toString());
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println("INFO -> PAYMENT PAGE ERROR");
			System.out.println(e.toString());
			TakesScreenshot screen = (TakesScreenshot)driver;
			try {
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Filling Reservation Details"+System.currentTimeMillis()/1000+".jpg"));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}	
	
		return true;
	}

	
}
