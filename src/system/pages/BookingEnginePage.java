package system.pages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import system.classes.SearchObject;

import com.utilities.Screenshot;
import com.utilities.ScreenshotPathSingleton;

public class BookingEnginePage {

	private HashMap<String, String> elements = new HashMap<String, String>();
	boolean countryResidenceLabel			= false;
	String	countryResidenceLabelText		= "";
	boolean isFlightDefaultSelected			= false;
	boolean tripTypeMainLabel				= false;
	String	tripTypeMainLabelTxt			= "";
	boolean tripTypeRoundTripLabel			= false;
	String	tripTypeRoundTripLabelTxt		= "";
	boolean tripTypeRoundTripRadButton		= false;
	boolean tripTypeOneWayLabel				= false;
	boolean tripTypeOneWayRadButton			= false;
	String	tripTypeOneWayLabelTxt			= "";
	boolean tripTypeMultiLabel				= false;
	boolean tripTypeMultiRadButton			= false;
	String	tripTypeMultiLabelTxt			= "";
	boolean isSelectedRoundTripDefault		= false;
	boolean whereGoingLabel					= false;
	String	whereGoingLabelTxt				= "";
	boolean fromLabel						= false;
	String	fromLabelTxt					= "";
	boolean fromTxtBox						= false;
	boolean toLabel							= false;
	String	toLabelTxt						= "";
	boolean toTxtBox						= false;
	boolean departureDateLabel				= false;
	String	departureDateLabelTxt			= "";
	boolean departureDatePicker				= false;
	boolean departureTimeLabel				= false;
	String	departureTimeLabelTxt			= "";
	boolean departureTimeDrpDwn				= false;
	int		departureTimeDrpDwnSize			= 0;
	boolean returnDateLabel					= false;
	String	returnDateLabelTxt				= "";
	boolean returnDatePicker				= false;
	boolean returnTimeLabel					= false;
	String	returnTimeLabelTxt				= "";
	boolean returnTimeDrpDwn				= false;
	int		returnTimeDrpDwnSize			= 0;
	boolean howManyPeopleLabel				= false;
	String	howManyPeopleLabelTxt			= "";
	boolean adultsLabel						= false;
	String	adultsLabelTxt					= "";
	boolean childrenLabel					= false;
	String	childrenLabelTxt				= "";
	boolean infantLabel						= false;
	String	infantLabelTxt					= "";
	int		adultDropdownSize				= 0;
	int		childrenDropdownSize			= 0;
	int		infantDropdownSize				= 0;
	int		childAgeDrodownCount			= 0;
	int		childAgeDropdowns				= 0;
	boolean prefferedAirLineLabel			= false;
	String	prefferedAirLineLabelTxt		= "";
	boolean prefferedAirLineTxtBox			= false;
	boolean classLabel						= false;
	String	classLabelTxt					= "";
	boolean classDrpDwn						= false;
	int		classDrpDwnSize					= 0;
	boolean preferredCurrencyLabel			= false;
	String	preferredCurrencyLabelTxt		= "";
	boolean preferredCurrencyDrpDwn			= false;
	boolean promotionCodeLabel				= false;
	String	promotionCodeLabelTxt			= "";
	boolean promotionCodeTxtBox				= false;
	boolean preferNonStopFlightLabel		= false;
	String	preferNonStopFlightLabelTxt		= "";
	boolean preferNonStopFlightChkBox		= false;
	boolean additionalOptionTxt				= false;
	String	additionalOptionTxtBeforeClick	= "";
	String	additionalOptionTxtAfterClick	= "";
	boolean searchButton					= false;
	
	public BookingEnginePage()
	{
		
	}
	
	
	public boolean isClassDrpDwn() {
		return classDrpDwn;
	}
	public void setClassDrpDwn(boolean classDrpDwn) {
		this.classDrpDwn = classDrpDwn;
	}
	public int getClassDrpDwnSize() {
		return classDrpDwnSize;
	}
	public void setClassDrpDwnSize(int classDrpDwnSize) {
		this.classDrpDwnSize = classDrpDwnSize;
	}
	public boolean isCountryResidenceLabel() {
		return countryResidenceLabel;
	}
	public void setCountryResidenceLabel(boolean countryResidenceLabel) {
		this.countryResidenceLabel = countryResidenceLabel;
	}
	public String getCountryResidenceLabelText() {
		return countryResidenceLabelText;
	}
	public void setCountryResidenceLabelText(String countryResidenceLabelText) {
		this.countryResidenceLabelText = countryResidenceLabelText;
	}
	public int getReturnTimeDrpDwnSize() {
		return returnTimeDrpDwnSize;
	}
	public String getTripTypeMainLabelTxt() {
		return tripTypeMainLabelTxt;
	}
	public void setTripTypeMainLabelTxt(String tripTypeMainLabelTxt) {
		this.tripTypeMainLabelTxt = tripTypeMainLabelTxt;
	}
	public String getTripTypeRoundTripLabelTxt() {
		return tripTypeRoundTripLabelTxt;
	}
	public void setTripTypeRoundTripLabelTxt(String tripTypeRoundTripLabelTxt) {
		this.tripTypeRoundTripLabelTxt = tripTypeRoundTripLabelTxt;
	}
	public String getTripTypeOneWayLabelTxt() {
		return tripTypeOneWayLabelTxt;
	}
	public void setTripTypeOneWayLabelTxt(String tripTypeOneWayLabelTxt) {
		this.tripTypeOneWayLabelTxt = tripTypeOneWayLabelTxt;
	}
	public String getTripTypeMultiLabelTxt() {
		return tripTypeMultiLabelTxt;
	}
	public void setTripTypeMultiLabelTxt(String tripTypeMultiLabelTxt) {
		this.tripTypeMultiLabelTxt = tripTypeMultiLabelTxt;
	}
	public String getWhereGoingLabelTxt() {
		return whereGoingLabelTxt;
	}
	public void setWhereGoingLabelTxt(String whereGoingLabelTxt) {
		this.whereGoingLabelTxt = whereGoingLabelTxt;
	}
	public String getFromLabelTxt() {
		return fromLabelTxt;
	}
	public void setFromLabelTxt(String fromLabelTxt) {
		this.fromLabelTxt = fromLabelTxt;
	}
	public String getToLabelTxt() {
		return toLabelTxt;
	}
	public void setToLabelTxt(String toLabelTxt) {
		this.toLabelTxt = toLabelTxt;
	}
	public String getDepartureDateLabelTxt() {
		return departureDateLabelTxt;
	}
	public void setDepartureDateLabelTxt(String departureDateLabelTxt) {
		this.departureDateLabelTxt = departureDateLabelTxt;
	}
	public String getDepartureTimeLabelTxt() {
		return departureTimeLabelTxt;
	}
	public void setDepartureTimeLabelTxt(String departureTimeLabelTxt) {
		this.departureTimeLabelTxt = departureTimeLabelTxt;
	}
	public String getReturnDateLabelTxt() {
		return returnDateLabelTxt;
	}
	public void setReturnDateLabelTxt(String returnDateLabelTxt) {
		this.returnDateLabelTxt = returnDateLabelTxt;
	}
	public String getReturnTimeLabelTxt() {
		return returnTimeLabelTxt;
	}
	public void setReturnTimeLabelTxt(String returnTimeLabelTxt) {
		this.returnTimeLabelTxt = returnTimeLabelTxt;
	}
	public String getHowManyPeopleLabelTxt() {
		return howManyPeopleLabelTxt;
	}
	public void setHowManyPeopleLabelTxt(String howManyPeopleLabelTxt) {
		this.howManyPeopleLabelTxt = howManyPeopleLabelTxt;
	}
	public String getAdultsLabelTxt() {
		return adultsLabelTxt;
	}
	public void setAdultsLabelTxt(String adultsLabelTxt) {
		this.adultsLabelTxt = adultsLabelTxt;
	}
	public String getChildrenLabelTxt() {
		return childrenLabelTxt;
	}
	public void setChildrenLabelTxt(String childrenLabelTxt) {
		this.childrenLabelTxt = childrenLabelTxt;
	}
	public String getInfantLabelTxt() {
		return infantLabelTxt;
	}
	public void setInfantLabelTxt(String infantLabelTxt) {
		this.infantLabelTxt = infantLabelTxt;
	}
	public String getPrefferedAirLineLabelTxt() {
		return prefferedAirLineLabelTxt;
	}
	public void setPrefferedAirLineLabelTxt(String prefferedAirLineLabelTxt) {
		this.prefferedAirLineLabelTxt = prefferedAirLineLabelTxt;
	}
	public String getClassLabelTxt() {
		return classLabelTxt;
	}
	public void setClassLabelTxt(String classLabelTxt) {
		this.classLabelTxt = classLabelTxt;
	}
	public String getPreferredCurrencyLabelTxt() {
		return preferredCurrencyLabelTxt;
	}
	public void setPreferredCurrencyLabelTxt(String preferredCurrencyLabelTxt) {
		this.preferredCurrencyLabelTxt = preferredCurrencyLabelTxt;
	}
	public String getPromotionCodeLabelTxt() {
		return promotionCodeLabelTxt;
	}
	public void setPromotionCodeLabelTxt(String promotionCodeLabelTxt) {
		this.promotionCodeLabelTxt = promotionCodeLabelTxt;
	}
	public String getPreferNonStopFlightLabelTxt() {
		return preferNonStopFlightLabelTxt;
	}
	public void setPreferNonStopFlightLabelTxt(String preferNonStopFlightLabelTxt) {
		this.preferNonStopFlightLabelTxt = preferNonStopFlightLabelTxt;
	}
	public boolean isPreferNonStopFlightChkBox() {
		return preferNonStopFlightChkBox;
	}
	public void setPreferNonStopFlightChkBox(boolean preferNonStopFlightChkBox) {
		this.preferNonStopFlightChkBox = preferNonStopFlightChkBox;
	}
	public boolean isFlightDefaultSelected() {
		return isFlightDefaultSelected;
	}
	public void setFlightDefaultSelected(boolean isFlightDefaultSelected) {
		this.isFlightDefaultSelected = isFlightDefaultSelected;
	}
	public boolean isTripTypeMainLabel() {
		return tripTypeMainLabel;
	}
	public void setTripTypeMainLabel(boolean tripTypeMainLabel) {
		this.tripTypeMainLabel = tripTypeMainLabel;
	}
	public boolean isTripTypeRoundTripLabel() {
		return tripTypeRoundTripLabel;
	}
	public void setTripTypeRoundTripLabel(boolean tripTypeRoundTripLabel) {
		this.tripTypeRoundTripLabel = tripTypeRoundTripLabel;
	}
	public boolean isTripTypeRoundTripRadButton() {
		return tripTypeRoundTripRadButton;
	}
	public void setTripTypeRoundTripRadButton(boolean tripTypeRoundTripRadButton) {
		this.tripTypeRoundTripRadButton = tripTypeRoundTripRadButton;
	}
	public boolean isTripTypeOneWayLabel() {
		return tripTypeOneWayLabel;
	}
	public void setTripTypeOneWayLabel(boolean tripTypeOneWayLabel) {
		this.tripTypeOneWayLabel = tripTypeOneWayLabel;
	}
	public boolean isTripTypeOneWayRadButton() {
		return tripTypeOneWayRadButton;
	}
	public void setTripTypeOneWayRadButton(boolean tripTypeOneWayRadButton) {
		this.tripTypeOneWayRadButton = tripTypeOneWayRadButton;
	}
	public boolean isTripTypeMultiLabel() {
		return tripTypeMultiLabel;
	}
	public void setTripTypeMultiLabel(boolean tripTypeMultiLabel) {
		this.tripTypeMultiLabel = tripTypeMultiLabel;
	}
	public boolean isTripTypeMultiRadButton() {
		return tripTypeMultiRadButton;
	}
	public void setTripTypeMultiRadButton(boolean tripTypeMultiRadButton) {
		this.tripTypeMultiRadButton = tripTypeMultiRadButton;
	}
	public boolean isSelectedRoundTripDefault() {
		return isSelectedRoundTripDefault;
	}
	public void setSelectedRoundTripDefault(boolean isSelectedRoundTripDefault) {
		this.isSelectedRoundTripDefault = isSelectedRoundTripDefault;
	}
	public boolean isWhereGoingLabel() {
		return whereGoingLabel;
	}
	public void setWhereGoingLabel(boolean whereGoingLabel) {
		this.whereGoingLabel = whereGoingLabel;
	}
	public boolean isFromLabel() {
		return fromLabel;
	}
	public void setFromLabel(boolean fromLabel) {
		this.fromLabel = fromLabel;
	}
	public boolean isFromTxtBox() {
		return fromTxtBox;
	}
	public void setFromTxtBox(boolean fromTxtBox) {
		this.fromTxtBox = fromTxtBox;
	}
	public boolean isToLabel() {
		return toLabel;
	}
	public void setToLabel(boolean toLabel) {
		this.toLabel = toLabel;
	}
	public boolean isToTxtBox() {
		return toTxtBox;
	}
	public void setToTxtBox(boolean toTxtBox) {
		this.toTxtBox = toTxtBox;
	}
	public boolean isDepartureDateLabel() {
		return departureDateLabel;
	}
	public void setDepartureDateLabel(boolean departureDateLabel) {
		this.departureDateLabel = departureDateLabel;
	}
	public boolean isDepartureDatePicker() {
		return departureDatePicker;
	}
	public void setDepartureDatePicker(boolean departureDatePicker) {
		this.departureDatePicker = departureDatePicker;
	}
	public boolean isDepartureTimeLabel() {
		return departureTimeLabel;
	}
	public void setDepartureTimeLabel(boolean departureTimeLabel) {
		this.departureTimeLabel = departureTimeLabel;
	}
	public boolean isDepartureTimeDrpDwn() {
		return departureTimeDrpDwn;
	}
	public void setDepartureTimeDrpDwn(boolean departureTimeDrpDwn) {
		this.departureTimeDrpDwn = departureTimeDrpDwn;
	}
	public int getDepartureTimeDrpDwnSize() {
		return departureTimeDrpDwnSize;
	}
	public void setDepartureTimeDrpDwnSize(int departureTimeDrpDwnSize) {
		this.departureTimeDrpDwnSize = departureTimeDrpDwnSize;
	}
	public boolean isReturnDateLabel() {
		return returnDateLabel;
	}
	public void setReturnDateLabel(boolean returnDateLabel) {
		this.returnDateLabel = returnDateLabel;
	}
	public boolean isReturnDatePicker() {
		return returnDatePicker;
	}
	public void setReturnDatePicker(boolean returnDatePicker) {
		this.returnDatePicker = returnDatePicker;
	}
	public boolean isReturnTimeLabel() {
		return returnTimeLabel;
	}
	public void setReturnTimeLabel(boolean returnTimeLabel) {
		this.returnTimeLabel = returnTimeLabel;
	}
	public boolean isReturnTimeDrpDwn() {
		return returnTimeDrpDwn;
	}
	public void setReturnTimeDrpDwn(boolean returnTimeDrpDwn) {
		this.returnTimeDrpDwn = returnTimeDrpDwn;
	}
	public int isReturnTimeDrpDwnSize() {
		return returnTimeDrpDwnSize;
	}
	public void setReturnTimeDrpDwnSize(int returnTimeDrpDwnSize) {
		this.returnTimeDrpDwnSize = returnTimeDrpDwnSize;
	}
	public boolean isHowManyPeopleLabel() {
		return howManyPeopleLabel;
	}
	public void setHowManyPeopleLabel(boolean howManyPeopleLabel) {
		this.howManyPeopleLabel = howManyPeopleLabel;
	}
	public boolean isAdultsLabel() {
		return adultsLabel;
	}
	public void setAdultsLabel(boolean adultsLabel) {
		this.adultsLabel = adultsLabel;
	}
	public boolean isChildrenLabel() {
		return childrenLabel;
	}
	public void setChildrenLabel(boolean childrenLabel) {
		this.childrenLabel = childrenLabel;
	}
	public boolean isInfantLabel() {
		return infantLabel;
	}
	public void setInfantLabel(boolean infantLabel) {
		this.infantLabel = infantLabel;
	}
	public int getAdultDropdownSize() {
		return adultDropdownSize;
	}
	public void setAdultDropdownSize(int adultDropdownSize) {
		this.adultDropdownSize = adultDropdownSize;
	}
	public int getChildrenDropdownSize() {
		return childrenDropdownSize;
	}
	public void setChildrenDropdownSize(int childrenDropdownSize) {
		this.childrenDropdownSize = childrenDropdownSize;
	}
	public int getInfantDropdownSize() {
		return infantDropdownSize;
	}
	public void setInfantDropdownSize(int infantDropdownSize) {
		this.infantDropdownSize = infantDropdownSize;
	}
	public int getChildAgeDrodownCount() {
		return childAgeDrodownCount;
	}
	public void setChildAgeDrodownCount(int childAgeDrodownCount) {
		this.childAgeDrodownCount = childAgeDrodownCount;
	}
	public int getChildAgeDropdowns() {
		return childAgeDropdowns;
	}
	public void setChildAgeDropdowns(int childAgeDropdowns) {
		this.childAgeDropdowns = childAgeDropdowns;
	}
	public boolean isPrefferedAirLineLabel() {
		return prefferedAirLineLabel;
	}
	public void setPrefferedAirLineLabel(boolean prefferedAirLineLabel) {
		this.prefferedAirLineLabel = prefferedAirLineLabel;
	}
	public boolean isPrefferedAirLineTxtBox() {
		return prefferedAirLineTxtBox;
	}
	public void setPrefferedAirLineTxtBox(boolean prefferedAirLineTxtBox) {
		this.prefferedAirLineTxtBox = prefferedAirLineTxtBox;
	}
	public boolean isClassLabel() {
		return classLabel;
	}
	public void setClassLabel(boolean classLabel) {
		this.classLabel = classLabel;
	}
	public boolean isClassDrpDwnSize() {
		return classDrpDwn;
	}
	public void setClassDrpDwnSize(boolean classDrpDwnSize) {
		this.classDrpDwn = classDrpDwnSize;
	}
	public boolean isPreferredCurrencyLabel() {
		return preferredCurrencyLabel;
	}
	public void setPreferredCurrencyLabel(boolean preferredCurrencyLabel) {
		this.preferredCurrencyLabel = preferredCurrencyLabel;
	}
	public boolean isPreferredCurrencyDrpDwn() {
		return preferredCurrencyDrpDwn;
	}
	public void setPreferredCurrencyDrpDwn(boolean preferredCurrencyDrpDwn) {
		this.preferredCurrencyDrpDwn = preferredCurrencyDrpDwn;
	}
	public boolean isPromotionCodeLabel() {
		return promotionCodeLabel;
	}
	public void setPromotionCodeLabel(boolean promotionCodeLabel) {
		this.promotionCodeLabel = promotionCodeLabel;
	}
	public boolean isPromotionCodeTxtBox() {
		return promotionCodeTxtBox;
	}
	public void setPromotionCodeTxtBox(boolean promotionCodeTxtBox) {
		this.promotionCodeTxtBox = promotionCodeTxtBox;
	}
	public boolean isPreferNonStopFlightLabel() {
		return preferNonStopFlightLabel;
	}
	public void setPreferNonStopFlightLabel(boolean preferNonStopFlightLabel) {
		this.preferNonStopFlightLabel = preferNonStopFlightLabel;
	}
	public boolean isPreferNonStopFlightTxtBox() {
		return preferNonStopFlightChkBox;
	}
	public void setPreferNonStopFlightTxtBox(boolean preferNonStopFlightTxtBox) {
		this.preferNonStopFlightChkBox = preferNonStopFlightTxtBox;
	}
	public boolean isAdditionalOptionTxt() {
		return additionalOptionTxt;
	}
	public void setAdditionalOptionTxt(boolean additionalOptionTxt) {
		this.additionalOptionTxt = additionalOptionTxt;
	}
	public String getAdditionalOptionTxtBeforeClick() {
		return additionalOptionTxtBeforeClick;
	}
	public void setAdditionalOptionTxtBeforeClick(
			String additionalOptionTxtBeforeClick) {
		this.additionalOptionTxtBeforeClick = additionalOptionTxtBeforeClick;
	}
	public String getAdditionalOptionTxtAfterClick() {
		return additionalOptionTxtAfterClick;
	}
	public void setAdditionalOptionTxtAfterClick(
			String additionalOptionTxtAfterClick) {
		this.additionalOptionTxtAfterClick = additionalOptionTxtAfterClick;
	}
	public boolean isSearchButton() {
		return searchButton;
	}
	public void setSearchButton(boolean searchButton) {
		this.searchButton = searchButton;
	}
	
	
	/*public boolean search(WebDriver driver, SearchObject searchObject) throws WebDriverException, IOException {
		
		System.out.println("==================================");
		System.out.println("SEARCH START");

		WebDriverWait wait = new WebDriverWait(driver, 15);
		String Departure = searchObject.getDepartureDate().trim();
		String Return = searchObject.getReturnDate().trim();
		String scenarioCommonPath = "";
		String scenarioFailedPath = "";
		scenarioCommonPath = ScreenshotPathSingleton.getInstance()
				.getScenarioCommonPath();
		scenarioFailedPath = ScreenshotPathSingleton.getInstance()
				.getScenarioFailedPath();

		try {
			try {
				driver.switchTo().defaultContent();
				driver.switchTo().frame(propertyMap.get("HmPg_Frame_id"));
				Thread.sleep(3000);

				System.out.println("INFO -> BEC VALIDATION START");

				Screenshot.takeScreenshot(scenarioCommonPath
						+ "/Default Load Booking Engine.jpg", driver);

				wait.until(ExpectedConditions.presenceOfElementLocated(By
						.xpath(propertyMap.get("HmPg_MenuFlight_Btn_xpath"))));
				Thread.sleep(3000);

				if (driver.findElement(
						By.id(propertyMap.get("HmPg_RoundTrip_RadioBtn_id")))
						.isDisplayed()) {
					bookingEnginePage.setFlightDefaultSelected(true);
				} else {
					Screenshot.takeScreenshot(scenarioFailedPath
							+ "/Booking Engine - Default View.jpg", driver);
				}

				try {
					driver.findElement(
							By.xpath(propertyMap
									.get("HmPg_MenuFlight_Btn_xpath"))).click();
				} catch (Exception e) {
					Screenshot.takeScreenshot(scenarioFailedPath
							+ "/Booking Engine - Flight Module.jpg", driver);
				}

				Thread.sleep(5000);
				Screenshot.takeScreenshot(scenarioCommonPath
						+ "/Booking Engine (Flight).jpg", driver);
				wait.until(ExpectedConditions.presenceOfElementLocated(By
						.id(propertyMap.get("HmPg_ResidenceCountry_DropDwn_id"))));

				try {

					try {
						if (driver
								.findElement(
										By.className(propertyMap
												.get("HmPg_AdditionalSearchOption_class")))
								.isDisplayed()) {
							bookingEnginePage.setAdditionalOptionTxt(true);
							bookingEnginePage
									.setAdditionalOptionTxtBeforeClick(driver
											.findElement(
													By.className("show_filter"))
											.getText().trim());
							driver.findElement(
									By.className(propertyMap
											.get("HmPg_AdditionalSearchOption_class")))
									.click();
							bookingEnginePage
									.setAdditionalOptionTxtAfterClick(driver
											.findElement(
													By.className(propertyMap
															.get("HmPg_AdditionalSearchOption_class")))
											.getText().trim());
						}

					} catch (Exception e) {
						Screenshot.takeScreenshot(scenarioFailedPath
								+ "/Additional Option Link Text Missing",
								driver);
					}

					try {
						ArrayList<WebElement> mainLabels = new ArrayList<WebElement>(
								driver.findElements(By.className(propertyMap
										.get("HmPg_tripTypeMainLabel_class"))));
						String mainLabelText = mainLabels.get(0).getText();
						if (mainLabelText.contains("Trip Type")) {
							bookingEnginePage.setTripTypeMainLabel(true);
							bookingEnginePage
									.setTripTypeMainLabelTxt(mainLabelText);
						}

						for (int y = 0; y < mainLabels.size(); y++) {
							System.out.println(mainLabels.get(y).getText());
						}
					} catch (Exception e) {
						Screenshot.takeScreenshot(scenarioFailedPath
								+ "/Trip Type Label Missing", driver);
					}

					try {
						ArrayList<WebElement> tripTypeDivs = new ArrayList<WebElement>(
								driver.findElement(
										By.className(propertyMap
												.get("HmPg_TripLabelsDiv_class")))
										.findElements(
												By.className(propertyMap
														.get("HmPg_TripTypeLabels_class"))));
						if (tripTypeDivs.size() == 3) {
							if (tripTypeDivs.get(0).getText().trim()
									.equalsIgnoreCase("Round Trip")) {
								bookingEnginePage
										.setTripTypeRoundTripLabel(true);
								bookingEnginePage
										.setTripTypeRoundTripLabelTxt(tripTypeDivs
												.get(0).getText().trim());
							} else {
								bookingEnginePage
										.setTripTypeRoundTripLabel(false);
								Screenshot.takeScreenshot(scenarioFailedPath
										+ "/Round Trip Label Text Missing",
										driver);
							}
							if (tripTypeDivs.get(1).getText().trim()
									.equalsIgnoreCase("One Way Trip")) {
								bookingEnginePage.setTripTypeOneWayLabel(true);
								bookingEnginePage
										.setTripTypeOneWayLabelTxt(tripTypeDivs
												.get(1).getText().trim());
							} else {
								bookingEnginePage.setTripTypeOneWayLabel(false);
								Screenshot.takeScreenshot(scenarioFailedPath
										+ "/Oneway Trip Label Text Missing",
										driver);
							}
							if (tripTypeDivs.get(2).getText().trim()
									.equalsIgnoreCase("Multi Destination")) {
								bookingEnginePage.setTripTypeMultiLabel(true);
								bookingEnginePage
										.setTripTypeMultiLabelTxt(tripTypeDivs
												.get(2).getText().trim());
							} else {
								bookingEnginePage.setTripTypeMultiLabel(false);
								Screenshot
										.takeScreenshot(
												scenarioFailedPath
														+ "/Multi destination Label Text Missing",
												driver);
							}
						}
					} catch (Exception e) {

					}

					try {
						bookingEnginePage
								.setTripTypeRoundTripRadButton(isElementPresent(
										driver,
										By.id(propertyMap
												.get("HmPg_RoundTrip_RadioBtn_id"))));
						bookingEnginePage
								.setTripTypeOneWayRadButton(isElementPresent(
										driver,
										By.id(propertyMap
												.get("HmPg_OnewayTrip_RadioBtn_id"))));
						bookingEnginePage
								.setTripTypeMultiRadButton(isElementPresent(
										driver,
										By.id(propertyMap
												.get("HmPg_MultiDestination_RadioBtn_id"))));
					} catch (Exception e) {
						Screenshot.takeScreenshot(scenarioFailedPath
								+ "/Trip type Radio buttons Missing", driver);
					}

					try {
						bookingEnginePage.setWhereGoingLabel(isElementPresent(
								driver, By.xpath(propertyMap
										.get("HmPg_whereGoingLabel_xpath"))));
						bookingEnginePage
								.setWhereGoingLabelTxt(driver
										.findElement(
												By.xpath(propertyMap
														.get("HmPg_whereGoingLabel_xpath")))
										.getText().trim());
					} catch (Exception e) {
						Screenshot.takeScreenshot(scenarioFailedPath
								+ "/Where going Label text Missing", driver);
					}

					try {
						// System.out.println("=======================================================");
						ArrayList<WebElement> blueboxlabels = new ArrayList<WebElement>(
								driver.findElements(By.className(propertyMap
										.get("HmPg_BlueboxLabelsLocation_class"))));
						ArrayList<WebElement> blueboxlabelsOne = new ArrayList<WebElement>(
								blueboxlabels
										.get(0)
										.findElements(
												By.className(propertyMap
														.get("HmPg_Bluebox_LabelText_class"))));

						for (int i = 0; i < blueboxlabelsOne.size(); i++) {
							try {
								System.out.println(blueboxlabelsOne.get(i)
										.getText());
							} catch (Exception e) {

							}
						}

						if (blueboxlabelsOne.size() == 2) {
							if (blueboxlabelsOne.get(0).getText().trim()
									.equalsIgnoreCase("From")) {
								bookingEnginePage.setFromLabel(true);
								bookingEnginePage.setFromLabelTxt(blueboxlabels
										.get(0).getText().trim());
							} else {
								bookingEnginePage.setFromLabel(false);
								Screenshot.takeScreenshot(scenarioFailedPath
										+ "/From Missing", driver);
							}
							if (blueboxlabelsOne.get(1).getText().trim()
									.equalsIgnoreCase("To")) {
								bookingEnginePage.setToLabel(true);
								bookingEnginePage.setToLabelTxt(blueboxlabels
										.get(1).getText().trim());
							} else {
								bookingEnginePage.setToLabel(false);
								Screenshot.takeScreenshot(scenarioFailedPath
										+ "/To Missing", driver);
							}
						}
					} catch (Exception e) {

					}

					try {
						// System.out.println("===================================================");
						ArrayList<WebElement> adultContainer = new ArrayList<WebElement>(
								driver.findElements(By.className(propertyMap
										.get("HmPg_PassengerContainer_class"))));
						ArrayList<WebElement> adultContainerText = new ArrayList<WebElement>(
								adultContainer
										.get(0)
										.findElements(
												By.className(propertyMap
														.get("HmPg_PassengerContainer_LabelText_class"))));
						for (int i = 0; i < adultContainerText.size(); i++) {
							System.out.println(adultContainerText.get(i)
									.getText());
						}
						if (adultContainerText.size() == 3) {
							if (adultContainerText.get(0).getText().trim()
									.contains("Adult")) {
								bookingEnginePage.setAdultsLabel(true);
								bookingEnginePage
										.setAdultsLabelTxt(adultContainerText
												.get(0).getText().trim());
							} else {
								bookingEnginePage.setAdultsLabel(false);
								Screenshot.takeScreenshot(scenarioFailedPath
										+ "/Adults Label Missing", driver);
							}
							if (adultContainerText.get(1).getText().trim()
									.contains("Children")) {
								bookingEnginePage.setChildrenLabel(true);
								bookingEnginePage
										.setChildrenLabelTxt(adultContainerText
												.get(1).getText().trim());
							} else {
								bookingEnginePage.setChildrenLabel(false);
								Screenshot.takeScreenshot(scenarioFailedPath
										+ "/Children Label Missing", driver);
							}
							if (adultContainerText.get(2).getText().trim()
									.contains("Infant")) {
								bookingEnginePage.setInfantLabel(true);
								bookingEnginePage
										.setInfantLabelTxt(adultContainerText
												.get(2).getText().trim());
							} else {
								bookingEnginePage.setInfantLabel(false);
								Screenshot.takeScreenshot(scenarioFailedPath
										+ "/Infants Label Missing", driver);
							}
						}
					} catch (Exception e) {

					}

					try {
						if (driver.findElement(
								By.id(propertyMap.get("HmPg_From_TxtBox_id")))
								.isDisplayed()) {
							bookingEnginePage.setFromTxtBox(true);
						} else {
							Screenshot.takeScreenshot(scenarioFailedPath
									+ "/From Textbox Missing", driver);
						}
						if (driver.findElement(
								By.id(propertyMap.get("HmPg_To_TxtBox_id")))
								.isDisplayed()) {
							bookingEnginePage.setToTxtBox(true);
						} else {
							Screenshot.takeScreenshot(scenarioFailedPath
									+ "/To Textbox Missing", driver);
						}
					} catch (Exception e) {

					}

					try {

						String depDate = driver
								.findElements(
										By.xpath(propertyMap
												.get("HmPg_DepartureDate_div_xpath")))
								.get(0).getText();
						String depTime = driver
								.findElements(
										By.xpath(propertyMap
												.get("HmPg_DepartureTime_div_xpath")))
								.get(0).getText();
						
						if (depDate.contains("Departure Date")) {
							bookingEnginePage.setDepartureDateLabel(true);
							bookingEnginePage.setDepartureDateLabelTxt(depDate);
						} else {
							Screenshot.takeScreenshot(scenarioFailedPath
									+ "/Departure Date Label Text Missing",
									driver);
						}

						if (depTime.contains("Departure Time")) {
							bookingEnginePage.setDepartureTimeLabel(true);
							bookingEnginePage.setDepartureTimeLabelTxt(depTime);
						} else {
							Screenshot.takeScreenshot(scenarioFailedPath
									+ "/Departure Time Label Text Missing",
									driver);
						}

						String retDate = driver
								.findElements(
										By.id(propertyMap
												.get("HmPg_RetrunDate_div_id")))
								.get(0).getText();
						String retTime = driver
								.findElements(
										By.id(propertyMap
												.get("HmPg_RetrunTime_div_id")))
								.get(0).getText();
						if (retDate.contains("Return Date")) {
							bookingEnginePage.setReturnDateLabel(true);
							bookingEnginePage.setReturnDateLabelTxt(retDate);
						} else {
							Screenshot
									.takeScreenshot(
											scenarioFailedPath
													+ "/Return Date Label Text Missing",
											driver);
						}
						if (retTime.contains("Return Time")) {
							bookingEnginePage.setReturnTimeLabel(true);
							bookingEnginePage.setReturnTimeLabelTxt(retTime);
						} else {
							Screenshot
									.takeScreenshot(
											scenarioFailedPath
													+ "/Return Time Label Text Missing",
											driver);
						}

					} catch (Exception e) {
						Screenshot.takeScreenshot(scenarioFailedPath
								+ "/Departure Return Label Missing", driver);
					}

					try {
						try {
							if (driver.findElements(
											By.id(propertyMap
													.get("HmPg_DepartureDateTemp_TxtBox_id")))
									.size() > 0) {
								bookingEnginePage.setDepartureDatePicker(true);
							} else {
								Screenshot.takeScreenshot(scenarioFailedPath
										+ "/Departure Date Selector Missing",
										driver);
							}
						} catch (Exception e) {

						}
						try {
							if (driver
									.findElements(
											By.id(propertyMap
													.get("HmPg_DepartureTime_DropDwn_id")))
									.size() > 0
												 * driver.findElement(By.id(
												 * "HmPg_DepartureTime_DropDwn_id"
												 * )).isDisplayed()
												 ) {
								bookingEnginePage.setDepartureTimeDrpDwn(true);
								int departureTimeDrpDwnSize = new Select(
										driver.findElement(By.id(propertyMap
												.get("HmPg_DepartureTime_DropDwn_id"))))
										.getOptions().size();
								bookingEnginePage
										.setDepartureTimeDrpDwnSize(departureTimeDrpDwnSize);
							} else {
								Screenshot.takeScreenshot(scenarioFailedPath
										+ "/Departure Time Dropdown Missing",
										driver);
							}
						} catch (Exception e) {
							Screenshot.takeScreenshot(scenarioFailedPath
									+ "/Departure Time Dropdown Missing",
									driver);
						}

						try {
							if (driver
									.findElements(
											By.id(propertyMap
													.get("HmPg_ReturnDateTemp_TxtBox_id")))
									.size() > 0
												 * driver.findElement(By.id(
												 * "HmPg_ReturnDateTemp_TxtBox_id"
												 * )).isDisplayed()
												 ) {
								bookingEnginePage.setReturnDatePicker(true);
							} else {
								Screenshot.takeScreenshot(scenarioFailedPath
										+ "/Return Date Selector Missing",
										driver);
							}
						} catch (Exception e) {
							Screenshot.takeScreenshot(scenarioFailedPath
									+ "/Return Date Selector Missing", driver);
						}
						try {
							if (driver
									.findElements(
											By.id(propertyMap
													.get("HmPg_ReturnTime_DropDwn_id")))
									.size() > 0
												 * driver.findElement(By.id(
												 * "HmPg_ReturnTime_DropDwn_id"
												 * )).isDisplayed()
												 ) {
								bookingEnginePage.setReturnTimeDrpDwn(true);
								int returnTimeDrpDwnSize = new Select(
										driver.findElement(By.id(propertyMap
												.get("HmPg_ReturnTime_DropDwn_id"))))
										.getOptions().size();
								bookingEnginePage
										.setReturnTimeDrpDwnSize(returnTimeDrpDwnSize);
							} else {
								Screenshot.takeScreenshot(scenarioFailedPath
										+ "/Return time Dropdown Missing",
										driver);
							}
						} catch (Exception e) {

						}

					} catch (Exception e) {

					}

					try {
						ArrayList<WebElement> HmPg_TripTypeLabels_class = new ArrayList<WebElement>(
								driver.findElements(By.className(propertyMap
										.get("HmPg_tripTypeMain_LabelText_class"))));
						for (int i = 0; i < HmPg_TripTypeLabels_class.size(); i++) {
							if (HmPg_TripTypeLabels_class.get(i).getText()
									.trim().contains("How Many People?")) {
								bookingEnginePage.setHowManyPeopleLabel(true);
								bookingEnginePage
										.setHowManyPeopleLabelTxt(HmPg_TripTypeLabels_class
												.get(3).getText().trim());
							}
							if (HmPg_TripTypeLabels_class.get(i).getText()
									.trim().contains("Trip Type")) {
								bookingEnginePage.setTripTypeMainLabel(true);
								bookingEnginePage
										.setTripTypeMainLabelTxt(HmPg_TripTypeLabels_class
												.get(i).getText().trim());
							}
							if (HmPg_TripTypeLabels_class.get(i).getText()
									.trim().contains("Where are you going ?")) {
								bookingEnginePage.setWhereGoingLabel(true);
								bookingEnginePage
										.setWhereGoingLabelTxt(HmPg_TripTypeLabels_class
												.get(i).getText().trim());
							}
							if (HmPg_TripTypeLabels_class.get(i).getText()
									.trim().contains("Country of Residence ?")) {
								bookingEnginePage
										.setCountryResidenceLabel(true);
								bookingEnginePage
										.setCountryResidenceLabelText(HmPg_TripTypeLabels_class
												.get(i).getText().trim());
							}
						}
					} catch (Exception e) {

					}

					try {
						int HmPg_AdultCount_DropDwn_id = new Select(
								driver.findElement(By.id(propertyMap
										.get("HmPg_AdultCount_DropDwn_id"))))
								.getOptions().size();
						int HmPg_ChildrenCount_DropDwn_id = new Select(
								driver.findElement(By.id(propertyMap
										.get("HmPg_ChildrenCount_DropDwn_id"))))
								.getOptions().size();
						int HmPg_InfantCount_DropDwn_id = new Select(
								driver.findElement(By.id(propertyMap
										.get("HmPg_InfantCount_DropDwn_id"))))
								.getOptions().size();
						bookingEnginePage
								.setAdultDropdownSize(HmPg_AdultCount_DropDwn_id);
						bookingEnginePage
								.setChildrenDropdownSize(HmPg_ChildrenCount_DropDwn_id);
						bookingEnginePage
								.setInfantDropdownSize(HmPg_InfantCount_DropDwn_id);
					} catch (Exception e) {

					}

					try {

						String prefLabelText = driver
								.findElement(
										By.className(propertyMap
												.get("HmPg_PrefferedAirline_Label_class")))
								.getText().trim();
						if (prefLabelText.contains("Preferred Airline Code")) {
							bookingEnginePage.setPrefferedAirLineLabel(true);
							bookingEnginePage
									.setPrefferedAirLineLabelTxt(prefLabelText);
						}
						bookingEnginePage
								.setPrefferedAirLineTxtBox(isElementPresent(
										driver,
										By.id(propertyMap
												.get("HmPg_PrefferedAirline_TxtBox_id"))));

					} catch (Exception e) {

					}

					try {
						ArrayList<WebElement> cabinClassEle = new ArrayList<WebElement>(
								driver.findElements(By.id(propertyMap
										.get("HmPg_CabinClass_Label_id"))));
						String cabinText = "";
						cabinText = cabinClassEle
								.get(0)
								.findElement(
										By.className(propertyMap
												.get("HmPg_CabinClass_LabelText_class")))
								.getText().trim();
						if (cabinText.contains("Class")) {
							bookingEnginePage.setClassLabel(true);
							bookingEnginePage.setClassLabelTxt(cabinText);
						}
						bookingEnginePage
								.setClassDrpDwn(isElementPresent(
										driver,
										By.id(propertyMap
												.get("HmPg_PrefferedAirline_TxtBox_id"))));
						int HmPg_PrefferedAirline_TxtBox_id = new Select(
								driver.findElement(By.id(propertyMap
										.get("HmPg_PrefferedAirline_TxtBox_id"))))
								.getOptions().size();
						bookingEnginePage
								.setClassDrpDwnSize(HmPg_PrefferedAirline_TxtBox_id);
					} catch (Exception e) {

					}

					try {
						ArrayList<WebElement> preferrcur = new ArrayList<WebElement>(
								driver.findElements(By.id(propertyMap
										.get("HmPg_PrefferedCurrency_Label_id"))));
						String preferrcurText = "";
						preferrcurText = preferrcur
								.get(0)
								.findElement(
										By.className(propertyMap
												.get("HmPg_PrefferedCurrency_LabelText_class")))
								.getText().trim();
						if (preferrcurText.contains("Preferred Currency")) {
							bookingEnginePage.setPreferredCurrencyLabel(true);
							bookingEnginePage
									.setPreferredCurrencyLabelTxt(preferrcurText);
						}
						bookingEnginePage
								.setPreferredCurrencyDrpDwn(isElementPresent(
										driver,
										By.id(propertyMap
												.get("HmPg_PrefferedCurrency_DropDwn_id"))));
					} catch (Exception e) {

					}

					try {
						ArrayList<WebElement> promoCode = new ArrayList<WebElement>(
								driver.findElements(By.id(propertyMap
										.get("HmPg_PromotionCode_Label_id"))));
						String promotionCodeLabelTxt = "";
						promotionCodeLabelTxt = promoCode
								.get(0)
								.findElement(
										By.className(propertyMap
												.get("HmPg_PromotionCode_LabelText_class")))
								.getText().trim();
						if (promotionCodeLabelTxt.contains("Promotion Code")) {
							bookingEnginePage.setPromotionCodeLabel(true);
							bookingEnginePage
									.setPromotionCodeLabelTxt(promotionCodeLabelTxt);
						}
						bookingEnginePage
								.setPromotionCodeTxtBox(isElementPresent(
										driver,
										By.id(propertyMap
												.get("HmPg_PromotionCode_TxtBox_id"))));

					} catch (Exception e) {

					}

					try {
						bookingEnginePage
								.setPreferNonStopFlightChkBox(isElementPresent(
										driver,
										By.xpath(propertyMap
												.get("HmPg_PreferNonStop_ChkBox_xpath"))));
						ArrayList<WebElement> preferrNonStop = new ArrayList<WebElement>(
								driver.findElements(By.id(propertyMap
										.get("HmPg_PreferNonStop_Div_id"))));
						String preferrNonStopText = "";
						preferrNonStopText = preferrNonStop
								.get(0)
								.findElement(
										By.className(propertyMap
												.get("HmPg_PreferNonStop_Label_class")))
								.getText().trim();
						if (preferrNonStopText
								.contains("I Prefer Non-Stop Flights")) {
							bookingEnginePage.setPreferNonStopFlightLabel(true);
							bookingEnginePage
									.setPreferNonStopFlightLabelTxt(preferrNonStopText);
						}
					} catch (Exception e) {

					}

					bookingEnginePage
							.setSearchButton(isElementPresent(driver, By
									.id(propertyMap
											.get("HmPg_SearchFlights_Btn_id"))));

				} catch (Exception e) {
					System.out.println(e.toString());
				}

			} catch (Exception e) {
				Screenshot.takeScreenshot(scenarioFailedPath
						+ "/Booking Engine.jpg", driver);
				System.out.println(e.toString());
			}
			System.out.println("INFO -> BEC VALIDATION END");

			System.out.println("INFO -> ENTER SEARCH DETAILS TO BEC");
			try {
				Thread.sleep(5000);
				new Select(driver.findElement(By.id(propertyMap
						.get("HmPg_ResidenceCountry_DropDwn_id"))))
						.selectByVisibleText(searchObject.getCountry().trim());
			} catch (Exception e) {
				System.out
						.println("This is a TO Booking dude... No residence country is required...");
			}

			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By
						.id(propertyMap.get("HmPg_From_TxtBox_id"))));
				Thread.sleep(3000);

				driver.findElement(
						By.id(propertyMap.get("HmPg_From_TxtBox_id"))).click();
				Screenshot.takeScreenshot(scenarioCommonPath
						+ "/BE From Origin drop down.jpg", driver);
				driver.findElement(
						By.id(propertyMap.get("HmPg_From_TxtBox_id")))
						.sendKeys(searchObject.getFrom().trim().split("\\|")[0]);
				((JavascriptExecutor) driver)
						.executeScript("document.getElementById('"
								+ propertyMap.get("HmPg_From_Hidden_id")
								+ "').setAttribute('value','"
								+ searchObject.getFrom().trim() + "');");

				driver.findElement(
						By.id(propertyMap.get("HmPg_To_TxtBox_id").trim()))
						.click();
				Screenshot.takeScreenshot(scenarioCommonPath
						+ "/BE To Destination drop down.jpg", driver);
				driver.findElement(By.id(propertyMap.get("HmPg_To_TxtBox_id")))
						.sendKeys(searchObject.getTo().trim().split("\\|")[0]);
				((JavascriptExecutor) driver)
						.executeScript("document.getElementById('"
								+ propertyMap.get("HmPg_To_Hidden_id")
								+ "').setAttribute('value','"
								+ searchObject.getTo().trim() + "');");

			} catch (Exception e) {

			}

			if (searchObject.getTriptype().equals("Round Trip")) {

				// driver.findElement(By.id(propertyMap.get("HmPg_DepartureDate_TxtBox_id"))).click();
				Screenshot.takeScreenshot(scenarioCommonPath
						+ "/BE From Date.jpg", driver);
				((JavascriptExecutor) driver).executeScript("$('#"
						+ propertyMap.get("HmPg_DepartureDate_TxtBox_id")
						+ "').val('" + Departure + "');");
				// new
				// Select(driver.findElement(By.id(propertyMap.get("HmPg_DepartureTime_DropDwn_id")))).selectByValue(searchObject.getDepartureTime().trim());

				// driver.findElement(By.id(propertyMap.get("HmPg_RetrunDate_TxtBox_id"))).click();
				Screenshot.takeScreenshot(scenarioCommonPath
						+ "/BE To Date.jpg", driver);
				((JavascriptExecutor) driver).executeScript("$('#"
						+ propertyMap.get("HmPg_RetrunDate_TxtBox_id")
						+ "').val('" + Return + "');");
				// new
				// Select(driver.findElement(By.id(propertyMap.get("HmPg_ReturnTime_DropDwn_id")))).selectByValue(searchObject.getReturnTime().trim());

			} else if (searchObject.getTriptype().equals("One Way Trip")) {

				wait.until(ExpectedConditions.presenceOfElementLocated(By
						.id(propertyMap.get("HmPg_OnewayTrip_RadioBtn_id"))));
				((JavascriptExecutor) driver).executeScript("$('#"
						+ propertyMap.get("HmPg_OnewayTrip_RadioBtn_id")
						+ "').click();");
				try {
					driver.findElement(
							By.id(propertyMap
									.get("HmPg_OnewayTrip_RadioBtn_id")))
							.click();
				} catch (Exception e) {
					e.printStackTrace();
				}

				Thread.sleep(4000);
				try {
					((JavascriptExecutor) driver).executeScript("$('#"
							+ propertyMap.get("HmPg_OnewayTrip_RadioBtn_id")
							+ "').click();");
				} catch (Exception e) {
					e.printStackTrace();
				}

				wait.until(ExpectedConditions.presenceOfElementLocated(By
						.id(propertyMap.get("HmPg_DepartureDate_TxtBox_id"))));
				// driver.findElement(By.id(propertyMap.get("HmPg_DepartureDate_TxtBox_id"))).click();
				Screenshot.takeScreenshot(scenarioCommonPath
						+ "/BE From Date.jpg", driver);
				((JavascriptExecutor) driver).executeScript("$('#"
						+ propertyMap.get("HmPg_DepartureDate_TxtBox_id")
						+ "').val('" + searchObject.getDepartureDate().trim()
						+ "');");
				new Select(driver.findElement(By.id(propertyMap
						.get("HmPg_DepartureTime_DropDwn_id"))))
						.selectByVisibleText(searchObject.getDepartureTime()
								.trim());
			}

			// FLEXIBLE FLIGHTS
			if (Boolean.valueOf(searchObject.isFlexible())) {
				driver.findElement(
						By.id(propertyMap.get("HmPg_Flexible_ChkBox_id")))
						.click();
			}

			// =========================PASSENGERS==========================
			// ADULTS
			try {
				driver.findElement(
						By.id(propertyMap.get("HmPg_AdultCount_DropDwn_id")))
						.click();
				Screenshot.takeScreenshot(scenarioCommonPath
						+ "/BE Adult drop down.jpg", driver);
				new Select(driver.findElement(By.id(propertyMap
						.get("HmPg_AdultCount_DropDwn_id"))))
						.selectByValue(searchObject.getAdult());
			} catch (Exception e) {

			}

			// CHILDREN
			if (!searchObject.getChildren().trim().equals("0")) {

				driver.findElement(
						By.id(propertyMap.get("HmPg_ChildrenCount_DropDwn_id")))
						.click();
				Screenshot.takeScreenshot(scenarioCommonPath
						+ "/BE Children drop down.jpg", driver);
				new Select(driver.findElement(By.id(propertyMap
						.get("HmPg_ChildrenCount_DropDwn_id"))))
						.selectByValue(searchObject.getChildren().trim());

				String value = "";
				value = propertyMap.get("HmPg_ChildrenAge_DropDwn_id");
				try {
					for (int i = 1; i <= searchObject.getChildrenAge().size(); i++) {
						value = value.replace("REPLACE", String.valueOf(i));
						new Select(driver.findElement(By.id(value)))
								.selectByValue(searchObject.getChildrenAge()
										.get((i - 1)).trim());// Child Loop
					}
				} catch (Exception e) {

				}
			}

			// INFANT
			if (!searchObject.getInfant().trim().equals("0")) {

				driver.findElement(
						By.id(propertyMap.get("HmPg_InfantCount_DropDwn_id")))
						.click();
				Screenshot.takeScreenshot(scenarioCommonPath
						+ "/BE Infant drop down.jpg", driver);
				new Select(driver.findElement(By.id(propertyMap
						.get("HmPg_InfantCount_DropDwn_id"))))
						.selectByValue(searchObject.getInfant().trim());
			}

			// SHOW ADDITIONAL SEARCH OPTIONS
			// driver.findElement(By.xpath(propertyMap.get("HmPg_AdditionalSearchOption_xpath"))).click();

			// PREFERRED AIRLINE CODE
			
			 * if(!searchObject.getPreferredAirline().trim().equals("NONE")){
			 * driver
			 * .findElement(By.id(propertyMap.get("HmPg_PrefferedAirline_TxtBox_id"
			 * ))).sendKeys(searchObject.getPreferredAirline().trim()); }
			 

			// CABIN CLASS
			// new
			// Select(driver.findElement(By.id(propertyMap.get("HmPg_CabinClass_DropDwn_id")))).selectByValue(searchObject.getCabinClass().trim());

			
			 * //PREFERRED CURRENCY
			 * if(!searchObject.getPreferredCurrency().trim(
			 * ).equals("Select Currency")) { new
			 * Select(driver.findElement(By.id
			 * (propertyMap.get("HmPg_PrefferedCurrency_DropDwn_id"
			 * )))).selectByValue(searchObject.getPreferredCurrency().trim()); }
			 

			// PROMOTION CODE
			
			 * if (false) { driver.findElement(By.id(propertyMap.get(
			 * "HmPg_PromotionCode_TxtBox_id"
			 * ))).sendKeys(searchObject.getPromotionCode().trim()); }
			 

			// PREFER NONSTOP
			
			 * if (true) { driver.findElement(By.xpath(propertyMap.get(
			 * "HmPg_PreferNonStop_ChkBox_xpath"))).click(); }
			 

			// new
			// Select(driver.findElement(By.id(propertyMap.get("HmPg_ResidenceCountry_DropDwn_id")))).selectByVisibleText(searchObject.getCountry().trim());

			if (searchObject.isApplyDiscount()) {
				try {
					Screenshot.takeScreenshot(scenarioCommonPath
							+ "/Before Click Show Additional Search.jpg",
							driver);
					driver.findElement(
							By.partialLinkText("Show Additional Search Options  "))
							.click();
					Screenshot
							.takeScreenshot(
									scenarioCommonPath
											+ "/After Click Show Additional Search.jpg",
									driver);
					wait.until(ExpectedConditions.presenceOfElementLocated(By
							.id(propertyMap.get("HmPg_PromotionCode_TxtBox_id"))));
					driver.findElement(
							By.id(propertyMap
									.get("HmPg_PromotionCode_TxtBox_id")))
							.sendKeys(propertyMap.get("DiscountCouponNo"));
				} catch (Exception e) {
					System.out.println("Discount No not entered");
				}

			}

			Thread.sleep(3000);
			try {
				((JavascriptExecutor) driver)
						.executeScript("JavaScript:search('F');");
			} catch (Exception e) {
				System.out.println(e.toString());
				try {
					driver.findElement(
							By.id(propertyMap.get("HmPg_SearchFlights_Btn_id")))
							.click();
				} catch (Exception e2) {
					System.out.println(e2.toString());
				}
			}

			done = true;

		} catch (Exception e) {
			e.printStackTrace();
			done = false;
			System.out.println(e.toString());
		}

		System.out.println("INFO -> ENTER SEARCH DETAILS TO BEC");

		System.out.println("SEARCH ENDED SUCCESSFULLY");
		System.out.println("==================================");

		return done;

	}*/

	
}
