package com.common.Validators;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CommonValidator
{
	private static String commonDatePattern = "yyyy-MM-dd";
	
	
	//EQAL STRING VALUES IGNORING CASE
	public static boolean compareStringEqualsIgnoreCase(String Expected, String Actual, String TestDescription, StringBuffer ReportPrinter, int Reportcount)
	{
		boolean pass = false;
		
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>"+TestDescription+"</td>"
		+ "<td>"+Expected+"</td>");
		if( Expected.equalsIgnoreCase(Actual) )
		{
			ReportPrinter.append("<td>"+Actual+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
			pass = true;
		}
		else
		{
			ReportPrinter.append("<td>"+Actual+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
			pass = false;
		}
		return pass;
	}

	//EQUAL STRING VALUE AND GENERATE REPORT
	public static boolean compareStringEquals(String Expected, String Actual, String TestDescription, StringBuffer ReportPrinter, int Reportcount)
	{
		boolean pass = false;
		
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>"+TestDescription+"</td>"
		+ "<td>"+Expected+"</td>");
		if( Expected.equals(Actual) )
		{
			ReportPrinter.append("<td>"+Actual+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
			pass = true;
		}
		else
		{
			ReportPrinter.append("<td>"+Actual+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
			pass = false;
		}
		return pass;
	}
	
	//COMPARE INT AND GENERATE REPORT
	public static boolean compareInt(int Expected, int Actual, String TestDescription, StringBuffer ReportPrinter, int Reportcount)
	{
		boolean pass = false;
		
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>"+TestDescription+"</td>"
		+ "<td>"+Expected+"</td>");
		if( Expected == Actual )
		{
			ReportPrinter.append("<td>"+Actual+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
			pass = true;
		}
		else
		{
			ReportPrinter.append("<td>"+Actual+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
			pass = false;
		}
		return pass;
	}
	
	//COMPARE STRING VALUES AND GENERATE REPORT
	public static boolean compareStringContains(String Expected, String Actual, String TestDescription, StringBuffer ReportPrinter, int Reportcount)
	{
		boolean pass = false;
		
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>"+TestDescription+"</td>"
		+ "<td>"+Expected+"</td>");
		if( Actual.contains(Expected) || Expected.contains(Actual) )
		{
			ReportPrinter.append("<td>"+Actual+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
			pass = true;
		}
		else
		{
			ReportPrinter.append("<td>"+Actual+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
			pass = false;
		}
		return pass;
	}
	
	//COMPARE DOUBLE VALUES AND GENERATE REPORT
	public static boolean compareDouble(double Expected, double Actual, String TestDescription, StringBuffer ReportPrinter, int Reportcount, String RoundUpDownStatus, String RoundUpDownBy)
	{
		boolean pass = false;
		
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>"+TestDescription+"</td>"
		+ "<td>"+Expected+"</td>");
		if(RoundUpDownStatus.trim().equalsIgnoreCase("true"))
		{
			double gap				= 1;
			
			try {
				gap	= Double.parseDouble(RoundUpDownBy);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			double ceilExpected		= Math.ceil(Expected);
			double floorExpected	= Math.floor(Expected);

			if( Actual >= (floorExpected - gap) )
			{
				if( Actual <= (ceilExpected + gap) )
				{
					ReportPrinter.append("<td>"+Actual+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
					pass = true;
				}
				else
				{
					ReportPrinter.append("<td>"+Actual+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
					pass = false;
				}
			}
			else
			{
				ReportPrinter.append("<td>"+Actual+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
				pass = false;
			}
		}
		else
		{
			if( Actual == Expected )
			{
				ReportPrinter.append("<td>"+Actual+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				pass = true;
			}
			else
			{
				ReportPrinter.append("<td>"+Actual+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
				pass = false;
			}
		}
		
		return pass;
	}

	//CONVERT DOUBLE VALUE FROM ITS INCURRENCY TO ANY CURRENCY
	public static double convert(String InCurrency, String ToCurrency, String portalCurrency, double value, Map<String, String> CurrencyMap)
	{
		String PortalCurrency = portalCurrency;
			
		double   returnValue  =  0;
		if(PortalCurrency.equals(InCurrency) && PortalCurrency.equals(ToCurrency))
		{
			double fvalue	= value;
			returnValue   	= fvalue;
		}
		if(PortalCurrency.equals(InCurrency) && !PortalCurrency.equals(ToCurrency))
		{
			double  rate    	= Double.parseDouble(CurrencyMap.get(ToCurrency)); 
			double  fvalue    	= value;//fvalue = converted value to float
			double  cValue    	= fvalue * rate;//cvalue = conveted final value
			returnValue   = cValue;   
		}
		if(PortalCurrency.equals(ToCurrency) && !PortalCurrency.equals(InCurrency))
		{
			//System.out.println(CurrencyMap.get(InCurrency));
			double  rate		= Double.parseDouble(CurrencyMap.get(InCurrency));
			double  fValue		= value; //fvalue = converted value to float
			double  cValue		= fValue / rate;    //cvalue = conveted final value
			returnValue   = cValue;   
		}
		if(!PortalCurrency.equals(ToCurrency) && !PortalCurrency.equals(InCurrency))
		{
			//System.out.println(CurrencyMap.get(InCurrency));
			double  crate		= Double.parseDouble(CurrencyMap.get(InCurrency));
			double  srate		= Double.parseDouble(CurrencyMap.get(ToCurrency));
			double  fValue		= value; //fvalue = converted value to float
			double  cValue		= fValue / srate; 
			cValue				= cValue*crate;
			returnValue		= cValue;
		}
	
		return Math.ceil(returnValue);
	}
	
	//REMOVE COMMA FROM STRING VALUES 
	public static String formatRemoveComma(String val)
	{
		if( val.contains(",") )
		{
			val = val.replaceAll(",", "").trim();
		}
		return val;
	}
	
	//GET DATE IN COMMON FORMAT
	public static String formatDateToCommon(String source, String currentDatePattern)
	{
		Date				dateDate	= null;
		String				stringDate	= "";
		SimpleDateFormat	sdfIn		= null;
		SimpleDateFormat	sdfTo		= new SimpleDateFormat(commonDatePattern);
		
		try {
			sdfIn		= new SimpleDateFormat(currentDatePattern);
			dateDate	= sdfIn.parse(source);
			stringDate	= sdfTo.format(dateDate);
		} catch (Exception e) {
			//stringDate	= e.getMessage();
		}
		
		return stringDate;
	}
	
	//SET THE COMMON DATE PATTERN
	public static void setCommonDatePattern(String commonDatePattern) {
		CommonValidator.commonDatePattern = commonDatePattern;
	}
	
	//GET TIME IN COMMON FORMAT (11:22:00)
	public static String formatTimeToCommon(String source)
	{
		try {
			if(source.contains("H") && !source.contains(":") && source.length()==5)//1125H
			{
				source	= source.replace("H", ":00");
				String general1	= source.substring(0, 2).concat(":");
				String general2	= source.substring(2, 4);
				source	= general1.concat(general2).concat(":00");
			}
			else if(!source.contains("H") && !source.contains("h") && !source.contains("m") && !source.contains(":") && source.length()==6)//112500
			{
				String general1	= source.substring(0, 2).concat(":");
				String general2	= source.substring(2, 4);
				String general3	= ":".concat(source.substring(4, source.length()));
				source	= general1.concat(general2).concat(general3);
			}
			else if(source.contains("H") && !source.contains(":") && source.length()==7)//112500H
			{
				source	= source.replace("H", "");
				String general1	= source.substring(0, 2).concat(":");
				String general2	= source.substring(2, 4);
				String general3	= ":".concat(source.substring(4, source.length()));
				source	= general1.concat(general2).concat(general3);
			}
			else if(source.contains("H") && source.contains(":") && source.length()==6)//11:25H
			{
				source	= source.replace("H", ":00");
			}
			else if(source.contains("H") && source.contains(":") && source.length()==9)//11:25:00H
			{
				source	= source.replace("H", "");
			}
			else if(!source.contains("H") && source.contains(":") && source.length()==5)//11:25
			{
				source	= source.concat(":00");
			}
			else if(source.contains("hrs ") && source.contains("mins"))
			{
				source = source.replace("hrs ", ":");
				source = source.replace("mins", "");
				if(source.length() == 4)
				{
					source = "0".concat(source);
				}
				source = source.concat(":00");
			}
			else if(source.contains("h ") && source.contains("m") && !source.contains("hrs") && !source.contains("mins") || source.equals("0") )
			{
				source = source.replace("h ", ":");
				source = source.replace("m", "");
				if(source.length() == 3)
				{
					source = "0".concat(source);
				}
				source = source.concat(":00");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return source;
	}
	
	//GET CANCELLATION DATE FROM SUPPLIERS TICKET TIME LIMIT DATE
	public static String getCancellationDate(String sdate, String pattern)
	{
		String b = "";
		Date date;
		SimpleDateFormat sdfIn = null;
		SimpleDateFormat sdfTo = new SimpleDateFormat(commonDatePattern);
		Calendar cal = Calendar.getInstance();
		if(pattern.equals(commonDatePattern))
		{
			try 
			{
				date	= sdfTo.parse(sdate);
				cal.setTime(date);
				cal.add(Calendar.DATE, -3);
				
				b = sdfTo.format(cal.getTime());
			} 
			catch (ParseException e) 
			{
				e.printStackTrace();
				//b = e.getMessage();
			}
		}
		else
		{
			try {
				sdfIn		= new SimpleDateFormat(pattern);
				date		= sdfIn.parse(sdate);
				b			= sdfTo.format(date);
				cal.setTime(date);
				cal.add(Calendar.DATE, -3);
				
				b = sdfTo.format(cal.getTime());
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return b;
	}
	
	//GET SYSTEM DATE IN DEFINED/COMMON DATE FORMAT
	public static String getSystemDateInCommonFormat()
	{
		//DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		SimpleDateFormat sdfTo = new SimpleDateFormat(commonDatePattern);
		Date date = new Date();
		//System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48
		String r = sdfTo.format(date);
		return r;
	}
	
	//GET STRING DATE IN COMON DATE FORMAT AS A DATE TYPE OBJECT
	public static Date getDateInCommonDateFormat(String source, String sourcePattern)
	{
		Date				dateDate	= null;
		SimpleDateFormat	sdfIn		= null;
		
		try {
			sdfIn		= new SimpleDateFormat(sourcePattern);
		} catch (Exception e) {
			
		}

		try {
			dateDate	= sdfIn.parse(source);
		} catch (Exception e) {
			//stringDate = e.getMessage();
		}
		
		return dateDate;
	}
	
	
	public static boolean elementAvailable(boolean x, String TestDescription, StringBuffer ReportPrinter, int Reportcount)
	{
		boolean pass = false;
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>"+TestDescription+"</td>"
		+ "<td>"+"TRUE"+"</td>");
		if(x)
		{
			ReportPrinter.append("<td>"+x+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
			pass = true;
		}
		else
		{
			ReportPrinter.append("<td>"+x+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
			pass = false;
		}
		
		return pass;
	}
	
	public static boolean elementVisible(WebDriver driver, String element)
	{
		boolean available = false;
		
		try {
			if( driver.findElement(By.id(element)).isDisplayed() )
			{
				available = true;
			}
		} catch (Exception e) {
			
		}
		
		return available;
	}
	
	public static double calculateDiscount(String discountType, double value, double cost)
	{
		double discount = 0;
		
		if(discountType.equalsIgnoreCase("Value"))
		{
			discount = value;
		}
		else if(discountType.equalsIgnoreCase("Percentage"))
		{
			discount = cost * value;
		}
		
		return discount;
	}
	
}
