package com.utilities;

public class Repository
{
	
	public static String  LowFareSearchRequest		= "-";
	public static String  LowFareSearchResponse		= "-";
	public static String  ReservationRequest		= "-";
	public static String  ReservationResponse		= "-";
	public static String  PrefAirlineSearchRequest	= "-";
	public static String  PrefAirlineSearchResponse = "-";
	public static String  PriceRequest				= "-";
	public static String  PriceResponse				= "-";
	public static String  ETicketRequest			= "-";
	public static String  ETicketResponse			= "-";
	public static String  PNRUpdateRequest			= "-";
	public static String  PNRUpdateResponse			= "-";
	public static String  Cancellationrequest		= "-";
	public static String  CancellationResponse		= "-";
	
	
	
	public static void setAll()
	{
		Repository.LowFareSearchRequest			= "-";
		Repository.LowFareSearchResponse		= "-";
		Repository.ReservationRequest			= "-";
		Repository.ReservationResponse			= "-";
		Repository.PrefAirlineSearchRequest		= "-";
		Repository.PrefAirlineSearchResponse	= "-";
		Repository.PriceRequest					= "-";
		Repository.PriceResponse				= "-";
		Repository.ETicketRequest				= "-";
		Repository.ETicketResponse				= "-";
		Repository.PNRUpdateRequest				= "-";
		Repository.PNRUpdateResponse			= "-";
		Repository.Cancellationrequest			= "-";
		Repository.CancellationResponse			= "-";
	}
}
