package com.utilities;

import java.io.File;
import org.apache.commons.io.FileUtils;
//import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Screenshot {

	//private static Logger logger = org.apache.log4j.Logger.getLogger("Screenshot");

	public static void takeScreenshot(String path, WebDriver driver) {

		try {
			//logger.info("Creating screenshot on path-->" + path);
			FileUtils.copyFile(((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE), new File(path));
			//logger.info("Screenshot Saved Successfully..!!!");
		} catch (Exception e) {
			//logger.fatal("Screenshot Saving Failed " + e.toString());
		}

	}
	
	public static String CreateDir(String screenpath, String count) {
		
		String StringPath = screenpath + "/Scenario_" + count;
		File ScenarioPath = new File(StringPath);
		if (!ScenarioPath.exists()) {
			//logger.info("Screenshot path not available - Creating directories..");

			try {
				ScenarioPath.mkdirs();
				//logger.info("Directory created succcessfully!!");
			} catch (Exception e) {
				//logger.fatal("Directory creation failed " + e.toString());
			}
		}
		return StringPath;
	}
	
}
