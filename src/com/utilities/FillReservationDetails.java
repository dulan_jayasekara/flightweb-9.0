//Sanoj
package com.utilities;

import java.util.ArrayList;

import system.classes.*;

public class FillReservationDetails 
{
	
	public ReservationInfo Fill_Reservation_details(SearchObject sobj)
	{
		System.out.println("SET FILLING PASSENGER DETAILS");
		
		String[] name = {"one","two","three","four","five","six","seven","eight","nine"};
		ReservationInfo resvFillobj = new ReservationInfo();
		int Noofadults = Integer.parseInt(sobj.getAdult());
		int Noofchildren = Integer.parseInt(sobj.getChildren());
		int Noofinfants = Integer.parseInt(sobj.getInfant());
		ArrayList<Traveler> adultlist = new ArrayList<Traveler>();
		ArrayList<Traveler> childrenlist = new ArrayList<Traveler>();
		ArrayList<Traveler> infantlist = new ArrayList<Traveler>(); 
		System.out.println("INFO -> Main traveller");
		String adultfname = "adf";
		String adultsurname = "ads";
		String childfname = "chf";
		String childsurname = "chs";
		String infantfname = "inff";
		String infantsurname = "infs";
		
		Traveler maincustomer = new Traveler();
		maincustomer.setGivenName("Adam");
		maincustomer.setSurname("Levine");
		
		Address add = new Address();
		add.setAddressStreetNo("California");
		add.setAddressCity("California");
		add.setAddressCountry("Christmas Island");
		add.setStateProv("California");
		add.setCountryZip("");
		maincustomer.setAddress(add);
		maincustomer.setPhoneNumber("25122015");
		maincustomer.setEmail("sanoj@colombo.rezgateway.com");
		
		resvFillobj.setMaincustomer(maincustomer);
		System.out.println("INFO -> Main traveller end");
		
		for(int a = 0; a<Noofadults; a++)
		{
			System.out.println("INFO -> adult : "+a+1);
			adultfname = "DanM";
			adultsurname = "WinM";
			adultfname = adultfname.concat(name[a]);
			adultsurname = adultsurname.concat(name[a]);
			Traveler adult = new Traveler();
			adult.setPassengertypeCode("ADT");
			adult.setNamePrefixTitle("Mr");
			adult.setGivenName(adultfname);
			adult.setSurname(adultsurname);
			adult.setPhoneNumber("2444555");
			adult.setPassportNo("111111111111");
			adult.setPassportExpDate("01-01-2020");
			adult.setPassprtIssuDate("01-01-2010");
			adult.setBirthDay("01-01-1995");
			adult.setGender("Male");
			adult.setNationality("Christmas Island");
			adult.setPassportIssuCountry("Christmas Island");
			adult.setPhoneType("HOME");
			adultlist.add(adult);
		}
		resvFillobj.setAdult(adultlist);
		
		for(int c = 0; c<Noofchildren; c++)
		{
			System.out.println("INFO -> child : "+c+1);
			childfname = "DanS";
			childsurname = "WinS";
			childfname = childfname.concat(name[c]);
			childsurname = childsurname.concat(name[c]);
			Traveler child = new Traveler();
			child.setPassengertypeCode("CHD");
			child.setNamePrefixTitle("Mst");
			child.setGivenName(childfname);
			child.setSurname(childsurname);
			child.setPhoneNumber("2444555");
			child.setPassportNo("1111111111111");
			child.setPassportExpDate("01-01-2020");
			child.setPassprtIssuDate("01-01-2010");
			child.setBirthDay("01-01-2008");
			child.setGender("Male");
			child.setNationality("Christmas Island");
			child.setPassportIssuCountry("Christmas Island");
			child.setPhoneType("HOME");
			childrenlist.add(child);
		}
		resvFillobj.setChildren(childrenlist);
		
		for(int c = 0; c<Noofinfants; c++)
		{
			System.out.println("INFO -> infant : "+c+1);
			infantfname = "DanI";
			infantsurname = "WinI";
			infantfname = infantfname.concat(name[c]);
			infantsurname = infantsurname.concat(name[c]);
			Traveler infant = new Traveler();
			infant.setPassengertypeCode("INF");
			infant.setNamePrefixTitle("Mst");
			infant.setGivenName(infantfname);
			infant.setSurname(infantsurname);
			infant.setBirthDay("01-01-2014");
			infant.setGender("Male");
			infant.setNationality("Christmas Island");
			infant.setPassportNo("1111111111111");
			infantlist.add(infant);
		}
		resvFillobj.setInfant(infantlist);
		
		System.out.println("SET FILLING PASSENGER DETAILS END");
		return resvFillobj;
	}

}
