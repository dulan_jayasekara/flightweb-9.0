package com.utilities;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ReportGenerator 
{
	public StringBuffer generateReportsBegin()
	{
		StringBuffer ReportPrinter	= null;
		ReportPrinter        		= new StringBuffer();
		SimpleDateFormat sdf		= new SimpleDateFormat("MM/dd/yyyy");
		
		ReportPrinter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
		ReportPrinter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Flight Reservation/Web("+sdf.format(Calendar.getInstance().getTime())+")</p></div>");
		ReportPrinter.append("<body>");
		
		return ReportPrinter;
	}
	
	public void generateReportsEnd(StringBuffer ReportPrinter, String path)
	{
		ReportPrinter.append("</body></html>");
		BufferedWriter bwr;
		try 
		{
			bwr = new BufferedWriter(new FileWriter(new File(path)));
			bwr.write(ReportPrinter.toString());
			bwr.flush();
			bwr.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	
}
