/*Sanoj*/
package com.utilities;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import system.supportParents.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class ExchangeRateUpdater 
{
	
	private HashMap<String, String>  CurrencyMap = new HashMap<String, String>();
	private boolean                  Validator   = true;
	HashMap<String, String> 		 PropertyMap = new HashMap<String, String>();
	SupportMethods 					 SUP 		 = null;

	
	public Map<String, String> getExchangeRates(HashMap<String, String> PropMap, WebDriver driver )throws IOException
	{   
		System.out.println("INFO -> EXCHANGE RATE READ START");
		String scenarioCommonPath = "";
		String scenarioFailedPath = "";
		scenarioCommonPath = ScreenshotPathSingleton.getInstance().getScenarioCommonPath();
		scenarioFailedPath = ScreenshotPathSingleton.getInstance().getScenarioFailedPath();
		
		PropertyMap = PropMap;
		SUP 		= new SupportMethods(PropertyMap);
		
	    driver.get(PropertyMap.get("Portal.Url").concat("/admin/setup/CurrencyExchangeRateSetupPage.do"));
		Screenshot.takeScreenshot(scenarioCommonPath + "/Currency Table.jpg", driver);
	
	    for(int count = 0;Validator;count++)
	    {
	    	try 
	    	{
	    		String Key   = driver.findElement(By.name("currencyCode["+count+"]")).getAttribute("value");
	    		String Value = driver.findElement(By.name("exchangeRate["+count+"]")).getAttribute("value");
	    		CurrencyMap.put(Key, Value);
	    		
			} 
	    	catch (Exception e) 
	    	{	
				Validator = false;
				Screenshot.takeScreenshot(scenarioFailedPath + "/Currency Table.jpg", driver);	
				System.out.println(e.toString());
			}
	    	
	    }
	    System.out.println("INFO -> EXCHANGE RATE READ END");
		return CurrencyMap;
	}
	

}

