package Results.Validate;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;

import Flight.Cancellation.CancellationReport;
import Flight.Cancellation.CancellationRequest;
import Flight.Cancellation.CancellationResponse;
import Flight.Cancellation.CancellationScreen;
import system.classes.*;
import Flight.Fare.FareRequest;
import Flight.Fare.FareResponse;
import Flight.PNR.PNR_Request;
import Flight.PNR.PNR_Response;
import Flight.Reports.BookingConfReportReader;
import Flight.Reports.BookingListReport;
import Flight.Reports.CustomerVoucherEmail;
import Flight.Reports.ProfitAndLoss;
import Flight.Reports.SupplierPayableReport;
import Flight.Reservation.ReservationRequest;
import Flight.Reservation.ReservationResponse;
import system.enumtypes.*;
import Flight.eTicket.EticketRequest;
import Flight.eTicket.EticketResponse;

import com.common.Validators.CommonValidator;
import com.utilities.ExchangeRateUpdater;
import system.supportParents.*;
import system.pages.*;

public class ValidateCC {

	String									Propertypath		= "Properties.properties";
	HashMap<String, String>					PropertyMap			= new HashMap<String, String>();
	HashMap<String, String>					CountryMap			= new HashMap<String, String>();
	private Map<String, String>				CurrencyMap			= new HashMap<String, String>();
	SupportMethods							SUP					= null;

	HashMap<Integer, Map<Integer,String>>	resultlist			= new HashMap<Integer, Map<Integer,String>>();
	int										Reportcount			= 1;
	double									profitval			= 0;
	double									profitpercentage	= 0;
	DecimalFormat 							df 					= new DecimalFormat("#.00");	
	
	boolean									statusPassFail		= false;
	
	
	
	public ValidateCC(HashMap<String, String> PropMap, WebDriver driver)
	{
		PropertyMap = PropMap;
		
		SUP = new SupportMethods(PropertyMap);
		ExchangeRateUpdater rates = new ExchangeRateUpdater();
		try 
		{
			CurrencyMap = rates.getExchangeRates(PropertyMap, driver);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}

	}
	
	public StringBuffer validateFareRequest(SearchObject Sobj, FareRequest Reqobj, StringBuffer ReportPrinter)
	{
		boolean twoway = false;
		if(Sobj.getTriptype().equalsIgnoreCase("Round Trip"))
		{
			twoway = true;
		}
		String reqorig1date		= "";
		String ReqOrigin1Date	= "";
		/*--------------------------------------------------------------------------------------------------------------------------*/
		if(!Reqobj.getOriginDates1().equalsIgnoreCase(""))
		{
			reqorig1date = Reqobj.getOriginDates1().trim();
			String[] reqorig1dateARR = reqorig1date.split("-");
			ReqOrigin1Date = reqorig1dateARR[1].concat("/").concat(reqorig1dateARR[2]).concat("/").concat(reqorig1dateARR[0]);		
		}
		
		String sobjdeparturedate = Sobj.getDepartureDate();
		statusPassFail = CommonValidator.compareStringContains(sobjdeparturedate, ReqOrigin1Date, "Check Departure Date", ReportPrinter, Reportcount);
		Reportcount++;
		
		/*--------------------------------------------------------------------------------------------------------------------------*/
		String reqorig2date = "";
		String ReqOrigin2Date = "";
		if(!Reqobj.getOriginDates2().equalsIgnoreCase(""))
		{
			reqorig2date = Reqobj.getOriginDates2().trim();
			String[] reqorig2dateARR = reqorig2date.split("-");
			ReqOrigin2Date = reqorig2dateARR[1].concat("/").concat(reqorig2dateARR[2]).concat("/").concat(reqorig2dateARR[0]);
		}
		
		if(twoway)
		{
			String sobjreturndate = Sobj.getReturnDate();
			statusPassFail = CommonValidator.compareStringContains(sobjreturndate, ReqOrigin2Date, "Check Arrival Date", ReportPrinter, Reportcount);
			Reportcount++;
		}
		
		/*--------------------------------------------------------------------------------------------------------------------------*/
		String SobjOrigin1 = Sobj.getFrom().split("\\|")[1];	
		statusPassFail = CommonValidator.compareStringContains(SobjOrigin1, Reqobj.getOrigin1(), "Check Leave From", ReportPrinter, Reportcount);
		Reportcount++;	
		
		/*--------------------------------------------------------------------------------------------------------------------------*/
			
		String SobjDepart1 = Sobj.getTo().split("\\|")[1];	
		statusPassFail = CommonValidator.compareStringContains(SobjDepart1, Reqobj.getDestination1(), "Check Leave To", ReportPrinter, Reportcount);
		Reportcount++;
		
		/*--------------------------------------------------------------------------------------------------------------------------*/
		
		String SobjOrigin2 = Sobj.getTo().split("\\|")[1];
		if(twoway)
		{
			statusPassFail = CommonValidator.compareStringContains(SobjOrigin2, Reqobj.getOrigin2(), "Check Return From", ReportPrinter, Reportcount);
			Reportcount++;
			
			/*--------------------------------------------------------------------------------------------------------------------------*/
			
			String SobjDepart2 = Sobj.getFrom().split("\\|")[1];
			statusPassFail = CommonValidator.compareStringContains(SobjDepart2, Reqobj.getDestination2(), "Check Return To", ReportPrinter, Reportcount);
			Reportcount++;
		}
		
		statusPassFail = CommonValidator.compareStringContains(Sobj.getCabinClass(), Reqobj.getFareclass(), "Cabin/Fare class", ReportPrinter, Reportcount);
		Reportcount++;	
		
		statusPassFail = CommonValidator.compareStringContains(String.valueOf(Integer.parseInt(Sobj.getAdult()) + Integer.parseInt(Sobj.getChildren())), Reqobj.getSeatsRequired(), "No. of Seats required", ReportPrinter, Reportcount);
		Reportcount++;
		
		/*----------------------------------------------------------------------------------------------------------------------------*/
			
		if(!Sobj.getPreferredAirline().equals("NONE"))
		{
			statusPassFail = CommonValidator.compareStringContains(Sobj.getPreferredAirline(), Reqobj.getPrefFlight(), "Preferred Airline Booking", ReportPrinter, Reportcount);
			Reportcount++;
		}			
			/*--------------------------------------------------------------------------------------------------------------------------*/
		String Infant   = "";
		String Adults   = "";
		String Children = "";
		Adults			= Reqobj.getAirTravelers().get("ADT");
		Children		= Reqobj.getAirTravelers().get("CHD");
		Infant			= Reqobj.getAirTravelers().get("INF");
			
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>No. of Adult Passengers</td>"
		+ "<td>"+Sobj.getAdult()+"</td>");	
		if(Adults==null)
		{	
			ReportPrinter.append("<td>"+Adults+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}
		else if(Adults.equals(Sobj.getAdult()))
		{
			ReportPrinter.append("<td>"+Adults+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
		}
			
		try
		{
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>No. of Children </td>"
			+ "<td>"+Sobj.getChildren()+"</td>");	
			if( (!Sobj.getChildren().equals("0")) && (Children!=null) )
			{
				if(Children.equals(Sobj.getChildren()))
				{
					ReportPrinter.append("<td>"+Children+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
				}
				else
				{
					ReportPrinter.append("<td>"+Children+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
				}
			}
			else if(Children==null && Sobj.getChildren().equals("0") )
			{
				ReportPrinter.append("<td>"+Children+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else if(Children==null && !Sobj.getChildren().equals("0"))
			{
				ReportPrinter.append("<td>"+Children+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			else if(Children!=null && Sobj.getChildren().equals("0"))
			{
				ReportPrinter.append("<td>"+Children+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();	
		}
			
		try
		{
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>No. of Infants </td>"
			+ "<td>"+Sobj.getInfant()+"</td>");	
			if( (!Sobj.getInfant().equals("0")) && (Infant!=null) )
			{
				if(Infant.equals(Sobj.getInfant()))
				{
					ReportPrinter.append("<td>"+Infant+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
				}
				else
				{
					ReportPrinter.append("<td>"+Infant+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
				}
			}
			else if(Infant==null && Sobj.getInfant().equals("0") )
			{
				ReportPrinter.append("<td>"+Infant+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else if(Infant==null && !Sobj.getInfant().equals("0"))
			{
				ReportPrinter.append("<td>"+Infant+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			else if(Infant!=null && Sobj.getInfant().equals("0"))
			{
				ReportPrinter.append("<td>"+Infant+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
			
		/*-----------------------------------------------------------------------------------------------------------------------------*/	
		
		return ReportPrinter;
	}
	
	
	public StringBuffer validateFareResponse(ArrayList<UIFlightItineraryCC> list, FareResponse Resobj, SearchObject Sobj, AirConfig conf, StringBuffer ReportPrinter)
	{
		try
		{
			ArrayList<UIFlightItineraryCC> UIFlightItineraryCC = list;
			ArrayList<XMLPriceItinerary> XMLpriceItinerary = Resobj.getList();
			
			if(true/*UIFlightItineraryCC.size() != XMLpriceItinerary.size()*/)
			{
				for(int i = 0; i<UIFlightItineraryCC.size(); i++)
				{
					ReportPrinter.append("<tr><td>Results Page Validation - Flight Itinerary "+(i+1)+"</td></tr>");
					
					UIFlightItineraryCC UI = UIFlightItineraryCC.get(i);
					XMLPriceItinerary XML = XMLpriceItinerary.get(i);
					resval(Sobj,UI,XML,conf,ReportPrinter);
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return ReportPrinter;
	}
	
	
	public StringBuffer resval(SearchObject Sobj, UIFlightItineraryCC UIO, XMLPriceItinerary XMLO,AirConfig conf, StringBuffer ReportPrinter )
	{
		boolean twoway = false;
		String direction = "";
		if(Sobj.getTriptype().equals("Round Trip"))
		{
			twoway = true;
			direction = "Circle";
		}
		if(Sobj.getTriptype().equals("One Way Trip"))
		{
			twoway = false;
			direction = "OneWay";
		}
		/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<TRIP TYPE VALIDATION*/
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>Low fare Response Direction Type and Searching Trip Type should match</td>"
		+ "<td>"+Sobj.getTriptype()+"</td>");
		if( (Sobj.getTriptype().equals("Round Trip")) && (XMLO.getDirectiontype().equals("Circle")) )
		{
			ReportPrinter.append("<td>"+XMLO.getDirectiontype()+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
		}
		else if( (Sobj.getTriptype().equals("One Way Trip")) && (XMLO.getDirectiontype().equals("OneWay")) )
		{
			ReportPrinter.append("<td>"+XMLO.getDirectiontype()+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
		}
		else
		{
			ReportPrinter.append("<td>"+XMLO.getDirectiontype()+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}

		/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<JOURNEY DURATION VALIDATION*/
		String uileaveduration = UIO.getLeaveduration().split(" ")[0].split("h")[0].concat(":").concat(UIO.getLeaveduration().split(" ")[1].split("m")[0]);
		String uireturnduration = "";
		if(twoway)
		{
			uireturnduration = UIO.getReturnduration().split(" ")[0].split("h")[0].concat(":").concat(UIO.getReturnduration().split(" ")[1]).split("m")[0];
		}
		
		String[] duration = XMLO.getJournyDuration().split(" ");
		String xmlleavedur = XMLO.getJournyDuration().split(" ")[0];
		String xmlreturndur = "";
		if(duration.length>2)
		{
			int h = duration.length/2;
			xmlreturndur = XMLO.getJournyDuration().split(" ")[h];
		}
		else
		{
			xmlreturndur = XMLO.getJournyDuration().split(" ")[1];
		}
		
		
		if(Sobj.getTriptype().equalsIgnoreCase("One Way Trip"))
		{
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>No.of Inbound flights should be null for oneway trip</td>"
			+ "<td>"+Sobj.getTriptype()+"</td>");
			try
			{
				if(UIO.getInbound() == null)
				{
					ReportPrinter.append("<td>"+UIO.getInbound()+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
				}
				else
				{
					ReportPrinter.append("<td>"+UIO.getInbound().getInBflightlist().size()+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			statusPassFail = CommonValidator.compareStringEqualsIgnoreCase(xmlleavedur, uileaveduration, "Outbound duration", ReportPrinter, Reportcount);
			Reportcount++;
		}
		else if( Sobj.getTriptype().equalsIgnoreCase("Round Trip") )
		{
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>No.of Inbound flights should NOT BE NULL for Twoway trip</td>"
			+ "<td>Direction type : "+direction+"</td>");
			try
			{
				if(!UIO.getInbound().equals(null))
				{
					ReportPrinter.append("<td>"+UIO.getInbound().getInBflightlist().size()+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
				}
				else
				{
					ReportPrinter.append("<td>"+UIO.getInbound().getInBflightlist().size()+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			statusPassFail = CommonValidator.compareStringEquals(xmlreturndur, uireturnduration, "Inbound duration", ReportPrinter, Reportcount);
			Reportcount++;
			
			statusPassFail = CommonValidator.compareStringEquals(xmlleavedur, uileaveduration, "Outbound duration", ReportPrinter, Reportcount);
			Reportcount++;
			
		}

		/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<COST VALIDATION*/
		XMLPriceInfo x = new XMLPriceInfo();
		
		if(XMLO.getPricinginfo().getPricingSource().trim().equalsIgnoreCase("Published"))
		{
			if(conf.getPubFareType().equals(ConfigFareType.Profit_Markup))
			{
				x = XMLcurrencyValidator(UIO.getCurrencyCode(), XMLO.getPricinginfo(), Sobj.getProfitType(), Sobj.getProfit());
			}
			else if(conf.getPubFareType().equals(ConfigFareType.Both))
			{
				x = XMLcurrencyValidator(UIO.getCurrencyCode(), XMLO.getPricinginfo(), Sobj.getProfitType(), Sobj.getProfit());
			}
			else if(conf.getPubFareType().equals(ConfigFareType.None))
			{
				x = XMLcurrencyValidator(UIO.getCurrencyCode(), XMLO.getPricinginfo(), "VALUE", 0);
			}
		}
		else if(XMLO.getPricinginfo().getPricingSource().trim().equalsIgnoreCase("Private"))
		{
			if(conf.getPubFareType().equals(ConfigFareType.Profit_Markup))
			{
				x = XMLcurrencyValidator(UIO.getCurrencyCode(), XMLO.getPricinginfo(), Sobj.getProfitType(), Sobj.getProfit());
			}
			else if(conf.getPubFareType().equals(ConfigFareType.Both))
			{
				x = XMLcurrencyValidator(UIO.getCurrencyCode(), XMLO.getPricinginfo(), Sobj.getProfitType(), Sobj.getProfit());
			}
		}
		
		double Sellingcost = x.getSellingCost();
		double UISellingCost = Double.parseDouble(UIO.getCost());
		statusPassFail = CommonValidator.compareDouble(Sellingcost, UISellingCost, "Selling Cost", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		/*while(true)
		{
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Selling Cost equal</td>"
			+ "<td>"+Sellingcost+"</td>");
			
			if(Sellingcost == UISellingCost)
			{
				ReportPrinter.append("<td>"+UISellingCost+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				break;
			}
			else
			{
				ReportPrinter.append("<td>"+UISellingCost+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Selling Cost equals within +3 and -3 price range</td>"
			+ "<td>"+Sellingcost+"</td>");
			if((Sellingcost+3) > UISellingCost && (Sellingcost-3) < UISellingCost)
			{
				ReportPrinter.append("<td>"+UISellingCost+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				break;
			}
			else
			{
				ReportPrinter.append("<td>"+UISellingCost+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			break;
		}*/
		
		/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<DEPARTURE DATE ARRIVAL DATE VALIDATION*/
		try 
		{
			ArrayList<Flight> UIoutflights = UIO.getOutbound().getOutBflightlist();
			ArrayList<Flight> XMLoutflights = XMLO.getOriginoptions().get(0).getFlightlist();
			
			ArrayList<Flight> UIinflights = new ArrayList<Flight>();
			ArrayList<Flight> XMLinflights = new ArrayList<Flight>();
			if(twoway)
			{
				UIinflights = UIO.getInbound().getInBflightlist();
				XMLinflights = XMLO.getOriginoptions().get(1).getFlightlist();
			}
			
			Date departing;
			String XMLSdeparting = "";
			
			try
			{
				departing = new SimpleDateFormat("yyyy-MM-dd").parse(XMLoutflights.get(0).getDepartureDate());
				XMLSdeparting = new SimpleDateFormat("dd-MMM-yyyy").format(departing);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			
			Date returning;
			String XMLSreturning1 = "";
			String XMLSreturning2 = "";
			Calendar cal = Calendar.getInstance();
			
			if(Sobj.getTriptype().equalsIgnoreCase("Round Trip"))
			{
				try
				{
					returning = new SimpleDateFormat("yyyy-MM-dd").parse(XMLinflights.get(0).getDepartureDate());
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
					XMLSreturning1 = sdf.format(returning);
					cal.setTime(returning);
					cal.add(Calendar.DATE, 1);
					XMLSreturning2 = sdf.format(cal.getTime());
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			
			/*DEPARTURE AND ARRIVAL DATE VALIDATION*/
			statusPassFail = CommonValidator.compareStringEquals(XMLSdeparting, UIO.getDeparting(), "Check Departure Date", ReportPrinter, Reportcount);
			Reportcount++;
			
			
			if(twoway)
			{
				@SuppressWarnings("unused")
				String returndate = "";
				if(XMLSreturning1.equals(XMLSreturning2))
				{
					returndate = XMLSreturning2;
				}
				else
				{
					returndate = XMLSreturning1;
				}
				
				statusPassFail = CommonValidator.compareStringEquals(XMLSreturning1, UIO.getReturning().trim(), "Check Return Date", ReportPrinter, Reportcount);
				Reportcount++;
				/*statusPassFail = CommonValidator.compareStringEquals(XMLSreturning2, UIO.getReturning().trim(), "Check Return Date", ReportPrinter, Reportcount);
				Reportcount++;*/
			}
				
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>No. of outbound flights</td>"
			+ "<td>"+XMLoutflights.size()+"</td>");
			if(UIoutflights.size()==XMLoutflights.size())
			{
				ReportPrinter.append("<td>"+UIoutflights.size()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				
				for(int i =0; i<UIoutflights.size(); i++)
				{
					Flight ui = new Flight();
					Flight xml = new Flight();
					ui = UIoutflights.get(i);
					xml = XMLoutflights.get(i);
						
					/*OUTBOUND FLIGHT DEPARTURE AIRPORT VALIDATION*/
					
					statusPassFail = CommonValidator.compareStringContains(xml.getDeparture_port(), ui.getDeparture_port(), "Outbound flight "+(i+1)+" departure port", ReportPrinter, Reportcount);
					Reportcount++;
					
					/*OUTBOUND FLIGHT DEPARTURE DATE VALIDATION*/
					statusPassFail = CommonValidator.compareStringEquals(xml.getDepartureDate(), ui.getDepartureDate(), "Outbound flight "+(i+1)+" Departure Date", ReportPrinter, Reportcount);
					Reportcount++;
					
					/*OUTBOUND FLIGHT DEPARTURE LOCATION CODE VALIDATION*/
					statusPassFail = CommonValidator.compareStringContains(xml.getDepartureLocationCode(), ui.getDepartureLocationCode(), "Outbound flight "+(i+1)+" Departure Location", ReportPrinter, Reportcount);
					Reportcount++;
					
					/*OUTBOUND FLIGHT DEPARTURE TIME VALIDATION*/
					statusPassFail = CommonValidator.compareStringEquals(xml.getDepartureTime(), ui.getDepartureTime().split("h")[0], "Outbound flight "+(i+1)+" Departure Time", ReportPrinter, Reportcount);
					Reportcount++;

					/*OUTBOUND FLIGHT ARRIVAL AIRPORT VALIDATION*/
					statusPassFail = CommonValidator.compareStringContains(xml.getArrival_port(), ui.getArrival_port(), "Outbound flight "+(i+1)+" Arrival Port", ReportPrinter, Reportcount);
					Reportcount++;
					
					/*OUTBOUND FLIGHT ARRIVAL DATE VALIDATION*/
					statusPassFail = CommonValidator.compareStringEquals(xml.getArrivalDate(), ui.getArrivalDate(), "Outbound flight "+(i+1)+" Arrival Date", ReportPrinter, Reportcount);
					Reportcount++;
					
					/*OUTBOUND FLIGHT ARRIVAL LOCATION CODE VALIDATION*/
					statusPassFail = CommonValidator.compareStringEquals(xml.getArrivalLocationCode(), ui.getArrivalLocationCode(), "Outbound flight "+(i+1)+" Arrival Location", ReportPrinter, Reportcount);
					Reportcount++;
					
					/*OUTBOUND FLIGHT ARRIVAL TIME VALIDATION*/
					statusPassFail = CommonValidator.compareStringEquals(xml.getArrivalTime(), ui.getArrivalTime().split("h")[0], "Outbound flight "+(i+1)+" Arrival Time", ReportPrinter, Reportcount);
					Reportcount++;
					
					/*OUTBOUND FLIGHT NUMBER VALIDATION*/
					statusPassFail = CommonValidator.compareStringContains(xml.getFlightNo(), ui.getFlightNo(), "Outbound flight "+(i+1)+" Flight No.", ReportPrinter, Reportcount);
					Reportcount++;
					
					/*OUTBOUND FLIGHT MARKETING AIRLINE NAME VALIDATION*/
					statusPassFail = CommonValidator.compareStringContains(xml.getMarketingAirline(), ui.getMarketingAirline(), "Outbound flight "+(i+1)+" Marketing Airline", ReportPrinter, Reportcount);
					Reportcount++;
					
					if(!Sobj.getPreferredAirline().equalsIgnoreCase("NONE"))
					{
						/*OUTBOUND FLIGHT PREFERRED AIRLINE NAME VALIDATION*/
						statusPassFail = CommonValidator.compareStringContains(Sobj.getPreferredAirline(), xml.getOperatingAirlineCode(), "Outbound flight "+(i+1)+" Preferred Airline", ReportPrinter, Reportcount);
						Reportcount++;
					}	
				}
			}
			else
			{
				ReportPrinter.append("<td>"+UIoutflights.size()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
				
			if(Sobj.getTriptype().equals("Round Trip"))
			{
				ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
				+ "<td>No. of inbound flights</td>"
				+ "<td>"+XMLinflights.size()+"</td>");
				if(UIinflights.size()==XMLinflights.size())
				{
					ReportPrinter.append("<td>"+UIinflights.size()+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
					
					for(int i =0; i<UIinflights.size(); i++)
					{
						Flight ui = new Flight();
						Flight xml = new Flight();
						ui = UIinflights.get(i);
						xml = XMLinflights.get(i);
							
						/*INBOUND FLIGHT DEPARTURE AIRPORT VALIDATION*/
						statusPassFail = CommonValidator.compareStringContains(xml.getDeparture_port(), ui.getDeparture_port(), "Inbound flight "+(i+1)+" departure port", ReportPrinter, Reportcount);
						Reportcount++;
						
						/*INBOUND FLIGHT DEPARTURE DATE VALIDATION*/
						statusPassFail = CommonValidator.compareStringEquals(xml.getDepartureDate(), ui.getDepartureDate(), "Inbound flight "+(i+1)+" Departure Date", ReportPrinter, Reportcount);
						Reportcount++;
						
						/*INBOUND FLIGHT DEPARTURE LOCATION CODE VALIDATION*/
						statusPassFail = CommonValidator.compareStringContains(xml.getDepartureLocationCode(), ui.getDepartureLocationCode(), "Inbound flight "+(i+1)+" Departure Location", ReportPrinter, Reportcount);
						Reportcount++;
						
						/*INBOUND FLIGHT DEPARTURE TIME VALIDATION*/
						statusPassFail = CommonValidator.compareStringEquals(xml.getDepartureTime(), ui.getDepartureTime().split("h")[0], "Inbound flight "+(i+1)+" Departure Time", ReportPrinter, Reportcount);
						Reportcount++;
						
						/*INBOUND FLIGHT ARRIVAL AIRPORT VALIDATION*/
						statusPassFail = CommonValidator.compareStringContains(xml.getArrival_port(), ui.getArrival_port(), "Inbound flight "+(i+1)+" Arrival Port", ReportPrinter, Reportcount);
						Reportcount++;
						
						/*INBOUND FLIGHT ARRIVAL DATE VALIDATION*/
						statusPassFail = CommonValidator.compareStringEquals(xml.getArrivalDate(), ui.getArrivalDate(), "Inbound flight "+(i+1)+" Arrival Date", ReportPrinter, Reportcount);
						Reportcount++;
						
						/*INBOUND FLIGHT ARRIVAL LOCATION CODE VALIDATION*/
						statusPassFail = CommonValidator.compareStringEquals(xml.getArrivalLocationCode(), ui.getArrivalLocationCode(), "Inbound flight "+(i+1)+" Arrival Location", ReportPrinter, Reportcount);
						Reportcount++;
						
						/*INBOUND FLIGHT ARRIVAL TIME VALIDATION*/
						statusPassFail = CommonValidator.compareStringEquals(xml.getArrivalTime(), ui.getArrivalTime().split("h")[0], "Inbound flight "+(i+1)+" Arrival Time", ReportPrinter, Reportcount);
						Reportcount++;
						
						/*INBOUND FLIGHT NUMBER VALIDATION*/
						statusPassFail = CommonValidator.compareStringContains(xml.getFlightNo(), ui.getFlightNo(), "Inbound flight "+(i+1)+" Flight No.", ReportPrinter, Reportcount);
						Reportcount++;
						
						/*INBOUND FLIGHT MARKETING AIRLINE NAME VALIDATION*/
						statusPassFail = CommonValidator.compareStringContains(xml.getMarketingAirline(), ui.getMarketingAirline(), "Inbound flight "+(i+1)+" Marketing Airline", ReportPrinter, Reportcount);
						Reportcount++;
						
						/*INBOUND FLIGHT PREFERRED AIRLINE NAME VALIDATION*/
						if(!Sobj.getPreferredAirline().equalsIgnoreCase("NONE"))
						{
							statusPassFail = CommonValidator.compareStringContains(Sobj.getPreferredAirline(), xml.getOperatingAirlineCode(), "Inbound flight "+(i+1)+" Preferred Airline", ReportPrinter, Reportcount);
							Reportcount++;
						}	
					}
				}
				else
				{
					ReportPrinter.append("<td>"+UIinflights.size()+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
				}
			}
		} 	
		catch (Exception e) 
		{
			e.printStackTrace();
		}	
		
		return ReportPrinter;	
	}	
		
	public XMLPriceInfo XMLcurrencyValidator(String CurrencyCode, XMLPriceInfo XMLO, String type, double val)
	{
		String basefare = XMLO.getBasefareAmount();
		String taxfare	= XMLO.getTaxAmount();
		
		double Dbasefare = Double.parseDouble(basefare);
		double Dtaxfare = Double.parseDouble(taxfare);
		String profittype = type/*Sobj.getProfitType();*/;	
		double profitval =  val/*Sobj.getProfit();*/;
		double PortalCost = 0;
		double SellingCost = 0;
		double newbase = 0;
		double tax = 0;
		double profit = 0;

		try {
			
			
		if(profittype.toUpperCase().equalsIgnoreCase("VALUE"))
		{
			if(XMLO.getBasefareCurrencyCode().equalsIgnoreCase(PropertyMap.get("Portal_Currency_Code")))
			{
				PortalCost = Dbasefare;
				
				if(PropertyMap.get("Portal_Currency_Code").equalsIgnoreCase(CurrencyCode))
				{
					newbase = PortalCost;
					profit = profitval;
					tax = Dtaxfare;
					SellingCost = PortalCost + profitval + Dtaxfare; 
					//System.out.println(SellingCost);
				}
				else
				{
					//System.out.println(CurrencyMap.get(CurrencyCode));
					double Cost = PortalCost * Double.parseDouble(CurrencyMap.get(CurrencyCode));
					tax = Dtaxfare * Double.parseDouble(CurrencyMap.get(CurrencyCode));
					profitval = profitval * Double.parseDouble(CurrencyMap.get(CurrencyCode));
					newbase = Cost;
					profit = profitval;
					SellingCost = Cost + profitval + tax; // Cost + profit + tax
					//System.out.println(SellingCost);
				}
			}
			else
			{
				double PCost = Dbasefare * Double.parseDouble(CurrencyMap.get(XMLO.getBasefareCurrencyCode()));
				tax = Dtaxfare * Double.parseDouble(CurrencyMap.get(XMLO.getBasefareCurrencyCode()));
				/*profitval = profitval * */
				//System.out.println(PCost);
				
				if(PropertyMap.get("Portal_Currency_Code").equalsIgnoreCase(CurrencyCode))
				{
					newbase = PCost;
					profit = profitval;
					SellingCost = PCost + profitval + tax; // PCost + profit + tax  
					//System.out.println(SellingCost);
				}
				else
				{
					//System.out.println(CurrencyMap.get(UIO.getCurrencyCode()));
					double SCost = PCost * Double.parseDouble(CurrencyMap.get(CurrencyCode));
					tax = Dtaxfare * Double.parseDouble(CurrencyMap.get(CurrencyCode));
					profitval = profitval * Double.parseDouble(CurrencyMap.get(CurrencyCode));
					newbase = SCost;
					profit = profitval;
					SellingCost = SCost + profitval + tax; //Scost + profit + tax
					//System.out.println(SellingCost);
				}
			}
		
		}
		if(profittype.toUpperCase().equalsIgnoreCase("PERCENTAGE"))
		{
			if(XMLO.getBasefareCurrencyCode().equalsIgnoreCase(PropertyMap.get("Portal_Currency_Code")))
			{
				PortalCost = Dbasefare;
				
				if(PropertyMap.get("Portal_Currency_Code").equalsIgnoreCase(CurrencyCode))
				{
					newbase = PortalCost;
					profit = (PortalCost * (profitval/100));
					tax = Dtaxfare;
					SellingCost = PortalCost + profit + Dtaxfare; // Basefare + profit + tax; 
				}
				else
				{
					double Cost = PortalCost * Double.parseDouble(CurrencyMap.get(CurrencyCode)); //Basefare * mapvalue
					tax = Dtaxfare * Double.parseDouble(CurrencyMap.get(CurrencyCode));
					newbase = Cost;
					profit = (Cost * (profitval/100));
					SellingCost = Cost + profit + tax; // Cost + profit + tax
				}
			}
			else
			{
				double PCost = Dbasefare * Double.parseDouble(CurrencyMap.get(XMLO.getBasefareCurrencyCode()));
				tax = Dtaxfare * Double.parseDouble(CurrencyMap.get(XMLO.getBasefareCurrencyCode()));
				
				if(PropertyMap.get("Portal_Currency_Code").equalsIgnoreCase(CurrencyCode))
				{
					newbase = PCost;
					profit = (PCost * (profitval/100));
					SellingCost = PCost + profit + tax; // PCost + profit + tax 
				}
				else
				{
					double SCost = PCost * Double.parseDouble(CurrencyMap.get(CurrencyCode));
					newbase = SCost;
					profit = (SCost * (profitval/100));
					SellingCost = SCost + profit + tax; //Scost + profit + tax
				}
			}
		
		}
		
		SellingCost = Math.ceil(SellingCost);
		newbase = Math.ceil(newbase);
		tax = Math.ceil(tax);
		profit = Math.ceil(profit);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//System.out.println("Selling Cost : "+SellingCost);
		XMLO.setNewbase(newbase);
		//System.out.println(newbase);
		XMLO.setNewtax(tax);
		//System.out.println(tax);
		XMLO.setProfit(profit);
		//System.out.println(profit);
		XMLO.setSellingCost(SellingCost);
		//System.out.println(newbase+tax+profit);
		//System.out.println(SellingCost);
		XMLO.setNewBasefareCurrencyCode(CurrencyCode);
		
		return XMLO;
	}
	
	
	public boolean Ispricechanged(XMLPriceInfo PriceResponsePrice, XMLPriceInfo SelectFlightPrice)
	{
		boolean change = true;
		if(PriceResponsePrice.getTotalFareCurrencyCode().equals(SelectFlightPrice.getTotalFareCurrencyCode()))
		{
			if(Double.parseDouble(PriceResponsePrice.getTotalFare()) == Double.parseDouble(SelectFlightPrice.getTotalFare()))
			{
				change = false;
			}
		}
		else
		{
			double pricetot = 0;
			try
			{
				pricetot = Double.parseDouble(PriceResponsePrice.getTotalFare()) * Double.parseDouble((CurrencyMap.get(SelectFlightPrice.getTotalFareCurrencyCode())));
			}
			catch(Exception e)
			{
				e.printStackTrace();
			} 
			
			if( pricetot== Double.parseDouble(SelectFlightPrice.getTotalFare()))
			{
				change = false;
			}
		}
		
		return change;
	}
	
	
	public double convertToGatewayValue(String SellCurrency, double SellValue)
	{
		double Portalvalue = 0;
		double Creditvalue = 0;
		double percentage = Double.parseDouble(PropertyMap.get("CreditCard_Cost_Percentage"))/100;
		
		if(SellCurrency.equals(PropertyMap.get("Portal_Currency_Code")))
		{
			Portalvalue = SellValue;
			
			if(PropertyMap.get("Portal_Currency_Code").equals(PropertyMap.get("Payment_Gateway_Currency_Code")))
			{
				Creditvalue = Portalvalue + (Portalvalue * percentage);
			}
			else
			{
				double portaltemp = Portalvalue * Double.parseDouble(CurrencyMap.get(PropertyMap.get("Payment_Gateway_Currency_Code")));
				Creditvalue = portaltemp + (portaltemp * percentage);
			}
		}
		
		else
		{
			double ToPortal = Math.ceil(SellValue / Double.parseDouble(CurrencyMap.get(SellCurrency)));
			if(PropertyMap.get("Portal_Currency_Code").equals(PropertyMap.get("Payment_Gateway_Currency_Code")))
			{
				Creditvalue = ToPortal + (ToPortal * percentage);
			}
			else
			{
				double portaltemp = ToPortal * Double.parseDouble(CurrencyMap.get(PropertyMap.get("Payment_Gateway_Currency_Code")));
				Creditvalue = portaltemp + (portaltemp * percentage);
			}
		}	
		return Creditvalue;
	}
	
	
	public XMLPriceInfo addBookingFee(String CurrencyCode, XMLPriceInfo XMLO, String type, double val)
	{
		double sellingcost = XMLO.getSellingCost();
		double bookingfee = 0;
		
		if(type.toUpperCase().equals("VALUE"))
		{
			if(PropertyMap.get("Portal_Currency_Code").equals(XMLO.getNewBasefareCurrencyCode()) )
			{
				bookingfee = val;
				sellingcost = sellingcost + bookingfee;
			}
			
			else
			{
				bookingfee = (val * Double.parseDouble(CurrencyMap.get(CurrencyCode)));
				sellingcost = sellingcost + bookingfee;
			}
		}
		if(type.toUpperCase().equals("PERCENTAGE"))
		{
			if(PropertyMap.get("Portal_Currency_Code").equals(XMLO.getNewBasefareCurrencyCode()) )
			{
				bookingfee = (XMLO.getNewbase() + XMLO.getProfit()) * (val/100);
				sellingcost = sellingcost + bookingfee;
			}
			
			else
			{
				bookingfee = (val * Double.parseDouble(CurrencyMap.get(CurrencyCode)));
				sellingcost = sellingcost + bookingfee;
			}
		}
		XMLO.setSellingCost(sellingcost);
		XMLO.setBookingfee(bookingfee);
		
		return XMLO;
	}
	
	
	public XMLPriceItinerary getNewPrice(String currencyCode, XMLPriceInfo PriceResPrice, XMLPriceItinerary XMLSelectFlight, SearchObject Sobj, AirConfig conf)
	{
		XMLPriceInfo newPrice = new XMLPriceInfo();
		
		if(XMLSelectFlight.getPricinginfo().getPricingSource().trim().equalsIgnoreCase("Published"))
		{
			if(conf.getPubFareType().equals(ConfigFareType.Profit_Markup))
			{
				newPrice = XMLcurrencyValidator(Sobj.getSellingCurrency(), PriceResPrice, Sobj.getProfitType(), Sobj.getProfit() );
				newPrice = addBookingFee(currencyCode, newPrice, "VALUE", 0);
			}
			else if(conf.getPubFareType().equals(ConfigFareType.Booking_Fee))
			{
				newPrice = XMLcurrencyValidator(Sobj.getSellingCurrency(), PriceResPrice, "VALUE", 0 );
				newPrice = addBookingFee(currencyCode, newPrice, Sobj.getBookingFeeType(), Sobj.getBookingFee());
			}
			else if(conf.getPubFareType().equals(ConfigFareType.Both))
			{
				newPrice = XMLcurrencyValidator(Sobj.getSellingCurrency(), PriceResPrice, Sobj.getProfitType(), Sobj.getProfit() );
				newPrice = addBookingFee(currencyCode, newPrice, Sobj.getBookingFeeType(), Sobj.getBookingFee());
			}
			else if(conf.getPubFareType().equals(ConfigFareType.None))
			{
				newPrice = XMLcurrencyValidator(Sobj.getSellingCurrency(), PriceResPrice, "VALUE", 0 );
				newPrice = addBookingFee(currencyCode, newPrice, "VALUE", 0);
			}
		}
		else if(XMLSelectFlight.getPricinginfo().getPricingSource().trim().equalsIgnoreCase("Private"))
		{
			if(conf.getPubFareType().equals(ConfigFareType.Profit_Markup))
			{
				newPrice = XMLcurrencyValidator(Sobj.getSellingCurrency(), PriceResPrice, Sobj.getProfitType(), Sobj.getProfit() );
			}
			else if(conf.getPubFareType().equals(ConfigFareType.Booking_Fee))
			{
				newPrice = XMLcurrencyValidator(Sobj.getSellingCurrency(), PriceResPrice, "VALUE", 0 );
				newPrice = addBookingFee(currencyCode, newPrice, Sobj.getBookingFeeType(), Sobj.getBookingFee());
			}
			else if(conf.getPubFareType().equals(ConfigFareType.Both))
			{
				newPrice = XMLcurrencyValidator(Sobj.getSellingCurrency(), PriceResPrice, Sobj.getProfitType(), Sobj.getProfit() );
				newPrice = addBookingFee(currencyCode, newPrice, Sobj.getBookingFeeType(), Sobj.getBookingFee());
			}
			/*else if(conf.getPubFareType().equals(ConfigFareType.None))
			{
				newPrice = XMLcurrencyValidator(Sobj.getSellingCurrency(), PriceResPrice, "VALUE", 0 );
				//newPrice = addBookingFee(currencyCode, newPrice, Sobj.getBookingFeeType(), Sobj.getBookingFee());
			}*/
		}
		//newPrice = XMLcurrencyValidator(Sobj.getSellingCurrency(), PriceResPrice, Sobj.getProfitType(), Sobj.getProfit() );
		//newPrice = addBookingFee(currencyCode, newPrice, Sobj.getBookingFeeType(), Sobj.getBookingFee());
		XMLSelectFlight.setPricinginfo(newPrice);
		return XMLSelectFlight;
	}

	
	/*public StringBuffer validateCart(CartFlight cartflight, XMLPriceInfo PriceResPrice, XMLPriceItinerary XMLSelectFlight, SearchObject Sobj, AirConfig conf, StringBuffer ReportPrinter )
	{
		boolean twoway = false;
		
		int inbXML = 0;
		if( (XMLSelectFlight.getDirectiontype().equals("Circle")) || (Sobj.getTriptype().equals("Round Trip")))
		{
			twoway = true;
			inbXML = XMLSelectFlight.getOriginoptions().get(1).getFlightlist().size();
		}

		try
		{
			Date cartdeparture = new SimpleDateFormat("dd-MMM-yyyy").parse(cartflight.getLeaveDate());
			String cartSdeparture = new SimpleDateFormat("yyyy-MM-dd").format(cartdeparture);
			String XMLdeparturetime = XMLSelectFlight.getOriginoptions().get(0).getFlightlist().get(0).getDepartureTime().replaceAll(":", "");
			//System.out.println(XMLdeparturetime);
			String cartdeparturetime = cartflight.getLeaveTime().replace("H", "00");
			//System.out.println(cartdeparturetime);
			String cartSreturn = "";
			String XMLreturntime= "";
			String cartreturntime = "";
			
			if(twoway)
			{
				Date cartreturn = new SimpleDateFormat("dd-MMM-yyyy").parse(cartflight.getReturnDate());
				cartSreturn = new SimpleDateFormat("yyyy-MM-dd").format(cartreturn);
				//System.out.println(cartSdeparture);
				//System.out.println(cartSreturn);
				XMLreturntime = XMLSelectFlight.getOriginoptions().get(1).getFlightlist().get(inbXML-1).getArrivalTime().replaceAll(":", "");
				//System.out.println(XMLreturntime);
				cartreturntime = cartflight.getReturnTime().replace("H", "00");
				//System.out.println(cartreturntime);
			}
			
			ReportPrinter = CommonValidator.compareStringEquals(Sobj.getSellingCurrency(), cartflight.getCurrencyCode(), "Check Selling Currency", ReportPrinter, Reportcount);
			Reportcount++;
			
			int outbound = XMLSelectFlight.getOriginoptions().get(0).getFlightlist().size();
			int inbound  = 0;
			if(XMLSelectFlight.getDirectiontype().equals("Circle"))
			{
				inbound  = XMLSelectFlight.getOriginoptions().get(1).getFlightlist().size();
			}
				
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////
			Outbound ob = new Outbound();
			ArrayList<Flight> outFlist = new ArrayList<Flight>();
			for(int out = 0; out<outbound; out++)
			{
				Flight flight = new Flight();
				flight.setMarketingAirline(cartflight.getFlights().get(0).split("[(]")[0].trim());
				//System.out.println(cartflight.getFlights().get(0).split("[()]")[1].trim());
				flight.setFlightNo(cartflight.getFlights().get(0).split("[()]")[1].trim());
				//System.out.println(cartflight.getFlights().get(0).split("[()]")[1].trim());
				flight.setMarketingAirline_Loc_Code(cartflight.getFlights().get(0).split("[()]")[1].trim().split(" ")[0]);
				outFlist.add(flight);
				cartflight.getFlights().remove(0);
				cartflight.getFlights().removeAll(Collections.singleton(null));
			}
			ob.setOutBflightlist(outFlist);
			cartflight.setOutbound(ob);
				
				
			Inbound inb = new Inbound();
			ArrayList<Flight> inFlist = new ArrayList<Flight>();	
			for(int in = 0; in<inbound; in++)
			{
				Flight flight = new Flight();
				flight.setMarketingAirline(cartflight.getFlights().get(0).split("[(]")[0].trim());
				flight.setFlightNo(cartflight.getFlights().get(0).split("[()]")[1].trim());
				flight.setMarketingAirline_Loc_Code(cartflight.getFlights().get(0).split("[()]")[1].trim().split(" ")[0]);
				inFlist.add(flight);
				cartflight.getFlights().remove(0);
				cartflight.getFlights().removeAll(Collections.singleton(null));
			}
			inb.setInBflightlist(inFlist);
			cartflight.setInbound(inb);
			//////////////////////////////////////////////////////////////////////////////////////////////////
			
			ArrayList<Flight> XMLoutboundFlights = XMLSelectFlight.getOriginoptions().get(0).getFlightlist();
			ArrayList<Flight> XMLinboundFlights = new ArrayList<Flight>();
				
			if(XMLSelectFlight.getDirectiontype().equals("Circle"))
			{
				XMLinboundFlights = XMLSelectFlight.getOriginoptions().get(1).getFlightlist();
			}
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Match No. of Outbound Flights</td>"
			+ "<td>Equal</td>");
			if((outbound == cartflight.getOutbound().getOutBflightlist().size()) && (inbound == cartflight.getInbound().getInBflightlist().size()))
			{
				ReportPrinter.append("<td>XML Outbound"+outbound+" Cart Outbound"+cartflight.getOutbound().getOutBflightlist().size()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				
				ReportPrinter = CommonValidator.compareStringContains(XMLoutboundFlights.get(0).getDeparture_port().replace(",",""), cartflight.getLeaveAirport().replace(",",""), "Check Departure Airport", ReportPrinter, inbound);
				Reportcount++;
				
				//departure date validation
				ReportPrinter = CommonValidator.compareStringEquals(XMLoutboundFlights.get(0).getDepartureDate(), cartSdeparture, "Check Departure Date", ReportPrinter, Reportcount);
				Reportcount++;
				
				//departure time validation
				ReportPrinter = CommonValidator.compareStringEquals(XMLdeparturetime, cartdeparturetime, "Check Departure Time", ReportPrinter, Reportcount);
				Reportcount++;
				
			}
			else
			{
				ReportPrinter.append("<td>Not Equal XML outbound "+outbound+" Cart outbound "+cartflight.getOutbound().getOutBflightlist().size()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
			
			if(XMLSelectFlight.getDirectiontype().equals("Circle"))
			{
				ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
				+ "<td>Match No. of Inbound Flights</td>"
				+ "<td>Equal</td>");
				if((inbound == cartflight.getInbound().getInBflightlist().size()))
				{
					ReportPrinter.append("<td>XML Inbound "+outbound+" Cart Inbound "+cartflight.getOutbound().getOutBflightlist().size()+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
					
					//destination port validation
					int size = XMLinboundFlights.size();
					ReportPrinter = CommonValidator.compareStringContains(XMLinboundFlights.get(0).getDeparture_port().replace(",",""), cartflight.getReturnAirport().replace(",",""), "Check Return Airport", ReportPrinter, Reportcount);
					Reportcount++;
				
					//Arrival date validation
					ReportPrinter = CommonValidator.compareStringEquals(XMLinboundFlights.get(size-1).getArrivalDate(), cartSreturn, "Check Arrival Date", ReportPrinter, Reportcount);
					Reportcount++;
					
					//reach time
					ReportPrinter = CommonValidator.compareStringEquals(XMLreturntime, cartreturntime, "Check Arrival Time", ReportPrinter, Reportcount);
					Reportcount++;
				}
				else
				{
					ReportPrinter.append("<td>Not Equal..! XML Inbound"+outbound+" Cart Inbound"+cartflight.getOutbound().getOutBflightlist().size()+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
					Reportcount++;	
				}
				
			}
				
			//VALIDATING SUBTOTAL, TOTAL TAXES AND OTHER CHARGES, AMOUNT BEING PROCESSED NOW. AMOUNT PROCESSED BY AIRLINE, FINAL TOTAL
			double cartSubTotal1		 = Double.parseDouble(cartflight.getSubtotal1());
			double cartTaxAndOther1		 = Double.parseDouble(cartflight.getTaxesandOther1());
			double cartTotalforFlight	 = Double.parseDouble(cartflight.getTotalforflight1());
			
			double cartfinalSubTotal	 = Double.parseDouble(cartflight.getSubtotalFinal());
			double cartfinalTaxAndOther  = Double.parseDouble(cartflight.getTaxesandOtherFinal());
			double cartamountProcessNow  = Double.parseDouble(cartflight.getAmountprocessnowFinal());
			double cartFinalBookingValue = Double.parseDouble(cartflight.getFinalvalue());
			//double cartamountProcessAirline = Double.parseDouble(cartflight.getAmountbyairline());
			
			
			double XMLSubtotal1			 = XMLSelectFlight.getPricinginfo().getNewbase() + XMLSelectFlight.getPricinginfo().getProfit();
			double XMLTaxAndOther1		 = XMLSelectFlight.getPricinginfo().getNewtax() + XMLSelectFlight.getPricinginfo().getBookingfee();
			double XMLTotalforFlight	 = XMLSelectFlight.getPricinginfo().getNewbase() + XMLSelectFlight.getPricinginfo().getProfit() + XMLSelectFlight.getPricinginfo().getNewtax() + XMLSelectFlight.getPricinginfo().getBookingfee();
			
			double XMLfinalSubTotal		 = XMLSelectFlight.getPricinginfo().getNewbase() + XMLSelectFlight.getPricinginfo().getProfit();
			double XMLfinalTaxAndOther	 = XMLSelectFlight.getPricinginfo().getNewtax() + XMLSelectFlight.getPricinginfo().getBookingfee();
			double XMLprocessNow		 = 0;
			double XMLFinalBookingValue  = XMLSelectFlight.getPricinginfo().getNewbase() + XMLSelectFlight.getPricinginfo().getProfit() + XMLSelectFlight.getPricinginfo().getNewtax() + XMLSelectFlight.getPricinginfo().getBookingfee();
			
			if( conf.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Full_Amount_at_Booking") )
			{
				XMLprocessNow = (XMLSelectFlight.getPricinginfo().getNewbase() + XMLSelectFlight.getPricinginfo().getProfit() + XMLSelectFlight.getPricinginfo().getNewtax() + XMLSelectFlight.getPricinginfo().getBookingfee() );
				XMLSelectFlight.getPricinginfo().setProcessnow(XMLprocessNow);
			}
			else if( conf.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Booking_Fee_as_Deposit") )
			{
				XMLprocessNow = XMLSelectFlight.getPricinginfo().getBookingfee();
				XMLSelectFlight.getPricinginfo().setProcessnow(XMLprocessNow);
			}
			else if( conf.getFlightPayOptCartBooking().equalsIgnoreCase("Pass_to_Air_Line_to_Charge") )
			{
				
			}
			
			//Sub Total
			ReportPrinter = CommonValidator.compareDouble(XMLSubtotal1, cartSubTotal1, "Check Cart Subtotal Per Flight", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
			//Tax and Other
			ReportPrinter = CommonValidator.compareDouble(XMLTaxAndOther1, cartTaxAndOther1, "Check Cart Tax and Other Per Flight", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
				
			//Total for Flight
			ReportPrinter = CommonValidator.compareDouble(XMLTotalforFlight, cartTotalforFlight, "Check Cart flight total", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
				
			//Final Sub Total
			ReportPrinter = CommonValidator.compareDouble(XMLfinalSubTotal, cartfinalSubTotal, "Check Cart Final Subtotal", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
			//Final Tax and Other
			ReportPrinter = CommonValidator.compareDouble(XMLfinalTaxAndOther, cartfinalTaxAndOther, "Check Cart Tax and Other per flight", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
				
			//Amount Process Now
			ReportPrinter = CommonValidator.compareDouble(XMLprocessNow, cartamountProcessNow, "Check Amount being Processed Now", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
			//Final Booking Value
			ReportPrinter = CommonValidator.compareDouble(XMLFinalBookingValue, cartFinalBookingValue, "Check Final Booking Value", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return ReportPrinter;

	}*/
	
	/*public StringBuffer validatePaymentPage(UIPaymentPage uipayment, XMLPriceItinerary XMLSelectFlight, SearchObject Sobj, AirConfig conf, StringBuffer ReportPrinter)
	{
		//System.out.println();
		//System.out.println("----------------------------------------------------------------------------------------------------");
		String xmlcurrencyCode		= XMLSelectFlight.getPricinginfo().getNewBasefareCurrencyCode();
		double xmlnewbase			= XMLSelectFlight.getPricinginfo().getNewbase();
		double xmlnewtax			= XMLSelectFlight.getPricinginfo().getNewtax();
		double xmlprofit			= XMLSelectFlight.getPricinginfo().getProfit();
		double xmlSellingCost		= XMLSelectFlight.getPricinginfo().getSellingCost();
		double xmlbookingFee		= XMLSelectFlight.getPricinginfo().getBookingfee();
		
		String uicurrencyCode		= uipayment.getUisummarypay().getCurrencycode();							
		double uisubtotal			= Double.parseDouble(uipayment.getUisummarypay().getSubtotal());
		double uitaxplusother		= Double.parseDouble(uipayment.getUisummarypay().getTaxplusother());
		double uitotal				= uisubtotal + uitaxplusother;
		double uiamountprocessnw	= Double.parseDouble(uipayment.getUisummarypay().getAmountprocess());
		//double uicreditcardamount =  convertToGatewayValue(xmlcurrencyCode, xmlSellingCost);			//System.out.println("UI credit card amount : "+uicreditcardamount);
		//double uibookingfee = Double.parseDouble(uipayment.getUisummarypay().getBookingfee());		//System.out.println("UI booking fee : "+uibookingfee);
		
		
		if(XMLSelectFlight.getPricinginfo().getPricingSource().trim().equalsIgnoreCase("Published"))
		{
			if(conf.getPubFareType().equals(ConfigFareType.Profit_Markup))
			{
				//x = XMLcurrencyValidator(UIO.getCurrencyCode(), XMLSelectFlight.getPricinginfo(), Sobj.getProfitType(), Sobj.getProfit());
			}
			else if(conf.getPubFareType().equals(ConfigFareType.Both))
			{
				//x = XMLcurrencyValidator(UIO.getCurrencyCode(), XMLO.getPricinginfo(), Sobj.getProfitType(), Sobj.getProfit());
			}
			else if(conf.getPubFareType().equals(ConfigFareType.None))
			{
				//x = XMLcurrencyValidator(UIO.getCurrencyCode(), XMLO.getPricinginfo(), "VALUE", 0);
			}
			
		}
		else if(XMLSelectFlight.getPricinginfo().getPricingSource().trim().equalsIgnoreCase("Private"))
		{
			if(conf.getPubFareType().equals(ConfigFareType.Profit_Markup))
			{
				//x = XMLcurrencyValidator(UIO.getCurrencyCode(), XMLO.getPricinginfo(), Sobj.getProfitType(), Sobj.getProfit());
			}
			else if(conf.getPubFareType().equals(ConfigFareType.Both))
			{
				//x = XMLcurrencyValidator(UIO.getCurrencyCode(), XMLO.getPricinginfo(), Sobj.getProfitType(), Sobj.getProfit());
			}
		}
		
		String sdate = uipayment.getCancellationdate();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		Date date;
		Calendar cal = Calendar.getInstance();
		
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>Check Ticket Time Limit</td>"
		+ "<td>"+XMLSelectFlight.getTicketTimeLimit().split("T")[0]+"</td>");
		try 
		{
			date = sdf.parse(sdate);
			sdate = sdf1.format(date);
			date = sdf1.parse(sdate);
			cal.setTime(date);
			cal.add(Calendar.DATE, 3);
			
			if(XMLSelectFlight.getTicketTimeLimit().split("T")[0].trim().equals(sdf1.format(cal.getTime())))
			{
				ReportPrinter.append("<td>"+sdf.format(cal.getTime())+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+uipayment.getCancellationdate()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
		} 
		catch (ParseException e) 
		{
			e.printStackTrace();
			//System.out.println(sdf.format(cal.getTime()));
			ReportPrinter.append("<td>Parse Exception.! Cancellation date is : "+uipayment.getCancellationdate()+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}
		
		if(xmlcurrencyCode.equals(uicurrencyCode))
		{
			ReportPrinter = CommonValidator.compareDouble((xmlnewbase + xmlprofit), uisubtotal, "Check Payment page subtotal", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
			//Validating payment page taxes plus other
			double creditcardamount = xmlSellingCost * (Double.parseDouble(PropertyMap.get("CreditCard_Cost_Percentage"))/100);
			ReportPrinter = CommonValidator.compareDouble((xmlnewtax + xmlbookingFee + creditcardamount), uitaxplusother, "Check Payment page Tax plus Other", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
			//Validating payment page total
			//double creditcard = xmlSellingCost * (Double.parseDouble(PropertyMap.get("CreditCard_Cost_Percentage"))/100);
			ReportPrinter = CommonValidator.compareDouble((xmlSellingCost + creditcardamount), uitotal, "Check Payment page Total Amount", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
			ReportPrinter = CommonValidator.compareDouble(xmlSellingCost+creditcardamount, uiamountprocessnw, "Check Payment Page Amount Process Now", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
		}
		
		return ReportPrinter;
	}*/
	
	public StringBuffer validateResvRequest(ReservationRequest resvReq, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, StringBuffer ReportPrinter)
	{
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>Request XML Available</td>"
		+ "<td>"+resvReq.isAvailable()+"</td>");
		if(resvReq.isAvailable())
		{
			ReportPrinter.append("<td>"+resvReq.isAvailable()+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
			
			boolean twoway = false;
			if(resvReq.getDirection().equals("Circle"))
			{
				twoway = true;
			}
			
			Traveler Fillmaincus = fillingObject.getMaincustomer();
			Address mainCusAddress = Fillmaincus.getAddress();
			///////////////////////////////////////////////////////////////////////////////
			statusPassFail = CommonValidator.compareStringContains(mainCusAddress.getAddressStreetNo(), resvReq.getDeliveryStreetNo(), "Direct customer address Street No", ReportPrinter, Reportcount);
			Reportcount++;
			
			///////////////////////////////////////////////////////////////////////////////
			statusPassFail = CommonValidator.compareStringContains(mainCusAddress.getAddressCity(), resvReq.getDeliveryCityName(), "Direct customer address City", ReportPrinter, Reportcount);
			Reportcount++;
			
			/////////////////////////////////////////////////////////////////////////////////
			statusPassFail = CommonValidator.compareStringContains(mainCusAddress.getPostalCode(), resvReq.getDeliveryPostal(), "Direct customer address PostalCode", ReportPrinter, Reportcount);
			Reportcount++;
			
			//////////////////////////////////////////////////////////////////////////////////
			statusPassFail = CommonValidator.compareStringContains(mainCusAddress.getAddressCountry(), resvReq.getDeliveryCountryName(), "Direct customer address Country", ReportPrinter, Reportcount);
			Reportcount++;
			
			//////////////////////////////////////////////////////////////////////////////////
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Direct customer address State Code</td>"
			+ "<td>"+mainCusAddress.getStateProv()+"</td>");
			if(  (resvReq.getDeliveryStateCode().equals("-")) && (mainCusAddress.getStateProv().equals("")) )
			{
				ReportPrinter.append("<td>"+resvReq.getDeliveryStateCode()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else if(resvReq.getDeliveryStateCode().equals(mainCusAddress.getStateProv()))
			{
				ReportPrinter.append("<td>"+resvReq.getDeliveryStateCode()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+resvReq.getDeliveryStateCode()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			////////////////////////////////////////////////////////////////////////////////////
			
			String direction = "";
			if(twoway)
			{
				direction = "Circle";
			}
			else
			{
				direction = "OneWay";
			}
			statusPassFail = CommonValidator.compareStringEqualsIgnoreCase(direction, resvReq.getDirection(), "Flight direction type", ReportPrinter, Reportcount);
			Reportcount++;
			///////////////////////////////////////////////////////////////////////////////////////
			
			//Origin options
			int outinResReq = resvReq.getOriginDestinationInfo().get(0).getFlightlist().size();
			int ininResReq = 0;
			if(twoway)
			{
				ininResReq = resvReq.getOriginDestinationInfo().get(1).getFlightlist().size();
			}
			////////////////////////////////////////////////////////////////////////////////////////
			
			statusPassFail = CommonValidator.compareInt(XMLSelectFlight.getOriginoptions().get(0).getFlightlist().size(), outinResReq, "No. of Outbound flights", ReportPrinter, Reportcount);
			Reportcount++;
			if( (outinResReq==XMLSelectFlight.getOriginoptions().get(0).getFlightlist().size()) )
			{
				ArrayList<Flight> list11 = resvReq.getOriginDestinationInfo().get(0).getFlightlist();
				ArrayList<Flight> list12 = XMLSelectFlight.getOriginoptions().get(0).getFlightlist();
				ReportPrinter = validateFlights(outinResReq, "Outbound", list11, list12, ReportPrinter );
			}
			
			//////////////////////////////////////////////////////////////////////////////////////////
			if(twoway)
			{
				statusPassFail = CommonValidator.compareInt(XMLSelectFlight.getOriginoptions().get(1).getFlightlist().size(), ininResReq, "No. of Inbound flights", ReportPrinter, Reportcount);
				Reportcount++;
				if(ininResReq==XMLSelectFlight.getOriginoptions().get(1).getFlightlist().size() )
				{
					ArrayList<Flight> resreqlist21 = resvReq.getOriginDestinationInfo().get(1).getFlightlist();
					ArrayList<Flight> xmllist22 = XMLSelectFlight.getOriginoptions().get(1).getFlightlist();
					ReportPrinter = validateFlights(ininResReq, "Inbound", resreqlist21, xmllist22, ReportPrinter);
				}
			}
			
			//Validate Passenger details
			ArrayList<Traveler> onelist		= resvReq.getPNR().getPassengers();
			ArrayList<Traveler> ReqAdult	= new ArrayList<Traveler>();
			ArrayList<Traveler> ReqChild	= new ArrayList<Traveler>();
			ArrayList<Traveler> ReqInfant	= new ArrayList<Traveler>();
			
			for (int i=0; i<onelist.size(); i++) 
			{
				Traveler traveler = onelist.get(i);
				
				//NEW CODE SPECIAL SERVICE TAG INFO
				//P-LKA-1111111111111-LKA-01JAN95-M-01JAN20-adfone-adsone-H
				XMLSpecialService service	= resvReq.getSpServiceRequests().get(i);
				String[] array				= service.getTextArray();
				traveler.setPassportIssuCountry(array[1]);
				traveler.setPassportNo(array[2]);
				traveler.setNationality(array[3]);
				traveler.setGender(array[5]);
				
				try
				{
					Date expire			= new SimpleDateFormat("ddMMMyy").parse(array[6]);
					String strexpire	= new SimpleDateFormat("dd-MM-yyyy").format(expire);
					traveler.setPassportExpDate(strexpire);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				
				if(traveler.getPassengertypeCode().equals(Passengers.ADT.toString()))
				{
					try
					{
						Date bdate			= new SimpleDateFormat("ddMMMyy").parse(array[4]);
						String strbdate		= new SimpleDateFormat("dd-MM-yyyy").format(bdate);
						traveler.setBirthDay(strbdate);
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
					
					ReqAdult.add(traveler);
				}
				if(traveler.getPassengertypeCode().equals(Passengers.CHD.toString()))
				{
					ReqChild.add(traveler);
				}
				if(traveler.getPassengertypeCode().equals(Passengers.INF.toString()))
				{
					ReqInfant.add(traveler);
				}
			}
			Traveler aT = null;
			Traveler bT = null;
			////////////////////////////////////////////////////////////////////////////////////////
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>No. of Adults in Request</td>"
			+ "<td>"+fillingObject.getAdult().size()+"</td>");
			if(ReqAdult.size()==fillingObject.getAdult().size())
			{
				ReportPrinter.append("<td>"+ReqAdult.size()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				for(int a=0; a<ReqAdult.size(); a++)
				{
					 aT = ReqAdult.get(a);
					 bT = fillingObject.getAdult().get(a);
					 ReportPrinter = validatePassengers(aT, bT, ReportPrinter);
				}
			}
			else
			{
				ReportPrinter.append("<td>"+ReqAdult.size()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			///////////////////////////////////////////////////////////////////////////////////////////
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>No. of Children in Request</td>"
			+ "<td>"+fillingObject.getChildren().size()+"</td>");
			if(ReqChild.size()==fillingObject.getChildren().size())
			{
				ReportPrinter.append("<td>"+ReqChild.size()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				for(int c=0; c<ReqChild.size(); c++)
				{
					 aT = ReqChild.get(c);
					 bT = fillingObject.getChildren().get(c);
					 ReportPrinter = validatePassengers(aT, bT, ReportPrinter);
				}
			}
			else
			{
				ReportPrinter.append("<td>"+ReqChild.size()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
			////////////////////////////////////////////////////////////////////////////////////////////
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>No. of Infants in Request</td>"
			+ "<td>"+fillingObject.getInfant().size()+"</td>");
			if(ReqInfant.size()==fillingObject.getInfant().size())
			{
				ReportPrinter.append("<td>"+ReqInfant.size()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				for(int i=0; i<ReqInfant.size(); i++)
				{
					 aT = ReqInfant.get(i);
					 bT = fillingObject.getInfant().get(i);
					 ReportPrinter = validatePassengers(aT, bT, ReportPrinter);
				}
			}
			else
			{
				ReportPrinter.append("<td>"+ReqInfant.size()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
		}
		else
		{
			ReportPrinter.append("<td>"+resvReq.isAvailable()+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}
		
		return ReportPrinter;
		
	}
	
	
	private StringBuffer validatePassengers(Traveler aT, Traveler bT, StringBuffer ReportPrinter)
	{
		statusPassFail = CommonValidator.compareStringEqualsIgnoreCase(bT.getGivenName().trim(), aT.getGivenName().trim(), "Passenger Given Name", ReportPrinter, Reportcount);
		Reportcount++;
		////////////////////////////////////////////////////////////////////////////////////
		statusPassFail = CommonValidator.compareStringEqualsIgnoreCase(bT.getSurname().trim(), aT.getSurname().trim(), "Passenger Surname", ReportPrinter, Reportcount);
		Reportcount++;
		////////////////////////////////////////////////////////////////////////////////////
		if(!aT.getPassengertypeCode().equals(Passengers.INF.toString()))
		{
			statusPassFail = CommonValidator.compareStringEqualsIgnoreCase(bT.getNamePrefixTitle().trim(), aT.getNamePrefixTitle().trim(), "Passenger Title", ReportPrinter, Reportcount);
			Reportcount++;
			//////////////////////////////////////////////////////////
			
			//statusPassFail = CommonValidator.compareStringContains(bT.getPhoneNumber().trim(), aT.getPhoneNumber().trim(), "Passenger Phone Number", ReportPrinter, Reportcount);
			//Reportcount++;
			
			statusPassFail = CommonValidator.compareStringContains(bT.getPassportNo().trim(), aT.getPassportNo().trim(), "Passenger Passport No", ReportPrinter, Reportcount);
			Reportcount++;
			//////////////////////////////////////////////////////////
			
			statusPassFail = CommonValidator.compareStringContains(bT.getPassprtIssuDate().trim(), aT.getPassprtIssuDate().trim(), "Passenger Passport Issued Date", ReportPrinter, Reportcount);
			Reportcount++;
			////////////////////////////////////////////////////////////
			
			statusPassFail = CommonValidator.compareStringContains(bT.getPassportExpDate().trim(), aT.getPassportExpDate().trim(), "Passenger Passport Expire Date", ReportPrinter, Reportcount);
			Reportcount++;
			//////////////////////////////////////////////////////////////////
			
			/*statusPassFail = CommonValidator.compareStringContains(bT.getPassportIssuCountry(), aT.getNationality(), "Passenger Passport Issued Country", ReportPrinter, Reportcount);
			Reportcount++;XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
			/////////////////////////////////////////////////////////////
			
			statusPassFail = CommonValidator.compareStringContains(bT.getGender(), aT.getGender(), "Passenger Gender", ReportPrinter, Reportcount);
			Reportcount++;
			//////////////////////////////////////////////////////////////////////////////////
		}
		
		/////////////////////////////////////////////////////////////////
		
		statusPassFail = CommonValidator.compareStringContains(bT.getBirthDay().trim(), aT.getBirthDay().trim(), "Passenger Birth Day", ReportPrinter, Reportcount);
		Reportcount++;
		
		return ReportPrinter;
	}
	
	
	private StringBuffer validateFlights(int noofflights, String direction, ArrayList<Flight> actualList, ArrayList<Flight> expectedList, StringBuffer ReportPrinter) 
	{
		for(int out=0; out<noofflights; out++)
		{
			Flight Actual = actualList.get(out);
			Flight Expected = expectedList.get(out);
			System.out.println();
			/*--------------------------------------DEPARTURE DATE---------------------------------------*/
			statusPassFail = CommonValidator.compareStringContains(Expected.getDepartureDate().trim(), Actual.getDepartureDate().trim(), ""+direction+" Flight "+(out+1)+" Departure Date", ReportPrinter, Reportcount);
			Reportcount++;
			
			/*------------------------------------DEPARTURE LOCATION-------------------------------------*/
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>"+direction+" Flight "+(out+1)+" Departure Port Location</td>"
			+ "<td>Location Code : "+Expected.getDepartureLocationCode()+"<br>Location port : "+Expected.getDeparture_port()+"</td>");
			
			if(Actual.getDepartureLocationCode().trim().contains(Expected.getDepartureLocationCode().trim()) || Expected.getDepartureLocationCode().trim().contains(Actual.getDepartureLocationCode().trim()))
			{
				ReportPrinter.append("<td>Location Code : "+Actual.getDepartureLocationCode()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else if( Actual.getDeparture_port().trim().contains(Expected.getDeparture_port().trim()) || Expected.getDeparture_port().trim().contains(Actual.getDeparture_port().trim()) )
			{
				ReportPrinter.append("<td>Location port : "+Actual.getDeparture_port()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else if( Actual.getDeparture_port().trim().contains(Expected.getDepartureLocationCode().trim()) || Expected.getDepartureLocationCode().trim().contains(Actual.getDeparture_port().trim()) )
			{
				ReportPrinter.append("<td>Location port : "+Actual.getDeparture_port()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else if( Actual.getDepartureLocationCode().trim().contains(Expected.getDeparture_port().trim()) || Expected.getDeparture_port().trim().contains(Actual.getDepartureLocationCode().trim()) )
			{
				ReportPrinter.append("<td>Location port : "+Actual.getDeparture_port()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>Location Code : "+Actual.getDepartureLocationCode()+"<br>Location port : "+Actual.getDeparture_port()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
			
			/*---------------------------------------------DEPARTURE TIME---------------------------------------*/
			//statusPassFail = CommonValidator.compareStringContains(Expected.getDepartureTime().trim(), Actual.getDepartureTime().trim(), ""+direction+" Flight "+(out+1)+" Departure Time", ReportPrinter, Reportcount);
			//Reportcount++;
			try {
				String a = CommonValidator.formatTimeToCommon(Expected.getDepartureTime().trim());
				String b = CommonValidator.formatTimeToCommon(Actual.getDepartureTime().trim());
				statusPassFail = CommonValidator.compareStringContains(a, b, ""+direction+" Flight "+(out+1)+" Departure Time", ReportPrinter, Reportcount);
				Reportcount++;
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			/*----------------------------------------------ARRIVAL DATE----------------------------------------*/
			statusPassFail = CommonValidator.compareStringContains(Expected.getArrivalDate().trim(), Actual.getArrivalDate().trim(), ""+direction+" Flight "+(out+1)+" Arrival Date", ReportPrinter, Reportcount);
			Reportcount++;
			
			/*--------------------------------------------ARRIVAL LOCATION--------------------------------------*/
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>"+direction+" Flight "+(out+1)+" Arrival Location</td>"
			+ "<td>Location Code : "+Expected.getArrivalLocationCode()+"<br>Location port : "+Expected.getArrival_port()+"</td>");
			if(Actual.getArrivalLocationCode().trim().contains(Expected.getArrivalLocationCode().trim()) || Expected.getArrivalLocationCode().trim().contains(Actual.getArrivalLocationCode().trim()) )
			{
				ReportPrinter.append("<td>Location Code : "+Actual.getArrivalLocationCode()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else if( Actual.getArrival_port().contains(Expected.getArrival_port()) || Expected.getArrival_port().contains(Actual.getArrival_port()) )
			{
				ReportPrinter.append("<td>Location port : "+Actual.getArrival_port()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else if( Actual.getArrival_port().contains(Expected.getArrivalLocationCode()) || Expected.getArrivalLocationCode().contains(Actual.getArrival_port()) )
			{
				ReportPrinter.append("<td>Location port : "+Actual.getArrival_port()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else if( Actual.getArrivalLocationCode().contains(Expected.getArrival_port()) || Expected.getArrival_port().contains(Actual.getArrivalLocationCode()) )
			{
				ReportPrinter.append("<td>Location port : "+Actual.getArrival_port()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>Location Code : "+Actual.getArrivalLocationCode()+"<br>Location port : "+Actual.getArrival_port()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
			/*-------------------------------------------ARRIVAL TIME----------------------------------------*/
			//statusPassFail = CommonValidator.compareStringContains(Expected.getArrivalTime().trim(), Actual.getArrivalTime().trim(), ""+direction+" Flight "+(out+1)+" Arrival Time", ReportPrinter, Reportcount);
			//Reportcount++;
			try {
				String aa = CommonValidator.formatTimeToCommon(Expected.getArrivalTime().trim());
				String bb = CommonValidator.formatTimeToCommon(Actual.getArrivalTime().trim());
				statusPassFail = CommonValidator.compareStringContains(aa, bb, ""+direction+" Flight "+(out+1)+" Arrival Time", ReportPrinter, Reportcount);
				Reportcount++;
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			/*--------------------------------------------FLIGHT NO------------------------------------------*/
			statusPassFail = CommonValidator.compareStringContains(Expected.getFlightNo().trim(), Actual.getFlightNo().trim(), ""+direction+" Flight "+(out+1)+" No", ReportPrinter, Reportcount);
			Reportcount++;
		}
		
		return ReportPrinter;
	}

	
	public StringBuffer validateResvResponse(ReservationResponse resvResp, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, StringBuffer ReportPrinter)
	{
		int xmloutbound = XMLSelectFlight.getOriginoptions().get(0).getFlightlist().size();
		int xmlinbound = 0;
		
		ArrayList<Flight> xmloutbflightlist = new ArrayList<Flight>();
		ArrayList<Flight> xmlinbflightlist = new ArrayList<Flight>();
		
		ArrayList<Flight> respoutbflightlist = new ArrayList<Flight>();
		ArrayList<Flight> respinbflightlist = new ArrayList<Flight>();
	
		xmloutbflightlist = XMLSelectFlight.getOriginoptions().get(0).getFlightlist();
		
		boolean twoway = false;
		if(XMLSelectFlight.getDirectiontype().equals("Circle"))
		{
			twoway = true;
			xmlinbound = XMLSelectFlight.getOriginoptions().get(1).getFlightlist().size();
			xmlinbflightlist = XMLSelectFlight.getOriginoptions().get(1).getFlightlist();	
		}
		
		ArrayList<XMLResvRespItem> xmlitemlist = resvResp.getItems();
		
		for(int i=0; i<xmloutbound; i++)
		{
			respoutbflightlist.add(xmlitemlist.get(0).getFlight());
			xmlitemlist.remove(0);
		}
		if(twoway)
		{
			for(int i=0; i<xmlinbound; i++)
			{
				respinbflightlist.add(xmlitemlist.get(0).getFlight());
				xmlitemlist.remove(0);
			}
		}
		
		ReportPrinter = validateFlights(xmloutbound, "Outbound", respoutbflightlist, xmloutbflightlist, ReportPrinter );
		if(twoway)
		{
			ReportPrinter = validateFlights(xmlinbound, "Inbound", respinbflightlist, xmlinbflightlist, ReportPrinter );
		}
		
		//Validate Passenger details
		ArrayList<Traveler> onelist = resvResp.getCustomers();
		ArrayList<Traveler> ResAdult = new ArrayList<Traveler>();
		ArrayList<Traveler> ResChild = new ArrayList<Traveler>();
		ArrayList<Traveler> ResInfant = new ArrayList<Traveler>();
		
		for (int i=0; i<onelist.size(); i++) 
		{
			Traveler traveler = onelist.get(i);
			//System.out.println(traveler.getPassengertypeCode());
			if(traveler.getPassengertypeCode().equals(Passengers.ADT.toString()))
			{
				ResAdult.add(traveler);
			}
			if(traveler.getPassengertypeCode().equals(Passengers.CHD.toString()))
			{
				ResChild.add(traveler);
			}
			if(traveler.getPassengertypeCode().equals(Passengers.INF.toString()))
			{
				ResInfant.add(traveler);
			}
		}
		Traveler aT = null;
		Traveler bT = null;
		////////////////////////////////////////////////////////////////////////////////////////
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>No. of Adults in Request</td>"
		+ "<td>"+fillingObject.getAdult().size()+"</td>");
		if(ResAdult.size()==fillingObject.getAdult().size())
		{
			ReportPrinter.append("<td>"+ResAdult.size()+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
			for(int a=0; a<ResAdult.size(); a++)
			{
				aT = ResAdult.get(a);
				bT = fillingObject.getAdult().get(a);
				ReportPrinter = validatePassengers(aT, bT, ReportPrinter);
			}
		}
		else
		{
			ReportPrinter.append("<td>"+ResAdult.size()+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}
		///////////////////////////////////////////////////////////////////////////////////////////
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>No. of Children in Request</td>"
		+ "<td>"+fillingObject.getChildren().size()+"</td>");
		if(ResChild.size()==fillingObject.getChildren().size())
		{
			ReportPrinter.append("<td>"+ResChild.size()+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
			for(int c=0; c<ResChild.size(); c++)
			{
				aT = ResChild.get(c);
				bT = fillingObject.getChildren().get(c);
				ReportPrinter = validatePassengers(aT, bT, ReportPrinter);
			}
		}
		else
		{
			ReportPrinter.append("<td>"+ResChild.size()+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}
		////////////////////////////////////////////////////////////////////////////////////////////
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>No. of Infants in Request</td>"
		+ "<td>"+fillingObject.getInfant().size()+"</td>");
		if(ResInfant.size()==fillingObject.getInfant().size())
		{
			ReportPrinter.append("<td>"+ResInfant.size()+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
			for(int i=0; i<ResInfant.size(); i++)
			{
				aT = ResInfant.get(i);
				bT = fillingObject.getInfant().get(i);
				ReportPrinter = validatePassengers(aT, bT, ReportPrinter);
			}
		}
		else
		{
			ReportPrinter.append("<td>"+ResInfant.size()+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}
			
		return ReportPrinter;
		
	}
		
	
    /*public StringBuffer validateConfirmationPage(UIConfirmationPage uiconfirmpage, StringBuffer ReportPrinter, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, ReservationResponse resvResp)
	{
		try
		{	
			int xmloutbound = XMLSelectFlight.getOriginoptions().get(0).getFlightlist().size();
			int xmlinbound  = 0;
			
			ArrayList<Flight> xmloutbflightlist = new ArrayList<Flight>();
			ArrayList<Flight> xmlinbflightlist  = new ArrayList<Flight>();
			
			ArrayList<Flight> confOutbflightlist = new ArrayList<Flight>();
			ArrayList<Flight> confInbflightlist  = new ArrayList<Flight>();
		
			xmloutbflightlist = XMLSelectFlight.getOriginoptions().get(0).getFlightlist();
			confOutbflightlist = uiconfirmpage.getOutbound().getOutBflightlist();
			
			boolean twoway = false;
			if(XMLSelectFlight.getDirectiontype().equals("Circle"))
			{
				twoway = true;
				xmlinbound = XMLSelectFlight.getOriginoptions().get(1).getFlightlist().size();
				xmlinbflightlist = XMLSelectFlight.getOriginoptions().get(1).getFlightlist();
				confInbflightlist = uiconfirmpage.getInbound().getInBflightlist();
			}
			
			ReportPrinter = validateFlights(xmloutbound, "Outbound", confOutbflightlist, xmloutbflightlist, ReportPrinter );
			if(twoway)
			{
				ReportPrinter = validateFlights(xmlinbound, "Inbound", confInbflightlist, xmlinbflightlist, ReportPrinter );
			}
			
			System.out.println();
			Traveler Fillmaincus = new Traveler();
			Traveler Confmaincus = new Traveler();
			Address FillmainCusAddress = new Address();
			Address ConfmainCusAddress = new Address();
			
			
			try
			{
				Fillmaincus = fillingObject.getMaincustomer();
				Confmaincus = uiconfirmpage.getMaincustomer();
				FillmainCusAddress = Fillmaincus.getAddress();
				ConfmainCusAddress = Confmaincus.getAddress();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			///////////////////////////////////////////////////////////////////////////////
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Direct customer's given name</td>"
			+ "<td>"+Fillmaincus.getGivenName()+"</td>");
			if(  Confmaincus.getGivenName().equalsIgnoreCase(Fillmaincus.getGivenName())  )
			{
				ReportPrinter.append("<td>"+Confmaincus.getGivenName()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+Confmaincus.getGivenName()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			/////////////////////////////////////////////////////////////////////////////////
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Direct customer's surname</td>"
			+ "<td>"+Fillmaincus.getSurname()+"</td>");
			if(  Fillmaincus.getSurname().equalsIgnoreCase(Confmaincus.getSurname())  )
			{
				ReportPrinter.append("<td>"+Confmaincus.getSurname()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+Confmaincus.getGivenName()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			//////////////////////////////////////////////////////////////////////////////////
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Direct customer address Street No</td>"
			+ "<td>"+FillmainCusAddress.getAddressStreetNo()+"</td>");
			//System.out.println(ConfmainCusAddress.getAddressStreetNo());
			if(ConfmainCusAddress.getAddressStreetNo().equalsIgnoreCase(FillmainCusAddress.getAddressStreetNo()))
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getAddressStreetNo()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getAddressStreetNo()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			///////////////////////////////////////////////////////////////////////////////
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Direct customer address City</td>"
			+ "<td>"+FillmainCusAddress.getAddressCity()+"</td>");
			if(ConfmainCusAddress.getAddressCity().equalsIgnoreCase(FillmainCusAddress.getAddressCity()))
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getAddressCity()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getAddressCity()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			/////////////////////////////////////////////////////////////////////////////////
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Direct customer address PostalCode</td>"
			+ "<td>"+FillmainCusAddress.getPostalCode()+"</td>");
			if(ConfmainCusAddress.getPostalCode().equalsIgnoreCase(FillmainCusAddress.getPostalCode()))
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getPostalCode()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else if(ConfmainCusAddress.getPostalCode().equals("-") && FillmainCusAddress.getPostalCode().equals(""))
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getPostalCode()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getPostalCode()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			//////////////////////////////////////////////////////////////////////////////////
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Direct customer address Country</td>"
			+ "<td>"+FillmainCusAddress.getAddressCountry()+"</td>");
			if(ConfmainCusAddress.getAddressCountry().equalsIgnoreCase(FillmainCusAddress.getAddressCountry()))
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getAddressCountry()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getAddressCountry()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			//////////////////////////////////////////////////////////////////////////////////
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Direct customer address State Code</td>"
			+ "<td>"+FillmainCusAddress.getStateProv()+"</td>");
			if(ConfmainCusAddress.getStateProv().equalsIgnoreCase(FillmainCusAddress.getStateProv()))
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getStateProv()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else if(ConfmainCusAddress.getStateProv().equals("-") && FillmainCusAddress.getStateProv().equals(""))
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getStateProv()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getStateProv()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return ReportPrinter;

	}
*/	
	
	public StringBuffer validateEticketRequest(EticketRequest eTicketRequest, UIConfirmationPage uiconfirmpage, ReservationResponse resvResp, StringBuffer ReportPrinter)
	{
		//////////////////////////////////////////////////////////////////////////
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>Check Pseudo City Code in Reservation Response with the E-Ticket Pseudo City Code XML must be same</td>"
		+ "<td>"+resvResp.getItineraryRefID_Context()+"</td>");
		if(eTicketRequest.getPseudoCityCode().equals(resvResp.getItineraryRefID_Context()))
		{
			ReportPrinter.append("<td>"+eTicketRequest.getPseudoCityCode()+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
		}
		else
		{
			ReportPrinter.append("<td>"+eTicketRequest.getPseudoCityCode()+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}	
		////////////////////////////////////////////////////////////////////////////
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>Supplier Confirmation Number in Reservation Response and UniqueID sending with the E-Ticket XML must be same</td>"
		+ "<td>"+resvResp.getItineraryRefID()+"</td>");
		if(eTicketRequest.getUniqueID().equals(resvResp.getItineraryRefID()))
		{
			ReportPrinter.append("<td>"+eTicketRequest.getUniqueID()+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
		}
		else
		{
			ReportPrinter.append("<td>"+eTicketRequest.getUniqueID()+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}	
		/////////////////////////////////////////////////////////////////////////////
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>Check Reservation Response Ticket type and E-Ticket XML Ticket type</td>"
		+ "<td>"+resvResp.getTicketType()+"</td>");
		if(eTicketRequest.getTicketType().equals(resvResp.getTicketType()))
		{
			ReportPrinter.append("<td>"+eTicketRequest.getTicketType()+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
		}
		else
		{
			ReportPrinter.append("<td>"+eTicketRequest.getTicketType()+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}
		//////////////////////////////////////////////////////////////////////////////
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>Check OmitInfant contains the value false </td>"
		+ "<td>false</td>");
		if(eTicketRequest.getOmitInfant().equals("false"))
		{
			ReportPrinter.append("<td>"+eTicketRequest.getOmitInfant()+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
		}
		else
		{
			ReportPrinter.append("<td>"+eTicketRequest.getOmitInfant()+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}
		
		return ReportPrinter;
	}
	
	
	public StringBuffer validateEticketResponse(EticketResponse eTicketResponse, EticketRequest eTicketRequest, StringBuffer ReportPrinter)
	{
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>Check E-Ticket Response UniqueID (Supplier Confirmation ID) with E-Ticket Request UniqueID </td>"
		+ "<td>"+eTicketRequest.getUniqueID()+"</td>");
		if( eTicketRequest.getUniqueID().equals(eTicketResponse.getUniqueID()) )
		{
			ReportPrinter.append("<td>"+eTicketResponse.getUniqueID()+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
		}
		else
		{
			ReportPrinter.append("<td>"+eTicketResponse.getUniqueID()+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}
		/////////////////////////////////////////////////////////////////////////////
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>Check E-Ticket request ticket type and E-Ticket response ticket type</td>"
		+ "<td>E-ticket Request ticket type and E-ticket Response ticket type must be same</td>");
		if(  (eTicketRequest.getTicketType().equals("eTicket")) && (eTicketResponse.getTicketType().equals("Electronic"))  )
		{
			ReportPrinter.append("<td>E-ticket Request : "+eTicketRequest.getTicketType()+"<br>E-ticket Response : "+eTicketResponse.getTicketType()+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
		}
		else
		{
			ReportPrinter.append("<td>E-ticket Request : "+eTicketRequest.getTicketType()+"<br>E-ticket Response : "+eTicketResponse.getTicketType()+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}
		//////////////////////////////////////////////////////////////////////////////
		
		return ReportPrinter;
	}

	
	public StringBuffer validatePNRRequest(PNR_Request pnrRequest, ReservationResponse resvResp, StringBuffer ReportPrinter)
	{
		////////////////////////////////////////////////////////////////////////////
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>Supplier Confirmation Number in Reservation Response and UniqueID sending with the E-Ticket XML must be same</td>"
		+ "<td>"+resvResp.getItineraryRefID()+"</td>");
		if(pnrRequest.getUniqueID().equals(resvResp.getItineraryRefID()))
		{
		ReportPrinter.append("<td>"+pnrRequest.getUniqueID()+"</td>"
		+ "<td class='Passed'>PASS</td></tr>");
		Reportcount++;
		}
		else
		{
		ReportPrinter.append("<td>"+pnrRequest.getUniqueID()+"</td>"
		+ "<td class='Failed'>Fail</td></tr>");
		Reportcount++;
		}	
			
		return ReportPrinter;
	}
	
	
	public StringBuffer validatePNRResponse(PNR_Response pnrResponse, ReservationResponse resvResp, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, StringBuffer ReportPrinter)
	{
		//////////////////////////////////////////////////////////////////////////
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>Check Pseudo City Code in Reservation Response with the E-Ticket Pseudo City Code XML must be same</td>"
		+ "<td>"+resvResp.getItineraryRefID_Context()+"</td>");
		if(pnrResponse.getItineraryRefID_Context().equals(resvResp.getItineraryRefID_Context()))
		{
			ReportPrinter.append("<td>"+pnrResponse.getItineraryRefID_Context()+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
		}
		else
		{
			ReportPrinter.append("<td>"+pnrResponse.getItineraryRefID_Context()+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}	
		////////////////////////////////////////////////////////////////////////////
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>Supplier Confirmation Number in Reservation Response and UniqueID sending with the E-Ticket XML must be same</td>"
		+ "<td>"+resvResp.getItineraryRefID()+"</td>");
		if(pnrResponse.getItineraryRefID().equals(resvResp.getItineraryRefID()))
		{
			ReportPrinter.append("<td>"+pnrResponse.getItineraryRefID()+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
		}
		else
		{
			ReportPrinter.append("<td>"+pnrResponse.getItineraryRefID()+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}	
		/////////////////////////////////////////////////////////////////////////////
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>Check Reservation Response Ticket type and E-Ticket XML Ticket type</td>"
		+ "<td>"+resvResp.getTicketType()+"</td>");
		if(pnrResponse.getTicketType().equals(resvResp.getTicketType()))
		{
			ReportPrinter.append("<td>"+pnrResponse.getTicketType()+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
		}
		else
		{
			ReportPrinter.append("<td>"+pnrResponse.getTicketType()+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}
		//////////////////////////////////////////////////////////////////////////////
		
		int xmloutbound = XMLSelectFlight.getOriginoptions().get(0).getFlightlist().size();
		int xmlinbound = 0;
		
		ArrayList<Flight> xmloutbflightlist = new ArrayList<Flight>();
		ArrayList<Flight> xmlinbflightlist = new ArrayList<Flight>();
		
		ArrayList<Flight> respoutbflightlist = new ArrayList<Flight>();
		ArrayList<Flight> respinbflightlist = new ArrayList<Flight>();
	
		xmloutbflightlist = XMLSelectFlight.getOriginoptions().get(0).getFlightlist();
		
		boolean twoway = false;
		if(XMLSelectFlight.getDirectiontype().equals("Circle"))
		{
			twoway = true;
			xmlinbound = XMLSelectFlight.getOriginoptions().get(1).getFlightlist().size();
			xmlinbflightlist = XMLSelectFlight.getOriginoptions().get(1).getFlightlist();	
		}
		
		ArrayList<XMLResvRespItem> xmlitemlist = resvResp.getItems();
		
		for(int i=0; i<xmloutbound; i++)
		{
			respoutbflightlist.add(xmlitemlist.get(0).getFlight());
			xmlitemlist.remove(0);
		}
		if(twoway)
		{
			for(int i=0; i<xmlinbound; i++)
			{
				respinbflightlist.add(xmlitemlist.get(0).getFlight());
				xmlitemlist.remove(0);
			}
		}
		
		ReportPrinter = validateFlights(xmloutbound, "Outbound", respoutbflightlist, xmloutbflightlist, ReportPrinter );
		if(twoway)
		{
			ReportPrinter = validateFlights(xmlinbound, "Inbound", respinbflightlist, xmlinbflightlist, ReportPrinter );
		}
		
		//Validate Passenger details
		ArrayList<Traveler> onelist = resvResp.getCustomers();
		ArrayList<Traveler> ResAdult = new ArrayList<Traveler>();
		ArrayList<Traveler> ResChild = new ArrayList<Traveler>();
		ArrayList<Traveler> ResInfant = new ArrayList<Traveler>();
		
		for (int i=0; i<onelist.size(); i++) 
		{
			Traveler traveler = onelist.get(i);
			//System.out.println(traveler.getPassengertypeCode());
			if(traveler.getPassengertypeCode().equals(Passengers.ADT.toString()))
			{
				ResAdult.add(traveler);
			}
			if(traveler.getPassengertypeCode().equals(Passengers.CHD.toString()))
			{
				ResChild.add(traveler);
			}
			if(traveler.getPassengertypeCode().equals(Passengers.INF.toString()))
			{
				ResInfant.add(traveler);
			}
		}
		Traveler aT = null;
		Traveler bT = null;
		////////////////////////////////////////////////////////////////////////////////////////
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>No. of Adults in Request</td>"
		+ "<td>"+fillingObject.getAdult().size()+"</td>");
		if(ResAdult.size()==fillingObject.getAdult().size())
		{
			ReportPrinter.append("<td>"+ResAdult.size()+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
			for(int a=0; a<ResAdult.size(); a++)
			{
				 aT = ResAdult.get(a);
				 bT = fillingObject.getAdult().get(a);
				 ReportPrinter = validatePassengers(aT, bT, ReportPrinter);
			}
		}
		else
		{
			ReportPrinter.append("<td>"+ResAdult.size()+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}
		///////////////////////////////////////////////////////////////////////////////////////////
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
				+ "<td>No. of Children in Request</td>"
				+ "<td>"+fillingObject.getChildren().size()+"</td>");
		if(ResChild.size()==fillingObject.getChildren().size())
		{
			ReportPrinter.append("<td>"+ResChild.size()+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
			for(int c=0; c<ResChild.size(); c++)
			{
				 aT = ResChild.get(c);
				 bT = fillingObject.getChildren().get(c);
				 ReportPrinter = validatePassengers(aT, bT, ReportPrinter);
			}
		}
		else
		{
			ReportPrinter.append("<td>"+ResChild.size()+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}
		////////////////////////////////////////////////////////////////////////////////////////////
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>No. of Infants in Request</td>"
		+ "<td>"+fillingObject.getInfant().size()+"</td>");
		if(ResInfant.size()==fillingObject.getInfant().size())
		{
			ReportPrinter.append("<td>"+ResInfant.size()+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
			for(int i=0; i<ResInfant.size(); i++)
			{
				 aT = ResInfant.get(i);
				 bT = fillingObject.getInfant().get(i);
				 ReportPrinter = validatePassengers(aT, bT, ReportPrinter);
			}
		}
		else
		{
			ReportPrinter.append("<td>"+ResInfant.size()+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}

		return ReportPrinter;
	}
	
	
	public StringBuffer validateCartCC(CartFlightCC cartflight, XMLPriceInfo PriceResPrice, XMLPriceItinerary XMLSelectFlight, SearchObject Sobj, AirConfig conf, StringBuffer ReportPrinter )
	{
		boolean twoway = false;
		//int outbXML = XMLSelectFlight.getOriginoptions().get(0).getFlightlist().size();
		int inbXML = 0;
		int inbCart= 0;
		if(Sobj.getTriptype().equals("Round Trip"))
		{
			twoway = true;
			inbXML = XMLSelectFlight.getOriginoptions().get(1).getFlightlist().size();
			inbCart= cartflight.getInbound().getInBflightlist().size();
		}

		try
		{
			Date cartdeparture = new SimpleDateFormat("dd-MMM-yyyy").parse(cartflight.getOutbound().getOutBflightlist().get(0).getDepartureDate());
			String cartSdeparture = new SimpleDateFormat("yyyy-MM-dd").format(cartdeparture);
			String XMLdeparturetime = XMLSelectFlight.getOriginoptions().get(0).getFlightlist().get(0).getDepartureTime();
			XMLdeparturetime = XMLdeparturetime.replaceAll(":", "");
			//System.out.println(XMLdeparturetime);
			String cartdeparturetime = cartflight.getOutbound().getOutBflightlist().get(0).getDepartureTime().replace("H", "00");
			//System.out.println(cartdeparturetime);
			String cartSreturn = "";
			String XMLreturntime= "";
			String cartreturntime = "";
			
			
			if(twoway)
			{
				Date cartreturn = new SimpleDateFormat("dd-MMM-yyyy").parse(cartflight.getInbound().getInBflightlist().get(0).getArrivalDate());
				cartSreturn = new SimpleDateFormat("yyyy-MM-dd").format(cartreturn);
				XMLreturntime = XMLSelectFlight.getOriginoptions().get(1).getFlightlist().get(inbXML-1).getArrivalTime();
				XMLreturntime = XMLreturntime.replaceAll(":", "");
				cartreturntime = cartflight.getInbound().getInBflightlist().get(inbCart-1).getArrivalTime().replace("H", "00");
			}
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Selling Currency</td>"
			+ "<td>"+Sobj.getSellingCurrency()+"</td>");
			if(Sobj.getSellingCurrency().equals(cartflight.getCurrencyCode()))
			{
				ReportPrinter.append("<td>"+cartflight.getCurrencyCode()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+cartflight.getCurrencyCode()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
			int outbound = XMLSelectFlight.getOriginoptions().get(0).getFlightlist().size();
			@SuppressWarnings("unused")
			int inbound  = 0;
			
			ArrayList<Flight> XMLoutboundFlights = XMLSelectFlight.getOriginoptions().get(0).getFlightlist();
			ArrayList<Flight> XMLinboundFlights = new ArrayList<Flight>();
			if(twoway)
			{
				inbound  = XMLSelectFlight.getOriginoptions().get(1).getFlightlist().size();
				XMLinboundFlights = XMLSelectFlight.getOriginoptions().get(1).getFlightlist();
			}
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Match No. of Outbound Flights</td>"
			+ "<td>Equal</td>");
			if((outbound == cartflight.getOutbound().getOutBflightlist().size()) /*&& (inbound == cartflight.getInbound().getInBflightlist().size())*/)
			{
				ReportPrinter.append("<td>XML outbound : "+outbound+" Cart outbound : "+cartflight.getOutbound().getOutBflightlist().size()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
						
				//departure port validation
				//System.out.println(cartflight.getLeaveAirport());
				//System.out.println(XMLoutboundFlights.get(0).getDeparture_port().replace(",",""));
				ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
				+ "<td>Check Departure Airport</td>"
				+ "<td>"+XMLoutboundFlights.get(0).getDepartureLocationCode()+"</td>");
				if(cartflight.getOutbound().getOutBflightlist().get(0).getDepartureLocationCode().contains(XMLoutboundFlights.get(0).getDepartureLocationCode()))
				{
					ReportPrinter.append("<td>"+cartflight.getOutbound().getOutBflightlist().get(0).getDepartureLocationCode()+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
				}
				else
				{
					ReportPrinter.append("<td>"+cartflight.getOutbound().getOutBflightlist().get(0).getDepartureLocationCode()+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
				}
				//departure date validation
				ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
				+ "<td>Check Departure Date</td>"
				+ "<td>"+XMLoutboundFlights.get(0).getDepartureDate()+"</td>");
				if(cartSdeparture.equals(XMLoutboundFlights.get(0).getDepartureDate()))
				{
					ReportPrinter.append("<td>"+cartSdeparture+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
				}
				else
				{
					ReportPrinter.append("<td>"+cartSdeparture+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
				}
				
				//departure time validation
				ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
				+ "<td>Check Departure Time</td>"
				+ "<td>"+XMLdeparturetime+"</td>");
				if(XMLdeparturetime.equals(cartdeparturetime))
				{
					ReportPrinter.append("<td>"+cartdeparturetime+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
				}
				else
				{
					ReportPrinter.append("<td>"+cartdeparturetime+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
				}
						
			}
			else
			{
				ReportPrinter.append("<td>Not Equal XML outbound : "+outbound+" Cart outbound : "+cartflight.getOutbound().getOutBflightlist().size()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
			if(twoway)
			{
				//Destination Port Validation
				//int size = XMLinboundFlights.size();
				ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
				+ "<td>Check Return Airport</td>"
				+ "<td>"+XMLinboundFlights.get(0).getDepartureLocationCode()+"</td>");
				//System.out.println(cartflight.getReturnAirport().replace(",",""));
				//System.out.println(XMLinboundFlights.get(size-1).getArrival_port().replace(",",""));
				if(cartflight.getInbound().getInBflightlist().get(0).getDepartureLocationCode().contains(XMLinboundFlights.get(0).getDepartureLocationCode()))
				{
					ReportPrinter.append("<td>"+cartflight.getInbound().getInBflightlist().get(0).getDepartureLocationCode()+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
				}
				else
				{
					ReportPrinter.append("<td>"+cartflight.getInbound().getInBflightlist().get(0).getDepartureLocationCode()+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
				}
					
				//Arrival date validation
				//cartSdeparture.equals(XMLSelectFlight.getOriginoptions().get(0).getFlightlist().get(outbound-1).getDepartureDate()
				ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
				+ "<td>Check Arrival Date</td>"
				+ "<td>"+XMLinboundFlights.get(0).getDepartureDate()+"</td>");
				if(cartSreturn.equals(XMLinboundFlights.get(0).getDepartureDate()))
				{
					ReportPrinter.append("<td>"+cartSreturn+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
				}
				else
				{
					ReportPrinter.append("<td>"+cartSreturn+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
				}
				
				//ARRIVAL TIME
				ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
				+ "<td>Check Arrival Time</td>"
				+ "<td>"+XMLreturntime+"</td>");
				if(cartreturntime.equals(XMLreturntime))
				{
					ReportPrinter.append("<td>"+cartreturntime+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
				}
				else
				{
					ReportPrinter.append("<td>"+cartreturntime+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
				}
					
			}
				
			//VALIDATING SUBTOTAL, TOTAL TAXES AND OTHER CHARGES, AMOUNT BEING PROCESSED NOW. AMOUNT PROCESSED BY AIRLINE, FINAL TOTAL
			double CartTotalBeforeTax			= Double.parseDouble(cartflight.getTotalBeforeTax());
			double CartTaxAndOther				= Double.parseDouble(cartflight.getTaxes()) + Double.parseDouble(cartflight.getBookingfee());
			double CartTotalInclusiveTax		= Double.parseDouble(cartflight.getTotalInclusiveTax());
			double CartTotalGrossBookingValue	= Double.parseDouble(cartflight.getTotalGrossBookingValue());
			double CartTaxesandOtherperFlight	= Double.parseDouble(cartflight.getTaxesandOther());
			double CartAmountprocessnowFinal	= 0;
			
			if(Sobj.getPaymentMode().equalsIgnoreCase("Pay Online"))
			{
				CartAmountprocessnowFinal	= Double.parseDouble(cartflight.getAmountprocessnowFinal());
			}
			
			//double cartamountProcessAirline = Double.parseDouble(cartflight.getAmountbyairline());
			double CartFinalvalue				= Double.parseDouble(cartflight.getFinalvalue());
				
			double XMLTotalBeforeTax			= (XMLSelectFlight.getPricinginfo().getNewbase() + XMLSelectFlight.getPricinginfo().getProfit());
			double XMLTaxAndOther				= (XMLSelectFlight.getPricinginfo().getNewtax() + XMLSelectFlight.getPricinginfo().getBookingfee());
			double XMLTotalInclusiveTax			= (XMLSelectFlight.getPricinginfo().getNewbase() + XMLSelectFlight.getPricinginfo().getProfit() + XMLSelectFlight.getPricinginfo().getNewtax() + XMLSelectFlight.getPricinginfo().getBookingfee());
			double XMLTotalGrossBookingValue	= (XMLSelectFlight.getPricinginfo().getNewbase() + XMLSelectFlight.getPricinginfo().getProfit() );
			double XMLTaxesandOtherperFlight	= (XMLSelectFlight.getPricinginfo().getNewtax() + XMLSelectFlight.getPricinginfo().getBookingfee());
			double XMLAmountprocessnowFinal		= 0;
			double XMLFinalvalue				= (XMLSelectFlight.getPricinginfo().getNewbase() + XMLSelectFlight.getPricinginfo().getProfit() + XMLSelectFlight.getPricinginfo().getNewtax() + XMLSelectFlight.getPricinginfo().getBookingfee() );
			
			if(Sobj.getPaymentMode().equalsIgnoreCase("Pay Online"))
			{
				if( conf.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Full_Amount_at_Booking") )
				{
					XMLAmountprocessnowFinal = (XMLSelectFlight.getPricinginfo().getNewbase() + XMLSelectFlight.getPricinginfo().getProfit() + XMLSelectFlight.getPricinginfo().getNewtax() + XMLSelectFlight.getPricinginfo().getBookingfee() );
				}
				else if( conf.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Booking_Fee_as_Deposit") )
				{
					XMLAmountprocessnowFinal = XMLSelectFlight.getPricinginfo().getBookingfee();
				}
				else if( conf.getFlightPayOptCartBooking().equalsIgnoreCase("Pass_to_Air_Line_to_Charge") )
				{
					
				}	
			}
			//Total Before Tax
			/*ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Cart Subtotal Per Flight</td>"
			+ "<td>"+XMLTotalBeforeTax+"</td>");
			if(  XMLTotalBeforeTax ==  CartTotalBeforeTax   )
			{
				ReportPrinter.append("<td>"+CartTotalBeforeTax+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+CartTotalBeforeTax+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}*/
			statusPassFail = CommonValidator.compareDouble(XMLTotalBeforeTax, CartTotalBeforeTax, "Check Cart Subtotal Per Flight", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
			//Tax and Other
			/*ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Cart Tax and Other Per Flight</td>"
			+ "<td>"+XMLTaxAndOther+"</td>");
			if(  XMLTaxAndOther ==  CartTaxAndOther   )
			{
				ReportPrinter.append("<td>"+CartTaxAndOther+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+CartTaxAndOther+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}*/
			statusPassFail = CommonValidator.compareDouble(XMLTaxAndOther, CartTaxAndOther, "Check Cart Tax and Other Per Flight", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
				
			//Tax Inclusive
			/*ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Cart flight total</td>"
			+ "<td>"+XMLTotalInclusiveTax+"</td>");
			if(  XMLTotalInclusiveTax ==  CartTotalInclusiveTax  )
			{
				ReportPrinter.append("<td>"+CartTotalInclusiveTax+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+CartTotalInclusiveTax+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}*/
			statusPassFail = CommonValidator.compareDouble(XMLTotalInclusiveTax, CartTotalInclusiveTax, "Check Cart flight total", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
				
			//Validate Final sub total
			/*ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Cart Final Subtotal</td>"
			+ "<td>"+XMLTotalGrossBookingValue+"</td>");
			if(  XMLTotalGrossBookingValue ==  CartTotalGrossBookingValue   )
			{
				ReportPrinter.append("<td>"+CartTotalGrossBookingValue+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;	
			}	
			else
			{
				ReportPrinter.append("<td>"+CartTotalGrossBookingValue+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}*/
			statusPassFail = CommonValidator.compareDouble(XMLTotalGrossBookingValue, CartTotalGrossBookingValue, "Check Cart Final Subtotal", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
			//Validate cart tax and other per flight
			/*ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Cart Tax and Other per flight</td>"
			+ "<td>"+XMLTaxesandOtherperFlight+"</td>");
			if(  XMLTaxesandOtherperFlight ==  CartTaxesandOtherperFlight  )
			{
				ReportPrinter.append("<td>"+CartTaxesandOtherperFlight+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+CartTaxesandOtherperFlight+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}*/
			statusPassFail = CommonValidator.compareDouble(XMLTaxesandOtherperFlight, CartTaxesandOtherperFlight, "Check Cart Tax and Other per flight", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
			//Amount Pocess now		
			/*ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Amount being Processed Now</td>"
			+ "<td>"+XMLAmountprocessnowFinal+"</td>");
			if(  XMLAmountprocessnowFinal ==  CartAmountprocessnowFinal  )
			{
				ReportPrinter.append("<td>"+CartAmountprocessnowFinal+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+CartAmountprocessnowFinal+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}*/
			statusPassFail = CommonValidator.compareDouble(XMLAmountprocessnowFinal, CartAmountprocessnowFinal, "Check Amount being Processed Now", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
				
			//Validate Final booking value
			/*ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Final Booking Value</td>"
			+ "<td>"+XMLFinalvalue+"</td>");
			if(  XMLFinalvalue >=  CartFinalvalue  )
			{
				ReportPrinter.append("<td>"+CartFinalvalue+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+CartFinalvalue+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}*/
			statusPassFail = CommonValidator.compareDouble(XMLFinalvalue, CartFinalvalue, "Check Final Booking Value", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return ReportPrinter;

	}

	
	public StringBuffer validateCCPaymentPage(UIPaymentPage uipayment, XMLPriceItinerary XMLSelectFlight, SearchObject Sobj, AirConfig conf, StringBuffer ReportPrinter)
	{
		boolean twoway = false;
		if(Sobj.getTriptype().equals("Round Trip"))
		{
			twoway = true;
		}

		//System.out.println();
		//System.out.println("----------------------------------------------------------------------------------------------------");

		String xmlcurrencyCode	= "";
		double xmlnewbase		= 0;
		double xmlnewtax		= 0;
		double xmlprofit		= 0;
		double xmlSellingCost	= 0;
		double xmlbookingFee	= 0;
		double xmlDiscount		= 0;
		
		try {
			xmlcurrencyCode		= XMLSelectFlight.getPricinginfo().getNewBasefareCurrencyCode();
			xmlnewbase			= XMLSelectFlight.getPricinginfo().getNewbase();
			xmlnewtax			= XMLSelectFlight.getPricinginfo().getNewtax();
			xmlprofit			= XMLSelectFlight.getPricinginfo().getProfit();
			xmlSellingCost		= XMLSelectFlight.getPricinginfo().getSellingCost();
			xmlbookingFee		= XMLSelectFlight.getPricinginfo().getBookingfee();
			try {
				xmlDiscount			= XMLSelectFlight.getPricinginfo().getDiscount();
				xmlDiscount			= Math.ceil(xmlDiscount);
			} catch (Exception e) {
				System.out.println("Error in Discount round up");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		String uicurrencyCode		= "";							
		double uisubtotal			= 0;
		double uitaxplusother		= 0;
		double uitotal				= 0;
		double uiamountprocessnw	= 0;
		double uidiscount			= 0;
		
		try {
			uicurrencyCode		= uipayment.getUisummarypay().getCurrencycode();							
			uisubtotal			= Double.parseDouble(uipayment.getUisummarypay().getSubtotal());
			uitaxplusother		= Double.parseDouble(uipayment.getUisummarypay().getTaxplusother());
			uitotal				= uisubtotal + uitaxplusother;
			uiamountprocessnw	= Double.parseDouble(uipayment.getUisummarypay().getAmountprocess());
			uidiscount			= Double.parseDouble(uipayment.getUisummarypay().getDiscount());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		/////////////////////////////////////////////////////////
		double creditcardamount		= 0;
		String creditCardCostType	= "Precentage";
		creditCardCostType			= PropertyMap.get("CreditCard_Cost_Type"); 
		double creditCardCost		= 0;
		creditCardCost				= Double.parseDouble(PropertyMap.get("CreditCard_Cost"));
		
		double CCAmountInPorCurr	= 0;
		double CCAmountInSelCurr	= 0;
		double xmlProcessNow		= 0;
		xmlProcessNow				= xmlnewbase + xmlnewtax + xmlprofit + xmlbookingFee;
		if(Sobj.isApplyDiscount())
		{
			xmlProcessNow = xmlProcessNow - xmlDiscount;
		}
		
		if( conf.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Full_Amount_at_Booking") )
		{
			xmlProcessNow = xmlSellingCost - xmlDiscount;
		}
		else if( conf.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Booking_Fee_as_Deposit") )
		{
			xmlProcessNow = xmlbookingFee;
		}
		else if( conf.getFlightPayOptCartBooking().equalsIgnoreCase("Pass_to_Air_Line_to_Charge") )
		{
			
		}
		
		if(creditCardCostType.equalsIgnoreCase("Value"))
		{
			creditcardamount	= creditCardCost;
			CCAmountInPorCurr	= CommonValidator.convert(PropertyMap.get("Portal_Currency_Code"), PropertyMap.get("Portal_Currency_Code"), PropertyMap.get("Portal_Currency_Code"), creditcardamount, CurrencyMap);
			XMLSelectFlight.getPricinginfo().setCreditcardFeeInPortalCurrency(CCAmountInPorCurr);
			CCAmountInSelCurr	= CommonValidator.convert(PropertyMap.get("Portal_Currency_Code"), xmlcurrencyCode, PropertyMap.get("Portal_Currency_Code"), creditcardamount, CurrencyMap);
			XMLSelectFlight.getPricinginfo().setCreditcardfeeinSellCurr(CCAmountInSelCurr);
		}
		else if(creditCardCostType.equalsIgnoreCase("Percentage"))
		{
			creditcardamount	= xmlProcessNow * (creditCardCost/100);
			XMLSelectFlight.getPricinginfo().setCreditcardfeeinSellCurr(creditcardamount);
			CCAmountInPorCurr	= CommonValidator.convert(xmlcurrencyCode, PropertyMap.get("Portal_Currency_Code"), PropertyMap.get("Portal_Currency_Code"), creditcardamount, CurrencyMap);
			XMLSelectFlight.getPricinginfo().setCreditcardFeeInPortalCurrency(CCAmountInPorCurr);
		}

		double xmlcreditcardfee		= 0;
		xmlcreditcardfee			= XMLSelectFlight.getPricinginfo().getCreditcardfeeinSellCurr();

		
		if(xmlcurrencyCode.equals(uicurrencyCode))
		{
			if(Sobj.isApplyDiscount())
			{
				statusPassFail = CommonValidator.compareDouble(xmlDiscount, uidiscount, "Check Discount Applied", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), "0"/*PropertyMap.get("RoundUpDownBy")*/);
				Reportcount++;
			}
			
			statusPassFail = CommonValidator.compareDouble((xmlnewbase + xmlprofit), uisubtotal, "Check Payment page subtotal", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
			statusPassFail = CommonValidator.compareDouble((xmlnewtax + xmlbookingFee + xmlcreditcardfee), (uitaxplusother), "Check Payment page Tax plus Other", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
			statusPassFail = CommonValidator.compareDouble((xmlSellingCost + xmlcreditcardfee), (uitotal), "Check Payment page Total Amount", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;	

			if( conf.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Full_Amount_at_Booking") )
			{
				xmlProcessNow = xmlSellingCost + xmlcreditcardfee;
			}
			else if( conf.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Booking_Fee_as_Deposit") )
			{
				xmlProcessNow = xmlbookingFee + xmlcreditcardfee;
			}
			else if( conf.getFlightPayOptCartBooking().equalsIgnoreCase("Pass_to_Air_Line_to_Charge") )
			{
				
			}
			
			statusPassFail = CommonValidator.compareDouble((xmlProcessNow), (uiamountprocessnw), "Check Payment Page Amount Process Now", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
		}
		
		/*statusPassFail = CommonValidator.elementAvailable(uipayment.isAirRulesDisplayed(), "Fare rules Available", ReportPrinter, Reportcount);
		Reportcount++;*/
		////////////////////////////////////////////////////////
		
		/*double	creditcardamountinPortal	= 0;
		double	xmlcreditcardinSelling		= 0;
		
		if(Sobj.getPaymentMode().equalsIgnoreCase("Pay Online"))
		{
			creditcardamountinPortal	= xmlSellingCost * (Double.parseDouble(PropertyMap.get("CreditCard_Cost_Percentage"))/100);		
			creditcardamountinPortal	= convert(xmlcurrencyCode, PropertyMap.get("Payment_Gateway_Currency_Code"), creditcardamountinPortal);
			xmlcreditcardinSelling		= xmlSellingCost * (Double.parseDouble(PropertyMap.get("CreditCard_Cost_Percentage"))/100);
			setCreditCardAmount(xmlcreditcardinSelling, XMLSelectFlight.getPricinginfo());
		}
		
		
		double XMLsubtotal				= (xmlnewbase + xmlprofit);
		double XMLtaxplusother			= (xmlnewtax + xmlbookingFee + xmlcreditcardinSelling);
		double XMLtotal					= (xmlSellingCost + xmlcreditcardinSelling);
		
		//double xmlbase = 0;
		//xmlbase = Double.parseDouble(XMLSelectFlight.getPricinginfo().getBasefareAmount());
		//xmlbase = convert(XMLSelectFlight.getPricinginfo().getBasefareCurrencyCode(), PropertyMap.get("Payment_Gateway_Currency_Code"), xmlbase);
		//double xmltax = 0;
		//xmltax = Double.parseDouble(XMLSelectFlight.getPricinginfo().get);
		//xmltax = convert(XMLSelectFlight.getPricinginfo().getBasefareCurrencyCode(), PropertyMap.get("Payment_Gateway_Currency_Code"), xmltax);
		
		double	xmltot		= 0;
				xmltot		= Double.parseDouble(XMLSelectFlight.getPricinginfo().getTotalFare());//Total fare in supplier currency
		double	xmlprft		= 0;
				xmlprft		= XMLSelectFlight.getPricinginfo().getProfit();//profit markup in selling currency
		double	xmlbkngfee	= 0;
				xmlbkngfee	= XMLSelectFlight.getPricinginfo().getBookingfee();//booking fee in selling currency
		String	xmlsellcurr	= XMLSelectFlight.getPricinginfo().getNewBasefareCurrencyCode();//xml selling currency
		String	xmlsuppcurr	= XMLSelectFlight.getPricinginfo().getBasefareCurrencyCode();//xml supplier currency
		
		
		
		double	xmltotInCreditCardCurr	= 0;
		double	XMLamountprocessnw		= 0; 
		if(Sobj.getPaymentMode().equalsIgnoreCase("Pay Online"))
		{
			xmltotInCreditCardCurr	= convert(xmlsuppcurr, PropertyMap.get("Payment_Gateway_Currency_Code"), xmltot);//convert supplier currency total to payment gateway currency
			xmltotInCreditCardCurr	= xmltotInCreditCardCurr + convert(xmlsellcurr, PropertyMap.get("Payment_Gateway_Currency_Code"), (xmlprft + xmlbkngfee));
			xmltotInCreditCardCurr	= xmltotInCreditCardCurr + creditcardamountinPortal;
			xmltotInCreditCardCurr	= Math.ceil(xmltotInCreditCardCurr);
			
			XMLamountprocessnw	= (xmlSellingCost + xmlcreditcardinSelling);
		
			if( conf.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Full_Amount_at_Booking") )
			{
				XMLamountprocessnw	= xmlnewbase + xmlprofit + xmlnewtax + xmlbookingFee + xmlcreditcardinSelling;
				setPaidNow(XMLamountprocessnw, XMLSelectFlight.getPricinginfo());
				//XMLSelectFlight.getPricinginfo().setProcessnow(XMLamountprocessnw);
			}
			else if( conf.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Booking_Fee_as_Deposit") )
			{
				XMLamountprocessnw	= xmlbookingFee;
				setPaidNow(XMLamountprocessnw, XMLSelectFlight.getPricinginfo());
				//XMLSelectFlight.getPricinginfo().setProcessnow(XMLamountprocessnw);
			}
			else if( conf.getFlightPayOptCartBooking().equalsIgnoreCase("Pass_to_Air_Line_to_Charge") )
			{
				
			}	
		}
		
		
		double uicreditcardamount	= 0;
		
		if(Sobj.getPaymentMode().equalsIgnoreCase("Pay Online"))
		{
			uicreditcardamount	= Double.parseDouble(uipayment.getCreditcardpay().getTotalpackage_bookVal());
		}
		
		
		if(xmlcurrencyCode.equals(uicurrencyCode))
		{
			//Validating Payment Page Subtotal
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Payment page Subtotal</td>"
			+ "<td>"+XMLsubtotal+"</td>");
			if(  XMLsubtotal == uisubtotal   )
			{
				ReportPrinter.append("<td>"+uisubtotal+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+uisubtotal+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
			if(Sobj.getPaymentMode().equalsIgnoreCase("Pay Online"))
			{
				//VALIDATE CREDITCARD TOTAL AMOUNT
				ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
				+ "<td>CreditCard Amount</td>"
				+ "<td>"+xmltotInCreditCardCurr+"</td>");
				if(  xmltotInCreditCardCurr == uicreditcardamount   )
				{
					ReportPrinter.append("<td>"+uicreditcardamount+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
				}
				else
				{
					ReportPrinter.append("<td>"+uicreditcardamount+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
				}
			}
			
			//Validating payment page taxes plus other
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Payment page Tax plus Other</td>"
			+ "<td>"+Math.ceil(XMLtaxplusother)+"</td>");
			if(  Math.ceil(XMLtaxplusother) == uitaxplusother   )
			{
				ReportPrinter.append("<td>"+uitaxplusother+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+uitaxplusother+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
			//Validating payment page total
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Payment page Total Amount</td>"
			+ "<td>"+Math.ceil(XMLtotal)+"</td>");
			if(  Math.ceil(XMLtotal) == uitotal   )
			{
				ReportPrinter.append("<td>"+uitotal+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+uitotal+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
			//Validating payment page amount process now
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Payment Page Amount Process Now</td>"
			+ "<td>"+Math.ceil(XMLamountprocessnw)+"</td>");
			if(  Math.ceil(XMLamountprocessnw) == uiamountprocessnw   )
			{
				ReportPrinter.append("<td>"+uiamountprocessnw+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+uiamountprocessnw+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
		}*/
		
		Date uideparture;
		Date uiarrival;
		
		String uiSreturndeparture		= "";
		String uiSreturnarrival			= "";
		String XMLreturntime			= "";
		String XMLreturnArrtime			= "";
		String uireturntime				= "";
		
		
		int noofXMLOutflights				= XMLSelectFlight.getOriginoptions().get(0).getFlightlist().size();  
		ArrayList<Flight> XMLoutboundlist	= new ArrayList<Flight>();
		XMLoutboundlist						= XMLSelectFlight.getOriginoptions().get(0).getFlightlist();
		int noofXMLInflights				= 0;
		ArrayList<Flight> XMLinboundlist	= new ArrayList<Flight>();;
		
		int noofPayPageOutFlights			= uipayment.getOutbound().getOutBflightlist().size();
		ArrayList<Flight> UIoutboundlist	= uipayment.getOutbound().getOutBflightlist();
		int noofPayPageInflights			= 0;
		ArrayList<Flight> UIinboundlist		= null;
		
		ArrayList<String> XMLdeparturetime	= new ArrayList<String>();
		ArrayList<String> XMLarrivaltime	= new ArrayList<String>();
		
		for(int u=0; u<noofPayPageOutFlights; u++)
		{
			try 
			{
				uideparture = new SimpleDateFormat("dd-MMM-yyyy").parse(uipayment.getOutbound().getOutBflightlist().get(u).getDepartureDate());
				String uiSdeparture = new SimpleDateFormat("yyyy-MM-dd").format(uideparture);
				UIoutboundlist.get(u).setDepartureDate(uiSdeparture);
				
				uiarrival = new SimpleDateFormat("dd-MMM-yyyy").parse(uipayment.getOutbound().getOutBflightlist().get(u).getArrivalDate());
				String uiSarrival = new SimpleDateFormat("yyyy-MM-dd").format(uiarrival);
				UIoutboundlist.get(u).setArrivalDate(uiSarrival);
			} 
			catch (ParseException e1)
			{
				e1.printStackTrace();
			}
			
			XMLdeparturetime.add(XMLoutboundlist.get(u).getDepartureTime());
			String modifiedXMLdeparturetime = XMLdeparturetime.get(u).replaceAll(":", "");
			XMLoutboundlist.get(u).setDepartureTime(modifiedXMLdeparturetime);
			
			String uideparturetime = uipayment.getOutbound().getOutBflightlist().get(u).getDepartureTime().replace("H", "00");
			UIoutboundlist.get(u).setDepartureTime(uideparturetime);
			
			XMLarrivaltime.add(XMLoutboundlist.get(u).getArrivalTime());
			String modifiedXMLarrivaltime = XMLarrivaltime.get(u).replaceAll(":", "");
			XMLoutboundlist.get(u).setArrivalTime(modifiedXMLarrivaltime);
			
			String uiarrivaltime = uipayment.getOutbound().getOutBflightlist().get(u).getArrivalTime().replace("H", "00");
			UIoutboundlist.get(u).setArrivalTime(uiarrivaltime);
		}
		
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>Match number of Outbound flights in Payment page with Outbound flights in Response XML</td>"
		+ "<td>"+noofXMLOutflights+"</td>");
		if(noofXMLOutflights == noofPayPageOutFlights )
		{
			ReportPrinter.append("<td>"+noofPayPageOutFlights+"</td>"
			+"<td class='Passed'>PASS</td></tr>");
			Reportcount++;
			
			ReportPrinter = validateFlights(noofXMLOutflights, "Outbound", UIoutboundlist, XMLoutboundlist, ReportPrinter );
		}
		else
		{
			ReportPrinter.append("<td>"+noofPayPageOutFlights+"</td>"
			+"<td class='Passed'>Fail</td></tr>");
			Reportcount++;
		}
		
		for(int h=0; h<noofXMLOutflights; h++)
		{
			XMLSelectFlight.getOriginoptions().get(0).getFlightlist().get(h).setDepartureTime(XMLdeparturetime.get(h));
			XMLSelectFlight.getOriginoptions().get(0).getFlightlist().get(h).setArrivalTime(XMLarrivaltime.get(h));
		}
		
		if(twoway)
		{
			ArrayList<String> XMLreturndeparturetime	= new ArrayList<String>();
			ArrayList<String> XMLreturnarrivaltime	= new ArrayList<String>();
			try
			{
				noofXMLInflights = XMLSelectFlight.getOriginoptions().get(1).getFlightlist().size();
				XMLinboundlist = XMLSelectFlight.getOriginoptions().get(1).getFlightlist();
			
				noofPayPageInflights = uipayment.getInbound().getInBflightlist().size();
				UIinboundlist = uipayment.getInbound().getInBflightlist();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			Date uireturndeparture;
			Date uireturnarrival;
			for(int i=0; i<noofPayPageInflights; i++)
			{
				try 
				{
					uireturndeparture = new SimpleDateFormat("dd-MMM-yyyy").parse(uipayment.getInbound().getInBflightlist().get(i).getDepartureDate());
					uiSreturndeparture = new SimpleDateFormat("yyyy-MM-dd").format(uireturndeparture);
					UIinboundlist.get(i).setDepartureDate(uiSreturndeparture);
					
					uireturnarrival = new SimpleDateFormat("dd-MMM-yyyy").parse(uipayment.getInbound().getInBflightlist().get(i).getArrivalDate());
					uiSreturnarrival = new SimpleDateFormat("yyyy-MM-dd").format(uireturnarrival);
					UIinboundlist.get(i).setArrivalDate(uiSreturnarrival);
					
				} 
				catch (ParseException e) 
				{
					e.printStackTrace();
				}
				
				XMLreturndeparturetime.add(XMLinboundlist.get(i).getDepartureTime())/*XMLSelectFlight.getOriginoptions().get(1).getFlightlist()*/;
				XMLreturntime = XMLreturndeparturetime.get(i).replaceAll(":", "");
				XMLinboundlist.get(i).setDepartureTime(XMLreturntime);
				
				uireturntime = uipayment.getInbound().getInBflightlist().get(i).getDepartureTime().replace("H", "00");
				UIinboundlist.get(i).setDepartureTime(uireturntime);
				
				XMLreturnarrivaltime.add(XMLinboundlist.get(i).getArrivalTime())/*XMLSelectFlight.getOriginoptions().get(1).getFlightlist()*/;
				XMLreturnArrtime = XMLreturnarrivaltime.get(i).replaceAll(":", "");
				XMLinboundlist.get(i).setArrivalTime(XMLreturnArrtime);
				
				String uireturnArrtime = uipayment.getInbound().getInBflightlist().get(i).getArrivalTime().replace("H", "00");
				UIinboundlist.get(i).setArrivalTime(uireturnArrtime);
			}

			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Match number of Inbound flights in Payment page with number of Inbound flights in XML</td>"
			+ "<td>"+noofXMLInflights+"</td>");
			if( (noofXMLInflights != 0) && (noofPayPageInflights != 0) && (noofXMLInflights == noofPayPageInflights) )
			{
				ReportPrinter.append("<td>"+noofPayPageInflights+"</td>"
				+"<td class='Passed'>PASS</td></tr>");
				Reportcount++;
						
				ReportPrinter = validateFlights(noofXMLInflights, "Inbound", UIinboundlist, XMLinboundlist, ReportPrinter );
			}
			else
			{
				ReportPrinter.append("<td>"+noofPayPageInflights+"</td>"
				+"<td class='Passed'>Fail</td></tr>");
				Reportcount++;
			}
			
			for(int h=0; h<noofXMLInflights; h++)
			{
				XMLSelectFlight.getOriginoptions().get(1).getFlightlist().get(h).setDepartureTime(XMLreturndeparturetime.get(h));
				XMLSelectFlight.getOriginoptions().get(1).getFlightlist().get(h).setArrivalTime(XMLreturnarrivaltime.get(h));
			}
			
		}

		return ReportPrinter;

	}
	
	
	public StringBuffer validateConfirmationPageCC( SearchObject Sobj, UIConfirmationPage uiconfirmpage, StringBuffer ReportPrinter, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight/*, ReservationResponse resvResp*/)
	{
		boolean twoway = false;
		if(Sobj.getTriptype().equals("Round Trip"))
		{
			twoway = true;
		}
		try
		{	
			Traveler Fillmaincus = fillingObject.getMaincustomer();
			Traveler Confmaincus = uiconfirmpage.getMaincustomer();
			Address FillmainCusAddress = Fillmaincus.getAddress();
			Address ConfmainCusAddress = Confmaincus.getAddress();
			///////////////////////////////////////////////////////////////////////////////
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Direct customer's given name</td>"
			+ "<td>"+Fillmaincus.getGivenName()+"</td>");
			if(  Confmaincus.getGivenName().equalsIgnoreCase(Fillmaincus.getGivenName())  )
			{
				ReportPrinter.append("<td>"+Confmaincus.getGivenName()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+Confmaincus.getGivenName()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			/////////////////////////////////////////////////////////////////////////////////
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Direct customer's surname</td>"
			+ "<td>"+Fillmaincus.getSurname()+"</td>");
			if(  Fillmaincus.getSurname().equalsIgnoreCase(Confmaincus.getSurname())  )
			{
				ReportPrinter.append("<td>"+Confmaincus.getSurname()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+Confmaincus.getGivenName()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			//////////////////////////////////////////////////////////////////////////////////
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Direct customer address Street No</td>"
			+ "<td>"+FillmainCusAddress.getAddressStreetNo()+"</td>");
			if(ConfmainCusAddress.getAddressStreetNo().equalsIgnoreCase(FillmainCusAddress.getAddressStreetNo()))
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getAddressStreetNo()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getAddressStreetNo()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			///////////////////////////////////////////////////////////////////////////////
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Direct customer address City</td>"
			+ "<td>"+FillmainCusAddress.getAddressCity()+"</td>");
			if(ConfmainCusAddress.getAddressCity().equalsIgnoreCase(FillmainCusAddress.getAddressCity()))
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getAddressCity()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getAddressCity()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			/////////////////////////////////////////////////////////////////////////////////
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Direct customer address PostalCode</td>"
			+ "<td>"+FillmainCusAddress.getPostalCode()+"</td>");
			if(ConfmainCusAddress.getPostalCode().equalsIgnoreCase(FillmainCusAddress.getPostalCode()))
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getPostalCode()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else if(ConfmainCusAddress.getPostalCode().equals("-") && FillmainCusAddress.getPostalCode().equals(""))
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getPostalCode()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getPostalCode()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			//////////////////////////////////////////////////////////////////////////////////
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Direct customer address Country</td>"
			+ "<td>"+FillmainCusAddress.getAddressCountry()+"</td>");
			if(ConfmainCusAddress.getAddressCountry().equalsIgnoreCase(FillmainCusAddress.getAddressCountry()))
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getAddressCountry()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getAddressCountry()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			//////////////////////////////////////////////////////////////////////////////////
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Direct customer address State Code</td>"
			+ "<td>"+FillmainCusAddress.getStateProv()+"</td>");
			if(ConfmainCusAddress.getStateProv().equalsIgnoreCase(FillmainCusAddress.getStateProv()))
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getStateProv()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else if(ConfmainCusAddress.getStateProv().equals("-") && FillmainCusAddress.getStateProv().equals(""))
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getStateProv()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getStateProv()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
			int xmloutbound = XMLSelectFlight.getOriginoptions().get(0).getFlightlist().size();
			int xmlinbound  = 0;
			
			ArrayList<Flight> xmloutbflightlist = new ArrayList<Flight>();
			ArrayList<Flight> xmlinbflightlist  = new ArrayList<Flight>();
			
			ArrayList<Flight> confOutbflightlist = new ArrayList<Flight>();
			ArrayList<Flight> confInbflightlist  = new ArrayList<Flight>();
		
			ArrayList<String> XMLOriginaldeparturetime	= new ArrayList<String>();
			ArrayList<String> XMLOriginalarrivaltime	= new ArrayList<String>();
			
			int noofConfPageOutFlights	= XMLSelectFlight.getOriginoptions().get(0).getFlightlist().size();
			xmloutbflightlist			= XMLSelectFlight.getOriginoptions().get(0).getFlightlist();
			
			confOutbflightlist			= uiconfirmpage.getOutbound().getOutBflightlist();
			
			for(int u=0; u<noofConfPageOutFlights; u++)
			{
				try 
				{
					Date uideparture	= new SimpleDateFormat("dd-MMM-yyyy").parse(uiconfirmpage.getOutbound().getOutBflightlist().get(u).getDepartureDate());
					String uiSdeparture	= new SimpleDateFormat("yyyy-MM-dd").format(uideparture);
					confOutbflightlist.get(u).setDepartureDate(uiSdeparture);
					
					Date uiarrival		= new SimpleDateFormat("dd-MMM-yyyy").parse(uiconfirmpage.getOutbound().getOutBflightlist().get(u).getArrivalDate());
					String uiSarrival	= new SimpleDateFormat("yyyy-MM-dd").format(uiarrival);
					confOutbflightlist.get(u).setArrivalDate(uiSarrival);
					
				} 
				catch (ParseException e1)
				{
					e1.printStackTrace();
				}
				
				String XMLdeparturetime	= xmloutbflightlist.get(u).getDepartureTime();
				XMLOriginaldeparturetime.add(XMLdeparturetime);
				XMLdeparturetime		= XMLdeparturetime.replaceAll(":", "");
				xmloutbflightlist.get(u).setDepartureTime(XMLdeparturetime);
				
				String uideparturetime = uiconfirmpage.getOutbound().getOutBflightlist().get(u).getDepartureTime().replace("H", "00");
				confOutbflightlist.get(u).setDepartureTime(uideparturetime);
				
				String XMLarrivaltime	= xmloutbflightlist.get(u).getArrivalTime();
				XMLOriginalarrivaltime.add(XMLarrivaltime);
				XMLarrivaltime			= XMLarrivaltime.replaceAll(":", "");
				xmloutbflightlist.get(u).setArrivalTime(XMLarrivaltime);
				
				String uiarrivaltime	= uiconfirmpage.getOutbound().getOutBflightlist().get(u).getArrivalTime().replace("H", "00");
				confOutbflightlist.get(u).setArrivalTime(uiarrivaltime);
			}
			
			ReportPrinter = validateFlights(xmloutbound, "Outbound", confOutbflightlist, xmloutbflightlist, ReportPrinter );
			for(int h=0; h<xmloutbound; h++)
			{
				XMLSelectFlight.getOriginoptions().get(0).getFlightlist().get(h).setDepartureTime(XMLOriginaldeparturetime.get(h));
				XMLSelectFlight.getOriginoptions().get(0).getFlightlist().get(h).setArrivalTime(XMLOriginalarrivaltime.get(h));
			}
			
			if(twoway)
			{ 
				ArrayList<String> XMLOriginalreturndeparturetime	= new ArrayList<String>();
				ArrayList<String> XMLOriginalreturnarrivaltime		= new ArrayList<String>();
				int noofConfPageInflights = 0;
				try
				{
					xmlinbflightlist = XMLSelectFlight.getOriginoptions().get(1).getFlightlist();
				
					noofConfPageInflights	= uiconfirmpage.getInbound().getInBflightlist().size();
					confInbflightlist		= uiconfirmpage.getInbound().getInBflightlist();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				
				Date uireturndeparture;
				Date uireturnarrival;
				for(int i=0; i<noofConfPageInflights; i++)
				{
					try 
					{
						uireturndeparture			= new SimpleDateFormat("dd-MMM-yyyy").parse(uiconfirmpage.getInbound().getInBflightlist().get(i).getDepartureDate());
						String uiSreturndeparture	= new SimpleDateFormat("yyyy-MM-dd").format(uireturndeparture);
						confInbflightlist.get(i).setDepartureDate(uiSreturndeparture);
						
						uireturnarrival			= new SimpleDateFormat("dd-MMM-yyyy").parse(uiconfirmpage.getInbound().getInBflightlist().get(i).getArrivalDate());
						String uiSreturnarrival	= new SimpleDateFormat("yyyy-MM-dd").format(uireturnarrival);
						confInbflightlist.get(i).setArrivalDate(uiSreturnarrival);
						
					} 
					catch (ParseException e) 
					{
						e.printStackTrace();
					}
					
					String XMLreturntime	= xmlinbflightlist.get(i).getDepartureTime();
					XMLOriginalreturndeparturetime.add(XMLreturntime);
					XMLreturntime			= XMLreturntime.replaceAll(":", "");
					xmlinbflightlist.get(i).setDepartureTime(XMLreturntime);
					
					String uireturntime		= uiconfirmpage.getInbound().getInBflightlist().get(i).getDepartureTime().replace("H", "00");
					confInbflightlist.get(i).setDepartureTime(uireturntime);
					
					String XMLreturnArrtime	= xmlinbflightlist.get(i).getArrivalTime();
					XMLOriginalreturnarrivaltime.add(XMLreturnArrtime);
					XMLreturnArrtime		= XMLreturnArrtime.replaceAll(":", "");
					xmlinbflightlist.get(i).setArrivalTime(XMLreturnArrtime);
					
					String uireturnArrtime	= uiconfirmpage.getInbound().getInBflightlist().get(i).getArrivalTime().replace("H", "00");
					confInbflightlist.get(i).setArrivalTime(uireturnArrtime);
				}
				
				ReportPrinter = validateFlights(xmlinbound, "Inbound", confInbflightlist, xmlinbflightlist, ReportPrinter );
				
				for(int h=0; h<xmlinbound; h++)
				{
					XMLSelectFlight.getOriginoptions().get(1).getFlightlist().get(h).setDepartureTime(XMLOriginalreturndeparturetime.get(h));
					XMLSelectFlight.getOriginoptions().get(1).getFlightlist().get(h).setArrivalTime(XMLOriginalreturnarrivaltime.get(h));
				}
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return ReportPrinter;

	}
	
	
	
	
	//================================REPORTS VALIDATION====================================
	
	//==========================RESERVATION REPORTS VALIDATION==============================
	
	public StringBuffer validateReservationReport(ReservationReport report, SearchObject Sobj, XMLPriceItinerary XMLSelectFlight, AirConfig conf, StringBuffer ReportPrinter)
	{
		XMLPriceInfo xmlprice			= XMLSelectFlight.getPricinginfo();
		
		String searchObjectCurrency		= Sobj.getSellingCurrency();
		//String xmlsupplierCurrency		= xmlprice.getBasefareCurrencyCode();
		
		String xmlsellingCurrency		= xmlprice.getNewBasefareCurrencyCode();
		double xmltotalRate				= xmlprice.getNewbase() + xmlprice.getProfit() + xmlprice.getNewtax();
		double xmlcreditcardFee			= 0; 
		if(Sobj.getPaymentMode().equalsIgnoreCase("Pay Online"))
		{
			xmlcreditcardFee			= xmlprice.getCreditcardfeeinSellCurr();
		}
		
		double xmlbookingFeeAndCharges	= xmlprice.getBookingfee();
		@SuppressWarnings("unused")
		double xmlamountPaid			= 0;
		if(Sobj.getPaymentMode().equalsIgnoreCase("Pay Online"))
		{
			xmlamountPaid				= xmlprice.getProcessnow();
		}
		
		double xmlgrossOrderValue		= xmlcreditcardFee + xmlbookingFeeAndCharges + xmltotalRate;
		double xmlagentCommission		= 0;
		double xmlnetOrderValue			= xmlgrossOrderValue - xmlagentCommission;
		double xmltotalCost				= xmlprice.getNewbase() + xmlprice.getNewtax() + xmlcreditcardFee;
		
		String portalCurrency			= PropertyMap.get("Portal_Currency_Code");
		double portalNetOrderValue		= 0;
		portalNetOrderValue				= Math.ceil(convert(xmlsellingCurrency, portalCurrency, xmlnetOrderValue));
		double portaltotalCost			= 0;
		portaltotalCost					= Math.ceil(convert(xmlsellingCurrency, portalCurrency, xmltotalCost));
		
		//Report Object Variables
		String reportsellingCurrency		= report.getSellingCurrency1();
		double reporttotalRate				= Double.parseDouble(report.getTotalRate());
		double reportcreditcardFee			= Double.parseDouble(report.getCreditcardFee());
		double reportbookingFeeAndCharges	= Double.parseDouble(report.getBookingFeeAndCharges());
		
		double reportamountPaid				= Double.parseDouble(report.getAmountPaid());
		
		double reportgrossOrderValue		= Double.parseDouble(report.getGrossOrderValue());
		double reportagentCommission		= Double.parseDouble(report.getAgentCommission());
		double reportnetOrderValue			= Double.parseDouble(report.getNetOrderValue());
		double reporttotalCost				= Double.parseDouble(report.getTotalCost());
		
		String reportportalCurrency			= report.getBaseCurrency();
		double reportportalNetOrderValue	= Double.parseDouble(report.getBaseNetOrderValue());
		double reportportaltotalCost		= Double.parseDouble(report.getBasetotalCost());
		
		//Validate Selling Currency
		statusPassFail = CommonValidator.compareStringEqualsIgnoreCase(searchObjectCurrency, reportsellingCurrency, "Selling Currency", ReportPrinter, Reportcount);
		Reportcount++;
		
		//Validate TotalRate
		statusPassFail = CommonValidator.compareDouble(xmltotalRate, reporttotalRate, "Total Rate", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		if(Sobj.getPaymentMode().equalsIgnoreCase("Pay Online"))
		{
			statusPassFail = CommonValidator.compareDouble(xmlcreditcardFee, reportcreditcardFee, "Credit Card Fee", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
		}
		
		//Validate Booking Fee and Other Charges
		statusPassFail = CommonValidator.compareDouble(xmlbookingFeeAndCharges, reportbookingFeeAndCharges, "Booking Fee and Other Chargers", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//Validate Amount Paid
		statusPassFail = CommonValidator.compareDouble(reportamountPaid, reportamountPaid, "Amount Paid", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//Validate Gross Order Value
		statusPassFail = CommonValidator.compareDouble(xmlgrossOrderValue, reportgrossOrderValue, "Gross Order Value", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//Validate Agent Commission	
		statusPassFail = CommonValidator.compareDouble(xmlagentCommission, reportagentCommission, "Agent Commission", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//Validate Net Order Value	
		statusPassFail = CommonValidator.compareDouble(xmlnetOrderValue, reportnetOrderValue, "Net Order Value", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//Validate Total Cost
		statusPassFail = CommonValidator.compareDouble(xmltotalCost, reporttotalCost, "Total Cost", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//Validate Portal Currency	
		statusPassFail = CommonValidator.compareStringEqualsIgnoreCase(portalCurrency, reportportalCurrency, "Portal Currency", ReportPrinter, Reportcount);
		Reportcount++;
				
		//Validate Base Net Order Value	
		statusPassFail = CommonValidator.compareDouble(portalNetOrderValue, reportportalNetOrderValue, "Portal Base Net Order Value", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//Validate Base Total Cost	
		statusPassFail = CommonValidator.compareDouble(portaltotalCost, reportportaltotalCost, "Portal Base Total Cost", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		return ReportPrinter;
	}
	
	public StringBuffer validateSupplierPayableReport(SupplierPayableReport beforePay, SupplierPayableReport afterPay, SearchObject Sobj, XMLPriceItinerary XMLSelectFlight, UIConfirmationPage uiconfirmationpage,  ReservationInfo fillingObject, AirConfig conf, StringBuffer ReportPrinter)
	{	
		try {
			statusPassFail = CommonValidator.compareStringEqualsIgnoreCase(uiconfirmationpage.getReservationNo().trim(), beforePay.getReservationNo().trim(), "Reservation Number", ReportPrinter, Reportcount);
			Reportcount++;
		} catch (Exception e) {
			// 
		}
		
		//VALIDATE BOOKING DATE
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
			Calendar cal = Calendar.getInstance();
		    String bd =  sdf.format(cal.getTime());
		    statusPassFail = CommonValidator.compareStringEqualsIgnoreCase(bd, beforePay.getBookingDate(), "Booking Date", ReportPrinter, Reportcount);
		    Reportcount++;
		} catch (Exception e) {
			// 
		}
		
		//VALIDATE BOOKING CHANNEL
	    try {
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Booking Channel</td>"
			+ "<td>"+Sobj.getSearchType()+"</td>");
			if( Sobj.getSearchType().equalsIgnoreCase("Call_Center") && beforePay.getBookingChannel().equalsIgnoreCase("CC") )
			{
				ReportPrinter.append("<td>"+beforePay.getBookingChannel()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else if(Sobj.getSearchType().equalsIgnoreCase("Web") && beforePay.getBookingChannel().equalsIgnoreCase("WEB"))
			{
				ReportPrinter.append("<td>"+beforePay.getBookingChannel()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+beforePay.getBookingChannel()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
		} catch (Exception e) {
			// 
		}
		
		
		//VALIDATE PRODUCT TYPE
		try {
			statusPassFail = CommonValidator.compareStringEqualsIgnoreCase("Air", beforePay.getProductType(), "Product Type", ReportPrinter, Reportcount);
			Reportcount++;
		} catch (Exception e) {
			// 
		}
		
		//VALIDATE PRODUCT NAME
		try {
			statusPassFail = CommonValidator.compareStringContains(XMLSelectFlight.getOriginoptions().get(0).getFlightlist().get(0).getMarketingAirline(), beforePay.getProductName(), "Product Name", ReportPrinter, Reportcount);
			Reportcount++;
		} catch (Exception e) {
			// 
		}
		
		//VALIDATE PRODUCT NAME
		try {
			statusPassFail = CommonValidator.compareStringEqualsIgnoreCase(PropertyMap.get("Flight_Supplier"), beforePay.getSupplierName(), "Supplier Name", ReportPrinter, Reportcount);
			Reportcount++;
		} catch (Exception e) {
			// 
		}
		
		//VALIDATE SUPPLIER CONFIRMATION
		try {
			statusPassFail = CommonValidator.compareStringEqualsIgnoreCase(uiconfirmationpage.getSupplierConfirmationNo().trim(), beforePay.getSupplierConf().trim(), "Supplier Confirmation Number", ReportPrinter, Reportcount);
			Reportcount++;
		} catch (Exception e) {
			// 
		}
		
		//VALIDATE BOOKING STATUS
		try {
			statusPassFail = CommonValidator.compareStringEqualsIgnoreCase("Normal", beforePay.getBookingStatus(), "Booking Status", ReportPrinter, Reportcount);
			Reportcount++;
		} catch (Exception e) {
			// 
		}
		
		//VALIDATE GUEST
		try {
			statusPassFail = CommonValidator.compareStringContains(fillingObject.getAdult().get(0).getGivenName(), beforePay.getGuestName(), "Guest Given Name", ReportPrinter, Reportcount);
			Reportcount++;
		} catch (Exception e) {
			// 
		}
		
		//VALIDATE GUEST
		try {
			statusPassFail = CommonValidator.compareStringContains(fillingObject.getAdult().get(0).getSurname(), beforePay.getGuestName(), "Guest Surname", ReportPrinter, Reportcount);
			Reportcount++;
		} catch (Exception e) {
			// 
		}
		
		//----------------------------------------------VALIDATE COST INFORMATION (PORTAL)------------------------------------------------
		
		//----------------BEFORE PAY--------------------
		try {
			XMLPriceInfo	XMLO		= XMLSelectFlight.getPricinginfo();
			String			basefare	= XMLO.getBasefareAmount();
			String			taxfare		= XMLO.getTaxAmount();
			
			//VALIDATE PORTAL PAYABLE AMOUNT BEFORE PAY
			String currency	= XMLO.getBasefareCurrencyCode();
			double amount	= Double.valueOf(basefare) + Double.valueOf(taxfare);
			amount			= convert(currency, PropertyMap.get("Portal_Currency_Code"), amount);
			statusPassFail	= CommonValidator.compareDouble(amount, Double.valueOf(beforePay.getPortalCu_PayableAmount()), "Payable Amount(In Portal Currency - Before Payment)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));	
			Reportcount++;
			
			//VALIDATE PORTAL BALANCE BEFORE PAY
			statusPassFail	= CommonValidator.compareDouble(amount, Double.valueOf(beforePay.getPortalCu_Balance()), "Balance(In Portal Currency - Before Payment)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
			amount			= Double.valueOf(basefare) + Double.valueOf(taxfare);
			statusPassFail	= CommonValidator.compareDouble(amount, Double.valueOf(beforePay.getSupCu_Payable()), "Payable Amount(In Supplier Currency - Before Payment)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
			//VALIDATE SUPPLIER BALANCE BEFORE PAY
			statusPassFail	= CommonValidator.compareDouble(amount, Double.valueOf(beforePay.getSupCu_Balance()), "Balance(In Supplier Currency - Before Payment)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
			
			if(Sobj.getSupplierPayablePayStatus().equalsIgnoreCase("YES"))
			{
				//----------------AFTER PAY--------------------
				
				//VALIDATE PORTAL PAYABLE AMOUNT AFTER PAY
				String currencyafter	= XMLO.getBasefareCurrencyCode();
				double amountafter		= Double.valueOf(basefare) + Double.valueOf(taxfare);
				amountafter				= convert(currencyafter, PropertyMap.get("Portal_Currency_Code"), amountafter);
				statusPassFail			= CommonValidator.compareDouble(amountafter, Double.valueOf(afterPay.getPortalCu_PayableAmount()), "Payable Amount(In Portal Currency - After Payment)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy")); 
				Reportcount++;
				
				//VALIDATE PORTAL TOTAL PAID AFTER PAY
				statusPassFail			= CommonValidator.compareDouble(amountafter, Double.valueOf(afterPay.getPortalCu_TotalPaid()), "Total Paid(In Portal Currency - After Payment)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
				Reportcount++;
				
				//VALIDATE SUPPLIER PAYABLE AMOUNT AFTER PAY
				amountafter				= Double.valueOf(basefare) + Double.valueOf(taxfare);
				statusPassFail			= CommonValidator.compareDouble(amountafter, Double.valueOf(afterPay.getSupCu_Payable()), "Payable Amount(In Supplier Currency - After Payment)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
				Reportcount++;
				
				//VALIDATE SUPPLIER TOTAL PAID AFTER PAY
				statusPassFail			= CommonValidator.compareDouble(amountafter, Double.valueOf(afterPay.getSupCu_TotaPaid()), "Total Paid(In Supplier Currency - After Payment)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
				Reportcount++;
				
				//VALIDATE PAYMENT HISTORY
				String[] history		= new String[10]; 
				history					= afterPay.getPaymentHistory().split("-");
				
				//VALIDATE AMOUNT IN THE HISTORY
				statusPassFail			= CommonValidator.compareStringEqualsIgnoreCase(String.valueOf(amountafter), history[2], "Amount paid mentioned in the payment history", ReportPrinter, Reportcount);
				Reportcount++;
				
				//VALIDATE PORTAL BALANCE AFTER PAY
				statusPassFail			= CommonValidator.compareStringEqualsIgnoreCase("0.00", beforePay.getPortalCu_Balance(), "Balance(In Portal Currency - After Payment)", ReportPrinter, Reportcount);
				Reportcount++;
				
				//VALIDATE SUPPLIER BALANCE AFTER PAY
				statusPassFail			= CommonValidator.compareStringEqualsIgnoreCase("0.00", afterPay.getSupCu_Balance(), "Balance(In Supplier Currency - After Payment)", ReportPrinter, Reportcount);
				Reportcount++;
			}
				
		} catch (Exception e) {
			// 
		}
		
		return ReportPrinter;
	}
	
	public StringBuffer validateBookingConfEmail(BookingConfReportReader ConfirmationReport, SearchObject Sobj, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, AirConfig conf, StringBuffer ReportPrinter)
	{
		boolean twoway = false;
		if(Sobj.getTriptype().equalsIgnoreCase("Round Trip"))
		{
			twoway = true;
		}
		
		Traveler fillmain = fillingObject.getMaincustomer();
		Traveler reportmain = ConfirmationReport.getMain();
		
		//VALIDATE BOOKING REFERENCE CUSTOMER NAME
		statusPassFail = CommonValidator.compareStringContains(fillmain.getGivenName(), ConfirmationReport.getBookingReference(), "Booiking Reference", ReportPrinter, Reportcount);	
		Reportcount++;
		
		//VALIDATE OUTBOUND FLIGHTS
		int countreportoutbound = ConfirmationReport.getOutbound().size();
		int countxmloutbound = XMLSelectFlight.getOriginoptions().get(0).getFlightlist().size();
		statusPassFail = CommonValidator.compareInt(countxmloutbound, countreportoutbound, "Match report outbound flight count with xml outboundflight count", ReportPrinter, Reportcount);
		Reportcount++;
		
		//VALIDATE INBOUND FLIGHTS
		if(twoway)
		{
			int countreportinbound = ConfirmationReport.getInbound().size();
			int countxmlinbound = XMLSelectFlight.getOriginoptions().get(1).getFlightlist().size();
			statusPassFail = CommonValidator.compareInt(countxmlinbound, countreportinbound, "Match report inbound flight count with xml outboundflight count", ReportPrinter, Reportcount);
			Reportcount++;
		}
		
		//VALIDATE DIRECT CUSTOMER
		ReportPrinter = validatePassengers(fillmain, reportmain, ReportPrinter);
		
		//--------------------------------VALIDATE PASSENGERS--------------------------------
		Traveler aT = new Traveler();
		Traveler bT = new Traveler();
		//ADULTS
		try {
			statusPassFail = CommonValidator.compareInt(fillingObject.getAdult().size(), ConfirmationReport.getAdults().size(), "Match reports Adult count with Xml Adult count", ReportPrinter, Reportcount);
			Reportcount++;
			if(statusPassFail)
			{
				int count = fillingObject.getAdult().size();
				/*for(int y=0; y<count; y++)
				{
					ReportPrinter = validatePassengers(ConfirmationReport.getAdults().get(y), fillingObject.getAdult().get(y), ReportPrinter);
				}*/
				for(int i=0; i<count; i++)
				{
					aT = ConfirmationReport.getAdults().get(i);
					for(int y=0; y<count; y++)
					{
						bT = fillingObject.getAdult().get(y);
						if(aT.getGivenName().trim().equalsIgnoreCase(bT.getGivenName().trim()))
						{
							ReportPrinter = validatePassengers(aT, bT, ReportPrinter);
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//CHILDREN
		try {
			statusPassFail = CommonValidator.compareInt(fillingObject.getChildren().size(), ConfirmationReport.getChildren().size(), "Match reports Children count with Test Children count", ReportPrinter, Reportcount);
			Reportcount++;
			if(statusPassFail)
			{
				int count = fillingObject.getChildren().size();
				/*for(int y=0; y<count; y++)
				{
					ReportPrinter = validatePassengers(ConfirmationReport.getChildren().get(y), fillingObject.getChildren().get(y), ReportPrinter);
				}*/
				for(int i=0; i<count; i++)
				{
					aT = ConfirmationReport.getChildren().get(i);
					for(int y=0; y<count; y++)
					{
						bT = fillingObject.getChildren().get(y);
						if(aT.getGivenName().trim().equalsIgnoreCase(bT.getGivenName().trim()))
						{
							ReportPrinter = validatePassengers(aT, bT, ReportPrinter);
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//INFANTS
		try {
			statusPassFail = CommonValidator.compareInt(fillingObject.getInfant().size(), ConfirmationReport.getInfants().size(), "Match reports Infants count with Test Infants count", ReportPrinter, Reportcount);
			Reportcount++;
			if(statusPassFail)
			{
				int count = fillingObject.getInfant().size();
				/*for(int y=0; y<count; y++)
				{
					ReportPrinter = validatePassengers(ConfirmationReport.getInfants().get(y), fillingObject.getInfant().get(y), ReportPrinter);
				}*/
				for(int i=0; i<count; i++)
				{
					aT = ConfirmationReport.getInfants().get(i);
					for(int y=0; y<count; y++)
					{
						bT = fillingObject.getInfant().get(y);
						if(aT.getGivenName().trim().equalsIgnoreCase(bT.getGivenName().trim()))
						{
							ReportPrinter = validatePassengers(aT, bT, ReportPrinter);
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//---------------------------------VALIDATE COST INFORMATION------------------------------------
		//VALIDATE CURRENCY
		statusPassFail = CommonValidator.compareStringEqualsIgnoreCase(XMLSelectFlight.getPricinginfo().getNewBasefareCurrencyCode(), ConfirmationReport.getPaymentCurrency(), "Check Currency", ReportPrinter, Reportcount);
		Reportcount++;
		
		//VALIDATE TOTAL BEFORE TAXES
		statusPassFail = CommonValidator.compareDouble((XMLSelectFlight.getPricinginfo().getNewbase()+XMLSelectFlight.getPricinginfo().getProfit()), Double.parseDouble(ConfirmationReport.getTotalBeforeTax().trim()), "Total before Tax", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//VALIDATE TAX
		statusPassFail = CommonValidator.compareDouble(XMLSelectFlight.getPricinginfo().getNewtax(), Double.parseDouble(ConfirmationReport.getTax().trim()), "Tax", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//VALIDATE BOOKING FEE
		statusPassFail = CommonValidator.compareDouble(XMLSelectFlight.getPricinginfo().getBookingfee(), Double.parseDouble(ConfirmationReport.getBookingFee().trim()), "Booking Fee", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//VALIDATE TOTAL AMOUNT
		double XMLFinalBookingValue  = XMLSelectFlight.getPricinginfo().getNewbase() + XMLSelectFlight.getPricinginfo().getProfit() + XMLSelectFlight.getPricinginfo().getNewtax() + XMLSelectFlight.getPricinginfo().getBookingfee();
		statusPassFail = CommonValidator.compareDouble(XMLFinalBookingValue, Double.valueOf(ConfirmationReport.getTotalAmount().trim()), "Total Amount", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//VALIDATE TOTAL AMOUNT
		double XMLSubtotal			 = XMLSelectFlight.getPricinginfo().getNewbase() + XMLSelectFlight.getPricinginfo().getProfit();
		statusPassFail = CommonValidator.compareDouble(XMLSubtotal, Double.parseDouble(ConfirmationReport.getSubTotal().trim()), "Subtotal", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//VALIDATE TAX AND OTHER CHARGES
		double XMLTaxAndOther = XMLSelectFlight.getPricinginfo().getNewtax() + XMLSelectFlight.getPricinginfo().getBookingfee() + XMLSelectFlight.getPricinginfo().getCreditcardfeeinSellCurr();
		statusPassFail = CommonValidator.compareDouble(XMLTaxAndOther, Double.parseDouble(ConfirmationReport.getTotalTaxAndOther().trim()), "Tax and Other chrages", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		double XMLprocessnow = 0;
		//VALIDATE AMOUNT PROCESS NOW
		if(conf.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Full_Amount_at_Booking"))
		{
			XMLprocessnow = XMLSelectFlight.getPricinginfo().getNewbase()+XMLSelectFlight.getPricinginfo().getNewtax()+XMLSelectFlight.getPricinginfo().getProfit()+XMLSelectFlight.getPricinginfo().getBookingfee()+XMLSelectFlight.getPricinginfo().getCreditcardfeeinSellCurr();
		}
		else if(conf.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Booking_Fee_as_Deposit"))
		{
			XMLprocessnow = XMLSelectFlight.getPricinginfo().getBookingfee()+XMLSelectFlight.getPricinginfo().getCreditcardfeeinSellCurr();
		}
		statusPassFail = CommonValidator.compareDouble(XMLprocessnow, Double.parseDouble(ConfirmationReport.getAmountToBePaidNow().trim()), "Amount process now", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		return ReportPrinter;
	}

	public StringBuffer validateBookingListReport(BookingListReport bookinglistreport, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, SearchObject Sobj, UIConfirmationPage uiconfirmationpage, AirConfig conf, StringBuffer ReportPrinter )
	{
		boolean twoway = false;
		if(Sobj.getTriptype().equalsIgnoreCase("Round Trip"))
		{
			twoway = true;
		}
		
		//VALIDATE RESERVATION NO
		statusPassFail = CommonValidator.compareStringContains(uiconfirmationpage.getReservationNo(), bookinglistreport.getReservationNo(), "Reservation No(Reservation Summary)", ReportPrinter, Reportcount);
		Reportcount++;
		
		//VALIDATE ORIGIN
		String origin = XMLSelectFlight.getOriginoptions().get(0).getFlightlist().get(0).getDeparture_port();
		statusPassFail = CommonValidator.compareStringContains(origin, bookinglistreport.getOrigin(), "Origin(Reservation Summary)", ReportPrinter, Reportcount);
		Reportcount++;
		
		//VALIDATE DESTINATION
		int last =  XMLSelectFlight.getOriginoptions().get(0).getFlightlist().size() - 1 ;
		String destination = XMLSelectFlight.getOriginoptions().get(0).getFlightlist().get(last).getArrival_port();
		statusPassFail = CommonValidator.compareStringContains(destination, bookinglistreport.getDestination(), "Destination(Reservation Summary)", ReportPrinter, Reportcount);
		Reportcount++;
		
		//VALIDATE CANCELLATION DEADLINE
		String sdate = "";
		sdate = XMLSelectFlight.getTicketTimeLimit().split("T")[0].trim();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		Date date;
		Calendar cal = Calendar.getInstance();
		
		try 
		{
			date = sdf1.parse(sdate);
			sdate = sdf.format(date);
			date = sdf.parse(sdate);
			cal.setTime(date);
			cal.add(Calendar.DATE, -3);
			String a = bookinglistreport.getCancellationDeadline().trim();
			String b = sdf.format(cal.getTime());
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Cancellation Date(Reservation Summary)</td>"
			+ "<td>"+b+"</td>");
			if(a.equals(b))
			{
				ReportPrinter.append("<td>"+bookinglistreport.getCancellationDeadline()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+bookinglistreport.getCancellationDeadline()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			/*statusPassFail = CommonValidator.compareStringEqualsIgnoreCase(b, a, "Cancellation Date(Reservation Summary)", ReportPrinter, Reportcount);
			Reportcount++;*/
		} 
		catch (ParseException e) 
		{
			e.printStackTrace();
			//System.out.println(sdf.format(cal.getTime()));
			ReportPrinter.append("<td>Parse Exception.! Cancellation date is : "+bookinglistreport.getCancellationDeadline()+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}
		
		//VALIDATE TICKET TIME LIMIT
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>Check Ticket Time limit(Reservation Summery)</td>"
		+ "<td>"+XMLSelectFlight.getTicketTimeLimit().substring(0,10)+"</td>");
		String ttlimit = "";
		ttlimit = bookinglistreport.getEticketingDeadline().trim();
		Date datettlimit;
		try
		{
			datettlimit = new SimpleDateFormat("dd/MM/yyyy").parse(ttlimit);
			ttlimit = new SimpleDateFormat("yyyy-MM-dd").format(datettlimit);
			if( ttlimit.equalsIgnoreCase(XMLSelectFlight.getTicketTimeLimit().substring(0,10)) )
			{
				ReportPrinter.append("<td>"+ttlimit+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+ttlimit+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
		}
		catch (ParseException e)
		{
			e.printStackTrace();
			ReportPrinter.append("<td>"+ttlimit+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}
		
		//VALIDATE DIRECT MAIN CUSTOMER
		ReportPrinter = validatePassengers(bookinglistreport.getMaincus(), fillingObject.getMaincustomer(), ReportPrinter);
		
		//--------------------------------------------VALIDATE FLIGHTS--------------------------------------
		//VALIDATE OUTBOUND FLIGHTS
		try {
			int xmloutcount		= XMLSelectFlight.getOriginoptions().get(0).getFlightlist().size();
			int reportoutcount	= bookinglistreport.getOutbound().getOutBflightlist().size();
			statusPassFail = CommonValidator.compareInt(xmloutcount, reportoutcount, "Report Outbound fligh count and XML Outbound flight count", ReportPrinter, Reportcount);
			Reportcount++;
			if(statusPassFail)
			{
				ReportPrinter = validateFlights(reportoutcount, "Outbound", bookinglistreport.getOutbound().getOutBflightlist(), XMLSelectFlight.getOriginoptions().get(0).getFlightlist(), ReportPrinter);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//VALIDATE INBOUND FLIGHT
		int xmlincount		= 0;
		int reportincount	= 0;
		if(twoway)
		{
			try {
				xmlincount		= XMLSelectFlight.getOriginoptions().get(1).getFlightlist().size();
				reportincount	= bookinglistreport.getInbound().getInBflightlist().size();
				statusPassFail	= CommonValidator.compareInt(xmlincount, reportincount, "Report Inbound fligh count and XML Inbound flight count", ReportPrinter, Reportcount);
				Reportcount++;
				if(statusPassFail)
				{
					ReportPrinter = validateFlights(reportincount, "Inbound",bookinglistreport.getInbound().getInBflightlist(), XMLSelectFlight.getOriginoptions().get(1).getFlightlist() , ReportPrinter);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}			
		}
		
		//-------------------------------------------VALIDATE PASSENGERS------------------------------------------
		ArrayList<Traveler> travelers = new ArrayList<Traveler>();
		travelers.addAll(fillingObject.getAdult());
		travelers.addAll(fillingObject.getChildren());
		travelers.addAll(fillingObject.getInfant());
		for(int a=0; a<travelers.size(); a++)
		{
			Traveler bT = new Traveler();
			bT = travelers.get(a);
			
			for(int b=0; b<bookinglistreport.getTravelers().size(); b++)
			{
				Traveler aT = new Traveler();
				aT = bookinglistreport.getTravelers().get(b);
				
				if( aT.getGivenName().equalsIgnoreCase(bT.getGivenName()) && aT.getSurname().equalsIgnoreCase(bT.getSurname()) )
				{
					ReportPrinter = validatePassengers(aT, bT, ReportPrinter);
				}
			}
		}
		
		//----------------------------------VALIDTAE COST INFORMATION RESERVATION SUMMEREY----------------------------------------------------
		//VALIDATE CURRENCY
		statusPassFail = CommonValidator.compareStringEqualsIgnoreCase(XMLSelectFlight.getPricinginfo().getNewBasefareCurrencyCode().trim(), bookinglistreport.getCurrencyCode().trim(), "Check Currency(Reservation Summery)", ReportPrinter, Reportcount);
		Reportcount++;
		
		//VALIDATE SUBTOTAL BEFORE TAXES
		double x = XMLSelectFlight.getPricinginfo().getNewbase()+XMLSelectFlight.getPricinginfo().getProfit();
		statusPassFail = CommonValidator.compareDouble(x, Double.parseDouble(bookinglistreport.getSubtotalBeforeTax()), "Total before Tax(Reservation Summery)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//VALIDATE BOOKING FEE
		statusPassFail = CommonValidator.compareDouble(XMLSelectFlight.getPricinginfo().getBookingfee(), Double.parseDouble(bookinglistreport.getBookingFee()), "Booking Fee(Reservation Summery)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//VALIDATE TAX AND OTHER CHARGES
		double XMLTaxAndOther = 0;
		if(Sobj.getPaymentMode().equalsIgnoreCase("Pay Online"))
		{
			XMLTaxAndOther = XMLSelectFlight.getPricinginfo().getNewtax() /*+ XMLSelectFlight.getPricinginfo().getBookingfee() + XMLSelectFlight.getPricinginfo().getCreditcardfeeinSellCurr()*/;
		}
		else
		{
			XMLTaxAndOther = XMLSelectFlight.getPricinginfo().getNewtax() /*+ XMLSelectFlight.getPricinginfo().getBookingfee()*/;
		}
		statusPassFail = CommonValidator.compareDouble(XMLTaxAndOther, Double.parseDouble(bookinglistreport.getTaxandOther()), "Tax and Other chrages(Reservation Summery)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//VALIDATE CREDIT CARD FEE
		if(Sobj.getPaymentMode().equalsIgnoreCase("Pay Online"))
		{
			statusPassFail = CommonValidator.compareDouble(XMLSelectFlight.getPricinginfo().getCreditcardfeeinSellCurr(), Double.parseDouble(bookinglistreport.getCreditCardFee()), "Credit Card Fee(Reservation Summery)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
		}
		
		//VALIDATE TOTAL BOOKING VALUE
		double XMLFinalBookingValue  = 0;
		if(Sobj.getPaymentMode().equalsIgnoreCase("Pay Online"))
		{
			XMLFinalBookingValue  = XMLSelectFlight.getPricinginfo().getNewbase() + XMLSelectFlight.getPricinginfo().getProfit() + XMLSelectFlight.getPricinginfo().getNewtax() + XMLSelectFlight.getPricinginfo().getBookingfee() + XMLSelectFlight.getPricinginfo().getCreditcardfeeinSellCurr();
		}
		else
		{
			XMLFinalBookingValue  = XMLSelectFlight.getPricinginfo().getNewbase() + XMLSelectFlight.getPricinginfo().getProfit() + XMLSelectFlight.getPricinginfo().getNewtax() + XMLSelectFlight.getPricinginfo().getBookingfee();
		}
		
		//VALIDATING FINALBOOKING VALUE
		statusPassFail = CommonValidator.compareDouble(XMLFinalBookingValue, Double.parseDouble(bookinglistreport.getTotalBookingValue()), "Total Booking Value(Reservation Summery)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//VALIDATE AMOUNT PAY UPFRONT
		//double XMLprocessnow = XMLSelectFlight.getPricinginfo().getProcessnow();
		statusPassFail = CommonValidator.compareDouble(XMLFinalBookingValue, Double.parseDouble(bookinglistreport.getAmountPayUpfront()), "Amount Paid Upfront(Reservation Summery)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//------------------------------------------------------VALIDATE FINAL FLIGHT COST INFORMATION --------------------------------------------------------
		//VALIDATE FINAL CURRENCY
		statusPassFail = CommonValidator.compareStringEqualsIgnoreCase(XMLSelectFlight.getPricinginfo().getNewBasefareCurrencyCode(), bookinglistreport.getFinalCurrency(), "Amount Paid Upfront(Reservation Summery)", ReportPrinter, Reportcount);
		Reportcount++;
						
		//VALIDATE FINAL SUBTOTAL BEFORE TAXES
		double y = XMLSelectFlight.getPricinginfo().getNewbase()+XMLSelectFlight.getPricinginfo().getProfit();
		statusPassFail = CommonValidator.compareDouble(y, Double.parseDouble(bookinglistreport.getFinalSubtotalBeforeTax()), "Total before Tax(Flight Cost Details)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//VALIDATE FINAL BOOKING FEE
		statusPassFail = CommonValidator.compareDouble(XMLSelectFlight.getPricinginfo().getBookingfee(), Double.parseDouble(bookinglistreport.getFinalBookingFee()), "Booking Fee(Flight Cost Details)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
				
		//VALIDATE FINAL TAX AND OTHER CHARGES
		double XMLFinalTaxAndOther = 0;
		if(Sobj.getPaymentMode().equalsIgnoreCase("Pay Online"))
		{
			XMLFinalTaxAndOther = XMLSelectFlight.getPricinginfo().getNewtax() /*+ XMLSelectFlight.getPricinginfo().getBookingfee() + XMLSelectFlight.getPricinginfo().getCreditcardfeeinSellCurr()*/;
		}
		else
		{
			XMLFinalTaxAndOther = XMLSelectFlight.getPricinginfo().getNewtax()/* + XMLSelectFlight.getPricinginfo().getBookingfee()*/;
		}
		statusPassFail = CommonValidator.compareDouble(XMLFinalTaxAndOther, Double.parseDouble(bookinglistreport.getFinalTaxAndOtherCharges()), "Tax and Other chrages(Flight Cost Details)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
				
		//VALIDATE FINAL TOTAL BOOKING VALUE
		double XMLFinalTotalBookingValue = 0;
		if(Sobj.getPaymentMode().equalsIgnoreCase("Pay Online"))
		{
			if( conf.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Full_Amount_at_Booking"))
			{
				XMLFinalTotalBookingValue = XMLSelectFlight.getPricinginfo().getNewbase() + XMLSelectFlight.getPricinginfo().getProfit() + XMLSelectFlight.getPricinginfo().getNewtax() + XMLSelectFlight.getPricinginfo().getBookingfee()/* + XMLSelectFlight.getPricinginfo().getCreditcardfeeinSellCurr()*/;
			}
			else if(conf.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Booking_Fee_as_Deposit"))
			{
				XMLFinalTotalBookingValue = XMLSelectFlight.getPricinginfo().getBookingfee();
			}
		}
		else
		{
			XMLFinalTotalBookingValue = XMLSelectFlight.getPricinginfo().getNewbase() + XMLSelectFlight.getPricinginfo().getProfit() + XMLSelectFlight.getPricinginfo().getNewtax() + XMLSelectFlight.getPricinginfo().getBookingfee();
		}
		statusPassFail = CommonValidator.compareDouble(XMLFinalTotalBookingValue, Double.parseDouble(bookinglistreport.getFinalTotalBookingValue()), "Final Total Booking Value(Flight Cost Details)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//VALIDATE FINAL AMOUNT PAY UPFRONT
		statusPassFail = CommonValidator.compareDouble(XMLFinalBookingValue, Double.parseDouble(bookinglistreport.getFinalAmountPayUpfront()), "Final Amount Paid Upfront(Flight Cost Details)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		return ReportPrinter;
	}

	public StringBuffer validateCustomerVoucherEmail(CustomerVoucherEmail email, ReservationInfo fillingObject, UIConfirmationPage uiconfirmationpage, XMLPriceItinerary XMLSelectFlight, SearchObject Sobj, AirConfig conf, StringBuffer ReportPrinter)
	{
		boolean twoway = false;
		if( Sobj.getTriptype().equalsIgnoreCase("Round Trip") )
		{
			twoway = true;
		}
		
		//VALIDATE LEAD PASSENGER 
		statusPassFail = CommonValidator.compareStringContains(fillingObject.getMaincustomer().getGivenName(), email.getLeadPassenger(), "Lead Passenger Given Name", ReportPrinter, Reportcount);
		Reportcount++;
		statusPassFail = CommonValidator.compareStringContains(fillingObject.getMaincustomer().getSurname(), email.getLeadPassenger(), "Lead Passenger Given Name", ReportPrinter, Reportcount);
		Reportcount++;
		
		//VALIDATE BOOKING NO 
		statusPassFail = CommonValidator.compareStringContains(uiconfirmationpage.getReservationNo(), email.getBookingNo(), "Booking No", ReportPrinter, Reportcount);
		Reportcount++;
		
		//VALIDATE ADULT COUNT
		statusPassFail = CommonValidator.compareInt(fillingObject.getAdult().size(), Integer.valueOf(email.getAdultCount().trim()), "Adult count", ReportPrinter, Reportcount);
		Reportcount++;
		
		//VALIDATE CHILD COUNT
		statusPassFail = CommonValidator.compareInt(fillingObject.getChildren().size(), Integer.valueOf(email.getChildrenCount().trim()), "Child Count", ReportPrinter, Reportcount);
		Reportcount++;
		
		//VALIDATE INFANT COUNT
		statusPassFail = CommonValidator.compareInt(fillingObject.getInfant().size(), Integer.valueOf(email.getInfantCount().trim()), "Infant Count", ReportPrinter, Reportcount);
		Reportcount++;
		
		//VALIDATE OUTBOUND FLIGHTS
		int xmloutcount		= XMLSelectFlight.getOriginoptions().get(0).getFlightlist().size();
		int reportoutcount	= email.getOutbound().size();			
		try {
			statusPassFail = CommonValidator.compareInt(xmloutcount, reportoutcount, "Customer Voucher Email Outbound flight count and XML Outbound flight count", ReportPrinter, Reportcount);
			Reportcount++;
			if(statusPassFail)
			{
				ReportPrinter = validateFlights(reportoutcount, "Outbound", email.getOutbound(), XMLSelectFlight.getOriginoptions().get(0).getFlightlist(), ReportPrinter);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
				
		//VALIDATE INBOUND FLIGHT
		int xmlincount		= 0;
		int reportincount	= 0;
		if(twoway)
		{
			xmlincount		= XMLSelectFlight.getOriginoptions().get(1).getFlightlist().size();
			reportincount	= email.getInbound().size();
			try {
				statusPassFail = CommonValidator.compareInt(xmlincount, reportincount, "Report Inbound fligh count and XML Inbound flight count", ReportPrinter, Reportcount);
				Reportcount++;
				if(statusPassFail)
				{
					ReportPrinter = validateFlights(reportincount, "Inbound", email.getInbound(), XMLSelectFlight.getOriginoptions().get(1).getFlightlist(), ReportPrinter);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}			
		}
		
		//--------------------------------VALIDATE PASSENGERS--------------------------------
		Traveler aT = new Traveler();
		Traveler bT = new Traveler();
		//ADULTS
		try 
		{
			statusPassFail = CommonValidator.compareInt(fillingObject.getAdult().size(), Integer.valueOf(email.getAdultCount().trim()), "Adult count", ReportPrinter, Reportcount);
			Reportcount++;
			if(statusPassFail)
			{
				int count = fillingObject.getAdult().size();
				/*for(int y=0; y<count; y++)
				{
					ReportPrinter = validatePassengers(email.getAdults().get(y), fillingObject.getAdult().get(y), ReportPrinter);
				}*/
				for(int i=0; i<count; i++)
				{
					aT = email.getAdults().get(i);
					for(int y=0; y<count; y++)
					{
						bT = fillingObject.getAdult().get(y);
						if(aT.getGivenName().trim().equalsIgnoreCase(bT.getGivenName().trim()))
						{
							ReportPrinter = validatePassengers(aT, bT, ReportPrinter);
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//CHILDREN
		try {
			statusPassFail = CommonValidator.compareInt(fillingObject.getChildren().size(), Integer.valueOf(email.getChildrenCount().trim()), "Child Count", ReportPrinter, Reportcount);
			Reportcount++;
			if(statusPassFail)
			{
				int count = fillingObject.getChildren().size();
				/*for(int y=0; y<count; y++)
				{
					ReportPrinter = validatePassengers(email.getChildren().get(y), fillingObject.getChildren().get(y), ReportPrinter);
				}*/
				for(int i=0; i<count; i++)
				{
					aT = email.getChildren().get(i);
					for(int y=0; y<count; y++)
					{
						bT = fillingObject.getChildren().get(y);
						if(aT.getGivenName().trim().equalsIgnoreCase(bT.getGivenName().trim()))
						{
							ReportPrinter = validatePassengers(aT, bT, ReportPrinter);
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//INFANTS
		try {
			statusPassFail = CommonValidator.compareInt(fillingObject.getInfant().size(), Integer.valueOf(email.getInfantCount().trim()), "Infant Count", ReportPrinter, Reportcount);
			Reportcount++;
			if(statusPassFail)
			{
				int count = fillingObject.getInfant().size();
				/*for(int y=0; y<count; y++)
				{
					ReportPrinter = validatePassengers(email.getInfants().get(y), fillingObject.getInfant().get(y), ReportPrinter);
				}*/
				for(int i=0; i<count; i++)
				{
					aT = email.getInfants().get(i);
					for(int y=0; y<count; y++)
					{
						bT = fillingObject.getInfant().get(y);
						if(aT.getGivenName().trim().equalsIgnoreCase(bT.getGivenName().trim()))
						{
							ReportPrinter = validatePassengers(aT, bT, ReportPrinter);
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return ReportPrinter;
	}

	public StringBuffer validateProfitAndLoss(XMLPriceItinerary XMLSelectFlight, SearchObject Sobj, ProfitAndLoss profitlossReport, AirConfig conf, StringBuffer ReportPrinter)
	{
		XMLPriceInfo XML	= new XMLPriceInfo();
		XML					= XMLSelectFlight.getPricinginfo();
		
		String PortalCurrency			= PropertyMap.get("Portal_Currency_Code");
		XMLSelectFlight.getPricinginfo().setPortalCurrencyCode(PortalCurrency);
		String SupplierCurrency			= XML.getBasefareCurrencyCode();
		String SellingCurrency			= XML.getNewBasefareCurrencyCode();
		
		double xmlbaseinPortCurr		= 0;
		double xmltaxinPortCurr			= 0;
		double xmlbookfeeinPortCurr		= 0;
		double xmlcreditfeeinPortCurr	= 0;
		double xmlprofit				= 0;
		//double xmlprofitinPortCurr		= 0;
		
		xmlbaseinPortCurr				= convert( SupplierCurrency, PortalCurrency, Double.parseDouble(XML.getBasefareAmount()) );
		XMLSelectFlight.getPricinginfo().setBaseFareInPortalCurrency(xmlbaseinPortCurr);
		
		xmltaxinPortCurr				= convert( SupplierCurrency, PortalCurrency, Double.parseDouble(XML.getTaxAmount()) );
		XMLSelectFlight.getPricinginfo().setTaxFareInPortalCurrency(xmltaxinPortCurr);
		
		xmlbookfeeinPortCurr			= convert( SellingCurrency, PortalCurrency, XML.getBookingfee() );
		XMLSelectFlight.getPricinginfo().setBookingFeeInPortalCurrency(xmlbookfeeinPortCurr);
		
		xmlprofit						= convert(SellingCurrency, PortalCurrency, XML.getProfit());
		XMLSelectFlight.getPricinginfo().setProfitInPortalCurrency(Math.ceil(xmlprofit));
		
		
		if(Sobj.getPaymentMode().equalsIgnoreCase("Pay Online"))
		{
			xmlcreditfeeinPortCurr			= convert( SellingCurrency, PortalCurrency, XML.getCreditcardfeeinSellCurr() );
			XMLSelectFlight.getPricinginfo().setCreditcardFeeInPortalCurrency(xmlcreditfeeinPortCurr);
		}
		
		//xmlprofitinPortCurr				= convert( SellingCurrency, PortalCurrency, XML.getProfit());
		
		double xmlGrossBValinPortCurr	= 0;
		double xmlAgentCommission		= 0;
		double xmlNetBValinPortCurr		= 0;
		double xmlCostfSalesinPortCurr	= 0;
		double xmlProfitinPortCurr		= 0;
		double xmlProfitPercentage		= 0;
		//double xmlProfitValue			= 0;
		String xmlProfitPercentageStr	= "0";
		
		xmlGrossBValinPortCurr			= xmlbaseinPortCurr + xmltaxinPortCurr + xmlbookfeeinPortCurr + xmlcreditfeeinPortCurr + xmlprofit;
		xmlNetBValinPortCurr			= (xmlbaseinPortCurr + xmltaxinPortCurr + xmlbookfeeinPortCurr + xmlcreditfeeinPortCurr + xmlprofit) - xmlAgentCommission;
		xmlCostfSalesinPortCurr			= xmlbaseinPortCurr + xmltaxinPortCurr + xmlcreditfeeinPortCurr;
		xmlProfitinPortCurr				= xmlNetBValinPortCurr - xmlCostfSalesinPortCurr;
		xmlProfitPercentage				= (xmlProfitinPortCurr * 100)/xmlCostfSalesinPortCurr;
		
		
		try {
			xmlProfitPercentageStr		= df.format(xmlProfitPercentage);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//ReportPrinter = compareDouble(xmlGrossBValinPortCurr, Double.valueOf(profitlossReport.getGrossBookingVal()), "Gross Booking Value", ReportPrinter);
		statusPassFail = CommonValidator.compareDouble(xmlGrossBValinPortCurr, Double.valueOf(profitlossReport.getGrossBookingVal()), "Gross Booking Value", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//ReportPrinter = compareDouble(xmlNetBValinPortCurr, Double.valueOf(profitlossReport.getNetBookingVal()), "NetBooking Value", ReportPrinter);
		statusPassFail = CommonValidator.compareDouble(xmlNetBValinPortCurr, Double.valueOf(profitlossReport.getNetBookingVal()), "NetBooking Value", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//ReportPrinter = compareDouble(xmlCostfSalesinPortCurr, Double.valueOf(profitlossReport.getCostOfSales()), "Cost of Sales", ReportPrinter);
		statusPassFail = CommonValidator.compareDouble(xmlCostfSalesinPortCurr, Double.valueOf(profitlossReport.getCostOfSales()), "Cost of Sales", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//ReportPrinter = compareDouble(Math.ceil(Double.valueOf(profitlossReport.getProfit())), Double.valueOf(profitlossReport.getProfit()), "Profit Value", ReportPrinter);
		statusPassFail = CommonValidator.compareDouble(Math.ceil(Double.valueOf(profitlossReport.getProfit())), Double.valueOf(profitlossReport.getProfit()), "Profit Value", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		try {
			String reportPercentage = "0";
			reportPercentage = profitlossReport.getProfitPerecent().replace("%", "");
			statusPassFail = CommonValidator.compareDouble(Double.parseDouble(xmlProfitPercentageStr), Double.parseDouble(reportPercentage), "Profit Percentage (%)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			//statusPassFail = CommonValidator.compareDouble(Math.ceil(Double.parseDouble(xmlProfitPercentageStr)), Double.parseDouble(reportPercentage), "Profit Percentage (%)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
		} catch (Exception e) {
			
		}
		
		return ReportPrinter;
	}


	
	
	//===========================CANCELLATION DETAILS VALIDATION=============================
	
	//===========================CANCELLATION DETAILS VALIDATION=============================
	
	public StringBuffer validateCancellationScreen(XMLPriceItinerary XMLSelectFlight, SearchObject Sobj, ReservationInfo fillingObject, UIConfirmationPage uiconfirmationpage, CancellationScreen Cancellation, StringBuffer ReportPrinter)
	{
		boolean twoway = false;
		if(Sobj.getTriptype().equalsIgnoreCase("Round Trip"))
		{
			twoway = true;
		}
		
		//CURRENCY CODE VALIDATION
		String	ActualDisplayedCurrencyCode		= "";	
		ActualDisplayedCurrencyCode				= Cancellation.getTotalCurrecy();
		String	ExpectedDisplayedCurrencyCode	= "";	
		ExpectedDisplayedCurrencyCode			= Sobj.getSellingCurrency()/*PropertyMap.get("Portal_Currency_Code")*/;
				
		ReportPrinter = compareStringEquals(ActualDisplayedCurrencyCode, ExpectedDisplayedCurrencyCode, "Displayed Currency Code", ReportPrinter);
		Reportcount++;
		
		//DATE 
		String	strCancellingDate			= "";	
		strCancellingDate					= Cancellation.getStrCancellingDate().trim();		
		String	strSupplierCancellationDate	= "";	
		strSupplierCancellationDate			= XMLSelectFlight.getTicketTimeLimit().trim().split("T")[0];		
		String	strPortalCancellationDate	= "";	
		strPortalCancellationDate			= CommonValidator.getCancellationDate(strSupplierCancellationDate, "yyyy-MM-dd");
				
		Date	dtCancellingDate			= null;	
		dtCancellingDate					= CommonValidator.getDateInCommonDateFormat(strCancellingDate, "yyyy-MM-dd");
		Date	dtPortalCancellationDate	= null;	
		dtPortalCancellationDate			= CommonValidator.getDateInCommonDateFormat(strPortalCancellationDate, "yyyy-MM-dd");
		Date	dtSupplierCancellationDate	= null;	
		dtSupplierCancellationDate			= CommonValidator.getDateInCommonDateFormat(strSupplierCancellationDate, "yyyy-MM-dd");

		double	profit						= 0;	
		profit								= XMLSelectFlight.getPricinginfo().getProfit();
		double	bookingFee					= 0;	
		bookingFee							= XMLSelectFlight.getPricinginfo().getBookingfee();
		double	baseFare					= 0;	
		baseFare							= Double.valueOf(XMLSelectFlight.getPricinginfo().getNewbase());
		double	creditCardFee				= 0;
		
		if(Sobj.getPaymentMode().equalsIgnoreCase("Pay Online"))
			creditCardFee					= Double.valueOf(XMLSelectFlight.getPricinginfo().getCreditcardfeeinSellCurr());
		
		double	taxes						= 0;	
		taxes								= Double.valueOf(XMLSelectFlight.getPricinginfo().getNewtax());


		double	caseExpectedSupCancelChrg	= 0;
		double  caseExpectedBalncCancelChrg	= 0;
		double	caseExpectedAdditCanclChrg	= 0;	
		double	caseExpectedTotalChrg		= 0;	
		
		
		//ACTUAL CANCELLATION SCREEN DETAILS
		double	ActualSupplierCancelChrg	= 0;
		double	ActualBalanceCancelChrg		= 0;
		double	ActualAdditCanclChrg		= 0;	
		double	ActualTotalChrg				= 0;	
		
		try {
			ActualSupplierCancelChrg		= Double.valueOf(Cancellation.getSupplierCancelChrg());
			ActualBalanceCancelChrg			= Double.valueOf(Cancellation.getBalanceCancelChrg());
			ActualAdditCanclChrg			= Double.valueOf(Cancellation.getAdditionalCancelChrg());
			ActualTotalChrg					= Double.valueOf(Cancellation.getTotalChrg());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if( dtCancellingDate != null && dtPortalCancellationDate != null && dtSupplierCancellationDate!= null /*&& ActualportalCurrencyCode.equals(ExpectedlportalCurrencyCode) */)
		{
			//CASE 1 
			if(dtPortalCancellationDate.compareTo(dtCancellingDate) > 0 )
			{
				caseExpectedSupCancelChrg	= 0;
				caseExpectedBalncCancelChrg	= 0;
				caseExpectedAdditCanclChrg	= bookingFee + creditCardFee;
				caseExpectedTotalChrg		= caseExpectedSupCancelChrg + caseExpectedBalncCancelChrg + caseExpectedAdditCanclChrg;
				//caseExpectedSupplierGet		= 0;
				//caseExpectedPortalGet		= bookingFee;
				//caseExpectedCustomerGet		= baseFare + profit + taxes;
				
				statusPassFail = CommonValidator.compareDouble(caseExpectedSupCancelChrg, ActualSupplierCancelChrg, "Supplier Cancellation Charge", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
				Reportcount++;
				statusPassFail = CommonValidator.compareDouble(caseExpectedBalncCancelChrg, ActualBalanceCancelChrg, "Customer Cancellation Charge", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
				Reportcount++;
				statusPassFail = CommonValidator.compareDouble(caseExpectedAdditCanclChrg, ActualAdditCanclChrg, "Additional Cancellation Charge", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
				Reportcount++;
				statusPassFail = CommonValidator.compareDouble(caseExpectedTotalChrg, ActualTotalChrg, "Total Cancellation Charge", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
				Reportcount++;
			}
			//CASE 2
			else if(dtSupplierCancellationDate.compareTo(dtCancellingDate) > 0)
			{
				caseExpectedSupCancelChrg	= 0;
				caseExpectedBalncCancelChrg	= baseFare + taxes + profit;
				caseExpectedAdditCanclChrg	= bookingFee + creditCardFee;
				caseExpectedTotalChrg		= caseExpectedSupCancelChrg + caseExpectedBalncCancelChrg + caseExpectedAdditCanclChrg;
				//caseExpectedSupplierGet		= 0;
				//caseExpectedPortalGet		= bookingFee  + baseFare + taxes + profit;
				//caseExpectedCustomerGet		= 0;

				statusPassFail = CommonValidator.compareDouble(caseExpectedSupCancelChrg, ActualSupplierCancelChrg, "Supplier Cancellation Charge", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
				Reportcount++;
				statusPassFail = CommonValidator.compareDouble(caseExpectedBalncCancelChrg, ActualBalanceCancelChrg, "Customer Cancellation Charge", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
				Reportcount++;
				statusPassFail = CommonValidator.compareDouble(caseExpectedAdditCanclChrg, ActualAdditCanclChrg, "Additional Cancellation Charge", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
				Reportcount++;
				statusPassFail = CommonValidator.compareDouble(caseExpectedTotalChrg, ActualTotalChrg, "Total Cancellation Charge", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
				Reportcount++;
			}
			else if(dtCancellingDate.compareTo(dtSupplierCancellationDate) > 0)
			{
				caseExpectedSupCancelChrg	= baseFare + taxes;
				caseExpectedBalncCancelChrg	= profit;
				caseExpectedAdditCanclChrg	= bookingFee + creditCardFee;
				caseExpectedTotalChrg		= caseExpectedSupCancelChrg + caseExpectedBalncCancelChrg + caseExpectedAdditCanclChrg;
				//caseExpectedSupplierGet		= baseFare + taxes;
				//caseExpectedPortalGet		= bookingFee + profit;
				//caseExpectedCustomerGet		= 0;
				
				statusPassFail = CommonValidator.compareDouble(caseExpectedSupCancelChrg, ActualSupplierCancelChrg, "Supplier Cancellation Charge", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
				Reportcount++;
				statusPassFail = CommonValidator.compareDouble(caseExpectedBalncCancelChrg, ActualBalanceCancelChrg, "Customer Cancellation Charge", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
				Reportcount++;
				statusPassFail = CommonValidator.compareDouble(caseExpectedAdditCanclChrg, ActualAdditCanclChrg, "Additional Cancellation Charge", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
				Reportcount++;
				statusPassFail = CommonValidator.compareDouble(caseExpectedTotalChrg, ActualTotalChrg, "Total Cancellation Charge", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
				Reportcount++;
			}
		}
		
		//VALIDATING RESERVATION DETAILS
		statusPassFail = CommonValidator.compareStringContains(XMLSelectFlight.getOriginoptions().get(0).getFlightlist().get(0).getDepartureLocationCode(), Cancellation.getOrigin(), "Origin", ReportPrinter, Reportcount);
		Reportcount++;
		
		if(twoway) {
			statusPassFail = CommonValidator.compareStringContains(XMLSelectFlight.getOriginoptions().get(1).getFlightlist().get(/*XMLSelectFlight.getOriginoptions().get(1).getFlightlist().size()-1*/0).getDepartureLocationCode(), Cancellation.getDestination(), "Destination", ReportPrinter, Reportcount);
			Reportcount++;
		}
		
		statusPassFail = CommonValidator.compareStringContains(String.valueOf(fillingObject.getAdult().size()), Cancellation.getAdultCount(), "Number of Adults", ReportPrinter, Reportcount);
		Reportcount++;
		statusPassFail = CommonValidator.compareStringContains(String.valueOf(fillingObject.getChildren().size()), Cancellation.getChildCount(), "Number of Children", ReportPrinter, Reportcount);
		Reportcount++;
		statusPassFail = CommonValidator.compareStringContains(String.valueOf(fillingObject.getInfant().size()), Cancellation.getInfantCount(), "Number of Infants", ReportPrinter, Reportcount);
		Reportcount++;
		statusPassFail = CommonValidator.compareStringContains(XMLSelectFlight.getOriginoptions().get(0).getFlightlist().get(0).getDepartureDate(), Cancellation.getDateDeparture(), "Departure Date", ReportPrinter, Reportcount);
		Reportcount++;
		
		if(twoway) {
			statusPassFail = CommonValidator.compareStringContains(XMLSelectFlight.getOriginoptions().get(1).getFlightlist().get(XMLSelectFlight.getOriginoptions().get(1).getFlightlist().size()-1).getArrivalDate(), Cancellation.getDateReturn(), "Return Date", ReportPrinter, Reportcount);
			Reportcount++;
		}
		
		statusPassFail = CommonValidator.compareStringContains(Sobj.getTriptype().trim(), Cancellation.getTripType().trim(), "Trip Type", ReportPrinter, Reportcount);
		Reportcount++;
		statusPassFail = CommonValidator.compareStringContains(Sobj.getCabinClass().trim(), Cancellation.getSeatClass().trim(), "Cabin/Seat Class", ReportPrinter, Reportcount);
		Reportcount++;
		statusPassFail = CommonValidator.compareStringContains(uiconfirmationpage.getReservationNo().trim(), Cancellation.getReservationNo().trim(), "Reservation Number", ReportPrinter, Reportcount);
		Reportcount++;
		statusPassFail = CommonValidator.compareStringContains(uiconfirmationpage.getSupplierConfirmationNo().trim(), Cancellation.getSupplConfNo().trim(), "Supplier Confirmation Number", ReportPrinter, Reportcount);
		Reportcount++;
		
		if (Sobj.getSearchType().equalsIgnoreCase("Call_Center"))
		{
			statusPassFail = CommonValidator.compareStringEqualsIgnoreCase("CC", Cancellation.getBookingChannel(), "Booking Channel", ReportPrinter, Reportcount);
			Reportcount++;
		}
		else if (Sobj.getSearchType().equalsIgnoreCase("Web"))
		{
			statusPassFail = CommonValidator.compareStringEqualsIgnoreCase("Web", Cancellation.getBookingChannel(), "Booking Channel", ReportPrinter, Reportcount);
			Reportcount++;
		}
		
		if(XMLSelectFlight.getOriginoptions().get(0).getFlightlist().size() == Cancellation.getOutbound().size())
		{
			for (int i = 0; i < Cancellation.getOutbound().size(); i++) {
				
				Flight Actual = new Flight();
				Actual = Cancellation.getOutbound().get(i);
				Flight Expected = new Flight();
				Expected = XMLSelectFlight.getOriginoptions().get(0).getFlightlist().get(i);
				
				statusPassFail = CommonValidator.compareStringContains(Expected.getFlightNo(), Actual.getFlightNo(), "Outbound Flight "+i+" No", ReportPrinter, Reportcount);
				Reportcount++;
				statusPassFail = CommonValidator.compareStringContains(Expected.getMarketingAirline(), Actual.getMarketingAirline_Loc_Code(), "Outbound Flight "+i+" Name", ReportPrinter, Reportcount);
				Reportcount++;
				statusPassFail = CommonValidator.compareStringContains(Expected.getDepartureDate(), Actual.getDepartureDate(), "Outbound Flight "+i+" Departure Date", ReportPrinter, Reportcount);
				Reportcount++;
				statusPassFail = CommonValidator.compareStringContains(Expected.getDepartureTime(), Actual.getDepartureTime(), "Outbound Flight "+i+" Departure Time", ReportPrinter, Reportcount);
				Reportcount++;
				statusPassFail = CommonValidator.compareStringContains(Expected.getDepartureLocationCode(), Actual.getDepartureLocationCode(), "Outbound Flight "+i+" Departure Location", ReportPrinter, Reportcount);
				Reportcount++;
				statusPassFail = CommonValidator.compareStringContains(Expected.getArrivalLocationCode(), Actual.getArrivalLocationCode(), "Outbound Flight "+i+" Arrival Location", ReportPrinter, Reportcount);
				Reportcount++;
			}
		}
		
		if(twoway)
		{
			if(XMLSelectFlight.getOriginoptions().get(1).getFlightlist().size() == Cancellation.getInbound().size())
			{
				for (int i = 0; i < Cancellation.getInbound().size(); i++) {
					
					Flight Actual = new Flight();
					Actual = Cancellation.getInbound().get(i);
					Flight Expected = new Flight();
					Expected = XMLSelectFlight.getOriginoptions().get(1).getFlightlist().get(i);
					
					statusPassFail = CommonValidator.compareStringContains(Expected.getFlightNo(), Actual.getFlightNo(), "Inbound Flight "+i+" No", ReportPrinter, Reportcount);
					Reportcount++;
					statusPassFail = CommonValidator.compareStringContains(Expected.getMarketingAirline(), Actual.getMarketingAirline_Loc_Code(), "Inbound Flight "+i+" Name", ReportPrinter, Reportcount);
					Reportcount++;
					statusPassFail = CommonValidator.compareStringContains(Expected.getDepartureDate(), Actual.getDepartureDate(), "Inbound Flight "+i+" Departure Date", ReportPrinter, Reportcount);
					Reportcount++;
					statusPassFail = CommonValidator.compareStringContains(Expected.getDepartureTime(), Actual.getDepartureTime(), "Inbound Flight "+i+" Departure Time", ReportPrinter, Reportcount);
					Reportcount++;
					statusPassFail = CommonValidator.compareStringContains(Expected.getDepartureLocationCode(), Actual.getDepartureLocationCode(), "Inbound Flight "+i+" Departure Location", ReportPrinter, Reportcount);
					Reportcount++;
					statusPassFail = CommonValidator.compareStringContains(Expected.getArrivalLocationCode(), Actual.getArrivalLocationCode(), "Inbound Flight "+i+" Arrival Location", ReportPrinter, Reportcount);
					Reportcount++;
				}
			}
		}
		
		statusPassFail = CommonValidator.compareInt(fillingObject.getAdult().size(), Cancellation.getAdults().size(), "Number of Adults", ReportPrinter, Reportcount);
		Reportcount++;
		
		if(fillingObject.getAdult().size() == Cancellation.getAdults().size())
		{
			for (int i = 0; i < fillingObject.getAdult().size(); i++)
			{
				Traveler Expected  = new Traveler();
				Traveler Actual = new Traveler();
				Expected = fillingObject.getAdult().get(i);
				for(int u = 0; u<fillingObject.getAdult().size(); u++)
				{
					Actual = Cancellation.getAdults().get(i);
					if(Expected.getGivenName().trim().equalsIgnoreCase(Actual.getGivenName().trim()))
					{
						statusPassFail = CommonValidator.compareStringContains(Expected.getNamePrefixTitle(), Actual.getNamePrefixTitle(), "Name Prefix Title", ReportPrinter, Reportcount);
						Reportcount++;
						statusPassFail = CommonValidator.compareStringContains(Expected.getGivenName(), Actual.getGivenName(), "Given Names", ReportPrinter, Reportcount);
						Reportcount++;
						statusPassFail = CommonValidator.compareStringContains(Expected.getSurname(), Actual.getSurname(), "Surname", ReportPrinter, Reportcount);
						Reportcount++;
						break;
					}
					
				}
				
			}	
		}	
		/*try {
			for(int y=0; y < fillingObject.getAdult().size(); y++)
			{
				if( Actual.getGivenName().contains(fillingObject.getAdult().get(y).getGivenName()) )
				{
							statusPassFail = CommonValidator.compareStringContains(Expected.getGivenName(), Actual.getGivenName(), "Given Names", ReportPrinter, Reportcount);
							Reportcount++;
						}
						if( Actual.getSurname().contains(fillingObject.getAdult().get(y).getSurname()) )
						{
							statusPassFail = CommonValidator.compareStringContains(Expected.getSurname(), Actual.getSurname(), "Surname", ReportPrinter, Reportcount);
							Reportcount++;
						}
					}
				} catch (Exception e) {
					
				}*/
				
			
			
			/*for(int i=0; i<count; i++)
			{
				aT = ConfirmationReport.getAdults().get(i);
				for(int y=0; y<count; y++)
				{
					bT = fillingObject.getAdult().get(y);
					if(aT.getGivenName().trim().equalsIgnoreCase(bT.getGivenName().trim()))
					{
						ReportPrinter = validatePassengers(aT, bT, ReportPrinter);
						break;
					}
				}
			}*/
		
		
		//VALIDATING CHILDREN
		if(fillingObject.getChildren().size() != 0)
		{
			statusPassFail = CommonValidator.compareInt(fillingObject.getChildren().size(), Cancellation.getChild().size(), "Number of Children", ReportPrinter, Reportcount);
			Reportcount++;
			
			
			if(fillingObject.getChildren().size() == Cancellation.getChild().size())
			{
				/*for (int i = 0; i < fillingObject.getChildren().size(); i++)
				{
					Traveler Expected  = new Traveler();
					Traveler Actual = new Traveler();
					Expected = fillingObject.getChildren().get(i);
					Actual = Cancellation.getChild().get(i);
					
					statusPassFail = CommonValidator.compareStringContains(Expected.getNamePrefixTitle(), Actual.getNamePrefixTitle(), "Name Prefix Title", ReportPrinter, Reportcount);
					Reportcount++;
					
					try {
						for(int y=0; y < fillingObject.getChildren().size(); y++)
						{
							if( Actual.getGivenName().contains(fillingObject.getChildren().get(y).getGivenName()) )
							{
								statusPassFail = CommonValidator.compareStringContains(Expected.getGivenName(), Actual.getGivenName(), "Given Names", ReportPrinter, Reportcount);
								Reportcount++;
							}
							if( Actual.getSurname().contains(fillingObject.getChildren().get(y).getSurname()) )
							{
								statusPassFail = CommonValidator.compareStringContains(Expected.getSurname(), Actual.getSurname(), "Surname", ReportPrinter, Reportcount);
								Reportcount++;
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}*/
				for (int i = 0; i < fillingObject.getChildren().size(); i++)
				{
					Traveler Expected  = new Traveler();
					Traveler Actual = new Traveler();
					Expected = fillingObject.getChildren().get(i);
					for(int u = 0; u<fillingObject.getChildren().size(); u++)
					{
						Actual = Cancellation.getChild().get(i);
						if(Expected.getGivenName().trim().equalsIgnoreCase(Actual.getGivenName().trim()))
						{
							statusPassFail = CommonValidator.compareStringContains(Expected.getNamePrefixTitle(), Actual.getNamePrefixTitle(), "Name Prefix Title", ReportPrinter, Reportcount);
							Reportcount++;
							statusPassFail = CommonValidator.compareStringContains(Expected.getGivenName(), Actual.getGivenName(), "Given Names", ReportPrinter, Reportcount);
							Reportcount++;
							statusPassFail = CommonValidator.compareStringContains(Expected.getSurname(), Actual.getSurname(), "Surname", ReportPrinter, Reportcount);
							Reportcount++;
							break;
						}
						
					}
					
				}	
			}
		}
		
		//VALIDATING INFANTS
		if(fillingObject.getInfant().size() != 0)
		{
			statusPassFail = CommonValidator.compareInt(fillingObject.getInfant().size(), Cancellation.getInfant().size(), "Number of Infant", ReportPrinter, Reportcount);
			Reportcount++;
			
			if(fillingObject.getInfant().size() == Cancellation.getInfant().size())
			{
				/*for (int i = 0; i < fillingObject.getInfant().size(); i++)
				{
					Traveler Expected  = new Traveler();
					Traveler Actual = new Traveler();
					Expected = fillingObject.getInfant().get(i);
					Actual = Cancellation.getInfant().get(i);
					
					//ReportPrinter = compareStringContains(Expected.getNamePrefixTitle(), Actual.getNamePrefixTitle(), "Name Prefix Title", ReportPrinter);
					
					try {
						for(int y=0; y < fillingObject.getInfant().size(); y++)
						{
							if( Actual.getGivenName().contains(fillingObject.getInfant().get(y).getGivenName()) )
							{
								statusPassFail = CommonValidator.compareStringContains(Expected.getGivenName(), Actual.getGivenName(), "Given Names", ReportPrinter, Reportcount);
								Reportcount++;
							}
							if( Actual.getSurname().contains(fillingObject.getInfant().get(y).getSurname()) )
							{
								statusPassFail = CommonValidator.compareStringContains(Expected.getSurname(), Actual.getSurname(), "Surname", ReportPrinter, Reportcount);
								Reportcount++;
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					//ReportPrinter = compareStringContains(Expected.getGivenName(), Actual.getGivenName(), "Given Names", ReportPrinter);
					//ReportPrinter = compareStringContains(Expected.getSurname(), Actual.getSurname(), "Surname ", ReportPrinter);
				}*/
				
				for (int i = 0; i < fillingObject.getInfant().size(); i++)
				{
					Traveler Expected  = new Traveler();
					Traveler Actual = new Traveler();
					Expected = fillingObject.getInfant().get(i);
					for(int u = 0; u<fillingObject.getInfant().size(); u++)
					{
						Actual = Cancellation.getInfant().get(i);
						if(Expected.getGivenName().trim().equalsIgnoreCase(Actual.getGivenName().trim()))
						{
							statusPassFail = CommonValidator.compareStringContains(Expected.getNamePrefixTitle(), Actual.getNamePrefixTitle(), "Name Prefix Title", ReportPrinter, Reportcount);
							Reportcount++;
							statusPassFail = CommonValidator.compareStringContains(Expected.getGivenName(), Actual.getGivenName(), "Given Names", ReportPrinter, Reportcount);
							Reportcount++;
							statusPassFail = CommonValidator.compareStringContains(Expected.getSurname(), Actual.getSurname(), "Surname", ReportPrinter, Reportcount);
							Reportcount++;
							break;
						}
						
					}
					
				}
			}
		}
		
		return ReportPrinter;
	}
	
	public StringBuffer validateCancellationRequest(CancellationRequest cancellationRequest, UIConfirmationPage uiconfirmationpage, StringBuffer ReportPrinter)
	{
		ReportPrinter = compareStringEquals(uiconfirmationpage.getSupplierConfirmationNo(), cancellationRequest.getUniqueID(), "Supplier Confirmation No / Unique ID", ReportPrinter);
		return ReportPrinter;
	}
	
	public StringBuffer validateCancellationResponse(CancellationResponse cancellationResponse, UIConfirmationPage uiconfirmationpage, CancellationReport cancellationReport, StringBuffer ReportPrinter)
	{
		ReportPrinter = compareStringEquals(uiconfirmationpage.getSupplierConfirmationNo(), cancellationResponse.getUniqueID(), "Supplier Confirmation No / Unique ID", ReportPrinter);
		Reportcount++;
		ReportPrinter = compareStringEquals(cancellationReport.getBooking_Staus().trim(), cancellationResponse.getStatus(), "Cancellation Status", ReportPrinter);
		Reportcount++;
		return ReportPrinter;
	}
	
	public StringBuffer validateCancellationReport(XMLPriceItinerary XMLSelectFlight, SearchObject Sobj, ReservationInfo fillingObject, UIConfirmationPage uiconfirmationpage, CancellationScreen cancellationScreen, CancellationReport cancellationReport, StringBuffer ReportPrinter)
	{
		ReportPrinter = compareStringEquals(uiconfirmationpage.getReservationNo().trim().concat("C"), cancellationReport.getCancellationNo(), "Cancellation Number", ReportPrinter);
		Reportcount++;
		ReportPrinter = compareStringContains(uiconfirmationpage.getReservationNo(), cancellationReport.getBooking_No(), "Booking Number", ReportPrinter);
		Reportcount++;
		ReportPrinter = compareStringEquals("Air", cancellationReport.getProduct_Type(), "Product Type", ReportPrinter);
		Reportcount++;
		ReportPrinter = compareStringEquals("Cancelled", cancellationReport.getBooking_Staus(), "Booking Status", ReportPrinter);
		Reportcount++;
		ReportPrinter = compareStringContains(PropertyMap.get("Flight_Supplier"), cancellationReport.getSupplier_Name(), "Supplier Name", ReportPrinter);
		
		//ReportPrinter = compareStringEquals("", cancellationReport.getDocumentNo(), "Document Number", ReportPrinter);
		ReportPrinter = compareStringEquals(CommonValidator.getSystemDateInCommonFormat(), cancellationReport.getBooking_Date(), "Booking Date", ReportPrinter);
		Reportcount++;
		ReportPrinter = compareStringContains(fillingObject.getAdult().get(0).getGivenName(), cancellationReport.getGuestLastFirst_Name(), "Guest First Name", ReportPrinter);
		Reportcount++;
		ReportPrinter = compareStringContains(fillingObject.getAdult().get(0).getSurname(), cancellationReport.getGuestLastFirst_Name(), "Guest Last Name", ReportPrinter);
		Reportcount++;
		
		if(Sobj.getSearchType().equalsIgnoreCase("Call_Center"))
		{
			ReportPrinter = compareStringEquals("Call Center", cancellationReport.getBooking_Channel(), "Booking Channel", ReportPrinter);
			Reportcount++;
		}
		else if(Sobj.getSearchType().equalsIgnoreCase("Web"))
		{
			ReportPrinter = compareStringEquals("Web", cancellationReport.getBooking_Channel(), "Booking Channel", ReportPrinter);
			Reportcount++;
		}
		
		ReportPrinter = compareStringContains(uiconfirmationpage.getSupplierConfirmationNo(), cancellationReport.getSupplier_ConfirmNo(), "Supplier Confirmation Number", ReportPrinter);
		Reportcount++;
		ReportPrinter = compareStringContains(uiconfirmationpage.getSupplierConfirmationNo().trim().concat("-C"), cancellationReport.getSupplier_Cancel_No(), "Supplier Cancellation Number", ReportPrinter);
		Reportcount++;
		
		ReportPrinter = compareStringEquals(PropertyMap.get("portal.username"), cancellationReport.getCancel_By(), "Cancelled By", ReportPrinter);
		Reportcount++;
		ReportPrinter = compareStringContains(fillingObject.getMaincustomer().getGivenName(), cancellationReport.getCustomer_Name(), "Customer Given Name", ReportPrinter);
		Reportcount++;
		ReportPrinter = compareStringContains(fillingObject.getMaincustomer().getSurname(), cancellationReport.getCustomer_Name(), "Customer Surname", ReportPrinter);
		Reportcount++;
		
		
		String	general	= "";
		general			= PropertyMap.get("Portal_Currency_Code").trim();				
		ReportPrinter	= compareStringEquals(Sobj.getSellingCurrency(), cancellationReport.getSell_Currency(), "Selling Currency", ReportPrinter);
		Reportcount++;
		ReportPrinter	= compareStringEquals(general, cancellationReport.getBase_Currency(), "Portal Currency", ReportPrinter);
		Reportcount++;
		
		//============================================================================================================================================================================
		XMLPriceInfo xmlPriceInfo				= new XMLPriceInfo();
					 xmlPriceInfo				= XMLSelectFlight.getPricinginfo();
		double		 xmlTotBookingValInPortCurr	= 0;
					 xmlTotBookingValInPortCurr	= xmlPriceInfo.getBaseFareInPortalCurrency() + xmlPriceInfo.getTaxFareInPortalCurrency() + xmlPriceInfo.getCreditcardFeeInPortalCurrency() + xmlPriceInfo.getBookingFeeInPortalCurrency() + xmlPriceInfo.getProfitInPortalCurrency();
		double	     xmlTotBookingValInSellSurr	= 0;
					 xmlTotBookingValInSellSurr	= xmlPriceInfo.getNewbase() + xmlPriceInfo.getNewtax() + xmlPriceInfo.getCreditcardfeeinSellCurr() + xmlPriceInfo.getProfit() + xmlPriceInfo.getBookingfee();
		
		statusPassFail = CommonValidator.compareDouble(xmlTotBookingValInSellSurr, Double.valueOf(cancellationReport.getSell_TotBookingVal()), "Total Booking Value in Selling Currency", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		xmlTotBookingValInPortCurr = convert(xmlPriceInfo.getPortalCurrencyCode(), xmlPriceInfo.getNewBasefareCurrencyCode(), xmlTotBookingValInSellSurr);
		statusPassFail = CommonValidator.compareDouble(xmlTotBookingValInPortCurr, Double.valueOf(cancellationReport.getBase_TotBookingVal()), "Total Booking Value in Base Currency", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//=============================================================================================================================================================================
		
		double SuppCancelFeeInSellCurr 	= 0;
		double SuppCancelFeeInPortCurr 	= 0;
		double CustCancelFeeInSellCurr 	= 0;
		double CustCancelFeeInPortCurr	= 0;
		
		String	sellingCurrency			= "";	
		sellingCurrency					= XMLSelectFlight.getPricinginfo().getNewBasefareCurrencyCode();
		double	profit					= 0;	
		profit							= XMLSelectFlight.getPricinginfo().getProfit();
		double	baseFare				= 0;	
		baseFare						= Double.valueOf(XMLSelectFlight.getPricinginfo().getNewbase());
		double	taxes					= 0;	
		taxes							= Double.valueOf(XMLSelectFlight.getPricinginfo().getNewtax());
		
		CancellationBreakdown breakdown = cancellationScreen.getCancellationBreakdown();
		
		if(breakdown.getCancellationScenario().toString().equals(CancellationType.Before_Cancellation.toString()))
		{
			SuppCancelFeeInSellCurr = 0;
			SuppCancelFeeInPortCurr = 0;
			CustCancelFeeInSellCurr = 0;
			CustCancelFeeInPortCurr = 0;
			
			statusPassFail = CommonValidator.compareDouble(SuppCancelFeeInSellCurr, Double.valueOf(cancellationReport.getSell_SuppCancelFee()), "Supplier Cancellation Fee in Selling Currrency"   , ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			statusPassFail = CommonValidator.compareDouble(SuppCancelFeeInPortCurr, Double.valueOf(cancellationReport.getBase_SuppCancelFee()), "Supplier Cancellation Fee in Base Currency"       , ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			statusPassFail = CommonValidator.compareDouble(CustCancelFeeInSellCurr, Double.valueOf(cancellationReport.getSell_CustCancelFee()), "Customer Cancellation Fee in Selling Currency"    , ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			statusPassFail = CommonValidator.compareDouble(CustCancelFeeInPortCurr, Double.valueOf(cancellationReport.getBase_CustCancelFee()), "Customer Cancellation Fee in Base Currency"       , ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
		}
		else if(cancellationScreen.getCancellationBreakdown().getCancellationScenario().toString().equals(CancellationType.Within_Cancellation.toString()))
		{
			SuppCancelFeeInSellCurr = /*xmlPriceInfo.getNewbase() + xmlPriceInfo.getNewtax()*/0;
			SuppCancelFeeInPortCurr = /*xmlPriceInfo.getBaseFareInPortalCurrency() + xmlPriceInfo.getTaxFareInPortalCurrency()*/0;
			CustCancelFeeInSellCurr = baseFare + taxes + profit;
			CustCancelFeeInPortCurr = CommonValidator.convert(sellingCurrency, PropertyMap.get("Portal_Currency_Code"), PropertyMap.get("Portal_Currency_Code"), (baseFare + taxes + profit), CurrencyMap);/*xmlPriceInfo.getBaseFareInPortalCurrency() + xmlPriceInfo.getTaxFareInPortalCurrency() + xmlPriceInfo.getProfitInPortalCurrency();*/
			
			statusPassFail = CommonValidator.compareDouble(SuppCancelFeeInSellCurr, Double.valueOf(cancellationReport.getSell_SuppCancelFee()), "Supplier Cancellation Fee in Selling Currrency"	, ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			statusPassFail = CommonValidator.compareDouble(SuppCancelFeeInPortCurr, Double.valueOf(cancellationReport.getBase_SuppCancelFee()), "Supplier Cancellation Fee in Base Currency"		, ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			statusPassFail = CommonValidator.compareDouble(CustCancelFeeInSellCurr, Double.valueOf(cancellationReport.getSell_CustCancelFee()), "Customer Cancellation Fee in Selling Currency"	    , ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			statusPassFail = CommonValidator.compareDouble(CustCancelFeeInPortCurr, Double.valueOf(cancellationReport.getBase_CustCancelFee()), "Customer Cancellation Fee in Base Currency"		, ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
		}
		else if(cancellationScreen.getCancellationBreakdown().getCancellationScenario().toString().equals(CancellationType.Outside_Cancellation.toString()))
		{
			SuppCancelFeeInSellCurr = baseFare + taxes;
			SuppCancelFeeInPortCurr = CommonValidator.convert(sellingCurrency, PropertyMap.get("Portal_Currency_Code"), PropertyMap.get("Portal_Currency_Code"), (baseFare + taxes), CurrencyMap);/*xmlPriceInfo.getBaseFareInPortalCurrency() + xmlPriceInfo.getTaxFareInPortalCurrency();*/
			CustCancelFeeInSellCurr = profit;
			CustCancelFeeInPortCurr = CommonValidator.convert(sellingCurrency, PropertyMap.get("Portal_Currency_Code"), PropertyMap.get("Portal_Currency_Code"), profit, CurrencyMap);
			
			statusPassFail = CommonValidator.compareDouble(SuppCancelFeeInSellCurr, Double.valueOf(cancellationReport.getSell_SuppCancelFee()), "Supplier Cancellation Fee in Selling Currrency"	, ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			statusPassFail = CommonValidator.compareDouble(SuppCancelFeeInPortCurr, Double.valueOf(cancellationReport.getBase_SuppCancelFee()), "Supplier Cancellation Fee in Base Currency"		, ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			statusPassFail = CommonValidator.compareDouble(CustCancelFeeInSellCurr, Double.valueOf(cancellationReport.getSell_CustCancelFee()), "Customer Cancellation Fee in Selling Currency"	    , ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			statusPassFail = CommonValidator.compareDouble(CustCancelFeeInPortCurr, Double.valueOf(cancellationReport.getBase_CustCancelFee()), "Customer Cancellation Fee in Base Currency"		, ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
		}
		
		return ReportPrinter;
	}
	
	
	
	//================================REPORTS AFTER CANCELLATION==============================
	
	//==========================AFTER CANCELLATION DETAILS VALIDATION=========================
	
	//================================REPORTS AFTER CANCELLATION==============================
	
	
	//==========================AFTER CANCELLATION DETAILS VALIDATION=========================
	/*public StringBuffer validateSupplierPayableReportAfterCancel(SupplierPayableReport beforePay, SupplierPayableReport afterPay, SearchObject Sobj, XMLPriceItinerary XMLSelectFlight, UIConfirmationPage uiconfirmationpage,  ReservationInfo fillingObject, StringBuffer ReportPrinter)
	{
		
		return ReportPrinter;
	}*/
	
	public StringBuffer validateProfitAndLossAfterCancellation(ProfitAndLoss profitlossReport, CancellationScreen cancellationScreen, CancellationReport cancellationReport, XMLPriceItinerary XMLSelectFlight, SearchObject Sobj, StringBuffer ReportPrinter)
	{
		try {
		double	profit						= 0;	
		profit								= XMLSelectFlight.getPricinginfo().getProfitInPortalCurrency();/*XMLSelectFlight.getPricinginfo().getProfit()*/
		double	bookingFee					= 0;	
		bookingFee							= XMLSelectFlight.getPricinginfo().getBookingFeeInPortalCurrency();/*XMLSelectFlight.getPricinginfo().getBookingfee()*/
		double	baseFare					= 0;	
		baseFare							= XMLSelectFlight.getPricinginfo().getBaseFareInPortalCurrency();/*Double.valueOf(XMLSelectFlight.getPricinginfo().getNewbase())*/
		double	creditCardFee				= 0;
		
		if(Sobj.getPaymentMode().equalsIgnoreCase("Pay Online"))
			creditCardFee					= XMLSelectFlight.getPricinginfo().getCreditcardFeeInPortalCurrency();/*Double.valueOf(XMLSelectFlight.getPricinginfo().getCreditcardfeeinSellCurr())*/
		
		double	taxes						= 0;	
		taxes								= XMLSelectFlight.getPricinginfo().getTaxFareInPortalCurrency();/*Double.valueOf(XMLSelectFlight.getPricinginfo().getNewtax())*/
		
		double	expectedTotal				= 0;
		expectedTotal						= Math.ceil(baseFare + taxes + profit + bookingFee + creditCardFee);	
		
		
		double	caseExpectedSupplierGet		= 0;	
		double	caseExpectedPortalGet		= 0;	
		double	caseExpectedCustomerGet		= 0;
		double	caseExpectedCostOfSales		= 0;
		
		if( cancellationScreen.getCancellationBreakdown().getCancellationScenario().equals(CancellationType.Before_Cancellation) )
		{
			//caseExpectedSupCancelChrg		= 0;
			//caseExpectedBalncCancelChrg	= 0;
			//caseExpectedAdditCanclChrg	= bookingFee + creditCardFee;////////////////////////////////////
			//caseExpectedTotalChrg			= caseExpectedSupCancelChrg + caseExpectedBalncCancelChrg + caseExpectedAdditCanclChrg;
			caseExpectedSupplierGet		= 0;
			caseExpectedPortalGet		= bookingFee;
			caseExpectedCustomerGet		= baseFare + profit + taxes;
			caseExpectedCostOfSales		= Math.floor(caseExpectedSupplierGet + caseExpectedCustomerGet + creditCardFee);
			
			statusPassFail = CommonValidator.compareDouble( expectedTotal, Double.parseDouble(profitlossReport.getGrossBookingVal()), "Gross Booking Value", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
			statusPassFail = CommonValidator.compareDouble( expectedTotal /*- Agent Commission*/, Double.parseDouble(profitlossReport.getNetBookingVal()), "Net Booking Value", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
			statusPassFail = CommonValidator.compareDouble(caseExpectedCostOfSales, Double.parseDouble(profitlossReport.getCostOfSales()), "Cost of Sales", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
			statusPassFail = CommonValidator.compareDouble(caseExpectedPortalGet, Double.parseDouble(profitlossReport.getProfit()), "Profit", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
		}
		//CASE 2
		else if( cancellationScreen.getCancellationBreakdown().getCancellationScenario().equals(CancellationType.Within_Cancellation) )
		{
			//caseExpectedSupCancelChrg		= 0;
			//caseExpectedBalncCancelChrg	= baseFare + taxes + profit;
			//caseExpectedAdditCanclChrg	= bookingFee + creditCardFee;
			//caseExpectedTotalChrg			= caseExpectedSupCancelChrg + caseExpectedBalncCancelChrg + caseExpectedAdditCanclChrg;
			caseExpectedSupplierGet		= 0;
			caseExpectedPortalGet		= bookingFee  + baseFare + taxes + profit;
			caseExpectedCustomerGet		= 0;
			caseExpectedCostOfSales		= Math.floor(caseExpectedSupplierGet + caseExpectedCustomerGet + creditCardFee);
			
			statusPassFail = CommonValidator.compareDouble( expectedTotal, Double.parseDouble(profitlossReport.getGrossBookingVal()), "Gross Booking Value", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
			statusPassFail = CommonValidator.compareDouble( expectedTotal, Double.parseDouble(profitlossReport.getNetBookingVal()), "Net Booking Value", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
			statusPassFail = CommonValidator.compareDouble(caseExpectedCostOfSales, Double.parseDouble(profitlossReport.getCostOfSales()), "Cost of Sales", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
			statusPassFail = CommonValidator.compareDouble(caseExpectedPortalGet, Double.parseDouble(profitlossReport.getProfit()), "Profit", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
		}
		else if( cancellationScreen.getCancellationBreakdown().getCancellationScenario().equals(CancellationType.Outside_Cancellation) ) 
		{
			//caseExpectedSupCancelChrg		= baseFare + taxes;
			//caseExpectedBalncCancelChrg	= profit;
			//caseExpectedAdditCanclChrg	= bookingFee + creditCardFee;
			//caseExpectedTotalChrg			= caseExpectedSupCancelChrg + caseExpectedBalncCancelChrg + caseExpectedAdditCanclChrg;
			caseExpectedSupplierGet		= baseFare + taxes;
			caseExpectedPortalGet		= bookingFee + profit;
			caseExpectedCustomerGet		= 0;
			caseExpectedCostOfSales		= Math.floor(caseExpectedSupplierGet + caseExpectedCustomerGet + creditCardFee);
			
			statusPassFail = CommonValidator.compareDouble( expectedTotal, Double.parseDouble(profitlossReport.getGrossBookingVal()), "Gross Booking Value", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
			statusPassFail = CommonValidator.compareDouble( expectedTotal, Double.parseDouble(profitlossReport.getNetBookingVal()), "Net Booking Value", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
			statusPassFail = CommonValidator.compareDouble(caseExpectedCostOfSales, Double.parseDouble(profitlossReport.getCostOfSales()), "Cost of Sales", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
			
			statusPassFail = CommonValidator.compareDouble(caseExpectedPortalGet, Double.parseDouble(profitlossReport.getProfit()), "Profit", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
			Reportcount++;
		}
		} catch (Exception e) {
			
		}
		
		return ReportPrinter;
	}

	/*public StringBuffer validateSupplierPayableAfterCancellation(UIConfirmationPage uiconfirmationpage, SupplierPayableReport beforePay, XMLPriceItinerary XMLSelectFlight, StringBuffer ReportPrinter)
	{
		try {
			statusPassFail = CommonValidator.compareStringContains(uiconfirmationpage.getReservationNo().trim(), beforePay.getReservationNo().trim(), "Reservation Number", ReportPrinter, Reportcount);
			Reportcount++;
		} catch (Exception e) {
			// 
		}
		
		//VALIDATE BOOKING DATE
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
			Calendar cal = Calendar.getInstance();
		    String bd =  sdf.format(cal.getTime());
		    statusPassFail = CommonValidator.compareStringEqualsIgnoreCase(bd, beforePay.getBookingDate(), "Booking Date", ReportPrinter, Reportcount);
		    Reportcount++;
		} catch (Exception e) {
			// 
		}
		
		//VALIDATE PRODUCT TYPE
		try {
			statusPassFail = CommonValidator.compareStringEqualsIgnoreCase("Air", beforePay.getProductType(), "Product Type", ReportPrinter, Reportcount);
			Reportcount++;
		} catch (Exception e) {
			// 
		}
				
		//VALIDATE PRODUCT NAME
		try {
			statusPassFail = CommonValidator.compareStringEqualsIgnoreCase(XMLSelectFlight.getOriginoptions().get(0).getFlightlist().get(0).getMarketingAirline(), beforePay.getProductName(), "Product Name", ReportPrinter, Reportcount);
			Reportcount++;
		} catch (Exception e) {
			// 
		}
				
		//VALIDATE PRODUCT NAME
		try {
			statusPassFail = CommonValidator.compareStringEqualsIgnoreCase(PropertyMap.get("Flight_Supplier"), beforePay.getSupplierName(), "Supplier Name", ReportPrinter, Reportcount);
			Reportcount++;
		} catch (Exception e) {
			// 
		}
				
		//VALIDATE SUPPLIER CONFIRMATION
		try {
			statusPassFail = CommonValidator.compareStringEqualsIgnoreCase(uiconfirmationpage.getSupplierConfirmationNo().trim(), beforePay.getSupplierConf().trim(), "Supplier Confirmation Number", ReportPrinter, Reportcount);
			Reportcount++;
		} catch (Exception e) {
			// 
		}
				
		//VALIDATE BOOKING STATUS
		try {
			statusPassFail = CommonValidator.compareStringEqualsIgnoreCase("Cancel", beforePay.getBookingStatus(), "Booking Status", ReportPrinter, Reportcount);
			Reportcount++;
		} catch (Exception e) {
			// 
		}
		
		XMLPriceInfo	XMLO		= XMLSelectFlight.getPricinginfo();
		String			basefare	= XMLO.getBasefareAmount();
		String			taxfare		= XMLO.getTaxAmount();
		
		//VALIDATE PORTAL PAYABLE AMOUNT BEFORE PAY
		String currency	= XMLO.getBasefareCurrencyCode();
		double amount	= Double.valueOf(basefare) + Double.valueOf(taxfare);
		amount			= convert(currency, PropertyMap.get("Portal_Currency_Code"), amount);
		statusPassFail	= CommonValidator.compareDouble(amount, Double.valueOf(beforePay.getPortalCu_PayableAmount()), "Payable Amount(In Portal Currency - Before Payment)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));	
		Reportcount++;
		
		//VALIDATE PORTAL BALANCE BEFORE PAY
		statusPassFail	= CommonValidator.compareDouble(amount, Double.valueOf(beforePay.getPortalCu_Balance()), "Balance(In Portal Currency - Before Payment)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//VALIDATE SUPPLIER PAYABLE AMOUNT BEFORE PAY
		amount			= Double.valueOf(basefare) + Double.valueOf(taxfare);
		statusPassFail	= CommonValidator.compareDouble(amount, Double.valueOf(beforePay.getSupCu_Payable()), "Payable Amount(In Supplier Currency - Before Payment)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//VALIDATE SUPPLIER BALANCE BEFORE PAY
		statusPassFail	= CommonValidator.compareDouble(amount, Double.valueOf(beforePay.getSupCu_Balance()), "Balance(In Supplier Currency - Before Payment)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		return ReportPrinter;
	}
	*/
	
	public StringBuffer validateSupplierPayableAfterCancellation(UIConfirmationPage uiconfirmationpage, SupplierPayableReport beforePay, XMLPriceItinerary XMLSelectFlight, StringBuffer ReportPrinter)
	{
		try {
			statusPassFail = CommonValidator.compareStringContains(uiconfirmationpage.getReservationNo().trim(), beforePay.getReservationNo().trim(), "Reservation Number", ReportPrinter, Reportcount);
			Reportcount++;
		} catch (Exception e) {
			// 
		}
		
		//VALIDATE BOOKING DATE
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
			Calendar cal = Calendar.getInstance();
		    String bd =  sdf.format(cal.getTime());
		    statusPassFail = CommonValidator.compareStringEqualsIgnoreCase(bd, beforePay.getBookingDate(), "Booking Date", ReportPrinter, Reportcount);
		    Reportcount++;
		} catch (Exception e) {
			// 
		}
		
		//VALIDATE PRODUCT TYPE
		try {
			statusPassFail = CommonValidator.compareStringEqualsIgnoreCase("Air", beforePay.getProductType(), "Product Type", ReportPrinter, Reportcount);
			Reportcount++;
		} catch (Exception e) {
			// 
		}
				
		//VALIDATE PRODUCT NAME
		try {
			statusPassFail = CommonValidator.compareStringEqualsIgnoreCase(XMLSelectFlight.getOriginoptions().get(0).getFlightlist().get(0).getMarketingAirline(), beforePay.getProductName(), "Product Name", ReportPrinter, Reportcount);
			Reportcount++;
		} catch (Exception e) {
			// 
		}
				
		//VALIDATE PRODUCT NAME
		try {
			statusPassFail = CommonValidator.compareStringEqualsIgnoreCase(PropertyMap.get("Flight_Supplier"), beforePay.getSupplierName(), "Supplier Name", ReportPrinter, Reportcount);
			Reportcount++;
		} catch (Exception e) {
			// 
		}
				
		//VALIDATE SUPPLIER CONFIRMATION
		try {
			statusPassFail = CommonValidator.compareStringEqualsIgnoreCase(uiconfirmationpage.getSupplierConfirmationNo().trim(), beforePay.getSupplierConf().trim(), "Supplier Confirmation Number", ReportPrinter, Reportcount);
			Reportcount++;
		} catch (Exception e) {
			// 
		}
				
		//VALIDATE BOOKING STATUS
		try {
			statusPassFail = CommonValidator.compareStringEqualsIgnoreCase("Cancel", beforePay.getBookingStatus(), "Booking Status", ReportPrinter, Reportcount);
			Reportcount++;
		} catch (Exception e) {
			// 
		}
		
		XMLPriceInfo	XMLO		= XMLSelectFlight.getPricinginfo();
		String			basefare	= XMLO.getBasefareAmount();
		String			taxfare		= XMLO.getTaxAmount();
		
		//VALIDATE PORTAL PAYABLE AMOUNT BEFORE PAY
		String currency	= XMLO.getBasefareCurrencyCode();
		double amount	= Double.valueOf(basefare) + Double.valueOf(taxfare);
		amount			= 0;
		amount			= convert(currency, PropertyMap.get("Portal_Currency_Code"), amount);
		statusPassFail	= CommonValidator.compareDouble(amount, Double.valueOf(beforePay.getPortalCu_PayableAmount()), "Payable Amount(In Portal Currency - Before Payment)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));	
		Reportcount++;
		
		//VALIDATE PORTAL BALANCE BEFORE PAY
		statusPassFail	= CommonValidator.compareDouble(amount, Double.valueOf(beforePay.getPortalCu_Balance()), "Balance(In Portal Currency - Before Payment)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//VALIDATE SUPPLIER PAYABLE AMOUNT BEFORE PAY
		amount			= Double.valueOf(basefare) + Double.valueOf(taxfare);
		amount = 0;
		statusPassFail	= CommonValidator.compareDouble(amount, Double.valueOf(beforePay.getSupCu_Payable()), "Payable Amount(In Supplier Currency - Before Payment)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		
		//VALIDATE SUPPLIER BALANCE BEFORE PAY
		statusPassFail	= CommonValidator.compareDouble(amount, Double.valueOf(beforePay.getSupCu_Balance()), "Balance(In Supplier Currency - Before Payment)", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
		return ReportPrinter;
	}
	
	
	public StringBuffer validateBookingListReportAfterCancellation(BookingListReport report, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, SearchObject searchObject, UIConfirmationPage confirmationpage, StringBuffer ReportPrinter)
	{
		try {
			statusPassFail = CommonValidator.compareStringEqualsIgnoreCase("CN-X", report.getBookingStatus().trim(), "Booking Status", ReportPrinter, Reportcount);
			Reportcount++;
		} catch (Exception e) {
			
		}
		
		return ReportPrinter;
	}
	
	
	//===========================SUPPORT METHODS=============================
	
	public double convert(String InCurrency, String ToCurrency, double value)
	{
		String PortalCurrency = PropertyMap.get("Portal_Currency_Code");
			
		double   returnValue  =  0;
		if(PortalCurrency.equals(InCurrency) && PortalCurrency.equals(ToCurrency))
		{
			double fvalue		= value;
			//System.out.println(fvalue);
			//valueList.add(fvalue);
			returnValue   = fvalue;
		}
		if(PortalCurrency.equals(InCurrency) && !PortalCurrency.equals(ToCurrency))
		{
			double  rate    	= Double.parseDouble(CurrencyMap.get(ToCurrency)); 
			double  fvalue    	= value;//fvalue = converted value to float
			double  cValue    	= fvalue / rate;//cvalue = conveted final value
				
			//cValue    = cValue + (cValue/100)*10;
			//System.out.println(cValue);
			//valueList.add(cValue);
			//BigDecimal roundfinalPrice  = new BigDecimal(cValue).setScale(0,BigDecimal.ROUND_CEILING);
			returnValue   = cValue;   
		}
		if(PortalCurrency.equals(ToCurrency) && !PortalCurrency.equals(InCurrency))
		{
			//System.out.println(CurrencyMap.get(InCurrency));
			double  rate		= Double.parseDouble(CurrencyMap.get(InCurrency));
			double  fValue		= value; //fvalue = converted value to float
			double  cValue		= fValue / rate;    //cvalue = conveted final value
			//cValue    = cValue + (cValue/100)*10;
			//System.out.println(cValue);
			//BigDecimal roundfinalPrice  = new BigDecimal(cValue).setScale(0,BigDecimal.ROUND_CEILING);
			returnValue   = cValue;   
		}
		if(!PortalCurrency.equals(ToCurrency) && !PortalCurrency.equals(InCurrency))
		{
			//System.out.println(CurrencyMap.get(InCurrency));
			double  crate		= Double.parseDouble(CurrencyMap.get(InCurrency));
			double  srate		= Double.parseDouble(CurrencyMap.get(ToCurrency));
			double  fValue		= value; //fvalue = converted value to float
			double  cValue		= fValue / srate; 
			cValue				= cValue*crate;
			//cValue			= cValue + (cValue/100)*10;
			//System.out.println(cValue);
			//valueList.add(cValue);
			//BigDecimal roundfinalPrice  = new BigDecimal(cValue).setScale(0,BigDecimal.ROUND_CEILING);
			returnValue		= cValue;
		}
	
		return returnValue;
	}
	
	public void setCreditCardAmount(double amount, XMLPriceInfo object)
	{
		object.setCreditcardfeeinSellCurr(Math.ceil(amount));
	}
	
	public void setPaidNow(double amount, XMLPriceInfo object)
	{
		object.setProcessnow(Math.ceil(amount));
	}	
	
	public String getCancellationDate(String sdate)
	{
		String b = "";
		SimpleDateFormat sdfIn = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdfTo = new SimpleDateFormat("yyyy-MM-dd");
		Date date;
		Calendar cal = Calendar.getInstance();
		try 
		{
			date	= sdfIn.parse(sdate);
			sdate	= sdfTo.format(date);
			date	= sdfTo.parse(sdate);
			cal.setTime(date);
			cal.add(Calendar.DATE, -3);
			
			b = sdfTo.format(cal.getTime());
		} 
		catch (ParseException e) 
		{
			e.printStackTrace();
		}
		
		return b;
	}

	public StringBuffer compareStringEquals(String Expected, String Actual, String TestDescription, StringBuffer ReportPrinter)
	{
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>"+TestDescription+"</td>"
		+ "<td>"+Expected+"</td>");
		if( Expected.trim().equalsIgnoreCase(Actual.trim()) )
		{
			ReportPrinter.append("<td>"+Actual+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
		}
		else
		{
			ReportPrinter.append("<td>"+Actual+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}
		return ReportPrinter;
	}	

	public StringBuffer compareStringContains(String Expected, String Actual, String TestDescription, StringBuffer ReportPrinter)
	{
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>"+TestDescription+"</td>"
		+ "<td>"+Expected+"</td>");
		if( Actual.contains(Expected) || Expected.contains(Actual) )
		{
			ReportPrinter.append("<td>"+Actual+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
		}
		else
		{
			ReportPrinter.append("<td>"+Actual+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}
		return ReportPrinter;
	}	

	public StringBuffer compareInteger(int Expected, int Actual, String TestDescription, StringBuffer ReportPrinter)
	{
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>"+TestDescription+"</td>"
		+ "<td>"+Expected+"</td>");
		if( Expected == Actual)
		{
			ReportPrinter.append("<td>"+Actual+"</td>"
			+ "<td class='Passed'>PASS</td></tr>");
			Reportcount++;
		}
		else
		{
			ReportPrinter.append("<td>"+Actual+"</td>"
			+ "<td class='Failed'>Fail</td></tr>");
			Reportcount++;
		}
		return ReportPrinter;
	}
	
	public StringBuffer compareDouble(double Expected, double Actual, String TestDescription, StringBuffer ReportPrinter)
	{
		ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
		+ "<td>"+TestDescription+"</td>"
		+ "<td>"+Expected+"</td>");
		if(PropertyMap.get("RoundUpDownStatus").trim().equalsIgnoreCase("true"))
		{
			double gap				= 0;
			
			try {
				gap	= Double.parseDouble(PropertyMap.get("RoundUpDown"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			double ceilExpected		= Math.ceil(Expected);
			double floorExpected	= Math.floor(Expected);

			if( Actual >= (floorExpected - gap) )
			{
				if( Actual <= (ceilExpected + gap) )
				{
					ReportPrinter.append("<td>"+Actual+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
				}
				else
				{
					ReportPrinter.append("<td>"+Actual+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
				}
			}
			else
			{
				ReportPrinter.append("<td>"+Actual+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
		}
		else
		{
			if( Actual == Expected )
			{
				ReportPrinter.append("<td>"+Actual+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+Actual+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
		}
		
		
		return ReportPrinter;
	}	

	public XMLPriceInfo convertValue(String CurrencyCode, XMLPriceInfo XMLO, String type, double val)
	{
		//XMLPriceInfo price = new XMLPriceInfo();
		String basefare = XMLO.getBasefareAmount();
		String taxfare = XMLO.getTaxAmount();
		/*String basefare = XMLO.getBasefareAmount().substring(0, XMLO.getBasefareAmount().length() - XMLO.getBasefareAmountDecimal() ).concat(".").concat(XMLO.getBasefareAmount().substring(XMLO.getBasefareAmount().length() - XMLO.getBasefareAmountDecimal(), XMLO.getBasefareAmount().length()));
		String taxfare = XMLO.getTaxAmount().substring(0, XMLO.getTaxAmount().length() - XMLO.getTaxAmountDecimal() ).concat(".").concat(XMLO.getTaxAmount().substring(XMLO.getTaxAmount().length() - XMLO.getTaxAmountDecimal(), XMLO.getTaxAmount().length()));*/
		//System.out.println("Basefare : "+basefare);
		//System.out.println("Totalfare : "+totalfare);
		//System.out.println("Taxfare : "+taxfare);
		//System.out.println();
		
		double Dbasefare = Double.parseDouble(basefare);
		double Dtaxfare = Double.parseDouble(taxfare);
		String profittype = type/*Sobj.getProfitType();*/;	
		double profitval =  val/*Sobj.getProfit();*/;
		//System.out.println(profittype);
		//System.out.println(profitval);
		double PortalCost = 0;
		double SellingCost = 0;
		double newbase = 0;
		double tax = 0;
		double profit = 0;

		if(profittype.toUpperCase().equalsIgnoreCase("VALUE"))
		{
			if(XMLO.getBasefareCurrencyCode().equalsIgnoreCase(PropertyMap.get("Portal_Currency_Code")))
			{
				PortalCost = Dbasefare;
				
				if(PropertyMap.get("Portal_Currency_Code").equalsIgnoreCase(CurrencyCode))
				{
					newbase = PortalCost;
					profit = profitval;
					tax = Dtaxfare;
					SellingCost = PortalCost + profitval + Dtaxfare; // Basefare + profit + tax; 
					//System.out.println(SellingCost);
				}
				else
				{
					//System.out.println(CurrencyMap.get(CurrencyCode));
					double Cost = PortalCost * Double.parseDouble(CurrencyMap.get(CurrencyCode)); //Basefare * mapvalue
					tax = Dtaxfare * Double.parseDouble(CurrencyMap.get(CurrencyCode));
					profitval = profitval * Double.parseDouble(CurrencyMap.get(CurrencyCode));
					//System.out.println(Cost);
					//System.out.println(profitval);
					//System.out.println(Dtaxfare);
					newbase = Cost;
					profit = profitval;
					SellingCost = Cost + profitval + tax; // Cost + profit + tax
					//System.out.println(SellingCost);
				}
			}
			else
			{
				double PCost = Dbasefare * Double.parseDouble(CurrencyMap.get(XMLO.getBasefareCurrencyCode()));
				tax = Dtaxfare * Double.parseDouble(CurrencyMap.get(XMLO.getBasefareCurrencyCode()));
				/*profitval = profitval * */
				//System.out.println(PCost);
				
				if(PropertyMap.get("Portal_Currency_Code").equalsIgnoreCase(CurrencyCode))
				{
					newbase = PCost;
					profit = profitval;
					SellingCost = PCost + profitval + tax; // PCost + profit + tax  
					//System.out.println(SellingCost);
				}
				else
				{
					//System.out.println(CurrencyMap.get(UIO.getCurrencyCode()));
					double SCost = PCost * Double.parseDouble(CurrencyMap.get(CurrencyCode));
					tax = Dtaxfare * Double.parseDouble(CurrencyMap.get(CurrencyCode));
					profitval = profitval * Double.parseDouble(CurrencyMap.get(CurrencyCode));
					//System.out.println(SCost);
					//System.out.println(profitval);
					//System.out.println(tax);
					newbase = SCost;
					profit = profitval;
					SellingCost = SCost + profitval + tax; //Scost + profit + tax
					//System.out.println(SellingCost);
				}
			}
		
		}
		if(profittype.toUpperCase().equalsIgnoreCase("PERCENTAGE"))
		{
			if(XMLO.getBasefareCurrencyCode().equalsIgnoreCase(PropertyMap.get("Portal_Currency_Code")))
			{
				PortalCost = Dbasefare;
				//System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
				//System.out.println();
				//System.out.println(PortalCost);
				
				if(PropertyMap.get("Portal_Currency_Code").equalsIgnoreCase(CurrencyCode))
				{
					//System.out.println("Profit : "+(PortalCost * (profitval/100)));
					newbase = PortalCost;
					profit = (PortalCost * (profitval/100));
					tax = Dtaxfare;
					SellingCost = PortalCost + profit + Dtaxfare; // Basefare + profit + tax; 
					//System.out.println(SellingCost);
					
				}
				else
				{
					double Cost = PortalCost * Double.parseDouble(CurrencyMap.get(CurrencyCode)); //Basefare * mapvalue
					tax = Dtaxfare * Double.parseDouble(CurrencyMap.get(CurrencyCode));
					//System.out.println(Cost);
					//System.out.println(tax);
					//System.out.println("Profit : "+(Cost * (profitval/100)));
					newbase = Cost;
					profit = (Cost * (profitval/100));
					SellingCost = Cost + profit + tax; // Cost + profit + tax
					//System.out.println(SellingCost);
				}
			}
			else
			{
				double PCost = Dbasefare * Double.parseDouble(CurrencyMap.get(XMLO.getBasefareCurrencyCode()));
				//System.out.println(PCost);
				tax = Dtaxfare * Double.parseDouble(CurrencyMap.get(XMLO.getBasefareCurrencyCode()));
				
				if(PropertyMap.get("Portal_Currency_Code").equalsIgnoreCase(CurrencyCode))
				{
					newbase = PCost;
					profit = (PCost * (profitval/100));
					SellingCost = PCost + profit + tax; // PCost + profit + tax 
					//System.out.println(SellingCost);
				}
				else
				{
					double SCost = PCost * Double.parseDouble(CurrencyMap.get(CurrencyCode));
					//System.out.println(SCost);
					newbase = SCost;
					profit = (SCost * (profitval/100));
					SellingCost = SCost + profit + tax; //Scost + profit + tax
					//System.out.println(SellingCost);
				}
			}
		
		}
		
		SellingCost = Math.ceil(SellingCost);
		newbase = Math.ceil(newbase);
		tax = Math.ceil(tax);
		profit = Math.ceil(profit);
		
		//System.out.println("Selling Cost : "+SellingCost);
		XMLO.setNewbase(newbase);
		//System.out.println(newbase);
		XMLO.setNewtax(tax);
		//System.out.println(tax);
		XMLO.setProfit(profit);
		//System.out.println(profit);
		XMLO.setSellingCost(SellingCost);
		//System.out.println(newbase+tax+profit);
		//System.out.println(SellingCost);
		XMLO.setNewBasefareCurrencyCode(CurrencyCode);
		
		return XMLO;
	
	}
	
	public StringBuffer validateQuotationPage(QuotationPage uiconfirmpage, StringBuffer ReportPrinter, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight)
	{
		try
		{	
			int xmloutbound = XMLSelectFlight.getOriginoptions().get(0).getFlightlist().size();
			int xmlinbound  = 0;
			
			ArrayList<Flight> xmloutbflightlist = new ArrayList<Flight>();
			ArrayList<Flight> xmlinbflightlist  = new ArrayList<Flight>();
			
			ArrayList<Flight> confOutbflightlist = new ArrayList<Flight>();
			ArrayList<Flight> confInbflightlist  = new ArrayList<Flight>();
		
			xmloutbflightlist = XMLSelectFlight.getOriginoptions().get(0).getFlightlist();
			confOutbflightlist = uiconfirmpage.getOutbound().getOutBflightlist();
			
			boolean twoway = false;
			if(XMLSelectFlight.getDirectiontype().equals("Circle"))
			{
				twoway = true;
				xmlinbound = XMLSelectFlight.getOriginoptions().get(1).getFlightlist().size();
				xmlinbflightlist = XMLSelectFlight.getOriginoptions().get(1).getFlightlist();
				confInbflightlist = uiconfirmpage.getInbound().getInBflightlist();
			}
			
			ReportPrinter = validateFlights(xmloutbound, "Outbound", confOutbflightlist, xmloutbflightlist, ReportPrinter );
			if(twoway)
			{
				ReportPrinter = validateFlights(xmlinbound, "Inbound", confInbflightlist, xmlinbflightlist, ReportPrinter );
			}
			
			
			Traveler Fillmaincus = new Traveler();
			Traveler Confmaincus = new Traveler();
			Address FillmainCusAddress = new Address();
			Address ConfmainCusAddress = new Address();
			
			try
			{
				Fillmaincus = fillingObject.getMaincustomer();
				Confmaincus = uiconfirmpage.getMaincustomer();
				FillmainCusAddress = Fillmaincus.getAddress();
				ConfmainCusAddress = Confmaincus.getAddress();
			}
			catch(Exception e)
			{
				
			}
			
			//////////////////////////////////////////////////////////////////////////////////
			try {
				statusPassFail = CommonValidator.compareStringContains(Fillmaincus.getGivenName().trim(), Confmaincus.getGivenName().trim(), "Direct customer's given name", ReportPrinter, Reportcount);
				Reportcount++;
			} catch (Exception e) {
				
			}
			
			//////////////////////////////////////////////////////////////////////////////////
			try {
				statusPassFail = CommonValidator.compareStringContains(Fillmaincus.getSurname().trim(), Confmaincus.getSurname().trim(), "Direct customer's surname", ReportPrinter, Reportcount);
				Reportcount++;
			} catch (Exception e) {
				
			}
			
			//////////////////////////////////////////////////////////////////////////////////
			try {
				statusPassFail = CommonValidator.compareStringContains(FillmainCusAddress.getAddressStreetNo().trim(), ConfmainCusAddress.getAddressStreetNo().trim(), "Direct customer address Street No", ReportPrinter, Reportcount);
				Reportcount++;
			} catch (Exception e) {
				
			}
			
			//////////////////////////////////////////////////////////////////////////////////
			try {
				statusPassFail = CommonValidator.compareStringContains(FillmainCusAddress.getAddressCity().trim(), ConfmainCusAddress.getAddressCity().trim(), "Direct customer address City", ReportPrinter, Reportcount);
				Reportcount++;
			} catch (Exception e) {
				
			}
			
			//////////////////////////////////////////////////////////////////////////////////
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Direct customer address PostalCode</td>"
			+ "<td>"+FillmainCusAddress.getPostalCode()+"</td>");
			if(ConfmainCusAddress.getPostalCode().equalsIgnoreCase(FillmainCusAddress.getPostalCode()))
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getPostalCode()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else if(ConfmainCusAddress.getPostalCode().contains("-") && FillmainCusAddress.getPostalCode().equals(""))
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getPostalCode()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getPostalCode()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			//////////////////////////////////////////////////////////////////////////////////
			
			statusPassFail = CommonValidator.compareStringContains(FillmainCusAddress.getAddressCountry().trim(), ConfmainCusAddress.getAddressCountry().trim(), "Direct customer address Country", ReportPrinter, Reportcount);
			Reportcount++;
			//////////////////////////////////////////////////////////////////////////////////
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Direct customer address State Code</td>"
			+ "<td>"+FillmainCusAddress.getStateProv()+"</td>");
			if(ConfmainCusAddress.getStateProv().equalsIgnoreCase(FillmainCusAddress.getStateProv()))
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getStateProv()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else if(ConfmainCusAddress.getStateProv().contains("-") && FillmainCusAddress.getStateProv().equals(""))
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getStateProv()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+ConfmainCusAddress.getStateProv()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return ReportPrinter;

	}
	
	
	//=================================TOUR OPERATOR VALIDATION==================================
	
	public void validateTOafterVoucher(TOperator beforePay, TOperator afterPay, XMLPriceItinerary XMLSelectFlight, SearchObject searchObject, AirConfig conf, StringBuffer ReportPrinter )
	{
		ReportPrinter.append("<span><center><p class='Hedding0'>TO Booking Validation after issue voucher</p></center></span>");
		ReportPrinter.append("<table style=width:100%>"
		+ "<tr><th>Test Case</th>"
		+ "<th>Test Description</th>"
		+ "<th>Expected Result</th>"
		+ "<th>Actual Result</th>"
		+ "<th>Test Status</th></tr>");
			
		double XMLFinalTotalBookingValue = 0;
			
		if(searchObject.getPaymentMode().equalsIgnoreCase("Pay Online"))
		{
			if( conf.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Full_Amount_at_Booking"))
			{
				XMLFinalTotalBookingValue = XMLSelectFlight.getPricinginfo().getNewbase() + XMLSelectFlight.getPricinginfo().getProfit() + XMLSelectFlight.getPricinginfo().getNewtax() + XMLSelectFlight.getPricinginfo().getBookingfee() - XMLSelectFlight.getPricinginfo().getDiscount()/* + XMLSelectFlight.getPricinginfo().getCreditcardfeeinSellCurr()*/;
			}
			else if(conf.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Booking_Fee_as_Deposit"))
			{
				XMLFinalTotalBookingValue = XMLSelectFlight.getPricinginfo().getBookingfee() - XMLSelectFlight.getPricinginfo().getDiscount();
			}
		}
		else
		{
			XMLFinalTotalBookingValue = XMLSelectFlight.getPricinginfo().getNewbase() + XMLSelectFlight.getPricinginfo().getProfit() + XMLSelectFlight.getPricinginfo().getNewtax() + XMLSelectFlight.getPricinginfo().getBookingfee() - XMLSelectFlight.getPricinginfo().getDiscount();
		}
			
		String ToCurrency				= "USD"; 
		String InCurrency				= "USD";
		ToCurrency						= beforePay.getCurrency();
		InCurrency						= XMLSelectFlight.getPricinginfo().getNewBasefareCurrencyCode();
		//double aftercreditLimit			= 0;
		double aftercreditBlanace		= 0;
		double aftercreditUtilization	= 0;
			
		double totalInTOCurrency	= 0;
		totalInTOCurrency			= this.convert(InCurrency, ToCurrency, XMLFinalTotalBookingValue);
			
		aftercreditBlanace				= beforePay.getCreditBlanace() - totalInTOCurrency;
		aftercreditUtilization			= beforePay.getCreditUtilization() + totalInTOCurrency;
			
		statusPassFail	= CommonValidator.compareDouble(aftercreditBlanace, afterPay.getCreditBlanace(), "Credit Balanace", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
			
		statusPassFail	= CommonValidator.compareDouble(aftercreditUtilization, afterPay.getCreditUtilization(), "Credit Utilization", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
			
		ReportPrinter.append("</table>");
		ReportPrinter.append("<br><br><br>");
	}
		
	public void validateTOafterPay(TOperator beforePay, TOperator afterPay, XMLPriceItinerary XMLSelectFlight, SearchObject searchObject, AirConfig conf, StringBuffer ReportPrinter )
	{
		ReportPrinter.append("<span><center><p class='Hedding0'>TO Booking Validation After Payment</p></center></span>");
		ReportPrinter.append("<table style=width:100%>"
		+ "<tr><th>Test Case</th>"
		+ "<th>Test Description</th>"
		+ "<th>Expected Result</th>"
		+ "<th>Actual Result</th>"
		+ "<th>Test Status</th></tr>");
			
		statusPassFail	= CommonValidator.compareDouble(beforePay.getCreditBlanace(), afterPay.getCreditBlanace(), "Credit Balanace", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
			
		statusPassFail	= CommonValidator.compareDouble(beforePay.getCreditUtilization(), afterPay.getCreditUtilization(), "Credit Utilization", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
			
		statusPassFail	= CommonValidator.compareDouble(beforePay.getCreditLimit(), afterPay.getCreditLimit(), "Credit Limit", ReportPrinter, Reportcount, PropertyMap.get("RoundUpDownStatus"), PropertyMap.get("RoundUpDownBy"));
		Reportcount++;
			
		ReportPrinter.append("</table>");
		ReportPrinter.append("<br><br><br>");
	}

	


}
