package Results.Validate;

import java.util.HashMap;

import org.openqa.selenium.WebDriver;

import Flight.Cancellation.CancellationReport;
import Flight.Cancellation.CancellationRequest;
import Flight.Cancellation.CancellationRequestReader;
import Flight.Cancellation.CancellationResponse;
import Flight.Cancellation.CancellationResponseReader;
import Flight.Cancellation.CancellationScreen;
import system.classes.AirConfig;
import system.classes.ReservationInfo;
import system.classes.ReservationReport;
import system.classes.SearchObject;
import system.classes.XMLPriceItinerary;
import Flight.Reports.BookingConfReportReader;
import Flight.Reports.BookingListReport;
import Flight.Reports.CustomerVoucherEmail;
import Flight.Reports.ProfitAndLoss;
import Flight.Reports.ReservationReportReader;
import Flight.Reports.SupplierPayableReport;
import system.enumtypes.XMLFileType;
import system.enumtypes.XMLLocateType;


public class ValidateReports {
	
	HashMap<String, String>	Propertymap	= new HashMap<String, String>();
	Validate				validate	= null;
	ValidateCC				validatecc	= null;
	
	public ValidateReports(HashMap<String, String>	Property, Validate valid)
	{
		this.Propertymap	= Property;
		this.validate		= valid;
	}
	public ValidateReports(HashMap<String, String>	Property, ValidateCC valid)
	{
		this.Propertymap	= Property;
		this.validatecc		= valid;
	}
	
	public StringBuffer validateReservationReport(WebDriver driver, String resNo, StringBuffer ReportPrinter, SearchObject searchObject, AirConfig configuration, XMLPriceItinerary XMLSelectFlight, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = resNo.trim();
			
		//RESERVATION REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Reservation Report Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			ReservationReportReader	resvReportReader	= new ReservationReportReader(Propertymap);
			ReservationReport		reservationReport	= new ReservationReport();
			try {
				reservationReport	= resvReportReader.getReport(driver, ReservationNo);
			} catch (Exception e) {
				
			}
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Reservation Report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			
			if(reservationReport.isAvailable())
			{
				ReportPrinter.append("<td>"+reservationReport.isAvailable()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;	
				
				ReportPrinter	= validate.validateReservationReport(reservationReport, searchObject, XMLSelectFlight, configuration, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+reservationReport.isAvailable()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}

		return ReportPrinter;
	}
	
	public StringBuffer validateSuppPlayableReport(WebDriver driver, String resNo, String supConfNo, StringBuffer ReportPrinter, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, AirConfig configuration, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = resNo.trim();
			
		//SUPPLIER PAYABLE REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Supplier Payable Report Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			SupplierPayableReport suppPayableReport = new SupplierPayableReport(Propertymap);
			
			suppPayableReport.getPaybleDetails(driver, Propertymap.get("Flight_Supplier"), ReservationNo);
			SupplierPayableReport suppPayableReportBeforePay = suppPayableReport;
			
			if( searchObject.getSupplierPayablePayStatus().equalsIgnoreCase("YES") )
			{
				suppPayableReport.paySupplier(Propertymap.get("Payment.Reference"));
			}
			
			SupplierPayableReport suppPayableReportAfterPay = suppPayableReport;
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Supplier Payable Report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			
			if(suppPayableReport.isRecordExist())
			{
				ReportPrinter.append("<td>"+suppPayableReport.isRecordExist()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;	
				
				ReportPrinter = validate.validateSupplierPayableReport(suppPayableReportBeforePay, suppPayableReportAfterPay, searchObject, XMLSelectFlight, ReservationNo.trim(), supConfNo.trim(), fillingObject, configuration, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+suppPayableReport.isRecordExist()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}

		return ReportPrinter;
	}
	
	public StringBuffer validateCustomerVoucherEmail(WebDriver driver, String resNo, StringBuffer ReportPrinter, SearchObject searchObject, XMLPriceItinerary XMLSelectFlight, ReservationInfo fillingObject, AirConfig configuration, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = resNo.trim();
		//CUSTOMER VOUCHER VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Customer Voucher E-mail Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			CustomerVoucherEmail	email	= new CustomerVoucherEmail(Propertymap);
			email.getCusVoucherEmail(driver, ReservationNo);
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Customer Voucher email availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			
			if(email.isAvailable())
			{
				ReportPrinter.append("<td>"+email.isAvailable()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				
				ReportPrinter	= validate.validateCustomerVoucherEmail(email, fillingObject, ReservationNo, XMLSelectFlight, searchObject, configuration, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+email.isAvailable()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}

		return ReportPrinter;
	}	
	
	public StringBuffer validateBookingConfReport(WebDriver driver, String resNo, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, AirConfig configuration, StringBuffer ReportPrinter, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = resNo.trim();
		
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Booking Confirmation Report Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			BookingConfReportReader	report	= new BookingConfReportReader(Propertymap);
			report.getReport(driver, ReservationNo);
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Booking Confirmation Report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(report.isAvailable())
			{
				ReportPrinter.append("<td>"+report.isAvailable()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;  
				
				ReportPrinter = validate.validateBookingConfEmail(report, searchObject, fillingObject, XMLSelectFlight, configuration, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+report.isAvailable()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		
		return ReportPrinter;
	}
	
	public StringBuffer validateBookingListReport(WebDriver driver, String resNo, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, AirConfig configuration, StringBuffer ReportPrinter, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = resNo.trim();
			
		//BOOKING LIST REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Booking List Report Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			BookingListReport	report	= new BookingListReport(Propertymap);
			report.getReport(driver, ReservationNo);
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Customer Voucher email availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(report.isIsreportLoaded())
			{
				ReportPrinter.append("<td>"+report.isIsreportLoaded()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				
				ReportPrinter = validate.validateBookingListReport(report, fillingObject, XMLSelectFlight, searchObject, ReservationNo, configuration, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+report.isIsreportLoaded()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			//System.out.println();
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		return ReportPrinter;
	}
	
	public StringBuffer validateProfitLossReport(WebDriver driver, String resNo, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, AirConfig configuration, StringBuffer ReportPrinter, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = resNo.trim();
			
		//Profit and Loss Report VALIDATION
		try
		{
			ProfitAndLoss report	= new ProfitAndLoss(Propertymap);
			try {
				report.loadReport(driver, ReservationNo);
			} catch (Exception e) {
				
			}
			
			
			ReportPrinter.append("<span><center><p class='Hedding0'>Profit and Loss Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check profit and loss report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(report.isReportLoaded())
			{
				ReportPrinter.append("<td>"+report.isReportLoaded()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				
				ReportPrinter = validate.validateProfitAndLoss(XMLSelectFlight, searchObject, report, configuration, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+report.isReportLoaded()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		return ReportPrinter;
	}
	
	public StringBuffer doCancellation(WebDriver driver, CancellationScreen cancellationScreen, CancellationReport cancellationReport, String resNo, String supConfNo, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, StringBuffer ReportPrinter, int Reportcount)
	{
		//======================================CANCELLATION SCREEN VALIDATION=====================================
		
		try {
			
			try {
				System.out.println();
				cancellationScreen.getCancellationScreen(driver, resNo.trim(), XMLSelectFlight, searchObject);
			} catch (Exception e) {
				
			}
			ReportPrinter.append("<span><center><p class='Hedding0'>Cancellation Screen Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Cancellation Screen Availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(!cancellationScreen.isNotFound())
			{
				ReportPrinter.append("<td>"+cancellationScreen.isNotFound()+"</td>"
				+ "<td class = 'Passed'>PASS</td></tr>" );
				Reportcount++;
				
				try {
					ReportPrinter = validate.validateCancellationScreen(XMLSelectFlight, searchObject, fillingObject, resNo, supConfNo, cancellationScreen, ReportPrinter);
				} catch (Exception e) {
					
				}
				
				ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
				+ "<td>Check Cancellation Status</td>"
				+ "<td>If Cancellation Done - true and Display Message , If Not Done - false and Display Error Message</td>");
				if(cancellationScreen.isCancelled())
				{
					ReportPrinter.append("<td>"+cancellationScreen.isCancelled()+"<br>"+cancellationScreen.getCancellationMessage()+"</td>"
					+ "<td class = 'Passed'>PASS</td></tr>" );
					Reportcount++;
				}
				else
				{
					ReportPrinter.append("<td>"+cancellationScreen.isCancelled()+"<br>"+cancellationScreen.getCancellationMessage()+"</td>"
					+ "<td class = 'Failed'>Fail</td></tr>");
					Reportcount++;
				}
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");	
			}
			else
			{
				ReportPrinter.append("<td>"+cancellationScreen.isNotFound()+"</td>"
				+ "<td class = 'Failed'>Fail</td></tr>");
				Reportcount++;
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			
			if( !cancellationScreen.isNotFound() && cancellationScreen.isCancelled() )
			{
				
				try {
					cancellationReport.loadReport(driver, resNo.trim());
				} catch (Exception e) {
						
				}
					
				ReportPrinter.append("<span><center><p class='Hedding0'>Cancellation Report Validation</p></center></span>");
				ReportPrinter.append("<table style=width:100%>"
				+ "<tr><th>Test Case</th>"
				+ "<th>Test Description</th>"
				+ "<th>Expected Result</th>"
				+ "<th>Actual Result</th>"
				+ "<th>Test Status</th></tr>");
				ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
				+ "<td>Check Cancellation Report Availability</td>"
				+ "<td>If Available - true , If Not Available - false</td>");
				if(cancellationReport.isReportLoded())
				{
					ReportPrinter.append("<td>"+cancellationReport.isReportLoded()+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
					try {
						ReportPrinter = validate.validateCancellationReport(XMLSelectFlight, searchObject, fillingObject, resNo, supConfNo, cancellationScreen, cancellationReport, ReportPrinter);
					} catch (Exception e) {
						
					}
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
					
					
					try {
						
						CancellationRequest cancellationRequest = new CancellationRequest();
						CancellationRequestReader cancellationReqReader = new CancellationRequestReader(Propertymap);
						ReportPrinter.append("<span><center><p class='Hedding0'>Cancellation Request Validation</p></center></span>");
						ReportPrinter.append("<table style=width:100%>"
						+ "<tr><th>Test Case</th>"
						+ "<th>Test Description</th>"
						+ "<th>Expected Result</th>"
						+ "<th>Actual Result</th>"
						+ "<th>Test Status</th></tr>");
						cancellationRequest = cancellationReqReader.RequestReader(XMLLocateType.TRACER, /*searchObject.getTracer()*/supConfNo.trim(), XMLFileType.AIR1_Cancellationrequest_);
						ReportPrinter = validate.validateCancellationRequest(cancellationRequest, supConfNo.trim(), ReportPrinter);
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");
					} catch (Exception e) {
						
					}
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
					
					try {
						
						CancellationResponse cancellationResponse = new CancellationResponse();
						CancellationResponseReader cancellationResReader = new CancellationResponseReader(Propertymap);
						ReportPrinter.append("<span><center><p class='Hedding0'>Cancellation Response Validation</p></center></span>");
						ReportPrinter.append("<table style=width:100%>"
						+ "<tr><th>Test Case</th>"
						+ "<th>Test Description</th>"
						+ "<th>Expected Result</th>"
						+ "<th>Actual Result</th>"
						+ "<th>Test Status</th></tr>");
						cancellationResponse = cancellationResReader.RequestReader(XMLLocateType.TRACER, /*searchObject.getTracer()*/supConfNo.trim(), XMLFileType.AIR1_CancellationResponse_);
						ReportPrinter = validate.validateCancellationResponse(cancellationResponse, supConfNo.trim(), cancellationReport, ReportPrinter);
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");
					} catch (Exception e) {
						
					}
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
					
				}
				else
				{
					ReportPrinter.append("<td>"+cancellationReport.isReportLoded()+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
				}
			}
			
		} catch (Exception e) {
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		
		return ReportPrinter;
	}
	
	public StringBuffer validateProfitLossReportAfterCancel(WebDriver driver, CancellationScreen cancellationScreen, CancellationReport cancellationReport, String confirmationpage, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, AirConfig configuration, StringBuffer ReportPrinter, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage/*.getReservationNo()*/.trim().concat("-C");
			
		//Profit and Loss Report VALIDATION
		try
		{
			ProfitAndLoss profitlossReport	= new ProfitAndLoss(Propertymap);
			profitlossReport.loadReport(driver, ReservationNo);
			//System.out.println();
			ReportPrinter.append("<span><center><p class='Hedding0'>Profit and Loss Validation After Cancellation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check profit and loss report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(profitlossReport.isReportLoaded())
			{
				ReportPrinter.append("<td>"+profitlossReport.isReportLoaded()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				
				ReportPrinter = validate.validateProfitAndLossAfterCancellation(profitlossReport, cancellationScreen, cancellationReport, XMLSelectFlight, searchObject, ReportPrinter);
				//System.out.println();
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+profitlossReport.isReportLoaded()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		return ReportPrinter;
	}
	
	public StringBuffer validateThirdPartySuppPayableAfterCancel(WebDriver driver, String resNo, String suppConNo, SearchObject searchObject, XMLPriceItinerary XMLSelectFlight, AirConfig configuration, StringBuffer ReportPrinter, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = resNo/*.getReservationNo()*/.trim();
			
		//SUPPLIER PAYABLE REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Supplier Payable Report Validation After Cancellation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			SupplierPayableReport suppPayableReport = new SupplierPayableReport(Propertymap);
			
			suppPayableReport.getPaybleDetails(driver, Propertymap.get("Flight_Supplier"), ReservationNo);
			SupplierPayableReport suppPayableReportBeforePay = suppPayableReport;
			
			if( searchObject.getSupplierPayablePayStatus().equalsIgnoreCase("YES") )
			{
				suppPayableReport.paySupplier(Propertymap.get("Payment.Reference"));
				//SupplierPayableReport suppPayableReportAfterPay = suppPayableReport;
			}
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Supplier Payable Report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			
			if(suppPayableReport.isRecordExist())
			{
				ReportPrinter.append("<td>"+suppPayableReport.isRecordExist()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;	
				
				ReportPrinter = validate.validateSupplierPayableAfterCancellation(resNo, suppConNo, suppPayableReportBeforePay, XMLSelectFlight, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+suppPayableReport.isRecordExist()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			//e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}

		return ReportPrinter;
	}
	
	public StringBuffer validateReservationReportAfterCancel(WebDriver driver, String resNo, SearchObject searchObject, XMLPriceItinerary XMLSelectFlight, AirConfig configuration, StringBuffer ReportPrinter, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = resNo/*.getReservationNo()*/.trim();
			
		//RESERVATION REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Reservation Report Validation After Cancellation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			ReservationReportReader	resvReportReader	= new ReservationReportReader(Propertymap);
			ReservationReport		reservationReport	= new ReservationReport();
			try {
				reservationReport	= resvReportReader.getReport(driver, ReservationNo);
			} catch (Exception e) {
				System.out.println();
			}
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Reservation should not exist in report after cancellation</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			
			if(!reservationReport.isAvailable())
			{
				ReportPrinter.append("<td>"+reservationReport.isAvailable()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;	
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+reservationReport.isAvailable()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}

		return ReportPrinter;
		
	}
	
	public StringBuffer validateBookingListReportAfterCancel(WebDriver driver, String resNo, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, AirConfig configuration, StringBuffer ReportPrinter, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = resNo/*.getReservationNo()*/.trim();
			
		//BOOKING LIST REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Booking List Report Validation After Cancellation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			BookingListReport	report	= new BookingListReport(Propertymap);
			report.getReport(driver, ReservationNo);
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Customer Voucher email availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(report.isIsreportLoaded())
			{
				ReportPrinter.append("<td>"+report.isIsreportLoaded()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				System.out.println();
				ReportPrinter = validate.validateBookingListReportAfterCancellation(report, fillingObject, XMLSelectFlight, searchObject/*, confirmationpage*/, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+report.isIsreportLoaded()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		return ReportPrinter;
	}
	
}
