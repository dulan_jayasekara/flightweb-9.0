package Results.Validate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;





import org.openqa.selenium.By;



//import org.openqa.selenium.By;
import java.util.Iterator;




//import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;





import com.common.Validators.CommonValidator;



//import system.classes.Flight;
//import system.classes.Inbound;
//import system.classes.Outbound;
import system.classes.UIFlightItinerary;

public class ValidateFilters {

	HashMap<String, String>					PropertyMap		= new HashMap<String, String>();
	private ArrayList<UIFlightItinerary>	Resultlist		= new ArrayList<UIFlightItinerary>();
	private boolean							twoway			= false;
	private ArrayList<WebElement>			flights			= new ArrayList<WebElement>();
	boolean									done			= false;
	
	public ValidateFilters(HashMap<String, String> propertymap)
	{
		PropertyMap = propertymap;
	}
	
	public ArrayList<UIFlightItinerary> validateFilters(WebDriver driver, StringBuffer ReportPrinter, String trip)
	{
		Resultlist		= new ArrayList<UIFlightItinerary>();
		
		if(trip.trim().equalsIgnoreCase("Round Trip"))
		{
			twoway = true;
		}
		else if(trip.trim().equalsIgnoreCase("One Way Trip"))
		{
			twoway = false;
		}
		
		try {

			int nflights = 0;
			driver.switchTo().defaultContent();
			String numofflights = "0";
			numofflights = driver.findElement(By.className(PropertyMap.get("ResultPg_FlightCount_Lbl_class"))).getText();
			
			nflights = Integer.parseInt(numofflights.trim());
			int navcount1 = nflights/10;
			int navcount2 = nflights%10;
			
			int navigate = navcount1;
			
			if((navcount2>0)&&(navcount2<10))
			{
				navigate+=1;
				
			}
			int clickpattern = 2; 
			int count = 0;
			
			
			while(count<navigate)
			{
				try
				{	
					flights = new ArrayList<WebElement>(driver.findElements(By.className(PropertyMap.get("ResultPg_FlightItinerary_article_class").trim()/*"result-container-flights"*/)));
					
					Iterator<WebElement> flightsIter = flights.iterator();
					int index = 0;
					while(flightsIter.hasNext())
					{
						UIFlightItinerary flightItinerary = new UIFlightItinerary();
						
						String OBDepFlightCode		= "";
						OBDepFlightCode				= flights.get(index).findElement(By.id(PropertyMap.get("ResultPg_OutDepFlightCode_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
						flightItinerary.setOBDepFlightCode(OBDepFlightCode);
						
						String OBDepLocationCode	= "";
						OBDepLocationCode			= flights.get(index).findElement(By.id(PropertyMap.get("ResultPg_OutDepLocCode_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim().split("[()]")[1].trim();
						flightItinerary.setOBDepLocationCode(OBDepLocationCode);
						
						String OBDepDate			= "";
						OBDepDate					= flights.get(index).findElement(By.id(PropertyMap.get("ResultPg_OutDepDate_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
						flightItinerary.setOBDepDate(OBDepDate);
						
						String OBDepTime			= "";
						OBDepTime					= flights.get(index).findElement(By.id(PropertyMap.get("ResultPg_OutDepTime_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
						flightItinerary.setOBDepTime(OBDepTime);
						
						String OBArrFlightCode		= "";
						OBArrFlightCode				= flights.get(index).findElement(By.id(PropertyMap.get("ResultPg_OutArrFlightCode_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
						flightItinerary.setOBArrFlightCode(OBArrFlightCode);
						
						String OBArrLocationCode	= "";
						OBArrLocationCode			= flights.get(index).findElement(By.id(PropertyMap.get("ResultPg_OutArrLocCode_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim().split("[()]")[1].trim();
						flightItinerary.setOBArrLocationCode(OBArrLocationCode);
						
						String OBArrDate			= "";
						OBArrDate					= flights.get(index).findElement(By.id(PropertyMap.get("ResultPg_OutArrDate_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
						flightItinerary.setOBArrDate(OBArrDate);
						
						String OBArrTime			= "";
						OBArrTime					= flights.get(index).findElement(By.id(PropertyMap.get("ResultPg_OutArrTime_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
						flightItinerary.setOBArrTime(OBArrTime);
						
						String OBDuration			= "";
						OBDuration					= flights.get(index).findElement(By.id(PropertyMap.get("ResultPg_OutDuration_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
						flightItinerary.setOBDuration(OBDuration);
						
						String OBStops				= "";
						OBStops						= flights.get(index).findElement(By.id(PropertyMap.get("ResultPg_OutStops_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
						flightItinerary.setOBStops(OBStops);
						
						String OBLayover			= "";
						OBLayover					= flights.get(index).findElement(By.id(PropertyMap.get("ResultPg_OutLayover_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
						flightItinerary.setOBLayover(OBLayover);
						
						if(twoway)
						{
							String IBDepFlightCode		= "";
							IBDepFlightCode				= flights.get(index).findElement(By.id(PropertyMap.get("ResultPg_InDepFlightCode_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
							flightItinerary.setIBDepFlightCode(IBDepFlightCode);
							
							String IBDepLocationCode	= "";
							IBDepLocationCode			= flights.get(index).findElement(By.id(PropertyMap.get("ResultPg_InDepLocCode_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim().split("[()]")[1].trim();
							flightItinerary.setIBDepLocationCode(IBDepLocationCode);
							
							String IBDepDate			= "";
							IBDepDate					= flights.get(index).findElement(By.id(PropertyMap.get("ResultPg_InDepDate_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
							flightItinerary.setIBDepDate(IBDepDate);
							
							String IBDepTime			= "";
							IBDepTime					= flights.get(index).findElement(By.id(PropertyMap.get("ResultPg_InDepTime_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
							flightItinerary.setIBDepTime(IBDepTime);
							
							String IBArrFlightCode		= "";
							IBArrFlightCode				= flights.get(index).findElement(By.id(PropertyMap.get("ResultPg_InArrFlightCode_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
							flightItinerary.setIBArrFlightCode(IBArrFlightCode);
							
							String IBArrLocationCode	= "";
							IBArrLocationCode			= flights.get(index).findElement(By.id(PropertyMap.get("ResultPg_InArrLocCode_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim().split("[()]")[1].trim();
							flightItinerary.setIBArrLocationCode(IBArrLocationCode);
							
							String IBArrDate			= "";
							IBArrDate					= flights.get(index).findElement(By.id(PropertyMap.get("ResultPg_InArrDate_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
							flightItinerary.setIBArrDate(IBArrDate);
							
							String IBArrTime			= "";
							IBArrTime					= flights.get(index).findElement(By.id(PropertyMap.get("ResultPg_InArrTime_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
							flightItinerary.setIBArrTime(IBArrTime);
							
							String IBDuration			= "";
							IBDuration					= flights.get(index).findElement(By.id(PropertyMap.get("ResultPg_InDuration_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
							flightItinerary.setIBDuration(IBDuration);
							
							String IBStops				= "";
							IBStops						= flights.get(index).findElement(By.id(PropertyMap.get("ResultPg_InStops_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
							flightItinerary.setIBStops(IBStops);
							
							String IBLayover			= "";
							IBLayover					= flights.get(index).findElement(By.id(PropertyMap.get("ResultPg_InLayover_Lbl_id").trim().replace("REPLACE", String.valueOf(index + 1)))).getText().trim();
							flightItinerary.setIBLayover(IBLayover);	
						}
						
						
						String currencyCode = "";
						String cost			= "0";
						try {
							currencyCode		= flights.get(index).findElement(By.className(PropertyMap.get("ResultPg_ItineraryCurrencyCode_Lbl_class").trim())).getText().trim();
							cost				= flights.get(index).findElement(By.className(PropertyMap.get("ResultPg_ItineraryPrice_Lbl_class").trim())).getText().trim();
						} catch (Exception e) {
							e.printStackTrace();
						}
						flightItinerary.setCurrencyCode(currencyCode);
						flightItinerary.setTotalCost(cost);
						
						//Resultlist = new ArrayList<UIFlightItinerary>();
						this.Resultlist.add(flightItinerary);
						index++;
					}	
				}	
				catch(Exception e)
				{
					
				}

				((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
				clickpattern++;		
				count++; 

			}
			
			clickpattern = 1;
			((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return Resultlist;
	}
	
	public boolean validateAllFilters(WebDriver driver, StringBuffer ReportPrinter, int Reportcount, String trip/*, ArrayList<UIFlightItinerary>	Resultlist*/)
	{
		System.out.println("==================================");
		System.out.println("VALIDATING FILTERS");
		
		boolean status = false;
		
		ArrayList<UIFlightItinerary>	ORIGINAL		= new ArrayList<UIFlightItinerary>();
		ArrayList<Double>				Original		= new ArrayList<Double>();
		try {
			ORIGINAL = validateFilters(driver, ReportPrinter, trip);
			for(int y = 0; y<ORIGINAL.size(); y++)
			{
				Original.add(Double.parseDouble(ORIGINAL.get(y).getTotalCost()) );
			}
		} catch (Exception e) {
			
		}
		
		
		//Filters : Price - Ascending
		try 
		{
			new Select(driver.findElement(By.id(PropertyMap.get("ResultPg_FlightSortOptions_DropDwn_id")))).selectByVisibleText("Price - Ascending");
			ArrayList<UIFlightItinerary> FILTEREDASCE = new ArrayList<UIFlightItinerary>();
			FILTEREDASCE = validateFilters(driver, ReportPrinter, trip);
			
			Collections.sort(Original);
			boolean pass = true;
			
			ReportPrinter.append("<span><center><p class='Hedding0'>Filters : Price - Ascending</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			for(int i = 0; i<Original.size(); i++)
			{
				ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
				+ "<td> Price Acsending </td>"
				+ "<td>"+Original.get(i)+"</td>");
				if(Original.get(i) == (Double.parseDouble(FILTEREDASCE.get(i).getTotalCost()) ))
				{
					pass = true;
				}
				else
				{
					pass = false;
				}
				
				if( pass ) 
				{
					ReportPrinter.append("<td>"+(Double.parseDouble(FILTEREDASCE.get(i).getTotalCost()) )+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
				}
				else
				{
					ReportPrinter.append("<td>"+(Double.parseDouble(FILTEREDASCE.get(i).getTotalCost()) )+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
				}
			}
		
			
			new Select(driver.findElement(By.id(PropertyMap.get("ResultPg_FlightSortOptions_DropDwn_id")))).selectByVisibleText("Price - Ascending");
			int clickpattern = 1;
			((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
		} catch (Exception e) {
			
		}
		
		ReportPrinter.append("</table>");
		ReportPrinter.append("<br><br><br>");
		
		
		
		//Filters : Price - Descending
		try 
		{
			Thread.sleep(3000);
			
			new Select(driver.findElement(By.id(PropertyMap.get("ResultPg_FlightSortOptions_DropDwn_id")))).selectByVisibleText("Price - Descending");
			ArrayList<UIFlightItinerary> FILTEREDDESC = new ArrayList<UIFlightItinerary>();
			FILTEREDDESC = validateFilters(driver, ReportPrinter, trip);
			
			Collections.sort(Original);
			Collections.reverse(Original);
			boolean pass = true;
			
			ReportPrinter.append("<span><center><p class='Hedding0'>Filters : Price - Descending</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
					
			for(int i = 0; i<Original.size(); i++)
			{
				ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
				+ "<td> Price Decsending </td>"
				+ "<td>"+Original.get(i)+"</td>");
				
				//System.out.println( (Double.parseDouble(FILTEREDDESC.get(i).getTotalCost())) );
				
				if(Original.get(i) == (Double.parseDouble(FILTEREDDESC.get(i).getTotalCost()) ))
				{
					pass = true;
				}
				else
				{
					pass = false;
				}
						
				if( pass ) 
				{
					ReportPrinter.append("<td>"+(Double.parseDouble(FILTEREDDESC.get(i).getTotalCost()) )+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
				}
				else
				{
					ReportPrinter.append("<td>"+(Double.parseDouble(FILTEREDDESC.get(i).getTotalCost()) )+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
				}
			}
			
			new Select(driver.findElement(By.id(PropertyMap.get("ResultPg_FlightSortOptions_DropDwn_id")))).selectByVisibleText("Price - Ascending");
			int clickpattern = 1;
			((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
				
		} catch (Exception e) {
					
		}
				
		ReportPrinter.append("</table>");
		ReportPrinter.append("<br><br><br>");

		
		//SLIDER MINMAX VALUE
		try {
			
			String X = driver.findElement(By.id("price_text_WJ_6")).getText().trim();
			
			if(X.contains("From"))
			{
				X = X.replace("From", "").trim();
			}
			
			String MIN = X.split("-")[0].trim();
			String MAX = X.split("-")[1].trim();
			MIN = MIN.split(" ")[1].trim();
			MAX = MAX.split(" ")[1].trim();
			
			Collections.sort(Original);
			int size = Original.size();
			
			ReportPrinter.append("<span><center><p class='Hedding0'>Filters : Price Slider Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td> Price slider minimum value </td>"
			+ "<td>"+Original.get(0)+"</td>");
			if( Original.get(0) ==  Double.parseDouble(MIN) ) 
			{
				ReportPrinter.append("<td>"+MIN+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+MIN+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td> Price slider maximum value </td>"
			+ "<td>"+Original.get(size-1)+"</td>");
			if( Original.get(size-1) ==  Double.parseDouble(MAX) ) 
			{
				ReportPrinter.append("<td>"+MAX+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+MAX+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
		} catch (Exception e) {
			
		}
		ReportPrinter.append("</table>");
		ReportPrinter.append("<br><br><br>");
		
		new Select(driver.findElement(By.id(PropertyMap.get("ResultPg_FlightSortOptions_DropDwn_id")))).selectByVisibleText("Price - Ascending");
		int clickpattern = 1;
		((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
		
		
		//LAYOVER VALIDATION
		try
		{
			ArrayList<Integer>	OriginalLayOver	= new ArrayList<Integer>();
			
			try {
				for(int y = 0; y<ORIGINAL.size(); y++)
				{
					int q = Integer.valueOf( CommonValidator.formatTimeToCommon(ORIGINAL.get(y).getOBLayover()).replace(":", "") ) ;
					if(twoway)
					{
						int r = Integer.valueOf( CommonValidator.formatTimeToCommon(ORIGINAL.get(y).getIBLayover()).replace(":", "") ) ;
						OriginalLayOver.add(r);
					}
					OriginalLayOver.add(q);
					
				}
			} catch (Exception e) {
				
			}
			
			String	X			= driver.findElement(By.id("layover_text_WJ_17")).getText().trim();
			String	MINLO		= X.split("-")[0].trim();
			String	MAXLO		= X.split("-")[1].trim();
			
			String strmin =  CommonValidator.formatTimeToCommon(MINLO).replace(":", "") ;
			String strmax =  CommonValidator.formatTimeToCommon(MAXLO).replace(":", "") ;
			
			Collections.sort(OriginalLayOver);
			int size = OriginalLayOver.size();
			
			ReportPrinter.append("<span><center><p class='Hedding0'>Filters : Layover Slider Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td> Layover slider minimum value </td>"
			+ "<td>"+OriginalLayOver.get(0)+"</td>");
			if( String.valueOf(OriginalLayOver.get(0)).contains(strmin) || strmin.contains(String.valueOf(OriginalLayOver.get(0))) ) 
			{
				ReportPrinter.append("<td>"+strmin+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				System.out.println(strmin);
			}
			else
			{
				ReportPrinter.append("<td>"+strmin+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td> Layover slider maximum value </td>"
			+ "<td>"+OriginalLayOver.get(size-1)+"</td>");
			if( String.valueOf(OriginalLayOver.get(size-1)).contains(strmax) || strmax.contains(String.valueOf(OriginalLayOver.get(size-1))) ) 
			{
				ReportPrinter.append("<td>"+strmax+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				System.out.println(strmax);
			}
			else
			{
				ReportPrinter.append("<td>"+strmax+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
		} catch (Exception e) {
			
		}
		ReportPrinter.append("</table>");
		ReportPrinter.append("<br><br><br>");
		
		new Select(driver.findElement(By.id(PropertyMap.get("ResultPg_FlightSortOptions_DropDwn_id")))).selectByVisibleText("Price - Ascending");
		clickpattern = 1;
		((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
		
		
		
		//FLIGHTLEG SLIDER VALIDATION
		try
		{
			ArrayList<Integer>	OriginalLeg1	= new ArrayList<Integer>();
			ArrayList<Integer>	OriginalLeg2	= new ArrayList<Integer>();
					
			try
			{
				for(int y = 0; y<ORIGINAL.size(); y++)
				{
					int q = Integer.valueOf( CommonValidator.formatTimeToCommon(ORIGINAL.get(y).getOBDuration()).replace(":", "") );
					OriginalLeg1.add(q);
					
					if(twoway)
					{
						int r = Integer.valueOf( CommonValidator.formatTimeToCommon(ORIGINAL.get(y).getIBDuration()).replace(":", "") );
						OriginalLeg2.add(r);
					}		
				}
			}
			catch (Exception e)
			{
						
			}
					
			String	X1		= driver.findElement(By.id("flight_leg1_text_WJ_18")).getText().trim();
			String	MINLO1	= X1.split("-")[0].trim();
			String	MAXLO1	= X1.split("-")[1].trim();
					
			String	strmin1	=  CommonValidator.formatTimeToCommon(MINLO1).replace(":", "") ;
			String	strmax1	=  CommonValidator.formatTimeToCommon(MAXLO1).replace(":", "") ;
					
			Collections.sort(OriginalLeg1);
			int size = OriginalLeg1.size();
					
			ReportPrinter.append("<span><center><p class='Hedding0'>Filters : Flightleg  Slider Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
					
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td> Flightleg 1 slider minimum value </td>"
			+ "<td>"+OriginalLeg1.get(0)+"</td>");
			if( String.valueOf(OriginalLeg1.get(0)).contains(strmin1) || strmin1.contains(String.valueOf(OriginalLeg1.get(0))) ) 
			{
				ReportPrinter.append("<td>"+strmin1+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+strmin1+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
					
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td> Flightleg 1 slider maximum value </td>"
			+ "<td>"+OriginalLeg1.get(size-1)+"</td>");
			if( String.valueOf(OriginalLeg1.get(size-1)).contains(strmax1) || strmax1.contains(String.valueOf(OriginalLeg1.get(size-1))) ) 
			{
				ReportPrinter.append("<td>"+strmax1+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+strmax1+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
			///////////LEG 2
			size = OriginalLeg2.size();
			if(twoway)
			{
				try
				{
					String	X2		= driver.findElement(By.id("flight_leg2_text_WJ_18")).getText().trim();
					String	MINLO2	= X2.split("-")[0].trim();
					String	MAXLO2	= X2.split("-")[1].trim();
							
					String	strmin2	=  CommonValidator.formatTimeToCommon(MINLO2).replace(":", "") ;
					String	strmax2	=  CommonValidator.formatTimeToCommon(MAXLO2).replace(":", "") ;
							
					Collections.sort(OriginalLeg2);
					//int size = Original.size();
							
					ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
					+ "<td> Flightleg 2 slider minimum value </td>"
					+ "<td>"+OriginalLeg2.get(0)+"</td>");
					if( String.valueOf(OriginalLeg2.get(0)).contains(strmin2) || strmin2.contains(String.valueOf(OriginalLeg2.get(0))) ) 
					{
						ReportPrinter.append("<td>"+strmin2+"</td>"
						+ "<td class='Passed'>PASS</td></tr>");
						Reportcount++;
					}
					else
					{
						ReportPrinter.append("<td>"+strmin2+"</td>"
						+ "<td class='Failed'>Fail</td></tr>");
						Reportcount++;
					}
							
					ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
					+ "<td> Flightleg 2 slider maximum value </td>"
					+ "<td>"+OriginalLeg2.get(size-1)+"</td>");
					if( String.valueOf(OriginalLeg2.get(size-1)).contains(strmax2) || strmax2.contains(String.valueOf(OriginalLeg2.get(size-1))) ) 
					{
						ReportPrinter.append("<td>"+strmax2+"</td>"
						+ "<td class='Passed'>PASS</td></tr>");
						Reportcount++;
					}
					else
					{
						ReportPrinter.append("<td>"+strmax2+"</td>"
						+ "<td class='Failed'>Fail</td></tr>");
						Reportcount++;
					}
				}
				catch (Exception e)
				{
					
				}//try
				
			}//twoway
		}
		catch (Exception e)
		{
					
		}
		
		ReportPrinter.append("</table>");
		ReportPrinter.append("<br><br><br>");
				
		new Select(driver.findElement(By.id(PropertyMap.get("ResultPg_FlightSortOptions_DropDwn_id")))).selectByVisibleText("Price - Ascending");
		clickpattern = 1;
		((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
		
		System.out.println("VALIDATING FILTERS END");
		System.out.println("====================================");
		
		return status;
	}

	
}
