package Flight.Cancellation;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
/*import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;*/

//import Flight.Classes.CancellationBreakdown;
import system.classes.*;
import system.enumtypes.*;

import com.common.Validators.CommonValidator;

public class CancellationScreen
{
	WebDriver	driver	= null;
	boolean		twoway	= true;
	
	//F3710C101014
	
	//INTERFACE
	String				productType		= "";
	String				RezRefNo		= "";
	String				Cancellation	= "";//Should be "Yes"
	
	//FLIGHT BOOKING SUMMARY
	String				Origin			= "";
	String				Destination		= "";
	String				adultCount		= "";
	String				seniorCount		= "";
	String				childCount		= "";
	String				infantCount		= "";
	String				dateDeparture	= "";
	String				dateReturn		= "";
	String				tripType		= "";
	String				seatClass		= "";
	String				ReservationNo	= "";
	String				supplConfNo		= "";
	String				bookingChannel	= "";
	String				bookingDate		= "";
	
	//FLIGHT DETAILS
	ArrayList<Flight>	outbound		= new ArrayList<Flight>();
	ArrayList<Flight>	inbound			= new ArrayList<Flight>();
	
	//FLIGHT OCCUPANCY DETAILS
	ArrayList<Traveler> Adults			= new ArrayList<Traveler>();
	ArrayList<Traveler> Child			= new ArrayList<Traveler>();
	ArrayList<Traveler> Infant			= new ArrayList<Traveler>();
	
	//FLIGHT RATE DETAILS
	String				totalCurrecy	= "";
	String				subTotal		= "";
	String				taxes			= "";
	String				totalPayable	= "";
	
	//FLIGHT CANCELLATION CHARGES
	String		cancellationCurrency	= "";
	String		supplierCancelChrg		= "";
	String		balanceCancelChrg		= "";
	String		additionalCancelChrg	= "";
	String		totalChrg				= "";
	
	//SEARCHING PARAMETERS
	String				searchResNo		= "F3710C101014";
	
	//INPROCESS VARIABLES
	String				strdate			= "";//Date value of report
	boolean				isCancelled		= false;
	boolean				notFound		= true;
	
	CancellationBreakdown	cancellationBreakdown	= new CancellationBreakdown();
	HashMap<String, String>	Propertymap				= new HashMap<String, String>();
	String					strCancellingDate		= "";
	String 					cancellationMessage		= "";
	
	
	public String getCancellationMessage() {
		return cancellationMessage;
	}

	public void setCancellationMessage(String cancellationMessage) {
		this.cancellationMessage = cancellationMessage;
	}

	public boolean isNotFound() {
		return notFound;
	}

	public CancellationScreen(HashMap<String, String> propertymap)
	{
		Propertymap = propertymap;
	}
	
	public void getCancellationScreen(WebDriver	driver, String ResNo, XMLPriceItinerary XMLSelectFlight, SearchObject Sobj) throws ParseException, WebDriverException, IOException
	{
		if(Sobj.getTriptype().contains("One Way Trip"))
		{
			twoway = false;
		}
		searchResNo = ResNo.trim();
		/*driver = new FirefoxDriver(new FirefoxProfile(new File("C:\\Users\\Sanoj\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\ujb8cwm6.default-1406265149085")));
		driver.get("http://dev3.rezg.net/rezbase_v3/admin/common/LoginPage.do");
		driver.findElement(By.id("user_id")).sendKeys("sanojC");
		driver.findElement(By.id("password")).sendKeys("123456");
		driver.findElement(By.id("loginbutton")).click();*/
		
		driver.get(Propertymap.get("Portal.Url").concat("/operations/reservation/ModificationCancellationInfoPage.do?module=operations"));
		
		driver.findElement(By.id("reservationId")).clear();
		driver.findElement(By.id("reservationId")).sendKeys(searchResNo.trim());
		driver.findElement(By.id("reservationId_lkup")).click();
		String resNo = "";
		
		try
		{
			driver.switchTo().defaultContent();
			driver.switchTo().frame("lookup");
			WebElement				x = driver.findElement(By.id("lookupDataArea"));
			ArrayList<WebElement>	y = new ArrayList<WebElement>(x.findElements(By.tagName("td")));
			int f = y.size();
			int c = 0;
			while(notFound)
			{
				try
				{
					resNo = driver.findElement(By.className("rezgLook"+c+"")).getText().trim();
					//System.out.println();
					if(searchResNo.equalsIgnoreCase(resNo))
					{
						notFound = false;
						driver.findElement(By.className("rezgLook"+c+"")).click();
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
					TakesScreenshot screen = (TakesScreenshot)driver;
					FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Cancellation Report Lookup.jpg"));
					if(f<=c+1)
					{
						((JavascriptExecutor)driver).executeScript("parent.hideLookup();");
						break;
					}
				}
				c++;
			}
			
			driver.switchTo().defaultContent();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (!notFound)
		{
			ArrayList<WebElement> elements		= new ArrayList<WebElement>(driver.findElements(By.className("grdtbl")));
			ArrayList<WebElement> TRelements	= new ArrayList<WebElement>(elements.get(0).findElements(By.tagName("tr")));
			
			this.productType	= TRelements.get(2).findElement(By.id("producttype_air")).getText().trim();
			this.RezRefNo		= TRelements.get(2).findElement(By.id("reservationno_air")).getText().trim();
			this.Cancellation	= TRelements.get(2).findElement(By.id("cancellationload_air")).getText().trim();
			
			if(Cancellation.equalsIgnoreCase("yes"))
			{
				((JavascriptExecutor) driver).executeScript("javascript:loadaircancellaitondata();");
				this.Origin			= driver.findElement(By.id("roigin")).getText().split(":")[1].trim();
				this.Destination	= driver.findElement(By.id("destination")).getText().split(":")[1].trim();
				this.adultCount		= driver.findElement(By.id("noofadults")).getText().split(":")[1].trim();
				this.seniorCount	= driver.findElement(By.id("noofsnr")).getText().split(":")[1].trim();
				this.childCount		= driver.findElement(By.id("noofchld")).getText().split(":")[1].trim();
				this.infantCount	= driver.findElement(By.id("noofinfants")).getText().split(":")[1].trim();
				
				try {
				strdate				= driver.findElement(By.id("depdate")).getText().split(":")[1].trim();
				/*date				= sdfin.parse(strdate);
				strdate				= sdfto.format(date);*/
				this.dateDeparture	= CommonValidator.formatDateToCommon(strdate, "dd-MMM-yyyy");
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
				strdate				= driver.findElement(By.id("returndate")).getText().split(":")[1].trim();
				/*date				= sdfin.parse(strdate);
				strdate				= sdfto.format(date);*/
				this.dateReturn		= CommonValidator.formatDateToCommon(strdate, "dd-MMM-yyyy");;
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				
				this.tripType		= driver.findElement(By.id("triptype")).getText().split(":")[1].trim();
				this.seatClass		= driver.findElement(By.id("seatclass")).getText().split(":")[1].trim();
				this.ReservationNo	= driver.findElement(By.id("reservationno")).getText().split(":")[1].trim();
				this.supplConfNo	= driver.findElement(By.id("supconfno")).getText().split(":")[1].trim();
				this.bookingChannel	= driver.findElement(By.id("bookingchannel")).getText().split(":")[1].trim();
				
				strdate				= driver.findElement(By.id("bookingdate")).getText().split(":")[1].trim();
				/*date				= sdfin.parse(strdate);
				strdate				= sdfto.format(date);*/
				this.bookingDate	= CommonValidator.formatDateToCommon(strdate, "dd-MMM-yyyy");;
				
				//Out bound and In bound flights
				ArrayList<WebElement> outlist	= new ArrayList<WebElement>(driver.findElements(By.id("outfligthdata")));
				for(int y=0; y<outlist.size(); y++)
				{
					String		general	= "";
					Flight		flight	= new Flight();
					WebElement	element	= outlist.get(y);
					
					general	= element.findElement(By.id("outflightno")).getText().trim();
					flight.setFlightNo(general);
					
					general	= element.findElement(By.id("outairline")).getText().trim();
					flight.setMarketingAirline(general);
					flight.setMarketingAirline_Loc_Code(general);
					
					try {
					general	= element.findElement(By.id("outseptime")).getText().trim().split(" ")[0];
					general	= CommonValidator.formatDateToCommon(general, "dd-MMM-yyyy");
					flight.setDepartureDate(general);
					
					general	= element.findElement(By.id("outseptime")).getText().trim().split(" ")[1];
					general = CommonValidator.formatTimeToCommon(general);
					flight.setDepartureTime(general);
					} catch (Exception e) {
						TakesScreenshot screen = (TakesScreenshot)driver;
						FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Outbound Details Cancellation Report.jpg"));
					}
					
					general	= element.findElement(By.id("outdeplocation")).getText().trim();
					flight.setDepartureLocationCode(general);
					flight.setDeparture_port(general);
					
					general = element.findElement(By.id("outarrlocation")).getText().trim();
					flight.setArrivalLocationCode(general);
					flight.setArrival_port(general);
					
					this.outbound.add(flight);
				}
				
				ArrayList<WebElement> inlist	= null;
				if(twoway)
				{
					inlist	= new ArrayList<WebElement>(driver.findElements(By.id("inflightdata")));
					
					for(int y=0; y<inlist.size(); y++)
					{
						String		general	= "";
						Flight		flight	= new Flight();
						WebElement	element	= inlist.get(y);
						
						general	= element.findElement(By.id("inflightno")).getText().trim();
						flight.setFlightNo(general);
						
						general	= element.findElement(By.id("inairlines")).getText().trim();
						flight.setMarketingAirline(general);
						flight.setMarketingAirline_Loc_Code(general);
						
						try {
						general	= element.findElement(By.id("indeptime")).getText().trim().split(" ")[0];
						general	= CommonValidator.formatDateToCommon(general, "dd-MMM-yyyy");
						flight.setDepartureDate(general);
						
						general	= element.findElement(By.id("indeptime")).getText().trim().split(" ")[1];
						general = CommonValidator.formatTimeToCommon(general);
						flight.setDepartureTime(general);
						} catch (Exception e) {
							TakesScreenshot screen = (TakesScreenshot)driver;
							FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Outbound Details Cancellation Report.jpg"));
						}
						
						general	= element.findElement(By.id("indeploaction")).getText().trim();
						flight.setDepartureLocationCode(general);
						flight.setDeparture_port(general);
						
						general = element.findElement(By.id("inarrlocation")).getText().trim();
						flight.setArrivalLocationCode(general);
						flight.setArrival_port(general);
						
						this.inbound.add(flight);
					}
				}
				
				//Passengers
				ArrayList<WebElement> adults = new ArrayList<WebElement>(driver.findElements(By.id("adultdetails")));
				for(int y=1; y<=adults.size(); y++)
				{
					Traveler	adult	= new Traveler();
					WebElement	ele		= adults.get(y-1);
					String		general	= "";
					general	= ele.findElement(By.id("adulttitle_"+y+">")).getText().trim();
					adult.setNamePrefixTitle(general);
					general	= ele.findElement(By.id("adultlastname_"+y+"")).getText().trim();
					adult.setSurname(general);
					general	= ele.findElement(By.id("adultfname_"+y+"")).getText().trim();
					adult.setGivenName(general);
					this.Adults.add(adult);
				}
				
				
				try {
					ArrayList<WebElement> children = new ArrayList<WebElement>(driver.findElements(By.id("childedetails")));
					
					for(int y=1; y<=children.size(); y++)
					{
						Traveler	child	= new Traveler();
						WebElement	ele		= children.get(y-1);
						String		general	= "";
						
						general	= ele.findElement(By.id("childtitle_"+y+"")).getText().trim();
						child.setNamePrefixTitle(general);
						general	= ele.findElement(By.id("childlname_"+y+"")).getText().trim();
						child.setSurname(general);
						general	= ele.findElement(By.id("childfname_"+y+"")).getText().trim();
						child.setGivenName(general);
						this.Child.add(child);
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				try {
					ArrayList<WebElement> infants = new ArrayList<WebElement>(driver.findElements(By.id("infantdetails")));
					
					for(int y=1; y<=infants.size(); y++)
					{
						Traveler	infant	= new Traveler();
						WebElement	ele		= infants.get(y-1);
						String		general	= "";
						
						general	= ele.findElement(By.id("infanttitle_"+y+"")).getText().trim();
						infant.setNamePrefixTitle(general);
						general	= ele.findElement(By.id("infantlname_"+y+"")).getText().trim();
						infant.setSurname(general);
						general	= ele.findElement(By.id("infantfname_"+y+"")).getText().trim();
						infant.setGivenName(general);
						this.Infant.add(infant);
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}

				
				//Flight rate details
				this.totalCurrecy			= driver.findElement(By.id("totalrate")).getText().split("[()]")[1].trim();
				this.subTotal				= CommonValidator.formatRemoveComma(driver.findElement(By.id("subtotal")).getText().trim());;
				this.taxes					= CommonValidator.formatRemoveComma(driver.findElement(By.id("totaltax")).getText().trim());;
				this.totalPayable			= CommonValidator.formatRemoveComma(driver.findElement(By.id("totalpayable")).getText().trim());;
				
				
				//CANCELLATION CHARGES

				//SETTING CANCELLATION CHARGES
				this.setCancellationCharges(XMLSelectFlight, Sobj);
				String h = "0";
				
				h = CommonValidator.formatRemoveComma(driver.findElement(By.id("fligthCancellationFee")).getText().trim());
				if(h.equals(""))
				{
					h = "0";
					driver.findElement(By.id("fligthCancellationFee")).clear();
					driver.findElement(By.id("fligthCancellationFee")).sendKeys(this.supplierCancelChrg);
				}
				
				h = CommonValidator.formatRemoveComma(driver.findElement(By.id("customerCancellationFee")).getText().trim());
				if(h.equals(""))
				{
					h = "0";
					driver.findElement(By.id("customerCancellationFee")).clear();
					driver.findElement(By.id("customerCancellationFee")).sendKeys(this.balanceCancelChrg);	
				}
				
				h = CommonValidator.formatRemoveComma(driver.findElement(By.id("additionalcancellationfee")).getText().trim());
				if(h.equals(""))
				{
					h = "0";
					driver.findElement(By.id("additionalcancellationfee")).clear();
					driver.findElement(By.id("additionalcancellationfee")).sendKeys(this.additionalCancelChrg);	
				}
				
				h = CommonValidator.formatRemoveComma(driver.findElement(By.id("totCancellationFee")).getText().trim());
				if(h.equals(""))
				{
					h = "0";
					driver.findElement(By.id("totCancellationFee")).clear();
					driver.findElement(By.id("totCancellationFee")).sendKeys(this.totalChrg);
				}
				
				driver.findElement(By.id("snd_hotbk_req_Y")).click();
			}
			
			isCancelled = doCancel(driver);
			
		}//End notFound
		
	}//End method	
	
	/*public static void main(ing[] args) throws WebDriverException, ParseException, IOException {
		CancellationScreen a = new CancellationScreen();
		
		a.getCancellationReport();
	}*/
	
	public boolean doCancel(WebDriver driver)
	{
		//hotelcancellationfee, additionalcancellationfee , totCancellationFee 
		
		boolean t = false;
		
		try {
			driver.switchTo().defaultContent();
			((JavascriptExecutor) driver).executeScript("javascript: submitForm();;");
			//t = true;
			if(driver.findElement(By.id("dialogMsgBox_msgBoxTitle")).isDisplayed())
			{
				
				cancellationMessage = driver.findElement(By.id("dialogMsgText")).getText();
				if(cancellationMessage.contains("Successfully cancelled"))
				{
					t = true;
				}
				((JavascriptExecutor) driver).executeScript("javascript:closeDialogMsg(dialogMsgBox);");
			}
		} catch (Exception e) {
		}
		try {
			if(driver.findElement(By.id("MainMsgBox")).isDisplayed())
			{
				cancellationMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
				((JavascriptExecutor) driver).executeScript("javascript:closeMsgBox('MainMsgBox');");
			}
		} catch (Exception e) {
			
		}
		
		
		
		
		return t;
	}
	
	public boolean setCancellationCharges(XMLPriceItinerary XMLSelectFlight, SearchObject Sobj)
	{
		strCancellingDate = CommonValidator.getSystemDateInCommonFormat();
		boolean done = false;
		try {
			
		
		String	strCancellingDate			= "";	
		strCancellingDate					= getStrCancellingDate().trim();		
		String	strSupplierCancellationDate	= "";	
		strSupplierCancellationDate			= XMLSelectFlight.getTicketTimeLimit().trim().split("T")[0];		
		String	strPortalCancellationDate	= "";	
		strPortalCancellationDate			= CommonValidator.getCancellationDate(strSupplierCancellationDate, "yyyy-MM-dd");
				
		Date	dtCancellingDate			= null;	
		dtCancellingDate					= CommonValidator.getDateInCommonDateFormat(strCancellingDate, "yyyy-MM-dd");
		Date	dtPortalCancellationDate	= null;	
		dtPortalCancellationDate			= CommonValidator.getDateInCommonDateFormat(strPortalCancellationDate, "yyyy-MM-dd");
		Date	dtSupplierCancellationDate	= null;	
		dtSupplierCancellationDate			= CommonValidator.getDateInCommonDateFormat(strSupplierCancellationDate, "yyyy-MM-dd");

		String	sellingCurrency				= "";	
		sellingCurrency						= XMLSelectFlight.getPricinginfo().getNewBasefareCurrencyCode();
		double	profit						= 0;	
		profit								= XMLSelectFlight.getPricinginfo().getProfit();
		double	bookingFee					= 0;	
		bookingFee							= XMLSelectFlight.getPricinginfo().getBookingfee();
		double	baseFare					= 0;	
		baseFare							= Double.valueOf(XMLSelectFlight.getPricinginfo().getNewbase());
		double	creditCardFee				= 0;
		
		if(Sobj.getPaymentMode().equalsIgnoreCase("Pay Online"))
			creditCardFee					= Double.valueOf(XMLSelectFlight.getPricinginfo().getCreditcardfeeinSellCurr());
		
		double	taxes						= 0;	
		taxes								= Double.valueOf(XMLSelectFlight.getPricinginfo().getNewtax());
		double	supplierCancellationFee		= 0;	//supplierCancellationFee		= XMLSelectFlight.getPricinginfo().get;
		
		double	caseExpectedSupCancelChrg	= 0;
		double  caseExpectedBalncCancelChrg	= 0;
		double	caseExpectedAdditCanclChrg	= 0;	
		double	caseExpectedTotalChrg		= 0;	
		double	caseExpectedSupplierGet		= 0;	
		double	caseExpectedPortalGet		= 0;	
		double	caseExpectedCustomerGet		= 0;
		
		System.out.println(dtPortalCancellationDate);
		System.out.println(dtCancellingDate);
		
		if( dtCancellingDate != null && dtPortalCancellationDate != null && dtSupplierCancellationDate!= null /*&& ActualportalCurrencyCode.equals(ExpectedlportalCurrencyCode) */)
		{
			//CASE 1
			if(dtPortalCancellationDate.compareTo(dtCancellingDate) > 0 )
			{
				done = true;
				caseExpectedSupCancelChrg	= 0;
				caseExpectedBalncCancelChrg	= 0;
				caseExpectedAdditCanclChrg	= bookingFee + creditCardFee;
				caseExpectedTotalChrg		= caseExpectedSupCancelChrg + caseExpectedBalncCancelChrg + caseExpectedAdditCanclChrg;
				caseExpectedSupplierGet		= 0;
				caseExpectedPortalGet		= bookingFee;
				caseExpectedCustomerGet		= baseFare + profit + taxes;
				
				cancellationBreakdown.setCancellationScenario(CancellationType.Before_Cancellation);
				cancellationBreakdown.setCurrencyCode(sellingCurrency);
				cancellationBreakdown.setBaseFare(baseFare);
				cancellationBreakdown.setBookingFee(bookingFee);
				cancellationBreakdown.setCancellationFee(supplierCancellationFee);
				cancellationBreakdown.setCreditCardFee(creditCardFee);
				cancellationBreakdown.setProfit(profit);
				cancellationBreakdown.setTax(taxes);
				cancellationBreakdown.setSupplierGet(caseExpectedSupplierGet);
				cancellationBreakdown.setPortalGet(caseExpectedPortalGet);
				cancellationBreakdown.setCustomerGet(caseExpectedCustomerGet);
				this.setCancellationBreakdown(cancellationBreakdown);
				
				this.supplierCancelChrg		= String.valueOf(caseExpectedSupCancelChrg);
				this.balanceCancelChrg		= String.valueOf(caseExpectedBalncCancelChrg);
				this.additionalCancelChrg	= String.valueOf(caseExpectedAdditCanclChrg);
				this.totalChrg				= String.valueOf(caseExpectedTotalChrg);
			}
			//CASE 2
			else if( (dtSupplierCancellationDate.compareTo(dtCancellingDate) >= 0) && (dtPortalCancellationDate.compareTo(dtCancellingDate) <= 0) )
			{
				done = true;
				caseExpectedSupCancelChrg	= 0;
				caseExpectedBalncCancelChrg	= baseFare + taxes + profit;
				caseExpectedAdditCanclChrg	= bookingFee + creditCardFee;
				caseExpectedTotalChrg		= caseExpectedSupCancelChrg + caseExpectedBalncCancelChrg + caseExpectedAdditCanclChrg;
				caseExpectedSupplierGet		= 0;
				caseExpectedPortalGet		= bookingFee  + baseFare + taxes + profit;
				caseExpectedCustomerGet		= 0;
				
				cancellationBreakdown.setCancellationScenario(CancellationType.Within_Cancellation);
				cancellationBreakdown.setCurrencyCode(sellingCurrency);
				cancellationBreakdown.setBaseFare(baseFare);
				cancellationBreakdown.setBookingFee(bookingFee);
				cancellationBreakdown.setCancellationFee(supplierCancellationFee);
				cancellationBreakdown.setCreditCardFee(creditCardFee);
				cancellationBreakdown.setProfit(profit);
				cancellationBreakdown.setTax(taxes);
				cancellationBreakdown.setSupplierGet(caseExpectedSupplierGet);
				cancellationBreakdown.setPortalGet(caseExpectedPortalGet);
				cancellationBreakdown.setCustomerGet(caseExpectedCustomerGet);
				this.setCancellationBreakdown(cancellationBreakdown);
				
				this.supplierCancelChrg		= String.valueOf(caseExpectedSupCancelChrg);
				this.balanceCancelChrg		= String.valueOf(caseExpectedBalncCancelChrg);
				this.additionalCancelChrg	= String.valueOf(caseExpectedAdditCanclChrg);
				this.totalChrg				= String.valueOf(caseExpectedTotalChrg);
			}
			else if(dtCancellingDate.compareTo(dtSupplierCancellationDate) > 0)
			{
				done = true;
				caseExpectedSupCancelChrg	= baseFare + taxes;
				caseExpectedBalncCancelChrg	= profit;
				caseExpectedAdditCanclChrg	= bookingFee + creditCardFee;
				caseExpectedTotalChrg		= caseExpectedSupCancelChrg + caseExpectedBalncCancelChrg + caseExpectedAdditCanclChrg;
				caseExpectedSupplierGet		= baseFare + taxes;
				caseExpectedPortalGet		= bookingFee + profit;
				caseExpectedCustomerGet		= 0;
				
				cancellationBreakdown.setCancellationScenario(CancellationType.Outside_Cancellation);
				cancellationBreakdown.setCurrencyCode(sellingCurrency);
				cancellationBreakdown.setBaseFare(baseFare);
				cancellationBreakdown.setBookingFee(bookingFee);
				cancellationBreakdown.setCancellationFee(supplierCancellationFee);
				cancellationBreakdown.setCreditCardFee(creditCardFee);
				cancellationBreakdown.setProfit(profit);
				cancellationBreakdown.setTax(taxes);
				cancellationBreakdown.setSupplierGet(caseExpectedSupplierGet);
				cancellationBreakdown.setPortalGet(caseExpectedPortalGet);
				cancellationBreakdown.setCustomerGet(caseExpectedCustomerGet);
				this.setCancellationBreakdown(cancellationBreakdown);
				
				this.supplierCancelChrg		= String.valueOf(caseExpectedSupCancelChrg);
				this.balanceCancelChrg		= String.valueOf(caseExpectedBalncCancelChrg);
				this.additionalCancelChrg	= String.valueOf(caseExpectedAdditCanclChrg);
				this.totalChrg				= String.valueOf(caseExpectedTotalChrg);
			}
		}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return done;
	}
	
	public boolean isCancelled() {
		return isCancelled;
	}

	public void setCancelled(boolean isCancelled) {
		this.isCancelled = isCancelled;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public boolean isTwoway() {
		return twoway;
	}

	public String getProductType() {
		return productType;
	}

	public String getRezRefNo() {
		return RezRefNo;
	}

	public String getCancellation() {
		return Cancellation;
	}

	public String getOrigin() {
		return Origin;
	}

	public String getDestination() {
		return Destination;
	}

	public String getAdultCount() {
		return adultCount;
	}

	public String getSeniorCount() {
		return seniorCount;
	}

	public String getChildCount() {
		return childCount;
	}

	public String getInfantCount() {
		return infantCount;
	}

	public String getDateDeparture() {
		return dateDeparture;
	}

	public String getDateReturn() {
		return dateReturn;
	}

	public String getTripType() {
		return tripType;
	}

	public String getSeatClass() {
		return seatClass;
	}

	public String getReservationNo() {
		return ReservationNo;
	}

	public String getSupplConfNo() {
		return supplConfNo;
	}

	public String getBookingChannel() {
		return bookingChannel;
	}

	public String getBookingDate() {
		return bookingDate;
	}

	public ArrayList<Flight> getOutbound() {
		return outbound;
	}

	public ArrayList<Flight> getInbound() {
		return inbound;
	}

	public ArrayList<Traveler> getAdults() {
		return Adults;
	}

	public ArrayList<Traveler> getChild() {
		return Child;
	}

	public ArrayList<Traveler> getInfant() {
		return Infant;
	}

	public String getTotalCurrecy() {
		return totalCurrecy;
	}

	public String getSubTotal() {
		return subTotal;
	}

	public String getTaxes() {
		return taxes;
	}

	public String getTotalPayable() {
		return totalPayable;
	}

	public String getCancellationCurrency() {
		return cancellationCurrency;
	}

	public String getSupplierCancelChrg() {
		return supplierCancelChrg;
	}

	public String getBalanceCancelChrg() {
		return balanceCancelChrg;
	}

	public HashMap<String, String> getPropertymap() {
		return Propertymap;
	}

	public String getAdditionalCancelChrg() {
		return additionalCancelChrg;
	}

	public String getTotalChrg() {
		return totalChrg;
	}

	public String getSearchResNo() {
		return searchResNo;
	}

	public String getStrdate() {
		return strdate;
	}

	public String getStrCancellingDate() {
		return strCancellingDate;
	}
	
	public CancellationBreakdown getCancellationBreakdown() {
		return cancellationBreakdown;
	}

	public void setCancellationBreakdown(CancellationBreakdown cancellationBreakdown) {
		this.cancellationBreakdown = cancellationBreakdown;
	}


}//End class
