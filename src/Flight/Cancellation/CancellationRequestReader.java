package Flight.Cancellation;

import java.util.HashMap;

//import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;

import system.classes.*;
import Flight.InitialLoading.JsoupDoc;
import system.enumtypes.*;

public class CancellationRequestReader
{
	//Logger 							logger				= null;
	private HashMap<String, String> Propertymap 		= null;
	JsoupDoc						getdoc				= null;
	
	public CancellationRequestReader(HashMap<String, String> Propmap)
	{
		Propertymap = Propmap;
		getdoc = new JsoupDoc(Propertymap);
	}
	
	public CancellationRequest RequestReader(XMLLocateType Value, String Val, XMLFileType type)
	{
		Document doc = getdoc.createDoc(Value, Val, type);
		CancellationRequest cancellationRequest = new CancellationRequest();
		
		XMLError error = new XMLError();
		try
		{
			error.setErrorMessage(doc.getElementsByTag("Error").text());
			error.setErrortype(doc.getElementsByTag("Error").attr("Type"));
			error.setErrorcode(doc.getElementsByTag("Error").attr("Code"));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		cancellationRequest.setError(error);
		
		if(cancellationRequest.getError().getErrorMessage().equals(""))
		{
			try {
							
				String general = "";
				
				general = doc.getElementsByTag("OTA_CancelRQ").attr("EchoToken");
				cancellationRequest.setEchoToken(general);
				
				general = doc.getElementsByTag("UniqueID").attr("ID");
				cancellationRequest.setUniqueID(general);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return cancellationRequest;
	}
}
