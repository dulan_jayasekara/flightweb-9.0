package Flight.Cancellation;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
/*import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;*/
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.common.Validators.CommonValidator;

public class CancellationReport
{
	//Report Variables
	private String				cancellationNo			= "";
	private String				booking_No				= "";
	private String				product_Type			= "";
	private String				booking_Staus			= "";
	private String				supplier_Name			= "";
	private String				sell_Currency			= "";
	private String				base_Currency			= "";
	
	private String				documentNo				= "";
	private String				booking_Date			= "";
	private String				guestLastFirst_Name		= "";
	private String				booking_Channel			= "";
	private String				supplier_ConfirmNo		= "";
	private String				sell_TotBookingVal		= "";
	private String				base_TotBookingVal		= "";
	
	private String				cancellation_Date		= "";
	private String				customer_Type			= "";
	private String				cancellation_Reason		= "";
	private String				docsIssued				= "";
	private String				supplier_Cancel_No		= "";
	private String				sell_SuppCancelFee		= "";
	private String				base_SuppCancelFee		= "";
	
	private String				cancel_By				= "";
	private String				customer_Name			= "";
	private String				sell_CustCancelFee		= "";
	private String				base_CustCancelFee		= "";
	
	private String				notes					= "";
	private String				sell_TotCancelFee		= "";
	private String				base_TotCancelFee		= "";
	
	//In process variables
	HashMap<String, String>		Propertymap				= new HashMap<String, String>();
	boolean						isReportLoded			= false;
	boolean						isRecordExist			= false;
	WebElement			 		Selectedelement			= null;
	String						ReservationNumber		= "F2963W150714";
	String						strdate					= ""; 			

	
	
	/*public static void main(String[] args) {
		CancellationReport a = new CancellationReport();
		a.loadReport(" F2963W150714".trim());
	}*/
	
	public CancellationReport(HashMap<String, String> Propymap)
	{
		Propertymap = Propymap;
	}
	
	public void loadReport(WebDriver driver, String ReservationNo)
	{
		WebDriverWait wait		= new WebDriverWait(driver, 20);
		//COMENT FROM HERE------TO
		/*WebDriver driver		= new FirefoxDriver(new FirefoxProfile(new File("C:\\Users\\Sanoj\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\ujb8cwm6.default-1406265149085")));
		
		driver.get("http://dev3.rezg.net/rezbase_v3/admin/common/LoginPage.do");
		driver.findElement(By.id("user_id")).sendKeys("sanojC");
		driver.findElement(By.id("password")).sendKeys("123456");
		driver.findElement(By.id("loginbutton")).click();*/
		
		driver.get(Propertymap.get("Portal.Url").concat("/reports/operational/mainReport.do?reportId=1&reportName=Cancellation%20Report"));
		driver.switchTo().defaultContent();
		
		//SEARCH BY RESERVATION NO
		driver.switchTo().frame("reportIframe");
		driver.getPageSource();
		driver.findElement(By.id("searchby_reservationno")).click();
		driver.findElement(By.id("label_td_searchby_reservationno")).click();	
		driver.findElement(By.id("searchby_reservationno")).click();
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("reservationno")));
		driver.findElement(By.id("reservationno")).clear();
		driver.findElement(By.id("reservationno")).sendKeys(/*confirmationpage.getReservationNo()*/ReservationNo.trim());
		driver.findElement(By.id("reservationno_lkup")).click();
		String	resNo		= "";
		boolean	notFound	= true;
		try
		{
			driver.switchTo().defaultContent();
			driver.switchTo().frame("lookup");
			WebElement x			= driver.findElement(By.id("lookupDataArea"));
			ArrayList<WebElement> y	= new ArrayList<WebElement>(x.findElements(By.tagName("td")));
			int f = y.size();
			int c = 0;
			while(notFound)
			{
				try
				{
					resNo = driver.findElement(By.className("rowstyle"+c+"")).getText().trim();
					if(ReservationNo.trim().equalsIgnoreCase(resNo))
					{
						notFound = false;
						driver.findElement(By.className("rowstyle"+c+"")).click();
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
					TakesScreenshot screen = (TakesScreenshot)driver;
					FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Reservation Report Lookup.jpg"));
					if(f<=c+1)
					{
						((JavascriptExecutor)driver).executeScript("parent.hideLookup();");
						break;
					}	
				}
				c++;
			}
			
			driver.switchTo().defaultContent();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		int         index      = 0;

		if(!notFound)
		{
			this.isReportLoded = true;
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			((JavascriptExecutor)driver).executeScript("javascript: getReportData('view');");
			try
			{
				forloop:for (int i = 1; i < 12; i++) 
				{
					WebElement  CurrentElement  = driver.findElement(By.id("RowNo_"+i+"_1"));
					ReservationNumber   		= CurrentElement.findElements(By.tagName("td")).get(2).getText().trim();
					
					if(this.ReservationNumber.equalsIgnoreCase(ReservationNo.trim()))
					{
						this.isRecordExist = true;
						index              = i;
					    Selectedelement    = CurrentElement;
						break  forloop;
					}	
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}	
				
			if(this.isRecordExist)
			{	
				this.cancellationNo               	= Selectedelement.findElements(By.tagName("td")).get(1).getText().trim();
				this.booking_No						= Selectedelement.findElements(By.tagName("td")).get(2).getText().trim();
				this.product_Type	                = Selectedelement.findElements(By.tagName("td")).get(3).getText().trim();
				this.booking_Staus					= Selectedelement.findElements(By.tagName("td")).get(4).getText().trim();
				this.supplier_Name					= Selectedelement.findElements(By.tagName("td")).get(5).getText().trim();
				this.sell_Currency					= Selectedelement.findElements(By.tagName("td")).get(6).getText().trim();
				this.base_Currency					= Selectedelement.findElements(By.tagName("td")).get(7).getText().trim();
				
				WebElement element2					= driver.findElement(By.id("RowNo_"+index+"_2"));
				this.documentNo						= element2.findElements(By.tagName("td")).get(0).getText().trim();
				
				strdate								= element2.findElements(By.tagName("td")).get(1).getText().trim();
				this.booking_Date					= CommonValidator.formatDateToCommon(strdate, "dd-MMM-yyyy");
				this.guestLastFirst_Name			= element2.findElements(By.tagName("td")).get(2).getText().trim();
				this.booking_Channel				= element2.findElements(By.tagName("td")).get(3).getText().trim();
				this.supplier_ConfirmNo				= element2.findElements(By.tagName("td")).get(4).getText().trim();
				this.sell_TotBookingVal				= CommonValidator.formatRemoveComma(element2.findElements(By.tagName("td")).get(5).getText().trim());
				this.base_TotBookingVal				= CommonValidator.formatRemoveComma(element2.findElements(By.tagName("td")).get(6).getText().trim());
				
				WebElement element3   		        = driver.findElement(By.id("RowNo_"+index+"_3"));
				strdate								= element3.findElements(By.tagName("td")).get(0).getText().trim();
				this.cancellation_Date				= CommonValidator.formatDateToCommon(strdate, "dd-MMM-yyyy");
				this.customer_Type					= element3.findElements(By.tagName("td")).get(1).getText().trim();
				this.cancellation_Reason			= element3.findElements(By.tagName("td")).get(2).getText().trim();
				this.docsIssued						= element3.findElements(By.tagName("td")).get(3).getText().trim();
				this.supplier_Cancel_No				= element3.findElements(By.tagName("td")).get(4).getText().trim();
				this.sell_SuppCancelFee				= CommonValidator.formatRemoveComma(element3.findElements(By.tagName("td")).get(5).getText().trim());
				this.base_SuppCancelFee				= CommonValidator.formatRemoveComma(element3.findElements(By.tagName("td")).get(6).getText().trim());	
				
				WebElement element4   		        = driver.findElement(By.id("RowNo_"+index+"_4"));
				this.cancel_By						= element4.findElements(By.tagName("td")).get(0).getText().trim();
				this.customer_Name					= element4.findElements(By.tagName("td")).get(1).getText().trim();
				//this.customer_Name					= element4.findElements(By.tagName("td")).get(1).getText().trim();
				this.sell_CustCancelFee				= CommonValidator.formatRemoveComma(element4.findElements(By.tagName("td")).get(5).getText().trim());
				this.base_CustCancelFee				= CommonValidator.formatRemoveComma(element4.findElements(By.tagName("td")).get(6).getText().trim());
				
				WebElement element5   		        = driver.findElement(By.id("RowNo_"+index+"_5"));
				this.notes							= element5.findElements(By.tagName("td")).get(1).getText().trim();
				this.sell_TotCancelFee				= CommonValidator.formatRemoveComma(element5.findElements(By.tagName("td")).get(5).getText().trim());
				this.base_TotCancelFee				= CommonValidator.formatRemoveComma(element5.findElements(By.tagName("td")).get(6).getText().trim());
			}
			else
			{
				this.isReportLoded = false;
			}	
		}
		else 
		{
			this.isReportLoded = false;
		}
	}
	
	public String getCancellationNo() {
		return cancellationNo;
	}

	public String getBooking_No() {
		return booking_No;
	}

	public String getProduct_Type() {
		return product_Type;
	}

	public String getBooking_Staus() {
		return booking_Staus;
	}

	public String getSupplier_Name() {
		return supplier_Name;
	}

	public String getSell_Currency() {
		return sell_Currency;
	}

	public String getBase_Currency() {
		return base_Currency;
	}

	public String getDocumentNo() {
		return documentNo;
	}

	public String getBooking_Date() {
		return booking_Date;
	}

	public String getGuestLastFirst_Name() {
		return guestLastFirst_Name;
	}

	public String getBooking_Channel() {
		return booking_Channel;
	}

	public String getSupplier_ConfirmNo() {
		return supplier_ConfirmNo;
	}

	public String getSell_TotBookingVal() {
		return sell_TotBookingVal;
	}

	public String getBase_TotBookingVal() {
		return base_TotBookingVal;
	}

	public String getCancellation_Date() {
		return cancellation_Date;
	}

	public String getCustomer_Type() {
		return customer_Type;
	}

	public String getCancellation_Reason() {
		return cancellation_Reason;
	}

	public String getDocsIssued() {
		return docsIssued;
	}

	public String getSupplier_Cancel_No() {
		return supplier_Cancel_No;
	}

	public String getSell_SuppCancelFee() {
		return sell_SuppCancelFee;
	}

	public String getBase_SuppCancelFee() {
		return base_SuppCancelFee;
	}

	public String getCancel_By() {
		return cancel_By;
	}

	public String getCustomer_Name() {
		return customer_Name;
	}

	public String getSell_CustCancelFee() {
		return sell_CustCancelFee;
	}

	public String getBase_CustCancelFee() {
		return base_CustCancelFee;
	}

	public String getNotes() {
		return notes;
	}

	public String getSell_TotCancelFee() {
		return sell_TotCancelFee;
	}

	public String getBase_TotCancelFee() {
		return base_TotCancelFee;
	}

	public HashMap<String, String> getPropertymap() {
		return Propertymap;
	}

	public boolean isReportLoded() {
		return isReportLoded;
	}

	public boolean isRecordExist() {
		return isRecordExist;
	}

	public WebElement getSelectedelement() {
		return Selectedelement;
	}

	public String getReservationNumber() {
		return ReservationNumber;
	}

	public String getStrdate() {
		return strdate;
	}
	
}
