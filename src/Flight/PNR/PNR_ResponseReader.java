package Flight.PNR;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import system.classes.*;
import Flight.InitialLoading.JsoupDoc;
import system.enumtypes.*;

public class PNR_ResponseReader 
{
	ArrayList<Traveler> customerslist				= new ArrayList<Traveler>();
	ArrayList<Flight> flightlist					= new ArrayList<Flight>();
	private HashMap<String, String> Propertymap 	= null;
	JsoupDoc getdoc 								= null;
	ArrayList<XMLSpecialService> SpServiceRequests	= new ArrayList<XMLSpecialService>();
	ArrayList<String> remarklist         			= new ArrayList<String>(); 
	ArrayList<XMLSpecialRemark> Spremarklist        = new ArrayList<XMLSpecialRemark>(); 
	ArrayList<String> ticketlist         			= new ArrayList<String>(); 
	
	public PNR_ResponseReader(HashMap<String, String> Propmap)
	{  
		Propertymap = Propmap;
		getdoc = new JsoupDoc(Propertymap);
	}
	
	public PNR_Response ResponseReader(XMLLocateType Value, String Val, XMLFileType type)
	{
		Document doc = getdoc.createDoc(Value, Val, type);
		PNR_Response PNRResobj = new PNR_Response();
		XMLError error = new XMLError();
		
		try
		{
			error.setErrorMessage(doc.getElementsByTag("Error").text());
			error.setErrortype(doc.getElementsByTag("Error").attr("Type"));
			error.setErrorcode(doc.getElementsByTag("Error").attr("Code"));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		PNRResobj.setError(error);
		
		if(PNRResobj.getError().getErrorMessage().equals(""))
		{
			try
			{
				PNRResobj.setItineraryRefType(doc.getElementsByTag("ItineraryRef").attr("Type"));
				PNRResobj.setItineraryRefID_Context(doc.getElementsByTag("ItineraryRef").attr("ID"));
				PNRResobj.setItineraryRefID_Context(doc.getElementsByTag("ItineraryRef").attr("ID_Context"));
				
				Elements customers = doc.getElementsByTag("CustomerInfo");
				Iterator<Element> customersiter = customers.iterator();
				while(customersiter.hasNext())
				{
					Element cusElement = customersiter.next();
				
					Traveler customer = new Traveler();
					customer.setTravelerRefNo(cusElement.getElementsByTag("CustomerInfo").attr("RPH"));
					customer.setPassengertypeCode(cusElement.getElementsByTag("PersonName").attr("NameType"));
					customer.setGivenName(cusElement.getElementsByTag("GivenName").text());
					customer.setSurname(cusElement.getElementsByTag("Surname").text());
					customer.setPhoneNumber(cusElement.getElementsByTag("Telephone").attr("PhoneNumber"));
					customer.setPhoneType(cusElement.getElementsByTag("Telephone").attr("PhoneUseType"));
					customer.setEmail(cusElement.getElementsByTag("Email").text());
					
					Address address = new Address();
					address.setAddressUseType(cusElement.getElementsByTag("Address").attr("UseType"));
					address.setAddressStreetNo(cusElement.getElementsByTag("StreetNmbr").text());
					address.setAddressCity(cusElement.getElementsByTag("CityName").text());
					address.setPostalCode(cusElement.getElementsByTag("PostalCode").text());
					address.setStateProv(cusElement.getElementsByTag("StateProv").text());
					address.setAddressCountry(cusElement.getElementsByTag("CountryName").text());
					
					customer.setAddress(address);
					
					customerslist.add(customer);
				}
			
				PNRResobj.setCustomers(customerslist);
				ArrayList<XMLResvRespItem> itemlist = new ArrayList<XMLResvRespItem>();
				Elements Items = doc.getElementsByTag("Item");
				Iterator<Element> itemiter = Items.iterator();
				
				while(itemiter.hasNext())
				{
					Element itemElement = itemiter.next();
					XMLResvRespItem item = new XMLResvRespItem();
					item.setStatus(itemElement.getElementsByTag("Item").attr("Status"));
					item.setItinerarySeq(itemElement.getElementsByTag("Item").attr("ItinSeqNumber"));
					Flight flight = new Flight();
					
					flight.setDepartureDate(itemElement.getElementsByTag("Air").attr("DepartureDateTime").split("T")[0]);
					flight.setDepartureTime(itemElement.getElementsByTag("Air").attr("DepartureDateTime").split("T")[1]);
					flight.setArrivalDate(itemElement.getElementsByTag("Air").attr("ArrivalDateTime").split("T")[0]);
					flight.setArrivalTime(itemElement.getElementsByTag("Air").attr("ArrivalDateTime").split("T")[1]);	
					item.setRPH(itemElement.getElementsByTag("Air").attr("RPH"));
					flight.setFlightNo(itemElement.getElementsByTag("Air").attr("FlightNumber"));
					item.setDepartureDay(itemElement.getElementsByTag("Air").attr("DepartureDay"));
					item.setE_TicketEligibility(itemElement.getElementsByTag("Air").attr("E_TicketEligibility"));
					
					flight.setDeparture_port(itemElement.getElementsByTag("DepartureAirport").text());
					System.out.println(itemElement.getElementsByTag("DepartureAirport").text());
					System.out.println(itemElement.getElementsByTag("DepartureAirport").attr("LocationCode"));
					flight.setDepartureLocationCode(itemElement.getElementsByTag("DepartureAirport").attr("LocationCode")/*.split(" ")[1]*/);
					flight.setArrival_port(itemElement.getElementsByTag("ArrivalAirport").text());
					flight.setArrivalLocationCode(itemElement.getElementsByTag("ArrivalAirport").attr("LocationCode")/*.split(" ")[1]*/);
					flight.setOperatingAirline(itemElement.getElementsByTag("OperatingAirline").text());
					flight.setMarketingAirline(itemElement.getElementsByTag("MarketingAirline").text());
					flight.setMarketingAirline_Loc_Code(itemElement.getElementsByTag("MarketingAirline").attr("Code"));
				
					item.setFlight(flight);
					
					item.setTPAExt_ConfirmationNumber(itemElement.getElementsByTag("TPA_Extensions").attr("ConfirmationNumber"));
					
					itemlist.add(item);
				}
				PNRResobj.setItems(itemlist);
				PNRResobj.setPricinsource(doc.getElementsByTag("AirFareInfo").attr("PricingSource"));
			
				XMLPriceInfo fare = new XMLPriceInfo();
				fare.setBasefareAmount(doc.getElementsByTag("BaseFare").attr("Amount"));
				fare.setBasefareCurrencyCode(doc.getElementsByTag("BaseFare").attr("CurrencyCode"));
				fare.setBasefareAmountDecimal(doc.getElementsByTag("BaseFare").attr("DecimalPlaces"));
				fare.setEquivFare(doc.getElementsByTag("EquivFare").attr("Amount"));
				fare.setEquivCurrencyCode(doc.getElementsByTag("EquivFare").attr("CurrencyCode"));
				fare.setEquivDecimal(doc.getElementsByTag("EquivFare").attr("DecimalPlaces"));
				fare.setTaxCode(doc.getElementsByTag("Tax").attr("TaxCode"));
				fare.setTaxAmount(doc.getElementsByTag("Tax").attr("Amount"));
				fare.setTaxCurrencyCode(doc.getElementsByTag("Tax").attr("CurrencyCode"));
				fare.setTaxAmountDecimal(doc.getElementsByTag("Tax").attr("DecimalPlaces"));
				fare.setTotalFare(doc.getElementsByTag("TotalFare").attr("Amount"));
				fare.setTotalFareCurrencyCode(doc.getElementsByTag("TotalFare").attr("CurrencyCode"));
				fare.setTotalFareDecimal(doc.getElementsByTag("TotalFare").attr("DecimalPlaces"));
				PNRResobj.setFare(fare);
				
				PNRResobj.setTickettimelimit(doc.getElementsByTag("Ticketing").attr("TicketTimeLimit"));
				PNRResobj.setTickettimelimit(doc.getElementsByTag("Ticketing").attr("TicketType"));
				
				/*Elements farebreakdwnElements = doc.getElementsByTag("PTC_FareBreakdown");
				Iterator<Element> farebrdwniter = farebreakdwnElements.iterator();
				while(farebrdwniter.hasNext())
				{
					
				}*/
				Elements specialServices = doc.getElementsByTag("SpecialServiceRequest");
				Iterator<Element> services = specialServices.iterator();
				
				while(services.hasNext())
				{
					Element ele = services.next();
					XMLSpecialService service = new XMLSpecialService();
					
					service.setSSRCode(ele.getElementsByTag("SpecialServiceRequest ").attr("SSRCode"));
					service.setFlightRefNumber(ele.getElementsByTag("SpecialServiceRequest").attr("FlightRefNumberRPHList"));
					service.setTravelerRefNumber(ele.getElementsByTag("SpecialServiceRequest").attr("TravelerRefNumberRPHList"));
					service.setText(ele.getElementsByTag("Text").text());
					
					SpServiceRequests.add(service);
				}
				PNRResobj.setSpServiceRequests(SpServiceRequests);
				
				Elements remark = doc.getElementsByTag("Remarks");
				Iterator<Element> remarkiter = remark.iterator();
				
				while(remarkiter.hasNext())
				{
					Element remarkele = remarkiter.next();
					remarklist.add(remarkele.getElementsByTag("Remark").text());
				}
				PNRResobj.setRemark(remarklist);
				
				Elements Spremark = doc.getElementsByTag("SpecialRemark");
				Iterator<Element> Spremarkiter = Spremark.iterator();
				while(Spremarkiter.hasNext())
				{
					Element Spremarkele = Spremarkiter.next();
					XMLSpecialRemark specialRemark = new XMLSpecialRemark();
					specialRemark.setRemarkType(Spremarkele.getElementsByTag("SpecialRemark").attr("RemarkType"));
					specialRemark.setTravelerRefNoRPH(Spremarkele.getElementsByTag("TravelerRefNumber").attr("RPH"));
					specialRemark.setFlgithrefNoRPH(Spremarkele.getElementsByTag("FlightRefNumber").attr("RPH"));
					specialRemark.setText(Spremarkele.getElementsByTag("Text").text());
					specialRemark.setRemarkOrigin(Spremarkele.getElementsByTag("RemarkOrigin").text());
					Spremarklist.add(specialRemark);
				}
				PNRResobj.setSpecialRemark(Spremarklist);
				
				Elements issuedTicketsEle = doc.getElementsByTag("TicketingCarrier");
				Iterator<Element> issuTicktsiter = issuedTicketsEle.iterator();
				while(issuTicktsiter.hasNext())
				{
					Element ticketElement = issuTicktsiter.next();
					ticketlist.add(ticketElement.getElementsByTag("TicketingCarrier").text());
				}
				PNRResobj.setIssuedTickets(ticketlist);
				
				PNRResobj.setUpdatedCreatedate(doc.getElementsByTag("UpdatedBy").attr("CreateDateTime").split("T")[0]);
				PNRResobj.setUpdatedCreatedTime(doc.getElementsByTag("UpdatedBy").attr("CreateDateTime").split("T")[1]);
				
				XMLPaymentDetails travelcost = new XMLPaymentDetails();
				travelcost.setFormofpayRPH(doc.getElementsByTag("FormOfPayment").attr("RPH"));
				travelcost.setDirectBillID(doc.getElementsByTag("DirectBill").attr("DirectBill_ID"));
				
				PNRResobj.setAgencyCommission(doc.getElementsByTag("Commission").attr("Percent"));
			
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
		}
		

		return PNRResobj;
			
	}
}
