package Flight.eTicket;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.jsoup.nodes.Document;

import system.classes.*;
import Flight.InitialLoading.JsoupDoc;
import system.enumtypes.*;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class EticketResponseReader 
{
	File input                              	= null;
	private HashMap<String, String> Propertymap = null;
	//Logger logger                           	= null;
	JsoupDoc getdoc 							= null;
	
	
	public EticketResponseReader(HashMap<String, String> Propmap)
	{  
		Propertymap = Propmap;
		getdoc = new JsoupDoc(Propertymap);
	}
	
	public EticketResponse ResponseReader(XMLLocateType Value, String Val, XMLFileType type)
	{
		Document doc = getdoc.createDoc(Value, Val, type);
		EticketResponse eticketres = new EticketResponse();
		ArrayList<Eticket> eticketList = new ArrayList<Eticket>();
		
		XMLError error = new XMLError();
		try
		{
			error.setErrorMessage(doc.getElementsByTag("Error").text());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		eticketres.setError(error);
		if(eticketres.getError().getErrorMessage().equals(""))
		{
			try
			{
				eticketres.setUniqueID(doc.getElementsByTag("UniqueID").attr("ID"));
				eticketres.setTicketingControlType(doc.getElementsByTag("TicketingControl").attr("Type"));
				
				Elements nodes = doc.getElementsByTag("Ticket");
				Iterator<Element> nodesiter = nodes.iterator();
				
				while(nodesiter.hasNext())
				{
					Element node = nodesiter.next();
					Eticket eticket = new Eticket();
					eticket.setType(node.getElementsByTag("Ticket").attr("Type"));
					eticket.setNumber(node.getElementsByTag("Ticket").attr("Number"));
					eticketres.setTicketType(node.getElementsByTag("Ticket").attr("Number"));
					eticketList.add(eticket);
				}
				eticketres.setEticketList(eticketList);
				
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}

		return eticketres;
	}
}
