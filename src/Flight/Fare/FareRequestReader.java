/*Sanoj*/
package Flight.Fare;

import Flight.InitialLoading.JsoupDoc;
import system.enumtypes.*;

//import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
//import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


public class FareRequestReader 
{
	//Logger 							logger				= null;
	private HashMap<String, String> Propertymap 		= null;
	//File							input				= null;
	Map<Integer, ArrayList<String>> OriginDestination 	= null;
	JsoupDoc getdoc = null;
	
	public FareRequestReader(HashMap<String, String> Propmap)
	{
		Propertymap = Propmap;
		getdoc = new JsoupDoc(Propertymap);

	}
	
	public FareRequest RequestReader(XMLLocateType Value, String Val, XMLFileType type)
	{
		System.out.println("================================");
		System.out.println("FARE REQUEST READING STARTED..!!");
		
		Document doc = getdoc.createDoc(Value, Val, type);
		FareRequest sabresearchReq = new FareRequest();
		
		try
		{
			Thread.sleep(3000);
			sabresearchReq.setISOCurrency(doc.getElementsByTag("Source").attr("ISOCurrency"));
			sabresearchReq.setProvider(doc.getElementsByTag("Name").text());
			
			Elements nodes = doc.getElementsByTag("OriginDestinationInformation");
			
			Iterator<Element> iterator = nodes.iterator();
			
			String originDate = "";
			String origin = "";
			String destination = "";
			ArrayList<String> OriginDestinationInfo = new ArrayList<String>();
			
			while(iterator.hasNext())
			{
				//System.out.println("Iterator started origin destination");
				//logger.info("Starting Origin Destination departure iteration");
				Element ele = iterator.next();
				originDate = ele.getElementsByTag("DepartureDateTime").text();
				origin = ele.getElementsByTag("OriginLocation").attr("LocationCode");
				destination = ele.getElementsByTag("DestinationLocation").attr("LocationCode");
				
				String all = originDate.concat(",").concat(origin).concat(",").concat(destination);
				OriginDestinationInfo.add(all);
			}
			sabresearchReq.setOriginDestinationInfo(OriginDestinationInfo);
			sabresearchReq.setSeatsRequired(doc.getElementsByTag("SeatsRequested").text());
			sabresearchReq.setFareclass(doc.getElementsByTag("CabinPref").attr("Cabin"));
			sabresearchReq.setPrefFlight(doc.getElementsByTag("VendorPref").attr("Code"));
			sabresearchReq.setPricingSource(doc.getElementsByTag("PriceRequestInformation").attr("PricingSource"));
			
			Elements elements = doc.getElementsByTag("PassengerTypeQuantity");
			
			Iterator<Element> iter = elements.iterator();
			
			HashMap<String, String> AirTravelers = new HashMap<String, String>();
			
			while(iter.hasNext())
			{
				System.out.println("passenger reading started..!!");
				Element ele = iter.next();
				String Code = ele.getElementsByTag("PassengerTypeQuantity").attr("Code");
				String Quantity = ele.getElementsByTag("PassengerTypeQuantity").attr("Quantity");
				AirTravelers.put(Code, Quantity);
			}
			
			sabresearchReq.setAirTravelers(AirTravelers);
			
			sabresearchReq.getAll();
			System.out.println("FARE REQUEST XML READ SUCCESFULLY..!!!");
			System.out.println("=======================================");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return sabresearchReq;
	}


}