package Flight.Reports;


//import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
//import java.util.HashMap;
//import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.FirefoxProfile;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.common.Validators.CommonValidator;

import system.classes.*;


public class BookingConfReportReader
{
	boolean				available			= false;
	
	//TOP CONTACT DETAILS
	String			  	RezgatewayContact	= "";
	
	//RESERVATION CONFIRMATION 
	String			 	BookingReference	= "";
	String			  	BookingNo			= "";
	String			  	ThankYou			= "";
	
	//MY VOUCHER
	String			  	SuppConfimationNo	= "";
	String  		  	BookingNo2			= "";
	
	//FLIGHTS
	ArrayList<Flight> 	Outbound			= new ArrayList<Flight>();
	ArrayList<Flight> 	Inbound				= new ArrayList<Flight>();
	
	RatesperPassenger 	AdultRate			= new RatesperPassenger();
	RatesperPassenger 	ChildRate			= new RatesperPassenger();
	RatesperPassenger 	InfantRate			= new RatesperPassenger();
	
	String            	TotalBeforeTax		= "0";
	String            	Tax					= "0";
	String            	BookingFee			= "0";
	String            	TotalAmount			= "0";
	
	String            	Currency			= "";
	String            	SubTotal			= "0";
	String            	TotalTaxAndOther	= "0";
	
	String			  	TotalBookingValue	= "0";
	String			  	AmountToBePaidNow	= "0";
	String			  	AmountToBePaidForAirLine = "0";
	
	//TRAVELLER/CUSTOMER DETAILS
	Traveler		  	main				= new Traveler();
	
	//FLIGHT PASSENGER DETAILS
	ArrayList<Traveler> Adults				= new ArrayList<Traveler>();
	ArrayList<Traveler> Children			= new ArrayList<Traveler>();
	ArrayList<Traveler> Infants				= new ArrayList<Traveler>();
	
	//PAYMENT DETAILS
	String 				ReferenceNumber		= "";
	String 				MerchantTrackID		= "";
	String 				PaymentID			= "";
	String 				PaymentCurrency		= "";
	String 				PaymentDetailsAmount= "";
	
	//FLIGHT INFORMATION
	String 				FlightInformation	= "";
	String 				ConfirmationDeadline= "";
	
	//CUSTOMER CONTACT DETAILS
	String				CancellationNote	= "";
	String				Tel					= "";
	String				Email				= "";

	HashMap<String, String> Propertymap		= new HashMap<String, String>();
	boolean				twoway				= true;
	
	
	
	
	public BookingConfReportReader(HashMap<String, String> propertymap)
	{
		Propertymap = propertymap;
	}
	
	/*public static void main(String[] args)
	{
		BookingConfReportReader readman = new BookingConfReportReader();
		readman.getReport();
	}*/
	
	public void getReport(WebDriver driver, String ResNo )
	{
		/*FirefoxProfile pro		= new FirefoxProfile(new File("C:\\Users\\Sanoj\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\ujb8cwm6.default-1406265149085"));
		WebDriver      driver	= new FirefoxDriver(pro);*/
		WebDriverWait wait 		= new WebDriverWait(driver, 20);
		//this.Propertymap = propertymap;
		
		try
		{
			//String ReservationNo1 = "F3233W070814";
			//String ReservationNo1 = "F3202C160714";
			String ReservationNo1 = ResNo.trim();

			/*driver.get("http://qav3.rezgateway.com/rezbase_v3/admin/common/LoginPage.do");
			driver.findElement(By.id("user_id")).sendKeys("sanojC");
			driver.findElement(By.id("password")).sendKeys("123456");
			driver.findElement(By.id("loginbutton")).click();*/
			driver.get(Propertymap.get("Portal.Url").concat("/reports/operational/mainReport.do?reportId=33&reportName=Booking%20Confirmation%20Report"));
			//driver.get("http://qav3.rezgateway.com/rezbase_v3/reports/operational/mainReport.do?reportId=33&reportName=Booking%20Confirmation%20Report");
			//driver.get(Propertymap.get("DevPortal.Url").concat("/reports/operational/mainReport.do?reportId=33&reportName=Booking%20Confirmation%20Report"));
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			driver.findElement(By.id("searchby_reservationno")).click();
			
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("reservationno")));
			driver.findElement(By.id("reservationno")).clear();
			driver.findElement(By.id("reservationno")).sendKeys(ReservationNo1);
			driver.findElement(By.id("reservationno_lkup")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("lookup");
			driver.findElement(By.className("rowstyle0")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			
			try
			{
				((JavascriptExecutor)driver).executeScript("javascript: getReportData('view');");
				//driver.findElement(By.xpath("/html/body/span/table/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
			}
			catch(Exception e)
			{
				
			}
										 
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			driver.findElement(By.partialLinkText(ReservationNo1)).click();
			driver.switchTo().frame("Myemailformat");
			
			ArrayList<WebElement> TRs		= new ArrayList<WebElement>(driver.findElements(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr")));
			
			//TR tag 1
			//ArrayList<WebElement> TR0TDs	= new ArrayList<WebElement>(TRs.get(0).findElements(By.tagName("td")));
			//String TR0Content				= TR0TDs.get(5).getText();
			//TR tag 3
			BookingReference	= TRs.get(2).getText().split(":")[1].trim();
			BookingNo			= TRs.get(3).getText().split(":")[1].trim();
			ThankYou			= TRs.get(4).getText().concat(" ").concat(TRs.get(5).getText());
			
			String TR8Content	= TRs.get(8).getText();
			String[] TR8Content1= TR8Content.split("-");
			SuppConfimationNo	= TR8Content1[1].split("]")[0].trim();
			BookingNo2			= TR8Content1[2].split("]")[0].trim();
			available = true;
			ArrayList<WebElement> TRoutbound	= new ArrayList<WebElement>(TRs.get(10).findElements(By.xpath("td/table/tbody/tr")));
			//System.out.println(TRoutbound.size());
			
			int c = 0;
			int outflightcount  = TRoutbound.size()/4;
			if(outflightcount   == 1)
			{
				outflightcount  = 1;
			}
			for(int i=0; i<outflightcount; i++)
			{
				Flight flight   = new Flight();
				String value    = "";
				
				ArrayList<WebElement> TDs = new ArrayList<WebElement>(TRoutbound.get(c).findElements(By.tagName("td")));
				c+=1;
				//departure date
				value = TDs.get(2).getText();
				try {
					value = CommonValidator.formatDateToCommon(value, "dd-MMM-yyyy");
				} catch (Exception e) {
					// TODO: handle exception
				}
				flight.setDepartureDate(value);
				
				//departure port
				value = TDs.get(3).getText();
				flight.setDeparture_port(value);
				
				//flight name
				value = TDs.get(4).getText();
				flight.setMarketingAirline(value);
				
				//booking class
				value = TDs.get(6).getText().split(" ")[1];
				flight.setCabintype(value);
				
				
				TDs = new ArrayList<WebElement>(TRoutbound.get(c).findElements(By.tagName("td")));
				c+=1;
				//Departure time
				value = TDs.get(0).getText();
				try {
					value = CommonValidator.formatTimeToCommon(value);
				} catch (Exception e) {
					// TODO: handle exception
				}
				flight.setDepartureTime(value);
				
				//departure location code
				value = TDs.get(1).getText();
				value = value.split("[()]")[1];
				flight.setDepartureLocationCode(value);
				
				//departure flight no
				value = TDs.get(2).getText();
				value = value.split("[()]")[1].split(" ")[2];
				flight.setFlightNo(value);
				
				//Fare class
				value = TDs.get(4).getText();
				value = value.replace(":", "").trim();
				flight.setFareclass(value);
				
				//ARRIVING FLIGHT
				TDs = new ArrayList<WebElement>(TRoutbound.get(c).findElements(By.tagName("td")));
				c+=1;
				//Arrive date
				value = TDs.get(2).getText();
				try {
					value = CommonValidator.formatDateToCommon(value, "dd-MMM-yyyy");
				} catch (Exception e) {
					// TODO: handle exception
				}
				flight.setArrivalDate(value);
				
				//===================================Arrival airport
				value = TDs.get(3).getText();
				flight.setArrival_port(value);
				
				TDs = new ArrayList<WebElement>(TRoutbound.get(c).findElements(By.tagName("td")));
				c+=1;
				
				//===================================Arrival time
				value = TDs.get(0).getText();
				try {
					value = CommonValidator.formatTimeToCommon(value);
				} catch (Exception e) {
					// TODO: handle exception
				}
				flight.setArrivalTime(value);
				
				//A================================Arrival location code
				value = TDs.get(1).getText();
				flight.setArrivalLocationCode(value.split("[()]")[1]);
				
				Outbound.add(flight);
			}
			
			int continuefrom = 14;
			boolean mark = false;
			ArrayList<WebElement> TR13TDs = new ArrayList<WebElement>(TRs.get(13).findElements(By.tagName("td")));
			String flag	= TR13TDs.get(0).getText();
			if(flag.equalsIgnoreCase("Flight Return Info"))
			{
				mark = true;
				continuefrom = 17;
				
			}
			else
			{
				continuefrom = 14;
			}
				
			//=============================================INBOUND FLIGHT DETAILS==============================================
			if(mark)
			{
				ArrayList<WebElement> TRinbound	= new ArrayList<WebElement>(TRs.get(14).findElements(By.xpath("td/table/tbody/tr")));
				
				int cin = 0;
				int inflightcount  = TRinbound.size()/4;
				if(inflightcount   == 1)
				{
					inflightcount  = 1;
				}
				for(int i=0; i<inflightcount; i++)
				{
					Flight flight   = new Flight();
					String value    = "";
					
					ArrayList<WebElement> TDs = new ArrayList<WebElement>(TRinbound.get(cin).findElements(By.tagName("td")));
					cin+=1;
					//departure date
					value = TDs.get(2).getText();
					try {
						value = CommonValidator.formatDateToCommon(value, "dd-MMM-yyyy");
					} catch (Exception e) {
						// TODO: handle exception
					}
					flight.setDepartureDate(value);
					
					//departure port
					value = TDs.get(3).getText();
					flight.setDeparture_port(value);
					
					//flight name
					value = TDs.get(4).getText();
					flight.setMarketingAirline(value);
					
					//booking class
					value = TDs.get(6).getText().split(" ")[1];
					flight.setCabintype(value);
					
					
					TDs = new ArrayList<WebElement>(TRinbound.get(cin).findElements(By.tagName("td")));
					cin+=1;
					//Departure time
					value = TDs.get(0).getText();
					try {
						value = CommonValidator.formatTimeToCommon(value);
					} catch (Exception e) {
						// TODO: handle exception
					}
					flight.setDepartureTime(value);
					
					//departure location code
					value = TDs.get(1).getText();
					value = value.split("[()]")[1];
					flight.setDepartureLocationCode(value);
					
					//departure flight no
					value = TDs.get(2).getText();
					value = value.split("[()]")[1].split(" ")[2];
					flight.setFlightNo(value);
					
					//Fare class
					value = TDs.get(4).getText();
					value = value.replace(":", "").trim();
					flight.setFareclass(value);
					
					//ARRIVING FLIGHT
					TDs = new ArrayList<WebElement>(TRinbound.get(cin).findElements(By.tagName("td")));
					cin+=1;
					//Arrive date
					value = TDs.get(2).getText();
					try {
						value = CommonValidator.formatDateToCommon(value, "dd-MMM-yyyy");
					} catch (Exception e) {
						// TODO: handle exception
					}
					flight.setArrivalDate(value);
					
					//Arrival airport
					value = TDs.get(3).getText();
					flight.setArrival_port(value);
					
					TDs = new ArrayList<WebElement>(TRinbound.get(cin).findElements(By.tagName("td")));
					cin+=1;
					//arrival time
					value = TDs.get(0).getText();
					try {
						value = CommonValidator.formatTimeToCommon(value);
					} catch (Exception e) {
						// TODO: handle exception
					}
					flight.setArrivalTime(value);
					
					//arrival location code
					value = TDs.get(1).getText();
					flight.setArrivalLocationCode(value.split("[()]")[1]);
					
					Inbound.add(flight);
				}
			}//End If twoway
			
			ArrayList<WebElement> TD = new ArrayList<WebElement>(TRs.get(continuefrom).findElements(By.tagName("td")));
			continuefrom+=1;
			String value = "";
			
			value = TD.get(0).getText();
			AdultRate.setPassengertype(value);
			value = TD.get(1).getText();
			AdultRate.setRateperpsngr(value);
			value = TD.get(2).getText();
			AdultRate.setNoofpassengers(value);
			value = TD.get(7).getText();
			AdultRate.setTotal(value);
			
			//boolean ischildExist = true;//Here if search object has children --> TRUE else --> FALSE
			boolean ischildExist = false;
			if(ischildExist)
			{
				TD = new ArrayList<WebElement>(TRs.get(continuefrom).findElements(By.tagName("td")));
				continuefrom+=1;
				value = TD.get(0).getText();
				ChildRate.setPassengertype(value);
				value = TD.get(1).getText();
				ChildRate.setRateperpsngr(value);
				value = TD.get(2).getText();
				ChildRate.setNoofpassengers(value);
				value = TD.get(7).getText();
				ChildRate.setTotal(value);
			}
			
			//boolean isinfantExist = true;//Here if search object has infants --> TRUE else --> FALSE
			boolean isinfantExist = false;
			if(isinfantExist)
			{
				TD = new ArrayList<WebElement>(TRs.get(continuefrom).findElements(By.tagName("td")));
				continuefrom+=1;
				value = TD.get(0).getText();
				InfantRate.setPassengertype(value);
				value = TD.get(1).getText();
				InfantRate.setRateperpsngr(value);
				value = TD.get(2).getText();
				InfantRate.setNoofpassengers(value);
				value = TD.get(7).getText();
				InfantRate.setTotal(value);
			}
			
			continuefrom+=1;//increment to pass the blank row
			
			TD = new ArrayList<WebElement>(TRs.get(continuefrom).findElements(By.tagName("td")));
			continuefrom+=1;
			ArrayList<WebElement> TDTRs = new ArrayList<WebElement>(TD.get(6).findElements(By.tagName("tr")));
			ArrayList<WebElement> TDTRTDs = new ArrayList<WebElement>(TDTRs.get(0).findElements(By.tagName("td")));
			
			String value1 = "";
			
			value1 = TDTRTDs.get(1).getText();
			try {
				value1 = CommonValidator.formatRemoveComma(value1);
			} catch (Exception e) {
				// TODO: handle exception
			}
			TotalBeforeTax = value1;
			
			TDTRTDs = new ArrayList<WebElement>(TDTRs.get(1).findElements(By.tagName("td")));
			value1 = TDTRTDs.get(1).getText();
			try {
				value1 = CommonValidator.formatRemoveComma(value1);
			} catch (Exception e) {
				// TODO: handle exception
			}
			Tax = value1;
			
			TDTRTDs = new ArrayList<WebElement>(TDTRs.get(2).findElements(By.tagName("td")));
			value1 = TDTRTDs.get(1).getText();
			try {
				value1 = CommonValidator.formatRemoveComma(value1);
			} catch (Exception e) {
				// TODO: handle exception
			}
			BookingFee = value1;
			
			TDTRTDs = new ArrayList<WebElement>(TDTRs.get(3).findElements(By.tagName("td")));
			value1 = TDTRTDs.get(1).getText();
			try {
				value1 = CommonValidator.formatRemoveComma(value1);
			} catch (Exception e) {
				// TODO: handle exception
			}
			TotalAmount = value1;
			
			continuefrom+=1;//increment to pass the blank row
			
			TD = new ArrayList<WebElement>(TRs.get(continuefrom).findElements(By.tagName("td")));
			ArrayList<WebElement> TRinTD = new ArrayList<WebElement>(TD.get(6).findElements(By.tagName("tr"))); // 7 <tr> tags are there...
			
			String value2 = "";
			ArrayList<WebElement> td = new ArrayList<WebElement>(TRinTD.get(0).findElements(By.tagName("td")));
			value2 = td.get(1).getText();
			Currency = value2;
			
			td = new ArrayList<WebElement>(TRinTD.get(1).findElements(By.tagName("td")));
			value2 = td.get(1).getText();
			try {
				value2 = CommonValidator.formatRemoveComma(value2);
			} catch (Exception e) {
				// TODO: handle exception
			}
			SubTotal = value2;
			
			td = new ArrayList<WebElement>(TRinTD.get(2).findElements(By.tagName("td")));
			value2 = td.get(1).getText();
			try {
				value2 = CommonValidator.formatRemoveComma(value2);
			} catch (Exception e) {
				// TODO: handle exception
			}
			TotalTaxAndOther = value2;
			
			td = new ArrayList<WebElement>(TRinTD.get(4).findElements(By.tagName("td")));
			value2 = td.get(1).getText();
			try {
				value2 = CommonValidator.formatRemoveComma(value2);
			} catch (Exception e) {
				// TODO: handle exception
			}
			TotalBookingValue = value2;
			
			td = new ArrayList<WebElement>(TRinTD.get(5).findElements(By.tagName("td")));
			value2 = td.get(1).getText();
			try {
				value2 = CommonValidator.formatRemoveComma(value2);
			} catch (Exception e) {
				// TODO: handle exception
			}
			AmountToBePaidNow = value2;
			
			td = new ArrayList<WebElement>(TRinTD.get(6).findElements(By.tagName("td")));
			value2 = td.get(1).getText();
			try {
				value2 = CommonValidator.formatRemoveComma(value2);
			} catch (Exception e) {
				// TODO: handle exception
			}
			AmountToBePaidForAirLine = value2;
			
			
			continuefrom+=1;// Increment to skip the blank <tr>
			continuefrom+=1;// Increment to skip the Traveler details heading row
			continuefrom+=1;// Increment to skip the Customer details heading row
			continuefrom+=1;
			//Starting main traveler details
			
			TD = new ArrayList<WebElement>(TRs.get(continuefrom).findElements(By.tagName("td")));
			continuefrom+=1;
			value2 = TD.get(1).getText().replace(":", "").trim();
			main.setGivenName(value2);
			
			Address address = new Address();
			value2 = TD.get(7).getText().replace(":", "").trim();
			address.setAddressCountry(value2);
			
			TD = new ArrayList<WebElement>(TRs.get(continuefrom).findElements(By.tagName("td")));
			continuefrom+=1;
			value2 = TD.get(1).getText().replace(":", "").trim();
			main.setSurname(value2);
			value2 = TD.get(7).getText().replace(":", "").trim();
			main.setPhoneNumber(value2);
			
			TD = new ArrayList<WebElement>(TRs.get(continuefrom).findElements(By.tagName("td")));
			continuefrom+=1;
			value2 = TD.get(1).getText().replace(":", "").trim();
			address.setAddressStreetNo(value2);
			value2 = TD.get(7).getText().replace(":", "").trim();
			main.setEmergencyNo(value2);
			
			TD = new ArrayList<WebElement>(TRs.get(continuefrom).findElements(By.tagName("td")));
			continuefrom+=1;
			value2 = TD.get(1).getText().replace(":", "").trim();
			address.setAddressStreetNo2(value2);
			value2 = TD.get(7).getText().replace(":", "").trim();
			main.setEmail(value2);
			
			TD = new ArrayList<WebElement>(TRs.get(continuefrom).findElements(By.tagName("td")));
			continuefrom+=1;
			value2 = TD.get(1).getText().replace(":", "").trim();
			address.setAddressCity(value2);
			
			main.setAddress(address);
			
			//FLIGHT PASSENGER DETAILS
			continuefrom+=1;//Increment to skip the blank row
			continuefrom+=1;//Increment to skip the flight passenger details heading
			
			
			ArrayList<WebElement> TRinTR = new ArrayList<WebElement>(TRs.get(continuefrom).findElements(By.tagName("tr")));
			continuefrom+=1;
			try
			{
				for(int i=1; i<=TRinTR.size(); i++)
				{
					String valuein = "";
					TD = new ArrayList<WebElement>( TRinTR.get(i).findElements(By.tagName("td")) );
					valuein = TD.get(0).getText().trim();
					
					if(valuein.contains("Adult"))
					{
						Traveler adult = new Traveler();
						adult.setPassenger(valuein);
						
						valuein = TD.get(1).getText().trim();
						adult.setNamePrefixTitle(valuein);
						
						valuein = TD.get(2).getText().trim();
						adult.setGivenName(valuein);
						
						valuein = TD.get(3).getText().trim();
						adult.setSurname(valuein);
						
						valuein = TD.get(4).getText().trim();
						adult.setPhoneNumber(valuein);
						
						valuein = TD.get(5).getText().trim();
						adult.setPassportNo(valuein);
						
						valuein = TD.get(6).getText().trim().split(" ")[0];
						try {
							valuein = CommonValidator.formatDateToCommon(valuein, "dd-MM-yyyy");
						} catch (Exception e) {
							// TODO: handle exception
						}
						adult.setPassportExpDate(valuein);
						
						valuein = TD.get(7).getText().trim();
						adult.setETicket_No(valuein);
						
						Adults.add(adult);
					}
					if(valuein.contains("Infant"))
					{
						Traveler infant = new Traveler();
						infant.setPassenger(valuein);
						
						valuein = TD.get(1).getText().trim();
						infant.setNamePrefixTitle(valuein);
						
						valuein = TD.get(2).getText().trim();
						infant.setGivenName(valuein);
						
						valuein = TD.get(3).getText().trim();
						infant.setSurname(valuein);
						
						valuein = TD.get(4).getText().trim();
						infant.setPhoneNumber(valuein);
						
						valuein = TD.get(5).getText().trim();
						infant.setPassportNo(valuein);
						
						valuein = TD.get(6).getText().trim().split(" ")[0];
						try {
							valuein = CommonValidator.formatDateToCommon(valuein, "dd-MM-yyyy");
						} catch (Exception e) {
							// TODO: handle exception
						}
						infant.setPassportExpDate(valuein);
						
						valuein = TD.get(7).getText().trim();
						infant.setETicket_No(valuein);
						
						Infants.add(infant);
					}
					
					if(valuein.contains("Children"))
					{
						Traveler child = new Traveler();
						child.setPassenger(valuein);
						
						valuein = TD.get(1).getText().trim();
						child.setNamePrefixTitle(valuein);
						
						valuein = TD.get(2).getText().trim();
						child.setGivenName(valuein);
						
						valuein = TD.get(3).getText().trim();
						child.setSurname(valuein);
						
						valuein = TD.get(4).getText().trim();
						child.setPhoneNumber(valuein);
						
						valuein = TD.get(5).getText().trim();
						child.setPassportNo(valuein);
						
						valuein = TD.get(6).getText().trim().split(" ")[0];
						try {
							valuein = CommonValidator.formatDateToCommon(valuein, "dd-MM-yyyy");
						} catch (Exception e) {
							// TODO: handle exception
						}
						child.setPassportExpDate(valuein);
						
						valuein = TD.get(7).getText().trim();
						child.setETicket_No(valuein);
						
						Children.add(child);
					}
		
				}
				
			}
			catch(Exception e)
			{
				
			}
			
			
			continuefrom+=1;//To skip blank <tr> line.
			continuefrom+=1;//Skip payment details heading <tr>.
			try {
				try {
					for(int i=1; i<=TRinTR.size(); i++)
					{
						String valuein = "";
						TD = new ArrayList<WebElement>( TRs.get(i).findElements(By.tagName("td")) );
						valuein = TD.get(0).getText().trim();
						
						if(valuein.contains("Flight Confirmation Deadline"))
						{
							
							ConfirmationDeadline = valuein;
						}
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			TD = new ArrayList<WebElement>(TRs.get(continuefrom).findElements(By.tagName("td")));
			continuefrom+=1;
			String value3 = "";
			
			value3 = TD.get(1).getText();
			value3 = value3.split(" ")[1].trim();
			ReferenceNumber = value3;
			value3 = TD.get(7).getText().split(" ")[1].trim();
			PaymentID = value3;
			
			TD = new ArrayList<WebElement>(TRs.get(continuefrom).findElements(By.tagName("td")));
			continuefrom+=1;
			value3 = TD.get(1).getText().split(" ")[1].trim();
			MerchantTrackID = value3;
			value3 = TD.get(3).getText().split("[()]")[1].trim();
			PaymentCurrency = value3;
			//value3 = TD.get(7).getText().split(" ")[1].trim();
			
			continuefrom+=1;//Skip blank <tr>
			continuefrom+=1;//Skip important information heading <tr>
			continuefrom+=1;//Skip flight information heading <tr>
			
			//FLIGHT INFORMATION 
			TD = new ArrayList<WebElement>(TRs.get(continuefrom).findElements(By.tagName("td")));
			continuefrom+=1;
			FlightInformation = TD.get(0).getText().trim();
		    
			/*TD = new ArrayList<WebElement>(TRs.get(continuefrom).findElements(By.tagName("td")));
			ArrayList<WebElement> TrinTd	= new ArrayList<WebElement>(TD.get(0).findElements(By.tagName("tr")));
			ArrayList<WebElement> TdTrinTd	= new ArrayList<WebElement>(TrinTd.get(1).findElements(By.tagName("td")));*/
			continuefrom+=1;//Skip the fare rule <tr>
			continuefrom+=1;//Skip the blank <tr>
			continuefrom+=1;//Skip The indicated policies are applicable based on the destination time zone <tr>
			
			TD = new ArrayList<WebElement>(TRs.get(continuefrom).findElements(By.tagName("td")));
			continuefrom+=1;
			value3 = TD.get(0).getText().replace(":", "").trim();
			ConfirmationDeadline = value3;
			continuefrom+=1;//Skip blank <tr>
			continuefrom+=1;//Skip customer details heading <tr>
			
			TD = new ArrayList<WebElement>(TRs.get(continuefrom).findElements(By.tagName("td")));
			continuefrom+=1;
			CancellationNote = TD.get(0).getText();
			
			TD = new ArrayList<WebElement>(TRs.get(continuefrom).findElements(By.tagName("td")));
			continuefrom+=1;
			Tel = TD.get(0).getText().replace(":", "").trim();
			
			TD = new ArrayList<WebElement>(TRs.get(continuefrom).findElements(By.tagName("td")));
			continuefrom+=1;
			Email = TD.get(0).getText().replace("Email :", "").trim();
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			
			available = true;
		}
		catch(Exception e)
		{
			
		}	
	}

	public String getRezgatewayContact() {
		return RezgatewayContact;
	}

	public void setRezgatewayContact(String rezgatewayContact) {
		RezgatewayContact = rezgatewayContact;
	}

	public String getBookingReference() {
		return BookingReference;
	}

	public void setBookingReference(String bookingReference) {
		BookingReference = bookingReference;
	}

	public String getBookingNo() {
		return BookingNo;
	}

	public void setBookingNo(String bookingNo) {
		BookingNo = bookingNo;
	}

	public String getThankYou() {
		return ThankYou;
	}

	public void setThankYou(String thankYou) {
		ThankYou = thankYou;
	}

	public String getSuppConfimationNo() {
		return SuppConfimationNo;
	}

	public void setSuppConfimationNo(String suppConfimationNo) {
		SuppConfimationNo = suppConfimationNo;
	}

	public String getBookingNo2() {
		return BookingNo2;
	}

	public void setBookingNo2(String bookingNo2) {
		BookingNo2 = bookingNo2;
	}

	public ArrayList<Flight> getOutbound() {
		return Outbound;
	}

	public void setOutbound(ArrayList<Flight> outbound) {
		Outbound = outbound;
	}

	public ArrayList<Flight> getInbound() {
		return Inbound;
	}

	public void setInbound(ArrayList<Flight> inbound) {
		Inbound = inbound;
	}

	public RatesperPassenger getAdultRate() {
		return AdultRate;
	}

	public void setAdultRate(RatesperPassenger adultRate) {
		AdultRate = adultRate;
	}

	public RatesperPassenger getChildRate() {
		return ChildRate;
	}

	public void setChildRate(RatesperPassenger childRate) {
		ChildRate = childRate;
	}

	public RatesperPassenger getInfantRate() {
		return InfantRate;
	}

	public void setInfantRate(RatesperPassenger infantRate) {
		InfantRate = infantRate;
	}

	public String getTotalBeforeTax() {
		return TotalBeforeTax;
	}

	public void setTotalBeforeTax(String totalBeforeTax) {
		TotalBeforeTax = totalBeforeTax;
	}

	public String getTax() {
		return Tax;
	}

	public void setTax(String tax) {
		Tax = tax;
	}

	public String getBookingFee() {
		return BookingFee;
	}

	public void setBookingFee(String bookingFee) {
		BookingFee = bookingFee;
	}

	public String getTotalAmount() {
		return TotalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		TotalAmount = totalAmount;
	}

	public String getCurrency() {
		return Currency;
	}

	public void setCurrency(String currency) {
		Currency = currency;
	}

	public String getSubTotal() {
		return SubTotal;
	}

	public void setSubTotal(String subTotal) {
		SubTotal = subTotal;
	}

	public String getTotalTaxAndOther() {
		return TotalTaxAndOther;
	}

	public void setTotalTaxAndOther(String totalTaxAndOther) {
		TotalTaxAndOther = totalTaxAndOther;
	}

	public String getTotalBookingValue() {
		return TotalBookingValue;
	}

	public void setTotalBookingValue(String totalBookingValue) {
		TotalBookingValue = totalBookingValue;
	}

	public String getAmountToBePaidNow() {
		return AmountToBePaidNow;
	}

	public void setAmountToBePaidNow(String amountToBePaidNow) {
		AmountToBePaidNow = amountToBePaidNow;
	}

	public String getAmountToBePaidForAirLine() {
		return AmountToBePaidForAirLine;
	}

	public void setAmountToBePaidForAirLine(String amountToBePaidForAirLine) {
		AmountToBePaidForAirLine = amountToBePaidForAirLine;
	}

	public Traveler getMain() {
		return main;
	}

	public void setMain(Traveler main) {
		this.main = main;
	}

	public ArrayList<Traveler> getAdults() {
		return Adults;
	}

	public void setAdults(ArrayList<Traveler> adults) {
		Adults = adults;
	}

	public ArrayList<Traveler> getChildren() {
		return Children;
	}

	public void setChildren(ArrayList<Traveler> children) {
		Children = children;
	}

	public ArrayList<Traveler> getInfants() {
		return Infants;
	}

	public void setInfants(ArrayList<Traveler> infants) {
		Infants = infants;
	}

	public String getReferenceNumber() {
		return ReferenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		ReferenceNumber = referenceNumber;
	}

	public String getMerchantTrackID() {
		return MerchantTrackID;
	}

	public void setMerchantTrackID(String merchantTrackID) {
		MerchantTrackID = merchantTrackID;
	}

	public String getPaymentID() {
		return PaymentID;
	}

	public void setPaymentID(String paymentID) {
		PaymentID = paymentID;
	}

	public String getPaymentCurrency() {
		return PaymentCurrency;
	}

	public void setPaymentCurrency(String paymentCurrency) {
		PaymentCurrency = paymentCurrency;
	}

	public String getPaymentDetailsAmount() {
		return PaymentDetailsAmount;
	}

	public void setPaymentDetailsAmount(String paymentDetailsAmount) {
		PaymentDetailsAmount = paymentDetailsAmount;
	}

	public String getFlightInformation() {
		return FlightInformation;
	}

	public void setFlightInformation(String flightInformation) {
		FlightInformation = flightInformation;
	}

	public String getConfirmationDeadline() {
		return ConfirmationDeadline;
	}

	public void setConfirmationDeadline(String confirmationDeadline) {
		ConfirmationDeadline = confirmationDeadline;
	}

	public String getCancellationNote() {
		return CancellationNote;
	}

	public void setCancellationNote(String cancellationNote) {
		CancellationNote = cancellationNote;
	}

	public String getTel() {
		return Tel;
	}

	public void setTel(String tel) {
		Tel = tel;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public HashMap<String, String> getPropertymap() {
		return Propertymap;
	}

	public void setPropertymap(HashMap<String, String> propertymap) {
		Propertymap = propertymap;
	}

	public boolean isTwoway() {
		return twoway;
	}

	public void setTwoway(boolean twoway) {
		this.twoway = twoway;
	}

	public boolean isAvailable() {
		return available;
	}
	
	
	
}
