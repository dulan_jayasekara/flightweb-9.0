package Flight.Reports;

//import java.io.File;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
//import java.util.concurrent.TimeUnit;



import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import system.classes.ReservationReport;

public class ReservationReportReader
{
	ReservationReport		reservationreport 	= new ReservationReport();
	HashMap<String, String>	Propertymap			= new HashMap<String, String>();								
	
	public ReservationReportReader(HashMap<String, String> Propymap)
	{
		Propertymap = Propymap;
	}
	/*public ReservationReport getReservationReport(String ReservationNo)
	{
		try
		{
			propertymap = ReadProperties.readpropeties(PropfilePath);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		ReservationReportReader r = new ReservationReportReader(propertymap);
		
		r.getReport("F3290C080814" confirmationpage, searchObject);
		
	}
	
	public ReservationReportReader(HashMap<String, String> Propymap, WebDriver driver)
	{
		try
		{
			propertymap = ReadProperties.readpropeties(PropfilePath);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		propertymap = Propymap;
		SUP = new SupportMethods(propertymap);
		try 
		{
			driver = SUP.initalizeDriver();
			SUP.login(driver);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}*/
	
	/*public static void main(String[] args) {
		
		ReservationReportReader readman = new ReservationReportReader();
		ReservationReport report = new ReservationReport();
		report = readman.getReport();
		
	}*/
	
	public ReservationReport getReport(WebDriver driver, String ReservationNo)
	{
		//COMMENT THIS------------->>>>>>>>>
		//String ReservationNo = "F3202C160714"; //One Outbound Flight
		//String ReservationNo1 = "F3206W010814";
		//String ReservationNo = ReservationNo
		
		/*FirefoxProfile pro		= new FirefoxProfile(new File("C:\\Users\\Sanoj\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\ujb8cwm6.default-1406265149085"));
		WebDriver      driver	= new FirefoxDriver(pro);*/
		
		WebDriverWait wait 		= new WebDriverWait(driver, 20);
		
		//http://dev3.rezg.net/rezbase_v3/admin/common/LoginPage.do//////////http://qav3.rezgateway.com/rezbase_v3/admin/common/LoginPage.do
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//driver.get("http://qav3.rezgateway.com/rezbase_v3/admin/common/LoginPage.do");
		//driver.findElement(By.id("user_id")).sendKeys("sanojC");
		//driver.findElement(By.id("password")).sendKeys("123456");
		//driver.findElement(By.id("loginbutton")).click();
		//COMMENT THIS------------->>>>>>>>>>
		
		
		driver.get(Propertymap.get("Portal.Url").concat("/reports/operational/mainReport.do?reportId=31&reportName=Reservation%20Report"));
		//driver.get("http://qav3.rezgateway.com/rezbase_v3/reports/operational/mainReport.do?reportId=31&reportName=Reservation%20Report");
		//WebDriverWait wait = new WebDriverWait(driver, 15);
		
		try
		{
			Thread.sleep(3000);
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		try
		{
			Thread.sleep(2000);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		
		
		//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		System.out.println();
		driver.switchTo().defaultContent();
		
		//SEARCH BY RESERVATION NO
		driver.switchTo().frame("reportIframe");
		driver.getPageSource();
		driver.findElement(By.id("searchby_reservationno")).click();
		driver.findElement(By.id("label_td_searchby_reservationno")).click();
		
		//((JavascriptExecutor)driver).executeScript("currentFieldFocus('searchby_label');checkRadioAction(this);");
		driver.findElement(By.id("searchby_reservationno")).click();
		//((JavascriptExecutor)driver).executeScript("currentFieldFocus('reservationno_label');");
		
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("reservationno")));
		driver.findElement(By.id("reservationno")).clear();
		driver.findElement(By.id("reservationno")).sendKeys(/*confirmationpage.getReservationNo()*/ReservationNo.trim());
		driver.findElement(By.id("reservationno_lkup")).click();
		String resNo = "";
		boolean notFound = true;
		try
		{
			driver.switchTo().defaultContent();
			driver.switchTo().frame("lookup");
			WebElement x = driver.findElement(By.id("lookupDataArea"));
			ArrayList<WebElement> y = new ArrayList<WebElement>(x.findElements(By.tagName("td")));
			int f = y.size();
			if(f==0)
			{
				notFound = true;
			}
			int c = 0;
			while(notFound)
			{
				try
				{
					resNo = driver.findElement(By.className("rowstyle"+c+"")).getText().trim();
					if(/*confirmationpage.getReservationNo()*/ReservationNo.trim().contains(resNo.trim()) || resNo.trim().contains(ReservationNo.trim()))
					{
						notFound = false;
						driver.findElement(By.className("rowstyle"+c+"")).click();
					}
					
				}
				catch(Exception e)
				{
					e.printStackTrace();
					TakesScreenshot screen = (TakesScreenshot)driver;
					FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Reservation Report Lookup.jpg"));
					if(f<=c+1)
					{
						((JavascriptExecutor)driver).executeScript("parent.hideLookup();");
						break;
					}
					
				}
				c++;
			}
			
			driver.switchTo().defaultContent();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(!notFound)
		{
			reservationreport.setAvailable(true);
			
			try
			{
				driver.switchTo().frame("reportIframe");
				((JavascriptExecutor)driver).executeScript("javascript: getReportData('view');");
				//driver.findElement(By.xpath("/html/body/span/table/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
				
				wait.until(ExpectedConditions.presenceOfElementLocated(By.className("reportDataRows0")));
				ArrayList<WebElement> rows = new ArrayList<WebElement>(driver.findElements(By.className("reportDataRows0")));
				
				ArrayList<WebElement> row1tds = new ArrayList<WebElement>(rows.get(0).findElements(By.tagName("td")));
				
				String value = "";
				
				value = row1tds.get(7).getText().trim();
				reservationreport.setSellingCurrency1(value);
				
				value = row1tds.get(8).getText().trim();
				reservationreport.setSellingCurrency2(value);
				
				value = row1tds.get(9).getText().trim();
				reservationreport.setBaseCurrency(value);
				
				
				ArrayList<WebElement> row2tds = new ArrayList<WebElement>(rows.get(1).findElements(By.tagName("td")));
				value = row2tds.get(0).getText().trim();
				reservationreport.setReservationNo(value);
				
				value = row2tds.get(1).getText().trim();
				reservationreport.setCustomerType(value);
				
				value = row2tds.get(2).getText().trim();
				reservationreport.setProductType(value);
				
				value = row2tds.get(3).getText().trim();
				reservationreport.setGuestName(value);
				
				value = row2tds.get(4).getText().trim();
				reservationreport.setVoucherIssued(value);
				
				value = row2tds.get(5).getText().trim();
				reservationreport.setLPOrequired(value);
				
				value = row2tds.get(6).getText().trim();
				value = checkExist(value);
				reservationreport.setTotalRate(value);
				
				value = row2tds.get(7).getText().trim();
				value = checkExist(value);
				reservationreport.setGrossOrderValue(value);
				
				
				ArrayList<WebElement> row3tds = new ArrayList<WebElement>(rows.get(2).findElements(By.tagName("td")));
				
				value = row3tds.get(0).getText().trim();
				reservationreport.setDocumentNo(value);
				
				value = row3tds.get(1).getText().trim();
				reservationreport.setCustomerName(value);
				
				value = row3tds.get(2).getText().trim();
				reservationreport.setBookingStatus(value);
				
				value = row3tds.get(3).getText().trim();
				reservationreport.setGuestContactNo(value);
				
				value = row3tds.get(5).getText().trim();
				reservationreport.setLPOprovided(value);
				
				value = row3tds.get(6).getText().trim();
				value = checkExist(value);
				reservationreport.setCreditcardFee(value);
				
				value = row3tds.get(7).getText().trim();
				value = checkExist(value);
				reservationreport.setAgentCommission(value);
				
				
				ArrayList<WebElement> row4tds = new ArrayList<WebElement>(rows.get(3).findElements(By.tagName("td")));
				
				value = row4tds.get(0).getText().trim();
				reservationreport.setBookingDate(value);
				
				value = row4tds.get(1).getText().trim();
				reservationreport.setSupplierName(value);
				
				value = row4tds.get(2).getText().trim();
				reservationreport.setBookingChannel(value);
				
				value = row4tds.get(3).getText().trim();
				reservationreport.setApprovalCode(value);
				
				value = row4tds.get(4).getText().trim();
				reservationreport.setInvoiceIssued(value);
				
				value = row4tds.get(5).getText().trim();
				reservationreport.setLPOno(value);
				
				value = row4tds.get(6).getText().trim();
				value = checkExist(value);
				reservationreport.setBookingFeeAndCharges(value);
				
				value = row4tds.get(7).getText().trim();
				value = checkExist(value);
				reservationreport.setNetOrderValue(value);
				
				value = row4tds.get(8).getText().trim();
				value = checkExist(value);
				reservationreport.setBaseNetOrderValue(value);
				
				
				ArrayList<WebElement> row5tds = new ArrayList<WebElement>(rows.get(4).findElements(By.tagName("td")));
				
				value = row5tds.get(0).getText().trim();
				reservationreport.setConfirmationDue(value);
				
				value = row5tds.get(1).getText().trim();
				reservationreport.setSupplierConfirmationNo(value);
				
				value = row5tds.get(2).getText().trim();
				reservationreport.setConsultantName(value);
				
				value = row5tds.get(3).getText().trim();
				reservationreport.setTransactionID(value);
				
				value = row5tds.get(4).getText();
				reservationreport.setInvoiceIssuedDate(value);
				
				value = row5tds.get(6).getText();
				value = checkExist(value);
				reservationreport.setAmountPaid(value);
				
				value = row5tds.get(7).getText();
				value = checkExist(value);
				reservationreport.setTotalCost(value);
				
				value = row5tds.get(8).getText();
				value = checkExist(value);
				reservationreport.setBasetotalCost(value);
				
			}
			catch(Exception e)
			{
				
			}		
		}
		else
		{
			reservationreport.setAvailable(false);
		}
		
		return reservationreport;
	}
	
	private String checkExist(String val)
	{
		if(val.contains(","))
		{
			val = val.replaceAll(",", "").trim();
		}
		
		return val;
	}
	
}
