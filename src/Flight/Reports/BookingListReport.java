package Flight.Reports;


import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.FirefoxProfile;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import system.classes.Address;
import system.classes.Flight;
import system.classes.Inbound;
import system.classes.Outbound;
import system.classes.RatesperPassenger;
import system.classes.Traveler;

import com.common.Validators.CommonValidator;



public class BookingListReport
{
	private String	date					= "-";
	private	String	time					= "-";
	private String	reservedBy				= "-";
	private String	paymentType				= "-";
	private String	type					= "-";
	private String	customerName			= "-";
	private	String	leadguestName			= "-";
	private	String	dateFirstElement		= "-";
	private String	cxlDeadline				= "-";
	private	String	cityName				= "-";
	private String	countryName				= "-";
	private String	product					= "-";
	private	String  verified				= "-";
	private	String	status					= "-";
	private	String	notificationInvoice		= "-";
	private String	notificationPayment		= "-";
	private String	notificationVoucher		= "-";
	private String	notificationTicket		= "-";
	
	//Reservation Summery
	private String  ReservationNumber		= "";//Validated
	private String  ReservationNo			= "";//Validated
	private String  ReservationDate			= "";
	private String  Origin					= "";//Validated
	private String  DateFirstElementUsed	= "";
	private String  Destination				= "";//Validated
	private String  CancellationDeadline	= "";//Validated
	private String  ProductsBooked			= "";
	private String  EticketingDeadline		= "";//Validated
	private String  BookingStatus			= "";
	private String  FailedSectorIncluded	= "";
	private String  paymentMethod			= "";
	private String  paymentReference		= "";
	
	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getPaymentReference() {
		return paymentReference;
	}

	public void setPaymentReference(String paymentReference) {
		this.paymentReference = paymentReference;
	}

	//Reservation Summery Cost details
	private String  CurrencyCode			= ""; //Validated
	private String  SubtotalBeforeTax		= "0";//Validated
	private String  BookingFee				= "0";//Validated
	private String  TaxandOther				= "0";//Validated
	private String  CreditCardFee			= "0";//Validated
	private String  TotalBookingValue		= "0";//Validated
	private String  AmountPayUpfront		= "0";//Validated
	private String  AmountPayToAirline		= "0";
	private String  AmountPaid				= "0";
	
	//Main Customer
	private Traveler maincus				= new Traveler();//Validated
	
	//PNR Details
	private String  PNR						= "";
	private String  TicketIssued			= "";
	private String  AirLinePaymentType		= "";
	private String  FlightBookingStatus		= "";
	private String  FailedReason			= "";
	
	//Trvaellers
	private ArrayList<Traveler> travelers	= new ArrayList<Traveler>();//Validated
	
	//Outbound and Inbound Flights
	private Outbound outbound				= new Outbound();//Validated
	private Inbound  inbound				= new Inbound();//Validated
	
	//Final Flight Cost Details
	private String finalCurrency			= ""; //Validated
	private String finalSubtotalBeforeTax	= "0";//Validated
	private String finalTaxAndOtherCharges	= "0";//Validated
	private String finalBookingFee			= "0";//Validated
	private String finalTotalBookingValue	= "0";//Validated
	private String finalAmountPayUpfront	= "0";//Validated
	private String finalAmountPaid			= "0";
	private String finalAmountPayToAirline	= "0";
	
	HashMap<String, String>	Propertymap		= new HashMap<String, String>();
	private boolean isreportLoaded			= false;
	
	
	
	public boolean isIsreportLoaded() {
		return isreportLoaded;
	}

	public void setIsreportLoaded(boolean isreportLoaded) {
		this.isreportLoaded = isreportLoaded;
	}

	public void setReservationNumber(String reservationNumber) {
		ReservationNumber = reservationNumber;
	}

	public void setReservationNo(String reservationNo) {
		ReservationNo = reservationNo;
	}

	public void setReservationDate(String reservationDate) {
		ReservationDate = reservationDate;
	}

	public void setOrigin(String origin) {
		Origin = origin;
	}

	public void setDateFirstElementUsed(String dateFirstElementUsed) {
		DateFirstElementUsed = dateFirstElementUsed;
	}

	public void setDestination(String destination) {
		Destination = destination;
	}

	public void setCancellationDeadline(String cancellationDeadline) {
		CancellationDeadline = cancellationDeadline;
	}

	public void setProductsBooked(String productsBooked) {
		ProductsBooked = productsBooked;
	}

	public void setEticketingDeadline(String eticketingDeadline) {
		EticketingDeadline = eticketingDeadline;
	}

	public void setBookingStatus(String bookingStatus) {
		BookingStatus = bookingStatus;
	}

	public void setFailedSectorIncluded(String failedSectorIncluded) {
		FailedSectorIncluded = failedSectorIncluded;
	}

	public void setCurrencyCode(String currencyCode) {
		CurrencyCode = currencyCode;
	}

	public void setSubtotalBeforeTax(String subtotalBeforeTax) {
		SubtotalBeforeTax = subtotalBeforeTax;
	}

	public void setBookingFee(String bookingFee) {
		BookingFee = bookingFee;
	}

	public void setTaxandOther(String taxandOther) {
		TaxandOther = taxandOther;
	}

	public void setCreditCardFee(String creditCardFee) {
		CreditCardFee = creditCardFee;
	}

	public void setTotalBookingValue(String totalBookingValue) {
		TotalBookingValue = totalBookingValue;
	}

	public void setAmountPayUpfront(String amountPayUpfront) {
		AmountPayUpfront = amountPayUpfront;
	}

	public void setAmountPayToAirline(String amountPayToAirline) {
		AmountPayToAirline = amountPayToAirline;
	}

	public void setAmountPaid(String amountPaid) {
		AmountPaid = amountPaid;
	}

	public void setMaincus(Traveler maincus) {
		this.maincus = maincus;
	}

	public void setPNR(String pNR) {
		PNR = pNR;
	}

	public void setTicketIssued(String ticketIssued) {
		TicketIssued = ticketIssued;
	}

	public void setAirLinePaymentType(String airLinePaymentType) {
		AirLinePaymentType = airLinePaymentType;
	}

	public void setFlightBookingStatus(String flightBookingStatus) {
		FlightBookingStatus = flightBookingStatus;
	}

	public void setFailedReason(String failedReason) {
		FailedReason = failedReason;
	}

	public void setTravelers(ArrayList<Traveler> travelers) {
		this.travelers = travelers;
	}

	public void setOutbound(Outbound outbound) {
		this.outbound = outbound;
	}

	public void setInbound(Inbound inbound) {
		this.inbound = inbound;
	}

	public void setFinalCurrency(String finalCurrency) {
		this.finalCurrency = finalCurrency;
	}

	public void setFinalSubtotalBeforeTax(String finalSubtotalBeforeTax) {
		this.finalSubtotalBeforeTax = finalSubtotalBeforeTax;
	}

	public void setFinalTaxAndOtherCharges(String finalTaxAndOtherCharges) {
		this.finalTaxAndOtherCharges = finalTaxAndOtherCharges;
	}

	public void setFinalBookingFee(String finalBookingFee) {
		this.finalBookingFee = finalBookingFee;
	}

	public void setFinalTotalBookingValue(String finalTotalBookingValue) {
		this.finalTotalBookingValue = finalTotalBookingValue;
	}

	public void setFinalAmountPayUpfront(String finalAmountPayUpfront) {
		this.finalAmountPayUpfront = finalAmountPayUpfront;
	}

	public void setFinalAmountPaid(String finalAmountPaid) {
		this.finalAmountPaid = finalAmountPaid;
	}

	public void setFinalAmountPayToAirline(String finalAmountPayToAirline) {
		this.finalAmountPayToAirline = finalAmountPayToAirline;
	}

	/*public static void main(String[] args)
	{
		BookingListReport readman = new BookingListReport();
		readman.getReport();
	}*/
	
	public BookingListReport(HashMap<String, String> propertymap)
	{
		Propertymap = propertymap;
	}
	
	public void getReport(WebDriver driver, String ResNo) throws WebDriverException, IOException 
	{	
		WebDriverWait wait = new WebDriverWait(driver, 15);
		driver.get(Propertymap.get("Portal.Url").concat("/reports/operational/mainReport.do?reportId=39&reportName=Booking%20List%20Report"));
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		driver.findElement(By.id("reservationno")).clear();
		driver.findElement(By.id("reservationno")).sendKeys(ResNo.trim());
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("reservationno_lkup")));
		driver.findElement(By.id("reservationno_lkup")).click();
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/table[2]/tbody/tr/td[1]/table/tbody/tr/td/table/tbody/tr/td")));
			driver.findElement(By.xpath("/html/body/table[2]/tbody/tr/td[1]/table/tbody/tr/td/table/tbody/tr/td")).click();
		} catch (Exception e) {
			
		}
		
		((JavascriptExecutor)driver).executeScript("javascript: getReportData('view');");
		
		String ReservationNo1	= ResNo.trim();
		
		try
		{
			
			wait.until(ExpectedConditions.presenceOfElementLocated(By.className("reportDataRows0")));
			ArrayList<WebElement> list = new ArrayList<WebElement>(driver.findElements(By.className("reportDataRows0")));
			forloop:for (int i = 0; i < list.size(); i++) 
			{
				try
				{
					WebElement  CurrentElement  = list.get(i);
					String      ReservationNo   = CurrentElement.findElements(By.tagName("td")).get(0).getText().trim();
					
					if(ReservationNo1.contains(ReservationNo) || ReservationNo.contains(ReservationNo1) )
					{
						try {
							date	= CurrentElement.findElements(By.tagName("td")).get(1).getText().trim();
							date	= CommonValidator.formatDateToCommon(date, "dd-MMM-yyyy");
						} catch (Exception e) {
							
						}
						
						try {
							time	= CurrentElement.findElements(By.tagName("td")).get(2).getText().trim();
							time	= CommonValidator.formatTimeToCommon(time);
						} catch (Exception e) {
							
						}
						
						reservedBy		= CurrentElement.findElements(By.tagName("td")).get(3).getText().trim();
						paymentType		= CurrentElement.findElements(By.tagName("td")).get(4).getText().trim();
						type			= CurrentElement.findElements(By.tagName("td")).get(5).getText().trim();
						customerName	= CurrentElement.findElements(By.tagName("td")).get(6).getText().trim();
						leadguestName	= CurrentElement.findElements(By.tagName("td")).get(7).getText().trim();
						
						try {
							dateFirstElement	= CurrentElement.findElements(By.tagName("td")).get(8).getText().trim();
							dateFirstElement	= CommonValidator.formatDateToCommon(dateFirstElement, "dd-MMM-yyyy");
						} catch (Exception e) {
							
						}
						
						try {
							cxlDeadline	= CurrentElement.findElements(By.tagName("td")).get(9).getText().trim();
							cxlDeadline	= CommonValidator.formatDateToCommon(cxlDeadline, "dd-MMM-yyyy");
						} catch (Exception e) {
							
						}
						
						try {
							cityName	= CurrentElement.findElements(By.tagName("td")).get(10).getText().trim().split("/")[0].trim();
							countryName	= CurrentElement.findElements(By.tagName("td")).get(10).getText().trim().split("/")[1].trim();
						} catch (Exception e) {
							
						}
						
						product			= CurrentElement.findElements(By.tagName("td")).get(11).getText().trim();
						verified		= CurrentElement.findElements(By.tagName("td")).get(12).getText().trim();
						status			= CurrentElement.findElements(By.tagName("td")).get(13).getText().trim();
						//notificationInvoice	= CurrentElement.findElements(By.tagName("td")).get(14).getText().trim();
						//notificationPayment	= CurrentElement.findElements(By.tagName("td")).get(15).getText().trim();
						//notificationVoucher	= CurrentElement.findElements(By.tagName("td")).get(16).getText().trim();
						//notificationTicket	= CurrentElement.findElements(By.tagName("td")).get(17).getText().trim();
						
						try {
							if(CurrentElement.findElements(By.id("imgiorange")).size() > 0)
							{
								notificationInvoice = "not issued";
							}
							else
							{
								notificationInvoice = "issued";
							}
						} catch (Exception e) {
							
						}
						
						try {
							if(CurrentElement.findElements(By.id("imgporange")).size() > 0)
							{
								notificationPayment = "not issued";
							}
							else
							{
								notificationPayment = "issued";
							}
						} catch (Exception e) {
							
						}
						
						try {
							if(CurrentElement.findElements(By.id("imgvorange")).size() > 0)
							{
								notificationVoucher = "not issued";
							}
							else
							{
								notificationVoucher = "issued";
							}
						} catch (Exception e) {
							
						}
						
						try {
							if(CurrentElement.findElements(By.id("imgtorange")).size() > 0)
							{
								notificationTicket = "not issued";
							}
							else
							{
								notificationTicket = "issued";
							}
						} catch (Exception e) {
							
						}
						
						((JavascriptExecutor)driver).executeScript("javascript:loadPopupExt('../operational/BookingCard.do?ActionType=submit&reservationNo="+ReservationNo1+"',1000,600);");
						
						isreportLoaded = true;
						break  forloop;
					}
				}
				catch(Exception e)
				{
					//isreportLoaded = false;
					//break;
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		if(isreportLoaded)
		{
			driver.switchTo().defaultContent();
			driver.switchTo().frame("dialogwindow");
			try
			{
				WebElement reservationDetails	= driver.findElement(By.id("reservationDetailsId"));
				WebElement customerDetails		= driver.findElement(By.id("customerDetailsId"));
				
				ArrayList<WebElement> reservationDetailsTRtags	= new ArrayList<WebElement>(reservationDetails.findElements(By.tagName("tr")));
				
				ArrayList<WebElement> TDtags	= new ArrayList<WebElement>(reservationDetailsTRtags.get(0).findElements(By.tagName("td")));
				this.ReservationNo				= TDtags.get(3).getText();
				this.ReservationDate			= TDtags.get(6).getText();
				
				TDtags = new ArrayList<WebElement>(reservationDetailsTRtags.get(1).findElements(By.tagName("td")));
				this.Origin						= TDtags.get(3).getText();
				this.DateFirstElementUsed		= TDtags.get(6).getText();
				
				TDtags = new ArrayList<WebElement>(reservationDetailsTRtags.get(2).findElements(By.tagName("td")));
				this.Destination				= TDtags.get(3).getText();
				this.CancellationDeadline		= TDtags.get(6).getText();
				
				TDtags = new ArrayList<WebElement>(reservationDetailsTRtags.get(3).findElements(By.tagName("td")));
				this.ProductsBooked				= TDtags.get(3).getText();
				this.EticketingDeadline			= TDtags.get(6).getText();
				
				TDtags = new ArrayList<WebElement>(reservationDetailsTRtags.get(4).findElements(By.tagName("td")));
				this.BookingStatus				= TDtags.get(3).getText();
				
				TDtags = new ArrayList<WebElement>(reservationDetailsTRtags.get(5).findElements(By.tagName("td")));
				this.FailedSectorIncluded		= TDtags.get(3).getText();
				
				TDtags = new ArrayList<WebElement>(reservationDetailsTRtags.get(6).findElements(By.tagName("td")));
				this.paymentMethod				= TDtags.get(3).getText();
				
				TDtags = new ArrayList<WebElement>(reservationDetailsTRtags.get(7).findElements(By.tagName("td")));
				this.paymentReference			= TDtags.get(3).getText();
				
				TDtags = new ArrayList<WebElement>(reservationDetailsTRtags.get(8).findElements(By.tagName("td")));
				this.CurrencyCode				= TDtags.get(6).getText().split("[()]")[1];
				
				//Cost Details
				String val = "";
				TDtags = new ArrayList<WebElement>(reservationDetailsTRtags.get(9).findElements(By.tagName("td")));
				val = TDtags.get(6).getText();
				val = checkExist(val);
				this.SubtotalBeforeTax			= val;
				
				TDtags = new ArrayList<WebElement>(reservationDetailsTRtags.get(10).findElements(By.tagName("td")));
				val = TDtags.get(6).getText();
				val = checkExist(val);
				this.BookingFee					= val;
				
				TDtags = new ArrayList<WebElement>(reservationDetailsTRtags.get(11).findElements(By.tagName("td")));
				val = TDtags.get(6).getText();
				val = checkExist(val);
				this.TaxandOther				= val;
				
				TDtags = new ArrayList<WebElement>(reservationDetailsTRtags.get(12).findElements(By.tagName("td")));
				val = TDtags.get(6).getText();
				val = checkExist(val);
				this.CreditCardFee				= val;
				
				TDtags = new ArrayList<WebElement>(reservationDetailsTRtags.get(13).findElements(By.tagName("td")));
				val = TDtags.get(6).getText();
				val = checkExist(val);
				this.TotalBookingValue			= val;
				
				TDtags = new ArrayList<WebElement>(reservationDetailsTRtags.get(14).findElements(By.tagName("td")));
				val = TDtags.get(6).getText();
				val = checkExist(val);
				this.AmountPayUpfront			= val;
				
				TDtags = new ArrayList<WebElement>(reservationDetailsTRtags.get(15).findElements(By.tagName("td")));
				val = TDtags.get(6).getText();
				val = checkExist(val);
				this.AmountPayToAirline			= val;
				
				TDtags = new ArrayList<WebElement>(reservationDetailsTRtags.get(16).findElements(By.tagName("td")));
				val = TDtags.get(6).getText();
				val = checkExist(val);
				this.AmountPaid					= val;
				
				
				//Main Customer Details
				ArrayList<WebElement> customerDetailsTRtags = new ArrayList<WebElement>(customerDetails.findElements(By.tagName("tr")));
									  customerDetailsTRtags = new ArrayList<WebElement>(customerDetailsTRtags.get(0).findElements(By.tagName("tr")));
				
				ArrayList<WebElement> CusTDtags	= new ArrayList<WebElement>(customerDetailsTRtags.get(0).findElements(By.tagName("td")));
				maincus.setGivenName(CusTDtags.get(3).getText().trim().split(" ")[0].trim());
				maincus.setSurname(CusTDtags.get(3).getText().trim().split(" ")[1].trim());
				maincus.setEmergencyNo(CusTDtags.get(6).getText());
				
				Address address = new Address();
				CusTDtags = new ArrayList<WebElement>(customerDetailsTRtags.get(1).findElements(By.tagName("td")));
				
				address.setAddressStreetNo(CusTDtags.get(3).getText());
				maincus.setPhoneNumber(CusTDtags.get(6).getText());
				
				CusTDtags = new ArrayList<WebElement>(customerDetailsTRtags.get(2).findElements(By.tagName("td")));
				maincus.setEmail(CusTDtags.get(3).getText());
				
				CusTDtags = new ArrayList<WebElement>(customerDetailsTRtags.get(3).findElements(By.tagName("td")));
				address.setAddressCity(CusTDtags.get(3).getText());
				
				CusTDtags = new ArrayList<WebElement>(customerDetailsTRtags.get(4).findElements(By.tagName("td")));
				address.setAddressCountry(CusTDtags.get(3).getText());
				
				maincus.setAddress(address);
				
				
				//FLIGHT DETAILS PANE
				driver.findElement(By.id("flightDetailsimgTD")).click();
				ArrayList<WebElement> flightDetailsTRtags	= new ArrayList<WebElement>();
				//WebElement x = flightDetails.findElement(By.xpath(".//*[@id='flightDetailsId']/table/tbody/tr[1]"));
				try {
					for(int i=1; i<10; i++)
					{
						flightDetailsTRtags.add(driver.findElement(By.xpath(".//*[@id='flightDetailsId']/table/tbody/tr["+i+"]")));
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				
				
				ArrayList<WebElement> flightTRtags	= new ArrayList<WebElement>(flightDetailsTRtags.get(1).findElements(By.tagName("tr")));
				//PNR data
				ArrayList<WebElement> flightTDtags	= new ArrayList<WebElement>(flightTRtags.get(0).findElements(By.tagName("td")));
				/*System.out.println(flightTDtags.get(0).getText());
				System.out.println(flightTDtags.get(1).getText());
				System.out.println(flightTDtags.get(2).getText());
				System.out.println(flightTDtags.get(3).getText());*/
				this.PNR					= flightTDtags.get(3).getText();
				
				flightTDtags = new ArrayList<WebElement>(flightTRtags.get(1).findElements(By.tagName("td")));
				/*System.out.println(flightTDtags.get(0).getText());
				System.out.println(flightTDtags.get(1).getText());
				System.out.println(flightTDtags.get(2).getText());
				System.out.println(flightTDtags.get(3).getText());*/
				this.TicketIssued			= flightTDtags.get(3).getText();
				
				flightTDtags = new ArrayList<WebElement>(flightTRtags.get(2).findElements(By.tagName("td")));
				/*System.out.println(flightTDtags.get(0).getText());
				System.out.println(flightTDtags.get(1).getText());
				System.out.println(flightTDtags.get(2).getText());
				System.out.println(flightTDtags.get(3).getText());*/
				this.AirLinePaymentType		= flightTDtags.get(3).getText();
				
				flightTDtags = new ArrayList<WebElement>(flightTRtags.get(3).findElements(By.tagName("td")));
				
				/*System.out.println(flightTDtags.get(0).getText());
				System.out.println(flightTDtags.get(1).getText());
				System.out.println(flightTDtags.get(2).getText());
				System.out.println(flightTDtags.get(3).getText());*/
				this.FlightBookingStatus	= flightTDtags.get(3).getText();
				
				flightTDtags = new ArrayList<WebElement>(flightTRtags.get(4).findElements(By.tagName("td")));
				/*System.out.println(flightTDtags.get(0).getText());
				System.out.println(flightTDtags.get(1).getText());
				System.out.println(flightTDtags.get(2).getText());
				System.out.println(flightTDtags.get(3).getText());*/
				this.FailedReason			= flightTDtags.get(3).getText();
				
				//Passenger and Rate per Passenger Details
				try
				{
					flightTRtags			= new ArrayList<WebElement>(flightDetailsTRtags.get(2).findElements(By.tagName("tr")));
					int people				= flightTRtags.size()-1;
					flightTDtags			= new ArrayList<WebElement>(flightTRtags.get(0).findElements(By.tagName("td")));
					String currency			= flightTDtags.get(8).getText();
					currency				= currency.split("[()]")[1];
					//System.out.println(currency);
					for(int i=1; i<=people; i++ )
					{
						flightTDtags			= new ArrayList<WebElement>(flightTRtags.get(i).findElements(By.tagName("td")));
						Traveler traveler 		= new Traveler();
						traveler.setPassenger(flightTDtags.get(0).getText().trim());
						traveler.setGivenName(flightTDtags.get(1).getText().trim());
						traveler.setSurname(flightTDtags.get(2).getText().trim());
						traveler.setAge(flightTDtags.get(3).getText().trim());
						
						String bd = "";
						bd = flightTDtags.get(4).getText().trim();
						try {
							bd = CommonValidator.formatDateToCommon(bd, "yyyy-MM-dd");
						} catch (Exception e) {
							try {
								bd = CommonValidator.formatDateToCommon(bd, "dd-MM-yyyy");
							} catch (Exception e2) {
								
							}
						}
						traveler.setBirthDay(bd);
						traveler.setPassportNo(flightTDtags.get(5).getText().trim());
						traveler.setETicket_No(flightTDtags.get(6).getText().trim());
						
						RatesperPassenger rate	= new RatesperPassenger();
						rate.setCurrency(currency);
						rate.setTotal(flightTDtags.get(7).getText().trim());
						rate.setTaxperpsngr(flightTDtags.get(8).getText().trim());
						traveler.setRate(rate);
						
						travelers.add(traveler);
					}
				}
				catch(Exception e)
				{
					
				}
				
				
				//Flight Inbound and Outbound Details
				flightTRtags					= new ArrayList<WebElement>(flightDetailsTRtags.get(5).findElements(By.tagName("tr")));
				ArrayList<Flight> outboundlist	= new ArrayList<Flight>();
				int outflight					= flightTRtags.size()-1;
				int outflightcount				= outflight / 5;
				int c							= 0;
				for(int j=0; j<outflightcount; j++)
				{
					Flight flight			= new Flight();
					flightTDtags			= new ArrayList<WebElement>(flightTRtags.get(c).findElements(By.tagName("td")));
					c+=1;
					flight.setDepartureLocationCode(flightTDtags.get(3).getText());
					flight.setArrivalLocationCode(flightTDtags.get(6).getText());
					
					flightTDtags			= new ArrayList<WebElement>(flightTRtags.get(c).findElements(By.tagName("td")));
					c+=1;
					flight.setDeparture_port(flightTDtags.get(3).getText());
					flight.setArrival_port(flightTDtags.get(6).getText());
					
					flightTDtags			= new ArrayList<WebElement>(flightTRtags.get(c).findElements(By.tagName("td")));
					c+=1;
					String y = "";
					try {
						y = flightTDtags.get(3).getText();
						SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
						SimpleDateFormat sdfto = new SimpleDateFormat("yyyy-MM-dd");
						Date d = sdf.parse(y);
						y = sdfto.format(d);
					} catch (Exception e) {
						e.printStackTrace();
					}
					flight.setDepartureDate(y);
					
					String z = "";
					try {
						z = flightTDtags.get(6).getText();
						SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
						SimpleDateFormat sdfto = new SimpleDateFormat("yyyy-MM-dd");
						Date d = sdf.parse(z);
						z = sdfto.format(d);
					} catch (Exception e) {
						e.printStackTrace();
					}
					flight.setArrivalDate(z);
					
					flightTDtags			= new ArrayList<WebElement>(flightTRtags.get(c).findElements(By.tagName("td")));
					c+=1;
					flight.setDepartureTime(flightTDtags.get(3).getText());
					flight.setArrivalTime(flightTDtags.get(6).getText());
					
					flightTDtags			= new ArrayList<WebElement>(flightTRtags.get(c).findElements(By.tagName("td")));
					flight.setFlightNo(flightTDtags.get(3).getText());
					c+=2;
					
					outboundlist.add(flight);
				}
				
				outbound.setOutBflightlist(outboundlist);
				
				//System.out.println("============================================================");
				//System.out.println(flightDetailsTRtags.get(6).getText());
				int l = 6;
				try {
				if(!flightDetailsTRtags.get(6).getText().equalsIgnoreCase("") || true)
				{
					System.out.println(flightDetailsTRtags.get(6).getText());
					System.out.println(flightDetailsTRtags.get(7).getText());
					flightTRtags					= new ArrayList<WebElement>(flightDetailsTRtags.get(7).findElements(By.tagName("tr")));
					ArrayList<Flight> inboundlist	= new ArrayList<Flight>();
					int inflight					= flightTRtags.size()-1;
					int inflightcount				= inflight / 5;
					int q							= 0;
					l=8;
					for(int j=0; j<inflightcount; j++)
					{
						Flight flight			= new Flight();
						flightTDtags			= new ArrayList<WebElement>(flightTRtags.get(q).findElements(By.tagName("td")));
						q+=1;
						flight.setDepartureLocationCode(flightTDtags.get(3).getText());
						flight.setArrivalLocationCode(flightTDtags.get(6).getText());
						
						flightTDtags			= new ArrayList<WebElement>(flightTRtags.get(q).findElements(By.tagName("td")));
						q+=1;
						flight.setDeparture_port(flightTDtags.get(3).getText());
						flight.setArrival_port(flightTDtags.get(6).getText());
						
						flightTDtags			= new ArrayList<WebElement>(flightTRtags.get(q).findElements(By.tagName("td")));
						q+=1;
						String y = "";
						try {
							y = flightTDtags.get(3).getText();
							SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
							SimpleDateFormat sdfto = new SimpleDateFormat("yyyy-MM-dd");
							Date d = sdf.parse(y);
							y = sdfto.format(d);
						} catch (Exception e) {
							e.printStackTrace();
						}
						flight.setDepartureDate(y);
						
						String z = "";
						try {
							z = flightTDtags.get(6).getText();
							SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
							SimpleDateFormat sdfto = new SimpleDateFormat("yyyy-MM-dd");
							Date d = sdf.parse(z);
							z = sdfto.format(d);
						} catch (Exception e) {
							e.printStackTrace();
						}
						flight.setArrivalDate(z);
						
						flightTDtags			= new ArrayList<WebElement>(flightTRtags.get(q).findElements(By.tagName("td")));
						q+=1;
						flight.setDepartureTime(flightTDtags.get(3).getText());
						flight.setArrivalTime(flightTDtags.get(6).getText());
						
						flightTDtags			= new ArrayList<WebElement>(flightTRtags.get(q).findElements(By.tagName("td")));
						flight.setFlightNo(flightTDtags.get(3).getText());
						q+=2;
						
						inboundlist.add(flight);
					}
					
					inbound.setInBflightlist(inboundlist);
					l=8;
				}
				} catch (Exception e) {
					e.printStackTrace();
					l=6;
				}
				
				
				System.out.println();
				flightTRtags	= new ArrayList<WebElement>(flightDetailsTRtags.get(l).findElements(By.tagName("tr")));
				flightTDtags	= new ArrayList<WebElement>(flightTRtags.get(0).findElements(By.tagName("td")));
				String str = flightTDtags.get(3).getText().split("[()]")[1].trim();
				this.finalCurrency = str;
				
				flightTDtags	= new ArrayList<WebElement>(flightTRtags.get(1).findElements(By.tagName("td")));
				str 			= flightTDtags.get(3).getText().trim();
				this.finalSubtotalBeforeTax		= str;
				
				flightTDtags	= new ArrayList<WebElement>(flightTRtags.get(2).findElements(By.tagName("td")));
				str				= flightTDtags.get(3).getText().trim();
				this.finalTaxAndOtherCharges	= str;
				
				flightTDtags	= new ArrayList<WebElement>(flightTRtags.get(3).findElements(By.tagName("td")));
				str				= flightTDtags.get(3).getText().trim();
				this.finalBookingFee			= str;
				
				flightTDtags	= new ArrayList<WebElement>(flightTRtags.get(4).findElements(By.tagName("td")));
				str				= flightTDtags.get(3).getText().trim();
				this.finalTotalBookingValue		= str;
				
				flightTDtags	= new ArrayList<WebElement>(flightTRtags.get(5).findElements(By.tagName("td")));
				str				= flightTDtags.get(3).getText().trim();
				this.finalAmountPayUpfront		= str;
				
				flightTDtags	= new ArrayList<WebElement>(flightTRtags.get(6).findElements(By.tagName("td")));
				str				= flightTDtags.get(3).getText().trim();
				this.finalAmountPaid			= str;
				
				flightTDtags	= new ArrayList<WebElement>(flightTRtags.get(7).findElements(By.tagName("td")));
				str				= flightTDtags.get(3).getText().trim();
				this.finalAmountPayToAirline	= str;
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Booking List Report Error.jpg"));	
			}
		}
		else
		{
			isreportLoaded = false;
		}
		
	}

	public String getReservationNumber() {
		return ReservationNumber;
	}

	public boolean isReportLoaded() {
		return isreportLoaded;
	}

	public String getReservationNo() {
		return ReservationNo;
	}

	public String getReservationDate() {
		return ReservationDate;
	}

	public String getOrigin() {
		return Origin;
	}

	public String getDateFirstElementUsed() {
		return DateFirstElementUsed;
	}

	public String getDestination() {
		return Destination;
	}

	public String getCancellationDeadline() {
		return CancellationDeadline;
	}

	public String getProductsBooked() {
		return ProductsBooked;
	}

	public String getEticketingDeadline() {
		return EticketingDeadline;
	}

	public String getBookingStatus() {
		return BookingStatus;
	}

	public String getFailedSectorIncluded() {
		return FailedSectorIncluded;
	}

	public String getCurrencyCode() {
		return CurrencyCode;
	}

	public String getSubtotalBeforeTax() {
		return SubtotalBeforeTax;
	}

	public String getBookingFee() {
		return BookingFee;
	}

	public String getTaxandOther() {
		return TaxandOther;
	}

	public String getCreditCardFee() {
		return CreditCardFee;
	}

	public String getTotalBookingValue() {
		return TotalBookingValue;
	}

	public String getAmountPayUpfront() {
		return AmountPayUpfront;
	}

	public String getAmountPayToAirline() {
		return AmountPayToAirline;
	}

	public String getAmountPaid() {
		return AmountPaid;
	}

	public Traveler getMaincus() {
		return maincus;
	}

	public String getPNR() {
		return PNR;
	}

	public String getTicketIssued() {
		return TicketIssued;
	}

	public String getAirLinePaymentType() {
		return AirLinePaymentType;
	}

	public String getFlightBookingStatus() {
		return FlightBookingStatus;
	}

	public String getFailedReason() {
		return FailedReason;
	}

	public ArrayList<Traveler> getTravelers() {
		return travelers;
	}

	public Outbound getOutbound() {
		return outbound;
	}

	public Inbound getInbound() {
		return inbound;
	}

	public String getFinalCurrency() {
		return finalCurrency;
	}

	public String getFinalSubtotalBeforeTax() {
		return finalSubtotalBeforeTax;
	}

	public String getFinalTaxAndOtherCharges() {
		return finalTaxAndOtherCharges;
	}

	public String getFinalBookingFee() {
		return finalBookingFee;
	}

	public String getFinalTotalBookingValue() {
		return finalTotalBookingValue;
	}

	public String getFinalAmountPayUpfront() {
		return finalAmountPayUpfront;
	}

	public String getFinalAmountPaid() {
		return finalAmountPaid;
	}

	public String getFinalAmountPayToAirline() {
		return finalAmountPayToAirline;
	}
	
	private String checkExist(String val)
	{
		if( val.contains(",") )
		{
			val = val.replaceAll(",", "").trim();
		}
		
		return val;
	}
	
}
