package Flight.Reports;

//import java.io.File;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
//import java.util.concurrent.TimeUnit;



import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.FirefoxProfile;
//import org.openqa.selenium.support.ui.Select;
//import org.openqa.selenium.support.ui.WebDriverWait;

public class ProfitAndLoss
{
	String		BookingNo		= "";
	String		BookingDate		= "";
	String		ConsultantName	= "";
	
	String		Combination		= "";
	String		CommTemplate	= "";
	String		ContractType	= "";
	
	String		BookingType		= "";
	String		AgentName		= "";
	
	String		BookingChannel	= "";
	String		POSlocation		= "";
	String		State			= "";
	
	String		GuestCount		= "";
	String		CheckIn			= "";
	String		CheckOut		= "";
	String		NightCount		= "";
	
	String		BaseCurrency1	= "";
	String		GrossBookingVal	= "0";
	String		AgentCommission	= "0";
	String		NetBookingVal	= "0";
	
	String		BaseCurrency2	= "";
	String		CostOfSales		= "";
	String		Profit			= "";
	String		ProfitPerecent	= "";
	
	String		BaseCurrency3	= "";
	String		InvoiceTotal	= "";
	String		InvoicePaid		= "";
	String		InvBalanceDue	= "";
	
	String		ReservationNo	= "";
	boolean		isRecordExist	= false;
	int			index			= 0;
	WebElement	Selectedelement	= null;
	boolean		ReportLoaded	= false;
	HashMap<String, String>	Propertymap	= new HashMap<String, String>();
	
	
	public ProfitAndLoss(HashMap<String, String> propertymap)
	{
		Propertymap = propertymap;
	}
	
	/*public static void main(String[] args)
	{
		ProfitAndLoss p = new ProfitAndLoss();
		p.loadReport();
	}*/
	
	public void loadReport(WebDriver driver, String ResNo ) throws WebDriverException, IOException
	{
		/*FirefoxProfile pro		= new FirefoxProfile(new File("C:\\Users\\Sanoj\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\ujb8cwm6.default-1406265149085"));
		WebDriver      driver	= new FirefoxDriver(pro);*/
		//WebDriverWait  wait 	= new WebDriverWait(driver, 20);
		
		//http://dev3.rezg.net/rezbase_v3/admin/common/LoginPage.do//////////http://qav3.rezgateway.com/rezbase_v3/admin/common/LoginPage.do
		/*driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://qav3.rezgateway.com/rezbase_v3/admin/common/LoginPage.do");
		driver.findElement(By.id("user_id")).sendKeys("sanojC");
		driver.findElement(By.id("password")).sendKeys("123456");
		driver.findElement(By.id("loginbutton")).click();*/
		//COMMENT THIS------------->>>>>>>>>>

		driver.get(Propertymap.get("Portal.Url").concat("/reports/operational/mainReport.do?reportId=38&reportName=Profit%20and%20Loss%20Report"));
		
		 /* F3494W170914*/ 
		//String	ReservNo	= "F3377C170914"ResNo;
		String	ReservNo	= ResNo;
		boolean	found		= false;
		//System.out.println();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		//new Select(driver.findElements(By.className("dropdownStyle")).get(0)).selectByValue("thisweek");
		((JavascriptExecutor)driver).executeScript("javascript: getReportData('view');");
		//((JavascriptExecutor)driver).executeScript("javascript:getReportData('view','2');");
		//driver.findElement(By.xpath(".//*[@id='pagingStyleRpt']/table/tbody/tr/th")).click();
		int c = 0;
		if(ReservNo.contains("-C"))
		{
			ReservNo = ReservNo.replace("-C", "");
		}
		
		//ArrayList<WebElement> trlist = new ArrayList<WebElement>(driver.findElements(By.xpath(".//*[@id='reportData']/tbody/tr")));
		ArrayList<WebElement> trlist = new ArrayList<WebElement>(driver.findElements(By.xpath("/html/body/span/table/tbody/tr/td/div/div/form/div[2]/table/tbody/tr")));
		
		boolean t = false;
		int n = 1;
		for(n = 1; n<15; n++ )
		{
			for(c = 0; c<trlist.size(); c++ )
			{
				String text = "";
				try
				{	
					text = trlist.get(c).findElements(By.tagName("td")).get(0).getText();
					System.out.println(text);
					try {
						if(text.contains("Page"))
						{
							((JavascriptExecutor)driver).executeScript("javascript:getReportData('view','"+(n+1)+"');");
							
							t = true;
							trlist = new ArrayList<WebElement>(driver.findElements(By.xpath("/html/body/span/table/tbody/tr/td/div/div/form/div[2]/table/tbody/tr")));
							break;
						}	
					} catch (Exception e) {
						
					}
					
					
					if(text.contains(ReservNo))
					{
						found = true;
						break;
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
					TakesScreenshot screen = (TakesScreenshot)driver;
					FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Profit and Loss Report Error.jpg"));
				}	
			}
			
			if(t && found || found)
			{
				break;
			}
		}
		
		
		if(found)
		{
			try
			{
				this.ReportLoaded	= true;
				String generalUse	= "";
				ArrayList<WebElement> tds = new ArrayList<WebElement>(trlist.get(c).findElements(By.tagName("td")));
				generalUse			= tds.get(7).getText().trim();
				this.BaseCurrency1	= generalUse;
				generalUse			= tds.get(7).getText();
				this.BaseCurrency2	= generalUse;
				generalUse			= tds.get(7).getText();
				this.BaseCurrency3	= generalUse;
				
				tds = new ArrayList<WebElement>(trlist.get(c).findElements(By.tagName("td")));
				try {
				generalUse			= tds.get(0).getText().trim();
				this.BookingNo		= generalUse;
				generalUse			= tds.get(1).getText().trim();
				this.Combination	= generalUse;
				generalUse			= tds.get(2).getText().trim();
				this.BookingType	= generalUse;
				generalUse			= tds.get(3).getText().trim();
				this.BookingChannel	= generalUse;
				generalUse			= tds.get(4).getText().trim();
				this.GuestCount		= generalUse;
				generalUse			= removeComma(tds.get(5).getText().trim());
				this.GrossBookingVal= generalUse;
				generalUse			= removeComma(tds.get(6).getText().trim());
				this.CostOfSales	= generalUse;
				generalUse			= removeComma(tds.get(7).getText().trim());
				this.InvoiceTotal	= generalUse;
				} catch (Exception e) {
					System.out.println();
				}
				
				
				tds = new ArrayList<WebElement>(trlist.get(c+1).findElements(By.tagName("td")));
				generalUse			= tds.get(0).getText();
				this.BookingDate	= generalUse;
				generalUse			= tds.get(1).getText();
				this.CommTemplate	= generalUse;
				generalUse			= tds.get(2).getText();
				this.AgentName		= generalUse;
				generalUse			= tds.get(3).getText();
				this.POSlocation	= generalUse;
				generalUse			= removeComma(tds.get(5).getText());
				this.AgentCommission= generalUse;
				generalUse			= removeComma(tds.get(6).getText());
				this.Profit			= generalUse;
				generalUse			= removeComma(tds.get(7).getText());
				this.InvoicePaid	= generalUse;
				
				tds = new ArrayList<WebElement>(trlist.get(c+2).findElements(By.tagName("td")));
				generalUse			= tds.get(0).getText();
				this.ConsultantName	= generalUse;
				generalUse			= tds.get(1).getText();
				this.ContractType	= generalUse;
				generalUse			= tds.get(3).getText();
				this.State			= generalUse;
				generalUse			= tds.get(4).getText();
				this.NightCount		= generalUse;
				generalUse			= removeComma(tds.get(5).getText());
				this.NetBookingVal	= generalUse;
				generalUse			= tds.get(6).getText();
				this.ProfitPerecent	= generalUse;
				generalUse			= tds.get(7).getText();
				this.InvBalanceDue	= generalUse;
			}
			catch(Exception e)
			{
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Profit and Loss Report Content Error.jpg"));
			}
		}
	}
	
	public String removeComma(String a)
	{
		if(a.contains(","))
		{
			a = a.replaceAll(",", "");
		}
		
		return a;
	}
	public String getBookingNo() {
		return BookingNo;
	}
	public void setBookingNo(String bookingNo) {
		BookingNo = bookingNo;
	}
	public String getBookingDate() {
		return BookingDate;
	}
	public void setBookingDate(String bookingDate) {
		BookingDate = bookingDate;
	}
	public String getConsultantName() {
		return ConsultantName;
	}
	public void setConsultantName(String consultantName) {
		ConsultantName = consultantName;
	}
	public String getCombination() {
		return Combination;
	}
	public void setCombination(String combination) {
		Combination = combination;
	}
	public String getCommTemplate() {
		return CommTemplate;
	}
	public void setCommTemplate(String commTemplate) {
		CommTemplate = commTemplate;
	}
	public String getContractType() {
		return ContractType;
	}
	public void setContractType(String contractType) {
		ContractType = contractType;
	}
	public String getBookingType() {
		return BookingType;
	}
	public void setBookingType(String bookingType) {
		BookingType = bookingType;
	}
	public String getAgentName() {
		return AgentName;
	}
	public void setAgentName(String agentName) {
		AgentName = agentName;
	}
	public String getBookingChannel() {
		return BookingChannel;
	}
	public void setBookingChannel(String bookingChannel) {
		BookingChannel = bookingChannel;
	}
	public String getPOSlocation() {
		return POSlocation;
	}
	public void setPOSlocation(String pOSlocation) {
		POSlocation = pOSlocation;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getGuestCount() {
		return GuestCount;
	}
	public void setGuestCount(String guestCount) {
		GuestCount = guestCount;
	}
	public String getCheckIn() {
		return CheckIn;
	}
	public void setCheckIn(String checkIn) {
		CheckIn = checkIn;
	}
	public String getCheckOut() {
		return CheckOut;
	}
	public void setCheckOut(String checkOut) {
		CheckOut = checkOut;
	}
	public String getNightCount() {
		return NightCount;
	}
	public void setNightCount(String nightCount) {
		NightCount = nightCount;
	}
	public String getBaseCurrency1() {
		return BaseCurrency1;
	}
	public void setBaseCurrency1(String baseCurrency1) {
		BaseCurrency1 = baseCurrency1;
	}
	public String getGrossBookingVal() {
		return GrossBookingVal;
	}
	public void setGrossBookingVal(String grossBookingVal) {
		GrossBookingVal = grossBookingVal;
	}
	public String getAgentCommission() {
		return AgentCommission;
	}
	public void setAgentCommission(String agentCommission) {
		AgentCommission = agentCommission;
	}
	public String getNetBookingVal() {
		return NetBookingVal;
	}
	public void setNetBookingVal(String netBookingVal) {
		NetBookingVal = netBookingVal;
	}
	public String getBaseCurrency2() {
		return BaseCurrency2;
	}
	public void setBaseCurrency2(String baseCurrency2) {
		BaseCurrency2 = baseCurrency2;
	}
	public String getCostOfSales() {
		return CostOfSales;
	}
	public void setCostOfSales(String costOfSales) {
		CostOfSales = costOfSales;
	}
	public String getProfit() {
		return Profit;
	}
	public void setProfit(String profit) {
		Profit = profit;
	}
	public String getProfitPerecent() {
		return ProfitPerecent;
	}
	public void setProfitPerecent(String profitPerecent) {
		ProfitPerecent = profitPerecent;
	}
	public String getBaseCurrency3() {
		return BaseCurrency3;
	}
	public void setBaseCurrency3(String baseCurrency3) {
		BaseCurrency3 = baseCurrency3;
	}
	public String getInvoiceTotal() {
		return InvoiceTotal;
	}
	public void setInvoiceTotal(String invoiceTotal) {
		InvoiceTotal = invoiceTotal;
	}
	public String getInvoicePaid() {
		return InvoicePaid;
	}
	public void setInvoicePaid(String invoicePaid) {
		InvoicePaid = invoicePaid;
	}
	public String getInvBalanceDue() {
		return InvBalanceDue;
	}
	public void setInvBalanceDue(String invBalanceDue) {
		InvBalanceDue = invBalanceDue;
	}
	public String getReservationNo() {
		return ReservationNo;
	}
	public void setReservationNo(String reservationNo) {
		ReservationNo = reservationNo;
	}
	public boolean isRecordExist() {
		return isRecordExist;
	}
	public void setRecordExist(boolean isRecordExist) {
		this.isRecordExist = isRecordExist;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public WebElement getSelectedelement() {
		return Selectedelement;
	}
	public void setSelectedelement(WebElement selectedelement) {
		Selectedelement = selectedelement;
	}
	public boolean isReportLoaded() {
		return ReportLoaded;
	}
	public void setReportLoaded(boolean reportLoaded) {
		ReportLoaded = reportLoaded;
	}

}
