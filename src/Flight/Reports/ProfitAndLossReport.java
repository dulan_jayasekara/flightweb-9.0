package Flight.Reports;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;



public class ProfitAndLossReport
{
	String		BookingNo			 = "";
	String		DocumentNo			 = "";
	String		BookingDate			 = "";
	String		TripType			 = "";
	String		ContactNo			 = "";
	
	String		outDeparture		 = "";
	String[]	outboundFlights		 = new String[10];
	String[]	outboundFlightNos	 = new String[10];
	String		outorigin			 = "";
	String		outDestination		 = "";
	String		outSeatClass		 = "";
	String		outairlineConfNo	 = "";
	
	String		inArrival			 = "";
	String[]    inboundFlights		 = new String[10];
	String[]    inboundFlightNos	 = new String[10];
	String		inorigin			 = "";
	String		inDestination		 = "";
	String		inSeatClass		 	 = "";
	String		inairlineConfNo		 = "";
	
	String		BookingChannel		 = "";
	String		SupplierName		 = "";
	
	//Traveler	traveler			 = new Traveler();
	String		GuestLName			 = "";
	String		GuestFName			 = "";
	String		GuestEmail			 = "";
	
	String		SupplierConfNo		 = "";
	String		TransactionID		 = "";
	
	String		AdultCount			 = "0";
	String		ChildCount			 = "0";
	String		SeniorCount			 = "0";
	String		InfantCount			 = "0";
	String		TicketTimeLimit		 = "";
 	 
	String		SellingCurrency		 = "";
	String		SellingBaseFare		 = "0";
	String		SellingTaxes		 = "0";
	String		SellingProcessFee	 = "0";
	String		SellingMarkup		 = "0";
	String		SellingAmount		 = "0";
 	 
	String		PortalBaseCurrency	 = "";
	String		PortalBaseFare		 = "0";
	String		PortalTaxes			 = "0";
	String		PortalProcessingFee  = "0";
	String		PortalMarkup		 = "0";
	String		PortalAmount		 = "0";
 	
	
	public static void main(String[] args)
	{
		ProfitAndLossReport report = new ProfitAndLossReport();
		report.getProfitLossReport("http://qav3.rezgateway.com/rezbase_v3");
	}
	
	public void getProfitLossReport(String Url/*, String ReservationNo, WebDriver driver*/)
	{
		String url = Url;
		String ReservationNo = "F3475W040914";/*ReservationNo*/
		
		if(url.contains("dev3") || url.contains("qav3"))
		{
			if(url.endsWith("3"))
			{
				url = url.concat("Reports");
			}
		}
		else if(url.contains("rezdemo2"));
		{
			if(url.endsWith("rezpackage"))
			{
				url = url.concat("Reports");
			}
		}
		
		url = url.concat("/reports/operational/ReservationReport.do?reportquerykeyname=reservationReport&format=view&searchby=reservationno&productType=air&reservationno=");
		url = url.concat(ReservationNo);
		
		FirefoxProfile pro    = new FirefoxProfile(new File("C:\\Users\\Sanoj\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\ujb8cwm6.default-1406265149085"));
		WebDriver      driver = new FirefoxDriver(pro);
		
		driver.get("http://qav3.rezgateway.com/rezbase_v3/admin/common/LoginPage.do");
		driver.findElement(By.id("user_id")).sendKeys("sanojC");
		driver.findElement(By.id("password")).sendKeys("123456");
		driver.findElement(By.id("loginbutton")).click();
		driver.get("http://qav3.rezgateway.com/rezbase_v3/reports/operational/mainReport.do?reportId=38&reportName=Profit%20and%20Loss%20Report");
		driver.get(url);
		
		
		String general = "";
		System.out.println("http://qav3.rezgateway.com/rezbase_v3Reports/reports/operational/ReservationReport.do?reportquerykeyname=reservationReport&format=view&searchby=reservationno&productType=air&reservationno=F3475W040914");
		System.out.println(url);
		SimpleDateFormat sdf1	= new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat sdf2	= new SimpleDateFormat("yyyy-MM-dd");
		
		
		//--------------------START EXTRACTING DATA------------------
		
		ArrayList<WebElement> row1TD = new ArrayList<WebElement>(driver.findElement(By.id("RowNo_1_1")).findElements(By.tagName("td")));
		
		general = row1TD.get(2).getText().trim();
		this.BookingNo = general;
		
		general = row1TD.get(3).getText().trim();
		Date outdate;
		
		try {
			outdate		= sdf1.parse(general);
			general		= sdf2.format(outdate);
			this.outDeparture = general;
		}
		catch (ParseException e) {
			e.printStackTrace();
		}
		
		general = row1TD.get(4).getText().trim();
		Date indate;
		try {
			indate		= sdf1.parse(general);
			general		= sdf2.format(indate);
			this.inArrival = general;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		general = row1TD.get(5).getText().trim();
		this.GuestLName = general.split(" ")[0];
		this.GuestFName = general.split(" ")[1];
		
		general = row1TD.get(6).getText().trim();
		this.AdultCount = general;
		
		general = row1TD.get(7).getText().trim();
		this.SellingCurrency = general;
		
		general = row1TD.get(8).getText().trim();
		this.PortalBaseCurrency = general;
		
		
		ArrayList<WebElement> row2TD = new ArrayList<WebElement>(driver.findElement(By.id("RowNo_1_2")).findElements(By.tagName("td")));
		
		general = row2TD.get(0).getText().trim();
		this.DocumentNo = general;
		
		general = row2TD.get(3).getText().trim();
		this.ChildCount = general;
		
		general = row2TD.get(4).getText().trim();
		this.SellingBaseFare = general;
		
		general = row2TD.get(5).getText().trim();
		this.PortalBaseFare = general;
		
		
		
		ArrayList<WebElement> row3TD = new ArrayList<WebElement>(driver.findElement(By.id("RowNo_1_3")).findElements(By.tagName("td")));
		
		general = row3TD.get(0).getText().trim();
		this.BookingDate = general;
		
		general = row3TD.get(1).getText().trim();
		String[] out = general.split("-");
		String[] outflightnames = new String[10];
		String[] outflightnos   = new String[10];
		if(out[0].contains(","))
		{
			outflightnames = out[0].split(",");
		}
		else
		{
			outflightnames[0] = out[0];
		}
		
		if(out[1].contains(","))
		{
			outflightnos = out[1].split(",");
		}
		else
		{
			outflightnos[0] = out[1];
		}
		
		outboundFlights		= outflightnames;
		outboundFlightNos	= outflightnos;
		
		general = row3TD.get(2).getText().trim();
		String[] in = general.split("-");
		String[] inflightnames  = new String[10];
		String[] inflightnos    = new String[10];
		if(in[0].contains(","))
		{
			inflightnames = in[0].split(",");
		}
		else
		{
			inflightnames[0] = in[0];
		}
		
		if(in[1].contains(","))
		{
			inflightnos = in[1].split(",");
		}
		else
		{
			inflightnos[0] = in[1];
		}
		
		inboundFlights		= inflightnames;
		inboundFlightNos	= inflightnos;
		
		general = row3TD.get(3).getText().trim();
		this.GuestEmail = general;
		
		general = row3TD.get(4).getText().trim();
		this.SeniorCount = general;
		
		general = row3TD.get(5).getText().trim();
		this.SellingTaxes = general;
		
		general = row3TD.get(6).getText().trim();
		this.PortalTaxes = general;
		
		
		ArrayList<WebElement> row4TD = new ArrayList<WebElement>(driver.findElement(By.id("RowNo_1_4")).findElements(By.tagName("td")));
		
		general = row4TD.get(0).getText().trim();
		this.TripType = general;
		
		general = row4TD.get(1).getText().trim();
		this.outorigin		= general.split(" ")[0];
		this.outDestination = general.split(" ")[1];
		
		general = row4TD.get(2).getText().trim();
		this.inorigin		= general.split(" ")[0];
		this.inDestination	= general.split(" ")[1];
		
		/*general = row4TD.get(3).getText().trim();
		this.SupplierConfNo = general;*/
		
		general = row4TD.get(4).getText().trim();
		this.InfantCount = general;
		
		general = row4TD.get(5).getText().trim();
		this.SellingProcessFee = general;
		
		general = row4TD.get(6).getText().trim();
		this.PortalProcessingFee = general;
		
		
		
		ArrayList<WebElement> row5TD = new ArrayList<WebElement>(driver.findElement(By.id("RowNo_1_5")).findElements(By.tagName("td")));
		
		general = row5TD.get(0).getText().trim();
		this.ContactNo = general;
		
		//OUTBOUND AIRCONF# SKIPPED(1)
		//INBOUND AIRCONF# SKIPPED(2)
		
		general = row5TD.get(3).getText().trim();
		this.TransactionID = general;
		
		general = row5TD.get(4).getText().trim();
		this.TicketTimeLimit = general;
		
		general = row5TD.get(5).getText().trim();
		this.SellingMarkup = general;
		
		general = row5TD.get(6).getText().trim();
		this.PortalMarkup = general;
		
		
		ArrayList<WebElement> row6TD = new ArrayList<WebElement>(driver.findElement(By.id("RowNo_1_6")).findElements(By.tagName("td")));
		
		general = row6TD.get(1).getText().trim();
		this.BookingChannel = general;
		
		general = row6TD.get(2).getText().trim();
		this.SupplierName = general;
		
		general = row6TD.get(3).getText().trim();
		this.SupplierConfNo = general;
		
		general = row6TD.get(5).getText().trim();
		this.SellingAmount = general;
		
		general = row6TD.get(6).getText().trim();
		this.PortalAmount = general;
	
	}
}
