package Flight.Reports;



import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;


public class SupplierPayableReport {
	
private WebDriver    driver                           ;
private boolean      isReportLoded                    ;	
private boolean      isPaid                           ;	
private boolean      isRecordExist                    ;

private String       ReservationNumber           = "-";
private String		 ReservationNo				 = ""; //Validated
private String       DocumentNumber              = "-";
private String       BookingDate                 = "-";//Validated
private String       BookingChannel              = "-";//Validated

private String       ProductType                 = "-";//Validated
private String       ProductName                 = "-";//Validated
private String       ServiceElementDate          = "-";

private String       SupplierName                = "-";//Validated
private String       SupplierConf                = "-";//Validated
private String       BookingStatus               = "-";//Validated

private String       GuestName                   = "-";//Validated

private String       PortalCu_PayableAmount 	 = "0";//Before pay Validated, After pay Validated
private String       PortalCu_TotalPaid 	     = "0";//Before pay Validated, After pay Validated
private String       PortalCu_Balance  	         = "0";//Before pay Validated, After pay Validated

private String       SupCu_Type 	             = "-";//Before pay Validated
private String       SupCu_Payable	             = "0";//Before pay Validated, After pay Validated
private String       SupCu_TotaPaid              = "0";//Before pay Validated, After pay Validated
private String       SupCu_Balance               = "0";//Before pay Validated, After pay Validated

private String       PaymentHistory              = "-";//Validated

private WebElement   Selectedelement             = null;
HashMap<String, String>	Propertymap				 = new HashMap<String, String>();

/*First Create object of  supplier_payble in Validation Class then call 
 * getpaybledetails method then validate with initially aquired data.
 * As the next step call PaySupplier with amount then again call getpaybledetails and validate.
 */


public SupplierPayableReport(HashMap<String, String> Propymap)
{
	Propertymap = Propymap;	
}


public boolean loadReport(WebDriver driver, String Supplier)
{
	try
	{
		driver.get(Propertymap.get("Portal.Url").concat("/reports/operational/mainReport.do?reportId=36&reportName=Third Party Supplier Payable Report"));
		driver.switchTo().frame("reportIframe");
		new Select(driver.findElement(By.xpath("/html/body/span/table/tbody/tr/td/div/div/form/div/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/span/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByValue("today");
		driver.findElement(By.id("supplierName")).clear();
		driver.findElement(By.id("supplierName")).sendKeys(Supplier);
		driver.findElement(By.id("supplierName_lkup")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("lookup");
		//ArrayList<WebElement> list = new ArrayList<WebElement>();
		for(int y = 0; y<10; y++)
		{
			try {
				String supp = "";
				supp = driver.findElement(By.id("row-"+y+"")).getText().trim();
				if(supp.equalsIgnoreCase(Supplier))
				{
					((JavascriptExecutor)driver).executeScript("javascript:selectedRecord("+y+",'');");
					break;
				}
			} catch (Exception e) {
				
			}
			
		}
		
		//((JavascriptExecutor)driver).executeScript("selectedRecord(0,'loadAutoFillSelect();');");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		//new Select(driver.findElement(By.id(""))).selectByValue("today"); 
	    ((JavascriptExecutor)driver).executeScript("getReportData('view');");
	    
	    try
	    {
			driver.findElement(By.name("paymentMethod"));
			return true;
		}
	    catch (Exception e)
	    {
			return false;
		} 
	}
	catch (Exception e)
	{
		return false;
	}
}


public void  getPaybleDetails(WebDriver driver, String Supplier, String ReservationNumber)
{
	int		index		= 0;

	String	supplier	= "";
	supplier			= Supplier/*Propertymap.get("")*/;
	if(this.loadReport(driver, supplier))
	{
		this.isReportLoded = true;
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		try
		{
			forloop:for (int i = 1; i < 15; i++) 
			{
				WebElement  CurrentElement  = driver.findElement(By.id("RowNo_"+i+"_1"));
				ReservationNo   			= CurrentElement.findElements(By.tagName("td")).get(1).getText().trim();
				System.out.println(ReservationNo);
				//System.out.println(ReservationNumber);
				//System.out.println("------------------");
				
				if(ReservationNumber.contains(ReservationNo) || ReservationNo.contains(ReservationNumber))
				{
					this.isRecordExist = true;
					index              = i;
				    Selectedelement    = CurrentElement;
					break  forloop;
				}
				
			}
		}
		catch(Exception e)
		{
			
		}
		
		
		if(this.isRecordExist)
		{
			
			this.ProductType               = Selectedelement.findElements(By.tagName("td")).get(2).getText().trim();
			this.SupplierName              = Selectedelement.findElements(By.tagName("td")).get(3).getText().trim();
			this.GuestName                 = Selectedelement.findElements(By.tagName("td")).get(4).getText().trim();
			this.PortalCu_PayableAmount    = Selectedelement.findElements(By.tagName("td")).get(5).getText().trim();
			this.SupCu_Payable             = Selectedelement.findElements(By.tagName("td")).get(6).getText().trim();
			
			WebElement element1            = driver.findElement(By.id("RowNo_"+index+"_2"));
			
			this.DocumentNumber            = element1.findElements(By.tagName("td")).get(0).getText().trim();
			this.ProductName               = element1.findElements(By.tagName("td")).get(1).getText().trim();
			this.SupplierConf              = element1.findElements(By.tagName("td")).get(2).getText().trim();
			this.PortalCu_TotalPaid        = element1.findElements(By.tagName("td")).get(3).getText().trim();
			this.SupCu_TotaPaid            = element1.findElements(By.tagName("td")).get(4).getText().trim();
			
			WebElement element2            = driver.findElement(By.id("RowNo_"+index+"_3"));
			
			this.BookingDate               = element2.findElements(By.tagName("td")).get(0).getText().trim();
			this.ServiceElementDate        = element2.findElements(By.tagName("td")).get(1).getText().trim();
			this.BookingStatus             = element2.findElements(By.tagName("td")).get(2).getText().trim();
			this.PortalCu_Balance          = element2.findElements(By.tagName("td")).get(3).getText().trim();
			this.SupCu_Balance             = element2.findElements(By.tagName("td")).get(4).getText().trim();
			
			
		}
		
	}
	else 
	{
		this.isReportLoded = false;
	}
}


public void paySupplier(String ref)
{
	//Selectedelement.findElements(By.tagName("td")).get(8).click();
	Selectedelement.findElements(By.tagName("td")).get(8).findElement(By.tagName("input")).click();
	driver.findElement(By.id("payReferNo")).clear();
	driver.findElement(By.id("payReferNo")).sendKeys(ref);
	((JavascriptExecutor)driver).executeScript("JavaScript:getConfirmList();");
	
	try
	{
		this.PaymentHistory   = Selectedelement.findElements(By.tagName("td")).get(9).getText();
	}
	catch (Exception e)
	{
		
	}
	//JavaScript:getConfirmList();
	//System.out.println(this.PaymentHistory);
}


public String getPaymentHistory() {
	return PaymentHistory;
}



public WebDriver getDriver() {
	return driver;
}



public boolean isReportLoded() {
	return isReportLoded;
}



public boolean isPaid() {
	return isPaid;
}



public boolean isRecordExist() {
	return isRecordExist;
}



public String getReservationNumber() {
	return ReservationNumber;
}



public String getDocumentNumber() {
	return DocumentNumber;
}



public String getBookingDate() {
	return BookingDate;
}



public String getBookingChannel() {
	return BookingChannel;
}



public String getProductType() {
	return ProductType;
}



public String getProductName() {
	return ProductName;
}



public String getServiceElementDate() {
	return ServiceElementDate;
}



public String getSupplierName() {
	return SupplierName;
}



public String getSupplierConf() {
	return SupplierConf;
}



public String getBookingStatus() {
	return BookingStatus;
}



public String getGuestName() {
	return GuestName;
}



public String getPortalCu_PayableAmount() {
	return PortalCu_PayableAmount;
}



public String getPortalCu_TotalPaid() {
	return PortalCu_TotalPaid;
}



public String getPortalCu_Balance() {
	return PortalCu_Balance;
}



public String getSupCu_Type() {
	return SupCu_Type;
}



public String getSupCu_Payable() {
	return SupCu_Payable;
}



public String getSupCu_TotaPaid() {
	return SupCu_TotaPaid;
}



public String getSupCu_Balance() {
	return SupCu_Balance;
}


public String getReservationNo() {
	return ReservationNo;
}


public void setReservationNo(String reservationNo) {
	ReservationNo = reservationNo;
}


public WebElement getSelectedelement() {
	return Selectedelement;
}


public void setSelectedelement(WebElement selectedelement) {
	Selectedelement = selectedelement;
}


public void setDriver(WebDriver driver) {
	this.driver = driver;
}


public void setReportLoded(boolean isReportLoded) {
	this.isReportLoded = isReportLoded;
}


public void setPaid(boolean isPaid) {
	this.isPaid = isPaid;
}


public void setRecordExist(boolean isRecordExist) {
	this.isRecordExist = isRecordExist;
}


public void setReservationNumber(String reservationNumber) {
	ReservationNumber = reservationNumber;
}


public void setDocumentNumber(String documentNumber) {
	DocumentNumber = documentNumber;
}


public void setBookingDate(String bookingDate) {
	BookingDate = bookingDate;
}


public void setBookingChannel(String bookingChannel) {
	BookingChannel = bookingChannel;
}


public void setProductType(String productType) {
	ProductType = productType;
}


public void setProductName(String productName) {
	ProductName = productName;
}


public void setServiceElementDate(String serviceElementDate) {
	ServiceElementDate = serviceElementDate;
}


public void setSupplierName(String supplierName) {
	SupplierName = supplierName;
}


public void setSupplierConf(String supplierConf) {
	SupplierConf = supplierConf;
}


public void setBookingStatus(String bookingStatus) {
	BookingStatus = bookingStatus;
}


public void setGuestName(String guestName) {
	GuestName = guestName;
}


public void setPortalCu_PayableAmount(String portalCu_PayableAmount) {
	PortalCu_PayableAmount = portalCu_PayableAmount;
}


public void setPortalCu_TotalPaid(String portalCu_TotalPaid) {
	PortalCu_TotalPaid = portalCu_TotalPaid;
}


public void setPortalCu_Balance(String portalCu_Balance) {
	PortalCu_Balance = portalCu_Balance;
}


public void setSupCu_Type(String supCu_Type) {
	SupCu_Type = supCu_Type;
}


public void setSupCu_Payable(String supCu_Payable) {
	SupCu_Payable = supCu_Payable;
}


public void setSupCu_TotaPaid(String supCu_TotaPaid) {
	SupCu_TotaPaid = supCu_TotaPaid;
}


public void setSupCu_Balance(String supCu_Balance) {
	SupCu_Balance = supCu_Balance;
}


public void setPaymentHistory(String paymentHistory) {
	PaymentHistory = paymentHistory;
}

}
