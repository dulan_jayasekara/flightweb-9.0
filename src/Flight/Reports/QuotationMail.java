package Flight.Reports;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.common.Validators.CommonValidator;

import system.classes.*;

public class QuotationMail {

	String address					= "";
	String tel						= "";
	String fax						= "";
	String email					= "";
	String website					= "";
	String quotationNo				= ""; 
	String correctNote				= "(THIS IS NOT A BOOKING CONFIRMATION)";
	String note						= "";
	String dear						= "";
	ArrayList<Flight> outbound		= new ArrayList<Flight>();
	ArrayList<Flight> inbound		= new ArrayList<Flight>();
	String adultrate 				= "0";
	String childrate 				= "0";
	String infantrate 				= "0";
	String adultcount 				= "0";
	String childcount				= "0";
	String infantcount				= "0";
	String adultrateTot				= "0";
	String childrateTot				= "0";
	String infantrateTot			= "0";
	String totalbeforetax			= "0";
	String totaltaxandothercharges1	= "0";
	String totalamount				= "0";
	String subtotal					= "0";
	String totaltaxandothercharges2	= "0";
	String totalBookingvalue		= "0";
	String amountchargeable			= "0";
	String processbyairline			= "0";
	String currency					= "-";
	String orngtotbookingval		= "0";
	String orngamountchargeable		= "0";
	String orngprocessbyairline 	= "0";
	Traveler customer				= new Traveler();
	
	
	public void setQuotation(WebDriver driver)
	{
		ArrayList<WebElement> tr = new ArrayList<WebElement>(driver.findElements(By.xpath("html/body/table/tbody/tr")));
		
		ArrayList<WebElement> tronetd = new ArrayList<WebElement>(tr.get(0).findElements(By.tagName("td")));
		String strtronetd	= "";
		strtronetd			= tronetd.get(5).getText();
		String[] split		= strtronetd.split("\\n");
		address = split[0];
		tel		= split[1];
		fax		= split[2];
		email	= split[3];
		website	= split[4];
		
		quotationNo	= tr.get(1).getText().trim();
		note	= tr.get(2).getText().trim();
		dear	= tr.get(3).getText().trim();
		
		
		int j = 0;
		String h = "";
		boolean fgoing = false;
		for(j=0; j<tr.size(); j++)
		{
			h = tr.get(j).findElements(By.tagName("td")).get(0).getText().trim();
			if(h.contains("Flight Outbound Info"))
			{
				fgoing = true;
				break;
			}
		}
		if(fgoing)
		{
			ArrayList<WebElement> tr12tr = new ArrayList<WebElement>(tr.get(j+1).findElements(By.tagName("tr")));
			int length = 0;
			length	= tr12tr.size();
			for(int i=0; i<length; i+=4)
			{
				String name = "";
				name = tr12tr.get(i).findElements(By.tagName("td")).get(1).getText().trim();
				if(name.equalsIgnoreCase("Depart"))
				{	
					Flight flight = new Flight();
					String depdate = "-";
					depdate = tr12tr.get(i).findElements(By.tagName("td")).get(2).getText().trim();
					try {
						String[] dA			= depdate.trim().split(" ");
						String dt			= "";
						if( dA[0].contains("th") )
						{
							dt			= dA[0].replace("th", "");
						}
						else if(dA[0].contains("rd"))
						{
							dt			= dA[0].replace("rd", "");
						}
						else if(dA[0].contains("st"))
						{
							dt			= dA[0].replace("st", "");
						}
						else if(dA[0].contains("nd"))
						{
							dt			= dA[0].replace("nd", "");
						}
						depdate = dt.trim().concat("-").concat(dA[1].trim().concat("-").concat(dA[2].trim()));
						depdate = CommonValidator.formatDateToCommon(depdate, "dd-MMM-yyyy");
					} catch (Exception e) {
						
					}
					flight.setDepartureDate(depdate);
					
					String depport = "-";
					depport = tr12tr.get(i).findElements(By.tagName("td")).get(3).getText().trim();
					flight.setDeparture_port(depport);
					
					String marketairline = "-";
					marketairline = tr12tr.get(i).findElements(By.tagName("td")).get(4).getText().trim();
					flight.setMarketingAirline(marketairline);
					
					String seatclass = "-";
					seatclass = tr12tr.get(i).findElements(By.tagName("td")).get(6).getText().trim();
					flight.setCabintype(seatclass);
					
					//tr+1
					String deptime = "-";
					deptime = tr12tr.get(i+1).findElements(By.tagName("td")).get(0).getText().trim();
					deptime = CommonValidator.formatTimeToCommon(deptime);
					flight.setDepartureTime(deptime);
					
					String deploc = "-";
					deploc = tr12tr.get(i+1).findElements(By.tagName("td")).get(1).getText().trim();
					flight.setDepartureLocationCode(deploc);
					
					String flightNo = "-";
					flightNo = tr12tr.get(i+1).findElements(By.tagName("td")).get(2).getText().trim();
					flight.setFlightNo(flightNo);
					
					String fareclass = "-";
					fareclass = tr12tr.get(i+1).findElements(By.tagName("td")).get(4).getText().trim();
					flight.setFareclass(fareclass);
					
					String arrivedate = "-";
					arrivedate = tr12tr.get(i+2).findElements(By.tagName("td")).get(2).getText().trim();
					try {
						String[] dA			= arrivedate.trim().split(" ");
						String dt			= "";
						if( dA[0].contains("th") )
						{
							dt			= dA[0].replace("th", "");
						}
						else if(dA[0].contains("rd"))
						{
							dt			= dA[0].replace("rd", "");
						}
						else if(dA[0].contains("st"))
						{
							dt			= dA[0].replace("st", "");
						}
						else if(dA[0].contains("nd"))
						{
							dt			= dA[0].replace("nd", "");
						}
						arrivedate = dt.trim().concat("-").concat(dA[1].trim().concat("-").concat(dA[2].trim()));
						arrivedate = CommonValidator.formatDateToCommon(arrivedate, "dd-MMM-yyyy");
					} catch (Exception e) {
						
					}
					flight.setArrivalDate(arrivedate);
					
					String arriveport = "-";
					arriveport = tr12tr.get(i+2).findElements(By.tagName("td")).get(3).getText().trim();
					flight.setArrival_port(arriveport);
					
					String arrivetime = "-";
					arrivetime = tr12tr.get(i+3).findElements(By.tagName("td")).get(0).getText().trim();
					arrivetime = CommonValidator.formatTimeToCommon(arrivetime);
					flight.setArrivalTime(arrivetime);
					
					String arriveloc = "-";
					arriveloc = tr12tr.get(i+3).findElements(By.tagName("td")).get(1).getText().trim();
					flight.setArrivalLocationCode(arriveloc);
					
					outbound.add(flight);
				}
			}
			
		}
		
		int q = 0;
		String r = "";
		boolean freturn = false;
		for(q=0; q<tr.size(); q++)
		{
			r = tr.get(q).findElements(By.tagName("td")).get(0).getText().trim();
			if(r.contains("Flight Return Info"))
			{
				freturn = true;
				break;
			}
		}
			
		if(freturn)
		{
			ArrayList<WebElement> tr16tr = new ArrayList<WebElement>(tr.get(q+1).findElements(By.tagName("tr")));
			int length2 = 0;
			length2	= tr16tr.size();
			for(int y=0; y<length2; y+=4)
			{
				String name2 = "";
				name2 = tr16tr.get(y).findElements(By.tagName("td")).get(1).getText().trim();
				if(name2.equalsIgnoreCase("Depart"))
				{	
					Flight flight = new Flight();
					String depdate = "-";
					depdate = tr16tr.get(y).findElements(By.tagName("td")).get(2).getText().trim();
					try {
						String[] dA			= depdate.trim().split(" ");
						String dt			= "";
						if( dA[0].contains("th") )
						{
							dt			= dA[0].replace("th", "");
						}
						else if(dA[0].contains("rd"))
						{
							dt			= dA[0].replace("rd", "");
						}
						else if(dA[0].contains("st"))
						{
							dt			= dA[0].replace("st", "");
						}
						else if(dA[0].contains("nd"))
						{
							dt			= dA[0].replace("nd", "");
						}
						depdate = dt.trim().concat("-").concat(dA[1].trim().concat("-").concat(dA[2].trim()));
						depdate = CommonValidator.formatDateToCommon(depdate, "dd-MMM-yyyy");
					} catch (Exception e) {
						
					}
					flight.setDepartureDate(depdate);
						
					String depport = "-";
					depport = tr16tr.get(y).findElements(By.tagName("td")).get(3).getText().trim();
					flight.setDeparture_port(depport);
						
					String marketairline = "-";
					marketairline = tr16tr.get(y).findElements(By.tagName("td")).get(4).getText().trim();
					flight.setMarketingAirline(marketairline);
						
					String seatclass = "-";
					seatclass = tr16tr.get(y).findElements(By.tagName("td")).get(6).getText().trim();
					flight.setCabintype(seatclass);
						
					//tr+1
					String deptime = "-";
					deptime = tr16tr.get(y+1).findElements(By.tagName("td")).get(0).getText().trim();
					deptime = CommonValidator.formatTimeToCommon(deptime);
					flight.setDepartureTime(deptime);
						
					String deploc = "-";
					deploc = tr16tr.get(y+1).findElements(By.tagName("td")).get(1).getText().trim();
					flight.setDepartureLocationCode(deploc);
						
					String flightNo = "-";
					flightNo = tr16tr.get(y+1).findElements(By.tagName("td")).get(2).getText().trim();
					flight.setFlightNo(flightNo);
						
					String fareclass = "-";
					fareclass = tr16tr.get(y+1).findElements(By.tagName("td")).get(4).getText().trim();
					flight.setFareclass(fareclass);
						
					String arrivedate = "-";
					arrivedate = tr16tr.get(y+2).findElements(By.tagName("td")).get(2).getText().trim();
					try {
						String[] dA			= arrivedate.trim().split(" ");
						String dt			= "";
						if( dA[0].contains("th") )
						{
							dt			= dA[0].replace("th", "");
						}
						else if(dA[0].contains("rd"))
						{
							dt			= dA[0].replace("rd", "");
						}
						else if(dA[0].contains("st"))
						{
							dt			= dA[0].replace("st", "");
						}
						else if(dA[0].contains("nd"))
						{
							dt			= dA[0].replace("nd", "");
						}
						arrivedate = dt.trim().concat("-").concat(dA[1].trim().concat("-").concat(dA[2].trim()));
						arrivedate = CommonValidator.formatDateToCommon(arrivedate, "dd-MMM-yyyy");
					} catch (Exception e) {
						
					}
					flight.setArrivalDate(arrivedate);
						
					String arriveport = "-";
					arriveport = tr16tr.get(y+2).findElements(By.tagName("td")).get(3).getText().trim();
					flight.setArrival_port(arriveport);
						
					String arrivetime = "-";
					arrivetime = tr16tr.get(y+3).findElements(By.tagName("td")).get(0).getText().trim();
					arrivetime = CommonValidator.formatTimeToCommon(arrivetime);
					flight.setArrivalTime(arrivetime);
						
					String arriveloc = "-";
					arriveloc = tr16tr.get(y+3).findElements(By.tagName("td")).get(1).getText().trim();
					flight.setArrivalLocationCode(arriveloc);
						
					inbound.add(flight);
				}
			}
		}
		
		q = 0;
		r = "";
		/*boolean adults = false;
		boolean children = false;
		boolean infant = false;*/
		for(q=0; q<tr.size(); q++)
		{
			try {
				r = tr.get(q).findElements(By.tagName("td")).get(0).getText().trim();
			} catch (Exception e) {
				
			}
			
			if(r.contains("Adult"))
			{
				adultrate		= tr.get(q).findElements(By.tagName("td")).get(1).getText().trim();
				adultcount		= tr.get(q).findElements(By.tagName("td")).get(2).getText().trim();
				adultrateTot	= tr.get(q).findElements(By.tagName("td")).get(7).getText().trim();
			}
			if(r.contains("Children"))
			{
				childrate		= tr.get(q).findElements(By.tagName("td")).get(1).getText().trim();
				childcount		= tr.get(q).findElements(By.tagName("td")).get(2).getText().trim();
				childrateTot	= tr.get(q).findElements(By.tagName("td")).get(7).getText().trim();
			}
			if(r.contains("Infants"))
			{
				infantrate		= tr.get(q).findElements(By.tagName("td")).get(1).getText().trim();
				infantcount		= tr.get(q).findElements(By.tagName("td")).get(2).getText().trim();
				infantrateTot	= tr.get(q).findElements(By.tagName("td")).get(7).getText().trim();
			}
		}
		
		//Cost breakdown
		//q = 0;
		int o=0;
		r = "";
		for(o=0; o<tr.size(); o++)
		{
			try {
				r = tr.get(o).findElements(By.tagName("td")).get(6).findElements(By.tagName("tbody")).get(0).findElements(By.tagName("tr")).get(0).getText();
			} catch (Exception e) {
				
			}
			
			if( r.contains("Total Before Taxes") || r.contains("Total") )
			{
				break;
			}
		}
		try {
			ArrayList<WebElement> costtr = new ArrayList<WebElement>(tr.get(o).findElements(By.tagName("td")).get(6).findElements(By.tagName("tbody")).get(0).findElements(By.tagName("tr")));
			totalbeforetax = costtr.get(0).findElements(By.tagName("td")).get(1).getText().trim();
			totaltaxandothercharges1 = costtr.get(1).findElements(By.tagName("td")).get(1).getText().trim();
			totalamount = costtr.get(2).findElements(By.tagName("td")).get(1).getText();
			currency = costtr.get(4).findElements(By.tagName("td")).get(1).getText();
			subtotal = costtr.get(5).findElements(By.tagName("td")).get(1).getText();
			totaltaxandothercharges2 = costtr.get(6).findElements(By.tagName("td")).get(1).getText();
		} catch (Exception e) {
			
		}
		
		//q = 0;
		r = "";
		int k=0;
		for(k=o; k<tr.size(); k++)
		{
			try {
				r = tr.get(k).findElements(By.tagName("td")).get(6).findElements(By.tagName("tbody")).get(0).findElements(By.tagName("tr")).get(1).findElements(By.tagName("td")).get(0).getText();
			} catch (Exception e) {
				
			}
			
			if( r.contains("Total Booking Value") || r.contains("Total") )
			{
				break;
			}
		}
		try {
			orngtotbookingval		= tr.get(k).findElements(By.tagName("td")).get(6).findElements(By.tagName("tbody")).get(0).findElements(By.tagName("tr")).get(1).findElements(By.tagName("td")).get(1).getText();
		} catch (Exception e) {
			
		}
		try {
			orngamountchargeable	= tr.get(k).findElements(By.tagName("td")).get(6).findElements(By.tagName("tbody")).get(0).findElements(By.tagName("tr")).get(2).findElements(By.tagName("td")).get(1).getText();
		} catch (Exception e) {
			
		}
		try {
			orngprocessbyairline	= tr.get(k).findElements(By.tagName("td")).get(6).findElements(By.tagName("tbody")).get(0).findElements(By.tagName("tr")).get(3).findElements(By.tagName("td")).get(1).getText();
		} catch (Exception e) {
			
		}
		
		//CUSTOMER DETAILS
		int m = 0;
		r = "";
		for(m=k; m<tr.size(); m++)
		{
			try {
				r = tr.get(m).findElements(By.tagName("td")).get(0).getText();
			} catch (Exception e) {
				
			}
			
			if( r.contains("Customer Details") )
			{
				break;
			}
		}
		String str = "-";
		str = tr.get(m+1).findElements(By.tagName("td")).get(1).getText().trim();
		customer.setGivenName(str);
		str = tr.get(m+1).findElements(By.tagName("td")).get(7).getText().trim();
		Address adrs = new Address();
		adrs.setAddressCountry(str);
		str = tr.get(m+2).findElements(By.tagName("td")).get(1).getText().trim();
		customer.setSurname(str);
		str = tr.get(m+2).findElements(By.tagName("td")).get(7).getText().trim();
		customer.setPhoneNumber(str);
		str = tr.get(m+3).findElements(By.tagName("td")).get(1).getText().trim();
		adrs.setAddressStreetNo(str);
		str = tr.get(m+3).findElements(By.tagName("td")).get(7).getText().trim();
		customer.setEmergencyNo(str);
		str = tr.get(m+4).findElements(By.tagName("td")).get(1).getText().trim();
		adrs.setAddressStreetNo2(str);
		str = tr.get(m+4).findElements(By.tagName("td")).get(7).getText().trim();
		customer.setEmail(str);
		str = tr.get(m+5).findElements(By.tagName("td")).get(1).getText().trim();
		adrs.setAddressCity(str);
		customer.setAddress(adrs);
		
		
	}//method end
	
	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getTel() {
		return tel;
	}


	public void setTel(String tel) {
		this.tel = tel;
	}


	public String getFax() {
		return fax;
	}


	public void setFax(String fax) {
		this.fax = fax;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getWebsite() {
		return website;
	}


	public void setWebsite(String website) {
		this.website = website;
	}


	public String getQuotationNo() {
		return quotationNo;
	}


	public void setQuotationNo(String quotationNo) {
		this.quotationNo = quotationNo;
	}


	public String getCorrectNote() {
		return correctNote;
	}


	public void setCorrectNote(String correctNote) {
		this.correctNote = correctNote;
	}


	public String getNote() {
		return note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public String getDear() {
		return dear;
	}


	public void setDear(String dear) {
		this.dear = dear;
	}


	public ArrayList<Flight> getOutbound() {
		return outbound;
	}


	public void setOutbound(ArrayList<Flight> outbound) {
		this.outbound = outbound;
	}


	public ArrayList<Flight> getInbound() {
		return inbound;
	}


	public void setInbound(ArrayList<Flight> inbound) {
		this.inbound = inbound;
	}


	public String getAdultrate() {
		return adultrate;
	}


	public void setAdultrate(String adultrate) {
		this.adultrate = adultrate;
	}


	public String getChildrate() {
		return childrate;
	}


	public void setChildrate(String childrate) {
		this.childrate = childrate;
	}


	public String getInfantrate() {
		return infantrate;
	}


	public void setInfantrate(String infantrate) {
		this.infantrate = infantrate;
	}


	public String getAdultcount() {
		return adultcount;
	}


	public void setAdultcount(String adultcount) {
		this.adultcount = adultcount;
	}


	public String getChildcount() {
		return childcount;
	}


	public void setChildcount(String childcount) {
		this.childcount = childcount;
	}


	public String getInfantcount() {
		return infantcount;
	}


	public void setInfantcount(String infantcount) {
		this.infantcount = infantcount;
	}


	public String getAdultrateTot() {
		return adultrateTot;
	}


	public void setAdultrateTot(String adultrateTot) {
		this.adultrateTot = adultrateTot;
	}


	public String getChildrateTot() {
		return childrateTot;
	}


	public void setChildrateTot(String childrateTot) {
		this.childrateTot = childrateTot;
	}


	public String getInfantrateTot() {
		return infantrateTot;
	}


	public void setInfantrateTot(String infantrateTot) {
		this.infantrateTot = infantrateTot;
	}


	public String getTotalbeforetax() {
		return totalbeforetax;
	}


	public void setTotalbeforetax(String totalbeforetax) {
		this.totalbeforetax = totalbeforetax;
	}


	public String getTotaltaxandothercharges1() {
		return totaltaxandothercharges1;
	}


	public void setTotaltaxandothercharges1(String totaltaxandothercharges1) {
		this.totaltaxandothercharges1 = totaltaxandothercharges1;
	}


	public String getTotalamount() {
		return totalamount;
	}


	public void setTotalamount(String totalamount) {
		this.totalamount = totalamount;
	}


	public String getSubtotal() {
		return subtotal;
	}


	public void setSubtotal(String subtotal) {
		this.subtotal = subtotal;
	}


	public String getTotaltaxandothercharges2() {
		return totaltaxandothercharges2;
	}


	public void setTotaltaxandothercharges2(String totaltaxandothercharges2) {
		this.totaltaxandothercharges2 = totaltaxandothercharges2;
	}


	public String getTotalBookingvalue() {
		return totalBookingvalue;
	}


	public void setTotalBookingvalue(String totalBookingvalue) {
		this.totalBookingvalue = totalBookingvalue;
	}


	public String getAmountchargeable() {
		return amountchargeable;
	}


	public void setAmountchargeable(String amountchargeable) {
		this.amountchargeable = amountchargeable;
	}


	public String getProcessbyairline() {
		return processbyairline;
	}


	public void setProcessbyairline(String processbyairline) {
		this.processbyairline = processbyairline;
	}


	public String getCurrency() {
		return currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public String getOrngtotbookingval() {
		return orngtotbookingval;
	}


	public void setOrngtotbookingval(String orngtotbookingval) {
		this.orngtotbookingval = orngtotbookingval;
	}


	public String getOrngamountchargeable() {
		return orngamountchargeable;
	}


	public void setOrngamountchargeable(String orngamountchargeable) {
		this.orngamountchargeable = orngamountchargeable;
	}


	public String getOrngprocessbyairline() {
		return orngprocessbyairline;
	}


	public void setOrngprocessbyairline(String orngprocessbyairline) {
		this.orngprocessbyairline = orngprocessbyairline;
	}


	public Traveler getCustomer() {
		return customer;
	}


	public void setCustomer(Traveler customer) {
		this.customer = customer;
	}


	
}//class end
