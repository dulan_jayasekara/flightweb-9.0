package Flight.Reports;

/*import java.io.File;*/
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
/*import java.util.concurrent.TimeUnit;*/






import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
/*import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;*/
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.common.Validators.CommonValidator;

import system.classes.*;

public class CustomerVoucherEmail
{
	private String Tel						= "";
	private String Fax						= "";
	private String Email					= "";
	private String WebSite					= "";
	
	private String LeadPassenger			= "";//Validated
	private String IssueDtae				= "";
	private String BookingNo				= "";//Validated
	private String VoucherNo				= "";
	
	private String SuppConfirmationNo		= "";
	private String RecordLocator			= "";
	
	private String AdultCount				= "0";//Validated
	private String ChildrenCount			= "0";//Validated
	private String InfantCount				= "0";//Validated
	
	private ArrayList<Flight> Outbound		= new ArrayList<Flight>();//Validated
	private ArrayList<Flight> Inbound		= new ArrayList<Flight>();//Validated
	
	private ArrayList<Traveler> Adults		= new ArrayList<Traveler>();//Validated
	private ArrayList<Traveler> Children	= new ArrayList<Traveler>();//Validated
	private ArrayList<Traveler> Infants		= new ArrayList<Traveler>();//Validated
	HashMap<String, String>	Propertymap		= new HashMap<String, String>();
	private boolean available				= false;
	
	
	public CustomerVoucherEmail(HashMap<String, String> Propymap)
	{
		Propertymap = Propymap;
	}
	
	public void getCusVoucherEmail(WebDriver driver, String resno)
	{
		try
		{
			String ReservationNo1 = resno.trim();
			WebDriverWait wait 		= new WebDriverWait(driver, 20);
			
			driver.get(Propertymap.get("Portal.Url").concat("/reports/operational/mainReport.do?reportId=33&reportName=Booking%20Confirmation%20Report"));
			
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			driver.findElement(By.id("searchby_reservationno")).click();
			driver.findElement(By.id("searchby_dates")).click();
			driver.findElement(By.id("searchby_reservationno")).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("reservationno")));
			driver.findElement(By.id("reservationno")).clear();
			driver.findElement(By.id("reservationno")).sendKeys(ReservationNo1);
			driver.findElement(By.id("reservationno_lkup")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("lookup");
			driver.findElement(By.className("rowstyle0")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			
			try
			{
				((JavascriptExecutor)driver).executeScript("javascript: getReportData('view');");
			}
			catch(Exception e)
			{
				
			}
										 
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			Thread.sleep(2000);
			driver.findElement(By.partialLinkText(ReservationNo1)).click();
			Thread.sleep(2000);
			driver.findElement(By.id("emaillayoutbasedon_customer2")).click();
			driver.switchTo().frame("Myemailformat");
																									  
			ArrayList<WebElement> TRs		= new ArrayList<WebElement>(driver.findElements(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr")));
			ArrayList<WebElement> TDsofTRs	= new ArrayList<WebElement>(TRs.get(0).findElements(By.tagName("td")));
			String[] ContentArray			= TDsofTRs.get(5).getText().split("\\n");
			try
			{
				available = true;
				Tel								= ContentArray[1].split(":")[1].trim();
				Fax								= ContentArray[2].split(":")[1].trim();
				Email							= ContentArray[3].split(":")[1].trim();
				WebSite							= ContentArray[4].split(":")[1].trim();
			}
			catch(Exception e )
			{
				
			}
			
			//Skip TRs.get(1) ---> Travel Voucher Heading <tr>
			
			TDsofTRs			= new ArrayList<WebElement>(TRs.get(3).findElements(By.tagName("td")));
			LeadPassenger		= TDsofTRs.get(1).getText().replace(":", "").trim();
			IssueDtae			= TDsofTRs.get(4).getText().replace(":", "").trim()/*.split(" ")[0].trim()*/;
			
			TDsofTRs			= new ArrayList<WebElement>(TRs.get(4).findElements(By.tagName("td")));
			BookingNo			= TDsofTRs.get(7).getText().replace(":", "").trim();
			
			TDsofTRs			= new ArrayList<WebElement>(TRs.get(5).findElements(By.tagName("td")));
			VoucherNo			= TDsofTRs.get(7).getText().replace(":", "").trim();
			
			//Skip TRs.get(6) ---> Blank <tr>
			//Skip TRs.get(7) ---> Flight Information Heading<tr>
			
			TDsofTRs			= new ArrayList<WebElement>(TRs.get(8).findElements(By.tagName("td")));
			SuppConfirmationNo	= TDsofTRs.get(1).getText().trim().replace(":", "").trim();
			RecordLocator		= TDsofTRs.get(7).getText().trim().replace(":", "").trim();
			
			TDsofTRs			= new ArrayList<WebElement>(TRs.get(9).findElements(By.tagName("td")));
			AdultCount			= TDsofTRs.get(1).getText().trim().replace(":", "").trim();
			ChildrenCount		= TDsofTRs.get(7).getText().trim().replace(":", "").trim();
			TDsofTRs			= new ArrayList<WebElement>(TRs.get(10).findElements(By.tagName("td")));
			InfantCount			= TDsofTRs.get(1).getText().trim().replace(":", "").trim();
			
			
			
			//Skip TRs.get(10) ---> Blank <tr>
			//Skip TRs.get(11) ---> Flight Outbound Info Heading <tr>
			
			
			ArrayList<WebElement> TROutFlight = null;
			ArrayList<ArrayList<WebElement>> TROutFlightList = new ArrayList<ArrayList<WebElement>>();
			int TRsize	= TRs.size();
			for(int i=11; i<TRsize; i++  )
			{
				TDsofTRs			= new ArrayList<WebElement>(TRs.get(i).findElements(By.tagName("td")));
				if(TDsofTRs.get(0).getText().trim().equalsIgnoreCase("Flight Outbound Info"))
				{
					for(int y=i; y<TRsize; y++)
					{
						TDsofTRs			= new ArrayList<WebElement>(TRs.get(y+1).findElements(By.tagName("td")));
						if(TDsofTRs.get(0).getText().trim().equalsIgnoreCase("Airline"))
						{
							TROutFlight = new ArrayList<WebElement>();
							TROutFlight.add(TRs.get(y+1));
							TROutFlight.add(TRs.get(y+2));
							TROutFlight.add(TRs.get(y+3));
							TROutFlight.add(TRs.get(y+4));
							TROutFlight.add(TRs.get(y+5));
							
							TROutFlightList.add(TROutFlight); 
							
						}
						if( (TDsofTRs.get(0).getText().trim().equalsIgnoreCase("Flight Return Info")) || (TDsofTRs.get(0).getText().trim().equalsIgnoreCase("Passenger Name(s)")))
						{
							break;
						}
					}
					break;
				}
			}
			
			//Create Out bound Flights
			try {
				
				int out = TROutFlightList.size();
				ArrayList<WebElement> tds = new ArrayList<WebElement>();
				
				for(int outcount=0; outcount<out; outcount++)
				{
					Flight flight = new Flight();
					TROutFlight = TROutFlightList.get(outcount);
					
					//TR 1 details
					tds = new ArrayList<WebElement>( TROutFlight.get(0).findElements(By.tagName("td")) );
					String airline = tds.get(1).getText();
					flight.setMarketingAirline(airline.split(":")[1].trim());
					String flightno = tds.get(7).getText();
					flight.setFlightNo(flightno.split(":")[1].trim());
					
					//TR 2 details
					tds = new ArrayList<WebElement>( TROutFlight.get(1).findElements(By.tagName("td")) );
					String depport = tds.get(1).getText();
					flight.setDeparture_port(depport.split("[()]")[1].trim());
					flight.setDepartureLocationCode(depport.split("[()]")[1].trim());
					String arrport = tds.get(3).getText();
					flight.setArrival_port(arrport.split("[()]")[1].trim());
					flight.setArrivalLocationCode(arrport.split("[()]")[1].trim());
					
					//TR 3 details
					tds = new ArrayList<WebElement>( TROutFlight.get(2).findElements(By.tagName("td")) );
					String depDate			= "";
					try {
						depDate				= tds.get(1).getText().trim();
						depDate				= depDate.replace(":", "").trim();
						try {
							String[] dA			= depDate.split(" ");
							String dt			= "";
							if( dA[0].contains("th") )
							{
								dt			= dA[0].replace("th", "");
							}
							else if(dA[0].contains("rd"))
							{
								dt			= dA[0].replace("rd", "");
							}
							else if(dA[0].contains("st"))
							{
								dt			= dA[0].replace("st", "");
							}
							else if(dA[0].contains("nd"))
							{
								dt			= dA[0].replace("nd", "");
							}
							depDate = dt.trim().concat("-").concat(dA[1].trim().concat("-").concat(dA[2].trim()));
							depDate = CommonValidator.formatDateToCommon(depDate, "dd-MMM-yyyy");
						} catch (Exception e) {
							depDate				= tds.get(1).getText().split(":")[1].trim();
						}
						
					} catch (Exception e) {
						depDate				= tds.get(1).getText().split(":")[1].trim();
					}
					try {
						depDate = CommonValidator.formatDateToCommon(depDate, "dd-MMM-yyyy");
					} catch (Exception e) {
						// TODO: handle exception
					}
					flight.setDepartureDate(depDate);
					
					String arrDate			= "";
					try {
						arrDate				= tds.get(7).getText().trim();
						arrDate				= arrDate.replace(":", "").trim();
						try {
							String[] aA			= arrDate.split(" ");
							String at			= "";
							if( aA[0].contains("th") )
							{
								at			= aA[0].replace("th", "");
							}
							else if(aA[0].contains("rd"))
							{
								at			= aA[0].replace("rd", "");
							}
							else if(aA[0].contains("st"))
							{
								at			= aA[0].replace("st", "");
							}
							else if(aA[0].contains("nd"))
							{
								at			= aA[0].replace("nd", "");
							}
							arrDate = at.trim().concat("-").concat(aA[1].trim().concat("-").concat(aA[2].trim()));
							arrDate = CommonValidator.formatDateToCommon(arrDate, "dd-MMM-yyyy");
						} catch (Exception e) {
							arrDate				= tds.get(1).getText().split(":")[1].trim();
						}
					} catch (Exception e) {
						arrDate		= tds.get(7).getText().split(":")[1].trim();
					}
					try {
						arrDate = CommonValidator.formatDateToCommon(arrDate, "dd-MMM-yyyy");
					} catch (Exception e) {
						// TODO: handle exception
					}
					flight.setArrivalDate(arrDate);
					
					//TR 4 details
					tds = new ArrayList<WebElement>( TROutFlight.get(3).findElements(By.tagName("td")) );
					String depTime	= tds.get(1).getText().split(":")[1].trim();
					depTime			= depTime.replace("H", "00");
					String[] depTimearr= depTime.split("");
					String a = "";
					for(int depcount=0; depcount<depTimearr.length; depcount+=2)
					{
						if(!depTimearr[depcount].equals(""))
						{
							a = a.concat(depTimearr[depcount-1]).concat(depTimearr[depcount]).concat(":");
						}
					}
					depTime = a.substring(0,a.length()-1);
					flight.setDepartureTime(depTime);
					
					
					String arrTime	= tds.get(7).getText().split(":")[1].trim();
					arrTime			= arrTime.replace("H", "00");
					String[] arrTimearr= arrTime.split("");
					String b = "";
					for(int arrcount=0; arrcount<arrTimearr.length; arrcount+=2)
					{
						if(!depTimearr[arrcount].equals(""))
						{
							b = b.concat(arrTimearr[arrcount-1]).concat(arrTimearr[arrcount]).concat(":");
						}
					}
					arrTime = b.substring(0,b.length()-1);
					flight.setArrivalTime(arrTime);
					
					//TR 5 details
					tds = new ArrayList<WebElement>( TROutFlight.get(4).findElements(By.tagName("td")) );
					flight.setCabintype( tds.get(1).getText().trim().split(":")[1].trim() );
					flight.setSegmentStatus( tds.get(7).getText().trim().split(":")[1].trim() );
					
					Outbound.add(flight);
				}
			} catch (Exception e) {
				
			}
			
			//Inbound Flights
			ArrayList<WebElement> TRInFlight = null;
			ArrayList<ArrayList<WebElement>> TRInFlightList = new ArrayList<ArrayList<WebElement>>();
			int TRinsize	= TRs.size();
			for(int i=11; i<TRinsize; i++  )
			{
				TDsofTRs			= new ArrayList<WebElement>(TRs.get(i).findElements(By.tagName("td")));
				if(TDsofTRs.get(0).getText().trim().equalsIgnoreCase("Flight Return Info"))
				{
					for(int y=i; y<TRsize; y++)
					{
						TDsofTRs			= new ArrayList<WebElement>(TRs.get(y+1).findElements(By.tagName("td")));
						if(TDsofTRs.get(0).getText().trim().equalsIgnoreCase("Airline"))
						{
							TRInFlight = new ArrayList<WebElement>();
							TRInFlight.add(TRs.get(y+1));
							TRInFlight.add(TRs.get(y+2));
							TRInFlight.add(TRs.get(y+3));
							TRInFlight.add(TRs.get(y+4));
							TRInFlight.add(TRs.get(y+5));
							
							TRInFlightList.add(TRInFlight); 
						}
						if( (TDsofTRs.get(0).getText().trim().equalsIgnoreCase("Passenger Name(s)")) )
						{
							break;
						}
					}
					break;
				}
			}
			
			try {
				int in = TRInFlightList.size();
				//System.out.println();
				ArrayList<WebElement> intds = new ArrayList<WebElement>();
				
				for(int incount=0; incount<in; incount++)
				{
					Flight flight = new Flight();
					TRInFlight = TRInFlightList.get(incount);
					
					//TR 1 details
					intds = new ArrayList<WebElement>( TRInFlight.get(0).findElements(By.tagName("td")) );
					String marketairline = intds.get(1).getText();
					flight.setMarketingAirline(marketairline.split(":")[1].trim());
					String flightno = intds.get(7).getText();
					flight.setFlightNo(flightno.split(":")[1].trim());
					
					//TR 2 details
					intds = new ArrayList<WebElement>( TRInFlight.get(1).findElements(By.tagName("td")) );
					String depport = intds.get(1).getText();
					flight.setDeparture_port(depport.split("[()]")[1].trim());
					flight.setDepartureLocationCode(depport.split("[()]")[1].trim());
					String arrport = intds.get(3).getText();
					flight.setArrival_port(arrport.split("[()]")[1].trim());
					flight.setArrivalLocationCode(arrport.split("[()]")[1].trim());
					
					//TR 3 details
					intds = new ArrayList<WebElement>( TRInFlight.get(2).findElements(By.tagName("td")) );
					String depDate			= ""; 
					try {
						depDate				= intds.get(1).getText().trim();
						depDate				= depDate.replace(":", "").trim();
						try {
							String[] dA			= depDate.split(" ");
							String dt			= "";
							if( dA[0].contains("th") )
							{
								dt			= dA[0].replace("th", "");
							}
							else if(dA[0].contains("rd"))
							{
								dt			= dA[0].replace("rd", "");
							}
							else if(dA[0].contains("st"))
							{
								dt			= dA[0].replace("st", "");
							}
							else if(dA[0].contains("nd"))
							{
								dt			= dA[0].replace("nd", "");
							}
							depDate = dt.trim().concat("-").concat(dA[1].trim().concat("-").concat(dA[2].trim()));
							depDate = CommonValidator.formatDateToCommon(depDate, "dd-MMM-yyyy");
						} catch (Exception e) {
							depDate				= intds.get(1).getText().split(":")[1].trim();
						}
					} catch (Exception e) {
						
					}
					try {
						depDate = CommonValidator.formatDateToCommon(depDate, "dd-MMM-yyyy");
					} catch (Exception e) {
						// TODO: handle exception
					}
					flight.setDepartureDate(depDate);
					
					String arrDate			= "";
					try {
						arrDate				= intds.get(7).getText().trim();
						arrDate				= arrDate.replace(":", "").trim();
						try {
							String[] aA			= arrDate.split(" ");
							String at			= "";
							if( aA[0].contains("th") )
							{
								at			= aA[0].replace("th", "");
							}
							else if(aA[0].contains("rd"))
							{
								at			= aA[0].replace("rd", "");
							}
							else if(aA[0].contains("st"))
							{
								at			= aA[0].replace("st", "");
							}
							else if(aA[0].contains("nd"))
							{
								at			= aA[0].replace("nd", "");
							}
							arrDate = at.trim().concat("-").concat(aA[1].trim().concat("-").concat(aA[2].trim()));
							arrDate = CommonValidator.formatDateToCommon(arrDate, "dd-MMM-yyyy");
						} catch (Exception e) {
							arrDate				= intds.get(1).getText().split(":")[1].trim();
						}
					} catch (Exception e) {
						
					}
					try {
						arrDate = CommonValidator.formatDateToCommon(arrDate, "dd-MMM-yyyy");
					} catch (Exception e) {
						// TODO: handle exception
					}
					flight.setArrivalDate(arrDate);
					
					//TR 4 details
					intds = new ArrayList<WebElement>( TRInFlight.get(3).findElements(By.tagName("td")) );
					String depTime		= intds.get(1).getText().split(":")[1].trim();
					depTime				= depTime.replace("H", "00");
					String[] depTimearr	= depTime.split("");
					String a 			= "";
					for(int depcount=0; depcount<depTimearr.length; depcount+=2)
					{
						if(!depTimearr[depcount].equals(""))
						{
							a = a.concat(depTimearr[depcount-1]).concat(depTimearr[depcount]).concat(":");
						}
					}
					depTime = a.substring(0,a.length()-1);
					flight.setDepartureTime(depTime);
					
					String arrTime	= intds.get(7).getText().split(":")[1].trim();
					try {
						arrTime			= arrTime.replace("H", "00");
						String[] arrTimearr= arrTime.split("");
						String b = "";
						for(int arrcount=0; arrcount<arrTimearr.length; arrcount+=2)
						{
							if(!arrTimearr[arrcount].equals(""))
							{
								b = b.concat(arrTimearr[arrcount-1]).concat(arrTimearr[arrcount]).concat(":");
							}
						}
						arrTime = b.substring(0,b.length()-1);
					} catch (Exception e) {
						
					}
					flight.setArrivalTime(arrTime);
					
					//TR 5 details
					intds = new ArrayList<WebElement>( TRInFlight.get(4).findElements(By.tagName("td")) );
					flight.setCabintype( intds.get(1).getText().trim().split(":")[1].trim() );
					flight.setSegmentStatus( intds.get(7).getText().trim().split(":")[1].trim() );
					
					Inbound.add(flight);
				}
			} catch (Exception e) {
				
			}
			
			int start = 0;
			for(int j=0; j<TRs.size(); j++)
			{
				TDsofTRs	= new ArrayList<WebElement>(TRs.get(j).findElements(By.tagName("td")));
				if(TDsofTRs.get(0).getText().trim().contains("Passenger Name(s)"))
				{
					start = j+1;
				}
			}
			
			int lastTRcount = TRs.size() - start;
			lastTRcount = lastTRcount - 3;
			
			for(int g=start; g<(start+lastTRcount); g++)
			{
				Traveler passenger = new Traveler();
				TDsofTRs	= new ArrayList<WebElement>( TRs.get(g).findElements(By.tagName("td")) );
				
				//Passenger first name and last name details
				String name = TDsofTRs.get(0).getText().trim();
				if(name.contains("/"))
				{
					name = name.replace("/", "");
				}
				String[] nameArr = name.split(" ");
				ArrayList<String> list = new ArrayList<String>();
				for(int y=0; y<nameArr.length; y++)
				{
					if(!nameArr[y].equals(""))
					{
						list.add(nameArr[y]);
					}
				}
				passenger.setSurname(list.get(0).trim());
				passenger.setGivenName(list.get(1).trim());
				passenger.setNamePrefixTitle(list.get(2).trim());				
				//Passenger Type Code
				String type = TDsofTRs.get(1).getText().trim();
				passenger.setPassengertypeCode(type);
				
				//Passenger Age
				String age = TDsofTRs.get(2).getText().trim();
				passenger.setAge(age);
				
				//Passenger  Passport No
				String passportNo = TDsofTRs.get(3).getText().trim();
				passenger.setPassportNo(passportNo);
				
				//Passenger E-ticket No details
				String eticketNo = TDsofTRs.get(4).getText().trim();
				passenger.setETicket_No(eticketNo);
				
				if( passenger.getPassengertypeCode().equalsIgnoreCase("ADT") )
				{
					Adults.add(passenger);
				}
				else if(passenger.getPassengertypeCode().equalsIgnoreCase("CHD"))
				{
					Children.add(passenger);
				}
				else if(passenger.getPassengertypeCode().equalsIgnoreCase("INF"))
				{
					Infants.add(passenger);
				}
			}
			
			available = true;
		}
		catch(Exception e)
		{
			TakesScreenshot screen = (TakesScreenshot)driver;
			try {
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Customer Voucher Email.jpg"));
			} catch (WebDriverException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		
	}	
	
	public String getTel() {
		return Tel;
	}

	public void setTel(String tel) {
		Tel = tel;
	}

	public String getFax() {
		return Fax;
	}

	public void setFax(String fax) {
		Fax = fax;
	}

	public String getWebSite() {
		return WebSite;
	}

	public void setWebSite(String webSite) {
		WebSite = webSite;
	}

	public String getLeadPassenger() {
		return LeadPassenger;
	}

	public void setLeadPassenger(String leadPassenger) {
		LeadPassenger = leadPassenger;
	}

	public String getIssueDtae() {
		return IssueDtae;
	}

	public void setIssueDtae(String issueDtae) {
		IssueDtae = issueDtae;
	}

	public String getBookingNo() {
		return BookingNo;
	}

	public void setBookingNo(String bookingNo) {
		BookingNo = bookingNo;
	}

	public String getVoucherNo() {
		return VoucherNo;
	}

	public void setVoucherNo(String voucherNo) {
		VoucherNo = voucherNo;
	}

	public String getSuppConfirmationNo() {
		return SuppConfirmationNo;
	}

	public void setSuppConfirmationNo(String suppConfirmationNo) {
		SuppConfirmationNo = suppConfirmationNo;
	}

	public String getRecordLocator() {
		return RecordLocator;
	}

	public void setRecordLocator(String recordLocator) {
		RecordLocator = recordLocator;
	}

	public String getAdultCount() {
		return AdultCount;
	}

	public void setAdultCount(String adultCount) {
		AdultCount = adultCount;
	}

	public String getChildrenCount() {
		return ChildrenCount;
	}

	public void setChildrenCount(String childrenCount) {
		ChildrenCount = childrenCount;
	}

	public String getInfantCount() {
		return InfantCount;
	}

	public void setInfantCount(String infantCount) {
		InfantCount = infantCount;
	}

	public ArrayList<Flight> getOutbound() {
		return Outbound;
	}

	public void setOutbound(ArrayList<Flight> outbound) {
		Outbound = outbound;
	}

	public ArrayList<Flight> getInbound() {
		return Inbound;
	}

	public void setInbound(ArrayList<Flight> inbound) {
		Inbound = inbound;
	}

	public ArrayList<Traveler> getAdults() {
		return Adults;
	}

	public void setAdults(ArrayList<Traveler> adults) {
		Adults = adults;
	}

	public ArrayList<Traveler> getChildren() {
		return Children;
	}

	public void setChildren(ArrayList<Traveler> children) {
		Children = children;
	}

	public ArrayList<Traveler> getInfants() {
		return Infants;
	}

	public void setInfants(ArrayList<Traveler> infants) {
		Infants = infants;
	}

	public void setEmail(String email) {
		Email = email;
	}
	
	public String getEmail() {
		return Email;
	}

	public boolean isAvailable() {
		return available;
	}
	
	
}
