/*Sanoj*/
package Flight.Price;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import system.classes.*;
import Flight.InitialLoading.JsoupDoc;
import system.enumtypes.*;

public class PriceResponseReader {
	
	
	//Logger 							logger				= null;
	private HashMap<String, String> Propertymap 		= null;
	File							input				= null;
	ArrayList<XMLOriginOptions> 	AirItinerary		= new ArrayList<XMLOriginOptions>();
	Map<String, String> 			AirTravelers 		= new HashMap<String, String>();
	JsoupDoc 						getdoc 				= null;
	String 							PropfilePath		= "Properties.properties";		
	boolean							err					= false;
	
	public PriceResponseReader()
	{
		
	}
	
	public PriceResponseReader(HashMap<String, String> Propymap)
	{
		Propertymap = Propymap;
		getdoc = new JsoupDoc(Propertymap);
	}

	public PriceResponse AmadeusPriceResponseReader(XMLLocateType Value, String Val, XMLFileType type)
	{
		System.out.println("========================================");
		System.out.println("PRICE RESPONSE XML READ");
		
		Document doc = getdoc.createDoc(Value, Val, type);
		PriceResponse priceiterobj = new PriceResponse();
		XMLError error = new XMLError();
		try
		{
			error.setErrorMessage(doc.getElementsByTag("Error").get(0).text());
			err = true;
			error.setErrortype(doc.getElementsByTag("Error").get(0).attr("Type"));
			error.setErrorcode(doc.getElementsByTag("Error").get(0).attr("Code"));
		}
		catch(Exception exx)
		{
			//exx.printStackTrace();
			
			try {
				if(!err/*priceiterobj.getError().getErrorMessage().equals("")*/)
				{
					XMLPriceInfo priceinfo = new XMLPriceInfo();
					
					try
					{
						priceiterobj.setPriceItineraryNumber(doc.getElementsByTag("PricedItinerary").attr("SequenceNumber"));
						
						String pricingSource = "Published";
						pricingSource = doc.getElementsByTag("AirItineraryPricingInfo ").attr("PricingSource");
						if(!pricingSource.equals(""))
						{
							priceinfo.setPricingSource(pricingSource);
						}
						else
						{
							priceinfo.setPricingSource("Published");
						}
						
						
						String BaseFare = "";
						BaseFare = doc.getElementsByTag("BaseFare").attr("Amount");
						priceinfo.setBasefareAmountDecimal(doc.getElementsByTag("BaseFare").attr("DecimalPlaces"));
						try {
							if(priceinfo.getBasefareAmountDecimal() != 0)
							{
								BaseFare = BaseFare.substring(0, BaseFare.length() - priceinfo.getBasefareAmountDecimal() ).concat(".").concat(BaseFare.substring(BaseFare.length() - priceinfo.getBasefareAmountDecimal(), BaseFare.length()));
							}
							
						} catch (Exception e) {
							e.printStackTrace();
						}
						priceinfo.setBasefareAmount(BaseFare);
						priceinfo.setBasefareCurrencyCode(doc.getElementsByTag("BaseFare").attr("CurrencyCode"));
						
						
						String Tax = "";
						Tax = doc.getElementsByTag("Tax").attr("Amount");
						priceinfo.setTaxAmountDecimal(doc.getElementsByTag("Tax").attr("DecimalPlaces"));
						try {
							if(priceinfo.getTaxAmountDecimal() != 0)
							{
								Tax = Tax.substring(0, Tax.length() - priceinfo.getTaxAmountDecimal() ).concat(".").concat(Tax.substring(Tax.length() - priceinfo.getTaxAmountDecimal(), Tax.length()));
							}
							
						} catch (Exception e) {
							e.printStackTrace();
						}
						priceinfo.setTaxCurrencyCode(doc.getElementsByTag("Tax").attr("CurrencyCode"));
						priceinfo.setTaxAmount(Tax);
						priceinfo.setTaxCode(doc.getElementsByTag("Tax").attr("TaxCode"));
						
						String Total = "";
						Total = doc.getElementsByTag("TotalFare").attr("Amount");
						priceinfo.setTotalFareDecimal(doc.getElementsByTag("TotalFare").attr("DecimalPlaces"));
						try {
							if(priceinfo.getTotalFareDecimal() != 0)
							{
								Total = Total.substring(0, Total.length() - priceinfo.getTotalFareDecimal() ).concat(".").concat(Total.substring(Total.length() - priceinfo.getTotalFareDecimal(), Total.length()));
							}
							
						} catch (Exception e) {
							e.printStackTrace();
						}
						priceinfo.setTotalFare(Total);
						priceinfo.setTotalFareCurrencyCode(doc.getElementsByTag("TotalFare").attr("CurrencyCode"));
						
					}
					catch(Exception ex)
					{
						ex.printStackTrace();
					}
					
					
					try
					{
						priceiterobj.setPriceinfo(priceinfo);		
						Elements farebreakdown = doc.getElementsByTag("PTC_FareBreakdown");
						//System.out.println(farebreakdown.size());
						
						Iterator<Element> fareiter = farebreakdown.iterator();
						
						while(fareiter.hasNext())
						{
							Element ele1 = fareiter.next();
							
							Elements farebasiscode = ele1.getElementsByTag("FareBasisCode");
							Iterator<Element> farebasisiter = farebasiscode.iterator();
							
							ArrayList<String> farebasisCODE = new ArrayList<String>();
							
							while(farebasisiter.hasNext())
							{
								Element ele2 = farebasisiter.next();
								farebasisCODE.add(ele2.getElementsByTag("FareBasisCode").text());
							}
			
						}
					}
					catch(Exception ep)
					{
						ep.printStackTrace();
					}
					
					try
					{
						Elements fareInfo = doc.getElementsByTag("FareInfo");
						
						Iterator<Element> fareInfoiter = fareInfo.iterator();
						ArrayList<String> FareInfo = new ArrayList<String>();
						
						while(fareInfoiter.hasNext())
						{
							Element ele4 = fareInfoiter.next();
							String a = ele4.getElementsByTag("DepartureDate").text();
							String b = a.split("T")[1];
							a = a.split("T")[0];
							String c = ele4.getElementsByTag("FareReference").text();
							String d = ele4.getElementsByTag("FilingAirLine").attr("Code");
							String e = ele4.getElementsByTag("DepartureAirport").attr("LocationCode");
							String f = ele4.getElementsByTag("ArrivalAirport").attr("LocationCode");
							
							String fareinfo = a.concat(",").concat(b).concat(",").concat(c).concat(",").concat(d).concat(",").concat(e).concat(",").concat(f);
							FareInfo.add(fareinfo);
						}
						
						priceiterobj.setTickettimelimit(doc.getElementsByTag("TicketingInfo ").attr("TicketTimeLimit").split("T")[0]);
					}
					catch(Exception er)
					{
						er.printStackTrace();
						
					}
					
					priceiterobj.setAvailable(true);
					System.out.println("Price response reading successful..!!");
				}
			} catch (Exception e) {
				priceiterobj.setAvailable(false);
			}
		}

		System.out.println("PRICE RESPONSE XML READ END");
		System.out.println("====================================");
		
		priceiterobj.setError(error);
		
		return priceiterobj;
		
	}
/*	
	
	public void SabrePriceRequestReader(XMLLocateType Value, String Val, XMLFileType type)
	{
		Document doc = getdoc.createDoc(Value, Val, type);
		PriceResponse priceiterobj = new PriceResponse();
		
		priceiterobj.setBaseDecimalPlaces(doc.getElementsByTag("BaseFare").attr("DecimalPlaces"));
		priceiterobj.setBaseCurrencyCode(doc.getElementsByTag("BaseFare").attr("CurrencyCode"));
		priceiterobj.setBaseFareAmount(doc.getElementsByTag("BaseFare").attr("Amount"));
		priceiterobj.setTaxDecimalPlaces(doc.getElementsByTag("Tax").attr("DecimalPlaces"));
		priceiterobj.setTaxCurrencyCode(doc.getElementsByTag("Tax").attr("CurrencyCode"));
		priceiterobj.setTaxAmount(doc.getElementsByTag("Tax").attr("Amount"));
		priceiterobj.setTaxCode(doc.getElementsByTag("Tax").attr("TaxCode"));
		
		Elements farebreakdown = doc.getElementsByTag("PTC_FareBreakdown");
		
		Iterator<Element> fareiter = farebreakdown.iterator();
		
		while(fareiter.hasNext())
		{
			Element ele1 = fareiter.next();
			
			System.out.println(ele1.getElementsByTag("PassengerTypeQuantity").attr("Code"));
			System.out.println(ele1.getElementsByTag("PassengerTypeQuantity").attr("Quantity"));
			
			Elements farebasiscode = ele1.getElementsByTag("FareBasisCode");
			Iterator<Element> farebasisiter = farebasiscode.iterator();
			
			ArrayList<String> farebasisCODE = new ArrayList<String>();
			
			while(farebasisiter.hasNext())
			{
				Element ele2 = farebasisiter.next();
				farebasisCODE.add(ele2.getElementsByTag("FareBasisCode").text());
				System.out.println(farebasisCODE);
			}

			System.out.println(ele1.getElementsByTag("TotalFare").attr("Amount"));
			System.out.println(ele1.getElementsByTag("TotalFare").attr("CurrencyCode"));
			System.out.println(ele1.getElementsByTag("TotalFare").attr("DecimalPlaces"));
			
		}
		
		Elements fareInfo = doc.getElementsByTag("FareInfo");
		
		Iterator<Element> fareInfoiter = fareInfo.iterator();
		ArrayList<String> FareInfo = new ArrayList<String>();
		
		while(fareInfoiter.hasNext())
		{
			Element ele4 = fareInfoiter.next();
			String a = ele4.getElementsByTag("DepartureDate").text();
			String b = ele4.getElementsByTag("FareReference").text();
			String c = ele4.getElementsByTag("RuleInfo").text();
			String d = ele4.getElementsByTag("FilingAirLine").attr("Code");
			String e = ele4.getElementsByTag("DepartureAirport").attr("LocationCode");
			String f = ele4.getElementsByTag("ArrivalAirport").attr("LocationCode");
			
			String fareinfo = a.concat(",").concat(b).concat(",").concat(c).concat(",").concat(d).concat(",").concat(e).concat(",").concat(f);
			FareInfo.add(fareinfo);
		}
	}*/
}
