/*Sanoj*/
package Flight.Price;

import java.util.ArrayList;
import java.util.Map;

import system.classes.*;


public class PriceRequest {
	
	
	private		String						ISOCurrency		 = "";
	private		String						provider		 = "";
	private		ArrayList<XMLOriginOptions>	originOptionList = new ArrayList<XMLOriginOptions>();
	private		String						seatRequested	 = "";
	private		Map<String, String>			AirTravelers	 = null;
	private		String						PricingSource	 = "";
	

	public String getPricingSource() {
		return PricingSource;
	}
	public void setPricingSource(String pricingSource) {
		PricingSource = pricingSource;
	}
	public String getISOCurrency() {
		return ISOCurrency;
	}
	public void setISOCurrency(String iSOCurrentcy) {
		ISOCurrency = iSOCurrentcy;
	}
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public ArrayList<XMLOriginOptions> getOriginOptionList() {
		return originOptionList;
	}
	public void setOriginOptionList(ArrayList<XMLOriginOptions> originOptionList) {
		this.originOptionList = originOptionList;
	}
	public String getSeatRequested() {
		return seatRequested;
	}
	public void setSeatRequested(String seatRequested) {
		this.seatRequested = seatRequested;
	}
	public Map<String, String> getAirTravelers() {
		return AirTravelers;
	}
	public void setAirTravelers(Map<String, String> airTravelers) {
		AirTravelers = airTravelers;
	}
	
	public void getAll()
	{
		System.out.println(getISOCurrency());
		System.out.println(getPricingSource());
		System.out.println(getProvider());
		System.out.println(getSeatRequested());
		System.out.println(getAirTravelers());
		//System.out.println(getOriginOptionList());
	}

}
