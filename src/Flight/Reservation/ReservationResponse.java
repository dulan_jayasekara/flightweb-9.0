/*Sanoj*/
package Flight.Reservation;

import java.util.ArrayList;

import system.classes.*;

public class ReservationResponse 
{
	String ItineraryRefType = "";
	String ItineraryRefID = "";
	String ItineraryRefID_Context = "";
	
	ArrayList<Traveler> Customers = null;
	ArrayList<XMLResvRespItem> Items = null;
	
	String pricinsource = "";
	XMLPriceInfo fare = null;
	
	String tickettimelimit = "";
	String ticketType = "";
	
	ArrayList<XMLSpecialService> SpServiceRequests = null;
	
	ArrayList<String> Remark = null;
	ArrayList<XMLSpecialRemark> specialRemark = null;
	
	ArrayList<String> issuedTickets = null;
	
	String UpdatedCreatedate = "";
	String UpdatedCreatedTime = "";
	String AgencyCommission = "";
	
/*	String Errortype		= "";	
	String Errorcode		= "";
	String ErrorMessage		= "";*/
	
	XMLError error = new XMLError();
	boolean available = false;
	
	
	public boolean isAvailable() {
		return available;
	}
	public void setAvailable(boolean available) {
		this.available = available;
	}
	public XMLError getError() {
		return error;
	}
	public void setError(XMLError error) {
		this.error = error;
	}
	public String getItineraryRefType() {
		return ItineraryRefType;
	}
	public void setItineraryRefType(String itineraryRefType) {
		ItineraryRefType = itineraryRefType;
	}
	public String getItineraryRefID() {
		return ItineraryRefID;
	}
	public void setItineraryRefID(String itineraryRefID) {
		ItineraryRefID = itineraryRefID;
	}
	public String getItineraryRefID_Context() {
		return ItineraryRefID_Context;
	}
	public void setItineraryRefID_Context(String itineraryRefID_Context) {
		ItineraryRefID_Context = itineraryRefID_Context;
	}
	
	public XMLPriceInfo getFare() {
		return fare;
	}
	public void setFare(XMLPriceInfo fare) {
		this.fare = fare;
	}
	
/*	public String getErrortype() {
		return Errortype;
	}
	public void setErrortype(String errortype) {
		Errortype = errortype;
	}
	public String getErrorcode() {
		return Errorcode;
	}
	public void setErrorcode(String errorcode) {
		Errorcode = errorcode;
	}
	public String getErrorMessage() {
		return ErrorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		ErrorMessage = errorMessage;
	}*/

	public String getAgencyCommission() {
		return AgencyCommission;
	}
	public void setAgencyCommission(String agencyCommission) {
		AgencyCommission = agencyCommission;
	}
	
	public String getUpdatedCreatedate() {
		return UpdatedCreatedate;
	}

	public void setUpdatedCreatedate(String updatedCreatedate) {
		UpdatedCreatedate = updatedCreatedate;
	}

	public String getUpdatedCreatedTime() {
		return UpdatedCreatedTime;
	}

	public void setUpdatedCreatedTime(String updatedCreatedTime) {
		UpdatedCreatedTime = updatedCreatedTime;
	}

	public ArrayList<String> getIssuedTickets() {
		return issuedTickets;
	}

	public void setIssuedTickets(ArrayList<String> issuedTickets) {
		this.issuedTickets = issuedTickets;
	}

	public ArrayList<XMLSpecialRemark> getSpecialRemark() {
		return specialRemark;
	}

	public void setSpecialRemark(ArrayList<XMLSpecialRemark> specialRemark) {
		this.specialRemark = specialRemark;
	}

	public ArrayList<String> getRemark() {
		return Remark;
	}

	public void setRemark(ArrayList<String> remark) {
		Remark = remark;
	}

	public String getPricinsource() {
		return pricinsource;
	}

	public void setPricinsource(String pricinsource) {
		this.pricinsource = pricinsource;
	}
	
 
	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	public String getTickettimelimit() {
		return tickettimelimit;
	}

	public void setTickettimelimit(String tickettimelimit) {
		this.tickettimelimit = tickettimelimit;
	}

	public ArrayList<XMLResvRespItem> getItems() {
		return Items;
	}

	public void setItems(ArrayList<XMLResvRespItem> items) {
		Items = items;
	}

	public ArrayList<Traveler> getCustomers() {
		return Customers;
	}

	public void setCustomers(ArrayList<Traveler> customers) {
		Customers = customers;
	}

	public void setSpServiceRequests(
			ArrayList<XMLSpecialService> spServiceRequests) {

		this.SpServiceRequests = spServiceRequests;	
	}
	
	public ArrayList<XMLSpecialService> getSpServiceRequests() {

		return SpServiceRequests;
	}
	

}
