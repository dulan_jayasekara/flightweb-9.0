package Flight.Reservation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import org.jsoup.nodes.Document;

import system.classes.*;
import Flight.InitialLoading.JsoupDoc;
import system.enumtypes.*;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ResvRequestReader 
{
	private HashMap<String, String> Propertymap 	= null;
	JsoupDoc getdoc 								= null;
	Elements originOption							= null;
	Elements flightsegments                 		= null;
	ArrayList<Flight> flightobjlist         		= null;
	ArrayList<XMLOriginOptions> originobjlist  		= new ArrayList<XMLOriginOptions>();
	ArrayList<String> remarklist					= new ArrayList<String>();
	ArrayList<XMLSpecialService> SpServiceRequests	= new ArrayList<XMLSpecialService>();
	ArrayList<Traveler> travelerslist				= new ArrayList<Traveler>();
	
	public ResvRequestReader(HashMap<String, String> Propmap)
	{  
		Propertymap = Propmap;
		getdoc = new JsoupDoc(Propertymap);
	}
	
	
	public ReservationRequest RequestReader(XMLLocateType Value, String Val, XMLFileType type)
	{
		System.out.println("===================================");
		System.out.println("RESERVATION REQ. XML READ");
		Document doc = getdoc.createDoc(Value, Val, type);
		ReservationRequest resvReqobj = new ReservationRequest();
		try
		{
			if(doc==null)
			{
				resvReqobj.setAvailable(false);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
		if(resvReqobj.isAvailable())
		{
			try
			{
				//Under <POS> tag
				System.out.println(doc);
				resvReqobj.setPsudoCityCode(doc.getElementsByTag("Source").attr("PseudoCityCode"));
				resvReqobj.setAgentSine(doc.getElementsByTag("Source").attr("AgentSine"));
				resvReqobj.setIsoCurrency(doc.getElementsByTag("Source").attr("ISOCurrency"));
				resvReqobj.setRequesterType(doc.getElementsByTag("RequestorID").attr("Type"));
				resvReqobj.setRequesterID(doc.getElementsByTag("RequestorID").attr("ID"));
				resvReqobj.setProvider(doc.getElementsByTag("Name").text());
				
				//Under <OTA_AirBookRQ> <AirItinerary>
				resvReqobj.setDirection(doc.getElementsByTag("AirItinerary").attr("DirectionInd"));
				Elements nodes = doc.getElementsByTag("OriginDestinationOption");
				Iterator<Element>   originiter	= nodes.iterator();
				while(originiter.hasNext())
				{
					Element OriginOption = originiter.next();
					originOption = OriginOption.getElementsByTag("OriginDestinationOption");
					XMLOriginOptions originobject = new XMLOriginOptions();
					/*Filling flight segments*/
					flightsegments = OriginOption.getElementsByTag("FlightSegment");
					Iterator<Element> flightsegmentiter = flightsegments.iterator();
					
					//logger.info("Initializing flight object list");
					flightobjlist = new ArrayList<Flight>();
					
					while(flightsegmentiter.hasNext())
					{
						//logger.info("Starting Flight segment (flight object) loop ");
						Element flightele = flightsegmentiter.next();
						
						/*Creating flight objects*/
						Flight flightobject = new Flight();
						flightobject.setDepartureDate(flightele.getElementsByTag("FlightSegment").attr("DepartureDateTime").split("T")[0]);
						flightobject.setDepartureTime(flightele.getElementsByTag("FlightSegment").attr("DepartureDateTime").split("T")[1]);
						flightobject.setArrivalDate(flightele.getElementsByTag("FlightSegment").attr("ArrivalDateTime").split("T")[0]);
						flightobject.setArrivalTime(flightele.getElementsByTag("FlightSegment").attr("ArrivalDateTime").split("T")[1]);
						flightobject.setFlightNo(flightele.getElementsByTag("FlightSegment").attr("FlightNumber"));
						flightobject.setDeparture_port(flightele.getElementsByTag("DepartureAirport").text());
						flightobject.setDepartureLocationCode(flightele.getElementsByTag("DepartureAirport").attr("LocationCode"));						
						flightobject.setArrival_port(flightele.getElementsByTag("ArrivalAirport").text());
						flightobject.setArrivalLocationCode(flightele.getElementsByTag("ArrivalAirport").attr("LocationCode"));
						flightobject.setOperatingAirline(flightele.getElementsByTag("MarketingAirline").attr("Code"));
						flightobject.setMarketingAirline(flightele.getElementsByTag("MarketingAirline").attr("Code"));
						flightobject.setMarketingAirline_Loc_Code(flightele.getElementsByTag("MarketingAirline").attr("Code"));
						
						//logger.info("Add object to the list");
						flightobjlist.add(flightobject);
						
					}
					
					originobject.setDeparturedate(flightobjlist.get(0).getDepartureDate());
					originobject.setArrivaldate(flightobjlist.get((flightobjlist.size()-1)).getDepartureDate());
					originobject.setFlightlist(flightobjlist);
					//logger.info("Add OriginOption object to OriginOptions list");
					originobjlist.add(originobject);
				}
				
				resvReqobj.setOriginDestinationInfo(originobjlist);
				//logger.info("Initializing Origin option object")
				
				
				XMLPaymentDetails paymentdetails = new XMLPaymentDetails();
				paymentdetails.setDirectBillID(doc.getElementsByTag("PaymentDetail").attr("DirectBill_ID"));
				resvReqobj.setPaymentdetails(paymentdetails);
				
				resvReqobj.setDeliveryStreetNo(doc.getElementsByTag("StreetNmbr").text());
				resvReqobj.setDeliveryCityName(doc.getElementsByTag("CityName").text());
				resvReqobj.setDeliveryPostal(doc.getElementsByTag("PostalCode").text());
				resvReqobj.setDeliveryStateCode(doc.getElementsByTag("StateProv").attr("StateCode"));
				resvReqobj.setDeliveryCountryName(doc.getElementsByTag("CountryName").attr("Code"));
				
				Elements remark = doc.getElementsByTag("Remarks");
				Iterator<Element> remarkiter = remark.iterator();
				
				while(remarkiter.hasNext())
				{
					Element remarkele = remarkiter.next();
					remarklist.add(remarkele.getElementsByTag("Remark").text());
				}
				resvReqobj.setRemark(remarklist);
				
				Elements specialServices = doc.getElementsByTag("SpecialServiceRequest");
				Iterator<Element> services = specialServices.iterator();
				
				while(services.hasNext())
				{
					Element ele = services.next();
					XMLSpecialService service = new XMLSpecialService();
					
					service.setSSRCode(ele.getElementsByTag("SpecialServiceRequest ").attr("SSRCode"));
					service.setFlightRefNumber(ele.getElementsByTag("SpecialServiceRequest").attr("FlightRefNumberRPHList"));
					service.setTravelerRefNumber(ele.getElementsByTag("SpecialServiceRequest").attr("TravelerRefNumberRPHList"));
					service.setText(ele.getElementsByTag("Text").text());
					service.setTextArray(ele.getElementsByTag("Text").text().split("-"));
					
					SpServiceRequests.add(service);
				}
				resvReqobj.setSpServiceRequests(SpServiceRequests);
				
				XMLPNRData PNR = new XMLPNRData();
				
				Elements travelersEle = doc.getElementsByTag("Traveler");
				Iterator<Element> traveliter = travelersEle.iterator();
				
				while(traveliter.hasNext())
				{
					Element passenger = traveliter.next();
					Traveler traveler = new Traveler();
					
					traveler.setPassengertypeCode(passenger.getElementsByTag("Traveler").attr("PassengerTypeCode"));
					traveler.setNamePrefixTitle(passenger.getElementsByTag("NamePrefix").text());
					traveler.setGivenName(passenger.getElementsByTag("GivenName").text());
					
					traveler.setSurname(passenger.getElementsByTag("Surname").text());
					traveler.setCusLoyalProgID(passenger.getElementsByTag("CustLoyalty").attr("ProgramID"));
					traveler.setMembershipID(passenger.getElementsByTag("CustLoyalty").attr("MembershipID"));
					
					if(passenger.getElementsByTag("Traveler").attr("PassengerTypeCode").equals("CHD") || passenger.getElementsByTag("Traveler").contains("C"))
					{
						String date  = passenger.getElementsByTag("Traveler").attr("BirthDate");
						Date dateD = new SimpleDateFormat("yyyy-MM-dd").parse(date.trim());
						String bday = new SimpleDateFormat("dd-MM-yyyy").format(dateD);
						
						traveler.setBirthDay(bday);
					}
					if(passenger.getElementsByTag("Traveler").attr("PassengerTypeCode").equals("INF"))
					{
						String date  = passenger.getElementsByTag("Traveler").attr("BirthDate");
						//System.out.println(date);
						try
						{
							Date dateD = new SimpleDateFormat("yyyy-MM-dd").parse(date.trim());
							String bday = new SimpleDateFormat("dd-MM-yyyy").format(dateD);
							traveler.setBirthDay(bday);
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
						
					}
					
					traveler.setTravelerRefNo(passenger.getElementsByTag("TravelerRefNumber").attr("RPH"));
					travelerslist.add(traveler);
				}
				PNR.setPassengers(travelerslist);
				PNR.setTelephoneLocation(doc.getElementsByTag("Telephone").attr("PhoneLocationType"));
				PNR.setCountryAccessCode(doc.getElementsByTag("Telephone").attr("CountryAccessCode"));
				PNR.setAreaCityCode(doc.getElementsByTag("Telephone").attr("AreaCityCode"));
				PNR.setPhoneNumber(doc.getElementsByTag("Telephone").attr("PhoneNumber"));
				PNR.setEmail(doc.getElementsByTag("Email").text());
				
				Address address = new Address();
				
				address.setAddressType(doc.getElementsByTag("Address").attr("Type"));
				address.setAddressStreetNo(doc.getElementsByTag("StreetNmbr").text());
				address.setAddressCity(doc.getElementsByTag("CityName").text());
				address.setStateProv(doc.getElementsByTag("StateProv ").attr("StateCode"));
				address.setAddressCountry(doc.getElementsByTag("CountryName").attr("Code"));
				PNR.setAddress(address);
				
				PNR.setTickettimelimit(doc.getElementsByTag("Ticketing").attr("TicketTimeLimit"));
				PNR.setTickettype(doc.getElementsByTag("Ticketing").attr("TicketType"));
				
				resvReqobj.setPriceType(doc.getElementsByTag("PriceData").attr("PriceType"));
				resvReqobj.setPriceType(doc.getElementsByTag("PriceData").attr("AutoTicketing"));
				resvReqobj.setPNR(PNR);
				
				resvReqobj.setAgencyCommission(doc.getElementsByTag("Commission").attr("Percent"));
			
			
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		System.out.println("RESERVATION REQ. XML READ END");
		System.out.println("==========================================");
		
		return resvReqobj;
	}
}
