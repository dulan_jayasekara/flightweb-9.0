/*Sanoj
package Flight.InitialLoading;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

public class LoadProperty {
	
	
	Logger 							logger				= null;
	Map<String, String> 			Propertymap 		= null;
	String							propfilepath		= "Config.properties";
	File							input				= null;
	Map<Integer, ArrayList<String>> OriginDestination 	= null;
	
	public Map<String, String> getProperty()
	{
		DOMConfigurator.configure("log4j.xml");
		logger   = Logger.getLogger(this.getClass());
	    try {
	    	logger.debug("Loading Properties");
				Propertymap         = loadProperties(propfilepath);
			logger.info("Properties Loaded Successfully----> "+Propertymap);
			} catch (Exception e) {
			logger.fatal("Error With Loading Properties ---> " + e.toString());
			}
	    
	    return Propertymap;
	}
	
	public Map<String, String> loadProperties(String filepath) throws IOException {
		
		Map<String, String> PropertyMap =  new HashMap<String, String>();
	
		   Properties prop = new Properties();
		   FileReader fs   = new FileReader(new File(filepath));
		   prop.load(fs);
		   
	       for(String key : prop.stringPropertyNames())
	       {
	    	   PropertyMap.put(key, prop.getProperty(key));
	       }
	
       return PropertyMap;
	}

}
*/